//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VJCONTROLLER_H
#define NEON_VJCONTROLLER_H

#include "GEGraphics.h"	// include this first!

#include "FPS.h"
#include "Sound.h"
#include "VJSource.h"
#include "VJLayer.h"

#define NUM_MASTER_LAYERS		(10)
#define NUM_GRID_PAGES			(5)
#define GRID_PAGE_COLS			(NUM_MASTER_LAYERS)
#define GRID_PAGE_ROWS			(8)

class CDisplayWindow;
class CDisplayDevice;
class CSource;
class CEffect;
class CFont;
class CVJSource;
class CVJLayer;
struct TCtrlVar;


//---------------------------------------------------------------------------//
//	Class CVJController
//---------------------------------------------------------------------------//
class CVJController {
public:
	CVJController();
	~CVJController();

	auto Init(THWnd hWnd, int nNumLayers, TGraphicsMode const& tMode, TVectorI2 const& vCustTex1, TVectorI2 const& vCustTex2, bool bCanMoveOutput, bool bShowMouse) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Reset() -> void;
	auto Update() -> void;
	auto Draw() -> void;
	auto GetTime() -> float;
	auto SetTime(float fTime) -> void;

	// Effects
	auto GetSource(uint uSourceID) -> CVJSource*;
	auto SourceAttach(uint uSourceID, int iLayer) -> void;
	auto SourceUnattach(uint uSourceID) -> void;

	// ebp-> feature: layer next/prev/rand
	auto LayerMove(int iLayerFrom, int iLayerTo) -> void;
	auto LayerUnattach(int iLayer) -> void;
	auto GetLayer(int iLayer) -> CVJLayer*;

	auto SourceLoad(std::string const& sClass, std::string const& sFile) -> uint;
	auto SourceUnload(uint uSourceID) -> void;
	auto SourceRenderSnapshot(uint uSourceID, TVJSnapshot& snapshot) -> void;
	auto SourceSetTime(uint uSourceID, float fTime) -> void;
	auto SourceSetSpeed(uint uSourceID, float fSpeed) -> void;
	auto SourceSetAlpha(uint uSourceID, float fAlpha) -> void;
	auto SourceSetBlend(uint uSourceID, int iBlend) -> void;
	auto SourceSetQuality(uint uSourceID, int iQuality) -> void;
	auto SourceSetFade(uint uSourceID, float fLength) -> void;
	auto SourceSetReset(uint uSourceID, bool bReset) -> void;
	auto SourceGetLength(uint uSourceID) -> float;
	auto SourceGetPos(uint uSourceID) -> float;
	auto SourceAddEffect(uint uSourceID, int iSlot, std::string& sEffect) -> uint;
	auto SourceRemoveEffect(uint uSourceID, int iSlot) -> void;
	auto SourceBypassEffect(uint uSourceID, int iSlot, bool bBypassed) -> void;
	auto SourceSwapEffect(uint uSourceID, int iSlotA, int iSlotB) -> void;
	auto SourceGetEffectID(uint uSourceID, int iSlot) -> uint;

	auto SourceGetVarScopes(uint uSourceID, std::vector<std::string>& Scopes) -> void;
	auto SourceGetVarObjects(uint uSourceID, std::vector<std::string>& Objects, int iScope) -> void;
	auto SourceGetVarCtrls(uint uSourceID, int iScope = -1) -> TCtrlVar*;
	auto SourceSetVar(uint uSourceID, int iScope, int iObj, int iVar, void const* pData) -> void;
	auto SourceGetVar(uint uSourceID, int iScope, int iObj, int iVar) -> void const*;
	auto SourceGetEffectVarCtrls(uint uSourceID, uint uEffectID) -> TCtrlVar*;
	auto SourceSetEffectVar(uint uSourceID, uint uEffectID, int iVar, void const* pData) -> void;
	auto SourceGetEffectVar(uint uSourceID, uint uEffectID, int iVar) -> void const*;

	// Options
	auto DrawDebug(bool bEnable) -> void { m_bDrawDebug = bEnable; }
	auto ToggleDrawDebug() -> void { m_bDrawDebug = !m_bDrawDebug; }
	auto GetDrawDebug() -> bool { return m_bDrawDebug; }
	auto SetFullScreen(bool bFullscreen) const -> void;
	auto SetClearColor(uint uColor) -> void { m_uClearColor = uColor; }
	auto SetMasterAlpha(float fMasterAlpha) -> void { m_fMasterAlpha = Clamp(fMasterAlpha, 0.f, 1.f); }
	auto SetMasterBypass(bool bMasterBypass) -> void { m_bMasterBypass = bMasterBypass; }
	auto ResetDisplayWindow() const -> void;

	// Sound
	auto GetSound() -> CSound* { return m_pSound; }
	auto GetFFT() const -> float const* { return m_pFFT; }
	auto GetFFTChannel() const -> float const* { return m_pFFTChannel; }
	auto GetFFTChannel(uint chan) const -> float { return m_pFFTChannel[chan < FREQ_CHANNELS ? chan : 0]; }
	auto SetGain(float fGain) -> void { m_fGain = fGain; }

	auto GetDisplayWindow() const -> CDisplayWindow* { return m_pDisplayWindow; }

	// ebp-> debug
	auto Dummy() -> void {}

private:
	auto RunFFT() -> void;
	bool m_bOk;
	bool m_bDrawDebug;
	bool m_bProcessMessages;
	float m_fTime;
	float m_fTimeCur;
	CFont* m_pDebugFont;
	CTimer m_tTimer;
	CFPSTimer m_FPSTimer;

	// The main VJ output window...
	CDisplayWindow* m_pDisplayWindow;
	uint m_uClearColor;
	float m_fMasterAlpha;
	bool m_bMasterBypass;

	// Sound/FFT
	CSound* m_pSound;
	float m_fGain;
	float m_pFFT[FFT_SIZE_256];
	float m_pFFTChannel[FREQ_CHANNELS];

	// Layers/Effects
	uint m_uNextSourceID;
	std::vector<CVJLayer> m_vLayers;
	//typedef std::pair<int, CVJSource*> source_pair_type;
	std::map<uint, CVJSource*> m_mSources;

	// Rendering Textures
	uint m_uSnapshotTextureIDs[2];				// snapshot renders, source & target
	uint m_uRenderTextureIDs[Q_QUALITIES][2];	// quality vs. A/B effect rendering swap
};

namespace neon {
	static std::string const sDirNeon = "Neon";	// ebp-> what about Neon64?
	static std::string const sDirSources = "Media";
	static std::string const sDirEffects = "Effects";
	static std::string const sDirPresets = "Presets";
	static std::string const sDirProjects = "Projects";
} //namespace neon

#endif//NEON_VJCONTROLLER_H
