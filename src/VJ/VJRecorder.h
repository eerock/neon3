//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VJRECORDER_H
#define NEON_VJRECORDER_H

#pragma pack(push)
#pragma pack(1)
struct TVJEvent {
	uchar iEvent;
	float fTime;
};
#pragma pack(pop)

enum
{
	EV_FFT,
	EV_LOAD_SOURCE,
	EV_UNLOAD_SOURCE,
	EV_RELOAD_SOURCE,
	EV_ATTACH_SOURCE,
	EV_UNATTACH_LAYER,
	EV_RESET_SOURCE,
	EV_COMMAND_SOURCE,
	EV_EFFECT_COMMAND,
	EV_SET_SOURCE_SPEED,
	EV_SET_SOURCE_ALPHA,
	EV_SET_SOURCE_BLEND,
	EV_SET_EFFECT,
	EV_SET_EFFECT_VAR,
	EV_SET_SOURCE_VAR,
	EV_SET_SOURCE_TIME,
};

class CVJController;

class CVJRecorder {
public:
	CVJRecorder();
	~CVJRecorder();

	auto Init(CVJController* pVJController) -> bool;

	auto IsRecording() const -> bool { return m_pFile != nullptr; }

	auto StartRecording(char const* pszFile) -> bool;
	auto StopRecording() -> void;

	auto RecordFFT(float const* pFFT) -> void;
	auto RecordLoadSource(char const* pszFile) -> void;
	auto RecordUnloadSource(int iID) -> void;
	auto RecordReloadSource(char const* pszFile, int iID) -> void;
	auto RecordAttachSource(int iID, char iLayer) -> void;
	auto RecordUnattachLayer(char iLayer) -> void;
	auto RecordSourceReset(int iID) -> void;
	auto RecordSourceCommand(int iID, char const* pszCommand) -> void;
	auto RecordEffectCommand(int iID, char const* pszCommand) -> void;
	auto RecordSetSourceTime(int iID, float fTime) -> void;
	auto RecordSetSourceSpeed(int iID, float fSpeed) -> void;
	auto RecordSetSourceAlpha(int iID, float fAlpha) -> void;
	auto RecordSetSourceBlend(int iID, int iBlend) -> void;
	auto RecordSetEffect(int iID, char const* pszEffect) -> void;
	auto RecordSetEffectMode(int iID, int iMode) -> void;
	auto RecordSetEffectVar(int iID, char iVar, void* pValue) -> void;
	auto RecordSetSourceVar(char iScope, char iObj, int iID, char iVar, void* pValue) -> void;

private:
	bool m_bOk;
	FILE* m_pFile;
	CVJController* m_pVJController;
	TVJEvent m_tEvent;
};

#endif//NEON_VJRECORDER_H
