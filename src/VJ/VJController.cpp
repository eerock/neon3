//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "VJController.h"
#include "VJ.h"
#include "DisplayWindow.h"
#include "Effects/Effect.h"
#include "Sources/Source.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJController::CVJController()
	: m_bOk(false)
	, m_bDrawDebug(false)
	, m_bProcessMessages(false)
	, m_fTime(0.f)
	, m_fTimeCur(0.f)
	, m_pDebugFont(nullptr)
	, m_tTimer()
	, m_FPSTimer()
	, m_pDisplayWindow(nullptr)
	, m_uClearColor(0)
	, m_fMasterAlpha(1.f)
	, m_bMasterBypass(false)
	, m_pSound(nullptr)
	, m_fGain(1.f)
	, m_uNextSourceID(INVALID_ID)
	, m_vLayers()
	, m_mSources()
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJController::~CVJController() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJController\n"));
#endif
	if (IsOk()) {
		m_bOk = false;

		auto pAppOpts = CServiceLocator<IAppOptionsService>::GetService();
		pAppOpts->Set("neonconfig.display.showfps", m_bDrawDebug);

		auto tOutWinRect = m_pDisplayWindow->GetWindowRect();
		pAppOpts->Set("neonconfig.display.rleft", tOutWinRect.x);
		pAppOpts->Set("neonconfig.display.rtop", tOutWinRect.y);
		pAppOpts->Set("neonconfig.display.rwidth", tOutWinRect.w);
		pAppOpts->Set("neonconfig.display.rheight", tOutWinRect.h);

		// ebp(old note??)-> should make the layers pointers so we don't call clear directly
		// and instead let the layer destructor handle it...
		for (auto& layer : m_vLayers) {
			layer.Clear();
		}
		m_vLayers.clear();

		// ebp-> need to delete the CVJSource*'s
		for (auto it = m_mSources.begin(); it != m_mSources.end(); ++it) {
			DELETE_PTR((*it).second);
		}
		m_mSources.clear();

		DELETE_PTR(m_pSound);
		DELETE_PTR(m_pDebugFont);

		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		for (int i = 0; i < Q_QUALITIES; ++i) {
			pMatMgr->RemoveTexture(m_uRenderTextureIDs[i][0]);
			pMatMgr->RemoveTexture(m_uRenderTextureIDs[i][1]);
		}
		pMatMgr->RemoveTexture(m_uSnapshotTextureIDs[0]);
		pMatMgr->RemoveTexture(m_uSnapshotTextureIDs[1]);

		DELETE_PTR(m_pDisplayWindow);
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CVJController::Init(THWnd hWnd,
						 int nNumLayers,
						 TGraphicsMode const& tMode,
						 TVectorI2 const& vCustTex1,
						 TVectorI2 const& vCustTex2,
						 bool bCanMoveOutput,
						 bool bShowMouse) -> bool {
	m_uClearColor = 0;

	IAppOptionsService* pAppOpts = 
		CServiceLocator<IAppOptionsService>::GetService();

	TRect tWindow(
		pAppOpts->Get("neonconfig.display.rleft", 0),
		pAppOpts->Get("neonconfig.display.rtop", 0),
		pAppOpts->Get("neonconfig.display.rwidth", DISPLAY_WINDOW_WIDTH),
		pAppOpts->Get("neonconfig.display.rheight", DISPLAY_WINDOW_HEIGHT)
	);

	m_pDisplayWindow = NEW CDisplayWindow;
	if (!m_pDisplayWindow->Init(
			tMode,
			tWindow,
			bCanMoveOutput,
			0,
			hWnd,
			bShowMouse)) {
		GLOG(("ERROR: Couldn't instantiate/initialize a CDisplayWindow!\n"));
		DELETE_PTR(m_pDisplayWindow);
		return false;
	}

	// Debug Font...
	// ebp-> is it really a 'Debug' font?  if so then only instantiate it in Debug.
	m_pDebugFont = NEW CFont;
	if (!m_pDebugFont->Init("Arial", 24, false, false)) {
		GLOG(("ERROR: Couldn't instantiate/initialize a Font!\n"));
		DELETE_PTR(m_pDebugFont);
	}

	// ebp-> whether to draw debug text on screen
	m_bDrawDebug = (false != pAppOpts->Get("neonconfig.display.showfps", false));

	// Sound & FFT...
	m_fGain = 1.f;
	m_pSound = NEW CSound(hWnd);

	// FFT Reset...
	memset(m_pFFT, 0, sizeof(float) * FFT_SIZE_256);
	memset(m_pFFTChannel, 0, sizeof(float) * FREQ_CHANNELS);

	m_tTimer.Reset();

	// Layers
	m_vLayers.resize(nNumLayers);
	for (auto& layer : m_vLayers) {
		layer.Clear();
	}

	m_FPSTimer.Init();
	m_bProcessMessages = (hWnd == 0);

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)		// because of the texture formats
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	m_uSnapshotTextureIDs[0] = pMatMgr->AddTexture(TVJSnapshot::WIDTH, TVJSnapshot::HEIGHT, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	m_uSnapshotTextureIDs[1] = pMatMgr->AddTexture(TVJSnapshot::WIDTH, TVJSnapshot::HEIGHT, HARD_TEX_FORMAT_A8R8G8B8, false, false, false, CTexture::SYSTEMMEM);
	m_uRenderTextureIDs[Q_FULL0][0] = pMatMgr->AddTexture(tMode.iWidth, tMode.iHeight, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	m_uRenderTextureIDs[Q_FULL0][1] = pMatMgr->AddTexture(tMode.iWidth, tMode.iHeight, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	m_uRenderTextureIDs[Q_CUST1][0] = pMatMgr->AddTexture(vCustTex1.x, vCustTex1.y, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	m_uRenderTextureIDs[Q_CUST1][1] = pMatMgr->AddTexture(vCustTex1.x, vCustTex1.y, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	m_uRenderTextureIDs[Q_CUST2][0] = pMatMgr->AddTexture(vCustTex2.x, vCustTex2.y, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	m_uRenderTextureIDs[Q_CUST2][1] = pMatMgr->AddTexture(vCustTex2.x, vCustTex2.y, HARD_TEX_FORMAT_A8R8G8B8, false, true);
#else
	UNREFERENCED_PARAMETER(vCustTex1);
	UNREFERENCED_PARAMETER(vCustTex2);
#endif//GFX_ENGINE

	Reset();

	GLOG(("Init: %d x %d @ %dHz on device %d\n", tMode.iWidth, tMode.iHeight, tMode.iRefresh, tMode.iDevice));
	GLOG(("Quality1: %d x %d\n", vCustTex1.x, vCustTex1.y));
	GLOG(("Quality2: %d x %d\n", vCustTex2.x, vCustTex2.y));

	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Reset
//---------------------------------------------------------------------------//
auto CVJController::Reset() -> void {
	for (auto& layer : m_vLayers) {
		layer.Clear();
	}

	m_tTimer.Reset();
	m_fTime = 0.f;
	m_fTimeCur = 0.f;
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CVJController::Update() -> void {
	if (m_pSound != nullptr) {
		m_pSound->Update();
	}
	RunFFT();
	SetTime(GetTime());
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CVJController::Draw() -> void {
	//GLOG(("CVJController::Draw()\n"));
	//auto pD3D = g_DisplayDevice.GetD3DDevice();

	m_FPSTimer.Frame();

	// Initialize the frame...
	// ebp-> Output Window Clear Color
	g_DisplayDevice.Clear(true, false, m_uClearColor, 0);
	g_DisplayDevice.BeginFrame();
	g_DisplayDevice.BeginScene();

	// Draw all layers
	if (!m_bMasterBypass) {
		for (auto& layer : m_vLayers) {
			layer.Draw(&g_DisplayDevice, m_fMasterAlpha);
		}
	}

	{	// draw the fps to the display window title...
		std::stringstream ss;
		ss << std::setprecision(2) << std::fixed;
		ss << m_FPSTimer.GetFPS() << " fps";
		m_pDisplayWindow->SetTitle(ss.str().c_str());
	}

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (m_bDrawDebug && m_pDebugFont) {
		// draw the fps, device, fullscreen
		std::stringstream ss;
		ss << std::setprecision(2) << std::fixed;
		ss << m_FPSTimer.GetFPS() << " fps - " << m_fTime << " sec";
		ss << " - Device " << g_DisplayDevice.GetModoGrafico().iDevice;
		if (m_pDisplayWindow->IsFullScreen()) {
			ss << " - Fullscreen";
		}
		m_pDebugFont->Print(&g_DisplayDevice, ss.str().c_str(), TVectorI2(6, 6), HARD_COLOR_ARGB(190, 0, 0, 0), DT_LEFT);
		m_pDebugFont->Print(&g_DisplayDevice, ss.str().c_str(), TVectorI2(5, 5), HARD_COLOR_ARGB(255, 255, 255, 255), DT_LEFT);
	}
#endif//GFX_ENGINE_DIRECT3D

	// Finish the frame and display
	g_DisplayDevice.EndScene();
	g_DisplayDevice.EndFrame();
	g_DisplayDevice.PageFlip();
}


//---------------------------------------------------------------------------//
//	GetTime
//---------------------------------------------------------------------------//
auto CVJController::GetTime() -> float {
	return m_tTimer.Get();
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CVJController::SetTime(float fTime) -> void {
	auto fWave = GetFFTChannel();
	CEngine3D::SetRegisterData(CEngine3D::V_WAVE, &fWave[0]);
	CEngine3D::SetRegisterData(CEngine3D::V_WAVE1, &fWave[1]);
	CEngine3D::SetRegisterData(CEngine3D::V_WAVE2, &fWave[2]);
	CEngine3D::SetRegisterData(CEngine3D::V_WAVE3, &fWave[3]);
	CEngine3D::SetRegisterData(CEngine3D::V_WAVE4, &fWave[4]);
	CEngine3D::SetRegisterData(CEngine3D::V_WAVE5, &fWave[5]);

	for (auto& layer : m_vLayers) {
		layer.SetTime(fTime);
	}

	m_fTime = fTime;

	// Procesado de mensajes si somos la ventana principal
	if (m_bProcessMessages) {
		m_pDisplayWindow->ProcessMessages();
	}

	m_pDisplayWindow->UpdateFullScreen();
}


//---------------------------------------------------------------------------//
//	GetSource
//---------------------------------------------------------------------------//
auto CVJController::GetSource(uint uSourceID) -> CVJSource* {
	if (m_mSources.count(uSourceID)) {
		return m_mSources[uSourceID];
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	SourceAttach
//---------------------------------------------------------------------------//
auto CVJController::SourceAttach(uint uSourceID, int iLayer) -> void {
	ASSERT(ValidIndex(iLayer, static_cast<int>(m_vLayers.size())));
	if (ValidIndex(iLayer, static_cast<int>(m_vLayers.size()))) {
		auto pVJSource = GetSource(uSourceID);
		for (auto& layer : m_vLayers) {
			if (layer.GetSourceOut() == pVJSource) {
				layer.Clear(false, true);
			}
		}
		m_vLayers[iLayer].Attach(m_fTime, pVJSource);
	}
}


//---------------------------------------------------------------------------//
//	SourceUnattach
//---------------------------------------------------------------------------//
auto CVJController::SourceUnattach(uint uSourceID) -> void {
	auto pVJSource = GetSource(uSourceID);
	// unattach its layer
	for (auto& layer : m_vLayers) {
		if (layer.GetSource() == pVJSource) {
			layer.Unattach(m_fTime);
		}
	}
}


//---------------------------------------------------------------------------//
//	LayerMove
//---------------------------------------------------------------------------//
auto CVJController::LayerMove(int iLayerFrom, int iLayerTo) -> void {
	ASSERT(ValidIndex(iLayerFrom, static_cast<int>(m_vLayers.size())));
	ASSERT(ValidIndex(iLayerTo, static_cast<int>(m_vLayers.size())));
	if (ValidIndex(iLayerFrom, static_cast<int>(m_vLayers.size())) && ValidIndex(iLayerTo, static_cast<int>(m_vLayers.size()))) {
		m_vLayers[iLayerFrom].MoveTo(m_fTime, &m_vLayers[iLayerTo]);
	}
}


//---------------------------------------------------------------------------//
//	LayerUnattach
//---------------------------------------------------------------------------//
auto CVJController::LayerUnattach(int iLayer) -> void {
	ASSERT(ValidIndex(iLayer, static_cast<int>(m_vLayers.size())));
	if (ValidIndex(iLayer, static_cast<int>(m_vLayers.size()))) {
		m_vLayers[iLayer].Unattach(m_fTime);
	}
}


//---------------------------------------------------------------------------//
//	GetLayer
//	todo: refactor to return CVJLayer const& / CVJLayer&
//---------------------------------------------------------------------------//
auto CVJController::GetLayer(int iLayer) -> CVJLayer* {
	ASSERT(ValidIndex(iLayer, static_cast<int>(m_vLayers.size())));
	if (ValidIndex(iLayer, static_cast<int>(m_vLayers.size()))) {
		return &m_vLayers[iLayer];
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	SourceLoad
//---------------------------------------------------------------------------//
auto CVJController::SourceLoad(std::string const& sClass, std::string const& sFile) -> uint {
	// ebp-> FIXIT: source files with '&' in the file name seem to have difficulty loading.  why?
	int uSourceID = INVALID_ID;
	auto pVJSource = NEW CVJSource;
	if (pVJSource->Load(sClass, sFile, ++m_uNextSourceID, m_uRenderTextureIDs)) {
		uSourceID = m_uNextSourceID;
		m_mSources[uSourceID] = pVJSource;
		GLOG(("#%d (0x%08X) - CVJController::SourceLoad(%s, %s)\n", uSourceID, pVJSource, sClass.c_str(), sFile.c_str()));
	} else {
		DELETE_PTR(pVJSource);
	}
	return uSourceID;
}


//---------------------------------------------------------------------------//
//	SourceUnload
//---------------------------------------------------------------------------//
auto CVJController::SourceUnload(uint uSourceID) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource != nullptr) {
		GLOG(("#%d - CVJController::SourceUnload(0x%08X)\n", uSourceID, pVJSource));
		// Quitarlo de los layers en los que este
		// es-en: removing the layers in this
		for (auto& layer : m_vLayers) {
			if (layer.GetSourceOut() == pVJSource) {
				layer.Clear(false, true);
			}

			if (layer.GetSource() == pVJSource) {
				layer.Clear(true, false);
			}
		}

		// remove it...
		m_mSources.erase(uSourceID);
		// and delete it...
		DELETE_PTR(pVJSource);
	}
}


//---------------------------------------------------------------------------//
//	SourceRenderSnapshot
//---------------------------------------------------------------------------//
auto CVJController::SourceRenderSnapshot(uint uSourceID, TVJSnapshot& tSnapshot) -> void {
	tSnapshot.m_uTexRenderID = m_uSnapshotTextureIDs[0];
	tSnapshot.m_uTexSnapshotID = m_uSnapshotTextureIDs[1];
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->CreateSnapshot(tSnapshot);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetTime
//---------------------------------------------------------------------------//
auto CVJController::SourceSetTime(uint uSourceID, float fTime) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetTime(fTime);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetSpeed
//---------------------------------------------------------------------------//
auto CVJController::SourceSetSpeed(uint uSourceID, float fSpeed) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetSpeed(fSpeed);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetAlpha
//---------------------------------------------------------------------------//
auto CVJController::SourceSetAlpha(uint uSourceID, float fAlpha) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetAlpha(fAlpha);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetBlend
//---------------------------------------------------------------------------//
auto CVJController::SourceSetBlend(uint uSourceID, int iBlend) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetBlend(iBlend);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetQuality
//---------------------------------------------------------------------------//
auto CVJController::SourceSetQuality(uint uSourceID, int iQuality) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetQuality(iQuality);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetFade
//---------------------------------------------------------------------------//
auto CVJController::SourceSetFade(uint uSourceID, float fLength) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetFadeLength(fLength);
	}
}


//---------------------------------------------------------------------------//
//	SourceSetReset
//---------------------------------------------------------------------------//
auto CVJController::SourceSetReset(uint uSourceID, bool bReset) -> void {
	auto pVJSource = GetSource(uSourceID);
	if (pVJSource) {
		pVJSource->SetResetOnAttach(bReset);
	}
}


//---------------------------------------------------------------------------//
//	SourceGetLength
//---------------------------------------------------------------------------//
auto CVJController::SourceGetLength(uint uSourceID) -> float {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	return pVJSource->GetSource()->GetLength();
}


//---------------------------------------------------------------------------//
//	SourceGetPos
//---------------------------------------------------------------------------//
auto CVJController::SourceGetPos(uint uSourceID) -> float {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	auto fLen = pVJSource->GetSource()->GetLength();
	if (fLen > 0.f) {
		return fmod(pVJSource->GetTime(), fLen);
	} else {
		return 0.f;
	}
}


//---------------------------------------------------------------------------//
//	SourceAddEffect
//---------------------------------------------------------------------------//
auto CVJController::SourceAddEffect(uint uSourceID, int iSlot, std::string& sEffect) -> uint {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource);
	return pVJSource->AddEffect(iSlot, sEffect);
}


//---------------------------------------------------------------------------//
//	SourceRemoveEffect
//---------------------------------------------------------------------------//
auto CVJController::SourceRemoveEffect(uint uSourceID, int iSlot) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource);
	pVJSource->RemoveEffect(iSlot);
}


//---------------------------------------------------------------------------//
//	SourceBypassEffect
//---------------------------------------------------------------------------//
auto CVJController::SourceBypassEffect(uint uSourceID, int iSlot, bool bBypassed) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource);
	pVJSource->BypassEffect(iSlot, bBypassed);
}


//---------------------------------------------------------------------------//
//	SourceSwapEffect
//---------------------------------------------------------------------------//
auto CVJController::SourceSwapEffect(uint uSourceID, int iSlotA, int iSlotB) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource);
	pVJSource->SwapEffect(iSlotA, iSlotB);
}


//---------------------------------------------------------------------------//
//	SourceGetEffectID
//---------------------------------------------------------------------------//
auto CVJController::SourceGetEffectID(uint uSourceID, int iSlot) -> uint {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource);
	return pVJSource->GetEffectID(iSlot);
}


//---------------------------------------------------------------------------//
//	SourceGetVarScopes
//---------------------------------------------------------------------------//
auto CVJController::SourceGetVarScopes(uint uSourceID, std::vector<std::string>& vScopes) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	pVJSource->GetSource()->GetVarScopes(vScopes);
}


//---------------------------------------------------------------------------//
//	SourceGetVarObjects
//---------------------------------------------------------------------------//
auto CVJController::SourceGetVarObjects(uint uSourceID, std::vector<std::string>& vObjects, int iScope) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	pVJSource->GetSource()->GetVarObjects(vObjects, iScope);
}


//---------------------------------------------------------------------------//
//	SourceGetVarCtrls
//---------------------------------------------------------------------------//
auto CVJController::SourceGetVarCtrls(uint uSourceID, int iScope) -> TCtrlVar* {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	return pVJSource->GetSource()->GetVarCtrls(iScope);
}


//---------------------------------------------------------------------------//
//	SourceSetVar
//---------------------------------------------------------------------------//
auto CVJController::SourceSetVar(uint uSourceID, int iScope, int iObj, int iVar, void const* pData) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	pVJSource->GetSource()->SetVar(iScope, iObj, iVar, pData);
}


//---------------------------------------------------------------------------//
//	SourceGetVar
//---------------------------------------------------------------------------//
auto CVJController::SourceGetVar(uint uSourceID, int iScope, int iObj, int iVar) -> void const* {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetSource());
	return pVJSource->GetSource()->GetVar(iScope, iObj, iVar);
}


//---------------------------------------------------------------------------//
//	SourceGetEffectVarCtrls
//---------------------------------------------------------------------------//
auto CVJController::SourceGetEffectVarCtrls(uint uSourceID, uint uEffectID) -> TCtrlVar* {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetEffectByID(uEffectID));
	return pVJSource->GetEffectByID(uEffectID)->GetVarCtrls();
}


//---------------------------------------------------------------------------//
//	SourceSetEffectVar
//---------------------------------------------------------------------------//
auto CVJController::SourceSetEffectVar(uint uSourceID, uint uEffectID, int iVar, void const* pData) -> void {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetEffectByID(uEffectID));
	pVJSource->GetEffectByID(uEffectID)->SetVar(iVar, pData);
}


//---------------------------------------------------------------------------//
//	SourceGetEffectVar
//---------------------------------------------------------------------------//
auto CVJController::SourceGetEffectVar(uint uSourceID, uint uEffectID, int iVar) -> void const* {
	auto pVJSource = GetSource(uSourceID);
	ASSERT(pVJSource && pVJSource->GetEffectByID(uEffectID));
	return pVJSource->GetEffectByID(uEffectID)->GetVar(iVar);
}


//---------------------------------------------------------------------------//
//	SetFullScreen
//---------------------------------------------------------------------------//
auto CVJController::SetFullScreen(bool bFullscreen) const -> void {
	if (m_pDisplayWindow) {
		m_pDisplayWindow->SwitchFullScreen(bFullscreen);
	}
}


//---------------------------------------------------------------------------//
//	ResetDisplayWindow
//---------------------------------------------------------------------------//
auto CVJController::ResetDisplayWindow() const -> void {
	SetFullScreen(false);
	m_pDisplayWindow->ResetWindow();
}


//---------------------------------------------------------------------------//
//	RunFFT
//---------------------------------------------------------------------------//
auto CVJController::RunFFT() -> void {
	// warning: changing the fftsize here to 512 resulted in memory stomp crashes.
	// keep it at 256 until fft gets checked in, then mess with sizes and 
	// make everything more robust.
	//if (m_pSound != nullptr && m_pSound->GetFFT(m_pFFT, FFT_SIZE_256, m_fGain)) {


	float fft2[FFT_SIZE_256];
	if (m_pSound != nullptr && m_pSound->GetFFT(fft2, FFT_SIZE_256)) {
		for (int i = 0; i < FFT_SIZE_256; ++i) {
			// ebp-> what's with the magic numbers 0.09 & 0.04?  Scaling?
			fft2[i] = fft2[i] * (1.f + i * 0.09f) * m_fGain;
			if (fft2[i] > m_pFFT[i]) {
				m_pFFT[i] = fft2[i];
				if (m_pFFT[i] > 1.f) {
					m_pFFT[i] = 1.f;
				}
			} else {
				m_pFFT[i] = m_pFFT[i] - 0.04f;
				if (m_pFFT[i] < fft2[i]) {
					m_pFFT[i] = fft2[i];
				}
			}
		}


		// FreqChannels
		for (int i = 0; i < FREQ_CHANNELS; ++i) {
			m_pFFTChannel[i] = 0;
			// Range
			int l = 0;
			int r = 255;
			if (i > 0) {
				l = ((i - 1) * (255 / 5));
				r = (l + (255 / 5));
			}

			for (int j = l; j < r; ++j) {
				if (m_pFFT[j] > m_pFFTChannel[i]) {
					m_pFFTChannel[i] = m_pFFT[j];
				}
			}
		}
	}
}
