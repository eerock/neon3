//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "VJLoader.h"
#include "GEDisplayDevice.h"
#include "GETemp.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJLoader::CVJLoader(uint num)
	: m_uMax(num)
	, m_uPos(0)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJLoader::~CVJLoader() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJLoader\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Next
//---------------------------------------------------------------------------//
auto CVJLoader::Next() -> void {
	if (++m_uPos > m_uMax) {
		m_uPos = m_uMax;
	}
}


//---------------------------------------------------------------------------//
//	Draw
//	ebp-> the only thing i can say is that this looks like a loadbar, and that
//	maybe the colors should be customizable.
//---------------------------------------------------------------------------//
auto CVJLoader::Draw(CDisplayDevice* pDD) -> void {
	pDD->BeginFrame();
	pDD->BeginScene();
	pDD->Clear(true, true, 0, 1.f);
	//float fWidth = static_cast<float>(pDD->Width());
	//float fHeight = static_cast<float>(pDD->Height());
	TVector2 vIni(6.f, 12.f);
	TVector2 vEnd(126.f, 18.f);
	float fPos = ((float)m_uPos * (vEnd.x - vIni.x - 4.f) / (float)m_uMax) + vIni.x + 2.f;
	neon::DrawQuadFlat(pDD, TVector2(vIni.x + 0, vIni.y + 0), TVector2(vEnd.x - 0, vEnd.y - 0), HARD_COLOR_ARGB(255, 255, 168, 0), 0.f);
	neon::DrawQuadFlat(pDD, TVector2(vIni.x + 1, vIni.y + 1), TVector2(vEnd.x - 1, vEnd.y - 1), HARD_COLOR_ARGB(255, 0, 0, 0), 0.f);
	neon::DrawQuadFlat(pDD, TVector2(vIni.x + 2, vIni.y + 2), TVector2(fPos, vEnd.y - 2), HARD_COLOR_ARGB(255, 255, 168, 0), 0.f);
	pDD->EndScene();
	pDD->EndFrame();
	pDD->PageFlip();
}
