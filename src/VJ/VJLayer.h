//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VJLAYER_H
#define NEON_VJLAYER_H

#include "Base.h"

class CDisplayDevice;
class CVJSource;

class CVJLayer {
public:
	CVJLayer();
	~CVJLayer();

	auto SetTime(float fTime) -> void;
	auto Draw(CDisplayDevice* pDD, float fMasterAlpha) -> void;

	auto Attach(float fTime, CVJSource* pVJSource) -> void;
	auto Unattach(float fTime) -> void;
	auto MoveTo(float fTime, CVJLayer* pVJLayer) -> void;
	auto Clear(bool bActive = true, bool bOut = true) -> void;

	auto SetBypass(bool bBypass) -> void { m_bLayerBypass = bBypass; }
	auto SetAlpha(float fAlpha) -> void { m_fLayerAlpha = fAlpha; }

	auto GetSource() -> CVJSource* { return m_pVJSource; }
	auto GetSourceOut() -> CVJSource* { return m_pVJSourceOut; }

private:
	CVJSource* m_pVJSource;		// the active "attached" source
	CVJSource* m_pVJSourceOut;	// the source that's fading out

	float m_fLayerAlpha;
	bool m_bLayerBypass;
};

#endif//NEON_VJLAYER_H
