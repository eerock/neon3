//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "VJ.h"
#include "Sources/Source.h"
#include "Effects/Effect.h"
#include "Xml.h"


static int s_BlendShader;


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJSource::CVJSource()
	: m_bOk(false)
	, m_bFadeIn(false)
	, m_bResetOnAttach(false)
	, m_uID(INVALID_ID)
	, m_iBlend(0)
	, m_iQuality(0)
	, m_fTime(0.f)
	, m_fLastRealTime(0.f)
	, m_fSpeed(1.f)
	, m_fAlpha(1.f)
	, m_fFadeAlpha(1.f)
	, m_fFadeLength(0.5f)
	, m_fFadeTime(0.f)
	, m_uNextEffectID(INVALID_ID)
	, m_eState(NORMAL)
	, m_pSource(nullptr)
{
	//GLOG(("CVJSource::Constructor()\n"));
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CVJSource::Load(std::string const& sClass, std::string const& sFile, uint uSourceID, void const* puRenderTextureIDs) -> bool {
	GLOG(("CVJSource::Load(%s, %s)\n", sClass.c_str(), sFile.c_str()));
	m_pSource = CSource::Create(sClass, sFile);
	if (m_pSource) {
		for (int i = 0; i < MAX_EFFECTS; ++i) {
			m_atEffects[i].uEffectID = INVALID_ID;
			m_atEffects[i].pEffect = nullptr;
			m_atEffects[i].bBypass = false;
		}

		m_bOk = true;
		m_bFadeIn = true;
		m_bResetOnAttach = true;
		m_uID = uSourceID;
		m_iBlend = BL_ALPHABLEND;
		m_iQuality = Q_FULL0;
		m_fTime = 0.f;
		m_fLastRealTime = 0.f;
		m_fSpeed = 1.f;
		m_fAlpha = 1.f;
		m_fFadeAlpha = 1.f;
		m_fFadeLength = 0.5f;
		m_fFadeTime = 0.f;
		m_uNextEffectID = INVALID_ID;

		for (int i = 0; i < Q_QUALITIES; ++i) {
			m_uuRenderTextureIDs[i][0] = ((uint*)puRenderTextureIDs)[i * 2 + 0];
			m_uuRenderTextureIDs[i][1] = ((uint*)puRenderTextureIDs)[i * 2 + 1];
		}
	}

	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJSource::~CVJSource() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJSource\n"));
#endif
	if (IsOk()) {
		for (int i = 0; i < MAX_EFFECTS; ++i) {
			DELETE_PTR(m_atEffects[i].pEffect);
			m_atEffects[i].uEffectID = INVALID_ID;
			m_atEffects[i].bBypass = false;
		}
		DELETE_PTR(m_pSource);
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Attach
//---------------------------------------------------------------------------//
auto CVJSource::Attach(float fTime) -> void {
	GLOG(("CVJSource::Attach - %f\n", fTime));
	m_pSource->OnAttached();
	if (m_bResetOnAttach) {
		m_fLastRealTime = fTime;
		m_fTime = 0.f;
	}
}


//---------------------------------------------------------------------------//
//	Unattach
//---------------------------------------------------------------------------//
auto CVJSource::Unattach() -> void {
	GLOG(("CVJSource::Unattach\n"));
	m_pSource->OnUnattached();
}


//---------------------------------------------------------------------------//
//	FadeIn
//---------------------------------------------------------------------------//
auto CVJSource::FadeIn(float fTime) -> void {
	m_fFadeTime = fTime;
	m_bFadeIn = true;
}


//---------------------------------------------------------------------------//
//	FadeOut
//---------------------------------------------------------------------------//
auto CVJSource::FadeOut(float fTime) -> void {
	m_fFadeTime = fTime;
	m_bFadeIn = false;
}


//---------------------------------------------------------------------------//
//	AddEffect
//---------------------------------------------------------------------------//
auto CVJSource::AddEffect(int iSlot, std::string& sEffect) -> uint {
	ASSERT(ValidIndex(iSlot, MAX_EFFECTS));
	PushDir(neon::sDirEffects);
	auto pEffect = CEffect::Create(sEffect);
	PopDir();
	if (pEffect != nullptr) {
		DELETE_PTR(m_atEffects[iSlot].pEffect);
		m_atEffects[iSlot].uEffectID = ++m_uNextEffectID;
		m_atEffects[iSlot].pEffect = pEffect;
		m_atEffects[iSlot].bBypass = false;
		GLOG(("#%d - CVJSource::AddEffect(slot: %d, %s)\n", m_uNextEffectID, iSlot, sEffect.c_str()));
		return m_atEffects[iSlot].uEffectID;
	}
	return INVALID_ID;
}


//---------------------------------------------------------------------------//
//	RemoveEffect
//---------------------------------------------------------------------------//
auto CVJSource::RemoveEffect(int iSlot) -> void {
	ASSERT(ValidIndex(iSlot, MAX_EFFECTS));
	DELETE_PTR(m_atEffects[iSlot].pEffect);
	GLOG(("#%d - CVJSource::RemoveEffect(slot: %d)\n", m_atEffects[iSlot].uEffectID, iSlot));
	m_atEffects[iSlot].uEffectID = INVALID_ID;
	m_atEffects[iSlot].bBypass = false;
}


//---------------------------------------------------------------------------//
//	BypassEffect
//---------------------------------------------------------------------------//
auto CVJSource::BypassEffect(int iSlot, bool bBypass) -> void {
	//GLOG(("CVJSource::BypassEffect(slot: %d, %d)\n", iSlot, bBypass));
	ASSERT(ValidIndex(iSlot, MAX_EFFECTS));
	m_atEffects[iSlot].bBypass = bBypass;
}


//---------------------------------------------------------------------------//
//	SwapEffect
//---------------------------------------------------------------------------//
auto CVJSource::SwapEffect(int iSlotA, int iSlotB) -> void {
	ASSERT(ValidIndex(iSlotA, MAX_EFFECTS));
	ASSERT(ValidIndex(iSlotB, MAX_EFFECTS));
	//GLOG(("CVJSource::SwapEffects(slotA: %d, slotB: %d)\n", iSlotA, iSlotB));
	Swap(m_atEffects[iSlotA].uEffectID, m_atEffects[iSlotB].uEffectID);
	Swap(m_atEffects[iSlotA].pEffect, m_atEffects[iSlotB].pEffect);
	Swap(m_atEffects[iSlotA].bBypass, m_atEffects[iSlotB].bBypass);
}


//---------------------------------------------------------------------------//
//	GetEffectByID
//---------------------------------------------------------------------------//
auto CVJSource::GetEffectByID(uint uEffectID) const -> CEffect* {
	auto iSlot = GetEffectSlot(uEffectID);
	if (iSlot != INVALID_INDEX) {
		ASSERT(ValidIndex(iSlot, MAX_EFFECTS));
		return m_atEffects[iSlot].pEffect;
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	GetEffectSlot
//---------------------------------------------------------------------------//
auto CVJSource::GetEffectSlot(uint uEffectID) const -> int {
	for (int slot = 0; slot < MAX_EFFECTS; ++slot) {
		if (m_atEffects[slot].uEffectID == uEffectID) {
			return slot;
		}
	}
	return INVALID_INDEX;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CVJSource::SetTime(float fTime) -> void {
	//GLOG(("VJSource::SetTime - %f\n", fTime));
	float fRunTime = (fTime - m_fLastRealTime) * m_fSpeed;
	m_fTime += fRunTime;
	m_fLastRealTime = fTime;

	float fFadePercent = 0.f;
	if (m_fFadeLength == 0.f) {
		fFadePercent = 1.f;
	} else {
		fFadePercent = (fTime - m_fFadeTime) / m_fFadeLength;
	}

	fFadePercent = Clamp(fFadePercent, 0.f, 1.f);

	if (m_bFadeIn) {
		m_fFadeAlpha = fFadePercent;
	} else {
		m_fFadeAlpha = 1.f - fFadePercent;
	}

	m_pSource->SetTime(m_fTime);

	for (int slot = 0; slot < MAX_EFFECTS; ++slot) {
		if (m_atEffects[slot].pEffect) {
			m_atEffects[slot].pEffect->SetTime(m_fTime);
		}
	}
}


//---------------------------------------------------------------------------//
//	Render
//	Updates current render of Source
//---------------------------------------------------------------------------//
auto CVJSource::Render(CDisplayDevice* pDD) -> void {
	if (IsVisible()) {
		int iQuality = Clamp(m_iQuality, 0, Q_QUALITIES - 1);
		m_pSource->Draw(pDD, m_uuRenderTextureIDs[iQuality][0]);
	}
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CVJSource::Draw(CDisplayDevice* pDD, float fLayerAlpha) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (IsVisible() && fLayerAlpha > 0.f) {
		int iQuality = Clamp(m_iQuality, 0, Q_QUALITIES - 1);
		float fRand = 0.f;
		float fSinTime = sinf(m_fTime);

		// ebp-> added a Layer Alpha value, but it doesn't work with time-based (motion blur) effects.
		float fAlpha = m_fAlpha * m_fFadeAlpha * fLayerAlpha;
		CEngine3D::SetRegisterData(CEngine3D::V_TIME, &m_fTime);
		CEngine3D::SetRegisterData(CEngine3D::V_ALPHA, &fAlpha);
		CEngine3D::SetRegisterData(CEngine3D::V_SINTIME, &fSinTime);

		fRand = static_cast<float>(rand() & 0xff) / 255.f;
		CEngine3D::SetRegisterData(CEngine3D::V_RAND1, &fRand);
		fRand = static_cast<float>(rand() & 0xff) / 255.f;
		CEngine3D::SetRegisterData(CEngine3D::V_RAND2, &fRand);
		fRand = static_cast<float>(rand() & 0xff) / 255.f;
		CEngine3D::SetRegisterData(CEngine3D::V_RAND3, &fRand);
		fRand = static_cast<float>(rand() & 0xff) / 255.f;
		CEngine3D::SetRegisterData(CEngine3D::V_RAND4, &fRand);

		// Save copy of the render target and apply all filters to it
		// We save a copy as filters can change its behaviour during the loop
		uint uTarget0TexID = m_uuRenderTextureIDs[iQuality][0];
		uint uTarget1TexID = m_uuRenderTextureIDs[iQuality][1];
		for (int slot = 0; slot < MAX_EFFECTS; ++slot) {
			if (!m_atEffects[slot].bBypass && m_atEffects[slot].pEffect) {
				if (m_atEffects[slot].pEffect->Apply(&g_DisplayDevice, uTarget0TexID, uTarget1TexID)) {
					Swap(uTarget0TexID, uTarget1TexID);
				}
			}
		}

		// ebp-> trying to apply a HLSL shader to do blend mode, but
		// there's seemingly no concept of a Source and Destination in shaders.
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

		//-------------------------------------------------------------------//
		// Draw final result on screen
		pDD->ApplyBasicShader();
		auto pD3D = pDD->GetD3DDevice();

		uint uAlpha = static_cast<uint>(fAlpha * 255.f);
		uint aColor = HARD_COLOR_ARGB(uAlpha, 255, 255, 255);
		pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
		pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, aColor);
		pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);

		// Only apply alpha modulate when texture is A8R8G8B8
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);

		pDD->SetRenderTarget(CMaterialManager::INVALID);
		pDD->SetBlendMode(m_iBlend, fAlpha);
		pDD->SetBilinearFiltering(0, true);
		pDD->SetBilinearFiltering(1, true);
		pDD->SetBilinearFiltering(2, true);
		pDD->SetBilinearFiltering(3, true);

		pMatMgr->SetTexture(uTarget0TexID, 0);
		neon::DrawQuad(pDD);
		//-------------------------------------------------------------------//
	}
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(fLayerAlpha);
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	CreateSnapshot
//---------------------------------------------------------------------------//
auto CVJSource::CreateSnapshot(TVJSnapshot& tSnapshot) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	bool bDrawing = g_DisplayDevice.IsDrawingScene();
	ASSERT(!bDrawing);
	if (!bDrawing) {
		g_DisplayDevice.BeginScene();
	}
	g_DisplayDevice.Clear(true, true, 0, 1.f);

	// Draw source on a texture
	auto pD3D = g_DisplayDevice.GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pTexSrc = pMatMgr->GetTexture(tSnapshot.m_uTexRenderID);
	auto pTexDst = pMatMgr->GetTexture(tSnapshot.m_uTexSnapshotID);

	float fLen = m_pSource->GetLength();
	//GLOG(("Length of this clip: %f\n", fLen));
	//if (fLen < 1.f) {
	//	fLen = 1.f;
	//} else {
	//	fLen *= 0.2f;
	//}
	//fLen = 1.f;
	fLen = 4.f;
	m_pSource->SetTime(fLen);
	m_pSource->Draw(&g_DisplayDevice, tSnapshot.m_uTexRenderID);

	if (!bDrawing) {
		g_DisplayDevice.EndScene();
	}

	// Coger el render y guardarlo
	// es-en: Take the render and save
	if (SUCCEEDED(pD3D->GetRenderTargetData(pTexSrc->GetSurfaceD3D(), pTexDst->GetSurfaceD3D()))) {
		TSurfaceDesc Desc;
		if (pTexDst->Lock(nullptr, Desc)) {
			uint* pSrc = (uint*)Desc.pBits;
			uint* pDst = tSnapshot.m_pData;
			for (int i = TVJSnapshot::WIDTH * TVJSnapshot::HEIGHT; i > 0; --i) {
				//uint c = *pSrc++;
				//uint a = (c >> 24);
				//uint r = (((c) & 0xFF) * a) >> 8;
				//uint g = (((c >> 8) & 0xFF) * a) >> 8;
				//uint b = (((c >> 16) & 0xFF) * a) >> 8;
				//*pDst++ = 0xFF000000 | (r << 16) | (g << 8) | b;

				// ABGR -> ARGB
				uint c = *pSrc++;
				uchar r = (c) & 0xff;
				uchar g = (c >> 8) & 0xff;
				uchar b = (c >> 16) & 0xff;
				//uchar a = (c >> 24) & 0xff;
				*pDst++ = (0xff << 24) | (r << 16) | (g << 8) | b;
			}
			pTexDst->Unlock();
		}
	}
#else
	UNREFERENCED_PARAMETER(pSnapshot);
#endif//GFX_ENGINE
}
