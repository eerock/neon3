//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VJSOURCE_H
#define NEON_VJSOURCE_H

#include "Base.h"
#include "VJDefs.h"

class CSource;
class CEffect;
class CDisplayDevice;


//---------------------------------------------------------------------------//
//	Struct TVJSnapshot
//---------------------------------------------------------------------------//
typedef struct {
	enum {
		WIDTH = 128,
		HEIGHT = 128,
	};
	uint m_uTexRenderID;
	uint m_uTexSnapshotID;
	uint m_pData[WIDTH * HEIGHT];
} TVJSnapshot;


//---------------------------------------------------------------------------//
//	Class CVJSource
//---------------------------------------------------------------------------//
class CVJSource {
public:
	enum EState {
		NORMAL,
		FADING_IN,
		FADING_OUT,
	};

	// ebp-> fixit: this is also defined in MGEffects.h
	static const int MAX_EFFECTS = 5;

public:
	CVJSource();
	~CVJSource();

	auto Load(std::string const& sClass, std::string const& sFile, uint uSourceID, void const* iTexturasRender) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto GetID() const -> uint { return m_uID; }
	auto GetTime() const -> float { return m_fTime; }
	auto GetState() const -> EState { return m_eState; }

	auto SetTime(float fTime) -> void;
	auto Render(CDisplayDevice* pDD) -> void;
	auto Draw(CDisplayDevice* pDD, float fLayerAlpha = 1.f) -> void;

	auto Attach(float fTime) -> void;
	auto Unattach() -> void;
	auto FadeIn(float fTime) -> void;
	auto FadeOut(float fTime) -> void;
	auto IsVisible() const -> bool { return (m_fAlpha * m_fFadeAlpha) > 0.f; }
	auto GetFadeAlpha() const -> float { return m_fFadeAlpha; }

	auto GetSource() const -> CSource* { return m_pSource; }
	auto GetEffectID(int iSlot) const -> uint {
		ASSERT(ValidIndex(iSlot, MAX_EFFECTS));
		return m_atEffects[iSlot].uEffectID;
	}
	auto GetEffectBySlot(int iSlot) const -> CEffect* {
		ASSERT(ValidIndex(iSlot, MAX_EFFECTS));
		return m_atEffects[iSlot].pEffect;
	}
	auto GetEffectByID(uint uEffectID) const -> CEffect*;
	auto GetEffectSlot(uint uEffectID) const -> int;

	// Vars
	auto SetAlpha(float fAlpha) -> void { m_fAlpha = fAlpha; }
	auto GetAlpha() const -> float { return m_fAlpha; }
	auto SetBlend(int iBlend) -> void { m_iBlend = iBlend; }
	auto GetBlend() const -> int { return m_iBlend; }
	auto SetSpeed(float fSpeed) -> void { m_fSpeed = fSpeed; }
	auto GetSpeed() const -> float { return m_fSpeed; }
	auto SetFadeLength(float fFadeLength) -> void { m_fFadeLength = fFadeLength; }
	auto GetFadeLength() const -> float { return m_fFadeLength; }
	auto SetResetOnAttach(bool bReset) -> void { m_bResetOnAttach = bReset; }
	auto GetResetOnAttach() const -> float { return m_bResetOnAttach; }
	auto SetQuality(int iQuality) -> void { m_iQuality = iQuality; }
	auto GetQuality() const -> int { return m_iQuality; }

	auto AddEffect(int iSlot, std::string& sEffect) -> uint;
	auto RemoveEffect(int iSlot) -> void;
	auto BypassEffect(int iSlot, bool bBypass) -> void;
	auto SwapEffect(int iSlotSrc, int iSlotDst) -> void;

	auto CreateSnapshot(TVJSnapshot& tSnapshot) -> void;

private:
	struct TVJEffect {
		uint uEffectID;
		CEffect* pEffect;
		bool bBypass;

		TVJEffect::TVJEffect()
			: uEffectID(INVALID_ID), pEffect(nullptr), bBypass(false)
		{}
	};

	bool m_bOk;
	bool m_bFadeIn;
	bool m_bResetOnAttach;
	uint m_uID;
	int m_iBlend;
	int m_iQuality;
	float m_fTime;
	float m_fLastRealTime;
	float m_fSpeed;
	float m_fAlpha;
	float m_fFadeAlpha;
	float m_fFadeLength;
	float m_fFadeTime;
	uint m_uuRenderTextureIDs[Q_QUALITIES][2];
	uint m_uNextEffectID;
	EState m_eState;
	CSource* m_pSource;
	TVJEffect m_atEffects[MAX_EFFECTS];
};

#endif//NEON_VJSOURCE_H
