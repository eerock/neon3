//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "VJLayer.h"
#include "VJSource.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJLayer::CVJLayer()
	: m_pVJSource(nullptr)
	, m_pVJSourceOut(nullptr)
	, m_fLayerAlpha(1.f)
	, m_bLayerBypass(false)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJLayer::~CVJLayer() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJLayer\n"));
#endif
	Clear();
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CVJLayer::SetTime(float fTime) -> void {
	// Actual
	if (m_pVJSource) {
		m_pVJSource->SetTime(fTime);
	}

	// Fade Out
	if (m_pVJSourceOut) {
		m_pVJSourceOut->SetTime(fTime);
		if (m_pVJSourceOut->GetFadeAlpha() <= 0.f) {
			Clear(false, true);
		}
	}
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CVJLayer::Draw(CDisplayDevice* pDD, float fMasterAlpha) -> void {
	if (!m_bLayerBypass) {
		if (m_pVJSource) {
			m_pVJSource->Render(pDD);
			m_pVJSource->Draw(pDD, fMasterAlpha * m_fLayerAlpha);
		}

		if (m_pVJSourceOut) {
			m_pVJSourceOut->Render(pDD);
			m_pVJSourceOut->Draw(pDD, fMasterAlpha * m_fLayerAlpha);
		}
	}
}


//---------------------------------------------------------------------------//
//	Attach
//---------------------------------------------------------------------------//
auto CVJLayer::Attach(float fTime, CVJSource* pVJSource) -> void {
	if (m_pVJSource) {
		Unattach(fTime);
	}

	m_pVJSource = pVJSource;

	if (m_pVJSource) {
		m_pVJSource->Attach(fTime);
		m_pVJSource->FadeIn(fTime);
	}
}


//---------------------------------------------------------------------------//
//	Unattach
//---------------------------------------------------------------------------//
auto CVJLayer::Unattach(float fTime) -> void {
	if (m_pVJSource) {
		m_pVJSourceOut = m_pVJSource;
		m_pVJSourceOut->FadeOut(fTime);
		m_pVJSource = nullptr;
	}
}


//---------------------------------------------------------------------------//
//	MoveTo
//---------------------------------------------------------------------------//
auto CVJLayer::MoveTo(float fTime, CVJLayer* pVJLayer) -> void {
	ASSERT(pVJLayer);
	if (pVJLayer) {
		pVJLayer->Unattach(fTime);
		pVJLayer->m_pVJSource = m_pVJSource;
		m_pVJSource = nullptr;
	}
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
auto CVJLayer::Clear(bool bActive, bool bOut) -> void {
	if (bActive) {
		if (m_pVJSource) {
			m_pVJSource->Unattach();
		}
		m_pVJSource = nullptr;
	}

	if (bOut) {
		if (m_pVJSourceOut) {
			m_pVJSourceOut->Unattach();
		}
		m_pVJSourceOut = nullptr;
	}
}
