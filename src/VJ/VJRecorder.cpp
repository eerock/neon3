//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "VJRecorder.h"
#include "VJController.h"
#include <string.h>

#define WRITE_EVENT(ev) { \
	m_tEvent.iEvent = ev; \
	m_tEvent.fTime  = m_pVJController->GetTime(); \
	fwrite(&m_tEvent, 1, sizeof(m_tEvent), m_pFile); \
}

#define WRITE_DATA(d, s) { \
	fwrite(d, 1, s, m_pFile); \
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJRecorder::CVJRecorder()
	: m_bOk(false)
	, m_pFile(nullptr)
	, m_pVJController(nullptr)
	, m_tEvent()
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CVJRecorder::Init(CVJController* pVJController) -> bool {
	m_pVJController = pVJController;
	m_pFile = nullptr;
	m_bOk = true;
	return m_bOk;
}

//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJRecorder::~CVJRecorder() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJRecorder\n"));
#endif
	StopRecording();
}


//---------------------------------------------------------------------------//
//	StartRecording
//---------------------------------------------------------------------------//
auto CVJRecorder::StartRecording(char const* pszFile) -> bool {
	m_pVJController->Reset();

#if (PLATFORM == PLATFORM_WINDOWS)
	auto err = fopen_s(&m_pFile, pszFile, "wt+");
	return err == 0;
#else
	m_pFile = fopen(pszFile, "wb+");
	return (m_pFile != nullptr);
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	StopRecording
//---------------------------------------------------------------------------//
auto CVJRecorder::StopRecording() -> void {
	if (m_pFile) {
		fclose(m_pFile);
		m_pFile = nullptr;
	}
}


//---------------------------------------------------------------------------//
//	RecordFFT
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordFFT(float const* pFFT) -> void {
	uchar pcFFT[FREQ_CHANNELS];
	for (int i = 0; i < FREQ_CHANNELS; ++i) {
		pcFFT[i] = (char)(pFFT[i] * 255.f);
	}
	WRITE_EVENT(EV_FFT);
	WRITE_DATA(pcFFT, FREQ_CHANNELS * sizeof(pcFFT[0]));
}


//---------------------------------------------------------------------------//
//	RecordLoadSource
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordLoadSource(char const* pszFile) -> void {
	WRITE_EVENT(EV_LOAD_SOURCE);
	WRITE_DATA(pszFile, strlen(pszFile) + 1);
}


//---------------------------------------------------------------------------//
//	RecordUnloadSource
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordUnloadSource(int iIDSource) -> void {
	WRITE_EVENT(EV_UNLOAD_SOURCE);
	WRITE_DATA(&iIDSource, sizeof(iIDSource));
}


//----------------------------------------------------------------------------
//	ReloadReloadSource
//----------------------------------------------------------------------------
auto CVJRecorder::RecordReloadSource(char const* pszFile, int iIDSource) -> void {
	WRITE_EVENT(EV_RELOAD_SOURCE);
	WRITE_DATA(pszFile, strlen(pszFile) + 1);
	WRITE_DATA(&iIDSource, sizeof(iIDSource));
}


//---------------------------------------------------------------------------//
//	RecordAttachSource
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordAttachSource(int iID, char cLayer) -> void {
	WRITE_EVENT(EV_ATTACH_SOURCE);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&cLayer, sizeof(cLayer));
}


//---------------------------------------------------------------------------//
//	RecordUnattachLayer
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordUnattachLayer(char cLayer) -> void {
	WRITE_EVENT(EV_UNATTACH_LAYER);
	WRITE_DATA(&cLayer, sizeof(cLayer));
}


//---------------------------------------------------------------------------//
//	RecordSourceReset
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSourceReset(int iID) -> void {
	WRITE_EVENT(EV_RESET_SOURCE);
	WRITE_DATA(&iID, sizeof(iID));
}


//---------------------------------------------------------------------------//
//	RecordSourceCommand
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSourceCommand(int iID, char const* pszCommand) -> void {
	WRITE_EVENT(EV_COMMAND_SOURCE);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(pszCommand, strlen(pszCommand) + 1);
}


//---------------------------------------------------------------------------//
//	RecordEffectCommand
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordEffectCommand(int iID, char const* pszCommand) -> void {
	WRITE_EVENT(EV_EFFECT_COMMAND);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(pszCommand, strlen(pszCommand) + 1);
}


//---------------------------------------------------------------------------//
//	RecordSetSourceTime
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetSourceTime(int iID, float fTime) -> void {
	WRITE_EVENT(EV_SET_SOURCE_TIME);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&fTime, sizeof(fTime));
}


//---------------------------------------------------------------------------//
//	RecordSetSourceSpeed
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetSourceSpeed(int iID, float fSpeed) -> void {
	WRITE_EVENT(EV_SET_SOURCE_SPEED);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&fSpeed, sizeof(fSpeed));
}


//---------------------------------------------------------------------------//
//	RecordSetSourceAlpha
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetSourceAlpha(int iID, float fAlpha) -> void {
	WRITE_EVENT(EV_SET_SOURCE_ALPHA);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&fAlpha, sizeof(fAlpha));
}


//---------------------------------------------------------------------------//
//	RecordSetSourceBlend
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetSourceBlend(int iID, int iBlend) -> void {
	WRITE_EVENT(EV_SET_SOURCE_BLEND);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&iBlend, sizeof(iBlend));
}


//---------------------------------------------------------------------------//
//	RecordSetEffect
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetEffect(int iID, char const* pszEffect) -> void {
	WRITE_EVENT(EV_SET_EFFECT);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(pszEffect, strlen(pszEffect) + 1);
}


//---------------------------------------------------------------------------//
//	RecordSetEffectVar
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetEffectVar(int iID, char cVar, void* pValue) -> void {
	WRITE_EVENT(EV_SET_EFFECT_VAR);
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&cVar, sizeof(cVar));
	WRITE_DATA(pValue, sizeof(pValue));
}


//---------------------------------------------------------------------------//
//	RecordSetSourceVar
//---------------------------------------------------------------------------//
auto CVJRecorder::RecordSetSourceVar(char cScope, char cObj, int iID, char cVar, void* pValue) -> void {
	if (!pValue) {
		return;
	}
	WRITE_EVENT(EV_SET_SOURCE_VAR);
	WRITE_DATA(&cScope, sizeof(cScope));
	WRITE_DATA(&cObj, sizeof(cObj));
	WRITE_DATA(&iID, sizeof(iID));
	WRITE_DATA(&cVar, sizeof(cVar));
	WRITE_DATA(pValue, sizeof(pValue));
}
