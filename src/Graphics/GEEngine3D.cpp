﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEEngine3D.h"

using namespace CEngine3D;

struct TRegister {
	int iPos;
	int iSize;
	std::string sName;
};

static TRegister s_pRegisterData[V_MAX_REGISTERS];
static float* s_pRegisters;

//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CEngine3D::Init() -> void {
	int iArrayPos = 0;
	int i = 0;

#define ITEM(e, str, varsize)	\
	s_pRegisterData[i].iPos = iArrayPos; \
	s_pRegisterData[i].iSize = varsize; \
	s_pRegisterData[i].sName = str; \
	iArrayPos += varsize; \
	i++;
#include "Enum3DEngineVars.h"
#undef ITEM
	
	s_pRegisters = NEW_ARRAY(float, iArrayPos);
	memset(s_pRegisters, 0, iArrayPos * sizeof(float));
}


//---------------------------------------------------------------------------//
//	Release
//---------------------------------------------------------------------------//
auto CEngine3D::Release() -> void {
	DELETE_ARRAY(s_pRegisters);
}


//---------------------------------------------------------------------------//
//	SetRegisterData
//---------------------------------------------------------------------------//
auto CEngine3D::SetRegisterData(int iRegister, float const* pValue) -> void {
	if (ValidIndex(iRegister, (int)V_MAX_REGISTERS)) {
		memcpy(&s_pRegisters[s_pRegisterData[iRegister].iPos], pValue, s_pRegisterData[iRegister].iSize * sizeof(float));
	}
}


//---------------------------------------------------------------------------//
//	GetRegisterData
//---------------------------------------------------------------------------//
auto CEngine3D::GetRegisterData(int iRegister, float* pValue) -> void {
	if (ValidIndex(iRegister, (int)V_MAX_REGISTERS)) {
		memcpy(pValue, &s_pRegisters[s_pRegisterData[iRegister].iPos], s_pRegisterData[iRegister].iSize * sizeof(float));
	}
}


//---------------------------------------------------------------------------//
//	GetRegisterPtr
//---------------------------------------------------------------------------//
auto CEngine3D::GetRegisterPtr(int iRegister) -> float* {
	if (ValidIndex(iRegister, (int)V_MAX_REGISTERS)) {
		return (&s_pRegisters[s_pRegisterData[iRegister].iPos]);
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	GetRegister
//---------------------------------------------------------------------------//
auto CEngine3D::GetRegisterByName(std::string const& sRegister) -> int {
	for (int i = 0; i < V_MAX_REGISTERS; i++) {
		if (Stricmp(sRegister, s_pRegisterData[i].sName) == 0) {
			return i;
		}
	}
	return -1;
}


//---------------------------------------------------------------------------//
//	GetRegister
//---------------------------------------------------------------------------//
auto CEngine3D::GetRegisterName(int iRegister, std::string& sRegister) -> bool {
	if (ValidIndex(iRegister, (int)V_MAX_REGISTERS)) {
		sRegister = s_pRegisterData[iRegister].sName;
		return true;
	}
	return false;
}
