//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Path.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CPath::CPath()
	: m_bOk(false)
	, m_iType(0)
	, m_iMaxKeys(0)
	, m_iNumKeys(0)
	, m_fTime(0.f)
	, m_fTimeIni(0.f)
	, m_fTimeEnd(0.f)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CPath::Init(int iType, int iNumKeys) -> bool {
	m_iNumKeys = 0;
	m_fTime = 0.f;
	m_fTimeIni = 999999.f;
	m_fTimeEnd =-999999.f;
	m_iMaxKeys = iNumKeys;
	m_iType = iType;
	m_bOk = true;

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CPath::~CPath() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CPath\n"));
#endif
	if (IsOk()) {
		m_bOk = false;
	}
}
