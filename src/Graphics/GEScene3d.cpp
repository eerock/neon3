//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"
#include "GEScene3D.h"
#include "GEObject3D.h"
#include "GECamera3D.h"
#include "GEEngine3D.h"
#include "GELight3D.h"
#include "GESceneLoader.h"
#include "CtrlVar.h"


static TCtrlVar s_VarsFog[] = {
	{ TCtrlVar::CHECK_BOX, 0, "Enable Fog", true, 0, { NULL } },
	{ TCtrlVar::SLIDER,    1, "Red",        true, 0, { NULL } },
	{ TCtrlVar::SLIDER,    2, "Green",      true, 0, { NULL } },
	{ TCtrlVar::SLIDER,    3, "Blue",       true, 0, { NULL } },
	{ TCtrlVar::SLIDER,    4, "Near",       true, 0, { NULL } },
	{ TCtrlVar::SLIDER,    5, "Far",        true, 0, { NULL } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CGEScene3D::CGEScene3D()
	: m_bOk(false)
	, m_fTime(0.f)
	, m_uActiveCamera(0)
	, m_fFirstFrame(0.f)
	, m_fLastFrame(0.f)
	, m_fFrameSpeed(0.f)
	, m_fTicksPerFrame(0.f)
	, m_fFPS(0.f)
	, m_fAmbientR(0.f)
	, m_fAmbientG(0.f)
	, m_fAmbientB(0.f)
	, m_uNumMaterials(0)
	, m_uNumObjects(0)
	, m_uNumCameras(0)
	, m_uNumLights(0)
	, m_pMaterials(nullptr)
	, m_pObjects(nullptr)
	, m_pCameras(nullptr)
	, m_pLights(nullptr)
	, m_ppSortObjects(nullptr)
	, m_vsObjectNames()
	, m_vsLightNames()
	, m_vsCameraNames()
	, m_bFog(false)
	, m_fFogColorR(0.f)
	, m_fFogColorG(0.f)
	, m_fFogColorB(0.f)
	, m_fFogNear(0.f)
	, m_fFogFar(0.f)
	, m_fFogDensity(0.f)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CGEScene3D::Init(std::string const& sFile) -> bool {
	ResetVars();

	CFile File;
	if (!File.Open(sFile.c_str(), "rb")) {
		return false;
	}

	// Leer todo el fichero en memoria
	uint uLength = static_cast<uint>(File.GetSize());
	char* pData = NEW_ARRAY(char, uLength);

	File.Read(pData, uLength);
	File.Close();

	// Load Scene
	if (CSceneLoader::LoadSelector(this, pData, uLength)) {
		Reset(0.f);
		m_bOk = true;
	} else {
		GLOG(("ERROR: Can't load 3D file %s\n", sFile.c_str()));
		ReleaseVars();
	}

	DELETE_ARRAY(pData);

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CGEScene3D::~CGEScene3D() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CGEScene3D\n"));
#endif
	if (IsOk()) {
		ReleaseVars();
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	ResetVars
//	es-en: Borra = delete, but this is more like a 'reset' or 'clear'
//---------------------------------------------------------------------------//
auto CGEScene3D::ResetVars() -> void {
	ASSERT(m_bOk == false);
	m_fTime = 0.f;
	m_uActiveCamera = 0;
	m_fFirstFrame = 0.f;
	m_fLastFrame = 0.f;
	m_fFrameSpeed = 0.f;
	m_fTicksPerFrame = 0.f;
	m_fFPS = 0.f;
	m_fAmbientR = 0.f;
	m_fAmbientG = 0.f;
	m_fAmbientB = 0.f;
	m_uNumMaterials = 0;
	m_uNumObjects = 0;
	m_uNumCameras = 0;
	m_uNumLights = 0;
	m_pMaterials = nullptr;
	m_pObjects = nullptr;
	m_pCameras = nullptr;
	m_pLights = nullptr;
	m_ppSortObjects = nullptr;
	//
	m_bFog = false;
	m_fFogColorR = 0.f;
	m_fFogColorG = 0.f;
	m_fFogColorB = 0.f;
	m_fFogNear = 0.25f;
	m_fFogFar = 0.5f;
	m_fFogDensity = 0.25f;
}


//---------------------------------------------------------------------------//
//	ReleaseVars
//---------------------------------------------------------------------------//
auto CGEScene3D::ReleaseVars() -> void {
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	for (int i = 0; i < m_uNumMaterials; ++i) {
		pMatMgr->RemoveMaterial(m_pMaterials[i]);
	}

	DELETE_ARRAY(m_pMaterials);
	DELETE_ARRAY(m_pObjects);
	DELETE_ARRAY(m_pCameras);
	DELETE_ARRAY(m_pLights);
	DELETE_ARRAY(m_ppSortObjects);
}


//---------------------------------------------------------------------------//
//	Reset
//---------------------------------------------------------------------------//
auto CGEScene3D::Reset(float fTime) -> void {
	SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CGEScene3D::SetTime(float fTime) -> void {
	m_fTime = fTime;
	// Aplicar a los objetos
	for (int i = 0; i < m_uNumObjects; i++) {
		m_pObjects[i].SetTime(m_fTime * m_fFPS);
	}
	// Aplicar a las camaras
	//for (i = 0; i < m_uNumCameras; i++)
	m_pCameras[m_uActiveCamera].SetTime(m_fTime * m_fFPS);
}


//---------------------------------------------------------------------------//
//	GetCamera
//---------------------------------------------------------------------------//
auto CGEScene3D::GetCamera(uint iCam) -> CCamera3D* {
	ASSERT(iCam < m_uNumCameras);
	if (iCam < m_uNumCameras) {
		return &m_pCameras[iCam];
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CGEScene3D::Draw(CDisplayDevice* pDD) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();

	// RenderStates especificos
	pDD->ApplyBasicShader();

	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (DWORD)(255 << 24));

	PrepareCamera(pDD);
	PrepareLights(pDD);
	PrepareFog(pDD);
	PrepareObjects(pDD);
	DrawObjects(pDD);

	pD3D->SetRenderState(D3DRS_LIGHTING, FALSE);
#else
	UNREFERENCED_PARAMETER(pDD);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	GetVarObjects
//---------------------------------------------------------------------------//
auto CGEScene3D::GetVarObjects(std::vector<std::string> &vObjects, int iScope) const -> void {
	vObjects.clear();
	switch (iScope) {
	case FXS_3DOBJECT: {
		vObjects = m_vsObjectNames;
		break;
	}
	case FXS_3DLIGHT: {
		vObjects = m_vsLightNames;
		break;
	}
	case FXS_3DCAMERA: {
		vObjects = m_vsCameraNames;
		break;
	}
	case FXS_3DFOG: {
		vObjects.push_back("Fog");
		break;
	}
	default: {
		break;
	}
	}
}


//---------------------------------------------------------------------------//
//	GetVars
//---------------------------------------------------------------------------//
auto CGEScene3D::GetVars(int iScope) const -> TCtrlVar* {
	switch (iScope) {
	case FXS_3DOBJECT: {
		return CObject3D::GetVars();
	}
	case FXS_3DLIGHT: {
		return CLight3D::GetVars();
	}
	case FXS_3DCAMERA: {
		return CCamera3D::GetVars();
	}
	case FXS_3DFOG: {
		return s_VarsFog;
	}
	default: {
		break;
	}
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CGEScene3D::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	switch (iScope) {
		case FXS_3DOBJECT: {
			m_pObjects[iObj].SetVar(iVar, pData);
			break;
		}
		case FXS_3DLIGHT: {
			m_pLights[iObj].SetVar(iVar, pData);
			break;
		}
		case FXS_3DCAMERA: {
			m_pCameras[iObj].SetVar(iVar, pData);
			break;
		}
		// Niebla
		case FXS_3DFOG: {
			switch (iVar) {
				case 0: {
					m_bFog = *(static_cast<bool const*>(pData));
					break;
				}
				case 1: {
					m_fFogColorR = *(static_cast<float const*>(pData));
					break;
				}
				case 2: {
					m_fFogColorG = *(static_cast<float const*>(pData));
					break;
				}
				case 3: {
					m_fFogColorB = *(static_cast<float const*>(pData));
					break;
				}
				case 4: {
					m_fFogNear = *(static_cast<float const*>(pData));
					break;
				}
				case 5: {
					m_fFogFar = *(static_cast<float const*>(pData));
					break;
				}
				default: {
					break;
				}
			}
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CGEScene3D::GetVar(int iScope, int iObj, int iVar) -> void const* {
	switch (iScope) {
		case FXS_3DOBJECT: {
			return m_pObjects[iObj].GetVar(iVar);
		}
		case FXS_3DLIGHT: {
			return m_pLights[iObj].GetVar(iVar);
		}
		case FXS_3DCAMERA: {
			return m_pCameras[iObj].GetVar(iVar);
		}
			// Niebla
		case FXS_3DFOG: {
			switch (iVar) {
				case 0: {
					return &m_bFog;
				}
				case 1: {
					return &m_fFogColorR;
				}
				case 2: {
					return &m_fFogColorG;
				}
				case 3: {
					return &m_fFogColorB;
				}
				case 4: {
					return &m_fFogNear;
				}
				case 5: {
					return &m_fFogFar;
				}
				default: {
					break;
				}
			}
			break;
		}
		default: {
			break;
		}
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	PrepareCamera
//---------------------------------------------------------------------------//
auto CGEScene3D::PrepareCamera(CDisplayDevice* pDD) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();

	auto pCam = GetCamera(m_uActiveCamera);
	D3DXVECTOR3 vFr(pCam->GetSrc()->x, pCam->GetSrc()->y, pCam->GetSrc()->z);
	D3DXVECTOR3 vAt(pCam->GetTgt()->x, pCam->GetTgt()->y, pCam->GetTgt()->z);
	D3DXVECTOR3 vUp(pCam->GetUp ()->x, pCam->GetUp ()->y, pCam->GetUp ()->z);
	D3DXMatrixLookAtLH(&m_MatrixView, &vFr, &vAt, &vUp);
	pD3D->SetTransform(D3DTS_VIEW, &m_MatrixView);
	D3DXMatrixPerspectiveFovLH(&m_MatrixProj, pCam->GetFov(), 4.f / 3.f, 1.f, 2000.f);
	pD3D->SetTransform(D3DTS_PROJECTION, &m_MatrixProj);

	m_MatrixVP = m_MatrixView * m_MatrixProj;
	CEngine3D::SetRegisterData(CEngine3D::V_VIEW, (float*)&m_MatrixView);
	CEngine3D::SetRegisterData(CEngine3D::V_PROJECTION, (float*)&m_MatrixProj);
	CEngine3D::SetRegisterData(CEngine3D::V_VIEWPROJECTION, (float*)&m_MatrixVP);
	CEngine3D::SetRegisterData(CEngine3D::V_CAMERAPOSITION, (float*)pCam->GetSrc());
#else
	UNREFERENCED_PARAMETER(pDD);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	PrepareLights
//---------------------------------------------------------------------------//
auto CGEScene3D::PrepareLights(CDisplayDevice* pDD) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();

	// Si tiene lucas las activamos, sino desactivamos la iluminacion
	if (m_uNumLights > 0) {
		pD3D->SetRenderState(D3DRS_LIGHTING, TRUE);
		for (int i = 0; i < m_uNumLights; i++) {
			pD3D->SetLight(i, m_pLights[i].GetD3DLight());
			pD3D->LightEnable(i, true);
		}
	} else {
		pD3D->SetRenderState(D3DRS_LIGHTING, FALSE);
	}

	// Variables
	TVector3 vLight;
	if (m_uNumLights > 0) {
		vLight.x = m_pLights[0].GetD3DLight()->Position.x;
		vLight.y = m_pLights[0].GetD3DLight()->Position.y;
		vLight.z = m_pLights[0].GetD3DLight()->Position.z;
	} else {
		vLight = *(GetCamera(m_uActiveCamera)->GetSrc());
		vLight.x = -vLight.x;
		vLight.y = -vLight.y;
		vLight.z = -vLight.z;
	} 
	Vector_Unit(&vLight, &vLight);
	CEngine3D::SetRegisterData(CEngine3D::V_DIRECTION, (float*)&vLight);
#else
	UNREFERENCED_PARAMETER(pDD);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	PrepareFog
//---------------------------------------------------------------------------//
auto CGEScene3D::PrepareFog(CDisplayDevice* pDD) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	if (m_bFog) {
		pD3D->SetRenderState(D3DRS_FOGENABLE, TRUE);
		pD3D->SetRenderState(D3DRS_RANGEFOGENABLE, TRUE);
		pD3D->SetRenderState(D3DRS_FOGCOLOR, HARD_COLOR_ARGB(255, (char)(m_fFogColorR * 255.0), (char)(m_fFogColorG * 255.0), (char)(m_fFogColorB * 255.0)));
		float fStart = m_fFogNear * 2000.f;
		float fEnd = m_fFogFar * 2000.f;
		pD3D->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_LINEAR);
		pD3D->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&fStart));
		pD3D->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&fEnd));
		//fStart = m_fFogNear * (1.f + m_fFogFar * (5.f - 1.f));
		//pD3D->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_EXP2);
		//pD3D->SetRenderState(D3DRS_FOGDENSITY, *(DWORD*)(&fStart));
	} else {
		pD3D->SetRenderState(D3DRS_FOGENABLE, FALSE);
	}
#else
	UNREFERENCED_PARAMETER(pDD);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	PrepareObjects
//	Los ordena por Z con un qsort
//	ebp-> Z-sort scene objects...
//---------------------------------------------------------------------------//
auto CompareZValue(void const* p1, void const* p2) -> int {
	CObject3D const* pObj1 = *((CObject3D const**)p1);
	CObject3D const* pObj2 = *((CObject3D const**)p2);
	return (pObj1->GetZ() < pObj2->GetZ()) ? 1 : -1;
}

auto CGEScene3D::PrepareObjects(CDisplayDevice* pDD) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	UNREFERENCED_PARAMETER(pDD);
	for (int i = 0; i < m_uNumObjects; ++i) {
		m_pObjects[i].CalculateUV(m_MatrixView);
		m_pObjects[i].CalculateZ(m_MatrixVP);
		m_ppSortObjects[i] = &m_pObjects[i];
	}
	qsort(m_ppSortObjects, m_uNumObjects, sizeof(CObject3D*), CompareZValue);
#else
	UNREFERENCED_PARAMETER(pDD);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DrawObjects
//---------------------------------------------------------------------------//
auto CGEScene3D::DrawObjects(CDisplayDevice* pDD) -> void {
	for (uint iObj = 0; iObj < m_uNumObjects; ++iObj) {
		m_ppSortObjects[iObj]->Draw(pDD);
	}
}


//---------------------------------------------------------------------------//
//	SetMaterials
//---------------------------------------------------------------------------//
auto CGEScene3D::SetMaterials(uint* pMaterials, uint uCount) -> void {
	m_pMaterials = pMaterials;
	m_uNumMaterials = static_cast<ushort>(uCount);
}


//---------------------------------------------------------------------------//
//	SetObjects
//---------------------------------------------------------------------------//
auto CGEScene3D::SetObjects(CObject3D* pObjects, uint uCant) -> void {
	m_pObjects = pObjects;
	m_uNumObjects = static_cast<ushort>(uCant);
	for (uint i = 0; i < m_uNumObjects; ++i) {
		m_vsObjectNames.push_back(m_pObjects[i].GetName());
	}
	// Para la ordenacion
	// es-en: For Ordination
	DELETE_ARRAY(m_ppSortObjects);
	m_ppSortObjects = NEW_ARRAY(CObject3D*, m_uNumObjects);
}


//---------------------------------------------------------------------------//
//	SetCameras
//---------------------------------------------------------------------------//
auto CGEScene3D::SetCameras(CCamera3D* pCameras, uint uCant) -> void {
	m_pCameras = pCameras;
	m_uNumCameras = static_cast<ushort>(uCant);
	for (uint i = 0; i < m_uNumCameras; ++i) {
		m_vsCameraNames.push_back(m_pCameras[i].GetName());
	}
}


//---------------------------------------------------------------------------//
//	SetLights
//---------------------------------------------------------------------------//
auto CGEScene3D::SetLights(CLight3D* pLights, uint uCant) -> void {
	m_pLights = pLights;
	m_uNumLights = static_cast<ushort>(uCant);
	for (uint i = 0; i < m_uNumLights; ++i) {
		m_vsLightNames.push_back(m_pLights[i].GetName());
	}
}
