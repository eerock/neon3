//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEFont.h"
#include "GEGraphics.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFont::CFont()
	: m_bOk(false)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	, m_pFont(nullptr)
#endif
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFont::Init(std::string const& sFontName, int iHeight, bool bBold, bool bItalic) -> bool {
	m_bOk = false;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DXFONT_DESCA LogFont = {
		iHeight,					// height
		0,							// width
		0,							// lfEscapement
		0,							// lfOrientation
		bBold ? FW_BOLD : FALSE,	// lfWeight
		bItalic ? TRUE : FALSE,		// lfItalic
		FALSE,						// lfUnderline
		FALSE,						// lfStrikeOut
		DEFAULT_CHARSET,			// lfCharSet
		OUT_DEFAULT_PRECIS,			// lfOutPrecision
		CLIP_DEFAULT_PRECIS,		// lfClipPrecision
		ANTIALIASED_QUALITY,		// lfQuality
		DEFAULT_PITCH,				// lfPitchAndFamily
		//pszFontName				// lfFaceName[LF_FACESIZE]
	};

	strcpy_s(LogFont.FaceName, LF_FACESIZE, sFontName.c_str());

	if(FAILED(D3DXCreateFontIndirect(g_DisplayDevice.GetD3DDevice(), &LogFont, &m_pFont))) {
		GLOG(("ERROR: Can't create font %s\n", sFontName.c_str()));
	} else {
		m_bOk = true;
	}
#else
	UNREFERENCED_PARAMETER(sFontName);
	UNREFERENCED_PARAMETER(iHeight);
	UNREFERENCED_PARAMETER(bBold);
	UNREFERENCED_PARAMETER(bItalic);
#endif//GFX_ENGINE

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFont::~CFont() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFont\n"));
#endif
	if (IsOk()) {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		SafeRelease(&m_pFont);
#endif
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Print
//---------------------------------------------------------------------------//
auto CFont::Print(CDisplayDevice* pDD, std::string const& sText, TVectorI2 const& vPos, uint uColor, int iAlign) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	RECT rect;
	rect.left = vPos.x;
	rect.top = vPos.y;
	rect.right = pDD->Width();
	rect.bottom = pDD->Height();
	m_pFont->DrawText(NULL, sText.c_str(), -1, &rect, iAlign, uColor);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(sText);
	UNREFERENCED_PARAMETER(vPos);
	UNREFERENCED_PARAMETER(uColor);
	UNREFERENCED_PARAMETER(iAlign);
#endif//GFX_ENGINE
}
