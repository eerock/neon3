//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEDISPLAYDEVICE_H
#define NEON_GEDISPLAYDEVICE_H

#include "Base.h"

#define GFX_ENGINE_DIRECT3D	(1)
#define GFX_ENGINE_OPENGL	(2)
#define GFX_ENGINE			(GFX_ENGINE_DIRECT3D)
//#define GFX_ENGINE			(GFX_ENGINE_OPENGL)


#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
// ebp-> Direct3D 9
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#else
#include <gl/GL.h>
#include <gl/GLU.h>
#endif//GFX_ENGINE


#include "GECustomVertex.h"
#include "GETypes.h"
#include "GEVector.h"

//---------------------------------------------------------------------------//
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
// ebp-> Direct3D 9
typedef IDirect3D9				D3D;
typedef IDirect3DDevice9		D3DDEVICE;
typedef IDirect3DVertexShader9	D3DVS;
typedef IDirect3DPixelShader9	D3DPS;
typedef IDirect3DTexture9		D3DTEXTURE;
typedef IDirect3DSurface9		D3DSURFACE;
typedef IDirect3DStateBlock9	D3DSTATEBLOCK;
#endif//GFX_ENGINE_DIRECT3D

//---------------------------------------------------------------------------//
template <class T>
inline auto SafeRelease(T** ppT) -> void {
	if (*ppT) {
		(*ppT)->Release();
		(*ppT) = nullptr;
	}
}

//---------------------------------------------------------------------------//
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
#define	HARD_COLOR_ARGB(a, r, g, b)			D3DCOLOR_ARGB((a), (r), (g), (b))
#define	HARD_COLOR_XRGB(r, g, b)			D3DCOLOR_XRGB((r), (g), (b))
#define	HARD_COLOR_GET_A(color)				(((color) >> 24) & 0xFF)
#define	HARD_COLOR_GET_R(color)				(((color) >> 16) & 0xFF)
#define	HARD_COLOR_GET_G(color)				(((color) >> 8) & 0xFF)
#define	HARD_COLOR_GET_B(color)				((color) & 0xFF)
#define	HARD_COLOR_SET_A(color, v)			(color = ((color) & 0x00FFFFFF) | ((v) << 24))
#define	HARD_COLOR_SET_R(color, v)			(color = ((color) & 0xFF00FFFF) | ((v) << 16))
#define	HARD_COLOR_SET_G(color, v)			(color = ((color) & 0xFFFF00FF) | ((v) << 8))
#define	HARD_COLOR_SET_B(color, v)			(color = ((color) & 0xFFFFFF00) | (v))
#define	HARD_PALETTE_TO_COLOR(PaletteEntry)	(*(uint*)&PaletteEntry)
#define	HARD_COLOR_A1R5G5B5(a, r, g, b)		((a << 15) | (r << 10) | (g << 5) | (b))

// ebp-> DX10+ (& backwards compatible)
#define D3D_ERROR(String, hRet)		{ \
	std::stringstream ss; \
	ss << String << std::endl; \
	ss << "DXErrorStr = " << DXGetErrorString(hRet) << std::endl; \
	ss << "DXErrorDesc = " << DXGetErrorDescription(hRet); \
	ASSERTM(false, ss.str().c_str()); \
}

#define D3D_ERROR_NO_ASSERT(String, hRet)	{ \
	std::stringstream ss; \
	ss << String << std::endl; \
	ss << "DXErrorStr = " << DXGetErrorString(hRet) << std::endl; \
	ss << "DXErrorDesc = " << DXGetErrorDescription(hRet); \
	GLOG(("%s\n", ss.str().c_str())); \
}

#else

#define	HARD_COLOR_ARGB(a, r, g, b)			((((a) & 0xFF) << 24) | (((r) & 0xFF) << 16) | (((g) & 0xFF) << 8) | ((b) & 0xFF))
#define	HARD_COLOR_XRGB(r, g, b)			((((r) & 0xFF) << 16) | (((g) & 0xFF) << 8) | ((b) & 0xFF))
#define	HARD_COLOR_GET_A(color)				(((color) >> 24) & 0xFF)
#define	HARD_COLOR_GET_R(color)				(((color) >> 16) & 0xFF)
#define	HARD_COLOR_GET_G(color)				(((color) >> 8) & 0xFF)
#define	HARD_COLOR_GET_B(color)				((color) & 0xFF)
#define	HARD_COLOR_SET_A(color, v)			(color = ((color) & 0x00FFFFFF) | ((v) << 24))
#define	HARD_COLOR_SET_R(color, v)			(color = ((color) & 0xFF00FFFF) | ((v) << 16))
#define	HARD_COLOR_SET_G(color, v)			(color = ((color) & 0xFFFF00FF) | ((v) << 8))
#define	HARD_COLOR_SET_B(color, v)			(color = ((color) & 0xFFFFFF00) | (v))

#endif//GFX_ENGINE

// ebp-> todo: name these enums, for clarity and debuggability.

// Tipos de primitivas hardware para el renderizado
// es-en: Types of hardware primitives for rendering
enum EPrimType {									// usage frequency
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	HARD_PRIM_POINTLIST = D3DPT_POINTLIST,			// *
	HARD_PRIM_LINELIST = D3DPT_LINELIST,
	HARD_PRIM_LINESTRIP = D3DPT_LINESTRIP,			// *
	HARD_PRIM_TRIANGLELIST = D3DPT_TRIANGLELIST,	// ***
	HARD_PRIM_TRIANGLESTRIP = D3DPT_TRIANGLESTRIP,
	HARD_PRIM_TRIANGLEFAN = D3DPT_TRIANGLEFAN,		// **
#else
	HARD_PRIM_POINTLIST = GL_POINTS,
	HARD_PRIM_LINELIST = GL_LINES,
	HARD_PRIM_LINESTRIP = GL_LINE_STRIP,
	HARD_PRIM_TRIANGLELIST = GL_TRIANGLES,
	HARD_PRIM_TRIANGLESTRIP = GL_TRIANGLE_STRIP,
	HARD_PRIM_TRIANGLEFAN = GL_TRIANGLE_FAN,
#endif//GFX_ENGINE
};

// Tipos de formato para una texura hardware
// es-en: Format types for hardware textures.
enum ETextureFormat {								// usage frequency
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	HARD_TEX_FORMAT_P8 = D3DFMT_P8,
	HARD_TEX_FORMAT_A8 = D3DFMT_A8,					// ***
	HARD_TEX_FORMAT_A8L8 = D3DFMT_A8L8,
	HARD_TEX_FORMAT_A4R4G4B4 = D3DFMT_A4R4G4B4,
	HARD_TEX_FORMAT_R5G6B5 = D3DFMT_R5G6B5,
	HARD_TEX_FORMAT_A1R5G5B5 = D3DFMT_A1R5G5B5,		// *
	HARD_TEX_FORMAT_A8R8G8B8 = D3DFMT_A8R8G8B8,		// ***
	HARD_TEX_FORMAT_R8G8B8 = D3DFMT_R8G8B8,			// *
	//HARD_TEX_FORMAT_D16 = D3DFMT_D16,
	//HARD_TEX_FORMAT_D24S8 = D3DFMT_D24S8,
	HARD_MAX_TEXTURE_FORMATS = 8,
#else
	HARD_MAX_TEXTURE_FORMATS = 0,
#endif//GFX_ENGINE
};

// Flags para las texturas hardware
// es-en: Flags for hardware textures.
// ebp-> unused
enum {
	HARD_TEX_FLAG_RENDERTARGET = 1 << 0,
	HARD_TEX_FLAG_DEPTHSTENCIL = 1 << 1,
};

enum EPixelShaderVersion {
	PS_NONE,
	PS_1_0,		// deprecated in DirectX 10
	PS_1_3,		// deprecated in DirectX 10
	PS_1_4,		// deprecated in DirectX 10
	PS_2_0,
	PS_3_0,
	PS_4_0,
	PS_4_1,
	PS_5_0,
};


//---------------------------------------------------------------------------//
//	CDisplayDevice
//---------------------------------------------------------------------------//
class CDisplayDevice {
public:
	CDisplayDevice();
	~CDisplayDevice();

	auto Init(TGraphicsMode const& tMode, THWnd hWnd) -> bool;
	auto End() -> void;
	auto IsOk() const -> bool { return m_bOk; }

	auto Clear(bool bClearTarget, bool bClearZBuffer, uint uColor, float fZ) -> void;
	auto Present() -> void;
	auto Reset(TGraphicsMode const& tMode) -> void;
	auto Release() -> void;
	auto Restore() -> void;
	auto ResetVideoMemory() -> void;

	auto PageFlip() -> void;

	// Scene
	auto BeginFrame() -> void;
	auto EndFrame() -> void;
	auto BeginScene() -> void;
	auto EndScene() -> void;
	auto IsDrawingScene() const -> bool { return m_bDrawingScene; }
	auto IsAvailable() const -> bool { return m_bAvailable; }

	// Draws
	auto DrawPrimitive(uint uCustomVertex, int iPrimitive, int iNumPrimitives) -> void;
	auto DrawIndexedPrimitive(uint uCustomVertex, int iPrimitive, int iNumPrimitives) -> void;

	// Vertex/Index buffers
	auto ProcessVertices(uint uCustomVertex, int iNumVertices) -> void;
	auto LockVertexBuffer(uint uCustomVertex, uint uNumVertex) -> void*;
	auto UnlockVertexBuffer(uint uCustomVertex) -> void;
	auto LockIndexBuffer(uint uNumIndex) -> ushort*;
	auto UnlockIndexBuffer() -> void;
	auto CreateVertexBuffers() -> void;
	auto LiberaVertexBuffers() -> void;
	auto CreateIndexBuffers() -> void;
	auto LiberaIndexBuffers() -> void;

	// Miscellaneous Utils
	auto SetViewport(TRect const &Viewport, float fMinZ, float fMaxZ) -> void;
	auto GetViewport() const -> TViewport const& { return m_tViewport; }
	auto GetModoGrafico() const -> TGraphicsMode const& { return m_tMode; }
	auto ViewportWidth() const -> int { return m_tViewport.x1 - m_tViewport.x0; }
	auto ViewportHeight() const -> int { return m_tViewport.y1 - m_tViewport.y0; }
	auto Width() const -> int { return m_iTargetWidth; }
	auto Height() const -> int { return m_iTargetHeight; }
	auto SetRenderTarget(uint uRenderTargetTexID) -> void;
	auto GetRenderTarget() const -> uint { return m_uRenderTargetTexID; }
	auto SetTextureFactor(uint uColor) -> void;
	auto SetBilinearFiltering(int iStage, bool bEnable) -> void;
	auto SetAlphaTest(bool bEnable) -> void;
	auto DeactivateStage(int iStage) -> void;
	auto SetAspectRatio(float fAspectRatio) -> void { m_fAspectRatio = fAspectRatio; }
	auto GetAspectRatio() const -> float { return m_fAspectRatio; }
	auto SetIdentity() -> void;
	auto SetBlendMode(int aBlend, float fAlpha) -> void;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Gets
	auto GetD3D() const -> D3D* { return m_pD3D; }
	auto GetD3DDevice() const -> D3DDEVICE* { return m_pD3DDevice; }
	auto GetBackBufferSurface() const -> D3DSURFACE* { return m_pColorBuffer; }
#endif//GFX_ENGINE_DIRECT3D

	// Shader
	auto ApplyBasicShader() -> void;
	auto GetPixelShaderVersion() const -> int { return m_iPixelShaderVersion; }

private:
	auto ResetVars() -> void;
	auto CreateBasicShader() -> void;

	bool m_bOk;
	THWnd m_hWnd;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3D* m_pD3D;
	D3DDEVICE* m_pD3DDevice;
	D3DDISPLAYMODE m_D3DDisplayMode;
	D3DSURFACE* m_pColorBuffer;
	D3DSURFACE* m_pDepthBuffer;
	D3DSURFACE* m_pDepthSurface;
	D3DSTATEBLOCK* m_pBasicBST;
	D3DXMATRIX m_tMatIdentity;
#endif//GFX_ENGINE_DIRECT3D

	TGraphicsMode m_tMode;
	int m_iPixelShaderVersion;
	bool m_bVertexLock;
	bool m_bDrawingScene;
	bool m_bAvailable;
	TViewport m_tViewport;
	float m_fAspectRatio;
	float m_fMinZViewport;
	float m_fMaxZViewport;
	int m_iTargetWidth;
	int m_iTargetHeight;
	uint m_uRenderTargetTexID;
};

#endif//NEON_GEDISPLAYDEVICE_H
