//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"
#include "GESceneLoader.h"
#include "GEScene3D.h"
#include "GEObject3D.h"
#include "GECamera3D.h"
#include "GELight3D.h"
#include "3DSFormat.h"

//---------------------------------------------------------------------------//
struct T3DSMaterial {
	char pMaterial[80];
};

//---------------------------------------------------------------------------//
struct T3DSObject {
	ushort		uNumFaces;
	ushort		uNumUV;
	ushort		uNumVertices;
	ushort		uNumPosiciones;
	ushort		uNumRotaciones;
	ushort		uNumEscalados;
	TFace*		pFaces;
	TVector2*	pUV;
	uint*		pFrameTimesVertices;
	TVector3*	pVertices;
	uint*		pFrameTimesPosiciones;
	TVector3*	pPosiciones;
	uint*		pFrameTimesRotaciones;
	TVector4*	pRotaciones;
	uint*		pFrameTimesEscalados;
	TVector3*	pEscalados;
};

//---------------------------------------------------------------------------//
struct T3DSCamera {
	uint*		pFrameTimesPosSrc;
	TVector3*	pPosSrc;
	uint*		pFrameTimesPosTgt;
	TVector3*	pPosTgt;
	uint*		pFrameTimesPosUp;
	TVector3*	pPosUp;
	uint*		pFrameTimesFov;
	float*		pFov;
};

//---------------------------------------------------------------------------//
struct T3DSLight {
	uint*		pFrameTimesPosSrc;
	TVector3*	pPosSrc;
	uint*		pFrameTimesPosTgt;
	TVector3*	pPosTgt;
	uint*		pFrameTimesFalloff;
	float*		pFalloff;
};


// ebp-> todo: refactor these globals away.
T3DSMaterial*	s_pMaterials;
CObject3D*		s_pObjects;
T3DSObject*		s_p3DSObjects;
CCamera3D*		s_pCameras;
T3DSCamera*		s_p3DSCameras;
CLight3D*		s_pLights;
T3DSLight*		s_p3DSLights;


//---------------------------------------------------------------------------//
//	STATIC: CountMaterials
//---------------------------------------------------------------------------//
auto C3DSLoader::CountMaterials(CChunker* pChunker) -> int {
	uint uCount = 0;
	pChunker->First();
	do {
		if (pChunker->Enter() == EDIT_MATERIAL) {
			++uCount;
		}
		pChunker->Leave();
	} while (!pChunker->EndOf());
	return uCount;
}


//---------------------------------------------------------------------------//
//	STATIC: CountObjects
//---------------------------------------------------------------------------//
auto C3DSLoader::CountObjects(CChunker* pChunker, int iObjType) -> int {
	uint uCount = 0;
	pChunker->First();
	do {
		if (pChunker->Enter() == EDIT_OBJECT) {
			char pBuffer[80];
			pChunker->ReadStr(pBuffer);
			if (pChunker->Enter() == iObjType) {
				++uCount;
			}
			pChunker->Leave();
		}
		pChunker->Leave();
	} while (!pChunker->EndOf());
	return uCount;
}


//---------------------------------------------------------------------------//
//	GetMaterialID
//---------------------------------------------------------------------------//
static auto GetMaterialID(char const* pszMaterial, uint uNumMateriales) -> int {
	for (uint i = 0; i < uNumMateriales; ++i) {
		if (Stricmp(pszMaterial, s_pMaterials[i].pMaterial) == 0) {
			return i;
		}
	}
	return -1;
};


//---------------------------------------------------------------------------//
//	Load3DSObjects
//---------------------------------------------------------------------------//
auto C3DSLoader::LoadObjects(CGEScene3D* pScene3D, CChunker* pChunker) -> void {
	uint uNumMateriales = CountMaterials(pChunker);

	// Crete array of materials
	uint* auMaterialIDs = nullptr;
	if (uNumMateriales > 0) {
		auMaterialIDs = NEW_ARRAY(uint, uNumMateriales);
		s_pMaterials = NEW_ARRAY(T3DSMaterial, uNumMateriales);
	}

	IMaterialManagerService* pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	int i = 0;
	pChunker->First();
	while (pChunker->Find(EDIT_MATERIAL)) {
		if (pChunker->Find(MAT_NAME)) {
			auMaterialIDs[i] = CMaterialManager::INVALID;
			pChunker->ReadStr(s_pMaterials[i].pMaterial);
			auMaterialIDs[i] = pMatMgr->AddMaterial(s_pMaterials[i].pMaterial);
			if (auMaterialIDs[i] == CMaterialManager::INVALID) {
				// Si no se puede cargar el material, intentaremos ponerle uno flat
				GLOG(("ERROR: Can't load material %s. Setting default flat material\n", s_pMaterials[i].pMaterial));
				/*
				auMaterialIDs[i] = pMatMgr->AddMaterial("flat.nmt");
				// Si aun asi no se puede cargar, 
				if (auMaterialIDs[i] == CMaterialManager::INVALID) {
					GLOG(("ERROR: Can't load standard material flat.nmt\n"));
					return false;
				}
				*/
			}
			i++;
			pChunker->Leave();
		}
		pChunker->Leave();
	};
	pScene3D->SetMaterials(auMaterialIDs, uNumMateriales);

	s_pObjects = nullptr;
	s_p3DSObjects = nullptr;
	s_pCameras = nullptr;
	s_p3DSCameras = nullptr;
	s_pLights = nullptr;
	s_p3DSLights = nullptr;

	// Count
	uint uNumObjects = CountObjects(pChunker, OBJ_TRIMESH);
	uint uNumCameras = CountObjects(pChunker, OBJ_CAMERA);
	uint uNumLights = CountObjects(pChunker, OBJ_LIGHT);

	//-----------------------------------------------------------------------//
	// Load Objects
	if (uNumObjects > 0) {
		s_p3DSObjects = NEW_ARRAY(T3DSObject, uNumObjects);
		s_pObjects = NEW_ARRAY(CObject3D, uNumObjects);

		int i = 0;
		pChunker->First();
		while (pChunker->Find(EDIT_OBJECT)) {
			char pBuffer[80];
			pChunker->ReadStr(pBuffer);
			if (pChunker->Find(OBJ_TRIMESH)) {
				T3DSObject *p3DSObj = &s_p3DSObjects[i];

				// Vertex List
				pChunker->First();
				if (pChunker->Find(TRI_VERTEXL)) {
					pChunker->ReadData(&p3DSObj->uNumVertices, sizeof(ushort));
					p3DSObj->pVertices = NEW_ARRAY(TVector3, p3DSObj->uNumVertices);
					p3DSObj->pFrameTimesVertices = NEW_ARRAY(uint, 1);
					p3DSObj->pFrameTimesVertices[0] = 0;
					for (int j = 0; j < p3DSObj->uNumVertices; ++j) {
						pChunker->ReadData(&p3DSObj->pVertices[j].x, sizeof(float));
						pChunker->ReadData(&p3DSObj->pVertices[j].y, sizeof(float));
						pChunker->ReadData(&p3DSObj->pVertices[j].z, sizeof(float));
					}
					pChunker->Leave();
				}

				// Face List
				pChunker->First();
				if (pChunker->Find(TRI_FACEL)) {
					pChunker->ReadData(&p3DSObj->uNumFaces, sizeof(ushort));
					p3DSObj->pFaces = NEW_ARRAY(TFace, p3DSObj->uNumFaces);
					for (int j = 0; j < p3DSObj->uNumFaces; ++j) {
						pChunker->ReadData(&p3DSObj->pFaces[j].i0, sizeof(ushort));
						pChunker->ReadData(&p3DSObj->pFaces[j].i1, sizeof(ushort));
						pChunker->ReadData(&p3DSObj->pFaces[j].i2, sizeof(ushort));
						p3DSObj->pFaces[j].iMat = 0;
					}
					pChunker->Leave();
				}

				// Face Materials
				pChunker->First();
				if (pChunker->Find(TRI_FACEM)) {
					char pMat[80];
					pChunker->ReadStr(pMat);
					int iMat = GetMaterialID(pMat, uNumMateriales);
					if (iMat >= 0) {
						ushort uFace, uFaces;
						pChunker->ReadData(&uFaces, sizeof(ushort));
						for (uint k = 0; k < uFaces; ++k) {
							pChunker->ReadData(&uFace, sizeof(ushort));
							p3DSObj->pFaces[uFace].iMat = static_cast<ushort>(iMat);
						}
					}
					pChunker->Leave();
				}

				i++;
				pChunker->Leave();
			}
			pChunker->Leave();
		}
	}
	pScene3D->SetObjects(s_pObjects, uNumObjects);

	//-----------------------------------------------------------------------//
	// Load Cameras
	if (uNumCameras > 0) {
		s_p3DSCameras = NEW_ARRAY(T3DSCamera, uNumCameras);
		s_pCameras = NEW_ARRAY(CCamera3D, uNumCameras);

		pChunker->First();
		while (pChunker->Find(EDIT_OBJECT)) {
			char pBuffer[80];
			pChunker->ReadStr(pBuffer);
			if (pChunker->Find(OBJ_CAMERA)) {
			}
			pChunker->Leave();
		}
	}
	pScene3D->SetCameras(s_pCameras, uNumCameras);

	//-----------------------------------------------------------------------//
	// Load Lights
	if (uNumLights > 0) {
		s_p3DSLights = NEW_ARRAY(T3DSLight, uNumLights);
		s_pLights = NEW_ARRAY(CLight3D, uNumLights);

		pChunker->First();
		while (pChunker->Find(EDIT_OBJECT)) {
			char pBuffer[80];
			pChunker->ReadStr(pBuffer);
			if (pChunker->Find(OBJ_LIGHT)) {
			}
			pChunker->Leave();
		}
	}
	pScene3D->SetLights(s_pLights, uNumLights);
}


//---------------------------------------------------------------------------//
//	LoadKeys
//---------------------------------------------------------------------------//
auto C3DSLoader::LoadKeys(CGEScene3D* pEscena3D, CChunker* pChunker) -> void {
	UNREFERENCED_PARAMETER(pEscena3D);
	UNREFERENCED_PARAMETER(pChunker);
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto C3DSLoader::Load(CGEScene3D* pScene3D, char const* pData, uint uDataSize) -> bool {
	UNREFERENCED_PARAMETER(uDataSize);
	CChunker* pChunker = NEW CChunker(pData);

	// Datos que seguramente no se cargaran
	// es-en: Certainly data not loaded.
	pScene3D->m_fAmbientR = 0.f;
	pScene3D->m_fAmbientG = 0.f;
	pScene3D->m_fAmbientB = 0.f;

	// Main chunk
	pChunker->Enter();

	// Cargar 
	if (pChunker->Find(KEYF3DS)) {
		// Leer datos de los keyframes
		if (pChunker->Find(KEYF_SEG)) {
			int iFirstFrame, iLastFrame;
			pChunker->ReadData(&iFirstFrame, sizeof(int));
			pChunker->ReadData(&iLastFrame, sizeof(int));
			pScene3D->m_fFirstFrame = (float)iFirstFrame;
			pScene3D->m_fLastFrame = (float)iLastFrame;
			pScene3D->m_fFrameSpeed = 1;
			pScene3D->m_fTicksPerFrame = 30.f;
			pScene3D->m_fFPS = pScene3D->m_fFrameSpeed * pScene3D->m_fTicksPerFrame;
		}
		pChunker->Leave();
	}
	pChunker->Leave();

	// 3DSData
	pChunker->First();
	do {
		// Current Chunk
		switch (pChunker->Enter()) {
			case EDIT3DS: {
				LoadObjects(pScene3D, pChunker);
				break;
			}
			case KEYF3DS: {
				LoadKeys(pScene3D, pChunker);
				break;
			}
			default: {
				break;
			}
		}
		pChunker->Leave();
	} while (!pChunker->EndOf());

	DELETE_PTR(pChunker);


	/*
	//---------------------------------------------------------------------------//
	// Materials
	//---------------------------------------------------------------------------//
	uint* auMaterialIDs = nullptr;
	if (Header.uNumMateriales > 0) {
		auMaterialIDs = NEW_ARRAY(uint, Header.uNumMateriales);
	}

	IMaterialManagerService* pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	for (i = 0; i < Header.uNumMateriales; i++) {
		auMaterialIDs[i] = CMaterialManager::INVALID;
		TN3DMaterialv1 Mat;
		ReadData(pData, &Mat, sizeof(Mat));
		auMaterialIDs[i] = pMatMgr->AddMaterial(Mat.pNombre);
		if (auMaterialIDs[i] == CMaterialManager::INVALID) {
			// Si no se puede cargar el material, intentaremos ponerle uno flat
			GLOG(("ERROR: Can't load material %s. Setting default flat material\n", Mat.pNombre));
			auMaterialIDs[i] = pMatMgr->AddMaterial("flat.nmt");
			// Si aun asi no se puede cargar, 
			if (auMaterialIDs[i] == CMaterialManager::INVALID) {
				GLOG(("ERROR: Can't load standard material flat.nmt\n"));
				return false;
			}
		}
	}
	pEscena3D->SetMaterials(auMaterialIDs, Header.uNumMateriales);


	//---------------------------------------------------------------------------//
	// Objetos
	//---------------------------------------------------------------------------//
	CObject3D* pObjetos = NEW_ARRAY(CObject3D, Header.uNumObjetos);
	for (i = 0; i < Header.uNumObjetos; i++) {
		CObject3D* pObj = &pObjetos[i];
		TObj3DHeaderv1 ObjHeader;
		pObj->Init(Header.fLastFrame - Header.fFirstFrame);
		ReadData(pData, &ObjHeader, sizeof(TObj3DHeaderv1));
		if (ObjHeader.uNumFaces < 0) {
			GLOG(("ERROR: 3D object does have 0 faces\n"));
			return false;
		}

		// Header
		sprintf(pObj->m_pNombre, ObjHeader.pNombre);
		pObj->m_uFlags = ObjHeader.uFlags;
		pObj->m_uColor = ObjHeader.uColor;

		// Faces
		TFace* pFaces = NEW_ARRAY(TFace, ObjHeader.uNumFaces);
		ReadData(pData, pFaces, ObjHeader.uNumFaces * sizeof(TFace));
		for (j = 0; j < ObjHeader.uNumFaces; j++) {
			swap(pFaces[j].i1, pFaces[j].i2);
		}
		pObj->SetFaces(pFaces, ObjHeader.uNumFaces);

		// UV
		if (ObjHeader.uNumUV > 0) {
			uint* pFrameTimesUV = NEW_ARRAY(uint, 1);
			TVector2* pUV = NEW_ARRAY(TVector2, ObjHeader.uNumUV);
			ReadData(pData, pUV, ObjHeader.uNumUV * sizeof(TVector2));
			pFrameTimesUV[0] = 0;
			for (j = 0; j < ObjHeader.uNumUV; j++) {
				swap(pUV[j * 3 + 1], pUV[j * 3 + 2]);
			}
			pObj->SetUV(pUV, ObjHeader.uNumUV, pFrameTimesUV, 1);
			pObj->m_uColor = 0xFFFFFFFF;
		}

		// Vertices
		uint* pFrameTimesVertices = NEW_ARRAY(uint, ObjHeader.uNumFramesVertices);
		TVector3* pVertices = NEW_ARRAY(TVector3, ObjHeader.uNumVertices * ObjHeader.uNumFramesVertices);
		ReadData(pData, pFrameTimesVertices, ObjHeader.uNumFramesVertices * sizeof(uint));
		ReadData(pData, pVertices, ObjHeader.uNumVertices * ObjHeader.uNumFramesVertices * sizeof(TVector3));
		pObj->SetVertices(pVertices, ObjHeader.uNumVertices, pFrameTimesVertices, ObjHeader.uNumFramesVertices);

		// Posiciones
		uint* pFrameTimesPosiciones = NEW_ARRAY(uint, ObjHeader.uNumFramesPosiciones);
		TVector3* pPosiciones = NEW_ARRAY(TVector3, ObjHeader.uNumFramesPosiciones);
		ReadData(pData, pFrameTimesPosiciones, ObjHeader.uNumFramesPosiciones * sizeof(uint));
		ReadData(pData, pPosiciones, ObjHeader.uNumFramesPosiciones * sizeof(TVector3));
		pObj->SetPosiciones(pPosiciones, pFrameTimesPosiciones, ObjHeader.uNumFramesPosiciones);

		// Rotaciones
		uint* pFrameTimesRotaciones = NEW_ARRAY(uint, ObjHeader.uNumFramesRotaciones);
		TVector4* pRotaciones = NEW_ARRAY(TVector4, ObjHeader.uNumFramesRotaciones);
		ReadData(pData, pFrameTimesRotaciones, ObjHeader.uNumFramesRotaciones * sizeof(uint));
		ReadData(pData, pRotaciones, ObjHeader.uNumFramesRotaciones * sizeof(TVector4));
		pObj->SetRotaciones(pRotaciones, pFrameTimesRotaciones, ObjHeader.uNumFramesRotaciones);

		// Escalados
		uint* pFrameTimesEscalados = NEW_ARRAY(uint, ObjHeader.uNumFramesEscalados);
		TVector3* pEscalados = NEW_ARRAY(TVector3, ObjHeader.uNumFramesEscalados);
		ReadData(pData, pFrameTimesEscalados, ObjHeader.uNumFramesEscalados * sizeof(uint));
		ReadData(pData, pEscalados, ObjHeader.uNumFramesEscalados * sizeof(TVector3));
		pObj->SetEscalados(pEscalados, pFrameTimesEscalados, ObjHeader.uNumFramesEscalados);

		// Prepara el objeto
		pObj->Prepara(pMateriales, Header.uNumMateriales);
	}
	pEscena3D->SetObjects(pObjetos, Header.uNumObjetos);


	//---------------------------------------------------------------------------//
	// Cameras
	//---------------------------------------------------------------------------//
	CCamera3D* pCamaras = NEW_ARRAY(CCamera3D, Header.uNumCamaras);
	for (i = 0; i < Header.uNumCamaras; i++) {
		TCam3DHeaderv1 CamHeader;
		CCamera3D* pCam = &pCamaras[i];
		pCam->Init();

		// Header
		ReadData(pData, &CamHeader, sizeof(TCam3DHeaderv1));
		sprintf(pCam->m_pNombre, "%d", CamHeader.uID);

		// PosSrc
		uint* pFrameTimesPosSrc = NEW_ARRAY(uint, CamHeader.uNumFramesPosSrc);
		TVector3* pPosSrc = NEW_ARRAY(TVector3, CamHeader.uNumFramesPosSrc);
		ReadData(pData, pFrameTimesPosSrc, CamHeader.uNumFramesPosSrc * sizeof(uint));
		ReadData(pData, pPosSrc, CamHeader.uNumFramesPosSrc * sizeof(TVector3));
		pCam->SetPosSrc(pPosSrc, pFrameTimesPosSrc, CamHeader.uNumFramesPosSrc);
		DISPOSE_ARRAY(pFrameTimesPosSrc);
		DISPOSE_ARRAY(pPosSrc);

		// PosTgt
		uint* pFrameTimesPosTgt = NEW_ARRAY(uint, CamHeader.uNumFramesPosTgt);
		TVector3* pPosTgt = NEW_ARRAY(TVector3, CamHeader.uNumFramesPosTgt);
		ReadData(pData, pFrameTimesPosTgt, CamHeader.uNumFramesPosTgt * sizeof(uint));
		ReadData(pData, pPosTgt, CamHeader.uNumFramesPosTgt * sizeof(TVector3));
		pCam->SetPosTgt(pPosTgt, pFrameTimesPosTgt, CamHeader.uNumFramesPosTgt);
		DISPOSE_ARRAY(pFrameTimesPosTgt);
		DISPOSE_ARRAY(pPosTgt);

		// Fov
		uint* pFrameTimesFov = NEW_ARRAY(uint, CamHeader.uNumFramesFov);
		float* pFov = NEW_ARRAY(float, CamHeader.uNumFramesFov);
		ReadData(pData, pFrameTimesFov, CamHeader.uNumFramesFov * sizeof(uint));
		ReadData(pData, pFov, CamHeader.uNumFramesFov * sizeof(float));
		pCam->SetFov(pFov, pFrameTimesFov, CamHeader.uNumFramesFov);
		DISPOSE_ARRAY(pFrameTimesFov);
		DISPOSE_ARRAY(pFov);
	}
	pEscena3D->SetCameras(pCamaras, Header.uNumCamaras);


	//---------------------------------------------------------------------------//
	// Luces
	//---------------------------------------------------------------------------//
	CLight3D* pLuces = NEW_ARRAY(CLight3D, Header.uNumLuces);
	for (i = 0; i < Header.uNumLuces; i++) {
		TLuz3DHeaderv1 LuzHeader;
		CLight3D* pLuz = &pLuces[i];
		pLuz->Init();

		// Header
		ReadData(pData, &LuzHeader, sizeof(TLuz3DHeaderv1));
		sprintf(pLuz->m_pNombre, "%d", i);

		// PosSrc
		uint* pFrameTimesPosSrc = NEW_ARRAY(uint, LuzHeader.uNumFramesPosSrc);
		TVector3* pPosSrc = NEW_ARRAY(TVector3, LuzHeader.uNumFramesPosSrc);
		ReadData(pData, pFrameTimesPosSrc, LuzHeader.uNumFramesPosSrc * sizeof(uint));
		ReadData(pData, pPosSrc, LuzHeader.uNumFramesPosSrc * sizeof(TVector3));
		pLuz->SetPosSrc(pPosSrc, pFrameTimesPosSrc, LuzHeader.uNumFramesPosSrc);
		DISPOSE_ARRAY(pFrameTimesPosSrc);
		DISPOSE_ARRAY(pPosSrc);

		// D3DLight
		pLuz->m_Light.Type = D3DLIGHT_POINT;
		pLuz->m_Light.Diffuse.r = LuzHeader.fR;
		pLuz->m_Light.Diffuse.g = LuzHeader.fG;
		pLuz->m_Light.Diffuse.b = LuzHeader.fB;
		pLuz->m_Light.Attenuation0 = 1.f;
		pLuz->m_Light.Attenuation1 = 0.f;
		pLuz->m_Light.Attenuation2 = 0.f;
		pLuz->m_Light.Falloff = 0.f;
		pLuz->m_Light.Range = LuzHeader.fRange;
	}
	pEscena3D->SetLights(pLuces, Header.uNumLuces);
	*/

	return false;
}
