//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_TGAHEADER_H
#define NEON_TGAHEADER_H

#pragma pack(push)
#pragma pack(1)

struct TGAHeader {
	uchar	IdLength;		// Image ID Field Length
	uchar	CmapType;		// Color Map Type
	uchar	ImageType;		// Image Type

	//-- Color Map Specification --//
	ushort	CmapIndex;		// First Entry Index
	ushort	CmapLength;		// Color Map Length
	uchar	CmapEntrySize;	// Color Map Entry Size

	//-- Image Specification --//
	ushort	X_Origin;		// X-origin of Image
	ushort	Y_Origin;		// Y-origin of Image
	ushort	ImageWidth;		// Image Width
	ushort	ImageHeight;	// Image Height
	uchar	PixelDepth;		// Pixel Depth
	uchar	ImagDesc;		// Image Descriptor
};

#pragma pack(pop)

#endif//NEON_TGAHEADER_H
