//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GECUSTOMVERTEX_H
#define NEON_GECUSTOMVERTEX_H

#include "GEVector.h"
// ebp-> Direct3D dependencies

//-----------------------------------------------------------------------------------------
enum {
	HCV_XYZ_F,
	HCV_XYZ_P,
	HCV_XYZ_S,
	HCV_XYZ_T,
	HCV_MAX_CUSTOM_VERTEX,
};

//---------------------------------------------------------------------------//
// Utils
//---------------------------------------------------------------------------//
#define HCV_DECL(Name) struct TVertex_##Name## {
#define HCV_DECL_END() }
#define HCV_SET_XYZ(_pV, _fX, _fY, _fZ)				{ (_pV)->vPos.x = _fX; (_pV)->vPos.y = _fY; (_pV)->vPos.z = _fZ; }
#define HCV_SET_XYZW(_pV, _fX, _fY, _fZ, _fW)		{ (_pV)->vPos.x = _fX; (_pV)->vPos.y = _fY; (_pV)->vPos.z = _fZ; (_pV)->vPos.w = _fW; }
#define HCV_SET_NORMAL(_pV, _fX, _fY, _fZ)			{ (_pV)->vNormal.x = _fX; (_pV)->vNormal.y = _fY; (_pV)->vNormal.z = _fZ; }
#define HCV_SET_TANGENT(_pV, _fX, _fY, _fZ, _fW)	{ (_pV)->vTangent.x = _fX; (_pV)->vTangent.y = _fY; (_pV)->vTangent.z  = _fZ; }
#define HCV_SET_BINORMAL(_pV, _fX, _fY, _fZ, _fW)	{ (_pV)->vBinormal.x = _fX; (_pV)->vBinormal.y = _fY; (_pV)->vBinormal.z = _fZ; }
#define HCV_SET_COLOR(_pV, _uDiffuse)				{ (_pV)->uDiffuse = _uDiffuse; }
#define HCV_SET_ARGB(_pV, _uA, _uR, _uG, _uB)		{ (_pV)->uDiffuse = HARD_COLOR_ARGB(_uA,_uR,_uG,_uB); }
#define HCV_SET_UV0(_pV, _fU, _fV)					{ (_pV)->vUV0.x = _fU; (_pV)->vUV0.y = _fV; }
#define HCV_SET_UV1(_pV, _fU, _fV)					{ (_pV)->vUV1.x = _fU; (_pV)->vUV1.y = _fV; }
#define HCV_SET_UV2(_pV, _fU, _fV)					{ (_pV)->vUV2.x = _fU; (_pV)->vUV2.y = _fV; }
#define HCV_SET_UV3(_pV, _fU, _fV)					{ (_pV)->vUV3.x = _fU; (_pV)->vUV3.y = _fV; }
#define HCV_SET_PSIZE(_pV, _fSize)					{ (_pV)->fPSize = _fSize; }


//---------------------------------------------------------------------------//
// Vertice para el fixed pipeline
// es-en: Vertices for the Fixed Pipeline
//---------------------------------------------------------------------------//
HCV_DECL(HCV_XYZ_F)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DVECTOR vPos;			// Vertice
	D3DVECTOR vNormal;		// Normal
	uint      uDiffuse;		// Diffuse
	D3DVECTOR vUV0;			// Texture Coordinates for Stage 0
	D3DVECTOR vUV1;			// Texture Coordinates for Stage 1
#else
	TVector3 vPos;
	TVector3 vNormal;
	uint     uDiffuse;
	TVector2 vUV0;
	TVector2 vUV1;
#endif//GFX_ENGINE
HCV_DECL_END();

//---------------------------------------------------------------------------//
// Vertice para el programable pipeline
// es-en: Verices for the Programmable Pipeline
//---------------------------------------------------------------------------//
HCV_DECL(HCV_XYZ_P)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DVECTOR vPos;			// Vertex Position
	D3DVECTOR vNormal;		// Normal
	D3DVECTOR vTangent;		// Tangent
	D3DVECTOR vBinormal;	// Binormal
	uint      uDiffuse;		// Diffuse
	D3DVECTOR vUV0;			// Texture Coordinates for Stage 0
	D3DVECTOR vUV1;			// Texture Coordinates for Stage 1
	D3DVECTOR vUV2;			// Texture Coordinates for Stage 2
	D3DVECTOR vUV3;			// Texture Coordinates for Stage 3
#else
	TVector3 vPos;
	TVector3 vNormal;
	TVector3 vTangent;
	TVector3 vBinormal;
	uint     uDiffuse;
	TVector2 vUV0;
	TVector2 vUV1;
	TVector2 vUV2;
	TVector2 vUV3;
#endif//GFX_ENGINE
HCV_DECL_END();

//---------------------------------------------------------------------------//
// Vertice para el filtershader
// es-en: Vertices for the Shader
//---------------------------------------------------------------------------//
HCV_DECL(HCV_XYZ_S)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DVECTOR vPos;			// Vertex Position
	float     fPSize;		// Point Size (for Point Sprites)
	uint      uDiffuse;		// Diffuse
	D3DVECTOR vUV0;			// Texture Coordinates for Stage 0
	D3DVECTOR vUV1;			// Texture Coordinates for Stage 1
	D3DVECTOR vUV2;			// Texture Coordinates for Stage 2
	D3DVECTOR vUV3;			// Texture Coordinates for Stage 3
#else
	TVector3 vPos;
	float    fPSize;
	uint     uDiffuse;
	TVector2 vUV0;
	TVector2 vUV1;
	TVector2 vUV2;
	TVector2 vUV3;
#endif//GFX_ENGINE
HCV_DECL_END();

//---------------------------------------------------------------------------//
// Vertice transformado
// es-en: Vertices Transformation
//---------------------------------------------------------------------------//
HCV_DECL(HCV_XYZ_T)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	TVector4  vPos;			// Vertex (transformation)
	float     fPSize;		// Point Size (for Point Sprites)
	uint      uDiffuse;		// Diffuse
	D3DVECTOR vUV0;			// Texture Coordinates for Stage 0
	D3DVECTOR vUV1;			// Texture Coordinates for Stage 1
#else
	TVector4 vPos;
	float    fPSize;
	uint     uDiffuse;
	TVector2 vUV0;
	TVector2 vUV1;
#endif//GFX_ENGINE
HCV_DECL_END();

#endif//NEON_GECUSTOMVERTEX_H
