//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_PATHLINEAR_H
#define NEON_PATHLINEAR_H

#include "Path.h"

class CPathLinear : public CPath {
public:
	CPathLinear();
	~CPathLinear() override;

	auto Init(int iType, int iNumKeys) -> bool override;

	auto AddKey(float fTime) -> void override;
	auto AddKey(float fTime, float Key) -> void override;
	auto AddKey(float fTime, TVector2 const& rKey) -> void override;
	auto AddKey(float fTime, TVector3 const& rKey) -> void override;
	auto AddKey(float fTime, TVector4 const& rKey) -> void override;
	auto Initialize() -> void override {}

	auto SetTime(float fTime) -> void override;
	auto GetTime() const -> float override {
		return m_fTime;
	}

	auto Run(float fRunTime) -> void override;

	auto GetNumKeys() const -> int override {
		return m_iNumKeys;
	}
	auto GetMaxKeys() const -> int override {
		return m_iMaxKeys;
	}

	// Get interpolated keys for the computed frame
	auto GetKeyValue(float& rValue) -> void override;
	auto GetKeyValue(TVector2& rValue) -> void override;
	auto GetKeyValue(TVector3& rValue) -> void override;
	auto GetKeyValue(TVector4& rValue) -> void override;
	auto GetKeyValues(TVector2 const* pKeys, TVector2* pValues, int iNumKeys) -> void override;
	auto GetKeyValues(TVector3 const* pKeys, TVector3* pValues, int iNumKeys) -> void override;

private:
	// GetKeys
	auto CurKey() const -> int { return m_iKey; }
	auto NextKey(int /*iKey*/) const -> int { return ((m_iKey < (m_iMaxKeys - 1)) ? (m_iKey + 1) : 0); }
	auto PrevKey(int /*iKey*/) const -> int { return ((m_iKey > 0) ? (m_iKey - 1) : (m_iMaxKeys - 1)); }

	// Get constant keys
	virtual auto GetKey1(int iKey) -> float* { ASSERT(iKey >= 0 && iKey < m_iNumKeys); return &m_pKeys1[iKey]; }
	virtual auto GetKey2(int iKey) -> TVector2* { ASSERT(iKey >= 0 && iKey < m_iNumKeys); return &m_pKeys2[iKey]; }
	virtual auto GetKey3(int iKey) -> TVector3* { ASSERT(iKey >= 0 && iKey < m_iNumKeys); return &m_pKeys3[iKey]; }
	virtual auto GetKey4(int iKey) -> TVector4* { ASSERT(iKey >= 0 && iKey < m_iNumKeys); return &m_pKeys4[iKey]; }

	auto SearchKeys() -> void;

private:
	// Keys
	int m_iKey;
	float* m_pKeys1;
	TVector2* m_pKeys2;
	TVector3* m_pKeys3;
	TVector4* m_pKeys4;
	float* m_pTimes;
};

#endif//NEON_PATHLINEAR_H
