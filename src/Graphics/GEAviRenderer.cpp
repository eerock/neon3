//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEDisplayDevice.h"
#include "GEAviRenderer.h"
#include "GEMaterialManager.h"
#include "GETexture.h"

#define AVIIF_KEYFRAME	0x00000010L


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CAviRenderer::CAviRenderer()
	: m_pAVIFile(nullptr)
#if (PLATFORM == PLATFORM_WINDOWS)
	, m_pAviFile(nullptr)
	, m_pAviStream(nullptr)
	, m_pAviStreamCompressed(nullptr)
	, m_pAviText(nullptr)
#endif//PLATFORM
	, m_uWidth(0)
	, m_uHeight(0)
	, m_uFrameCount(0)
	, m_bIsWorking(false)
{
#if (PLATFORM == PLATFORM_WINDOWS)
	m_pAviOptions[0] = &m_aviOptions;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CAviRenderer::~CAviRenderer() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CAviRenderer\n"));
#endif
	StopRender();
}


//---------------------------------------------------------------------------//
//	StartRender
//---------------------------------------------------------------------------//
auto CAviRenderer::StartRender(CDisplayDevice* pDD, char const* pszFile, uint nFramerate) -> bool {
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(pszFile);
	UNREFERENCED_PARAMETER(nFramerate);
	// ebp-> This code was disabled because GEngine shouldn't depend on DemoSystem,
	// and by removing that dependency the CAppWindow global is no longer found.
	// So pass in the hWnd to this function or add it to the DisplayDevice.
	// This code is also disabled because it's not being called from anywhere.
	/*
#if (PLATFORM == PLATFORM_WINDOWS)
	m_bIsWorking = false;

	// Check VFW version.
	WORD wVer = HIWORD(VideoForWindowsVersion());

	if (wVer < 0x010A) {
		return false;
	}

	// Init library
	AVIFileInit();

	// Get an image and stuff it into a bitmap.
	HRESULT hr;
	HBITMAP bmp;

	if ((bmp = CaptureScreen(pDD)) == NULL) {
		return false;
	}

	LPBITMAPINFOHEADER lpInfoHeader = (LPBITMAPINFOHEADER)MakeDib(bmp, 32);
	DeleteObject(bmp);

	if (lpInfoHeader == nullptr) {
		return false;
	}

	m_uWidth = (int)lpInfoHeader->biWidth;
	m_uHeight = (int)lpInfoHeader->biHeight;

	// Open an avi file for writing
	hr = AVIFileOpen(&m_pAviFile, pszFile, OF_WRITE | OF_CREATE, NULL);
	if (hr != AVIERR_OK) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	// Configure the stream
	memset(&m_aviStreamInfo, 0, sizeof(m_aviStreamInfo));
	m_aviStreamInfo.fccType = streamtypeVIDEO; // stream type
	m_aviStreamInfo.fccHandler = 0;
	m_aviStreamInfo.dwScale = 1;
	m_aviStreamInfo.dwRate = nFramerate;
	m_aviStreamInfo.dwSuggestedBufferSize = lpInfoHeader->biSizeImage;
	SetRect(&m_aviStreamInfo.rcFrame, 0, 0, (int)lpInfoHeader->biWidth, (int)lpInfoHeader->biHeight);

	// And create the stream
	hr = AVIFileCreateStream(m_pAviFile, &m_pAviStream,	&m_aviStreamInfo);
	if (hr != AVIERR_OK) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	// Get save options (prompt dialog)
	memset(&m_aviOptions, 0, sizeof(m_aviOptions));
	if (!AVISaveOptions((HWND)g_AppWindow.GetHandle(), 0, 1, &m_pAviStream, (LPAVICOMPRESSOPTIONS FAR*) &m_pAviOptions)) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	// Create compressed stream
	hr = AVIMakeCompressedStream(&m_pAviStreamCompressed, m_pAviStream, &m_aviOptions, NULL);
	if (hr != AVIERR_OK) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	// Set it's format
	hr = AVIStreamSetFormat(m_pAviStreamCompressed, 0, lpInfoHeader, lpInfoHeader->biSize + lpInfoHeader->biClrUsed * sizeof(RGBQUAD));
	if (hr != AVIERR_OK) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	m_bIsWorking = true;

	// ebp-> this could cause a problem, but it seems like it should happen
	DISPOSE(lpInfoHeader);

#endif//PLATFORM
	*/

	return true;
}

//---[  StopRender  ]--------------------------------------------------------//
//
//  - Class     : CAviRenderer
//  - prototype : bool StopRender()
//
//  - Purpose   : Stops capturing and closes the file.
//
//---------------------------------------------------------------------------//
auto CAviRenderer::StopRender() -> bool {
#if (PLATFORM == PLATFORM_WINDOWS)
	// Close/free stuff

	if (m_pAviStream) {
		AVIStreamClose(m_pAviStream);
		m_pAviStream = nullptr;
	}

	if (m_pAviStreamCompressed) {
		AVIStreamClose(m_pAviStreamCompressed);
		m_pAviStreamCompressed = nullptr;
	}

	if (m_pAviText) {
		AVIStreamClose(m_pAviText);
		m_pAviText = nullptr;
	}

	if (m_pAviFile) {
		AVIFileClose(m_pAviFile);
		m_pAviFile = nullptr;
	}

	WORD wVer = HIWORD(VideoForWindowsVersion());

	if (wVer >= 0x010A) {
		AVIFileExit();
	}

	m_bIsWorking = false;

#endif//PLATFORM

	return true;
}


//---------------------------------------------------------------------------//
//	SaveFrame
//---------------------------------------------------------------------------//
auto CAviRenderer::SaveFrame(CDisplayDevice* pDD) -> bool {
	UNREFERENCED_PARAMETER(pDD);
	/*
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DSURFACE* pSurf = pDD->GetBackBufferSurface();
	D3DLOCKED_RECT Desc;
	pSurf->LockRect(&Desc, nullptr, 0);

	HRESULT hr = AVIStreamWrite(m_pAviStreamCompressed,
		m_uFrameCount,
		1,
		Desc.pBits,
		640 * 480 * 4,
		AVIIF_KEYFRAME,
		NULL,
		NULL
	);

	pSurf->UnlockRect();

	if (hr != AVIERR_OK) {
		return false;
	}
	m_uFrameCount++;

	if (!IsValid()) {
		return false;
	}

	// Get an image and stuff it into a bitmap.
	HRESULT hr;
	HBITMAP bmp;

	if ((bmp = CaptureScreen(pDD)) == NULL) {
		return false;
	}

	LPBITMAPINFOHEADER lpInfoHeader = (LPBITMAPINFOHEADER)MakeDib(bmp, 32);
	DeleteObject(bmp);

	if (lpInfoHeader == nullptr) {
		return false;
	}

	if (m_uWidth != (uint)lpInfoHeader->biWidth) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	if (m_uHeight != (uint)lpInfoHeader->biHeight) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	// Save frame
	hr = AVIStreamWrite(
		m_pAviStreamCompressed,
		m_uFrameCount,
		1,
		(LPBYTE) lpInfoHeader +
			lpInfoHeader->biSize +
			lpInfoHeader->biClrUsed * sizeof(RGBQUAD),
		lpInfoHeader->biSizeImage,
		AVIIF_KEYFRAME,
		NULL,
		NULL
	);

	if (hr != AVIERR_OK) {
		DISPOSE(lpInfoHeader);
		return false;
	}

	DISPOSE(lpInfoHeader);
	m_uFrameCount++;
#endif//GFX_ENGINE_DIRECT3D
	*/

	return true;
}


//---------------------------------------------------------------------------//
//	MakeDib
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CAviRenderer::MakeDib(HBITMAP hBitmap, UINT uBits) -> HANDLE {
	HANDLE              hdib;
	HDC                 hdc;
	BITMAP              bitmap;
	UINT                wLineLen;
	DWORD               dwSize;
	DWORD               wColSize;
	LPBITMAPINFOHEADER  lpbi;
	LPBYTE              lpBits;

	GetObject(hBitmap, sizeof(BITMAP), &bitmap);

	// DWORD align the width of the DIB.
	// Figure out the size of the colour table
	// Calculate the size of the DIB

	wLineLen = (bitmap.bmWidth * uBits + 31) / 32 * 4;
	wColSize = sizeof(RGBQUAD) * ((uBits <= 8) ? 1 << uBits : 0);
	dwSize = sizeof(BITMAPINFOHEADER) + wColSize + (DWORD)(UINT)wLineLen * (DWORD)(UINT)bitmap.bmHeight;

	// Allocate room for a DIB and set the LPBI fields

	hdib = (HANDLE)NEW_ARRAY(char, dwSize);
	if (!hdib) {
		return hdib;
	}

	lpbi = (LPBITMAPINFOHEADER)GlobalLock(hdib);

	lpbi->biSize = sizeof(BITMAPINFOHEADER);
	lpbi->biWidth = bitmap.bmWidth;
	lpbi->biHeight = bitmap.bmHeight;
	lpbi->biPlanes = 1;
	lpbi->biBitCount = (WORD)uBits;
	lpbi->biCompression = BI_RGB;
	lpbi->biSizeImage = dwSize - sizeof(BITMAPINFOHEADER) - wColSize;
	lpbi->biXPelsPerMeter = 0;
	lpbi->biYPelsPerMeter = 0;
	lpbi->biClrUsed = (uBits <= 8) ? 1 << uBits : 0;
	lpbi->biClrImportant  = 0;

	// Get the bits from the bitmap and stuff them after the LPBI

	lpBits = (LPBYTE)(lpbi + 1) + wColSize;

	hdc = CreateCompatibleDC(nullptr);

	GetDIBits(hdc, hBitmap, 0, bitmap. bmHeight, lpBits, (LPBITMAPINFO)lpbi, DIB_RGB_COLORS);

	// Fix this if GetDIBits messed it up...

	lpbi->biClrUsed = (uBits <= 8) ? 1 << uBits : 0;

	DeleteDC(hdc);
	GlobalUnlock(hdib);

	return hdib;
}



//---------------------------------------------------------------------------//
//	CaptureScreen
//---------------------------------------------------------------------------//
auto CAviRenderer::CaptureScreen(CDisplayDevice* pDD) -> HBITMAP {
	UNREFERENCED_PARAMETER(pDD);
	/*
	HDC hdcScreen;
	pDD->GetBackBufferSurface()->GetDC(&hdcScreen);
	HDC hdcCompatible = CreateCompatibleDC(hdcScreen);

	// Create a compatible bitmap for hdcScreen. 
	HBITMAP hbmScreen = CreateCompatibleBitmap(hdcScreen, pDD->GetModoGrafico().iWidth, pDD->GetModoGrafico().iHeight);
	if (hbmScreen == 0) {
		return 0;
	}

	// Select the bitmaps into the compatible DC.
	if (!SelectObject(hdcCompatible, hbmScreen)) {
		return 0;
	}

	if (!BitBlt(
		hdcCompatible,
		0, 0,
		pDD->GetModoGrafico().iWidth,
		pDD->GetModoGrafico().iHeight,
		hdcScreen,
		0, 0,
		SRCCOPY
		)) {
		return 0;
	}

	pDD->GetBackBufferSurface()->ReleaseDC(hdcScreen);
	// ebp-> todo: Global Refactor!  either pass in a pointer to the CAppWindow or have the CDisplayDevice store
	// a pointer back to the CAppWindow.  Maybe store the hWnd in the ModoGrafico
	ShowWindow((HWND)g_AppWindow.GetHandle(), SW_SHOW); 
	DeleteDC(hdcCompatible);
	return hbmScreen;
	*/
	return 0;
}

#endif//PLATFORM

