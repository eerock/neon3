//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GETYPES_H
#define NEON_GETYPES_H

//---------------------------------------------------------------------------//
enum EBlendMode {
	BL_ADDITIVE,
	BL_ALPHABLEND,
	BL_SUBTRACTIVE,
	BL_INVERT,
	BL_INVERTDEST,
	BL_MASK,
	BL_MULTIPLY,
	BL_INVMULTIPLY,
	BL_COLORMULTIPLY,
	BL_DARKEN,
	BL_LIGHTEN,
	BL_MODES_MAX,
};

//---------------------------------------------------------------------------//
// Viewport (two points)
//---------------------------------------------------------------------------//

struct TViewport {
	TViewport() : x0(0), y0(0), x1(0), y1(0) {}
	TViewport(int _x0, int _y0, int _x1, int _y1)
		: x0(_x0), y0(_y0), x1(_x1), y1(_y1)
	{}
	int x0;
	int y0;
	int x1;
	int y1;
};


//---------------------------------------------------------------------------//
//	GraphicsMode
//---------------------------------------------------------------------------//
struct TGraphicsMode {
	TGraphicsMode()
		: iDevice(0), iWidth(0), iHeight(0), iDepth(0), iRefresh(0), bFullScreen(false), bLockable(false)
	{}
	TGraphicsMode(int iDev, int iW, int iH, int iD, int iR, bool bF, bool bL)
		: iDevice(iDev), iWidth(iW), iHeight(iH), iDepth(iD), iRefresh(iR), bFullScreen(bF), bLockable(bL)
	{}
	int iDevice;
	int iWidth;
	int iHeight;
	int iDepth;
	int iRefresh;
	bool bFullScreen;
	bool bLockable;
};


#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)

#include <d3d9.h>

//---------------------------------------------------------------------------//
//	Static Vertex Buffers
//---------------------------------------------------------------------------//
struct TVertexBufferInfo {
	// ebp-> Direct3D 9
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer;
	LPDIRECT3DVERTEXDECLARATION9 pDecl;
	LPD3DVERTEXELEMENT9 pElements;
	uint uSize;
	uint uNumVertex;
	uint uLockIndex;
	uint uDrawIndex;
};

//---------------------------------------------------------------------------//
//	Static Index Buffers
//---------------------------------------------------------------------------//
struct TIndexBufferInfo {
	// ebp-> Direct3D 9
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer;
	uint uFormat;
	uint uSize;
	uint uNumIndex;
	uint uLockIndex;
	uint uDrawIndex;
	uint uMaxLocked;
};

#elif (GFX_ENGINE == GFX_ENGINE_OPENGL)
#endif//GFX_ENGINE

#endif//NEON_GETYPES_H
