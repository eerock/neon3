//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "GEDisplayDevice.h"
#include "GEMaterialManager.h"
#include "GETexture.h"
#include "GEEngine3D.h"
#include "GEEffects.h"


//---------------------------------------------------------------------------//
#ifndef offsetof
//#define offsetof(s, m)	(size_t)&(((s*)0)->m)
#define offsetof(s, m)	(size_t)&reinterpret_cast<const volatile char&>((((s*)0)->m))
#endif


#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)

// Stream, Offset, Type, Method, Usage, UsageIndex

// ebp-> Direct3D 9
// Fixed Pipeline Vertex
static D3DVERTEXELEMENT9 s_HCV_XYZ_F_Elements[] = {
	{ 0, offsetof(TVertex_HCV_XYZ_F, vPos),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_F, vNormal),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_F, uDiffuse),
		D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_F, vUV0),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_F, vUV1),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
	D3DDECL_END(),
};
// ebp-> Direct3D 9
// Programmable Pipeline Vertex
static D3DVERTEXELEMENT9 s_HCV_XYZ_P_Elements[] = {
	{ 0, offsetof(TVertex_HCV_XYZ_P, vPos),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vNormal),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vTangent),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vBinormal),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, uDiffuse),
		D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vUV0),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vUV1),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vUV2),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
	{ 0, offsetof(TVertex_HCV_XYZ_P, vUV3),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
	D3DDECL_END(),
};

// ebp-> Direct3D 9
// Shader Effect Vertex
static D3DVERTEXELEMENT9 s_HCV_XYZ_S_Elements[] = {
	{ 0, offsetof(TVertex_HCV_XYZ_S, vPos),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_S, fPSize),
		D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_PSIZE, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_S, uDiffuse),
		D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_S, vUV0),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_S, vUV1),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
	{ 0, offsetof(TVertex_HCV_XYZ_S, vUV2),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
	{ 0, offsetof(TVertex_HCV_XYZ_S, vUV3),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
	D3DDECL_END(),
};

// ebp-> Direct3D 9
// Transformation Vertex
static D3DVERTEXELEMENT9 s_HCV_XYZ_T_Elements[] = {
	{ 0, offsetof(TVertex_HCV_XYZ_T, vPos),
		D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITIONT, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_T, fPSize),
		D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_PSIZE, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_T, uDiffuse),
		D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_T, vUV0),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
	{ 0, offsetof(TVertex_HCV_XYZ_T, vUV1),
		D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
	D3DDECL_END(),
};

// La cantidad de vertices es algo estadistico. Cuando se necesita mas se incrementa
// el buffer automaticamente
// es-en: The number of vertices is something statistical. When you need more you
// increase the buffer automatically
static TVertexBufferInfo s_VBInfo[HCV_MAX_CUSTOM_VERTEX] = {
	{ nullptr, nullptr, s_HCV_XYZ_F_Elements,		// (F)ixed Pipeline
		sizeof(TVertex_HCV_XYZ_F), 50000, 0, 0 },
	{ nullptr, nullptr, s_HCV_XYZ_P_Elements,		// (P)rogrammable Pipeline
		sizeof(TVertex_HCV_XYZ_P), 50000, 0, 0 },
	{ nullptr, nullptr, s_HCV_XYZ_S_Elements,		// (S)hader Effect
		sizeof(TVertex_HCV_XYZ_S), 1000, 0, 0 },
	{ nullptr, nullptr, s_HCV_XYZ_T_Elements,		// (T)ransformation
		sizeof(TVertex_HCV_XYZ_T), 5000, 0, 0 },
};

static TIndexBufferInfo s_IBInfo = {
	nullptr, D3DFMT_INDEX16, sizeof(ushort), 5000, 0, 0, 0
};

//---------------------------------------------------------------------------//
//	Static Vars
//---------------------------------------------------------------------------//
static uint s_aTextureFormats[HARD_MAX_TEXTURE_FORMATS] = {
	HARD_TEX_FORMAT_P8,
	HARD_TEX_FORMAT_A8,
	HARD_TEX_FORMAT_A8L8,
	HARD_TEX_FORMAT_A4R4G4B4,
	HARD_TEX_FORMAT_R5G6B5,
	HARD_TEX_FORMAT_A1R5G5B5,
	HARD_TEX_FORMAT_A8R8G8B8,
	HARD_TEX_FORMAT_R8G8B8,
};

#endif//GFX_ENGINE_DIRECT3D



//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CDisplayDevice::CDisplayDevice()
	: m_bOk(false)
	, m_hWnd(0)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	, m_pD3D(nullptr)
	, m_pD3DDevice(nullptr)
	, m_pColorBuffer(nullptr)
	, m_pDepthBuffer(nullptr)
	, m_pDepthSurface(nullptr)
	, m_pBasicBST(nullptr)
	, m_tMatIdentity()
#endif//GFX_ENGINE_DIRECT3D
	, m_tMode()
	, m_iPixelShaderVersion(0)
	, m_bVertexLock(false)
	, m_bDrawingScene(false)
	, m_bAvailable(false)
	, m_tViewport()
	, m_fAspectRatio(0.f)
	, m_fMinZViewport(0.f)
	, m_fMaxZViewport(0.f)
	, m_iTargetWidth(0)
	, m_iTargetHeight(0)
	, m_uRenderTargetTexID(CMaterialManager::INVALID)
{}


//---------------------------------------------------------------------------//
//	Init
//	ebp-> can we go to DX10 or DX11?  What about a pure OpenGL renderer?
//---------------------------------------------------------------------------//
auto CDisplayDevice::Init(TGraphicsMode const& tMode, THWnd hWnd) -> bool {
	GLOG(("Initializing Display Device...\n"));
	//End();
	ResetVars();

	// Crear el objeto D3D9
	// es-en: Create the D3D9 object
	m_hWnd = hWnd;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)

	// ebp-> Direct3D 9
	m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (!m_pD3D) {
		GLOG(("ERROR: Direct3D9 not found!\n"));
		m_bOk = false;
		return m_bOk;
	}
	//HRESULT hRes = Direct3DCreate9Ex(D3D_SDK_VERSION, &m_pD3D);
	//if (FAILED(hRes)) {
	//	GLOG(("ERROR: Direct3D9Ex not created!\n"));
	//	return (m_bOk = false);
	//}

#endif//GFX_ENGINE_DIRECT3D

	auto pMatMgr = NEW CMaterialManager;
	if (!pMatMgr->Init()) {
		GLOG(("ERROR: Unable to instantiate/initialize CMaterialManager!\n"));
		DELETE_PTR(pMatMgr);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		SafeRelease(&m_pD3D);
#endif//GFX_ENGINE_DIRECT3D
		m_bOk = false;
		return m_bOk;
	}

	CServiceLocator<IMaterialManagerService>::RegisterService(pMatMgr);
	
	m_bOk = true;

	// Inicializar el dispositivo
	// es-en: Initialize the device
	Reset(tMode);
	Restore();

	CEngine3D::Init();
	CEffects::Init();

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CDisplayDevice::~CDisplayDevice() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CDisplayDevice\n"));
#endif
	End();
}

//---------------------------------------------------------------------------//
//	End
//---------------------------------------------------------------------------//
auto CDisplayDevice::End() -> void {
	if (IsOk()) {
		// Liberar D3D
		// es-en: Release D3D
		Release();
		CEngine3D::Release();
		CEffects::Release();

		CServiceLocator<IMaterialManagerService>::RegisterService(nullptr);

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		SafeRelease(&m_pD3DDevice);
		SafeRelease(&m_pD3D);
#endif
		// Destruir la ventana
		// es-en: Destroy the window
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	ResetVars
//	es-en: ClearVars
//---------------------------------------------------------------------------//
auto CDisplayDevice::ResetVars() -> void {
	m_hWnd = 0;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pD3D = nullptr;
	m_pD3DDevice = nullptr;
	//m_D3DDisplayMode ???
	m_pColorBuffer = nullptr;
	m_pDepthBuffer = nullptr;
	m_pDepthSurface = nullptr;
	m_pBasicBST = nullptr;
	D3DXMatrixIdentity(&m_tMatIdentity);
#endif//GFX_ENGINE_DIRECT3D

	//m_tModo ???
	m_iPixelShaderVersion = 0;
	m_bVertexLock = false;
	m_bDrawingScene = false;
	m_bAvailable = false;
	m_tViewport = TViewport(0, 0, 0, 0);
	m_fAspectRatio = 1.f;
	m_fMinZViewport = 0.f;
	m_fMaxZViewport = 0.f;
	m_iTargetWidth = 0;
	m_iTargetHeight = 0;
	//m_iIdRenderingToSurface = -2;	// ebp-> something other than -1
	m_uRenderTargetTexID = static_cast<uint>(-1);
}


//---------------------------------------------------------------------------//
//	PageFlip
//---------------------------------------------------------------------------//
auto CDisplayDevice::PageFlip() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pD3DDevice->Present(nullptr, nullptr, 0, nullptr);
	//m_pD3DDevice->PresentEx(NULL, NULL, NULL, NULL, NULL);
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	Release
//	Funcion: Se llama cuando se ha perdido el contexto del dispositivo
//	(minimizar, alt+tab)
//	es-en: Called when the device context is lost (Minimize, alt + tab)
//---------------------------------------------------------------------------//
auto CDisplayDevice::Release() -> void {
	CServiceLocator<IMaterialManagerService>::GetService()->Release();
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	SafeRelease(&m_pBasicBST);
	SafeRelease(&m_pColorBuffer);
	SafeRelease(&m_pDepthBuffer);
	SafeRelease(&m_pDepthSurface);
#endif
	LiberaVertexBuffers();
	LiberaIndexBuffers();
}


//---------------------------------------------------------------------------//
//	Restore
//	Funcion: Se llama cuando hay que restaurar el objeto direct3d debido
//	a un cambio de contexto (minimizar/maximizar, alt+tab en fullscreen)
//	Hay que volver a crear los buffers y las texturas
//	es-en: Called when the object to be restored due to a direct3d
//	context switch (minimize / maximize, alt + tab in fullscreen)
//	We must re-create the buffers and textures
//---------------------------------------------------------------------------//
auto CDisplayDevice::Restore() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Para el render normal
	// es-en: For normal render
	auto hRet = m_pD3DDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &m_pColorBuffer);
	if (FAILED(hRet)) {
		D3D_ERROR("Error: Can't get BackBuffer surface", hRet);
	}

	hRet = m_pD3DDevice->GetDepthStencilSurface(&m_pDepthBuffer);
	if (FAILED(hRet)) {
		D3D_ERROR("Error: Can't get DepthStencil surface", hRet);
	}

	// magic fuckin numbers
	int size = 512;
	if (m_tMode.iWidth >= 1024 || m_tMode.iHeight >= 1024) {
		size = 2048;
	} else if (m_tMode.iWidth >= 512  || m_tMode.iHeight >= 512 ) {
		size = 1024;
	}

	hRet = m_pD3DDevice->CreateDepthStencilSurface(size, size, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, TRUE, &m_pDepthSurface, NULL);
	if (FAILED(hRet)) {
		D3D_ERROR("Error: Can't create the offscreen DepthStencil buffer", hRet);
	}

	CreateVertexBuffers();
	CreateIndexBuffers();
	CreateBasicShader();

	// Valores de render por defecto
	// es-en: Render default values
	m_pD3DDevice->SetRenderState(D3DRS_CLIPPING, TRUE);
	CServiceLocator<IMaterialManagerService>::GetService()->Restore();

	SetRenderTarget(CMaterialManager::INVALID);
#endif
}


//---------------------------------------------------------------------------//
//	Reset
//	Reinicializa el dispositivo con los nuevos datos
//	es-en: Resets the device with new data
//---------------------------------------------------------------------------//
auto CDisplayDevice::Reset(TGraphicsMode const& tMode) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_bAvailable = false;
	m_tMode = tMode;
	int iDevice = m_tMode.iDevice ? m_tMode.iDevice : D3DADAPTER_DEFAULT;

	auto hr = m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &m_D3DDisplayMode);
	if (FAILED(hr)) {
		D3D_ERROR("ERROR: Failed to GetAdapterDisplayMode", hr);
		return;
	}

	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = !m_tMode.bFullScreen;
	d3dpp.BackBufferFormat = m_tMode.bFullScreen ? D3DFMT_A8R8G8B8 : m_D3DDisplayMode.Format;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.Flags = m_tMode.bLockable ? D3DPRESENTFLAG_LOCKABLE_BACKBUFFER : 0;
	d3dpp.BackBufferWidth = m_tMode.iWidth;
	d3dpp.BackBufferHeight = m_tMode.iHeight;
	d3dpp.hDeviceWindow = m_hWnd;
	d3dpp.PresentationInterval = m_tMode.bFullScreen ? D3DPRESENT_INTERVAL_ONE : 0;
	d3dpp.FullScreen_RefreshRateInHz = m_tMode.bFullScreen ? m_tMode.iRefresh : 0;

	// ebp-> Direct3D 9
	D3DCAPS9 Caps;
	hr = m_pD3D->GetDeviceCaps(iDevice, D3DDEVTYPE_HAL, &Caps);
	if (FAILED(hr)) {
		D3D_ERROR("ERROR: Failed to GetDeviceCaps", hr);
		return;
	}

	int ps_major = D3DSHADER_VERSION_MAJOR(Caps.PixelShaderVersion);
	int ps_minor = D3DSHADER_VERSION_MINOR(Caps.PixelShaderVersion);
	switch (ps_major) {
		case 1: {
			switch (ps_minor) {
				case 0: {
					m_iPixelShaderVersion = PS_1_0;
					break;
				}
				case 3: {
					m_iPixelShaderVersion = PS_1_3;
					break;
				}
				case 4: {
					m_iPixelShaderVersion = PS_1_4;
					break;
				}
				default: {
					m_iPixelShaderVersion = PS_1_0;
					break;
				}
			}
			break;
		}
		case 2: {
			m_iPixelShaderVersion = PS_2_0;
			break;
		}
		case 3: {
			m_iPixelShaderVersion = PS_3_0;
			break;
		}
		case 4: {
			switch (ps_minor) {
				case 0: {
					m_iPixelShaderVersion = PS_4_0;
					break;
				}
				case 1: {
					m_iPixelShaderVersion = PS_4_1;
					break;
				}
				default: {
					m_iPixelShaderVersion = PS_4_0;
					break;
				}
			}
			break;
		}
		case 5: {
			m_iPixelShaderVersion = PS_5_0;
			break;
		}
		default: {
			m_iPixelShaderVersion = PS_NONE;
			break;
		}
	}

	// Si aun no hay dispositivo, lo creamos
	// es-en: If still no device, we see
	if (!m_pD3DDevice) {
		hr = m_pD3D->CreateDevice(
			iDevice,
			D3DDEVTYPE_HAL,
			m_hWnd,
			D3DCREATE_HARDWARE_VERTEXPROCESSING,
			&d3dpp,
			//NULL,		// use with CreateDeviceEx
			&m_pD3DDevice
		);
		if (FAILED(hr)) {
			D3D_ERROR("ERROR: Failed to create Direct3D 9 device", hr);
		}
	} else {
		Release();
		hr = m_pD3DDevice->Reset(&d3dpp);
		if (FAILED(hr)) {
			// A las malas, si el Reset ha fallado (por un LostDevice) volvemos a crear el Dispositivo
			// es-en: A poor, if the reset has failed (for LostDevice) turn to create Device
			SafeRelease(&m_pD3DDevice);
			hr = m_pD3D->CreateDevice(
				iDevice,
				D3DDEVTYPE_HAL,
				m_hWnd,
				D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_FPU_PRESERVE,
				&d3dpp,
				//NULL,		// use with CreateDeviceEx
				&m_pD3DDevice
			);
			if (FAILED(hr)) {
				D3D_ERROR("ERROR: Failed to create Direct3D 9 device", hr);
			}
		}
		Restore();
	}

	m_bAvailable = true;
#else
	UNREFERENCED_PARAMETER(tMode);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	SetViewport
//---------------------------------------------------------------------------//
auto CDisplayDevice::SetViewport(TRect const& Viewport, float fMinZ, float fMaxZ) -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto tNewViewport = TViewport(Viewport.x, Viewport.y, Viewport.x + Viewport.w, Viewport.y + Viewport.h);
	if (fMinZ != m_fMinZViewport || fMaxZ != m_fMaxZViewport ||
			tNewViewport.x0 != m_tViewport.x0 ||
			tNewViewport.y0 != m_tViewport.y0 ||
			tNewViewport.x1 != m_tViewport.x1 ||
			tNewViewport.y1 != m_tViewport.y1) {
		// ebp-> Direct3D 9
		D3DVIEWPORT9 vp;
		m_tViewport = tNewViewport;
		vp.X = Viewport.x;
		vp.Y = Viewport.y;
		vp.Width = Viewport.w;
		vp.Height = Viewport.h;
		vp.MinZ = fMinZ;
		vp.MaxZ = fMaxZ;
		m_fAspectRatio = (float)m_iTargetWidth / (float)m_iTargetHeight;
#ifdef _DEBUG
		auto hRet = 
#endif
		m_pD3DDevice->SetViewport(&vp);
		ASSERT(SUCCEEDED(hRet));
	}
#else
	UNREFERENCED_PARAMETER(Viewport);
	UNREFERENCED_PARAMETER(fMinZ);
	UNREFERENCED_PARAMETER(fMaxZ);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	SetRenderTarget
//---------------------------------------------------------------------------//
auto CDisplayDevice::SetRenderTarget(uint uRenderTargetTexID) -> void {
	ASSERT(IsOk());
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DSURFACE* pSurfaceRenderTarget = nullptr;
	//D3DSURFACE* pSurfaceDepthStencil = nullptr;

	if (uRenderTargetTexID != m_uRenderTargetTexID) {
		bool bBegin = m_bDrawingScene;
		if (m_bDrawingScene) {
			EndScene();
		}

		/////////////////////////////////////////////////////////////
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		auto pTex = pMatMgr->GetTexture(uRenderTargetTexID);
		if (uRenderTargetTexID != CMaterialManager::INVALID) {
			pSurfaceRenderTarget = pTex->GetSurfaceD3D();
		} else {
			pSurfaceRenderTarget = m_pColorBuffer;
		}

		/////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////
		if (uRenderTargetTexID != CMaterialManager::INVALID) {
			if (FAILED(m_pD3DDevice->SetDepthStencilSurface(m_pDepthSurface))) {
				GLOG(("ERROR:: Can't set stencil target for texture %d\n", uRenderTargetTexID));
			}
		} else {
			if (FAILED(m_pD3DDevice->SetDepthStencilSurface(m_pDepthBuffer))) {
				GLOG(("ERROR:: Can't set stencil target for texture %d\n", uRenderTargetTexID));
			}
		}

		/*
		if (iIdTexturaDepthStencil != CMaterialManager::INVALID) {
		//	CHardwareMaterial* pMaterial = m_pMaterialMgr->GetMaterial(iIdTexturaDepthStencil);
		//	pSurfaceDepthStencil = pMaterial->m_pSurface;
		} else {
		//	HRESULT hRet = m_pD3DDevice->GetDepthStencilSurface(&pSurfaceDepthStencil);
		//	ASSERT(SUCCEEDED(hRet));
		}
		*/

		m_uRenderTargetTexID = uRenderTargetTexID;
		//ASSERT(pSurfaceRenderTarget && pSurfaceDepthStencil);
		if (FAILED(m_pD3DDevice->SetRenderTarget(0, pSurfaceRenderTarget))) {
			GLOG(("ERROR: Can't set render target for texture %d\n", m_uRenderTargetTexID));
		}

		// Viewport
		if (m_uRenderTargetTexID == CMaterialManager::INVALID) {
			m_iTargetWidth = m_tMode.iWidth;
			m_iTargetHeight = m_tMode.iHeight;
		} else {
			m_iTargetWidth = pTex->GetWidth();
			m_iTargetHeight = pTex->GetHeight();
		}

		SetViewport(TRect(0, 0, m_iTargetWidth, m_iTargetHeight), 0.f, 1.f);
		float f = 1.f / static_cast<float>(m_iTargetWidth);
		CEngine3D::SetRegisterData(CEngine3D::V_INVSCREENW, &f);
		f = 1.f / static_cast<float>(m_iTargetHeight);
		CEngine3D::SetRegisterData(CEngine3D::V_INVSCREENH, &f);

		f = static_cast<float>(m_iTargetWidth);
		CEngine3D::SetRegisterData(CEngine3D::V_SCREENW, &f);
		f = static_cast<float>(m_iTargetHeight);
		CEngine3D::SetRegisterData(CEngine3D::V_SCREENH, &f);

		if (bBegin) {
			BeginScene();
		}
	}
#else
	UNREFERENCED_PARAMETER(iIdTexturaRenderTarget);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	SetBilinearFiltering
//---------------------------------------------------------------------------//
auto CDisplayDevice::SetBilinearFiltering(int iStage, bool bEnable) -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (bEnable) {
		m_pD3DDevice->SetSamplerState(iStage, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pD3DDevice->SetSamplerState(iStage, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	} else {
		m_pD3DDevice->SetSamplerState(iStage, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		m_pD3DDevice->SetSamplerState(iStage, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	}
#else
	UNREFERENCED_PARAMETER(iStage);
	UNREFERENCED_PARAMETER(bEnable);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DeactivateStage
//---------------------------------------------------------------------------//
auto CDisplayDevice::DeactivateStage(int iStage) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pD3DDevice->SetTextureStageState(iStage, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	m_pD3DDevice->SetTextureStageState(iStage, D3DTSS_COLOROP, D3DTOP_DISABLE);
	m_pD3DDevice->SetTexture(iStage, nullptr);
#else
	UNREFERENCED_PARAMETER(iStage);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	SetIdentity
//---------------------------------------------------------------------------//
auto CDisplayDevice::SetIdentity() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pD3DDevice->SetTransform(D3DTS_VIEW, &m_tMatIdentity);
	m_pD3DDevice->SetTransform(D3DTS_WORLD, &m_tMatIdentity);
	m_pD3DDevice->SetTransform(D3DTS_PROJECTION, &m_tMatIdentity);
#endif//GFX_ENGINE
}

//---------------------------------------------------------------------------//
//	ResetVideoMemory
//---------------------------------------------------------------------------//
auto CDisplayDevice::ResetVideoMemory() -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (m_pD3DDevice) {
		//HRESULT hRet = m_pD3DDevice->ResourceManagerDiscardBytes(0);
		//if (FAILED(hRet)) {
		//	D3D_ERROR("Error when calling ResourceManagerDiscardBytes", hRet);
		//}
	}
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
auto CDisplayDevice::Clear(bool bClearTarget, bool bClearZBuffer, uint uColor, float fZ) -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	uint uFlags = 0;
	uFlags |= bClearTarget ? D3DCLEAR_TARGET : 0;
	uFlags |= bClearZBuffer ? D3DCLEAR_ZBUFFER : 0;

#ifdef _DEBUG
	auto hRet = 
#endif
	m_pD3DDevice->Clear(0, nullptr, uFlags, uColor, fZ, 0);
	ASSERT(SUCCEEDED(hRet));
#else
	UNREFERENCED_PARAMETER(bClearTarget);
	UNREFERENCED_PARAMETER(bClearZBuffer);
	UNREFERENCED_PARAMETER(uColor);
	UNREFERENCED_PARAMETER(fZ);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	BeginFrame
//---------------------------------------------------------------------------//
auto CDisplayDevice::BeginFrame() -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Reset de los VertexBuffer
	// es-en: Reset the VertexBuffer
	for (uint uVB = 0; uVB < HCV_MAX_CUSTOM_VERTEX; ++uVB) {
		auto pVBInfo = &s_VBInfo[uVB];
		pVBInfo->uLockIndex = 0;
		pVBInfo->uDrawIndex = 0;
	}

	// Reset del IndexBuffer
	// es-en: Reset the IndexBuffer
	s_IBInfo.uLockIndex = 0;
	s_IBInfo.uDrawIndex = 0;

	SetRenderTarget(CMaterialManager::INVALID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	EndFrame
//---------------------------------------------------------------------------//
auto CDisplayDevice::EndFrame() -> void {
	// really?  nothing?
}


//---------------------------------------------------------------------------//
//	BeginScene
//---------------------------------------------------------------------------//
auto CDisplayDevice::BeginScene() -> void {
	ASSERT(IsOk());
	ASSERTM(!m_bDrawingScene, "CDisplayDevice::BeginScene()");

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
#ifdef _DEBUG
	auto hRet =
#endif
	m_pD3DDevice->BeginScene();
	ASSERT(SUCCEEDED(hRet));
#endif//GFX_ENGINE
	m_bDrawingScene = true;
}


//---------------------------------------------------------------------------//
//	EndScene
//---------------------------------------------------------------------------//
auto CDisplayDevice::EndScene() -> void {
	ASSERT(IsOk());
	ASSERTM(m_bDrawingScene, "CDisplayDevice::EndScene()");

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
#ifdef _DEBUG
	auto hRet =
#endif
	m_pD3DDevice->EndScene();
	ASSERT(SUCCEEDED(hRet));
#endif//GFX_ENGINE
	m_bDrawingScene = false;
}


//---------------------------------------------------------------------------//
//	DrawPrimitive
//---------------------------------------------------------------------------//
auto CDisplayDevice::DrawPrimitive(uint uCustomVertex, int iPrimitive, int iNumPrimitives) -> void {
	ASSERT(IsOk());
	ASSERT(ValidIndex(uCustomVertex, (uint)HCV_MAX_CUSTOM_VERTEX));
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pVBInfo = &s_VBInfo[uCustomVertex];
	m_pD3DDevice->SetVertexDeclaration(pVBInfo->pDecl);
	m_pD3DDevice->SetStreamSource(0, pVBInfo->pVertexBuffer, 0, pVBInfo->uSize);
	m_pD3DDevice->DrawPrimitive((D3DPRIMITIVETYPE)iPrimitive, pVBInfo->uDrawIndex, iNumPrimitives);
#else
	UNREFERENCED_PARAMETER(uCustomVertex);
	UNREFERENCED_PARAMETER(iPrimitive);
	UNREFERENCED_PARAMETER(iNumPrimitives);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DrawIndexedPrimitive
//---------------------------------------------------------------------------//
auto CDisplayDevice::DrawIndexedPrimitive(uint uCustomVertex, int iPrimitive, int iNumPrimitives) -> void {
	ASSERT(IsOk());
	ASSERT(ValidIndex(uCustomVertex, (uint)HCV_MAX_CUSTOM_VERTEX));
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pVBInfo = &s_VBInfo[uCustomVertex];
	auto pIBInfo = &s_IBInfo;
	m_pD3DDevice->SetVertexDeclaration(pVBInfo->pDecl);
	m_pD3DDevice->SetIndices(pIBInfo->pIndexBuffer);
	m_pD3DDevice->SetStreamSource(0, pVBInfo->pVertexBuffer, 0, pVBInfo->uSize);
	m_pD3DDevice->DrawIndexedPrimitive((D3DPRIMITIVETYPE)iPrimitive, pVBInfo->uDrawIndex, 0, pVBInfo->uLockIndex - pVBInfo->uDrawIndex, pIBInfo->uDrawIndex, iNumPrimitives);
#else
	UNREFERENCED_PARAMETER(uCustomVertex);
	UNREFERENCED_PARAMETER(iPrimitive);
	UNREFERENCED_PARAMETER(iNumPrimitives);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	ProcessVertices
//---------------------------------------------------------------------------//
auto CDisplayDevice::ProcessVertices(uint uCustomVertex, int iNumVertices) -> void {
	ASSERT(IsOk());
	ASSERT(uCustomVertex < HCV_MAX_CUSTOM_VERTEX);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pVBInfo = &s_VBInfo[uCustomVertex];
	m_pD3DDevice->SetVertexDeclaration(pVBInfo->pDecl);
	m_pD3DDevice->SetStreamSource(0, pVBInfo->pVertexBuffer, 0, pVBInfo->uSize);
	m_pD3DDevice->ProcessVertices(pVBInfo->uDrawIndex, pVBInfo->uLockIndex, iNumVertices, pVBInfo->pVertexBuffer, nullptr, D3DPV_DONOTCOPYDATA);
#else
	UNREFERENCED_PARAMETER(uCustomVertex);
	UNREFERENCED_PARAMETER(iNumVertices);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	LockVertexBuffer
//---------------------------------------------------------------------------//
auto CDisplayDevice::LockVertexBuffer(uint uCustomVertex, uint uNumVertex) -> void* {
	ASSERT(!m_bVertexLock);
	ASSERT(IsOk());
	ASSERT(uCustomVertex < HCV_MAX_CUSTOM_VERTEX);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pVBInfo = &s_VBInfo[uCustomVertex];

	// Gestion para que crezcan los vertices
	// es-en: Management to grow the vertices
	if (uNumVertex >= pVBInfo->uNumVertex) {
		EndScene();
		BeginScene();

		GLOG(("INF: uNumVertex(%d) > LockVertexBuffer(%d), growing buffer\n", uNumVertex, pVBInfo->uNumVertex));
		pVBInfo->uNumVertex = uNumVertex + 100;
		SafeRelease(&pVBInfo->pVertexBuffer);
#ifdef _DEBUG
		auto hRet =
#endif
		m_pD3DDevice->CreateVertexBuffer(
			pVBInfo->uNumVertex * pVBInfo->uSize,
			D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
			0,
			D3DPOOL_DEFAULT,
			&pVBInfo->pVertexBuffer,
			nullptr
		);
		ASSERT(SUCCEEDED(hRet));
		pVBInfo->uLockIndex = 0;
	}

	void* pVertices = nullptr;
	if ((pVBInfo->uLockIndex + uNumVertex) >= pVBInfo->uNumVertex) {
		pVBInfo->uLockIndex = 0;
	}

#ifdef _DEBUG
	auto hRet =
#endif
	pVBInfo->pVertexBuffer->Lock(
		pVBInfo->uSize * pVBInfo->uLockIndex,
		pVBInfo->uSize * uNumVertex,
		(void**)&pVertices,
		pVBInfo->uLockIndex ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD
	);
	pVBInfo->uDrawIndex = pVBInfo->uLockIndex;
	pVBInfo->uLockIndex += uNumVertex;
	ASSERT(SUCCEEDED(hRet));
	m_bVertexLock = true;

	return pVertices;
#else
	UNREFERENCED_PARAMETER(uCustomVertex);
	UNREFERENCED_PARAMETER(uNumVertex);
	return nullptr;
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	UnlockVertexBuffer
//---------------------------------------------------------------------------//
auto CDisplayDevice::UnlockVertexBuffer(uint uCustomVertex) -> void {
	ASSERT(m_bVertexLock);
	ASSERT(IsOk());
	ASSERT(uCustomVertex < HCV_MAX_CUSTOM_VERTEX);

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pVBInfo = &s_VBInfo[uCustomVertex];
#ifdef _DEBUG
	auto hRet =
#endif
	pVBInfo->pVertexBuffer->Unlock();
	ASSERT(SUCCEEDED(hRet));
	m_bVertexLock = false;
#else
	UNREFERENCED_PARAMETER(uCustomVertex);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	LockIndexBuffer
//---------------------------------------------------------------------------//
auto CDisplayDevice::LockIndexBuffer(uint uNumIndex) -> ushort* {
	ASSERT(IsOk());
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pIBInfo = &s_IBInfo;
	ASSERT(uNumIndex > 0 && uNumIndex < pIBInfo->uNumIndex);

	ushort* pIndices = nullptr;
	uint uSize = uNumIndex * pIBInfo->uSize;
	uint uFlags = D3DLOCK_NOOVERWRITE;
	if ((pIBInfo->uLockIndex + uNumIndex) >= pIBInfo->uNumIndex) {
		uFlags = D3DLOCK_DISCARD;
		pIBInfo->uLockIndex = 0;
	}
#ifdef _DEBUG
	auto hRet =
#endif
	pIBInfo->pIndexBuffer->Lock(pIBInfo->uLockIndex * pIBInfo->uSize, uSize, (void**)&pIndices, uFlags);
	pIBInfo->uDrawIndex = pIBInfo->uLockIndex;
	pIBInfo->uLockIndex += uNumIndex;
	ASSERT(SUCCEEDED(hRet));

	return pIndices;
#else
	UNREFERENCED_PARAMETER(uNumIndex);
    return nullptr;
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	UnlockIndexBuffer
//---------------------------------------------------------------------------//
auto CDisplayDevice::UnlockIndexBuffer() -> void {
	ASSERT(IsOk());
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pIBInfo = &s_IBInfo;

#ifdef _DEBUG
	auto hRet =
#endif
	pIBInfo->pIndexBuffer->Unlock();
	ASSERT(SUCCEEDED(hRet));
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	CreateVertexBuffers
//---------------------------------------------------------------------------//
auto CDisplayDevice::CreateVertexBuffers() -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	for (int i = 0; i < HCV_MAX_CUSTOM_VERTEX; i++) {
		auto pVBInfo = &s_VBInfo[i];
#ifdef _DEBUG
		auto hRet =
#endif
		m_pD3DDevice->CreateVertexBuffer(
			pVBInfo->uNumVertex * pVBInfo->uSize,
			D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
			0,
			D3DPOOL_DEFAULT,
			&pVBInfo->pVertexBuffer,
			nullptr
		);
		m_pD3DDevice->CreateVertexDeclaration(pVBInfo->pElements, &pVBInfo->pDecl);
		ASSERT(SUCCEEDED(hRet));
	}
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	LiberaVertexBuffers
//	es-en: Release Vertex Buffers
//---------------------------------------------------------------------------//
auto CDisplayDevice::LiberaVertexBuffers() -> void {
	ASSERT(!m_bVertexLock);
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	for (int i = 0; i < HCV_MAX_CUSTOM_VERTEX; i++) {
		auto pVBInfo = &s_VBInfo[i];
		SafeRelease(&pVBInfo->pVertexBuffer);
		SafeRelease(&pVBInfo->pDecl);
	}
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	CreateIndexBuffers
//---------------------------------------------------------------------------//
auto CDisplayDevice::CreateIndexBuffers() -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Crear todos los IndexBuffer
	// es-en: Create all IndexBuffers
	auto pIBInfo = &s_IBInfo;
#ifdef _DEBUG
	auto hRet =
#endif
	m_pD3DDevice->CreateIndexBuffer(
		pIBInfo->uNumIndex * pIBInfo->uSize,
		D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC,
		(D3DFORMAT)pIBInfo->uFormat,
		D3DPOOL_DEFAULT,
		&pIBInfo->pIndexBuffer,
		nullptr
	);
	ASSERT(SUCCEEDED(hRet));
#endif//GFX_ENGINE_DIRECT3D
}


//----------------------------------------------------------------------
//	LiberaIndexBuffers
//	es-en: Release Index Buffers
//----------------------------------------------------------------------
auto CDisplayDevice::LiberaIndexBuffers() -> void {
	ASSERT(IsOk());

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Liberar todos los IndexBuffer
	// es-en: Release all IndexBuffers
	SafeRelease(&s_IBInfo.pIndexBuffer);
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	ApplyBasicShader
//---------------------------------------------------------------------------//
auto CDisplayDevice::ApplyBasicShader() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pBasicBST->Apply();
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	CreateBasicShader
//---------------------------------------------------------------------------//
auto CDisplayDevice::CreateBasicShader() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pD3DDevice->BeginStateBlock();

	// Fill Mode
	m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pD3DDevice->SetRenderState(D3DRS_CLIPPING, TRUE);
	m_pD3DDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	m_pD3DDevice->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_TRUE);
	m_pD3DDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	m_pD3DDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
	m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pD3DDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pD3DDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);
	m_pD3DDevice->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
	m_pD3DDevice->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
	m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	for (int i = 0; i < 4; ++i) {
		m_pD3DDevice->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pD3DDevice->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pD3DDevice->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		m_pD3DDevice->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		m_pD3DDevice->SetTextureStageState(i, D3DTSS_TEXCOORDINDEX, i);
	}

	m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);

	m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

	m_pD3DDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	m_pD3DDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

	m_pD3DDevice->SetVertexShader(nullptr);
	m_pD3DDevice->SetPixelShader(nullptr);

	// Fin del bloque
	// es-en: End of Block
	m_pD3DDevice->EndStateBlock(&m_pBasicBST);
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	SetBlendMode
//---------------------------------------------------------------------------//
auto CDisplayDevice::SetBlendMode(int aBlend, float fAlpha) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto uAlpha = static_cast<uint>(fAlpha * 255.f);
	switch (aBlend) {
		case BL_ADDITIVE: {
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		case BL_ALPHABLEND: {
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			break;
		}
		case BL_SUBTRACTIVE: {
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		case BL_INVERT: {
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TFACTOR);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			break;
		}
		case BL_INVERTDEST: {
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_SUBTRACT);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			break;
		}
		case BL_MASK: {
			m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(255 - uAlpha, 255, 255, 255));
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_BOTHINVSRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			break;
		}
		case BL_MULTIPLY: {
			m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(uAlpha, 255, 255, 255));
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TFACTOR);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		case BL_INVMULTIPLY: {
			m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(uAlpha, 255, 255, 255));
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		case BL_COLORMULTIPLY: {
			m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(uAlpha, 255, 255, 255));
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TFACTOR);
			m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCCOLOR);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		case BL_DARKEN: {
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_MIN);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		case BL_LIGHTEN: {
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_MAX);
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		}
		default: {
			break;
		}
	}
#else
	UNREFERENCED_PARAMETER(fAlpha);
	UNREFERENCED_PARAMETER(aBlend);
#endif//GFX_ENGINE
}
