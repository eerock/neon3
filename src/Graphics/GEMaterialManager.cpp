﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"

// ebp-> Direct3D dependency

//#define CACHE_RESOURCES

IMaterialManagerService* CServiceLocator<IMaterialManagerService>::s_pService = nullptr;


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMaterialManager::CMaterialManager()
	: m_bOk(false)
	, m_mMaterials()
	, m_mTextures()
	, m_mShaders()
	, m_uNextMaterialID(INVALID)
	, m_uNextTextureID(INVALID)
	, m_uNextShaderID(INVALID)
	, m_uFlatMaterialID(INVALID)
	, m_uWireMaterialID(INVALID)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMaterialManager::~CMaterialManager() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMaterialManager\n"));
#endif
	if (IsOk()) {
		// Default materials
		//RemoveMaterial(m_uFlatMaterialID);
		//RemoveMaterial(m_uWireMaterialID);

		m_mMaterials.clear();
		m_mTextures.clear();
		m_mShaders.clear();

		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMaterialManager::Init() -> bool {
	m_bOk = true;

	m_mMaterials.clear();
	m_mTextures.clear();
	m_mShaders.clear();

	m_uNextMaterialID = INVALID;
	m_uNextTextureID = INVALID;
	m_uNextShaderID = INVALID;

	// Default materials
	//PushDir("Resources");
	//m_uFlatMaterialID = AddMaterial("flat.nmt");
	//m_uWireMaterialID = AddMaterial("wire.nmt");
	//PopDir();

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Release
//---------------------------------------------------------------------------//
auto CMaterialManager::Release() -> void {
	// this was commented out before refactor...
	for (auto& mp : m_mMaterials) {
		mp.second->Release();
	}

	for (auto& tp : m_mTextures) {
		tp.second->Release();
	}

	for (auto& sp : m_mShaders) {
		sp.second->Release();
	}

	// should we clear the maps too?
}


//---------------------------------------------------------------------------//
//	Restore
//---------------------------------------------------------------------------//
auto CMaterialManager::Restore() -> void {
	// this was commented out before refactor...
	for (auto& mp : m_mMaterials) {
		mp.second->Restore();
	}

	for (auto& tp : m_mTextures) {
		tp.second->Restore();
	}

	for (auto& sp : m_mShaders) {
		sp.second->Restore();
	}
}


//---------------------------------------------------------------------------//
//	AddMaterial
//---------------------------------------------------------------------------//
auto CMaterialManager::AddMaterial(std::string const& sFilename) -> uint {
	uint id = INVALID;

#ifdef CACHE_RESOURCES
	for (auto& mp : m_mMaterials) {
		if (Stricmp(sFilename, mp.second->GetFilename()) == 0) {
			id = mp.second->GetID();
			break;
		}
	}
#endif

	if (id == INVALID) {
		auto mp = std::make_unique<CMaterial>();
		if (mp->Init(sFilename)) {
			id = ++m_uNextMaterialID;
			mp->SetID(id);
			mp->AddRef();
			m_mMaterials[id] = std::move(mp);
		}
	}

	return id;
}


//---------------------------------------------------------------------------//
//	RemoveMaterial
//---------------------------------------------------------------------------//
auto CMaterialManager::RemoveMaterial(uint uMaterialID) -> void {
	if (m_mMaterials.count(uMaterialID)) {
		if (!m_mMaterials[uMaterialID]->SubRef()) {
			m_mMaterials.erase(uMaterialID);
		}
	}
}


//---------------------------------------------------------------------------//
//	GetMaterial
//---------------------------------------------------------------------------//
auto CMaterialManager::GetMaterial(uint uMaterialID) -> CMaterial* {
	if (m_mMaterials.count(uMaterialID)) {
		return m_mMaterials[uMaterialID].get();
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	AddTexture
//---------------------------------------------------------------------------//
auto CMaterialManager::AddTexture(std::string const& sFilename, uint uFlags) -> uint {
	uint id = INVALID;

#ifdef CACHE_RESOURCES
	for (auto& tp : m_mTextures) {
		if (Stricmp(sFilename, tp.second->GetFilename()) == 0) {
			id = tp.second->GetID();
			break;
		}
	}
#endif

	if (id == INVALID) {
		auto tp = std::make_unique<CTexture>();
		if (tp->Init(sFilename, uFlags)) {
			id = ++m_uNextTextureID;
			tp->SetID(id);
			tp->AddRef();
			m_mTextures[id] = std::move(tp);
		}
	}

	return id;
}


//---------------------------------------------------------------------------//
//	AddTexture
//---------------------------------------------------------------------------//
auto CMaterialManager::AddTexture(int iWidth, int iHeight, int iFormat, bool bDynamic, bool bRenderTarget, bool bDepthStencil, uint uFlags) -> uint {
	uint id = INVALID;
	auto tp = std::make_unique<CTexture>();
	if (tp->Init(iWidth, iHeight, iFormat, bDynamic, bRenderTarget, bDepthStencil, uFlags)) {
		id = ++m_uNextTextureID;
		tp->SetID(id);
		tp->AddRef();
		m_mTextures[id] = std::move(tp);
	}
	return id;
}


//---------------------------------------------------------------------------//
//	AddTexture
//	This probably shouldn't be used because the texture is allocated elsewhere.
//---------------------------------------------------------------------------//
auto CMaterialManager::AddTexture(CTexture* pTex, uint uFlags) -> uint {
	UNREFERENCED_PARAMETER(uFlags);
	uint id = INVALID;
	if (pTex != nullptr) {
		auto tp = std::unique_ptr<CTexture>(pTex);
		id = ++m_uNextTextureID;
		tp->SetID(id);
		tp->AddRef();
		m_mTextures[id] = std::move(tp);
	}
	return id;
}


//---------------------------------------------------------------------------//
//	RemoveTexture
//---------------------------------------------------------------------------//
auto CMaterialManager::RemoveTexture(uint uTextureID) -> void {
	if (m_mTextures.count(uTextureID)) {
		if (!m_mTextures[uTextureID]->SubRef()) {
			m_mTextures.erase(uTextureID);
		}
	}
}


//---------------------------------------------------------------------------//
//	GetTexture
//---------------------------------------------------------------------------//
auto CMaterialManager::GetTexture(uint uTextureID) -> CTexture* {
	if (m_mTextures.count(uTextureID)) {
		return m_mTextures[uTextureID].get();
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	AddShader
//---------------------------------------------------------------------------//
auto CMaterialManager::AddShader(std::string const& sFilename) -> uint {
	uint id = INVALID;

#ifdef CACHE_RESOURCES
	for (auto& sp : m_mShaders) {
		if (Stricmp(sFilename, sp.second->GetFilename()) == 0) {
			id = sp.second->GetID();
			break;
		}
	}
#endif

	if (id == INVALID) {
		auto sp = std::make_unique<CShader>();
		if (sp->Init(sFilename)) {
			id = ++m_uNextShaderID;
			sp->SetID(id);
			sp->AddRef();
			m_mShaders[id] = std::move(sp);
		}
	}

	return id;
}


//---------------------------------------------------------------------------//
//	RemoveShader
//---------------------------------------------------------------------------//
auto CMaterialManager::RemoveShader(uint uShaderID) -> void {
	if (m_mShaders.count(uShaderID)) {
		if (!m_mShaders[uShaderID]->SubRef()) {
			m_mShaders.erase(uShaderID);
		}
	}
}


//---------------------------------------------------------------------------//
//	GetShader
//---------------------------------------------------------------------------//
auto CMaterialManager::GetShader(uint uShaderID) -> CShader* {
	if (m_mShaders.count(uShaderID)) {
		return m_mShaders[uShaderID].get();
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	SetTexture
//	ebp-> this sets the 'Source' texture
//---------------------------------------------------------------------------//
auto CMaterialManager::SetTexture(uint uTextureID, int iStage) -> void {
	if (uTextureID != INVALID) {
		if (m_mTextures.count(uTextureID)) {
			m_mTextures[uTextureID]->Set(iStage);
		}
	}
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	else {
		g_DisplayDevice.GetD3DDevice()->SetTexture(iStage, nullptr);
	}
#endif//GFX_ENGINE_DIRECT3D
}
