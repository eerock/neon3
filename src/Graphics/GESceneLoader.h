//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GESCENELOADER_H
#define NEON_GESCENELOADER_H

class CGEScene3D;
class CChunker;


//---------------------------------------------------------------------------//
//	SceneLoader classes
//---------------------------------------------------------------------------//


//---------------------------------------------------------------------------//
class CSceneLoader {
public:
	virtual ~CSceneLoader() {}
	static auto LoadSelector(CGEScene3D*, char const*, uint) -> bool;
protected:
	virtual auto Load(CGEScene3D*, char const*, uint) -> bool = 0;
	auto ReadData(char const*& pSrc, void* pDst, uint uSize) const -> void {
		memcpy(pDst, pSrc, uSize);
		pSrc += uSize;
	}
};


//---------------------------------------------------------------------------//
class CN3DV1Loader : public CSceneLoader {
public:
	auto Load(CGEScene3D* pScene3D, char const* pData, uint uDataSize) -> bool override;
};


//---------------------------------------------------------------------------//
class CN3DV2Loader : public CSceneLoader {
public:
	auto Load(CGEScene3D* pScene3D, char const* pData, uint uDataSize) -> bool override;
};


//---------------------------------------------------------------------------//
class C3DSLoader : public CSceneLoader {
public:
	auto Load(CGEScene3D* pScene3D, char const* pData, uint uDataSize) -> bool override;
	auto LoadObjects(CGEScene3D* pScene3D, CChunker* pChunker) -> void;
	auto LoadKeys(CGEScene3D* pScene3D, CChunker* pChunker) -> void;

private:
	static auto CountMaterials(CChunker* pChunker) -> int;
	static auto CountObjects(CChunker* pChunker, int objType) -> int;
};

#endif//NEON_GESCENELOADER_H
