//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GETEMP_H
#define NEON_GETEMP_H

#include "GEVector.h"

class CDisplayDevice;
struct TVertex_HCV_XYZ_T;

namespace neon {

auto DrawLine(CDisplayDevice* pDD, TVector2 const& v0, TVector2 const& v1) -> void;
auto DrawRect(CDisplayDevice* pDD, TVector2 const& v0, TVector2 const& v1) -> void;
auto DrawQuad(CDisplayDevice* pDD, TVector2 const& v0, TVector2 const& v1) -> void;
auto DrawQuad(CDisplayDevice* pDD, bool bFlipY = false) -> void;
auto DrawQuadUV(CDisplayDevice* pDD, TVector2 const& v0, TVector2 const& v1, TVector2 const& uv0, TVector2 const& uv1) -> void;
auto DrawQuadUV(CDisplayDevice* pDD, TVector2 const& uv0, TVector2 const& uv1, bool bFlipY = false) -> void;
auto DrawQuadFlat(CDisplayDevice* pDD, TVector2 const& v0, TVector2 const& v1, uint uColor, float fZ) -> void;
auto PrepareVertices(TVertex_HCV_XYZ_T* pVertices, TVector2 const& v0, TVector2 const& v1, uint uColor = 0xFFFFFFFF) -> void;

} //namespace neon

#endif//NEON_GETEMP_H
