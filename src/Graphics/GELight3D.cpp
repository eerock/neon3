﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEDisplayDevice.h"
#include "GELight3D.h"
#include "PathLinear.h"
#include "PathSpline.h"
#include "CtrlVar.h"


static TCtrlVar s_Vars[] = {
	{ TCtrlVar::SLIDER, 0, "Intens", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, 1, "Range", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, 2, "Red", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, 3, "Green", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, 4, "Blue", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CLight3D::CLight3D()
	: m_bOk(false)
	, m_sName()
	, m_uType(0)
	, m_fR(0.f)
	, m_fG(0.f)
	, m_fB(0.f)
	, m_fIntens(0.f)
	, m_fRange(0.f)
	, m_pPathSrc(nullptr)
	, m_pPathTgt(nullptr)
	, m_pPathFalloff(nullptr)
	, m_fChangeIntens(0.f)
	, m_fChangeRange(0.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLight3D::~CLight3D() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLight3D\n"));
#endif
	if (IsOk()) {
		DELETE_PTR(m_pPathSrc);
		DELETE_PTR(m_pPathTgt);
		DELETE_PTR(m_pPathFalloff);
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CLight3D::Init() -> bool {
	m_fChangeIntens = 1.f;
	m_fChangeRange = 1.f;
	m_fR = 1.f;
	m_fG = 1.f;
	m_fB = 1.f;
	m_fRange = 1000.f;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	memset(&m_tLight, 0, sizeof(m_tLight));
	m_tLight.Type = D3DLIGHT_POINT;
	m_tLight.Position.x = 0.f;
	m_tLight.Position.y = 0.f;
	m_tLight.Position.z = 0.f;
	m_tLight.Attenuation0 = 1.f;
	m_tLight.Attenuation1 = 0.f;
	m_tLight.Attenuation2 = 0.f;
	m_tLight.Falloff = 0.f;
#endif//GFX_ENGINE_DIRECT3D

	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CLight3D::SetTime(float fTime) -> void {
	if (m_pPathSrc && m_pPathSrc->GetNumKeys() > 1) {
		m_pPathSrc->SetTime(fTime);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		m_pPathSrc->GetKeyValue(*((TVector3*)&m_tLight.Position));
#endif//GFX_ENGINE_DIRECT3D
	}

	if (m_pPathTgt && m_pPathTgt->GetNumKeys() > 1) {
		TVector3 Target;
		m_pPathTgt->SetTime(fTime);
		m_pPathTgt->GetKeyValue(Target);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		m_tLight.Direction.x = Target.x - m_tLight.Position.x;
		m_tLight.Direction.y = Target.y - m_tLight.Position.y;
		m_tLight.Direction.z = Target.z - m_tLight.Position.z;
#endif//GFX_ENGINE_DIRECT3D
	}

	if (m_pPathFalloff && m_pPathFalloff->GetNumKeys() > 1) {
		m_pPathFalloff->SetTime(fTime);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		m_pPathFalloff->GetKeyValue(*(static_cast<float*>(&m_tLight.Falloff)));
#endif//GFX_ENGINE_DIRECT3D
	}
}


//---------------------------------------------------------------------------//
//	SetFrame
//---------------------------------------------------------------------------//
auto CLight3D::Update() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_tLight.Diffuse.r = m_fR * m_fChangeIntens;
	m_tLight.Diffuse.g = m_fG * m_fChangeIntens;
	m_tLight.Diffuse.b = m_fB * m_fChangeIntens;
	m_tLight.Range = m_fRange * m_fChangeRange;
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	SetPosSrc
//	ebp-> should make pVertices and pTimes const as applicable
//---------------------------------------------------------------------------//
auto CLight3D::SetPosSrc(TVector3* pVertices, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathSrc);
	m_pPathSrc = NEW CPathSpline;
	if (m_pPathSrc->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathSrc->AddKey((float)pTimes[i], pVertices[i]);
		}
		m_pPathSrc->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathSpline for source position!\n"));
		DELETE_PTR(m_pPathSrc);
	}
    
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Cache
	m_tLight.Position.x = pVertices[0].x;
	m_tLight.Position.y = pVertices[0].y;
	m_tLight.Position.z = pVertices[0].z;
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	SetPosTgt
//	ebp-> look into making params const
//---------------------------------------------------------------------------//
auto CLight3D::SetPosTgt(TVector3* pVertices, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathTgt);
	m_pPathTgt = NEW CPathSpline;
	if (m_pPathTgt->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathTgt->AddKey((float)pTimes[i], pVertices[i]);
		}
		m_pPathTgt->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathSpline for target position!\n"));
		DELETE_PTR(m_pPathTgt);
	}
    
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Cache
	m_tLight.Direction.x = pVertices[0].x - m_tLight.Position.x;
	m_tLight.Direction.y = pVertices[0].y - m_tLight.Position.y;
	m_tLight.Direction.z = pVertices[0].z - m_tLight.Position.z;
#endif//GFX_ENGINE_DIRECT3D
}

//---------------------------------------------------------------------------//
//	SetFalloff
//---------------------------------------------------------------------------//
auto CLight3D::SetFalloff(float* pFalloff, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathFalloff);
	m_pPathFalloff = NEW CPathLinear;
	if (m_pPathFalloff->Init(CPath::PATH_1D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathFalloff->AddKey((float)pTimes[i], pFalloff[i]);
		}
		m_pPathFalloff->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathSpline for Falloff!\n"));
		DELETE_PTR(m_pPathSrc);
	}
    
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Cache
	m_tLight.Falloff = pFalloff[0];
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	GetVars
//---------------------------------------------------------------------------//
auto CLight3D::GetVars() -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CLight3D::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case 0: {
			m_fChangeIntens = *(static_cast<float const*>(pData));
			break;
		}
		case 1: {
			m_fChangeRange = *(static_cast<float const*>(pData));
			break;
		}
		case 2: {
			m_fR = *(static_cast<float const*>(pData));
			break;
		}
		case 3: {
			m_fG = *(static_cast<float const*>(pData));
			break;
		}
		case 4: {
			m_fB = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
	Update();
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CLight3D::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case 0: {
			return &m_fChangeIntens;
		}
		case 1: {
			return &m_fChangeRange;
		}
		case 2: {
			return &m_fR;
		}
		case 3: {
			return &m_fG;
		}
		case 4: {
			return &m_fB;
		}
		default: {
			return nullptr;
		}
	}
}
