//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEFONT_H
#define NEON_GEFONT_H

#include "GEVector.h"

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
// ebp-> Direct3D 9
#include <d3dx9.h>
#endif

class CDisplayDevice;

class CFont {
public:
	CFont();
	~CFont();

	auto Init(std::string const& sFontName, int iHeight, bool bBold, bool bItalic) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Print(CDisplayDevice* pDD, std::string const& Text, TVectorI2 const& vPos, uint uColor, int iAlign) -> void;

private:
	bool m_bOk;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	ID3DXFont* m_pFont;
#endif
};

#endif//NEON_GEFONT_H
