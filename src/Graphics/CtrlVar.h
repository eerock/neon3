//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_CTRLVAR_H
#define NEON_CTRLVAR_H

//---------------------------------------------------------------------------//
//	TCtrlVar
//	Structure that defines a Control Variable, a variable that sits behind
//	a GUI control and can be linked to various controllers.
//	NOTE: if this structure changes, the corresponding changes need to be
//	reflected in the FXDLLs project!!
//---------------------------------------------------------------------------//
struct TCtrlVar {

	enum EType {
		INVALID = -1,
		CHECK_BOX = 0,
		EDIT_BOX,
		COMBO_BOX,
		SLIDER,
		BUTTON,
		MAX_VAR_TYPES,
	};

	static const int SIZE_NAME_STR = 15;
	static const int SIZE_OPTION_STR = 31;
	static const int NUM_OPTIONS_MAX = 16;

	int iType;
	int iNum;
	char szName[SIZE_NAME_STR + 1];
	bool bLinkable;

	// todo: is there any way to set a default var value?
	// problem is there's different data types: checkbox = bool,
	// editbox = string, combobox = int, slider = float, button = ??

	// For ComboBox
	int iNumOptions;
	char aszOptions[NUM_OPTIONS_MAX][SIZE_OPTION_STR + 1];

	static auto GetCtrlVarType(std::string const& sStr) -> int {
		static std::string const s_asStr[] = { "CHECK_BOX", "EDIT_BOX", "COMBO_BOX", "SLIDER" };
		for (int i = 0; i < MAX_VAR_TYPES; ++i) {
			if (Stricmp(s_asStr[i], sStr) == 0) {
				return i;
			}
		}
		return -1;
	}
};

#endif//NEON_CTRLVAR_H
