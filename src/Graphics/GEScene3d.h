//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GESCENE3D_H
#define NEON_GESCENE3D_H

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
// ebp-> Direct3D 9
#include <d3dx9.h>
#endif//GFX_ENGINE_DIRECT3D

class CDisplayDevice;
class CMaterial;
class CObject3D;
class CCamera3D;
class CLight3D;
class CSceneLoader;
struct TCtrlVar;

enum {
	FXS_3DOBJECT = 0,
	FXS_3DLIGHT = 1,
	FXS_3DCAMERA = 2,
	FXS_3DFOG = 3,
};


//---------------------------------------------------------------------------//
class CGEScene3D {

	// ebp-> is there a better way to get classes derived of CSceneLoader to also
	// be friend classes?
	friend class CN3DV1Loader;
	friend class CN3DV2Loader;
	friend class C3DSLoader;

public:
	enum {
		WAVE_TIME = 1 << 0,
		WAVE_XFORM = 1 << 1,
	};

public:
	CGEScene3D();
	~CGEScene3D();

	auto Init(std::string const& sFile) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Reset(float fTime) -> void;

	auto SetTime(float fTime) -> void;
	auto SetCamera(uint iCam) -> void { m_uActiveCamera = iCam; }
	auto GetCamera(uint iCam) -> CCamera3D*;
	auto GetLength() const -> float { return (m_fLastFrame - m_fFirstFrame) / m_fFPS; }

	auto GetVarObjects(std::vector<std::string>& Objects, int iScope) const -> void;
	auto SetVar(int iScope, int iObj, int iVar, void const* pData) -> void;
	auto GetVar(int iScope, int iObj, int iVar) -> void const*;
	auto GetVars(int iScope) const -> TCtrlVar*;

	auto GetActiveCamera() const -> int { return m_uActiveCamera; }
	auto GetNumCameras() const -> int { return m_uNumCameras; }
	auto GetCameraNames() const -> std::vector<std::string> const& { return m_vsCameraNames; }

	auto Draw(CDisplayDevice* pDD) -> void;

	// For Loader
	auto SetMaterials(uint* pMaterials, uint uCount) -> void;
	auto SetObjects(CObject3D* pObjects, uint uCount) -> void;
	auto SetCameras(CCamera3D* pCameras, uint uCount) -> void;
	auto SetLights(CLight3D* pLights, uint uCount) -> void;

private:
	auto PrepareCamera(CDisplayDevice* pDD) -> void;
	auto PrepareLights(CDisplayDevice* pDD) -> void;
	auto PrepareFog(CDisplayDevice* pDD) -> void;
	auto PrepareObjects(CDisplayDevice* pDD) -> void;
	auto DrawObjects(CDisplayDevice* pDD) -> void;

	auto ResetVars() -> void;
	auto ReleaseVars() -> void;

	bool m_bOk;
	float m_fTime;
	uint m_uActiveCamera;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DXMATRIX m_MatrixView;
	D3DXMATRIX m_MatrixProj;
	D3DXMATRIX m_MatrixVP;
#endif

	// Datos de la escena
	float m_fFirstFrame;
	float m_fLastFrame;
	float m_fFrameSpeed;
	float m_fTicksPerFrame;
	float m_fFPS;
	float m_fAmbientR;
	float m_fAmbientG;
	float m_fAmbientB;

	// Lista de componentes
	ushort m_uNumMaterials;
	ushort m_uNumObjects;
	ushort m_uNumCameras;
	ushort m_uNumLights;
	uint* m_pMaterials;
	CObject3D* m_pObjects;
	CCamera3D* m_pCameras;
	CLight3D* m_pLights;

	// For Z ordering objects
	CObject3D** m_ppSortObjects;
	std::vector<std::string> m_vsObjectNames;
	std::vector<std::string> m_vsLightNames;
	std::vector<std::string> m_vsCameraNames;

	// For fog
	bool m_bFog;
	float m_fFogColorR;
	float m_fFogColorG;
	float m_fFogColorB;
	float m_fFogNear;
	float m_fFogFar;
	float m_fFogDensity;
};

#endif//NEON_GESCENE3D_H
