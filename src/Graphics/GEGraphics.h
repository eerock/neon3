//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEGRAPHICS_H
#define NEON_GEGRAPHICS_H

#include "GEDisplayDevice.h"
#include "GEDisplayDevice_OpenGL.h"
#include "GEMaterialManager.h"
#include "GEMaterial.h"
#include "GETexture.h"
#include "GEShader.h"
#include "GETemp.h"
#include "GEFont.h"
#include "GEEngine3D.h"
#include "GEEffects.h"

// ebp-> Global extern CDisplayDevice g_DisplayDevice!
extern CDisplayDevice g_DisplayDevice;

#endif//NEON_GEGRAPHICS_H
