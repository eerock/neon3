//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GESceneLoader.h"

#define N2_N3DFILEV2	(('N' << 8) | ('1' << 0))
#define N2_3DSFILE		(0x4D4D)
#define N3_N3DFILEV1	(0)

//---------------------------------------------------------------------------//
//	STATIC: LoadSelector
//---------------------------------------------------------------------------//
auto CSceneLoader::LoadSelector(CGEScene3D* pScene3D, char const* pData, uint uDataSize) -> bool {
	ushort uVersion = *(reinterpret_cast<ushort const*>(pData));
	switch (uVersion) {
		case N3_N3DFILEV1: {
			CN3DV1Loader loader;
			return loader.Load(pScene3D, pData, uDataSize);
		}
		case N2_N3DFILEV2: {
			CN3DV2Loader loader;
			return loader.Load(pScene3D, pData, uDataSize);
		}
		case N2_3DSFILE: {
			C3DSLoader loader;
			return loader.Load(pScene3D, pData, uDataSize);
		}
		default: {
			return false;
		}
	}
}

