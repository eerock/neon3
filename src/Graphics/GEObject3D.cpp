﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEObject3D.h"
#include "GEEngine3D.h"
#include "GETexture.h"
#include "GEMaterial.h"
#include "GEMaterialManager.h"
#include "CtrlVar.h"
#include "PathLinear.h"

enum {
	RENDER_NORMAL = 0,
	RENDER_ENVMAP = 1,
	RENDER_ENVMAPFLAT  = 2,
	RENDER_POINTSPRITE = 3,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, 0, "RenderMode",     false,  4, { "Normal", "EnvMap", "EnvMapFlat", "PointSprite" } },
	{ TCtrlVar::CHECK_BOX, 1, "WaveXform",      false,  0, { 0 } },
	{ TCtrlVar::COMBO_BOX, 2, "XformChan",      false,  6, { "Global", "1", "2", "3", "4", "5" } },
	{ TCtrlVar::COMBO_BOX, 3, "MorphMode",      false,  5, { "None", "Rotate", "Sin Morph", "Sin Y Morph", "Tentacle" } },
	{ TCtrlVar::SLIDER,    4, "MorphScale",     false,  0, { 0 } },
	{ TCtrlVar::SLIDER,    5, "MorphSpeed",     true,   0, { 0 } },
	{ TCtrlVar::SLIDER,    6, "MorphX",         true,   0, { 0 } },
	{ TCtrlVar::SLIDER,    7, "MorphY",         true,   0, { 0 } },
	{ TCtrlVar::SLIDER,    8, "MorphZ",         true,   0, { 0 } },
	{ TCtrlVar::CHECK_BOX, 9, "Update Normals", false,  0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CObject3D::CObject3D()
	: m_bOk(false)
	, m_uColor(0)
	, m_sName()
	, m_uFlags(0)
	, m_uNumFaces(0)
	, m_uNumUV(0)
	, m_uNumVertices(0)
	, m_bUseUVEnvMap(false)
	, m_bUseUVEnvMapFlat(false)
	, m_pFaces(nullptr)
	, m_pUV(nullptr)
	, m_pPathUV(nullptr)
	, m_pPathVertices(nullptr)
	, m_pVertices(nullptr)
	, m_pFNormals(nullptr)
	, m_pVNormals(nullptr)
	, m_pTangents(nullptr)
	, m_pBinormals(nullptr)
	, m_pPathPositions(nullptr)
	, m_pPathRotations(nullptr)
	, m_pPathScales(nullptr)
	, m_iNumBatches(0)
	, m_pBatches(nullptr)
	, m_fCacheZ(0.f)
	, m_fCacheFrame(0.f)
	, m_pCacheUV(nullptr)
	, m_pCacheUVEnvMap(nullptr)
	, m_pCacheUVEnvMapFlat(nullptr)
	, m_pCacheVertices(nullptr)
	, m_vCachePosition()
	, m_vCacheRotation()
	, m_vCacheScale()
	, m_pCacheFNormals(nullptr)
	, m_pCacheVNormals(nullptr)
	, m_fNumFrames(0.f)
	, m_fTime(0.f)
	, m_iRenderMode(0)
	, m_bWaveXform(false)
	, m_iWaveXformChan(0)
	, m_iMorphMode(0)
	, m_fMorphScale(0.f)
	, m_fMorphSpeed(0.f)
	, m_fMorphX(0.f)
	, m_fMorphY(0.f)
	, m_fMorphZ(0.f)
	, m_bRecalcNormals(false)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CObject3D::Init(float fNumFrames) -> bool {
	ResetVars();

	m_fNumFrames = fNumFrames;
	m_bOk = true;

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CObject3D::~CObject3D() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CObject3D\n"));
#endif
	if (IsOk()) {
		// ebp-> fixit: probably some crashes that might happen here.
		DELETE_ARRAY(m_pFaces);
		DELETE_PTR(m_pUV);
		DELETE_PTR(m_pPathUV);
		DELETE_PTR(m_pPathVertices);
		DELETE_ARRAY(m_pVertices);
		DELETE_ARRAY(m_pFNormals);
		DELETE_ARRAY(m_pVNormals);
		DELETE_ARRAY(m_pTangents);
		DELETE_ARRAY(m_pBinormals);
		DELETE_PTR(m_pPathPositions);
		DELETE_PTR(m_pPathRotations);
		DELETE_PTR(m_pPathScales);
		for (int i = 0; i < m_iNumBatches; i++) {
			DELETE_ARRAY(m_pBatches[i].pFaces);
		}
		DELETE_ARRAY(m_pBatches);
		DELETE_ARRAY(m_pCacheUV);
		DELETE_ARRAY(m_pCacheUVEnvMap);
		DELETE_ARRAY(m_pCacheUVEnvMapFlat);
		DELETE_ARRAY(m_pCacheVertices);
		DELETE_ARRAY(m_pCacheFNormals);
		DELETE_ARRAY(m_pCacheVNormals);
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	SetFrame
//---------------------------------------------------------------------------//
auto CObject3D::SetTime(float fTime) -> void {
	float fOrigTime = fTime;
	// Xform?
	if (m_bWaveXform) {
		float fWave;
		CEngine3D::GetRegisterData(CEngine3D::V_WAVE + m_iWaveXformChan, &fWave);
		fWave = fWave * 1.2f;
		if (fWave > 1.f) {
			fWave = 1.f;
		}
		fTime = fWave * m_fNumFrames;
	}

	// Cacheado
	bool bVrtxChanged = false;
	//if (m_fCacheFrame != fTime)
	{
		bool bChanged = false;
		m_fCacheFrame = fTime;

		// Mesh animation
		if (m_pPathVertices && m_pPathVertices->GetNumKeys() > 1) {
			m_pPathVertices->SetTime(fTime);
			m_pPathVertices->GetKeyValues(m_pVertices, m_pCacheVertices, m_uNumVertices);
			bVrtxChanged = true;
		}

		// UV animation
		if (m_pPathUV && m_pPathUV->GetNumKeys() > 1 && !m_bUseUVEnvMap && !m_bUseUVEnvMapFlat) {
			m_pPathUV->SetTime(fTime);
			m_pPathUV->GetKeyValues(m_pUV, m_pCacheUV, m_uNumUV);
		}

		// Position animation
		if (m_pPathPositions && m_pPathPositions->GetNumKeys() > 1) {
			m_pPathPositions->SetTime(fTime);
			m_pPathPositions->GetKeyValue(m_vCachePosition);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
			D3DXMatrixTranslation(&m_CacheMatrixPos, m_vCachePosition.x, m_vCachePosition.y, m_vCachePosition.z);
#endif//GFX_ENGINE_DIRECT3D
			bChanged = true;
		}

		// Rotation animation
		if (m_pPathRotations && m_pPathRotations->GetNumKeys() > 1) {
			m_pPathRotations->SetTime(fTime);
			m_pPathRotations->GetKeyValue(m_vCacheRotation);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
			D3DXMatrixRotationQuaternion(&m_CacheMatrixRot, (D3DXQUATERNION*)&m_vCacheRotation);
#endif//GFX_ENGINE_DIRECT3D
			bChanged = true;
		}

		// Scale animation
		if (m_pPathScales && m_pPathScales->GetNumKeys() > 1) {
			m_pPathScales->SetTime(fTime);
			m_pPathScales->GetKeyValue(m_vCacheScale);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
			D3DXMatrixScaling(&m_CacheMatrixScale, m_vCacheScale.x, m_vCacheScale.y, m_vCacheScale.z);
#endif//GFX_ENGINE_DIRECT3D
			bChanged = true;
		}

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		// Compute matrix
		if (bChanged) {
			m_CacheMatrixPRS = m_CacheMatrixScale * m_CacheMatrixRot * m_CacheMatrixPos;
		}
#endif//GFX_ENGINE_DIRECT3D
	}

	// Morph
	switch (m_iMorphMode) {
		case 1: {
			ApplyMorphRot(bVrtxChanged, fOrigTime);
			break;
		}
		case 2: {
			ApplyMorphSin(bVrtxChanged, fOrigTime);
			break;
		}
		case 3: {
			ApplyMorphSinY(bVrtxChanged, fOrigTime);
			break;
		}
		case 4: {
			ApplyMorphTent(bVrtxChanged, fOrigTime);
			break;
		}
		default: {
			break;
		}
	}

	// Update normals?
	if (m_bRecalcNormals) {
		CalculateFNormals();
		CalculateVNormals();
	}
}


//---------------------------------------------------------------------------//
//	CalculateZ
//---------------------------------------------------------------------------//
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
auto CObject3D::CalculateZ(D3DXMATRIX const& matProj) -> void {
	D3DXMATRIX matWVP = m_CacheMatrixPRS * matProj;
	TVector3 vSrc;
	vSrc.x = vSrc.y = vSrc.z = 0.f;
	m_fCacheZ = matWVP.m[0][2] * vSrc.x +
		matWVP.m[1][2] * vSrc.y +
		matWVP.m[2][2] * vSrc.z +
		matWVP.m[3][2];

	//m_fCacheZ = m_CacheMatrixPRS.m[0][2] * vSrc.x +
	//	m_CacheMatrixPRS.m[1][2] * vSrc.y +
	//	m_CacheMatrixPRS.m[2][2] * vSrc.z +
	//	m_CacheMatrixPRS.m[3][2];

	/*
	pD3D->SetTransform(D3DTS_WORLD, &m_CacheMatrixPRS);

	TVertex_HCV_XYZ_C* pVertices = (TVertex_HCV_XYZ_C*)pDD->LockVertexBuffer(HCV_XYZ_C, 1);
	HCV_SET_XYZ(pVertices, 0.f, 0.f, 0.f);
	pDD->UnlockVertexBuffer(HCV_XYZ_C);
	pDD->ProcessVertices(HCV_XYZ_C, 1);

	TVertex_HCV_XYZ* pOut = (TVertex_HCV_XYZ*)pDD->LockVertexBuffer(HCV_XYZ, 1);
	m_fCacheZ = pOut->fZ;
	pDD->UnlockVertexBuffer(HCV_XYZ);
	*/
}
#endif//GFX_ENGINE_DIRECT3D


//---------------------------------------------------------------------------//
//	CalculateFNormals
//---------------------------------------------------------------------------//
auto CObject3D::CalculateFNormals() -> void {
	TFace* pTri = nullptr;
	TVector3* pVrtx = nullptr;
	TVector3* pNTri = nullptr;
	TVector3* v1 = nullptr;
	TVector3* v2 = nullptr;
	TVector3* v3 = nullptr;

	//-- Calcular normales de las caras --//
	pTri = m_pFaces;
	pNTri = m_pFNormals;
	pVrtx = m_pCacheVertices;
	for (int i = 0; i < m_uNumFaces; i++, pNTri++) {
		v1 = &pVrtx[m_pFaces[i].i0];
		v2 = &pVrtx[m_pFaces[i].i1];
		v3 = &pVrtx[m_pFaces[i].i2];
		Vector_Normal(pNTri, v1, v3, v2); // ebp-> note: swap y/z
	}
	memcpy(m_pCacheFNormals, m_pFNormals, m_uNumFaces * sizeof(TVector3));
}


//---------------------------------------------------------------------------//
//	CalculateVNormals
//---------------------------------------------------------------------------//
auto CObject3D::CalculateVNormals() -> void {
	int i, k, t1, t2, t3;
	int incident;
	TFace* pTri = nullptr;
	TVector3* pVrtx = nullptr;
	TVector3* pNVrtx = nullptr;
	TVector3* pNTri = nullptr;
	static TVector3 pUsed[65000];

	pVrtx = m_pCacheVertices;

	//pUsed = NEW_ARRAY(TVector3, m_uNumVertices);
	//if (!pUsed) {
	//	GLOG(("ERROR: Couldn't instantiate normals buffer!\n"));
	//	return;
	//}
	//-- Calcular normales de los vertices --//
	pNVrtx = m_pVNormals;
	for (k = 0; k < m_uNumVertices; ++k, ++pNVrtx, ++pVrtx) {
		pNVrtx->x = 0.f;
		pNVrtx->y = 0.f;
		pNVrtx->z = 0.f;
		incident = 0;
		pTri = m_pFaces;
		pNTri = m_pFNormals;
		for (i = 0; i < m_uNumFaces; i++, ++pNTri) {
			t1 = m_pFaces[i].i0;
			t2 = m_pFaces[i].i1;
			t3 = m_pFaces[i].i2;
			if ((t1 == k) || (t2 == k) || (t3 == k)) {
				//---------------------------------------------------------
				/*
				bool usable = 1;
				for (uint u = 0; u < incident; u++) {
					if ((fabs (pNTri->x - pUsed[u].x) < 0.1f) &&
						(fabs (pNTri->y - pUsed[u].y) < 0.1f) &&
						(fabs (pNTri->z - pUsed[u].z) < 0.1f)) {
						usable = 0;
						break;
					}
				}
				if (usable)
				*/
				{
					//pUsed[incident].x = pNTri->x;
					//pUsed[incident].y = pNTri->y;
					//pUsed[incident].z = pNTri->z;
					pNVrtx->x += pNTri->x;
					pNVrtx->y += pNTri->y;
					pNVrtx->z += pNTri->z;
					incident++;
				}
			}
		}
		float d = 1.f / incident;
		pNVrtx->x *= d;
		pNVrtx->y *= d;
		pNVrtx->z *= d;
		Vector_Unit(pNVrtx, pNVrtx);
	}
	//DELETE_ARRAY(pUsed);
	memcpy(m_pCacheVNormals, m_pVNormals, m_uNumVertices * sizeof(TVector3));
}


//---------------------------------------------------------------------------//
//	CalculateTangents
//---------------------------------------------------------------------------//
auto CObject3D::CalculateTangents() -> void {
	TVector3* pTan1 = NEW_ARRAY(TVector3, m_uNumVertices);
	//TVector3* pTan2 = pTan1 + m_uNumVertices;
	memset(pTan1, 0, m_uNumVertices * sizeof(TVector3));

	for (int i = 0; i < m_uNumFaces; ++i) {
		int i1 = m_pFaces[i].i0;
		int i2 = m_pFaces[i].i1;
		int i3 = m_pFaces[i].i2;

		TVector3 const& v1 = m_pVertices[i1];
		TVector3 const& v2 = m_pVertices[i2];
		TVector3 const& v3 = m_pVertices[i3];

		TVector2 const& w1 = m_pCacheUV[i * 3 + 0];
		TVector2 const& w2 = m_pCacheUV[i * 3 + 1];
		TVector2 const& w3 = m_pCacheUV[i * 3 + 2];

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;

		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;

		float r = 1.0f / (s1 * t2 - s2 * t1);
		TVector3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
		//TVector3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

		pTan1[i1].x += sdir.x;	pTan1[i1].y += sdir.y;	pTan1[i1].z += sdir.z;
		pTan1[i2].x += sdir.x;	pTan1[i2].y += sdir.y;	pTan1[i2].z += sdir.z;
		pTan1[i3].x += sdir.x;	pTan1[i3].y += sdir.y;	pTan1[i3].z += sdir.z;
		/*
		pTan2[i1].x += tdir.x;	pTan2[i1].y += tdir.y;	pTan2[i1].z += tdir.z;
		pTan2[i2].x += tdir.x;	pTan2[i2].y += tdir.y;	pTan2[i2].z += tdir.z;
		pTan2[i3].x += tdir.x;	pTan2[i3].y += tdir.y;	pTan2[i3].z += tdir.z;
		*/
	}

	for (int i = 0; i < m_uNumVertices; ++i) {
		TVector3 n = m_pVNormals[i];
		TVector3 t = pTan1[i];

		TVector3 r;
		float fDot = Vector_Dot(&n, &t);
		r.x = t.x - n.x * fDot;
		r.y = t.y - n.y * fDot;
		r.z = t.z - n.z * fDot;
		Vector_Unit(&m_pTangents[i], &r);
		Vector_Cross(&m_pBinormals[i], &n, &m_pTangents[i]);
	}

	DELETE_ARRAY(pTan1);
}


//---------------------------------------------------------------------------//
//	CalculateUV
//---------------------------------------------------------------------------//
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
auto CObject3D::CalculateUV(D3DXMATRIX const& matProj) -> void {
	if ((m_iRenderMode == RENDER_ENVMAP) || m_bUseUVEnvMap) {
		TVector2* pUV = NEW_ARRAY(TVector2, m_uNumVertices);
		D3DXMATRIX matWVP = m_CacheMatrixRot * matProj;
		for (int i = 0; i < m_uNumVertices; i++) {
			TVector3 vSrc = m_pCacheVNormals[i];
			pUV[i].x = matWVP.m[0][0] * vSrc.x +
					matWVP.m[1][0] * vSrc.y +
					matWVP.m[2][0] * vSrc.z;
			pUV[i].y = matWVP.m[0][1] * vSrc.x +
					matWVP.m[1][1] * vSrc.y +
					matWVP.m[2][1] * vSrc.z;
			pUV[i].x = pUV[i].x * 0.5f + 0.5f;
			pUV[i].y = pUV[i].y * 0.5f + 0.5f;
		}

		for (int i = 0; i < m_uNumFaces; i++) {
			m_pCacheUVEnvMap[i * 3 + 0] = pUV[m_pFaces[i].i0];
			m_pCacheUVEnvMap[i * 3 + 1] = pUV[m_pFaces[i].i1];
			m_pCacheUVEnvMap[i * 3 + 2] = pUV[m_pFaces[i].i2];
		}
		DELETE_ARRAY(pUV);
	}

	if ((m_iRenderMode == RENDER_ENVMAPFLAT) || m_bUseUVEnvMapFlat) {
		D3DXMATRIX matWVP = m_CacheMatrixRot * matProj;
		for (int i = 0; i < m_uNumFaces; i++) {
			TVector3 vSrc = m_pCacheFNormals[i];
			TVector2 uv;
			uv.x = matWVP.m[0][0] * vSrc.x +
					matWVP.m[1][0] * vSrc.y +
					matWVP.m[2][0] * vSrc.z;
			uv.y = matWVP.m[0][1] * vSrc.x +
					matWVP.m[1][1] * vSrc.y +
					matWVP.m[2][1] * vSrc.z;
			uv.x = uv.x * 0.5f + 0.5f;
			uv.y = uv.y * 0.5f + 0.5f;
			m_pCacheUVEnvMapFlat[i * 3 + 0] = uv;
			m_pCacheUVEnvMapFlat[i * 3 + 1] = uv;
			m_pCacheUVEnvMapFlat[i * 3 + 2] = uv;
		}
	}
}
#endif//GFX_ENGINE_DIRECT3D


//---------------------------------------------------------------------------//
//	PrepareBatches
//---------------------------------------------------------------------------//
auto CObject3D::PrepareBatches(uint* pMaterialIDs, int iCount) -> void {
	m_iNumBatches = iCount;
	// ebp-> match up these new-array's to their delete-array's
	m_pBatches = NEW_ARRAY(TFaceBatch, iCount);

	for (int i = 0; i < iCount; ++i) {
		m_pBatches[i].uMaterialID = CMaterialManager::INVALID;
		m_pBatches[i].uNumFaces = 0;
		m_pBatches[i].pFaces = nullptr;
		int uNumFaces = 0;
		for (ushort j = 0; j < m_uNumFaces; ++j) {
			if (m_pFaces[j].iMat == i) {
				++uNumFaces;
			}
		}
		// Hay faces, guardar el batch
		if (uNumFaces > 0) {
			m_pBatches[i].uMaterialID = pMaterialIDs[i];
			m_pBatches[i].uNumFaces = uNumFaces;
			m_pBatches[i].pFaces = NEW_ARRAY(int, uNumFaces);

			int iFace = 0;
			for (ushort j = 0; j < m_uNumFaces; ++j) {
				if (m_pFaces[j].iMat == i) {
					m_pBatches[i].pFaces[iFace++] = j;
				}
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	ApplyMorphRot
//---------------------------------------------------------------------------//
auto CObject3D::ApplyMorphRot(bool bChanged, float fTime) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> todo: there might be a bug with this change
	//float fMorphTime = fTime * m_fMorphSpeed * 1.f / 200.f;
	float fMorphTime = fTime * m_fMorphSpeed;

	TVector3* pVerticesSrc = bChanged ? m_pCacheVertices : m_pVertices;
	TVector3* pVerticesDst = m_pCacheVertices;

	for (int i = 0; i < m_uNumVertices; ++i) {
		float fVTime = fMorphTime;
		TVector4 vRot;
		vRot.x = sinf(fVTime * 0.92f) * m_fMorphX * 3.f + cosf(fVTime * 0.64f) * m_fMorphX * 3.f;
		vRot.y = cosf(fVTime * 1.44f) * m_fMorphY * 3.f + sinf(fVTime * 0.82f) * m_fMorphY * 3.f;
		vRot.z = sinf(fVTime * 0.94f) * m_fMorphZ * 3.f + cosf(fVTime * 1.22f) * m_fMorphZ * 3.f;
		vRot.x *= m_fMorphScale;
		vRot.y *= m_fMorphScale;
		vRot.z *= m_fMorphScale;
		vRot.w = 1.f;

		Vector_Unit(&vRot, &vRot);
		D3DXMATRIX Matrix;
		D3DXMatrixRotationQuaternion(&Matrix, (D3DXQUATERNION*)&vRot);

		D3DXVec3TransformCoord((D3DXVECTOR3*)pVerticesDst, (D3DXVECTOR3*)pVerticesSrc, &Matrix);
		D3DXVec3TransformCoord((D3DXVECTOR3*)&m_pCacheFNormals[i], (D3DXVECTOR3*)&m_pFNormals[i], &Matrix);
		D3DXVec3TransformCoord((D3DXVECTOR3*)&m_pCacheVNormals[i], (D3DXVECTOR3*)&m_pVNormals[i], &Matrix);
		pVerticesSrc++;
		pVerticesDst++;
	}
#else
	UNREFERENCED_PARAMETER(bChanged);
	UNREFERENCED_PARAMETER(fTime);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	ApplyMorphSin
//	Morph sinusoidal en los 3 ejes
//---------------------------------------------------------------------------//
auto CObject3D::ApplyMorphSin(bool bChanged, float fTime) -> void {
	// ebp-> todo: there might be a bug with this change.
	//float fMorphTime = fTime * m_fMorphSpeed * 1.f / 200.f;
	float fMorphTime = fTime * m_fMorphSpeed;

	TVector3* pVerticesSrc = bChanged ? m_pCacheVertices : m_pVertices;
	TVector3* pVerticesDst = m_pCacheVertices;
	for (int i = 0; i < m_uNumVertices; ++i) {
		float x = pVerticesSrc->x;
		float y = pVerticesSrc->y;
		float z = pVerticesSrc->z;
		pVerticesDst->x = x * ((sinf(z * 0.087f * m_fMorphScale * 2.f + fMorphTime * 0.35f)) * m_fMorphX * 0.5f + 1.f);
		pVerticesDst->y = y * ((cosf(z * 0.065f * m_fMorphScale * 2.f + fMorphTime * 0.46f)) * m_fMorphY * 0.5f + 1.f);
		pVerticesDst->z = z * ((cosf(x * 0.077f * m_fMorphScale * 2.f + fMorphTime * 0.25f)) * m_fMorphZ * 0.2f + 
			(sinf(y * 0.096f * m_fMorphScale * 2.f + fMorphTime * 0.57f)) * m_fMorphZ * 0.3f + 1.f);
		pVerticesSrc++;
		pVerticesDst++;
	}
}


//---------------------------------------------------------------------------//
//	RotateYScale
//	MorphY sinusoidal (solo en eje Y)
//---------------------------------------------------------------------------//
static auto RotateYScale(float fAng, float fScale, TVector3 const* pPos, TVector3* pDst) -> void {
	float x = pPos->x;
	float z = pPos->z;
	float fS = sinf(fAng);
	float fC = cosf(fAng);
	pDst->x = (x * fC - z * fS) * fScale;
	pDst->z = (z * fC + x * fS) * fScale;
}


//---------------------------------------------------------------------------//
//	RotateY
//---------------------------------------------------------------------------//
static auto RotateY(float fAng, TVector3 const* pPos, TVector3* pDst) -> void {
	float x = pPos->x;
	float z = pPos->z;
	float fS = sinf(fAng);
	float fC = cosf(fAng);
	pDst->x = (x * fC - z * fS);
	pDst->z = (z * fC + x * fS);
}


//---------------------------------------------------------------------------//
//	ApplyMorphSinY
//---------------------------------------------------------------------------//
auto CObject3D::ApplyMorphSinY(bool bChanged, float fTime) -> void {
	// ebp-> todo: there might be a bug with this change
	//float fMorphTime = fTime * m_fMorphSpeed * 1.f / 200.f;
	float fMorphTime = fTime * m_fMorphSpeed;

	TVector3* pVerticesSrc = bChanged ? m_pCacheVertices : m_pVertices;
	TVector3* pVerticesDst = m_pCacheVertices;
	for (int i = 0; i < m_uNumVertices; ++i) {
		float fAng = sinf(fMorphTime * 0.21f + pVerticesSrc->y * 0.005f * m_fMorphScale * 3.f) * m_fMorphY * 4.f +
			cosf(fMorphTime * 0.13f + pVerticesSrc->y * 0.007f * m_fMorphScale * 3.f) * m_fMorphZ * 4.f;
		RotateYScale(fAng,
			cosf(fMorphTime * 0.43f + pVerticesSrc->y * 0.0378f * m_fMorphScale * 3.f) * m_fMorphX * 0.25f + 
			sinf(fMorphTime * 0.37f + pVerticesSrc->y * 0.0413f * m_fMorphScale * 3.f) * m_fMorphX * 0.25f + 1.f,
			pVerticesSrc, pVerticesDst);
		RotateY(fAng, &m_pFNormals[i], &m_pCacheFNormals[i]);
		RotateY(fAng, &m_pVNormals[i], &m_pCacheVNormals[i]);
		++pVerticesSrc;
		++pVerticesDst;
	}
}


//---------------------------------------------------------------------------//
//	ApplyMorphTent
//	Morph Tent (Morph para el tentaculo de la Sound Pressure)
//---------------------------------------------------------------------------//
auto CObject3D::ApplyMorphTent(bool bChanged, float fTime) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> todo: there might be a bug with this change.
	//float fMorphTime = fTime * m_fMorphSpeed * 1.f / 200.f;
	float fMorphTime = fTime * m_fMorphSpeed;

	TVector3* pVerticesSrc = bChanged ? m_pCacheVertices : m_pVertices;
	TVector3* pVerticesDst = m_pCacheVertices;

	for (int i = 0; i < m_uNumVertices; ++i) {
		float fX = pVerticesSrc->x - m_vCachePosition.x;
		float fY = pVerticesSrc->y - m_vCachePosition.y;
		float fZ = pVerticesSrc->z - m_vCachePosition.z;
		float fVTime = fMorphTime - sqrtf(fX * fX + fY * fY + fZ * fZ) * m_fMorphScale * 0.05f;
		TVector4 vRot;
		vRot.x = sinf(fVTime * 0.52f) * m_fMorphX * 0.57f;
		vRot.y = cosf(fVTime * 1.44f) * m_fMorphY * 0.34f;
		vRot.z = sinf(fVTime * 0.94f) * m_fMorphZ * 0.78f;
		vRot.w = 1.f;

		Vector_Unit(&vRot, &vRot);
		D3DXMATRIX Matrix;
		D3DXMatrixRotationQuaternion(&Matrix, (D3DXQUATERNION*)&vRot);
		D3DXVec3TransformCoord((D3DXVECTOR3*)pVerticesDst, (D3DXVECTOR3*)pVerticesSrc, &Matrix);
		D3DXVec3TransformCoord((D3DXVECTOR3*)&m_pCacheFNormals[i], (D3DXVECTOR3*)&m_pFNormals[i], &Matrix);
		D3DXVec3TransformCoord((D3DXVECTOR3*)&m_pCacheVNormals[i], (D3DXVECTOR3*)&m_pVNormals[i], &Matrix);
		++pVerticesSrc;
		++pVerticesDst;
	}
#else
	UNREFERENCED_PARAMETER(bChanged);
	UNREFERENCED_PARAMETER(fTime);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	ResetVars
//---------------------------------------------------------------------------//
auto CObject3D::ResetVars() -> void {
	ASSERT(m_bOk == false);
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_uColor = HARD_COLOR_ARGB(255, 255, 255, 255);
#endif//GFX_ENGINE_DIRECT3D
	m_sName.clear();
	m_uFlags = 0;
	m_uNumFaces = 0;
	m_uNumUV = 0;
	m_uNumVertices = 0;
	m_bUseUVEnvMap = false;
	m_bUseUVEnvMapFlat = false;
	m_pFaces = nullptr;
	m_pUV = nullptr;
	m_pPathUV = nullptr;
	m_pPathVertices = nullptr;
	m_pVertices = nullptr;
	m_pFNormals = nullptr;
	m_pVNormals = nullptr;
	m_pTangents = nullptr;
	m_pBinormals = nullptr;
	m_pPathPositions = nullptr;
	m_pPathRotations = nullptr;
	m_pPathScales = nullptr;
	m_iNumBatches = 0;
	m_pBatches = nullptr;
	m_fCacheZ = 0.f;
	m_fCacheFrame = 0.f;
	m_pCacheUV = nullptr;
	m_pCacheUVEnvMap = nullptr;
	m_pCacheUVEnvMapFlat = nullptr;
	m_pCacheVertices = nullptr;
	//
	m_pCacheFNormals = nullptr;
	m_pCacheVNormals = nullptr;
	//
	m_fNumFrames = 0.f;
	m_fTime = 0.f;
	m_iRenderMode = RENDER_NORMAL;
	m_bWaveXform = false;
	m_iWaveXformChan = 0;
	m_iMorphMode = 0;
	m_fMorphScale = 1.f;
	m_fMorphSpeed = 0.5f;
	m_fMorphX = 0.25f;
	m_fMorphY = 0.25f;
	m_fMorphZ = 0.25f;
	m_bRecalcNormals = false;
}


//---------------------------------------------------------------------------//
//	FillVertex
//---------------------------------------------------------------------------//
auto CObject3D::FillVertex(TVertex_HCV_XYZ_F* pVertices, CMaterial* pMat, int i, int uv, uint* pFlags) -> void {
	UNREFERENCED_PARAMETER(pFlags);
	TVector3 const* pSrc = GetVertices();
	TVector3 const* pNor = GetVNormals();
	//TVector3 const* pFN = GetFNormales();
	TVector2 const* pUVNormal = GetUV();
	TVector2 const* pUVEnv = GetUVEnvMap();
	TVector2 const* pUVEnvFlat = GetUVEnvMapFlat();
	TVector2 const* pUV = nullptr;

	bool bEnvMap = (m_iRenderMode == RENDER_ENVMAP);
	bool bEnvMapFlat = (m_iRenderMode == RENDER_ENVMAPFLAT);

	HCV_SET_COLOR(pVertices, GetColor());
	HCV_SET_XYZ(pVertices, pSrc[i].x, pSrc[i].y, pSrc[i].z);

	// Tex 0
	pUV = pUVNormal;
	if (bEnvMap) {
		pUV = pUVEnv;
	} else if (bEnvMapFlat) {
		pUV = pUVEnvFlat;
	}
	HCV_SET_UV0(pVertices, pUV[uv].x, pUV[uv].y);

	// Tex 1
	if (pMat->GetNumTextures() > 1) {
		pUV = pUVNormal;
		if (bEnvMap) {
			pUV = pUVEnv;
		} else if (bEnvMapFlat) {
			pUV = pUVEnvFlat;
		}
		HCV_SET_UV1(pVertices, pUV[uv].x, pUV[uv].y);
	}

	// Normal
	HCV_SET_NORMAL(pVertices, pNor[i].x, pNor[i].y, pNor[i].z);
}


//---------------------------------------------------------------------------//
//	FillVertex
//---------------------------------------------------------------------------//
auto CObject3D::FillVertex(TVertex_HCV_XYZ_F* pVertices, int i0) -> void {
	TVector3 const* pSrc = GetVertices();
	TVector3 const* pNor = GetVNormals();

	HCV_SET_COLOR(pVertices, GetColor());
	HCV_SET_XYZ(pVertices, pSrc[i0].x, pSrc[i0].y, pSrc[i0].z);
	HCV_SET_NORMAL(pVertices, pNor[i0].x, pNor[i0].y, pNor[i0].z);
}


//---------------------------------------------------------------------------//
//	FillVertex
//---------------------------------------------------------------------------//
auto CObject3D::FillVertex(TVertex_HCV_XYZ_P* pVertices, CMaterial* pMat, int i, int uv, uint* pFlags) -> void {
	UNREFERENCED_PARAMETER(pFlags);
	TVector3 const* pSrc = GetVertices();
	TVector3 const* pNor = GetVNormals();
	//TVector3 const* pFN = GetFNormales();
	TVector2 const* pUVNormal = GetUV();
	TVector2 const* pUVEnv = GetUVEnvMap();
	TVector2 const* pUVEnvFlat = GetUVEnvMapFlat();
	TVector2 const* pUV = nullptr;

	bool bEnvMap = (m_iRenderMode == RENDER_ENVMAP);
	bool bEnvMapFlat = (m_iRenderMode == RENDER_ENVMAPFLAT);

	HCV_SET_COLOR(pVertices, GetColor());
	HCV_SET_XYZ(pVertices, pSrc[i].x, pSrc[i].y, pSrc[i].z);
	// Tex 0
	pUV = pUVNormal;
	if (bEnvMap) {
		pUV = pUVEnv;
	} else if (bEnvMapFlat) {
		pUV = pUVEnvFlat;
	}
	HCV_SET_UV0(pVertices, pUV[uv].x, pUV[uv].y);

	// Tex 1
	if (pMat->GetNumTextures() > 1) {
		pUV = pUVNormal;
		if (bEnvMap) {
			pUV = pUVEnv;
		} else if (bEnvMapFlat) {
			pUV = pUVEnvFlat;
		}
		HCV_SET_UV1(pVertices, pUV[uv].x, pUV[uv].y);
	}

	// Tex 2
	if (pMat->GetNumTextures() > 2) {
		pUV = pUVNormal;
		if (bEnvMap) {
			pUV = pUVEnv;
		} else if (bEnvMapFlat) {
			pUV = pUVEnvFlat;
		}
		HCV_SET_UV2(pVertices, pUV[uv].x, pUV[uv].y);
	}

	// Tex 3
	if (pMat->GetNumTextures() > 3) {
		pUV = pUVNormal;
		if (bEnvMap) {
			pUV = pUVEnv;
		} else if (bEnvMapFlat) {
			pUV = pUVEnvFlat;
		}
		HCV_SET_UV3(pVertices, pUV[uv].x, pUV[uv].y);
	}

	// Normal, Tangent, Binormal
	HCV_SET_NORMAL(pVertices, pNor[i].x, pNor[i].y, pNor[i].z);
	HCV_SET_TANGENT(pVertices, GetTangents()[i].x, GetTangents()[i].y, GetTangents()[i].z, 1);
	HCV_SET_BINORMAL(pVertices, GetBinormals()[i].x, GetBinormals()[i].y, GetBinormals()[i].z, 1);
}


//---------------------------------------------------------------------------//
//	FillVertex
//---------------------------------------------------------------------------//
auto CObject3D::FillVertex(TVertex_HCV_XYZ_P* pVertices, int i0) -> void {
	TVector3 const* pSrc = GetVertices();
	TVector3 const* pNor = GetVNormals();

	HCV_SET_COLOR(pVertices, GetColor());
	HCV_SET_XYZ(pVertices, pSrc[i0].x, pSrc[i0].y, pSrc[i0].z);
	HCV_SET_NORMAL(pVertices, pNor[i0].x, pNor[i0].y, pNor[i0].z);
	HCV_SET_TANGENT(pVertices, GetTangents()[i0].x, GetTangents()[i0].y, GetTangents()[i0].z, 1);
	HCV_SET_BINORMAL(pVertices, GetBinormals()[i0].x, GetBinormals()[i0].y, GetBinormals()[i0].z, 1);
}

//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CObject3D::Draw(CDisplayDevice* pDD) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DDEVICE* pD3D = pDD->GetD3DDevice();
	pD3D->SetTransform(D3DTS_WORLD, &m_CacheMatrixPRS);
	D3DXMATRIX matVP, matWVP;
	CEngine3D::GetRegisterData(CEngine3D::V_VIEWPROJECTION, (float*)&matVP);
	matWVP = m_CacheMatrixPRS * matVP;
	CEngine3D::SetRegisterData(CEngine3D::V_WORLD, (float*)&m_CacheMatrixPRS);
	CEngine3D::SetRegisterData(CEngine3D::V_WORLDVIEWPROJECTION, (float*)&matWVP);

	IMaterialManagerService* pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	for (int iBatch = 0; iBatch < m_iNumBatches; ++iBatch) {
		CMaterial* pMat = pMatMgr->GetMaterial(m_pBatches[iBatch].uMaterialID);
		// Numero de caras a pintar
		int iNumFaces = m_pBatches[iBatch].uNumFaces;
		if (pMat && iNumFaces > 0) {
			bool bFixed = pMat->FixedPipeline();
			// Calcular los flags para el envmap
			uint pFlags[MAX_TEXTURES];
			for (int f = 0; f < MAX_TEXTURES; ++f) {
				if (pMat->GetTextureID(f) != CMaterialManager::INVALID) {
					pFlags[f] = pMatMgr->GetTexture(pMat->GetTextureID(f))->GetFlags();
				} else {
					pFlags[f] = 0;
				}
			}

			if (m_iRenderMode != RENDER_POINTSPRITE) {
				if (GetUV()) {
					// Vertices
					int iHCV;
					TVertex_HCV_XYZ_F* pVerticesF = nullptr;
					TVertex_HCV_XYZ_P* pVerticesP = nullptr;
					if (bFixed) {
						iHCV = HCV_XYZ_F;
						pVerticesF = (TVertex_HCV_XYZ_F*)pDD->LockVertexBuffer(HCV_XYZ_F, iNumFaces * 3);
					} else {
						iHCV = HCV_XYZ_P;
						pVerticesP = (TVertex_HCV_XYZ_P*)pDD->LockVertexBuffer(HCV_XYZ_P, iNumFaces * 3);
					}
					//TVertex_HCV_XYZ* pVertices = (TVertex_HCV_XYZ*)pDD->LockVertexBuffer(HCV_XYZ, iNumFaces * 3);
					for (int i = 0; i < iNumFaces; ++i) {
						int iFace = m_pBatches[iBatch].pFaces[i];
						int i0 = m_pFaces[iFace].i0;
						int i1 = m_pFaces[iFace].i1;
						int i2 = m_pFaces[iFace].i2;
						int uv = iFace * 3;
						if (bFixed) {
							ASSERT(pVerticesF);
							FillVertex(pVerticesF++, pMat, i0, uv, pFlags);
							FillVertex(pVerticesF++, pMat, i1, uv + 1, pFlags);
							FillVertex(pVerticesF++, pMat, i2, uv + 2, pFlags);
						} else {
							ASSERT(pVerticesP);
							FillVertex(pVerticesP++, pMat, i0, uv, pFlags);
							FillVertex(pVerticesP++, pMat, i1, uv + 1, pFlags);
							FillVertex(pVerticesP++, pMat, i2, uv + 2, pFlags);
						}
					}
					pDD->UnlockVertexBuffer(iHCV);
					// Material
					uint uPasses = pMat->BeginDraw(0);
					for (uint uPass = 0; uPass < uPasses; uPass++) {
						pMat->BeginPass(uPass);
						pDD->DrawPrimitive(iHCV, HARD_PRIM_TRIANGLELIST, iNumFaces);
						pMat->EndPass();
					}
					pMat->EndDraw();
				} else {
					// Vertices
					int iHCV;
					TVertex_HCV_XYZ_F* pVerticesF = nullptr;
					TVertex_HCV_XYZ_P* pVerticesP = nullptr;
					if (bFixed) {
						iHCV = HCV_XYZ_F;
						pVerticesF = (TVertex_HCV_XYZ_F*)pDD->LockVertexBuffer(HCV_XYZ_F, iNumFaces * 3);
					} else {
						iHCV = HCV_XYZ_P;
						pVerticesP = (TVertex_HCV_XYZ_P*)pDD->LockVertexBuffer(HCV_XYZ_P, iNumFaces * 3);
					}
					for (int i = 0; i < iNumFaces; ++i) {
						int iFace = m_pBatches[iBatch].pFaces[i];
						int i0 = m_pFaces[iFace].i0;
						int i1 = m_pFaces[iFace].i1;
						int i2 = m_pFaces[iFace].i2;
						if (bFixed) {
							ASSERT(pVerticesF);
							FillVertex(pVerticesF++, i0);
							FillVertex(pVerticesF++, i1);
							FillVertex(pVerticesF++, i2);
						} else {
							ASSERT(pVerticesP);
							FillVertex(pVerticesP++, i0);
							FillVertex(pVerticesP++, i1);
							FillVertex(pVerticesP++, i2);
						}
					}
					pDD->UnlockVertexBuffer(iHCV);
					// Material
					uint uPasses = pMat->BeginDraw(0);
					for (uint uPass = 0; uPass < uPasses; uPass++) {
						pMat->BeginPass(uPass);
						pDD->DrawPrimitive(iHCV, HARD_PRIM_TRIANGLELIST, iNumFaces);
						pMat->EndPass();
					}
					pMat->EndDraw();
				}
			} else {
				// POINTSPRITE
				pD3D->SetRenderState(D3DRS_POINTSPRITEENABLE, TRUE);
				pD3D->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);
				float fValue = 64.f;
				pD3D->SetRenderState(D3DRS_POINTSIZE, *((DWORD*)&fValue));
				fValue = 1.f;
				pD3D->SetRenderState(D3DRS_POINTSIZE_MIN, *((DWORD*)&fValue));
				fValue = 0.f;
				pD3D->SetRenderState(D3DRS_POINTSCALE_A, *((DWORD*)&fValue));
				fValue = 0.f;
				pD3D->SetRenderState(D3DRS_POINTSCALE_B, *((DWORD*)&fValue));
				fValue = 1.f;
				pD3D->SetRenderState(D3DRS_POINTSCALE_C, *((DWORD*)&fValue));
				pD3D->SetRenderState(D3DRS_ZENABLE, FALSE);

				int iNumVertices = m_uNumVertices;
				// Vertices
				int iHCV;
				TVertex_HCV_XYZ_F* pVerticesF = nullptr;
				TVertex_HCV_XYZ_P* pVerticesP = nullptr;
				if (bFixed) {
					iHCV = HCV_XYZ_F;
					pVerticesF = (TVertex_HCV_XYZ_F*)pDD->LockVertexBuffer(HCV_XYZ_F, iNumVertices);
				} else {
					iHCV = HCV_XYZ_P;
					pVerticesP = (TVertex_HCV_XYZ_P*)pDD->LockVertexBuffer(HCV_XYZ_P, iNumVertices);
				}
				//TVertex_HCV_XYZ* pVertices = (TVertex_HCV_XYZ*)pDD->LockVertexBuffer(HCV_XYZ, iNumVertices);
				for (int i = 0; i < iNumVertices; ++i) {
					if (bFixed) {
						ASSERT(pVerticesF);
						FillVertex(pVerticesF++, i);
					} else {
						ASSERT(pVerticesP);
						FillVertex(pVerticesP++, i);
					}
				}
				pDD->UnlockVertexBuffer(iHCV);
				// Material
				uint uPasses = pMat->BeginDraw(0);
				for (uint uPass = 0; uPass < uPasses; ++uPass) {
					pMat->BeginPass(uPass);
					pDD->DrawPrimitive(iHCV, HARD_PRIM_POINTLIST, iNumVertices);
					pMat->EndPass();
				}
				pMat->EndDraw();
			}
		}
	}
#else
	UNREFERENCED_PARAMETER(pDD);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	GetVars
//---------------------------------------------------------------------------//
auto CObject3D::GetVars() -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CObject3D::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case 0: {
			m_iRenderMode = *(static_cast<int const*>(pData));
			break;
		}
		case 1: {
			m_bWaveXform = *(static_cast<bool const*>(pData));
			break;
		}
		case 2: {
			m_iWaveXformChan = *(static_cast<int const*>(pData));
			break;
		}
		case 3: {
			m_iMorphMode = *(static_cast<int const*>(pData));
			break;
		}
		case 4: {
			m_fMorphScale = *(static_cast<float const*>(pData));
			break;
		}
		case 5: {
			m_fMorphSpeed = *(static_cast<float const*>(pData));
			break;
		}
		case 6: {
			m_fMorphX = *(static_cast<float const*>(pData));
			break;
		}
		case 7: {
			m_fMorphY = *(static_cast<float const*>(pData));
			break;
		}
		case 8: {
			m_fMorphZ = *(static_cast<float const*>(pData));
			break;
		}
		case 9: {
			m_bRecalcNormals = *(static_cast<bool const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CObject3D::GetVar(int iVar) -> void const* {
	static char pBuffer[256];
	switch (iVar) {
		case 0: {
			return &m_iRenderMode;
		}
		case 1: {
			return &m_bWaveXform;
		}
		case 2: {
			return &m_iWaveXformChan;
		}
		case 3: {
			return &m_iMorphMode;
		}
		case 4: {
			return &m_fMorphScale;
		}
		case 5: {
			return &m_fMorphSpeed;
		}
		case 6: {
			return &m_fMorphX;
		}
		case 7: {
			return &m_fMorphY;
		}
		case 8: {
			return &m_fMorphZ;
		}
		case 9: {
			return &m_bRecalcNormals;
		}
		default: {
			return nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	SetFaces
//---------------------------------------------------------------------------//
auto CObject3D::SetFaces(TFace* pFaces, uint uCant) -> void {
	m_pFaces = pFaces;
	m_uNumFaces = static_cast<ushort>(uCant);
}


//---------------------------------------------------------------------------//
//	SetUV
//---------------------------------------------------------------------------//
auto CObject3D::SetUV(TVector2* pUV, uint uCant, uint* pTimes, uint uFrames) -> void {
	m_pUV = pUV;
	m_uNumUV = static_cast<ushort>(uCant);
	DELETE_PTR(m_pPathUV);
	m_pPathUV = NEW CPathLinear;
	if (m_pPathUV && m_pPathUV->Init(CPath::PATH_TIME, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathUV->AddKey((float)pTimes[i]);
		}
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathLinear for UV path!\n"));
		DELETE_PTR(m_pPathUV);
	}
	// Cache
	DELETE_ARRAY(m_pCacheUV);
	m_pCacheUV = NEW_ARRAY(TVector2, m_uNumUV);
	memcpy(m_pCacheUV, m_pUV, m_uNumUV * sizeof(TVector2));
}


//---------------------------------------------------------------------------//
//	SetVertices
//---------------------------------------------------------------------------//
auto CObject3D::SetVertices(TVector3* pVertices, uint uCant, uint* pTimes, uint uFrames) -> void {
	m_pVertices = pVertices;
	m_uNumVertices = static_cast<ushort>(uCant);

	DELETE_PTR(m_pPathVertices);
	m_pPathVertices = NEW CPathLinear;
	if (m_pPathVertices && m_pPathVertices->Init(CPath::PATH_TIME, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathVertices->AddKey((float)pTimes[i]);
		}
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathLinear for vertices!\n"));
		DELETE_PTR(m_pPathVertices);
	}
	// Cache
	DELETE_ARRAY(m_pCacheVertices);
	m_pCacheVertices = NEW_ARRAY(TVector3, m_uNumVertices);
	memcpy(m_pCacheVertices, m_pVertices, m_uNumVertices * sizeof(TVector3));
}


//---------------------------------------------------------------------------//
//	SetPositions
//---------------------------------------------------------------------------//
auto CObject3D::SetPositions(TVector3* pPosiciones, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathPositions);
	m_pPathPositions = NEW CPathLinear;
	if (m_pPathPositions && m_pPathPositions->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathPositions->AddKey((float)pTimes[i], pPosiciones[i]);
		}
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathLinear for positions!\n"));
		DELETE_PTR(m_pPathPositions);
	}
	// Cache
	m_vCachePosition = pPosiciones[0];
}


//---------------------------------------------------------------------------//
//	SetRotations
//---------------------------------------------------------------------------//
auto CObject3D::SetRotations(TVector4* pRotaciones, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathRotations);
	m_pPathRotations = NEW CPathLinear;
	if (m_pPathRotations && m_pPathRotations->Init(CPath::PATH_4D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathRotations->AddKey((float)pTimes[i], pRotaciones[i]);
		}
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathLinear for rotations!\n"));
		DELETE_PTR(m_pPathRotations);
	}
	// Cache
	m_vCacheRotation = pRotaciones[0];
}


//---------------------------------------------------------------------------//
//	SetScales
//---------------------------------------------------------------------------//
auto CObject3D::SetScales(TVector3* pEscalados, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathScales);
	m_pPathScales = NEW CPathLinear;
	if (m_pPathScales && m_pPathScales->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathScales->AddKey((float)pTimes[i], pEscalados[i]);
		}
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathLinear for scales!\n"));
		DELETE_PTR(m_pPathScales);
	}
	// Cache
	m_vCacheScale = pEscalados[0];
}


//---------------------------------------------------------------------------//
//	Prepare
//---------------------------------------------------------------------------//
auto CObject3D::Prepare(uint* pMaterialIDs, int iCount) -> void {
	m_pCacheFNormals = NEW_ARRAY(TVector3, m_uNumFaces);
	m_pCacheVNormals = NEW_ARRAY(TVector3, m_uNumVertices);
	m_pCacheUVEnvMapFlat = NEW_ARRAY(TVector2, m_uNumUV);
	m_pCacheUVEnvMap = NEW_ARRAY(TVector2, m_uNumUV);

	// Calcular normales si no estaban precalculadas
	if (!m_pFNormals) {
		m_pFNormals = NEW_ARRAY(TVector3, m_uNumFaces);
		CalculateFNormals();
	}
	if (!m_pVNormals) {
		m_pVNormals = NEW_ARRAY(TVector3, m_uNumVertices);
		CalculateVNormals();
	}
	
	// Calcular Tangentes/Binormales (se necesitan coordenadas de textura)
	if (m_pUV) {
		m_pTangents = NEW_ARRAY(TVector3, m_uNumVertices);
		m_pBinormals = NEW_ARRAY(TVector3, m_uNumVertices);
		CalculateTangents();
	}

	// Preparar los batches
	PrepareBatches(pMaterialIDs, iCount);

	// Calcular si se usa EnvMap o EnvMapFlat
	/*
	m_bUseUVEnvMap = false;
	m_bUseUVEnvMapFlat = false;
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	for (int iMat = 0; iMat < iNumMateriales; iMat++) {
		auto pMat = pMatMgr->GetMaterial(pTraductorMateriales[iMat]);
		if (pMat) {
			for (int i = 0; i < pMat->GetNumTextures(); i++) {
				if (pMat->GetTextureID(i) >= 0) {
					m_bUseUVEnvMap = m_bUseUVEnvMap || (pMatMgr->GetTexture(pMat->GetTextureID(i))->GetFlags() & CTextura::ENVMAP);
					m_bUseUVEnvMapFlat = m_bUseUVEnvMapFlat || (pMatMgr->GetTexture(pMat->GetTextureID(i))->GetFlags() & CTextura::ENVMAPFLAT);
				}
			}
		}
	}
	*/

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DXMatrixTranslation(&m_CacheMatrixPos, m_vCachePosition.x, m_vCachePosition.y, m_vCachePosition.z);
	D3DXMatrixRotationQuaternion(&m_CacheMatrixRot, (D3DXQUATERNION*)&m_vCacheRotation);
	D3DXMatrixScaling(&m_CacheMatrixScale, m_vCacheScale.x, m_vCacheScale.y, m_vCacheScale.z);
	m_CacheMatrixPRS = m_CacheMatrixScale * m_CacheMatrixRot * m_CacheMatrixPos;
#endif//GFX_ENGINE_DIRECT3D
}
