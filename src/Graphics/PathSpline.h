//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_PATHSPLINE_H
#define NEON_PATHSPLINE_H

#include "Path.h"

struct TKeySpline {
	TVector3 pos;
	TVector3 ds;
	TVector3 dd;

	float tens;
	float cont;
	float bias;
	float easeFrom;
	float easeTo;
	float fTime;
};

class CPathSpline : public CPath {
public:
	CPathSpline();
	~CPathSpline() override;

	auto Init(int iType, int iNumKeys) -> bool override;

	auto AddKey(float /*fTime*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto AddKey(float /*fTime*/, float /*Key*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto AddKey(float /*fTime*/, TVector2 const& /*Key*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto AddKey(float fTime, TVector3 const& Key) -> void override;
	auto AddKey(float /*fTime*/, TVector4 const& /*Key*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto Initialize() -> void override;

	auto SetTime(float fTime) -> void override;
	auto GetTime() const -> float override {
		return m_fTime;
	}

	auto Run(float fRunTime) -> void override;

	auto GetNumKeys() const -> int override {
		return m_iNumKeys;
	}
	auto GetMaxKeys() const -> int override {
		return m_iMaxKeys;
	}

	// Get interpolated keys for the computed frame
	auto GetKeyValue(float& /*Value*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto GetKeyValue(TVector2& /*Value*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto GetKeyValue(TVector3& Value) -> void override;
	auto GetKeyValue(TVector4& /*Value*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto GetKeyValues(TVector2 const* /*pKeys*/, TVector2* /*pValues*/, int /*iNumKeys*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}
	auto GetKeyValues(TVector3 const* /*pKeys*/, TVector3* /*pValues*/, int /*iNumKeys*/) -> void override {
		ASSERTM(false, "CPathSpline: Function not supported");
	}

private:
	// GetKeys
	auto CurKey() const -> int { return m_iKey; }
	auto NextKey(int /*iKey*/) const -> int { return ((m_iKey < (m_iMaxKeys - 1)) ? (m_iKey + 1) : 0); }
	auto PrevKey(int /*iKey*/) const -> int { return ((m_iKey > 0) ? (m_iKey - 1) : (m_iMaxKeys - 1)); }

	auto CompDeriv(TKeySpline* pKeyP, TKeySpline* pKey, TKeySpline* pKeyN) -> void;
	auto CompDerivFirst(TKeySpline* pKey, TKeySpline* pKeyN, TKeySpline* pKeyNN) -> void;
	auto CompDerivLast(TKeySpline* pKeyPP, TKeySpline* pKeyP, TKeySpline* pKey) -> void;
	auto CompDerivTwo(TKeySpline* pKeyA, TKeySpline* pKeyB) -> void;
	auto Ease(float fT, float fA, float fB) -> float;

	auto SearchKeys() -> void;

private:
	int m_iKey;
	TKeySpline* m_pKeys;
};

#endif//NEON_PATHSPLINE_H
