﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GETEXTURE_H
#define NEON_GETEXTURE_H

#include "GEVector.h"
#include "GEDisplayDevice.h"

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
// ebp-> Direct3D 9
#include <d3d9.h>
#else
#include <GL/gl.h>
#endif//GFX_ENGINE

struct TSurfaceDesc {
	int iWidth;
	int iHeight;
	int iPitch;
	void* pBits;
};

class CTexture {
public:
	enum {
		CUBE		= 1 << 1,
		SYSTEMMEM	= 1 << 2,
	};

public:
	CTexture();
	~CTexture();

	auto Init(std::string const& sFile, uint uFlags = 0) -> bool;
	auto Init(int iWidth, int iHeight, int iFormat, bool bDynamic = false, bool bRenderTarget = false, bool bDepthStencil = false, uint uFlags = 0) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Release() -> void;
	auto Restore() -> void;

	auto GetWidth() const -> int { return m_iWidth;  }
	auto GetHeight() const -> int { return m_iHeight; }
	auto GetFormat() const -> int { return m_iFormat; }
	auto GetFilename() const -> std::string const& { return m_sFilename; }

	auto LoadFromTGA(std::string const& sFilename) -> bool;

	auto Lock(TRect const* pRect, TSurfaceDesc& Desc) -> bool;
	auto Unlock() -> bool;

	auto AddRef() -> void { ++m_iCount; }
	auto SubRef() -> bool { return (--m_iCount > 0); }

	auto Set(int iStage) -> void;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto GetTexturaD3D() -> D3DTEXTURE* { return m_pTexture; }
	auto GetSurfaceD3D() -> D3DSURFACE* { return m_pSurface; }
#endif//GFX_ENGINE_DIRECT3D

	auto SetFlags(uint uFlags) -> void { m_uFlags = uFlags; }
	auto GetFlags() const -> uint { return m_uFlags; }

	auto SetID(uint id) -> void { m_id = id; }
	auto GetID() const -> uint { return m_id; }

	auto SetDuration(float fDuration) -> void {
		//GLOG(("Setting Duration: %f\n", fDuration));
		m_fDuration = (fDuration == 0.f) ? (33.f / 1000.f) : fDuration;
	}
	auto GetDuration() -> float { return m_fDuration; }

	static auto sLoadFromGIF(std::string const& sFilename, int& numFrames) -> CTexture**;

private:
	bool m_bOk;
	uint m_id;
	bool m_bLocked;

#if (PLATFORM == PLATFORM_WINDOWS)
	char m_Dir[MAX_PATH];
#else
	char m_Dir[FILENAME_MAX];
#endif//PLATFORM

	std::string m_sFilename;
	int m_iCount;
	int m_iWidth;
	int m_iHeight;
	int m_iFormat;
	bool m_bDynamic;
	float m_fDuration;
	uint m_uUsage;
	uint m_uPool;
	uint m_uFlags;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DTEXTURE* m_pTexture;
	D3DSURFACE* m_pSurface;
#endif//GFX_ENGINE_DIRECT3D
};

#endif//NEON_GETEXTURE_H
