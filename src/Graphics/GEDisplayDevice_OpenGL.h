
#ifndef NEON_GEDISPLAYDEVICE_OPENGL_H
#define NEON_GEDISPLAYDEVICE_OPENGL_H

#include <gl/GL.h>
#include <gl/GLU.h>

#include "GETypes.h"
#include "GEVector.h"

class CDisplayDevice_OpenGL {
public:
	CDisplayDevice_OpenGL();
	~CDisplayDevice_OpenGL();

	auto Init(TGraphicsMode const& tMode, THWnd hWnd) -> bool;
	auto End() -> void;
	auto IsOk() const -> bool { return m_bOk; }

	auto Clear(bool bClearColor, uint uColor, bool bClearZ, float fZ) -> void;
	auto ResetMode(TGraphicsMode const& tMode) -> void;
	auto GetMode() const -> TGraphicsMode const& { return m_tMode; }
	auto Release() -> void;
	auto PageFlip() -> void;

	// Scene
	auto BeginFrame() -> void;
	auto EndFrame() -> void;
	auto BeginScene() -> void;
	auto EndScene() -> void;
	auto IsDrawingScene() const -> bool { return m_bDrawingScene; }

	auto SetViewport(TViewport const& viewport, float minZ, float maxZ) -> void;
	auto GetViewport() const -> TViewport const& { return m_tViewport; }
	auto GetViewportWidth() const -> int { return m_tViewport.x1 - m_tViewport.x0; }
	auto GetViewportHeight() const -> int { return m_tViewport.y1 - m_tViewport.y0; }
	auto Width() const -> int { return m_iTargetWidth; }
	auto Height() const -> int { return m_iTargetHeight; }

	auto SetRenderTarget(uint uRenderTargetTexID) -> void;
	auto GetRenderTarget() const -> uint { return m_uRenderTargetTexID; }


private:
	bool m_bOk;
	bool m_bDrawingScene;
	int m_iTargetWidth;
	int m_iTargetHeight;
	uint m_uRenderTargetTexID;

	THWnd m_hWnd;
	TViewport m_tViewport;
	TGraphicsMode m_tMode;
};

#endif//NEON_GEDISPLAYDEVICE_OPENGL_H
