//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

ITEM(V_INVSCREENW, "INVSCREENW", 1)
ITEM(V_INVSCREENH, "INVSCREENH", 1)
ITEM(V_SCREENW, "SCREENW", 1)
ITEM(V_SCREENH, "SCREENH", 1)
ITEM(V_ALPHA, "ALPHA", 1)
ITEM(V_TIME, "TIME", 1)
ITEM(V_SINTIME, "SINTIME", 1)
ITEM(V_WAVE, "WAVE", 1)
ITEM(V_WAVE1, "WAVEFREQ1", 1)
ITEM(V_WAVE2, "WAVEFREQ2", 1)
ITEM(V_WAVE3, "WAVEFREQ3", 1)
ITEM(V_WAVE4, "WAVEFREQ4", 1)
ITEM(V_WAVE5, "WAVEFREQ5", 1)
ITEM(V_FOV, "FOV", 1)
ITEM(V_CAMERAPOSITION, "CAMERAPOSITION", 3)
ITEM(V_DIRECTION, "DIRECTION", 3)
ITEM(V_LIGHTINTENS, "LIGHTINTENS", 1)
ITEM(V_DUMMY2, "DUMMY2", 1)
ITEM(V_VIEW, "VIEW", 16)								// View matrix
ITEM(V_PROJECTION, "PROJECTION", 16)					// Projection matrix
ITEM(V_VIEWPROJECTION, "VIEWPROJECTION", 16)			// View-Projection matrix
ITEM(V_WORLD, "WORLD", 16)								// World matrix
ITEM(V_WORLDVIEWPROJECTION, "WORLDVIEWPROJECTION", 16)	// World-View-Projection matrix
ITEM(V_RAND1, "RAND1", 1)
ITEM(V_RAND2, "RAND2", 1)
ITEM(V_RAND3, "RAND3", 1)
ITEM(V_RAND4, "RAND4", 1)
ITEM(V_VAR1, "VAR1", 1)
ITEM(V_VAR2, "VAR2", 1)
ITEM(V_VAR3, "VAR3", 1)
ITEM(V_VAR4, "VAR4", 1)
ITEM(V_VAR5, "VAR5", 1)
ITEM(V_VAR6, "VAR6", 1)
ITEM(V_VAR7, "VAR7", 1)
ITEM(V_VAR8, "VAR8", 1)
ITEM(V_VAR9, "VAR9", 1)
ITEM(V_VAR10, "VAR10", 1)
ITEM(V_VAR11, "VAR11", 1)
ITEM(V_VAR12, "VAR12", 1)
ITEM(V_VAR13, "VAR13", 1)
ITEM(V_VAR14, "VAR14", 1)
ITEM(V_VAR15, "VAR15", 1)
ITEM(V_VAR16, "VAR16", 1)
