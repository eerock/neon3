//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"
#include "Xml.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMaterial::CMaterial()
	: m_bOk(false)
	, m_id(CMaterialManager::INVALID)
	, m_sFilename()
	, m_bMaterial(false)
	, m_iCount(0)
	, m_iNumTextures(0)
	, m_uShaderID(CMaterialManager::INVALID)
{
	for (int i = 0; i < MAX_TEXTURES; ++i) {
		m_uTextureIDs[i] = CMaterialManager::INVALID;
	}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMaterial::~CMaterial() {
	if (IsOk()) {
		ReleaseVars();
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMaterial::Init(std::string const& sFile) -> bool {
	ResetVars();

	m_bOk = true;
	m_sFilename = sFile;

	auto idx = sFile.rfind('.');
	if (idx != std::string::npos) {
		std::string ext = sFile.substr(idx + 1);
		if (ext == "fx") {
			m_bOk = LoadMaterialFX(sFile);
		}
		else if (ext == "nsh" || ext == "nmt" || ext == "xml") {
			m_bOk = LoadMaterialNeon(sFile);
		}
		else {
			m_bOk = LoadMaterialTex(sFile);
		}
	}

	if (!IsOk()) {
		GLOG(("Can't load material %s\n", sFile.c_str()));
		ReleaseVars();
	}

	return IsOk();
}


//---------------------------------------------------------------------------//
//	ResetVars
//---------------------------------------------------------------------------//
auto CMaterial::ResetVars() -> void {
	m_bMaterial = false;
	m_iCount = 0;
	m_iNumTextures = 0;
	m_uShaderID = CMaterialManager::INVALID;
	for (int i = 0; i < MAX_TEXTURES; ++i) {
		m_uTextureIDs[i] = CMaterialManager::INVALID;
	}
}


//---------------------------------------------------------------------------//
//	ReleaseVars
//---------------------------------------------------------------------------//
auto CMaterial::ReleaseVars() -> void {
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	// Texturas
	for (int i = 0; i < m_iNumTextures; ++i) {
		pMatMgr->RemoveTexture(m_uTextureIDs[i]);
	}
	// Shaders
	pMatMgr->RemoveShader(m_uShaderID);
}


//---------------------------------------------------------------------------//
//	LoadMaterialNeon
//---------------------------------------------------------------------------//
auto CMaterial::LoadMaterialNeon(std::string const& sFile) -> bool {
	bool bRes = false;
	// XML Document
	TiXmlDocument XMLDoc(sFile.c_str());
	if (XMLDoc.LoadFile() && XMLDoc.FirstChild("material")) {
		// Material completo
		bRes = true;
		m_bMaterial = true;

		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		auto pNode = XMLDoc.FirstChild("material");

		// NumTexturas
		if (neon::GetNumSameChilds(pNode, "texture") > MAX_TEXTURES) {
			GLOG(("WARN: Material %s specified more than %d textures\n", sFile.c_str(), MAX_TEXTURES));
		}

		// Textures
		TiXmlNode* pIter = nullptr;
		int i = 0;
		do {
			pIter = pNode->IterateChildren("texture", pIter);
			if (pIter) {
				auto pValue = pIter->ToElement()->Attribute("file");
				if (pValue && pValue[0]) {
					// Add to Materials Manager...
					m_uTextureIDs[i] = pMatMgr->AddTexture(pValue);
					if (m_uTextureIDs[i] == CMaterialManager::INVALID) {
						GLOG(("ERROR: Can't load texture %s on Material %s\n", pValue, sFile.c_str()));
					}
				} else {
					GLOG(("ERROR: Texture file not specified on Material %s\n", sFile.c_str()));
				}
				m_iNumTextures++;
			}
			i++;
		} while (pIter && i < MAX_TEXTURES);

		// Shader
		pIter = pNode->FirstChild("shader");
		if (pIter) {
			auto pValue = pIter->ToElement()->Attribute("file");
			if (pValue && pValue[0]) {
				m_uShaderID = pMatMgr->AddShader(pValue);
				if (m_uShaderID == CMaterialManager::INVALID) {
					GLOG(("ERROR: Can't load Shader %s on Material %s\n", pValue, sFile.c_str()));
				}
			} else {
				GLOG(("ERROR: Shader file not specified on Material %s\n", sFile.c_str()));
			}
		}
	}
	return bRes;
}


//---------------------------------------------------------------------------//
//	LoadMaterialFX
//---------------------------------------------------------------------------//
auto CMaterial::LoadMaterialFX(std::string const& sFile) -> bool {
	// Shader
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	m_uShaderID = pMatMgr->AddShader(sFile);
	if (m_uShaderID != CMaterialManager::INVALID) {
		return true;
	} else {
		GLOG(("ERROR: Can't load shader %s\n", sFile.c_str()));
		return false;
	}
}


//---------------------------------------------------------------------------//
//	LoadMaterialTex
//---------------------------------------------------------------------------//
auto CMaterial::LoadMaterialTex(std::string const& sFile) -> bool {
	m_iNumTextures = 1;
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	m_uTextureIDs[0] = pMatMgr->AddTexture(sFile);
	return (m_uTextureIDs[0] != CMaterialManager::INVALID);
}


//---------------------------------------------------------------------------//
//	BeginDraw
//---------------------------------------------------------------------------//
auto CMaterial::BeginDraw(uint uTech) -> uint {
	uint uPasses = 1;
	//auto pD3D = g_DisplayDevice.GetD3DDevice();

	// Textures
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	pMatMgr->SetTexture(CMaterialManager::INVALID, 0);
	for (int i = 0; i < m_iNumTextures; ++i) {
		if (m_uTextureIDs[i] != CMaterialManager::INVALID) {
			pMatMgr->SetTexture(m_uTextureIDs[i], i);
		} else {
			pMatMgr->SetTexture(CMaterialManager::INVALID, i);
		}
	}

	// Shader
	if (m_uShaderID != CMaterialManager::INVALID) {
		uPasses = pMatMgr->GetShader(m_uShaderID)->BeginDraw(uTech);
	}
	return uPasses;
}


//---------------------------------------------------------------------------//
//	EndDraw
//---------------------------------------------------------------------------//
auto CMaterial::EndDraw() -> void {
	// Shader
	if (m_uShaderID != CMaterialManager::INVALID) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->GetShader(m_uShaderID)->EndDraw();
	}
}


//---------------------------------------------------------------------------//
//	BeginPass
//---------------------------------------------------------------------------//
auto CMaterial::BeginPass(uint uPass) -> void {
	// Shader
	if (m_uShaderID != CMaterialManager::INVALID) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->GetShader(m_uShaderID)->BeginPass(uPass);
	}
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	else {
		auto pD3D = g_DisplayDevice.GetD3DDevice();
		pD3D->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
		pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		pD3D->SetRenderState(D3DRS_CLIPPING, TRUE);
		pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
		pD3D->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_TRUE);
		pD3D->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		pD3D->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
		pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		pD3D->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
		pD3D->SetRenderState(D3DRS_ALPHAREF, (DWORD)8);

		pD3D->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		pD3D->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

		pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
		pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

		pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

		pD3D->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		pD3D->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

		pD3D->SetVertexShader(nullptr);
		pD3D->SetPixelShader(nullptr);
	}
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	EndPass
//---------------------------------------------------------------------------//
auto CMaterial::EndPass() -> void {
	// Shader
	if (m_uShaderID != CMaterialManager::INVALID) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->GetShader(m_uShaderID)->EndPass();
	}
}


//---------------------------------------------------------------------------//
//	FixedPipeline
//---------------------------------------------------------------------------//
auto CMaterial::FixedPipeline() const -> bool {
	bool bRes = true;
	// Shader
	if (m_uShaderID != CMaterialManager::INVALID) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		bRes = pMatMgr->GetShader(m_uShaderID)->FixedPipeline();
	}
	return bRes;
}
