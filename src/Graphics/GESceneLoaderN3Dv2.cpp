//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"
#include "GESceneLoader.h"
#include "GEScene3D.h"
#include "GEObject3D.h"
#include "GECamera3D.h"
#include "GELight3D.h"
#include "N3DFormat.h"


//---------------------------------------------------------------------------//
//	Load N3DV2
//---------------------------------------------------------------------------//
auto CN3DV2Loader::Load(CGEScene3D* pScene3D, char const* pData, uint uDataSize) -> bool {
	UNREFERENCED_PARAMETER(uDataSize);
	uint i = 0, j = 0;

	// Leer cabecera y comprobar que todo esta bien
	// es-en: Read header and check that all is well.
	TN3DHeaderv1 tHeader;
	ReadData(pData, &tHeader, sizeof(tHeader));

	if (tHeader.uNumObjects < 1) {
		GLOG(("ERROR: At least one object must exist in 3D scenes\n"));
		return false;
	}
	if (tHeader.uNumCameras < 1) {
		GLOG(("ERROR: At least one camera must exist in 3D scenes\n"));
		return false;
	}


	//-----------------------------------------------------------------------//
	//	Scene Data
	//-----------------------------------------------------------------------//
	pScene3D->m_fFirstFrame = tHeader.fFirstFrame;
	pScene3D->m_fLastFrame = tHeader.fLastFrame;
	pScene3D->m_fFrameSpeed = tHeader.fFrameSpeed;
	pScene3D->m_fTicksPerFrame = tHeader.fTicksPerFrame;
	pScene3D->m_fFPS = tHeader.fFPS;
	pScene3D->m_fAmbientR = tHeader.fAmbientR;
	pScene3D->m_fAmbientG = tHeader.fAmbientG;
	pScene3D->m_fAmbientB = tHeader.fAmbientB;

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();


	//-----------------------------------------------------------------------//
	//	Materials
	//-----------------------------------------------------------------------//
	uint* auMaterialIDs = (tHeader.uNumMaterials > 0) ? NEW_ARRAY(uint, tHeader.uNumMaterials) : nullptr;

	for (i = 0; i < tHeader.uNumMaterials; ++i) {
		// read a material name
		auMaterialIDs[i] = CMaterialManager::INVALID;
		TN3DMaterialv1 tMat;
		ReadData(pData, &tMat, sizeof(tMat));

		// add the material
		auMaterialIDs[i] = pMatMgr->AddMaterial(tMat.pName);
		if (auMaterialIDs[i] == CMaterialManager::INVALID) {
			// Si no se puede cargar el material, intentaremos ponerle uno flat
			// If you cannot load the material, we will try to add the flat one.
			GLOG(("ERROR: Can't load material %s. Setting default flat material\n", tMat.pName));
			auMaterialIDs[i] = pMatMgr->AddMaterial("flat.nmt");
			// Si aun asi no se puede cargar,
			// If you still cannot load...
			if (auMaterialIDs[i] == CMaterialManager::INVALID) {
				GLOG(("ERROR: Can't load standard material flat.nmt\n"));
				return false;
			}
		}
	}

	pScene3D->SetMaterials(auMaterialIDs, tHeader.uNumMaterials);


	//---------------------------------------------------------------------------//
	//	Objects
	//---------------------------------------------------------------------------//
	CObject3D* pObjects = (tHeader.uNumObjects > 0) ? NEW_ARRAY(CObject3D, tHeader.uNumObjects) : nullptr;

	for (i = 0; i < tHeader.uNumObjects; ++i) {
		CObject3D* pObj = &pObjects[i];
		TObj3DHeaderv1 tObjHeader;
		pObj->Init(tHeader.fLastFrame - tHeader.fFirstFrame);
		ReadData(pData, &tObjHeader, sizeof(TObj3DHeaderv1));
		if (tObjHeader.uNumFaces < 0) {
			GLOG(("ERROR: 3D object has no faces!\n"));
			return false;
		}

		// Header
		pObj->m_sName = tObjHeader.pName;
		pObj->m_uFlags = tObjHeader.uFlags;
		pObj->m_uColor = tObjHeader.uColor;

		// Faces
		TFace* pFaces = NEW_ARRAY(TFace, tObjHeader.uNumFaces);

		// read in the faces data...
		ReadData(pData, pFaces, tObjHeader.uNumFaces * sizeof(TFace));
		for (j = 0; j < tObjHeader.uNumFaces; ++j) {
			Swap(pFaces[j].i1, pFaces[j].i2);	// ebp-> note: swap y/z
		}

		pObj->SetFaces(pFaces, tObjHeader.uNumFaces);

		// UV Coords
		if (tObjHeader.uNumUV > 0) {
			uint* pFrameTimesUV = NEW_ARRAY(uint, 1);
			TVector2* pUV = NEW_ARRAY(TVector2, tObjHeader.uNumUV);

			// read in the uv coord data...
			ReadData(pData, pUV, tObjHeader.uNumUV * sizeof(TVector2));
			pFrameTimesUV[0] = 0;

			// ebp-> when it parses the faces above, the 3d cube has 372 faces.
			// now here it sees 372 * 3 = 1,116 UV coords.  that makes sense, there are
			// 3 verts per triangle/face, and there should be a UV per vert.

			// the NEW_ARRAY was busted, allocating way too much memory, so now we need
			// to look for places where we write beyond our allocations.  below was an example
			// of that.  it swapped the second and third vert indices in the faces data.
			// so here it was doing the same thing with the corresponding UV coords, but
			// instead of looping through numFaces, it looped through num UVs, which is wrong.
			
			//for (j = 0; j < ObjHeader.uNumUV; j++)	// old, bad!  writes way beyond allocation!
			for (j = 0; j < tObjHeader.uNumFaces; ++j) {	// correct!  this stays in bounds!
				Swap(pUV[j * 3 + 1], pUV[j * 3 + 2]);	// ebp-> note: swap y/z
			}

			pObj->SetUV(pUV, tObjHeader.uNumUV, pFrameTimesUV, 1);
			pObj->m_uColor = 0xFFFFFFFF;
			DELETE_ARRAY(pFrameTimesUV);
		}

		// Vertices
		uint* pFrameTimesVertices = NEW_ARRAY(uint, tObjHeader.uNumFramesVertices);
		TVector3* pVertices = NEW_ARRAY(TVector3, tObjHeader.uNumVertices * tObjHeader.uNumFramesVertices);
		ReadData(pData, pFrameTimesVertices, tObjHeader.uNumFramesVertices * sizeof(uint));
		ReadData(pData, pVertices, tObjHeader.uNumVertices * tObjHeader.uNumFramesVertices * sizeof(TVector3));
		pObj->SetVertices(pVertices, tObjHeader.uNumVertices, pFrameTimesVertices, tObjHeader.uNumFramesVertices);
		DELETE_ARRAY(pFrameTimesVertices);

		// Positions
		uint* pFrameTimesPositions = NEW_ARRAY(uint, tObjHeader.uNumFramesPositions);
		TVector3* pPositions = NEW_ARRAY(TVector3, tObjHeader.uNumFramesPositions);
		ReadData(pData, pFrameTimesPositions, tObjHeader.uNumFramesPositions * sizeof(uint));
		ReadData(pData, pPositions, tObjHeader.uNumFramesPositions * sizeof(TVector3));
		pObj->SetPositions(pPositions, pFrameTimesPositions, tObjHeader.uNumFramesPositions);
		DELETE_ARRAY(pFrameTimesPositions);

		// Rotations
		uint* pFrameTimesRotations = NEW_ARRAY(uint, tObjHeader.uNumFramesRotations);
		TVector4* pRotations = NEW_ARRAY(TVector4, tObjHeader.uNumFramesRotations);
		ReadData(pData, pFrameTimesRotations, tObjHeader.uNumFramesRotations * sizeof(uint));
		ReadData(pData, pRotations, tObjHeader.uNumFramesRotations * sizeof(TVector4));
		pObj->SetRotations(pRotations, pFrameTimesRotations, tObjHeader.uNumFramesRotations);
		DELETE_ARRAY(pFrameTimesRotations);

		// Scales
		uint* pFrameTimesScales = NEW_ARRAY(uint, tObjHeader.uNumFramesScales);
		TVector3* pScales = NEW_ARRAY(TVector3, tObjHeader.uNumFramesScales);
		ReadData(pData, pFrameTimesScales, tObjHeader.uNumFramesScales * sizeof(uint));
		ReadData(pData, pScales, tObjHeader.uNumFramesScales * sizeof(TVector3));
		pObj->SetScales(pScales, pFrameTimesScales, tObjHeader.uNumFramesScales);
		DELETE_ARRAY(pFrameTimesScales);

		// Prepare the object
		pObj->Prepare(auMaterialIDs, tHeader.uNumMaterials);
	}

	pScene3D->SetObjects(pObjects, tHeader.uNumObjects);


	//-----------------------------------------------------------------------//
	//	Cameras
	//-----------------------------------------------------------------------//
	CCamera3D* pCameras = (tHeader.uNumCameras > 0) ? NEW_ARRAY(CCamera3D, tHeader.uNumCameras) : nullptr;

	for (i = 0; i < tHeader.uNumCameras; ++i) {
		TCam3DHeaderv1 tCamHeader;
		CCamera3D* pCam = &pCameras[i];
		pCam->Init();

		// Header
		ReadData(pData, &tCamHeader, sizeof(TCam3DHeaderv1));
		char pBuffer[32];
		Snprintf(pBuffer, 32, "%d", i);
		pCam->m_sName = pBuffer;

		// PosSrc
		uint* pFrameTimesPosSrc = NEW_ARRAY(uint, tCamHeader.uNumFramesPosSrc);
		TVector3* pPosSrc = NEW_ARRAY(TVector3, tCamHeader.uNumFramesPosSrc);
		ReadData(pData, pFrameTimesPosSrc, tCamHeader.uNumFramesPosSrc * sizeof(uint));
		ReadData(pData, pPosSrc, tCamHeader.uNumFramesPosSrc * sizeof(TVector3));
		pCam->SetPosSrc(pPosSrc, pFrameTimesPosSrc, tCamHeader.uNumFramesPosSrc);
		DELETE_ARRAY(pFrameTimesPosSrc);
		DELETE_ARRAY(pPosSrc); // ebp-> this is ok

		// PosTgt
		uint* pFrameTimesPosTgt = NEW_ARRAY(uint, tCamHeader.uNumFramesPosTgt);
		TVector3* pPosTgt = NEW_ARRAY(TVector3, tCamHeader.uNumFramesPosTgt);
		ReadData(pData, pFrameTimesPosTgt, tCamHeader.uNumFramesPosTgt * sizeof(uint));
		ReadData(pData, pPosTgt, tCamHeader.uNumFramesPosTgt * sizeof(TVector3));
		pCam->SetPosTgt(pPosTgt, pFrameTimesPosTgt, tCamHeader.uNumFramesPosTgt);
		DELETE_ARRAY(pFrameTimesPosTgt);
		DELETE_ARRAY(pPosTgt); // ebp-> this is ok

		// Fov
		uint* pFrameTimesFov = NEW_ARRAY(uint, tCamHeader.uNumFramesFov);
		float* pFov = NEW_ARRAY(float, tCamHeader.uNumFramesFov);
		ReadData(pData, pFrameTimesFov, tCamHeader.uNumFramesFov * sizeof(uint));
		ReadData(pData, pFov, tCamHeader.uNumFramesFov * sizeof(float));
		pCam->SetFov(pFov, pFrameTimesFov, tCamHeader.uNumFramesFov);
		DELETE_ARRAY(pFrameTimesFov);
		DELETE_ARRAY(pFov); // ebp-> this is ok
	}

	pScene3D->SetCameras(pCameras, tHeader.uNumCameras);


	//---------------------------------------------------------------------------//
	//	Lights
	//---------------------------------------------------------------------------//
	CLight3D* pLights = (tHeader.uNumLights > 0) ? NEW_ARRAY(CLight3D, tHeader.uNumLights) : nullptr;

	for (i = 0; i < tHeader.uNumLights; ++i) {
		TLight3DHeaderv1 tLightHeader;
		CLight3D* pLight = &pLights[i];
		pLight->Init();

		// Header
		ReadData(pData, &tLightHeader, sizeof(tLightHeader));
		char pBuffer[32];
		Snprintf(pBuffer, 32, "%d", i);
		pLight->m_sName = pBuffer;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		// D3DLight
		switch (tLightHeader.uType) {
			case CLight3D::OMNI: {
				pLight->m_tLight.Type = D3DLIGHT_POINT;
				break;
			}
			case CLight3D::DIRECTIONAL: {
				pLight->m_tLight.Type = D3DLIGHT_DIRECTIONAL;
				break;
			}
			case CLight3D::SPOT: {
				pLight->m_tLight.Type = D3DLIGHT_SPOT;
				break;
			}
			default: {
				break;
			}
		}
		pLight->m_tLight.Diffuse.r = tLightHeader.fR;
		pLight->m_tLight.Diffuse.g = tLightHeader.fG;
		pLight->m_tLight.Diffuse.b = tLightHeader.fB;
		pLight->m_tLight.Attenuation0 = 1.f;
		pLight->m_tLight.Attenuation1 = 0.f;
		pLight->m_tLight.Attenuation2 = 0.f;
		pLight->m_tLight.Falloff = 0.f;
		pLight->m_tLight.Range = tLightHeader.fRange;
#endif//GFX_ENGINE_DIRECT3D

		// PosSrc
		uint* pFrameTimesPosSrc = NEW_ARRAY(uint, tLightHeader.uNumFramesPosSrc);
		TVector3* pPosSrc = NEW_ARRAY(TVector3, tLightHeader.uNumFramesPosSrc);
		ReadData(pData, pFrameTimesPosSrc, tLightHeader.uNumFramesPosSrc * sizeof(uint));
		ReadData(pData, pPosSrc, tLightHeader.uNumFramesPosSrc * sizeof(TVector3));
		pLight->SetPosSrc(pPosSrc, pFrameTimesPosSrc, tLightHeader.uNumFramesPosSrc);
		DELETE_ARRAY(pFrameTimesPosSrc);
		DELETE_ARRAY(pPosSrc);

		// PosTgt
		/*
		uint* pFrameTimesPosTgt = NEW_ARRAY(uint, tLightHeader.uNumFramesPosTgt);
		TVector3* pPosTgt = NEW_ARRAY(TVector3, tLightHeader.uNumFramesPosTgt);
		ReadData(pData, pFrameTimesPosTgt, tLightHeader.uNumFramesPosTgt * sizeof(uint));
		ReadData(pData, pPosTgt, tLightHeader.uNumFramesPosTgt * sizeof(TVector3));
		pLight->SetPosTgt(pPosTgt, pFrameTimesPosTgt, tLightHeader.uNumFramesPosTgt);
		DELETE_ARRAY(pFrameTimesPosTgt);
		DELETE_ARRAY(pPosTgt);

		// Falloff
		uint* pFrameTimesFalloff = NEW_ARRAY(uint, tLightHeader.uNumFramesFalloff);
		float* pFalloff = NEW_ARRAY(float, tLightHeader.uNumFramesFalloff);
		ReadData(pData, pFrameTimesFalloff, tLightHeader.uNumFramesFalloff * sizeof(uint));
		ReadData(pData, pFalloff, tLightHeader.uNumFramesFalloff * sizeof(float));
		pLight->SetFalloff(pFalloff, pFrameTimesFalloff, tLightHeader.uNumFramesFalloff);
		DELETE_ARRAY(pFrameTimesFalloff);
		DELETE_ARRAY(pFalloff);
		*/
	}

	pScene3D->SetLights(pLights, tHeader.uNumLights);

	return true;
}
