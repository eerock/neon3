//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEMATERIAL_H
#define NEON_GEMATERIAL_H

#define MAX_TEXTURES	(4)

//---------------------------------------------------------------------------//
//	Class CMaterial
//---------------------------------------------------------------------------//
class CMaterial {
public:
	CMaterial();
	~CMaterial();

	auto Init(std::string const& sFile) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Release() -> void {}
	auto Restore() -> void {}

	auto BeginDraw(uint uTech) -> uint;
	auto EndDraw() -> void;
	auto BeginPass(uint uPass) -> void;
	auto EndPass() -> void;

	auto GetFilename() const -> std::string const& { return m_sFilename; }
	auto GetEsMaterial() const -> bool { return m_bMaterial; }
	auto GetNumTextures() const -> int { return m_iNumTextures; }
	auto GetTextureID(int iTex) const -> uint { return m_uTextureIDs[iTex]; }
	auto FixedPipeline() const -> bool;

	auto AddRef() -> void { ++m_iCount; }
	auto SubRef() -> bool { return (--m_iCount > 0); }

	auto SetID(uint id) -> void { m_id = id; }
	auto GetID() const -> uint { return m_id; }

private:
	auto LoadMaterialNeon(std::string const& sFile) -> bool;
	auto LoadMaterialFX(std::string const& sFile) -> bool;
	auto LoadMaterialTex(std::string const& sFile) -> bool;
	auto ResetVars() -> void;
	auto ReleaseVars() -> void;

private:
	bool m_bOk;
	uint m_id;
	std::string m_sFilename;
	bool m_bMaterial;
	int m_iCount;
	int m_iNumTextures;
	uint m_uTextureIDs[MAX_TEXTURES];
	uint m_uShaderID;
};

#endif//NEON_GEMATERIAL_H
