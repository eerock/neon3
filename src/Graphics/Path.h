//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_PATH_H
#define NEON_PATH_H

#include "Base.h"
#include "GEVector.h"

class CPath {
public:
	enum {
		PATH_TIME,
		PATH_1D,
		PATH_2D,
		PATH_3D,
		PATH_4D,
	};

	CPath();
	virtual ~CPath();

	virtual auto Init(int iType, int iNumKeys) -> bool;
	virtual auto IsOk() const -> bool { return m_bOk; }

	virtual auto AddKey(float fTime) -> void = 0;
	virtual auto AddKey(float fTime, float Key) -> void = 0;
	virtual auto AddKey(float fTime, TVector2 const& Key) -> void = 0;
	virtual auto AddKey(float fTime, TVector3 const& Key) -> void = 0;
	virtual auto AddKey(float fTime, TVector4 const& Key) -> void = 0;
	virtual auto Initialize() -> void = 0;

	virtual auto SetTime(float fTime) -> void = 0;
	virtual auto GetTime() const -> float { return m_fTime; }

	virtual auto Run(float fRunTime) -> void = 0;

	virtual auto GetNumKeys() const -> int { return m_iNumKeys; }
	virtual auto GetMaxKeys() const -> int { return m_iMaxKeys; }

	// Get interpolated keys for the computed frame
	virtual auto GetKeyValue(float& Value) -> void = 0;
	virtual auto GetKeyValue(TVector2& Value) -> void = 0;
	virtual auto GetKeyValue(TVector3& Value) -> void = 0;
	virtual auto GetKeyValue(TVector4& Value) -> void = 0;
	virtual auto GetKeyValues(TVector2 const* pKeys, TVector2* pValues, int iNumKeys) -> void = 0;
	virtual auto GetKeyValues(TVector3 const* pKeys, TVector3* pValues, int iNumKeys) -> void = 0;

#if (PLATFORM == PLATFORM_WINDOWS)
	// not on windows
#else
	static auto min(float a, float b) -> float { return (a < b) ? a : b; }
	static auto max(float a, float b) -> float { return (b < a) ? a : b; }
#endif//PLATFORM

protected:
	bool m_bOk;
	int m_iType;
	int m_iMaxKeys;
	int m_iNumKeys;
	float m_fTime;
	float m_fTimeIni;
	float m_fTimeEnd;
};

#endif//NEON_PATH_H
