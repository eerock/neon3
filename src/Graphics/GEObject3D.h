﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEOBJECT3D_H
#define NEON_GEOBJECT3D_H

// ebp-> Direct3D dependencies

#include "GEGraphics.h"

class  CMaterial;
class  CPath;
class  CSceneLoader;
struct TCtrlVar;

struct TFace {
	ushort i0;
	ushort i1;
	ushort i2;
	ushort iMat;
};

struct TFaceBatch {
	uint uMaterialID;
	uint uNumFaces;
	int* pFaces;
};

class CObject3D {
	friend class CN3DV1Loader;
	friend class CN3DV2Loader;
	friend class C3DSLoader;

public:
	CObject3D();
	~CObject3D();

	auto Init(float fNumFrames) -> bool;
	auto IsOk() const -> bool { return  m_bOk; }

	auto GetName() const -> std::string const& { return m_sName; }
	auto GetColor() const -> uint { return m_uColor; }
	auto GetFaces() const -> TFace const* { return m_pFaces; }
	auto GetUV() const -> TVector2 const* { return m_pCacheUV; }
	auto GetUVEnvMap() const -> TVector2 const* { return m_pCacheUVEnvMap; }
	auto GetUVEnvMapFlat() const -> TVector2 const* { return m_pCacheUVEnvMapFlat; }
	auto GetVertices() const -> TVector3 const* { return m_pCacheVertices; }
	auto GetFNormals() const -> TVector3 const* { return m_pCacheFNormals; }
	auto GetVNormals() const -> TVector3 const* { return m_pCacheVNormals; }
	auto GetTangents() const -> TVector3 const* { return m_pTangents; }
	auto GetBinormals() const -> TVector3 const* { return m_pBinormals; }
	auto GetPos() const -> TVector3 const* { return &m_vCachePosition; }
	auto GetRot() const -> TVector4 const* { return &m_vCacheRotation; }
	auto GetScale() const -> TVector3 const* { return &m_vCacheScale; }
	auto GetZ() const -> float { return  m_fCacheZ; }

	auto GetNumFaces() const -> ushort { return m_uNumFaces; }
	auto GetNumUV() const -> ushort { return m_uNumUV; }
	auto GetNumVertices() const -> ushort { return m_uNumVertices; }

	auto GetFaceBatch(int iIDMat) -> TFaceBatch* { return &m_pBatches[iIDMat]; }

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto GetMatrixPRS() -> D3DMATRIX* { return &m_CacheMatrixPRS; }
#endif//GFX_ENGINE_DIRECT3D

	auto SetTime(float fTime) -> void;

	auto Draw(CDisplayDevice* pDD) -> void;

	auto CalculateFNormals() -> void;
	auto CalculateVNormals() -> void;
	auto CalculateTangents() -> void;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto CalculateUV(D3DXMATRIX const& matProj) -> void;
	auto CalculateZ(D3DXMATRIX const& matProj) -> void;
#endif//GFX_ENGINE_DIRECT3D

	auto SetVar(int iVar, void const* pData) -> void;
	auto GetVar(int iVar) -> void const*;

	// For Loader
	auto SetFaces(TFace* pFaces, uint uCant) -> void;
	auto SetUV(TVector2* pUV, uint uCant, uint* pTimes, uint uFrames) -> void;
	auto SetVertices(TVector3* pVertices, uint uCant, uint* pTimes, uint uFrames) -> void;
	auto SetPositions(TVector3* pPositions, uint* pTimes, uint uFrames) -> void;
	auto SetRotations(TVector4* pRotations, uint* pTimes, uint uFrames) -> void;
	auto SetScales(TVector3* pScales, uint* pTimes, uint uFrames) -> void;
	auto Prepare(uint* pMaterialIDs, int iCount) -> void;

	static auto GetVars() -> TCtrlVar*;

private:
	auto FillVertex(TVertex_HCV_XYZ_F* pVertices, CMaterial* pMat, int i0, int uv, uint* pFlags) -> void;
	auto FillVertex(TVertex_HCV_XYZ_F* pVertices, int i0) -> void;
	auto FillVertex(TVertex_HCV_XYZ_P* pVertices, CMaterial* pMat, int i0, int uv, uint* pFlags) -> void;
	auto FillVertex(TVertex_HCV_XYZ_P* pVertices, int i0) -> void;
	auto PrepareBatches(uint* pMaterialIDs, int iCount) -> void;
	auto ApplyMorphRot(bool bChanged, float fTime) -> void;
	auto ApplyMorphSin(bool bChanged, float fTime) -> void;
	auto ApplyMorphSinY(bool bChanged, float fTime) -> void;
	auto ApplyMorphTent(bool bChanged, float fTime) -> void;

	auto ResetVars() -> void;
	auto ReleaseVars() -> void;

	bool m_bOk;
	uint m_uColor;
	std::string m_sName;
	ushort m_uFlags;
	ushort m_uNumFaces;
	ushort m_uNumUV;
	ushort m_uNumVertices;
	bool m_bUseUVEnvMap;
	bool m_bUseUVEnvMapFlat;

	TFace* m_pFaces;

	// UV
	TVector2* m_pUV;
	CPath* m_pPathUV;

	// Vertices
	CPath* m_pPathVertices;
	TVector3* m_pVertices;
	TVector3* m_pFNormals;
	TVector3* m_pVNormals;
	TVector3* m_pTangents;
	TVector3* m_pBinormals;

	// PRS
	CPath* m_pPathPositions;
	CPath* m_pPathRotations;
	CPath* m_pPathScales;

	// Batches
	int m_iNumBatches;
	TFaceBatch* m_pBatches;

	// Datos del frame cacheado
	float m_fCacheZ;
	float m_fCacheFrame;
	TVector2* m_pCacheUV;
	TVector2* m_pCacheUVEnvMap;
	TVector2* m_pCacheUVEnvMapFlat;
	TVector3* m_pCacheVertices;
	TVector3 m_vCachePosition;
	TVector4 m_vCacheRotation;
	TVector3 m_vCacheScale;
	TVector3* m_pCacheFNormals;
	TVector3* m_pCacheVNormals;
    
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DXMATRIX m_CacheMatrixPos;
	D3DXMATRIX m_CacheMatrixRot;
	D3DXMATRIX m_CacheMatrixScale;
	D3DXMATRIX m_CacheMatrixPRS;
#endif//GFX_ENGINE_DIRECT3D

	// Timing
	float m_fNumFrames;
	float m_fTime;

	// Vars
	int m_iRenderMode;
	bool m_bWaveXform;
	int m_iWaveXformChan;
	int m_iMorphMode;
	float m_fMorphScale;
	float m_fMorphX;
	float m_fMorphY;
	float m_fMorphZ;
	float m_fMorphSpeed;
	bool m_bRecalcNormals;
};

#endif//NEON_GEOBJECT3D_H
