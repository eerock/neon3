﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GECAMERA3D_H
#define NEON_GECAMERA3D_H

#include "N3DFormat.h"
#include "GEVector.h"

class  CPath;
class  CFile;
struct TCtrlVar;


//---------------------------------------------------------------------------//
//	Class CCamera3D
//---------------------------------------------------------------------------//
class CCamera3D {
	friend class CN3DV1Loader;
	friend class CN3DV2Loader;
	friend class C3DSLoader;

public:
	CCamera3D();
	~CCamera3D();

	auto Init() -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto SetTime(float fTime) -> void;

	auto GetName() const -> std::string const& { return m_sName; }
	auto GetSrc() const -> TVector3 const* { return &m_vCachePosSrc; }
	auto GetTgt() const -> TVector3 const* { return &m_vCachePosTgt; }
	auto GetUp() const -> TVector3 const* { return &m_vCachePosUp; }
	auto GetFov() const -> float { return m_fCacheFov; }

	static auto GetVars() -> TCtrlVar*;
	auto SetVar(int iVar, void const* pData) -> void;
	auto GetVar(int iVar) -> void const*;

	// Loader
	auto SetPosSrc(TVector3* pVertices, uint* pTimes, uint uFrames) -> void;
	auto SetPosTgt(TVector3* pVertices, uint* pTimes, uint uFrames) -> void;
	auto SetPosUp(TVector3* pVertices, uint* pTimes, uint uFrames) -> void;
	auto SetFov(float* pFov, uint* pTimes, uint uFrames) -> void;

private:
	auto ApplyFFT() -> void;

	bool m_bOk;
	std::string m_sName;
	float m_fNearClip;
	float m_fFarClip;

	// Paths
	CPath* m_pPathSrc;
	CPath* m_pPathTgt;
	CPath* m_pPathUp;
	CPath* m_pPathFov;

	// Frame cacheado
	TVector3 m_vCachePosSrc;
	TVector3 m_vCachePosTgt;
	TVector3 m_vCachePosUp;
	float m_fCacheFov;

	// Vars
	int m_iFFTChan;
	float m_fFFTXOff;
	float m_fFFTYOff;
	float m_fFFTZOff;
	float m_fFFTDOff;
	float m_fNoiseScale;
	float m_fNoise;
	bool m_bManualFov;
	float m_fFov;
};

#endif//NEON_GECAMERA3D_H
