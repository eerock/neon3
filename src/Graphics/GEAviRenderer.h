//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_AVIRENDERER_H
#define NEON_AVIRENDERER_H

#if (PLATFORM == PLATFORM_WINDOWS)
#undef  CDECL
#define CDECL __cdecl
#pragma warning(push)
#pragma warning(disable:4201)
#include <vfw.h>
#pragma warning(pop)
#undef  CDECL
#define CDECL
#endif//PLATFORM

class CAviRenderer {
public:
	CAviRenderer();
	~CAviRenderer();

	auto StartRender(CDisplayDevice* pDD, char const* pszFile, uint uFramerate) -> bool;
	auto StopRender() -> bool;
	auto SaveFrame(CDisplayDevice* pDD) -> bool;

	auto IsValid() const -> bool { return m_bIsWorking; }

#if (PLATFORM == PLATFORM_WINDOWS)
private:
	auto MakeDib(HBITMAP hBitmap, UINT uBits) -> HANDLE;
	auto CaptureScreen(CDisplayDevice* pDD) -> HBITMAP;
#endif//PLATFORM

private:
	char* m_pAVIFile;

#if (PLATFORM == PLATFORM_WINDOWS)
	AVISTREAMINFO m_aviStreamInfo;
	PAVIFILE m_pAviFile;
	PAVISTREAM m_pAviStream;
	PAVISTREAM m_pAviStreamCompressed;
	PAVISTREAM m_pAviText;

	AVICOMPRESSOPTIONS m_aviOptions;
	AVICOMPRESSOPTIONS FAR* m_pAviOptions[1];
#endif//PLATFORM

	uint m_uWidth;
	uint m_uHeight;
	uint m_uFrameCount;
	bool m_bIsWorking;
};

#endif//NEON_AVIRENDERER_H
