//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_CUSTOMVAR_H
#define NEON_CUSTOMVAR_H

//---------------------------------------------------------------------------//
//	TCustomVar
//---------------------------------------------------------------------------//
struct TCustomVar {
	enum EType {
		V_BOOL,
		V_INT,
		V_FLOAT,
		INVALID = -1,
	};

	//-----------------------------------------------------------------------//
	TCustomVar()
		: iType(INVALID)
		, iRegister(0)
		, iValue(0)
		, m_fMin(0.f)
		, m_fMax(1.f)
	{}

	//-----------------------------------------------------------------------//
	auto Init(bool bV, int iReg) -> void {
		iType = V_BOOL;
		bValue = bV;
		iRegister = iReg;
	}

	//-----------------------------------------------------------------------//
	auto Init(int iV, int iReg) -> void {
		iType = V_INT;
		iValue = iV;
		iRegister = iReg;
	}

	//-----------------------------------------------------------------------//
	auto Init(float fV, int iReg) -> void {
		iType = V_FLOAT;
		fValue = fV;
		iRegister = iReg;
	}

	//-----------------------------------------------------------------------//
	auto SetMinMax(float fMin, float fMax) -> void {
		if (fMin == fMax) {
			m_fMin = 0.f;
			m_fMax = 1.f;
		} else {
			m_fMin = fMin;
			m_fMax = fMax;
		}
	}

	//-----------------------------------------------------------------------//
	auto SetValue(void const* pData) -> void {
		memcpy(&pValue, pData, 4);
	}

	//-----------------------------------------------------------------------//
	auto GetValue() -> void const* {
		return &pValue;
	}

	//-----------------------------------------------------------------------//
	auto GetFloat() -> float {
		if (iType == V_BOOL) {
			return static_cast<float>(bValue);
		}
		if (iType == V_INT) {
			return static_cast<float>(iValue);
		}
		return m_fMin + fValue * (m_fMax - m_fMin);
	}

	//-----------------------------------------------------------------------//
	// data
	int iType;
	int iRegister;
	union {
		void* pValue;
		bool bValue;
		int iValue;
		float fValue;
	};

	// ebp-> added min/max to make authoring more straightforward.
	float m_fMin;
	float m_fMax;
};

#endif//NEON_CUSTOMVAR_H
