//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GESHADER_H
#define NEON_GESHADER_H

#include "GEDisplayDevice.h"

class CShader {
public:
	enum {
		NEON,
		D3DEFFECT,
	};

	CShader();
	~CShader();

	auto Init(std::string const& sFile) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Release() -> void;
	auto Restore() -> bool;

	auto BeginDraw(uint uTech) -> uint;
	auto EndDraw() -> void;
	auto BeginPass(uint uPass) -> void;
	auto EndPass() -> void;

	auto FixedPipeline() const -> bool;

	auto GetFilename() const -> std::string const& { return m_sFilename; }

	auto AddRef() -> void { m_iCount++; }
	auto SubRef() -> bool { m_iCount--; return (m_iCount > 0); }

	auto SetID(uint id) -> void { m_id = id; }
	auto GetID() const -> uint { return m_id; }

	auto GetNumTechniques() const -> int { return m_iTechniques; }
	auto GetTechniqueName(uint uTech) const -> char const* { return m_pTechniquesName[uTech]; }

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto GetTechnique(uint uTech) -> D3DXHANDLE { return m_pTechniques[uTech]; }
#endif//GFX_ENGINE_DIRECT3D

private:
	auto ResetVars() -> void;
	auto ReleaseVars() -> void;

	auto LoadShaderNeon(std::string const& pszFile) -> bool;
	auto LoadShaderD3D(std::string const& pszFile) -> bool;

	auto GetTextureOp(char const* pValue) -> uint;
	auto GetTextureArg(char const* pValue) -> uint;

	bool m_bOk;
	uint m_id;
	int m_iType;
	int m_iCount;
	int m_iTechniques;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DXHANDLE m_pTechniques[16];
#endif//GFX_ENGINE_DIRECT3D

	char m_pTechniquesName[16][32];

#if (PLATFORM == PLATFORM_WINDOWS)
	char m_Dir[MAX_PATH];
#else
	char m_Dir[FILENAME_MAX];
#endif//PLATFORM
	std::string m_sFilename;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DSTATEBLOCK* m_pStateBlock;
	D3DVS* m_pVS;
	D3DPS* m_pPS;
	LPD3DXEFFECT m_pD3DEffect;
#endif//GFX_ENGINE_DIRECT3D

	// Parametros
	struct TParam {
		int iRegister;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		D3DXHANDLE hParam;
#endif//GFX_ENGINE_DIRECT3D
	};

	int m_iNumParameters;
	TParam* m_pParameters;
};

#endif//NEON_GESHADER_H
