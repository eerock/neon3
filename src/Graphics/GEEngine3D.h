﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEENGINE3D_H
#define NEON_GEENGINE3D_H

namespace CEngine3D {

#define ITEM(e, str, varsize)	e,
	enum {
#include "Enum3DEngineVars.h"
		V_MAX_REGISTERS,
	};
#undef ITEM

	auto Init() -> void;
	auto Release() -> void;

	auto SetRegisterData(int iRegister, float const* pValue) -> void;
	auto GetRegisterData(int iRegister, float* pValue) -> void;
	auto GetRegisterPtr(int iRegister) -> float*;

	auto GetRegisterByName(std::string const& sRegister) -> int;
	auto GetRegisterName(int iRegister, std::string& sRegister) -> bool;
} // namespace CEngine3D

#endif//NEON_GEENGINE3D_H
