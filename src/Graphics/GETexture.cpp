﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"
#include "TGAHeader.h"
#include "libnsgif.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CTexture::CTexture()
	: m_bOk(false)
	, m_id(CMaterialManager::INVALID)
	, m_bLocked(false)
	, m_sFilename()
	, m_iCount(0)
	, m_iWidth(0)
	, m_iHeight(0)
	, m_iFormat(0)
	, m_bDynamic(false)
	, m_fDuration(0.f)
	, m_uUsage(0)
	, m_uPool(0)
	, m_uFlags(0)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	, m_pTexture(nullptr)
	, m_pSurface(nullptr)
#endif//GFX_ENGINE_DIRECT3D
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CTexture::Init(int iWidth, int iHeight, int iFormat, bool bDynamic, bool bRenderTarget, bool bDepthStencil, uint uFlags) -> bool {
	// Datos
	m_sFilename.clear();
	m_bLocked = false;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pTexture = nullptr;
	m_pSurface = nullptr;
#endif//GFX_ENGINE_DIRECT3D
	m_iCount = 0;
	m_iWidth = iWidth;
	m_iHeight = iHeight;
	m_iFormat = iFormat;
	m_bDynamic = bDynamic;
	m_fDuration = 0.f;
	m_uFlags = uFlags;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_uUsage = (bDepthStencil ? D3DUSAGE_DEPTHSTENCIL : 0);
	m_uUsage |= (bRenderTarget ? D3DUSAGE_RENDERTARGET : 0);
	m_uUsage |= (bDynamic ? D3DUSAGE_DYNAMIC : 0);
	m_uPool = (m_uUsage != 0) ? D3DPOOL_DEFAULT : D3DPOOL_MANAGED;
	m_uPool = ((uFlags & SYSTEMMEM) ? D3DPOOL_SYSTEMMEM : m_uPool);
#else
	UNREFERENCED_PARAMETER(bDepthStencil);
	UNREFERENCED_PARAMETER(bRenderTarget);
#endif//GFX_ENGINE

	Restore();

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (m_pTexture) {
		m_bOk = true;
	}
#endif//GFX_ENGINE_DIRECT3D

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CTexture::Init(std::string const& sFile, uint uFlags) -> bool {
	// Nombre
#if (PLATFORM == PLATFORM_WINDOWS)
	GetCurrentDirectory(MAX_PATH, m_Dir);
#endif//PLATFORM

	m_sFilename = sFile;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_pTexture = nullptr;
	m_pSurface = nullptr;
#endif//GFX_ENGINE_DIRECT3D

	m_iCount = 0;
	m_uFlags = uFlags;
	Restore();

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (m_pTexture) {
		m_bOk = true;
	}
#endif//GFX_ENGINE_DIRECT3D

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CTexture::~CTexture() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CTexture\n"));
#endif
	if (IsOk()) {
		ASSERTM(!m_iCount, "A Texture with outstanding references can't be released.");
		Release();
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Release
//---------------------------------------------------------------------------//
auto CTexture::Release() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	SafeRelease(&m_pSurface);
	SafeRelease(&m_pTexture);
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	Restore
//---------------------------------------------------------------------------//
auto CTexture::Restore() -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> Direct3D 9
	LPDIRECT3DDEVICE9 pD3D = g_DisplayDevice.GetD3DDevice();

	if (m_sFilename.empty()) {
		// Create Texture
		//bool bCube = (m_uFlags & CUBE) != 0;
		//if (bCube) {
		//	pD3D->CreateCubeTexture(1, 1, m_uUsage, uFormat, (D3DPOOL)m_uPool, &m_pTexture, nullptr);
		//} else {
		//	pD3D->CreateTexture(m_iWidth, m_iHeight, 1, m_uUsage, uFormat, (D3DPOOL)m_uPool, &m_pTexture, nullptr);
		//}

		HRESULT hres = pD3D->CreateTexture(m_iWidth, m_iHeight, 1, m_uUsage, (D3DFORMAT)m_iFormat, (D3DPOOL)m_uPool, &m_pTexture, nullptr);
		if (FAILED(hres)) {
			D3D_ERROR_NO_ASSERT("D3D Error: ", hres);
		}

		if (m_pTexture) {
			m_pTexture->GetSurfaceLevel(0, &m_pSurface);
			if (m_pSurface) {
				pD3D->ColorFill(m_pSurface, nullptr, 0);
			}
		}
	} else {
		PushDir(m_Dir);
		CFile File;
		if (File.Open(m_sFilename.c_str(), "rb")) {
			int iSize = static_cast<int>(File.GetSize());
			char* pData = NEW_ARRAY(char, iSize);

			File.Read(pData, iSize);
			File.Close();

			HRESULT hRes = D3DXCreateTextureFromFileInMemory(g_DisplayDevice.GetD3DDevice(), pData, iSize, &m_pTexture);
			if (SUCCEEDED(hRes)) {
				D3DXIMAGE_INFO ImageInfo;
				D3DXGetImageInfoFromFileInMemory(pData, iSize, &ImageInfo);
				m_pTexture->GetSurfaceLevel(0, &m_pSurface);
				m_iWidth = ImageInfo.Width;
				m_iHeight = ImageInfo.Height;
				m_bDynamic = false;

				/*
				D3DSURFACE_DESC Desc;
				m_pTexture->GetLevelDesc(0, &Desc);
				m_pSurface = nullptr;
				m_iWidth = Desc.Width;
				m_iHeight = Desc.Height;
				m_bDynamic = false;
				*/
			} else {
				D3D_ERROR_NO_ASSERT("ERROR: D3D Could not create texture from file.", hRes);
			}
			DELETE_ARRAY(pData);
		}
		PopDir();
	}
#endif//GFX_ENGINE_DIRECT3D
}


//---------------------------------------------------------------------------//
//	Lock
//---------------------------------------------------------------------------//
auto CTexture::Lock(TRect const* pRect, TSurfaceDesc& Desc) -> bool {
	ASSERT(!m_bLocked);
	if (m_bLocked) {
		return false;
	}

	bool bRes = false;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> Direct3D 9
	LPDIRECT3DSURFACE9 pSurface;
	if (SUCCEEDED(m_pTexture->GetSurfaceLevel(0, &pSurface))) {
		D3DLOCKED_RECT LockInfo;
		RECT LockRect;
		// Si nos pasan el rectangulo lo usamos
		if (pRect) {
			LockRect.left = pRect->x;
			LockRect.top = pRect->y;
			LockRect.right = pRect->w - pRect->x;
			LockRect.bottom = pRect->h - pRect->y;
		} else {
			LockRect.left = 0;
			LockRect.top = 0;
			LockRect.right = m_iWidth;
			LockRect.bottom = m_iHeight;
		}

		// Todo ok?
		if (SUCCEEDED(pSurface->LockRect(&LockInfo, &LockRect, 0))) {
			Desc.iWidth = m_iWidth;
			Desc.iHeight= m_iHeight;
			Desc.iPitch = LockInfo.Pitch;
			Desc.pBits = LockInfo.pBits;
			m_bLocked = true;
			bRes = true;
		}
		SafeRelease(&pSurface);
	}
#else
	UNREFERENCED_PARAMETER(pRect);
	UNREFERENCED_PARAMETER(Desc);
#endif//GFX_ENGINE
    
	return bRes;
}


//---------------------------------------------------------------------------//
//	Unlock
//---------------------------------------------------------------------------//
auto CTexture::Unlock() -> bool {
	ASSERT(m_bLocked);
	if (!m_bLocked) {
		return false;
	}

	bool bRes = false;
    
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> Direct3D 9
	LPDIRECT3DSURFACE9 pSurface;
	if (SUCCEEDED(m_pTexture->GetSurfaceLevel(0, &pSurface))) {
		if (SUCCEEDED(pSurface->UnlockRect())) {
			m_bLocked = false;
			bRes = true;
		}
		SafeRelease(&pSurface);
	}
#endif//GFX_ENGINE_DIRECT3D
	return bRes;
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CTexture::Set(int iStage) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (m_pTexture) {
		g_DisplayDevice.GetD3DDevice()->SetTexture(iStage, m_pTexture);
	}
#else
	UNREFERENCED_PARAMETER(iStage);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	LoadFromTGA
//---------------------------------------------------------------------------//
auto CTexture::LoadFromTGA(std::string const& pszFilename) -> bool {
	bool bRes = false;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	CFile File;
	if (!File.Open(pszFilename, "rb")) {
		return false;
	}

	TGAHeader Hdr;
	File.Read(&Hdr, sizeof(Hdr));
	if (Hdr.PixelDepth < 24) {
		File.Close();
		return false;
	}

	if (!Init(Hdr.ImageWidth, Hdr.ImageHeight, HARD_TEX_FORMAT_A8R8G8B8)) {
		File.Close();
		return false;
	}

	// Nombre
	m_sFilename = pszFilename;

	TSurfaceDesc Desc;
	if (Lock(nullptr, Desc)) {
		int iSize = m_iWidth * m_iHeight * Hdr.PixelDepth;
		uchar* pTemp = NEW_ARRAY(uchar, iSize);

		File.Read(pTemp, iSize);
		File.Close();

		//-- Leemos la imagen (de abajo a arriba) --//
		//-- es-en: We read the image (from bottom to top) --//
		uint* pDst = (uint*)Desc.pBits;
		//int iPitch = (Desc.iPitch / 4) - m_iWidth;
		if (Hdr.PixelDepth == 32) {
			// 32bpp
			for (int y = m_iHeight; y; y--) {
				uchar* pSrc = pTemp + (y - 1) * m_iWidth * 4;
				for (int x = m_iHeight; x; x--) {
					uchar b = *pSrc++;
					uchar g = *pSrc++;
					uchar r = *pSrc++;
					uchar a = *pSrc++;
					*pDst++ = HARD_COLOR_ARGB(a, r, g, b);
				}
			}
		} else if (Hdr.PixelDepth == 24) {
			// 24bpp
			for (int y = m_iHeight; y; y--) {
				uchar* pSrc = pTemp + (y - 1) * m_iWidth * 4;
				for (int x = m_iHeight; x; x--) {
					uchar b = *pSrc++;
					uchar g = *pSrc++;
					uchar r = *pSrc++;
					*pDst++ = HARD_COLOR_ARGB(255, r, g, b);
				}
			}
		}

		// ebp-> where do we call Unlock after Lock-ing?
		DELETE_ARRAY(pTemp);
		bRes = true;
	}

#else
	UNREFERENCED_PARAMETER(pszFilename);
#endif//GFX_ENGINE
	return bRes;
}



//---------------------------------------------------------------------------//
//	ebp-> i think these functions are for the client to write their own
//	allocator, get raw memory, and deallocator as per the engine.
//	since we're integrating this gif library into Neon, I think these
//	will need to be revisited and ported to use NEW and DISPOSE, same
//	as the rest of the engine.
//---------------------------------------------------------------------------//
auto bitmap_create(int width, int height) -> void*;
auto bitmap_get_buffer(void* bitmap) -> uchar*;
auto bitmap_destroy(void* bitmap) -> void;

//---------------------------------------------------------------------------//
auto bitmap_create(int width, int height) -> void* {
	return calloc(width * height, 4);
}

//---------------------------------------------------------------------------//
auto bitmap_get_buffer(void* bitmap) -> uchar* {
	ASSERT(bitmap);
	return static_cast<uchar*>(bitmap);
}

//---------------------------------------------------------------------------//
auto bitmap_destroy(void* bitmap) -> void {
	ASSERT(bitmap);
	free(bitmap);
}


//---------------------------------------------------------------------------//
//	LoadFromGIF
//	IN: sFilename - name of gif file to load
//	IN/OUT: numFrames - number of frames decoded, or size of CTexture array
//	RET: array of CTexture pointers, representing the animation frames
//---------------------------------------------------------------------------//
auto CTexture::sLoadFromGIF(std::string const& sFilename, int& numFrames) -> CTexture** {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	numFrames = 0;

	CFile File;

	// define some gif decoding callbacks...
	gif_bitmap_callback_vt gif_callbacks = {
		bitmap_create,
		bitmap_destroy,
		bitmap_get_buffer,
		nullptr, nullptr, nullptr
	};

	// open the raw file...
	if (File.Open(sFilename.c_str(), "rb")) {
		gif_animation gif;
		size_t size;
		gif_result code;

		// create a gif decoding structure...
		gif_create(&gif, &gif_callbacks);

		// read the entire raw file into a byte array...
		size = File.GetSize();
		uchar* pData = NEW_ARRAY(uchar, size);
		File.Read(pData, size);
		File.Close();

		// initialize gif structures with file data...
		do {
			code = gif_initialise(&gif, size, pData);
			if (code != GIF_OK && code != GIF_WORKING) {
				GLOG(("WARNING: gif_initialize returned code %d\n", code));
				exit(1);
			}
		} while(code != GIF_OK);

		// rattle off gif dimensions and info...
		//GLOG(("GIF size: (%d x %d)\n", gif.width, gif.height));
		//GLOG(("GIF loops: %d\n", gif.loop_count));
		//GLOG(("GIF frame count: %d   fc partial: %d\n", gif.frame_count, gif.frame_count_partial));

		// create an array to hold the Textures we create...
		CTexture** ppGifFrames = NEW_ARRAY(CTexture*, gif.frame_count);

		// decode the frames...
		for (uint i = 0; i < gif.frame_count; ++i ) {
			uint row, col;
			uchar* image = nullptr;

			code = gif_decode_frame(&gif, i);
			if (code != GIF_OK) {
				GLOG(("WARNING: gif_decode_frame returned code %d\n", code));
				continue;
			}

			// create a texture...
			ppGifFrames[i] = NEW CTexture;
			if (!ppGifFrames[i]->Init(gif.width, gif.height, HARD_TEX_FORMAT_A8R8G8B8)) {
				GLOG(("ERROR: Couldn't initialize texture %d for %s.\n", i, sFilename));
				DELETE_ARRAY(pData);
				DELETE_ARRAY(ppGifFrames);
				return nullptr;
			}

			// lock the surface and write the data...
			TSurfaceDesc Desc;
			if (ppGifFrames[i]->Lock(nullptr, Desc)) {
				image = static_cast<uchar*>(gif.frame_image);
				uint* pDest = static_cast<uint*>(Desc.pBits);

				for (row = 0; row < gif.height; ++row) {
					for (col = 0; col < gif.width; ++col) {
						size_t z = (row * gif.width + col) * 4;
						uchar r = image[z];
						uchar g = image[z + 1];
						uchar b = image[z + 2];
						*pDest++ = HARD_COLOR_ARGB(255, r, g, b);
					}
				}

				ppGifFrames[i]->Unlock();

				// ebp-> set the frame duration here, although this needs further testing and make use of it downstream in the ImageList update...
				ppGifFrames[i]->SetDuration(static_cast<float>(gif.frames[gif.decoded_frame].frame_delay) / 100.f);

				++numFrames;
			}

		}

		// release the gif data...
		gif_finalise(&gif);

		DELETE_ARRAY(pData);

		return ppGifFrames;

	} else {
		GLOG(("ERROR: Couldn't open file %s as GIF.\n", sFilename.c_str()));
	}

#else
	UNREFERENCED_PARAMETER(numFrames);
	UNREFERENCED_PARAMETER(sFilename);
#endif//GFX_ENGINE
	return nullptr;
}

