
#include "Base.h"
#include "GEDisplayDevice_OpenGL.h"

CDisplayDevice_OpenGL::CDisplayDevice_OpenGL() {

}

CDisplayDevice_OpenGL::~CDisplayDevice_OpenGL() {

}

auto CDisplayDevice_OpenGL::Init(TGraphicsMode const& tMode, THWnd hWnd) -> bool {
	UNREFERENCED_PARAMETER(tMode);
	UNREFERENCED_PARAMETER(hWnd);
	return false;
}

auto CDisplayDevice_OpenGL::End() -> void {

}

auto CDisplayDevice_OpenGL::Clear(bool bClearColor, uint uColor, bool bClearZ, float fZ) -> void {
	UNREFERENCED_PARAMETER(bClearColor);
	UNREFERENCED_PARAMETER(uColor);
	UNREFERENCED_PARAMETER(bClearZ);
	UNREFERENCED_PARAMETER(fZ);
	if (bClearColor) {

	}
}

auto CDisplayDevice_OpenGL::ResetMode(TGraphicsMode const& tMode) -> void {
	UNREFERENCED_PARAMETER(tMode);
}

auto CDisplayDevice_OpenGL::Release() -> void {

}

auto CDisplayDevice_OpenGL::PageFlip() -> void {

}

auto CDisplayDevice_OpenGL::BeginFrame() -> void {

}

auto CDisplayDevice_OpenGL::EndFrame() -> void {

}

auto CDisplayDevice_OpenGL::BeginScene() -> void {

}

auto CDisplayDevice_OpenGL::EndScene() -> void {

}

auto CDisplayDevice_OpenGL::SetViewport(TViewport const& viewport, float minZ, float maxZ) -> void {
	UNREFERENCED_PARAMETER(viewport);
	UNREFERENCED_PARAMETER(minZ);
	UNREFERENCED_PARAMETER(maxZ);
}

auto CDisplayDevice_OpenGL::SetRenderTarget(uint uRenderTargetTexID) -> void {
	UNREFERENCED_PARAMETER(uRenderTargetTexID);
}
