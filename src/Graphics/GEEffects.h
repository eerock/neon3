//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEEFFECTS_H
#define NEON_GEEFFECTS_H

#include "GEVector.h"

namespace CEffects {

	enum {
		TEX_TARGET,
		TEX_GLOW_MIPMAP_0,
		TEX_GLOW_PS_0,
		TEX_GLOW_PS_1,
		TEX_MOTION_BLUR,
		TEX_BLUR_0,
		TEX_BLUR_1,
		TEX_TEMP_0,
		TEX_SPRITER,
		TEX_NUM,
	};

	enum {
		SH_BLURXGLOW,
		SH_BLURX,
		SH_BLURY,
		SH_NUM,
	};

	auto Init() -> void;
	auto Release() -> void;
	auto Restore() -> void;

	auto GetTexture(int iTexEnum) -> uint;
	auto GetShader(int iShader) -> uint;

	auto DoRadialBlur(CDisplayDevice* pDD, uint uSourceTexID, int iRadio2, float fIntens, TVector2 const& vPos) -> void;
	auto DoRadialBlurSteps(CDisplayDevice* pDD, uint uSourceTexID, float fRadio, float fIntens, int nSteps, TVector2 const& vPos) -> void;
	auto DoBoxBlur(CDisplayDevice* pDD, uint uSourceTexID, float fRadio) -> void;
	auto DoHorzBlur(CDisplayDevice* pDD, uint uSourceTexID, float fRadio) -> void;
	auto DoVertBlur(CDisplayDevice* pDD, uint uSourceTexID, float fRadio) -> void;
	auto DoHighlights(CDisplayDevice* pDD, uint uSourceTexID, int iRadio, float fUmbral, float fMul) -> void;
	auto DoHighlightsPS(CDisplayDevice* pDD, uint uSourceTexID, int iRadio, float fUmbral, float fMul) -> void;
	auto DoWhiteLightsPS(CDisplayDevice* pDD, uint uSourceTexID, int iRadio, float fUmbral, float fMul) -> void;
	auto DoGlowMipmap(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void;
	auto DoGlowPS(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void;
	auto DoBlur(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void;
	auto DoPixelate(CDisplayDevice* pDD, uint uSourceTexID, float fIntens, float& fU, float& fV) -> uint;
	auto DoGlowPsycho(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void;
	auto DoMotionBlurPass1(CDisplayDevice* pDD, uint uSourceTexID) -> void;
	auto DoMotionBlurPass2(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void;
	auto DoSpriter(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID, uint uSpriteID, float fResolution, float fMinSize, float fMaxSize) -> void;

} //namespace CEffects

#endif//NEON_GEEFFECTS_H
