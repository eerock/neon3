﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GECamera3D.h"
#include "GEEngine3D.h"
#include "PathLinear.h"
#include "PathSpline.h"
#include "CtrlVar.h"

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::EDIT_BOX, 0, "FFT Chan", false, 0, { NULL } },
	{ TCtrlVar::SLIDER, 1, "FFT X-Off", true, 0, { NULL } },
	{ TCtrlVar::SLIDER, 2, "FFT Y-Off", true, 0, { NULL } },
	{ TCtrlVar::SLIDER, 3, "FFT Z-Off", true, 0, { NULL } },
	{ TCtrlVar::SLIDER, 4, "Noise Scale", false, 0, { NULL } },
	{ TCtrlVar::SLIDER, 5, "Noise", true, 0, { NULL } },
	{ TCtrlVar::CHECK_BOX, 6, "Manual Fov", true, 0, { NULL } },
	{ TCtrlVar::SLIDER, 7, "Fov", true, 0, { NULL } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CCamera3D::CCamera3D()
	: m_bOk(false)
	, m_sName()
	, m_fNearClip(0.f)
	, m_fFarClip(0.f)
	, m_pPathSrc(nullptr)
	, m_pPathTgt(nullptr)
	, m_pPathUp(nullptr)
	, m_pPathFov(nullptr)
	, m_vCachePosSrc()
	, m_vCachePosTgt()
	, m_vCachePosUp()
	, m_fCacheFov(0.f)
	, m_iFFTChan(0)
	, m_fFFTXOff(0.f)
	, m_fFFTYOff(0.f)
	, m_fFFTZOff(0.f)
	, m_fFFTDOff(0.f)
	, m_fNoiseScale(0.f)
	, m_fNoise(0.f)
	, m_bManualFov(false)
	, m_fFov(0.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CCamera3D::~CCamera3D() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CCamera3D\n"));
#endif
	if (IsOk()) {
		DELETE_PTR(m_pPathSrc);
		DELETE_PTR(m_pPathTgt);
		DELETE_PTR(m_pPathUp);
		DELETE_PTR(m_pPathFov);
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CCamera3D::Init() -> bool {
	m_sName.clear();
	m_fNearClip = 1.f;
	m_fFarClip = 1000.f;
	m_pPathFov = nullptr;
	m_pPathSrc = nullptr;
	m_pPathUp = nullptr;
	m_pPathTgt = nullptr;
	m_iFFTChan = 0;
	m_fFFTXOff = 0.5f;
	m_fFFTYOff = 0.5f;
	m_fFFTZOff = 0.5f;
	m_fNoiseScale = 0.f;
	m_fNoise = 0.f;
	m_fFov = 0.2f;
	m_bManualFov = false;
	m_fCacheFov = 0.2f;
	m_vCachePosSrc = TVector3(0.f, 100.f, -100.f);
	m_vCachePosTgt = TVector3(0.f, 0.f, 0.f);
	m_vCachePosUp = TVector3(0.f, 1.f, 0.f);

	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CCamera3D::SetTime(float fTime) -> void {
	if (m_pPathSrc) {
		m_pPathSrc->SetTime(fTime);
		m_pPathSrc->GetKeyValue(m_vCachePosSrc);
	}

	if (m_pPathTgt) {
		m_pPathTgt->SetTime(fTime);
		m_pPathTgt->GetKeyValue(m_vCachePosTgt);
	}

	if (m_pPathUp) {
		m_pPathUp->SetTime(fTime);
		m_pPathUp->GetKeyValue(m_vCachePosUp);
	}

	if (m_pPathFov && !m_bManualFov) {
		m_pPathFov->SetTime(fTime);
		m_pPathFov->GetKeyValue(m_fCacheFov);
	}

	// Apply FFT
	ApplyFFT();
}


//---------------------------------------------------------------------------//
//	GetVars
//---------------------------------------------------------------------------//
auto CCamera3D::GetVars() -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CCamera3D::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case 0: {
			if (pData) {
				m_iFFTChan = atoi(static_cast<char const*>(pData));
			}
			break;
		}
		case 1: {
			m_fFFTXOff = *(static_cast<float const*>(pData));
			break;
		}
		case 2: {
			m_fFFTYOff = *(static_cast<float const*>(pData));
			break;
		}
		case 3: {
			m_fFFTZOff = *(static_cast<float const*>(pData));
			break;
		}
		case 4: {
			m_fNoiseScale = *(static_cast<float const*>(pData));
			break;
		}
		case 5: {
			m_fNoise = *(static_cast<float const*>(pData));
			break;
		}
		case 6: {
			m_bManualFov = *(static_cast<bool const*>(pData));
			m_fCacheFov = ((m_fFov * 180.f + 30.f) / 90.f); // De 70 a 170
			break;
		}
		case 7: {
			m_fFov = *(static_cast<float const*>(pData));
			m_fCacheFov = ((m_fFov * 180.f + 30.f) / 90.f); // De 70 a 170
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CCamera3D::GetVar(int iVar) -> void const* {
	static char pBuffer[256];
	switch (iVar) {
		case 0: {
#if (PLATFORM == PLATFORM_WINDOWS)
			_itoa_s(m_iFFTChan, pBuffer, 256, 10);
#else
			pBuffer[0] = 0;
#endif//PLATFORM
			return pBuffer;
		}
		case 1: {
			return &m_fFFTXOff;
		}
		case 2: {
			return &m_fFFTYOff;
		}
		case 3: {
			return &m_fFFTZOff;
		}
		case 4: {
			return &m_fNoiseScale;
		}
		case 5: {
			return &m_fNoise;
		}
		case 6: {
			return &m_bManualFov;
		}
		case 7: {
			return &m_fFov;
		}
		default: {
			return nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	ApplyFFT
//---------------------------------------------------------------------------//
auto CCamera3D::ApplyFFT() -> void {
	float fWave;
	CEngine3D::GetRegisterData(CEngine3D::V_WAVE + m_iFFTChan, &fWave);

	// Actualizar pos segun el offset FFT especificado y el noise
	float fNoise = m_fNoise * m_fNoiseScale * 1000.f;
	m_vCachePosSrc.x += (m_fFFTXOff - 0.5f) * fWave * 1000.f + (rand() & 0xffff) / 65535.f * fNoise;
	m_vCachePosSrc.y += (m_fFFTYOff - 0.5f) * fWave * 1000.f + (rand() & 0xffff) / 65535.f * fNoise;
	m_vCachePosSrc.z += (m_fFFTZOff - 0.5f) * fWave * 1000.f + (rand() & 0xffff) / 65535.f * fNoise;
}


//---------------------------------------------------------------------------//
//	SetPosSrc
//---------------------------------------------------------------------------//
auto CCamera3D::SetPosSrc(TVector3* pVertices, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathSrc);
	m_pPathSrc = NEW CPathSpline;
	if (m_pPathSrc && m_pPathSrc->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathSrc->AddKey((float)pTimes[i], pVertices[i]);
		}
		m_pPathSrc->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathSpline for source position!\n"));
		DELETE_PTR(m_pPathSrc);
		//m_bOk = false;	// ebp-> should i set ok to false here on error?  what affect would that have on the behavior downstream from here?
	}
	// Cache
	m_vCachePosSrc = pVertices[0];
}


//---------------------------------------------------------------------------//
//	SetPosTgt
//---------------------------------------------------------------------------//
auto CCamera3D::SetPosTgt(TVector3* pVertices, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathTgt);
	m_pPathTgt = NEW CPathSpline;
	if (m_pPathTgt && m_pPathTgt->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathTgt->AddKey((float)pTimes[i], pVertices[i]);
		}
		m_pPathTgt->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathSpline for target position!\n"));
		DELETE_PTR(m_pPathTgt);
		//m_bOk = false;	// ebp-> do this?
	}
	// Cache
	m_vCachePosTgt = pVertices[0];
}


//---------------------------------------------------------------------------//
//	SetPosUp
//---------------------------------------------------------------------------//
auto CCamera3D::SetPosUp(TVector3* pVertices, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathUp);
	m_pPathUp = NEW CPathSpline;
	if (m_pPathUp && m_pPathUp->Init(CPath::PATH_3D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathUp->AddKey((float)pTimes[i], pVertices[i]);
		}
		m_pPathUp->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathSpline for up position!\n"));
		DELETE_PTR(m_pPathUp);
		//m_bOk = false;	// ebp-> ??
	}
	// Cache
	m_vCachePosUp = pVertices[0];
}


//---------------------------------------------------------------------------//
//	SetFov
//---------------------------------------------------------------------------//
auto CCamera3D::SetFov(float* pFov, uint* pTimes, uint uFrames) -> void {
	DELETE_PTR(m_pPathFov);
	m_pPathFov = NEW CPathLinear;
	if (m_pPathFov && m_pPathFov->Init(CPath::PATH_1D, uFrames)) {
		for (uint i = 0; i < uFrames; ++i) {
			m_pPathFov->AddKey((float)pTimes[i], pFov[i]);
		}
		m_pPathFov->Initialize();
	} else {
		GLOG(("ERROR: Couldn't instantiate/initialize new CPathLinear for Path Fov!\n"));
		DELETE_PTR(m_pPathFov);
		//m_bOk = false;	// ebp-> ??
	}
	// Cache
	m_fCacheFov = pFov[0];
}
