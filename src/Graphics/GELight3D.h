﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GELIGHT3D_H
#define NEON_GELIGHT3D_H

// ebp-> Direct3D dependencies

class CPath;
class CSceneLoader;
struct TCtrlVar;

class CLight3D {
	friend class CN3DV1Loader;
	friend class CN3DV2Loader;
	friend class C3DSLoader;

	enum EType {
		OMNI,	// AMBIENT?
		DIRECTIONAL,
		SPOT,
	};

public:
	CLight3D();
	~CLight3D();

	auto Init() -> bool;
	auto IsOk() const -> bool { return  m_bOk; }

	auto SetTime(float fTime) -> void;
	auto GetName() const -> std::string const& { return m_sName; }

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> Direct3D 9
	auto GetD3DLight() -> D3DLIGHT9* { return &m_tLight; }
#endif//GFX_ENGINE_DIRECT3D

	static auto GetVars() -> TCtrlVar*;
	auto SetVar(int iVar, void const* pData) -> void;
	auto GetVar(int iVar) -> void const*;

	// Loader
	auto SetPosSrc(TVector3* pVertices, uint* pTimes, uint uFrames) -> void;
	auto SetPosTgt(TVector3* pVertices, uint* pTimes, uint uFrames) -> void;
	auto SetFalloff(float* pFalloff,  uint* pTimes, uint uFrames) -> void;

private:
	auto Update() -> void;
	auto ResetVars() -> void;
	auto ReleaseVars() -> void;

	bool m_bOk;
	std::string m_sName;
	ushort m_uType;
	float m_fR;
	float m_fG;
	float m_fB;
	float m_fIntens;
	float m_fRange;
	CPath* m_pPathSrc;
	CPath* m_pPathTgt;
	CPath* m_pPathFalloff;

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> Direct3D 9
	D3DLIGHT9 m_tLight;
#endif//GFX_ENGINE_DIRECT3D

	// Vars
	float m_fChangeIntens;
	float m_fChangeRange;
};

#endif//NEON_GELIGHT3D_H
