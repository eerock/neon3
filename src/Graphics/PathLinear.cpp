//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "PathLinear.h"

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
// ebp-> Direct3D 9
#include <d3dx9.h>
#endif//GFX_ENGINE_DIRECT3D


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CPathLinear::CPathLinear()
	: m_iKey(0)
	, m_pKeys1(nullptr)
	, m_pKeys2(nullptr)
	, m_pKeys3(nullptr)
	, m_pKeys4(nullptr)
	, m_pTimes(nullptr)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CPathLinear::Init(int iType, int iNumKeys) -> bool {
	m_iKey = 0;
	m_pKeys1 = nullptr;
	m_pKeys2 = nullptr;
	m_pKeys3 = nullptr;
	m_pKeys4 = nullptr;
	m_pTimes = nullptr;

	CPath::Init(iType, iNumKeys);
	
	m_pTimes = NEW_ARRAY(float, m_iMaxKeys);

	switch (iType) {
		case PATH_1D: {
			m_pKeys1 = NEW_ARRAY(float, m_iMaxKeys);
			break;
		}
		case PATH_2D: {
			m_pKeys2 = NEW_ARRAY(TVector2, m_iMaxKeys);
			break;
		}
		case PATH_3D: {
			m_pKeys3 = NEW_ARRAY(TVector3, m_iMaxKeys);
			break;
		}
		case PATH_4D: {
			m_pKeys4 = NEW_ARRAY(TVector4, m_iMaxKeys);
			break;
		}
		default: {
			break;
		}
	}

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CPathLinear::~CPathLinear() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CPathLinear\n"));
#endif
	if (IsOk()) {
		DELETE_ARRAY(m_pTimes);
		DELETE_ARRAY(m_pKeys1);
		DELETE_ARRAY(m_pKeys2);
		DELETE_ARRAY(m_pKeys3);
		DELETE_ARRAY(m_pKeys4);
	}
}



//---------------------------------------------------------------------------//
//	AddKey
//---------------------------------------------------------------------------//
auto CPathLinear::AddKey(float fTime) -> void {
	ASSERT(m_pTimes);
	ASSERT(m_iNumKeys < m_iMaxKeys);

	if (m_iNumKeys < m_iMaxKeys) {
		m_fTimeIni = Min(fTime, m_fTimeIni);
		m_fTimeEnd = Max(fTime, m_fTimeEnd);
		m_fTime = m_fTimeIni;
		m_pTimes[m_iNumKeys] = fTime;
		m_iNumKeys++;
	}
}


//---------------------------------------------------------------------------//
//	AddKey
//---------------------------------------------------------------------------//
auto CPathLinear::AddKey(float fTime, float Key) -> void {
	ASSERT(m_pTimes && m_pKeys1);
	ASSERT(m_iNumKeys < m_iMaxKeys);

	if (m_iNumKeys < m_iMaxKeys) {
		m_fTimeIni = Min(fTime, m_fTimeIni);
		m_fTimeEnd = Max(fTime, m_fTimeEnd);
		m_fTime = m_fTimeIni;
		m_pTimes[m_iNumKeys] = fTime;
		m_pKeys1[m_iNumKeys] = Key;
		m_iNumKeys++;
	}
}


//---------------------------------------------------------------------------//
//	AddKey
//---------------------------------------------------------------------------//
auto CPathLinear::AddKey(float fTime, TVector2 const& Key) -> void {
	ASSERT(m_pTimes && m_pKeys2);
	ASSERT(m_iNumKeys < m_iMaxKeys);

	if (m_iNumKeys < m_iMaxKeys) {
		m_fTimeIni = Min(fTime, m_fTimeIni);
		m_fTimeEnd = Max(fTime, m_fTimeEnd);
		m_fTime = m_fTimeIni;
		m_pTimes[m_iNumKeys] = fTime;
		m_pKeys2[m_iNumKeys] = Key;
		m_iNumKeys++;
	}
}


//---------------------------------------------------------------------------//
//	AddKey
//---------------------------------------------------------------------------//
auto CPathLinear::AddKey(float fTime, TVector3 const& Key) -> void {
	ASSERT(m_pTimes && m_pKeys3);
	ASSERT(m_iNumKeys < m_iMaxKeys);

	if (m_iNumKeys < m_iMaxKeys) {
		m_fTimeIni = Min(fTime, m_fTimeIni);
		m_fTimeEnd = Max(fTime, m_fTimeEnd);
		m_fTime = m_fTimeIni;
		m_pTimes[m_iNumKeys] = fTime;
		m_pKeys3[m_iNumKeys] = Key;
		m_iNumKeys++;
	}
}


//---------------------------------------------------------------------------//
//	AddKey
//---------------------------------------------------------------------------//
auto CPathLinear::AddKey(float fTime, TVector4 const& Key) -> void {
	ASSERT(m_pTimes && m_pKeys4);
	ASSERT(m_iNumKeys < m_iMaxKeys);

	if (m_iNumKeys < m_iMaxKeys) {
		m_fTimeIni = Min(fTime, m_fTimeIni);
		m_fTimeEnd = Max(fTime, m_fTimeEnd);
		m_fTime = m_fTimeIni;
		m_pTimes[m_iNumKeys] = fTime;
        
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		D3DXQuaternionNormalize((D3DXQUATERNION*)&m_pKeys4[m_iNumKeys], (D3DXQUATERNION*)&Key);
#else
		m_pKeys4[m_iNumKeys] = Key;
#endif//GFX_ENGINE
		m_iNumKeys++;
	}
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CPathLinear::SetTime(float fTime) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		// Rango
		while (fTime > m_fTimeEnd) {
			fTime -= (m_fTimeEnd - m_fTimeIni);
		}
		while (fTime < m_fTimeIni) {
			fTime += (m_fTimeEnd - m_fTimeIni);
		}
		m_fTime = fTime;
		SearchKeys();
	}
}


//---------------------------------------------------------------------------//
//	Run
//---------------------------------------------------------------------------//
auto CPathLinear::Run(float fRunTime) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		m_fTime+= fRunTime;
		while (m_fTime > m_fTimeEnd) {
			m_fTime -= (m_fTimeEnd - m_fTimeIni);
		}
		while (m_fTime < m_fTimeIni) {
			m_fTime += (m_fTimeEnd - m_fTimeIni);
		}
		SearchKeys();
	}
}


//---------------------------------------------------------------------------//
//	GetKeyValue
//---------------------------------------------------------------------------//
auto CPathLinear::GetKeyValue(float& rValue) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		int iNextKey = NextKey(m_iKey);
		LinearInterp(m_pTimes[m_iKey], m_pTimes[iNextKey], m_fTime, m_pKeys1[m_iKey], m_pKeys1[iNextKey], rValue);
	} else {
		rValue = m_pKeys1[0];
	}
}


//---------------------------------------------------------------------------//
//	GetKeyValue
//---------------------------------------------------------------------------//
auto CPathLinear::GetKeyValue(TVector2& rValue) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		int iNextKey = NextKey(m_iKey);
		LinearInterp(m_pTimes[m_iKey], m_pTimes[iNextKey], m_fTime, m_pKeys2[m_iKey], m_pKeys2[iNextKey], rValue);
	} else {
		rValue = m_pKeys2[0];
	}
}


//---------------------------------------------------------------------------//
//	GetKeyValue
//---------------------------------------------------------------------------//
auto CPathLinear::GetKeyValue(TVector3& rValue) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		int iNextKey = NextKey(m_iKey);
		LinearInterp(m_pTimes[m_iKey], m_pTimes[iNextKey], m_fTime, m_pKeys3[m_iKey], m_pKeys3[iNextKey], rValue);
	} else {
		rValue = m_pKeys3[0];
	}
}


//---------------------------------------------------------------------------//
//	GetKeyValue
//---------------------------------------------------------------------------//
auto CPathLinear::GetKeyValue(TVector4& rValue) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		int iNextKey = NextKey(m_iKey);
		float fT = (m_fTime - m_pTimes[m_iKey]) / (m_pTimes[iNextKey] - m_pTimes[m_iKey]);
		D3DXQuaternionSlerp((D3DXQUATERNION*)&rValue, (D3DXQUATERNION*)&m_pKeys4[m_iKey], (D3DXQUATERNION*)&m_pKeys4[iNextKey], fT);
#else
        // ebp-> todo: quaternion slerp
#endif//GFX_ENGINE_DIRECT3D
	} else {
		rValue = m_pKeys4[0];
	}
}


//---------------------------------------------------------------------------//
//	GetKeyValues
//---------------------------------------------------------------------------//
auto CPathLinear::GetKeyValues(TVector3 const* pKeys, TVector3* pValues, int iNumKeys) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		int iNextKey = NextKey(CurKey());
		TVector3 const* pKeysA = pKeys + iNumKeys * CurKey();
		TVector3 const* pKeysB = pKeys + iNumKeys * iNextKey;
		float fRA = (m_fTime - m_pTimes[m_iKey]) / (m_pTimes[iNextKey] - m_pTimes[m_iKey]);
		for (int i = 0; i < iNumKeys; i++) {
			pValues->x = pKeysA->x + (pKeysB->x - pKeysA->x) * fRA;
			pValues->y = pKeysA->y + (pKeysB->y - pKeysA->y) * fRA;
			pValues->z = pKeysA->z + (pKeysB->z - pKeysA->z) * fRA;
			pKeysA++;
			pKeysB++;
			pValues++;
		}
	} else {
		memcpy(pValues, pKeys + m_iNumKeys * CurKey(), iNumKeys * sizeof(TVector3));
	}
}


//---------------------------------------------------------------------------//
//	GetKeyValues
//---------------------------------------------------------------------------//
auto CPathLinear::GetKeyValues(TVector2 const* pKeys, TVector2* pValues, int iNumKeys) -> void {
	if (m_iNumKeys > 1 && (m_fTimeEnd > m_fTimeIni)) {
		int iNextKey = NextKey(CurKey());
		TVector2 const* pKeysA = pKeys + iNumKeys * CurKey();
		TVector2 const* pKeysB = pKeys + iNumKeys * iNextKey;
		float fRA = (m_fTime - m_pTimes[m_iKey]) / (m_pTimes[iNextKey] - m_pTimes[m_iKey]);
		for (int i = 0; i < iNumKeys; i++) {
			pValues->x = pKeysA->x + (pKeysB->x - pKeysA->x) * fRA;
			pValues->y = pKeysA->y + (pKeysB->y - pKeysA->y) * fRA;
			pKeysA++;
			pKeysB++;
			pValues++;
		}
	} else {
		memcpy(pValues, pKeys + m_iNumKeys * CurKey(), iNumKeys * sizeof(TVector2));
	}
}


//---------------------------------------------------------------------------//
//	SearchKeys
//---------------------------------------------------------------------------//
auto CPathLinear::SearchKeys() -> void {
	ASSERT (m_fTime >= m_fTimeIni && m_fTime <= m_fTimeEnd);
	m_iKey = 0;
	int iNextKey = NextKey(m_iKey);
	bool bFound = false;
	do {
		if (m_fTime >= m_pTimes[m_iKey] && m_fTime <= m_pTimes[iNextKey]) {
			bFound = true;
		} else {
			m_iKey = iNextKey;
			iNextKey = NextKey(m_iKey);
		}
	} while (!bFound);
}
