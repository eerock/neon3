//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "GEGraphics.h"
#include "GEEffects.h"

using namespace CEffects;

static uint s_uTextureIDs[TEX_NUM];
static uint s_uShaderIDs[SH_NUM];
static int s_iSize = 128;
static int s_iSizeSource = 128;


//---------------------------------------------------------------------------//
//	GetTexture/GetShader
//---------------------------------------------------------------------------//
auto CEffects::GetTexture(int iTexEnum) -> uint {
	ASSERT(ValidIndex(iTexEnum, (int)TEX_NUM));
	return s_uTextureIDs[iTexEnum];
}
auto CEffects::GetShader(int iShader) -> uint {
	ASSERT(ValidIndex(iShader, (int)SH_NUM));
	return s_uShaderIDs[iShader];
}

//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CEffects::Init() -> void {
	for (int i = 0; i < TEX_NUM; ++i) {
		s_uTextureIDs[i] = CMaterialManager::INVALID;
	}
	for (int i = 0; i < SH_NUM; ++i) {
		s_uShaderIDs[i] = CMaterialManager::INVALID;
	}

	// magic fuckin numbers
	s_iSize = 512;
	if (g_DisplayDevice.Width() >= 1024 || g_DisplayDevice.Height() >= 1024) {
		s_iSize = 2048;
	} else if (g_DisplayDevice.Width() >=  512 || g_DisplayDevice.Height() >=  512) {
		s_iSize = 1024;
	}

	s_iSizeSource = s_iSize >> 2;

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Textures
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)	// due to the TEX_FORMAT's)
	s_uTextureIDs[TEX_TARGET] = pMatMgr->AddTexture(s_iSize, s_iSize, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_GLOW_MIPMAP_0] = pMatMgr->AddTexture(g_DisplayDevice.Width() >> 1, g_DisplayDevice.Height() >> 1, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_GLOW_PS_0] = pMatMgr->AddTexture(s_iSizeSource, s_iSizeSource, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_GLOW_PS_1] = pMatMgr->AddTexture(s_iSizeSource, s_iSizeSource, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_MOTION_BLUR] = pMatMgr->AddTexture(g_DisplayDevice.Width() >> 1, g_DisplayDevice.Height() >> 1, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_BLUR_0] = pMatMgr->AddTexture(s_iSizeSource, s_iSizeSource, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_BLUR_1] = pMatMgr->AddTexture(s_iSizeSource, s_iSizeSource, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_TEMP_0] = pMatMgr->AddTexture(g_DisplayDevice.Width() >> 1, g_DisplayDevice.Height() >> 1, HARD_TEX_FORMAT_A8R8G8B8, false, true);
	s_uTextureIDs[TEX_SPRITER] = pMatMgr->AddTexture(g_DisplayDevice.Width(), g_DisplayDevice.Height(), HARD_TEX_FORMAT_A8R8G8B8, false, false, false, CTexture::SYSTEMMEM);
#endif//GFX_ENGINE_DIRECT3D
    
	// Built-in Shaders
	// ebp-> todo: are these necessary anymore?  can they be removed or are they deeply tied into some feature?  investigate
	// update: removed the heat shaders.  Moved these all to the Effects folder.  If we get rid of the FXBlur then BlurX and BlurY
	// can both go away.  BlurXGlow is tied to FXGlow.
	PushDir("Effects");
	s_uShaderIDs[SH_BLURXGLOW] = pMatMgr->AddShader("Neon_BlurXGlow.fx");
	s_uShaderIDs[SH_BLURX] = s_uShaderIDs[SH_BLURY] = pMatMgr->AddShader("Neon_Blur.fx");
	PopDir();
}


//---------------------------------------------------------------------------//
//	Release
//---------------------------------------------------------------------------//
auto CEffects::Release() -> void {
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	for (int i = 0; i < TEX_NUM; ++i) {
		pMatMgr->RemoveTexture(s_uTextureIDs[i]);
		s_uTextureIDs[i] = CMaterialManager::INVALID;
	}
	for (int i = 0; i < SH_NUM; ++i) {
		pMatMgr->RemoveShader(s_uShaderIDs[i]);
		s_uShaderIDs[i] = CMaterialManager::INVALID;
	}
}


//---------------------------------------------------------------------------//
//	Restore
//---------------------------------------------------------------------------//
auto CEffects::Restore() -> void {
	// really?  nothing?
}


//---------------------------------------------------------------------------//
//	DoRadialBlur
//---------------------------------------------------------------------------//
auto CEffects::DoRadialBlur(CDisplayDevice* pDD, uint uSourceTexID, int iRadio2, float fIntens, TVector2 const& vPos) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Calcular el radio^2
	//int nRadius = (int)pow(2, iRadio2);
	//if(nRadius < 1) nRadius = 1;
	int nRadius = iRadio2;
	if (nRadius < 1) {
		nRadius = 1;
	}

	// Tamaños
	auto pSrc = pMatMgr->GetTexture(uSourceTexID);
	auto pDst = pMatMgr->GetTexture(s_uTextureIDs[TEX_TARGET]);
	int iW = pSrc->GetWidth();
	int iH = pSrc->GetHeight();
	RECT Rect = { 0, 0, iW, iH };
	float fU = (float)iW / pDst->GetWidth();
	float fV = (float)iH / pDst->GetHeight();

	// Compute blur direction
	TVector2 vDir;
	vDir.x = (0.5f - vPos.x) * ((float)nRadius / iW) * 2.f;
	vDir.y = (0.5f - vPos.y) * ((float)nRadius / iH) * 2.f;

	// States
	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pDD->SetBilinearFiltering(0, true);
	pMatMgr->SetTexture(s_uTextureIDs[TEX_TARGET], 0);

	float fHalfWidth = (float)iW * 0.5f;
	float fHalfHeight = (float)iH * 0.5f;
	int nCurRadius = 1;
	float fAlpha = fIntens;
	while(nCurRadius < nRadius) {
		pDD->EndScene();
		pD3D->StretchRect(pSrc->GetSurfaceD3D(), &Rect, pDst->GetSurfaceD3D(), &Rect, D3DTEXF_POINT);
		pDD->BeginScene();

		// Calcular el quad
		float fPos = (float)nCurRadius / (float)nRadius;
		float fCenterX = (0.5f + (vDir.x * fPos)) * iW;
		float fCenterY = (0.5f + (vDir.y * fPos)) * iH;

		// Compute some parameters
		float fLeft = fCenterX - fHalfWidth - nCurRadius;
		float fTop = fCenterY - fHalfHeight - nCurRadius;
		float fRight = fCenterX + fHalfWidth  + nCurRadius;
		float fBottom = fCenterY + fHalfHeight + nCurRadius;

		uint uAlpha = static_cast<uint>(fAlpha * 255.f);
		pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (uAlpha << 24));

		// Draw Quad
		neon::DrawQuadUV(pDD, TVector2(fLeft, fTop), TVector2(fRight, fBottom), TVector2(), TVector2(fU, fV));

		nCurRadius = nCurRadius * 2;
	}
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(iRadio2);
	UNREFERENCED_PARAMETER(fIntens);
	UNREFERENCED_PARAMETER(vPos);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoRadialBlurSteps
//---------------------------------------------------------------------------//
auto CEffects::DoRadialBlurSteps(CDisplayDevice* pDD, uint uSourceTexID, float fRadio, float fIntens, int nSteps, TVector2 const& vPos) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	float fRadius = fRadio;
	if (fRadius < 1.f) {
		fRadius = 1.f;
	}

	// Tamaños
	auto pSrc = pMatMgr->GetTexture(uSourceTexID);
	auto pDst = pMatMgr->GetTexture(s_uTextureIDs[TEX_TARGET]);
	int iW = pSrc->GetWidth();
	int iH = pSrc->GetHeight();
	RECT Rect = { 0, 0, iW, iH };
	float fU = (float)iW / pDst->GetWidth();
	float fV = (float)iH / pDst->GetHeight();

	// Compute blur direction
	TVector2 vDir;
	vDir.x = (0.5f - vPos.x) * (fRadius / iW) * 2.f;
	vDir.y = (0.5f - vPos.y) * (fRadius / iH) * 2.f;

	// States
	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pDD->SetBilinearFiltering(0, true);
	pMatMgr->SetTexture(s_uTextureIDs[TEX_TARGET], 0);

	float fHalfWidth = iW * 0.5f;
	float fHalfHeight = iH * 0.5f;
	float fCurRadius = 1;
	float fStep = (fRadius - fCurRadius) / nSteps;
	float fAlpha = fIntens;
	for (int i = 0; i < nSteps; ++i) {
		pDD->EndScene();
		pD3D->StretchRect(pSrc->GetSurfaceD3D(), &Rect, pDst->GetSurfaceD3D(), &Rect, D3DTEXF_POINT);
		pDD->BeginScene();

		// Calcular el quad
		float fPos = fCurRadius / fRadius;
		float fCenterX = (0.5f + (vDir.x * fPos)) * iW;
		float fCenterY = (0.5f + (vDir.y * fPos)) * iH;

		// Compute some parameters
		float fLeft = fCenterX - fHalfWidth - fCurRadius;
		float fTop = fCenterY - fHalfHeight - fCurRadius;
		float fRight = fCenterX + fHalfWidth  + fCurRadius;
		float fBottom = fCenterY + fHalfHeight + fCurRadius;

		uint uAlpha = int(fAlpha * 255.f);
		pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (uAlpha << 24));

		// Draw Quad
		neon::DrawQuadUV(pDD, TVector2(fLeft, fTop), TVector2(fRight, fBottom), TVector2(), TVector2(fU, fV));

		fCurRadius += fStep;
	}
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fRadio);
	UNREFERENCED_PARAMETER(fIntens);
	UNREFERENCED_PARAMETER(nSteps);
	UNREFERENCED_PARAMETER(vPos);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoBoxBlurQuad
//---------------------------------------------------------------------------//
auto DrawBoxBlurQuad(CDisplayDevice* pDD, int iXOffset, int iYOffset) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, 4));

	auto Rect = pDD->GetViewport();
	TVector2 v0((float)Rect.x0, (float)Rect.y0);
	TVector2 v1((float)Rect.x1, (float)Rect.y1);
	float fXD = (float)iXOffset / pDD->ViewportWidth();
	float fYD = (float)iYOffset / pDD->ViewportHeight();

	HCV_SET_ARGB(pVertices, 128, 255, 255, 255);
	HCV_SET_XYZW(pVertices, v0.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, 0.f + fXD, 0.f + fYD);
	HCV_SET_UV1(pVertices, 0.f - fXD, 0.f - fYD);
	++pVertices;

	HCV_SET_ARGB(pVertices, 128, 255, 255, 255);
	HCV_SET_XYZW(pVertices, v1.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, 1.f + fXD, 0.f + fYD);
	HCV_SET_UV1(pVertices, 1.f - fXD, 0.f - fYD);
	++pVertices;

	HCV_SET_ARGB(pVertices, 128, 255, 255, 255);
	HCV_SET_XYZW(pVertices, v1.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, 1.f + fXD, 1.f + fYD);
	HCV_SET_UV1(pVertices, 1.f - fXD, 1.f - fYD);
	++pVertices;

	HCV_SET_ARGB(pVertices, 128, 255, 255, 255);
	HCV_SET_XYZW(pVertices, v0.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, 0.f + fXD, 1.f + fYD);
	HCV_SET_UV1(pVertices, 0.f - fXD, 1.f - fYD);
	++pVertices;

	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLEFAN, 2);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iXOffset);
	UNREFERENCED_PARAMETER(iYOffset);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoBoxBlur
//---------------------------------------------------------------------------//
auto CEffects::DoBoxBlur(CDisplayDevice* pDD, uint uSourceTexID, float fRadio) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();

	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
	pD3D->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);

	pD3D->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_BLENDDIFFUSEALPHA);
	pD3D->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_CURRENT);
	pD3D->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1);

	pMatMgr->SetTexture(s_uTextureIDs[TEX_TEMP_0], 0);		// ebp-> What is this all about?
	pMatMgr->SetTexture(s_uTextureIDs[TEX_TEMP_0], 1);

	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(128, 255, 255, 255));

	int iRadio = static_cast<int>(fRadio);
	int iOffset = 1;
	int iOldOffset = 1;
	while (iOldOffset < iRadio) {
		if (iOffset >= iRadio) {
			int iIntens = 128 - static_cast<int>((static_cast<float>(iOffset - iRadio) * 128.f) / static_cast<float>(iOldOffset));
			pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(iIntens, 255, 255, 255));
		}

		auto pSrc = pMatMgr->GetTexture(uSourceTexID)->GetSurfaceD3D();
		auto pDst = pMatMgr->GetTexture(TEX_TEMP_0)->GetSurfaceD3D();
		pD3D->StretchRect(pSrc, nullptr, pDst, nullptr, D3DTEXF_POINT);

		DrawBoxBlurQuad(pDD, iOffset, 0);
		DrawBoxBlurQuad(pDD, 0, iOffset);
		iOldOffset = iOffset;
		iOffset = iOffset << 1;
	}
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fRadio);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoGlowMipmap
//---------------------------------------------------------------------------//
auto CEffects::DoGlowMipmap(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Pintaremos encima de esta
	pDD->SetRenderTarget(s_uTextureIDs[TEX_GLOW_MIPMAP_0]);
	pDD->ApplyBasicShader();
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(255, 128, 128, 128));

	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);

	pD3D->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);

	pD3D->SetTextureStageState(2, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(2, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);

	pD3D->SetTextureStageState(3, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(3, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetTextureStageState(3, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);

	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TFACTOR);

	pD3D->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_CURRENT);
	pD3D->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TEXTURE);

	pD3D->SetTextureStageState(2, D3DTSS_COLOROP, D3DTOP_MODULATE4X);
	pD3D->SetTextureStageState(2, D3DTSS_COLORARG1, D3DTA_CURRENT);
	pD3D->SetTextureStageState(2, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	pD3D->SetTextureStageState(3, D3DTSS_COLOROP, D3DTOP_MODULATE4X);
	pD3D->SetTextureStageState(3, D3DTSS_COLORARG1, D3DTA_CURRENT);
	pD3D->SetTextureStageState(3, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	//auto pTex = pMatMgr->GetTexture(s_pTexturas[TEX_GLOW_MIPMAP_0]);
	//pMatMgr->GetTexture(s_pTexturas[TEX_GLOW_MIPMAP_0]);
	pMatMgr->SetTexture(uSourceTexID, 0);
	pMatMgr->SetTexture(uSourceTexID, 1);
	pMatMgr->SetTexture(uSourceTexID, 2);
	pMatMgr->SetTexture(uSourceTexID, 3);

	neon::DrawQuad(pDD);
	pDD->DeactivateStage(1);
	pDD->DeactivateStage(2);
	pDD->DeactivateStage(3);

	DoBoxBlur(pDD, s_uTextureIDs[TEX_GLOW_MIPMAP_0], 24);

	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(static_cast<uint>(fIntens * 255.f), 0, 0, 0));
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);

	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	pMatMgr->SetTexture(s_uTextureIDs[TEX_GLOW_MIPMAP_0], 0);
	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoGlowPS
//---------------------------------------------------------------------------//
auto CEffects::DoGlowPS(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Aplicar un filtro de blur en X
	pDD->ApplyBasicShader();
	pDD->SetRenderTarget(s_uTextureIDs[TEX_GLOW_PS_0]);
	pMatMgr->SetTexture(uSourceTexID, 0);

	// Shader
	auto pShader = pMatMgr->GetShader(s_uShaderIDs[SH_BLURXGLOW]);
	auto uPasses = pShader->BeginDraw(0);
	for (uint uPass = 0; uPass < uPasses; ++uPass) {
		pShader->BeginPass(uPass);
		neon::DrawQuad(pDD, TVector2(), TVector2((float)s_iSizeSource, (float)s_iSizeSource));
		pShader->EndPass();
	}
	pShader->EndDraw();

	// Aplicar un filtro de blur en Y
	pDD->SetRenderTarget(s_uTextureIDs[TEX_GLOW_PS_1]);
	pMatMgr->SetTexture(s_uTextureIDs[TEX_GLOW_PS_0], 0);
	// Shader
	pShader = pMatMgr->GetShader(s_uShaderIDs[SH_BLURY]);
	uPasses = pShader->BeginDraw(1);	// 0 = BLURY technique
	for (uint uPass = 0; uPass < uPasses; ++uPass) {
		pShader->BeginPass(uPass);
		neon::DrawQuad(pDD, TVector2(), TVector2((float)s_iSizeSource, (float)s_iSizeSource));
		pShader->EndPass();
	}
	pShader->EndDraw();

	uint uAlpha = static_cast<uint>(fIntens * 255.f);
	// Rendear el resultado encima de la textura
	pDD->ApplyBasicShader();
	pDD->SetRenderTarget(uSourceTexID);
	pMatMgr->SetTexture(s_uTextureIDs[TEX_GLOW_PS_1], 0);

	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (uAlpha << 24));
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoGlowPsycho
//---------------------------------------------------------------------------//
auto CEffects::DoGlowPsycho(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	pDD->EndScene();

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Copiar el source en varias texturas
	auto pTexSrc = pMatMgr->GetTexture(uSourceTexID);
	auto pSrc = pTexSrc->GetSurfaceD3D();
	auto pDst0 = pMatMgr->GetTexture(s_uShaderIDs[TEX_GLOW_PS_0])->GetSurfaceD3D();

	pD3D->StretchRect(pSrc, nullptr, pDst0, nullptr, D3DTEXF_POINT);

	// Los siguientes los pintamos en aditivo
	pDD->ApplyBasicShader();
	uint uAlpha = static_cast<uint>(fIntens * 255.f);
	if (uAlpha > 255) {
		uAlpha = 255;
	}
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);

	float fX = 0.f;
	float fY = 0.f;
	float fW = static_cast<float>(pTexSrc->GetWidth());
	float fH = static_cast<float>(pTexSrc->GetHeight());

	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB(uAlpha, 110, 110, 110));
	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TFACTOR);

	// Dibujar el mas pequeño (mas blurreado)
	int iNumPasses = 3;
	pMatMgr->SetTexture(s_uTextureIDs[TEX_GLOW_PS_0], 0);
	// Preparar los vertices
	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, 18 * iNumPasses));
	float fDist = 16.f;
	float fHDist = fDist * 0.5f;
	for (int i = 0; i < iNumPasses; ++i, pVertices += 18) {
		fY = -fDist;
		neon::PrepareVertices(pVertices, TVector2(fX, fY), TVector2(fX + fW, fY + fH));
		fY = fHDist;
		fX = -fHDist;
		neon::PrepareVertices(pVertices + 6, TVector2(fX, fY), TVector2(fX + fW, fY + fH));
		fY = fHDist;
		fX = fHDist;
		neon::PrepareVertices(pVertices + 12, TVector2(fX, fY), TVector2(fX + fW, fY + fH));
	}

	pDD->UnlockVertexBuffer(HCV_XYZ_T);

	// Pintaremos encima de esta
	pDD->SetRenderTarget(uSourceTexID);
	pDD->BeginScene();
	pDD->DrawPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, 6 * iNumPasses);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoBlur
//---------------------------------------------------------------------------//
auto CEffects::DoBlur(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Aplicar un filtro de blur en X
	pDD->ApplyBasicShader();
	pDD->SetRenderTarget(s_uTextureIDs[TEX_BLUR_0]);
	pMatMgr->SetTexture(uSourceTexID, 0);

	// Shader
	auto pShader = pMatMgr->GetShader(s_uShaderIDs[SH_BLURX]);
	auto uPasses = pShader->BeginDraw(0);	// 0 = BLURX technique
	for (uint uPass = 0; uPass < uPasses; ++uPass) {
		pShader->BeginPass(uPass);
		neon::DrawQuad(pDD);
		pShader->EndPass();
	}
	pShader->EndDraw();

	// Aplicar un filtro de blur en Y
	pDD->SetRenderTarget(s_uTextureIDs[TEX_BLUR_1]);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(s_uTextureIDs[TEX_BLUR_0], 0);

	// Shader
	pShader = pMatMgr->GetShader(s_uShaderIDs[SH_BLURY]);
	uPasses = pShader->BeginDraw(1);	// 1 = BLURY technique
	for (uint uPass = 0; uPass < uPasses; ++uPass) {
		pShader->BeginPass(uPass);
		neon::DrawQuad(pDD);
		pShader->EndPass();
	}
	pShader->EndDraw();

	uint uAlpha = static_cast<uint>(fIntens * 1.2f * 255.f);
	if (uAlpha > 255) {
		uAlpha = 255;
	}
	// Rendear el resultado encima de la textura
	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(s_uTextureIDs[TEX_BLUR_1], 0);

	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (uAlpha << 24));
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoHorzBlur
//---------------------------------------------------------------------------//
auto CEffects::DoHorzBlur(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Aplicar un filtro de blur en X
	pDD->SetRenderTarget(s_uTextureIDs[TEX_BLUR_0]);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uSourceTexID, 0);

	// Shader
	auto pShader = pMatMgr->GetShader(s_uShaderIDs[SH_BLURX]);
	auto uPasses = pShader->BeginDraw(0);	// 0 = BLURX technique
	for (uint uPass = 0; uPass < uPasses; ++uPass) {
		pShader->BeginPass(uPass);
		neon::DrawQuad(pDD, TVector2(), TVector2((float)s_iSizeSource, (float)s_iSizeSource));
		pShader->EndPass();
	}
	pShader->EndDraw();

	uint uAlpha = static_cast<uint>(fIntens * 255.f);
	// Rendear el resultado encima de la textura
	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(s_uTextureIDs[TEX_BLUR_0], 0);

	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (uAlpha << 24));
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoVertBlur
//---------------------------------------------------------------------------//
auto CEffects::DoVertBlur(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	// Aplicar un filtro de blur en X
	pDD->SetRenderTarget(s_uTextureIDs[TEX_BLUR_0]);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uSourceTexID, 0);

	// Shader
	auto pShader = pMatMgr->GetShader(s_uShaderIDs[SH_BLURY]);
	auto uPasses = pShader->BeginDraw(1);	// 1 = BLURY technique
	for (uint uPass = 0; uPass < uPasses; ++uPass) {
		pShader->BeginPass(uPass);
		neon::DrawQuad(pDD, TVector2(), TVector2((float)s_iSizeSource, (float)s_iSizeSource));
		pShader->EndPass();
	}
	pShader->EndDraw();

	uint uAlpha = static_cast<uint>(fIntens * 255.f);
	// Rendear el resultado encima de la textura
	pDD->SetRenderTarget(uSourceTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(s_uTextureIDs[TEX_BLUR_0], 0);

	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, (uAlpha << 24));
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoPixelate
//---------------------------------------------------------------------------//
auto CEffects::DoPixelate(CDisplayDevice* pDD, uint uSourceTexID, float fIntens, float& fU, float& fV) -> uint {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pTex = pMatMgr->GetTexture(uSourceTexID);

	pDD->ApplyBasicShader();
	fU = fIntens * pTex->GetWidth();
	fV = fIntens * pTex->GetHeight();
	pDD->SetRenderTarget(s_uTextureIDs[TEX_TARGET]);
	pDD->SetBilinearFiltering(0, false);
	pMatMgr->SetTexture(uSourceTexID, 0);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	neon::DrawQuad(pDD, TVector2(), TVector2(fU, fV));
	pTex = pMatMgr->GetTexture(s_uTextureIDs[TEX_TARGET]);
	fU /= pTex->GetWidth();
	fV /= pTex->GetHeight();
	return s_uTextureIDs[TEX_TARGET];

	/*
	auto pSrc = pTex->GetSurfaceD3D();
	auto pDst = pMatMgr->GetTexture(m_iTextura)->GetSurfaceD3D();
	RECT RectDest;
	RectDest.left = 0;
	RectDest.top = 0;
	RectDest.right = (1.f - (m_fIntensAct * 0.8f)) * pTex->GetWidth ();
	RectDest.bottom= (1.f - (m_fIntensAct * 0.8f)) * pTex->GetHeight();
	pD3D->StretchRect(pSrc,  nullptr, pDst, &RectDest, D3DTEXF_POINT);
	*/
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
	UNREFERENCED_PARAMETER(fU);
	UNREFERENCED_PARAMETER(fV);
	return s_uTextureIDs[TEX_TARGET];
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoMotionBlurPass1
//---------------------------------------------------------------------------//
auto CEffects::DoMotionBlurPass1(CDisplayDevice* pDD, uint uSourceTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pSrc = pMatMgr->GetTexture(uSourceTexID)->GetSurfaceD3D();
	auto pDst = pMatMgr->GetTexture(s_uTextureIDs[TEX_MOTION_BLUR])->GetSurfaceD3D();
	pDD->GetD3DDevice()->StretchRect(pSrc, nullptr, pDst, nullptr, D3DTEXF_POINT);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoMotionBlurPass2
//---------------------------------------------------------------------------//
auto CEffects::DoMotionBlurPass2(CDisplayDevice* pDD, uint uSourceTexID, float fIntens) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	pDD->SetRenderTarget(uSourceTexID);

	pDD->ApplyBasicShader();
	pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TFACTOR);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetRenderState (D3DRS_TEXTUREFACTOR, HARD_COLOR_ARGB((int)(fIntens * 255.f), 2, 2, 2));

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	pMatMgr->SetTexture(s_uTextureIDs[TEX_MOTION_BLUR], 0);

	auto Rect = pDD->GetViewport();
	neon::DrawQuad(pDD, TVector2((float)Rect.x0 - 0.5f, (float)Rect.y0 - 0.5f), TVector2((float)Rect.x1 - 0.5f, (float)Rect.y1 - 0.5f));
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(fIntens);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DoSpriter
//---------------------------------------------------------------------------//
auto CEffects::DoSpriter(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID, uint uSpriteID, float fResolution, float fMinSize, float fMaxSize) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pTexSrc = pMatMgr->GetTexture(uSourceTexID);
	auto pTexDst = pMatMgr->GetTexture(s_uTextureIDs[TEX_SPRITER]);

	pD3D->GetRenderTargetData(pTexSrc->GetSurfaceD3D(), pTexDst->GetSurfaceD3D());

	auto Rect = pDD->GetViewport();
	float fRatio = (Rect.x1 - Rect.x0) / 512.f;
	TSurfaceDesc Desc;
	if (pTexDst->Lock(nullptr, Desc)) {
		uint* pBits = static_cast<uint*>(Desc.pBits);
		int iResX = static_cast<int>(fResolution * 50.f) + 20;
		int iResY = (iResX * 3) >> 2;
		int iDX = (pTexSrc->GetWidth () << 16) / iResX;
		int iDY = (pTexSrc->GetHeight() << 16) / iResY;
		int iY = iDY >> 1;
		auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, iResX * iResY));
		for (int j = 0; j < iResY; ++j) {
			int iX = iDX >> 1;
			int iCY = iY >> 16;
			int iBase = iCY * pTexSrc->GetWidth();
			for (int i = 0; i < iResX; ++i) {
				int iCX = iX >> 16;
				uint uColor = pBits[iBase + iCX];
				uint uSize = HARD_COLOR_GET_R(uColor) + HARD_COLOR_GET_G(uColor) + HARD_COLOR_GET_B(uColor);
				float fSize = ((uSize / 768.f) * (fMaxSize - fMinSize) + fMinSize) * fRatio;
				HCV_SET_XYZW(pVertices, static_cast<float>(iCX), static_cast<float>(iCY), 0.f, 1.f);
				HCV_SET_COLOR(pVertices, uColor);
				HCV_SET_PSIZE(pVertices, fSize);
				++pVertices;
				iX += iDX;
			}
			iY += iDY;
		}
		pDD->UnlockVertexBuffer(HCV_XYZ_T);
		pTexDst->Unlock();

		pDD->ApplyBasicShader();
		pDD->SetRenderTarget(uTargetTexID);
		pDD->SetBilinearFiltering(0, true);
		pD3D->SetRenderState(D3DRS_CLIPPING, TRUE);
		pD3D->SetRenderState(D3DRS_ZENABLE, FALSE);
		pD3D->SetRenderState(D3DRS_POINTSPRITEENABLE, TRUE);
		pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
		pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

		pMatMgr->SetTexture(uSpriteID, 0);
		pDD->Clear(true, false, 0, 0.f);

		pDD->DrawPrimitive(HCV_XYZ_T, HARD_PRIM_POINTLIST, iResX * iResY);
	}
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
	UNREFERENCED_PARAMETER(uSpriteID);
	UNREFERENCED_PARAMETER(fResolution);
	UNREFERENCED_PARAMETER(fMinSize);
	UNREFERENCED_PARAMETER(fMaxSize);
#endif//GFX_ENGINE
}
