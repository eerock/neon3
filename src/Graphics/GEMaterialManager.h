﻿//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_GEMATERIALMANAGER_H
#define NEON_GEMATERIALMANAGER_H

//#include <vector>

class CMaterial;
class CTexture;
class CShader;


//---------------------------------------------------------------------------//
//	Class IMaterialManagerService
//	ebp-> service provider interface, in order to remove the global pointer
//	used everywhere, it will amount to slightly more code in the long run.
//---------------------------------------------------------------------------//
class IMaterialManagerService {
public:
	virtual ~IMaterialManagerService() {}

	// Release
	virtual auto Release() -> void = 0;
	virtual auto Restore() -> void = 0;

	// Material Interface
	virtual auto AddMaterial(std::string const& sFile) -> uint = 0;
	virtual auto RemoveMaterial(uint iMat) -> void = 0;
	virtual auto GetMaterial(uint iMat) -> CMaterial* = 0;

	// Shader Interface
	virtual auto AddShader(std::string const& sFile) -> uint = 0;
	virtual auto RemoveShader(uint iShader) -> void = 0;
	virtual auto GetShader(uint iShader) -> CShader* = 0;

	// Textura Interface
	virtual auto AddTexture(std::string const& sFile, uint uFlags = 0) -> uint = 0;
	virtual auto AddTexture(CTexture* pTex, uint uFlags = 0) -> uint = 0;
	virtual auto AddTexture(int iW, int iH, int iFormat, bool bDynamic = false, bool bRenderTgt = false, bool bDepthStencil = false, uint uFlags = 0) -> uint = 0;
	virtual auto RemoveTexture(uint iTex) -> void = 0;
	virtual auto GetTexture(uint iTex) -> CTexture* = 0;
	virtual auto SetTexture(uint iTex, int iStage) -> void = 0;
};


//---------------------------------------------------------------------------//
//	Material Manager Service Provider Implementation
//---------------------------------------------------------------------------//
class CMaterialManager : public IMaterialManagerService {
public:
	CMaterialManager();
	~CMaterialManager() override;

	auto Init() -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Release() -> void;
	auto Restore() -> void;

	// Materials
	auto AddMaterial(std::string const& sFile) -> uint override;
	auto RemoveMaterial(uint uMaterialID) -> void override;		// maybe this takes a uint& and resets it to INVALID when it's been removed.
	auto GetMaterial(uint uMaterialID) -> CMaterial* override;

	// Textures
	auto AddTexture(std::string const& sFile, uint uFlags = 0) -> uint override;
	auto AddTexture(CTexture* pTex, uint uFlags = 0) -> uint override;
	auto AddTexture(int iWidth, int iHeight, int iFormat, bool bDynamic = false, bool bRenderTarget = false, bool bDepthStencil = false, uint uFlagss = 0) -> uint override;
	auto RemoveTexture(uint uTextureID) -> void override;	// maybe this takes a uint& and resets it to INVALID when it's been removed.
	auto GetTexture(uint uTextureID) -> CTexture* override;
	auto SetTexture(uint uTextureID, int iStage) -> void override;

	// Shaders
	auto AddShader(std::string const& sFile) -> uint override;
	auto RemoveShader(uint uShaderID) -> void override;	// maybe this takes a uint& and resets it to INVALID when it's been removed.
	auto GetShader(uint uShaderID) -> CShader* override;

	static const uint INVALID = 0;

private:
	auto ResetVars() -> void;
	auto ReleaseVars() -> void;

	bool m_bOk;

	std::map<uint, std::unique_ptr<CMaterial>> m_mMaterials;
	std::map<uint, std::unique_ptr<CTexture>> m_mTextures;
	std::map<uint, std::unique_ptr<CShader>> m_mShaders;

	// id counters...
	uint m_uNextMaterialID;
	uint m_uNextTextureID;
	uint m_uNextShaderID;

	// Default materials
	uint m_uFlatMaterialID;
	uint m_uWireMaterialID;
};

#endif//NEON_GEMATERIALMANAGER_H
