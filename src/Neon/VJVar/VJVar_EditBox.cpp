//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "Controls/MGEditBox.h"
#include "Controls/MGLabel.h"
#include "VJVar_EditBox.h"
#include "CtrlVar.h"
#include "LinkSystem/Link.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar_EditBox::CVJVar_EditBox(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject)
	: CVJVar(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject) {
	m_iType = TCtrlVar::EDIT_BOX;
	m_pEditBox = NEW CMGEditBox(pParent, VJVAR_CONTROL_XPOS, ypos, 100, bind(&CVJVar_EditBox::OnChange, this));
	m_pControl = m_pEditBox;
	Set(pSource->GetVar(iScope, iObject, iVar));
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar_EditBox::CVJVar_EditBox(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID)
	: CVJVar(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID) {
	m_iType = TCtrlVar::EDIT_BOX;
	m_pEditBox = NEW CMGEditBox(pParent, VJVAR_CONTROL_XPOS, ypos, 100, bind(&CVJVar_EditBox::OnChange, this));
	m_pControl = m_pEditBox;
	Set(pEffect->GetVar(iVar));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJVar_EditBox::~CVJVar_EditBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJVar_EditBox\n"));
#endif
	m_pControl = nullptr;
	m_pEditBox = nullptr;
	//DELETE_PTR(m_pEditBox);
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CVJVar_EditBox::Set(void const* pData) -> void {
	if (m_pEditBox) {
		m_pEditBox->SetText(static_cast<char const*>(pData));
	}
}


//---------------------------------------------------------------------------//
//	OnChange
//---------------------------------------------------------------------------//
auto CVJVar_EditBox::OnChange() -> void {
	if (IsEffectVar()) {
		m_pEffect->SetVar(m_iVar, m_pEditBox->GetText().c_str());
	} else {
		m_pSource->SetVar(m_iScope, m_iObject, m_iVar, m_pEditBox->GetText().c_str());

		//if (m_pSource->SnapshotNeedsUpdate()) {
		//	// do stuff...?
		//}
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CVJVar_EditBox::Load(CNodeFile::CNode* pNode) -> void {
	pNode = pNode->FirstNode("var");
	while (pNode && Stricmp(pNode->AttrAsString("name"), m_pLabel->Get())) {
		pNode = pNode->NextNode("var");
	}

	if (pNode) {
		m_pEditBox->SetText(pNode->AsString("value"));
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CVJVar_EditBox::Save(CNodeFile* pFile) -> void {
	pFile->WriteOpenNode("var", "name=\"" + m_pLabel->Get() + "\"");
	pFile->WriteNode("value", "", m_pEditBox->GetText());
	pFile->WriteCloseNode();
}
