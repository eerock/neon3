//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "Controls/MGButton.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGLabel.h"
#include "VJVar_Checkbox.h"
#include "CtrlVar.h"
#include "LinkSystem/Link.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar_CheckBox::CVJVar_CheckBox(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject)
	: CVJVar(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject) {
	m_iType = TCtrlVar::CHECK_BOX;
	m_pCheckBox = NEW CMGCheckBox(pParent, VJVAR_CONTROL_XPOS, ypos, "", true, false, bind(&CVJVar_CheckBox::OnChange, this));
	m_pControl = m_pCheckBox;
	Set(m_pSource->GetVar(iScope, iObject, iVar));
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar_CheckBox::CVJVar_CheckBox(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID)
	: CVJVar(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID) {
	m_iType = TCtrlVar::CHECK_BOX;
	m_pCheckBox = NEW CMGCheckBox(pParent, VJVAR_CONTROL_XPOS, ypos, "", true, false, bind(&CVJVar_CheckBox::OnChange, this));
	m_pControl = m_pCheckBox;
	Set(m_pEffect->GetVar(iVar));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJVar_CheckBox::~CVJVar_CheckBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJVar_CheckBox\n"));
#endif
	m_pControl = nullptr;
	m_pCheckBox = nullptr;
	//DELETE_PTR(m_pCheckBox);
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CVJVar_CheckBox::Set(void const* pData) -> void {
	if (m_pCheckBox) {
		m_pCheckBox->SetChecked(*(static_cast<bool const*>(pData)));
	}
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CVJVar_CheckBox::Evaluate() -> void{
	if (m_pLink && m_pLink->Evaluate()) {
		bool bChecked = m_pLink->ValueCheckBox();
		m_pCheckBox->SetChecked(bChecked);
		if (IsEffectVar()) {
			m_pEffect->SetVar(m_iVar, &bChecked);
		} else {
			m_pSource->SetVar(m_iScope, m_iObject, m_iVar, &bChecked);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnChange
//---------------------------------------------------------------------------//
auto CVJVar_CheckBox::OnChange() -> void {
	bool bChecked = m_pCheckBox->Checked();
	if (IsEffectVar()) {
		m_pEffect->SetVar(m_iVar, &bChecked);
	} else {
		m_pSource->SetVar(m_iScope, m_iObject, m_iVar, &bChecked);

		//if (m_pSource->SnapshotNeedsUpdate()) {
		//	// do stuff...?
		//}
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CVJVar_CheckBox::Load(CNodeFile::CNode* pNode) -> void {
	pNode = pNode->FirstNode("var");
	while (pNode && Stricmp(pNode->AttrAsString("name"), m_pLabel->Get()) != 0) {
		pNode = pNode->NextNode("var");
	}

	if (pNode) {
		m_pCheckBox->SetChecked(pNode->AsBool("value"));
		auto pNodeLink = pNode->FirstNode("link");
		if (pNodeLink) {
			m_pLink = NEW CLink(nullptr, nullptr);
			m_pLink->Load(pNodeLink);
		}
	}

	// ebp-> check for null here, because of bLinkable flag doesn't 
	// always create this button in VJVar...
	if (m_pBtnLink) {
		m_pBtnLink->SetLinked(m_pLink != nullptr);
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CVJVar_CheckBox::Save(CNodeFile* pFile) -> void {
	pFile->WriteOpenNode("var", "name=\"" + m_pLabel->Get() + "\"");
	pFile->WriteNode("value", "", m_pCheckBox->Checked());
	if (m_pLink) {
		m_pLink->Save(pFile, "link");
	}
	pFile->WriteCloseNode();
}
