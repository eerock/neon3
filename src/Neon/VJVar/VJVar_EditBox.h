//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VJVAR_EDITBOX_H
#define NEON_VJVAR_EDITBOX_H

#include "VJVar.h"

struct TCtrlVar;
class CMGEditBox;

class CVJVar_EditBox : public CVJVar {
public:
	// ebp-> new code
	CVJVar_EditBox(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject);
	CVJVar_EditBox(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID);

	virtual ~CVJVar_EditBox() override;

	auto Set(void const* pData) -> void;
	auto Evaluate() -> void override {}
	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;

private:
	void OnChange();

	CMGEditBox* m_pEditBox;
};

#endif//NEON_VJVAR_EDITBOX_H
