//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"

#include "Controls/MGButton.h"
#include "Controls/MGLabel.h"
#include "Controls/MGTrackBar.h"
#include "CtrlVar.h"
#include "LinkSystem/Link.h"
#include "NeonMain.h"
#include "MGSource.h"
#include "VJVar_Slider.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar_Slider::CVJVar_Slider(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject)
	: CVJVar(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject) {
	m_iType = TCtrlVar::SLIDER;
	m_pTrackBar = NEW CMGTrackBar(pParent, VJVAR_CONTROL_XPOS, ypos + 2, 175, false, bind(&CVJVar_Slider::OnChange, this));
	m_pControl = m_pTrackBar;
	Set(pSource->GetVar(iScope, iObject, iVar));
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar_Slider::CVJVar_Slider(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID)
	: CVJVar(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID) {
	m_iType = TCtrlVar::SLIDER;
	m_pTrackBar = NEW CMGTrackBar(pParent, VJVAR_CONTROL_XPOS, ypos + 2, 175, false, bind(&CVJVar_Slider::OnChange, this));
	m_pControl = m_pTrackBar;
	Set(pEffect->GetVar(iVar));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJVar_Slider::~CVJVar_Slider() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJVar_Slider\n"));
#endif
	m_pControl = nullptr;
	m_pTrackBar = nullptr;
	//DELETE_PTR(m_pTrackBar);
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CVJVar_Slider::Set(void const* pData) -> void {
	if (m_pTrackBar) {
		m_pTrackBar->SetPos(*(static_cast<float const*>(pData)));
	}
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CVJVar_Slider::Evaluate() -> void {
	if (m_pLink && m_pLink->Evaluate()) {
		float fPos = m_pLink->ValueTrackBar();
		m_pTrackBar->SetPos(fPos);
		if (IsEffectVar()) {
			m_pEffect->SetVar(m_iVar, &fPos);
		} else {
			m_pSource->SetVar(m_iScope, m_iObject, m_iVar, &fPos);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnChange
//---------------------------------------------------------------------------//
auto CVJVar_Slider::OnChange() -> void {
	float fPos = m_pTrackBar->GetPos();
	if (IsEffectVar()) {
		m_pEffect->SetVar(m_iVar, &fPos);
	} else {
		m_pSource->SetVar(m_iScope, m_iObject, m_iVar, &fPos);

		if (m_pSource->SnapshotNeedsUpdate()) {
			// todo: encapsulate this stuff in VJController
			// fixit: this does work, but there's a bug where you have another
			// source selected then drag in a new Color source and the selected
			// source's snapshot gets updated along with the new color's.
			auto pMGSource = g_pAppMain->GetActiveSource();
			if (pMGSource != nullptr) {
				auto pSnapshot = std::make_unique<TVJSnapshot>();
				auto pVJC = g_pAppMain->VJController();
				pVJC->SourceRenderSnapshot(m_uSourceID, *pSnapshot);
				pMGSource->UpdateSnapshot(*pSnapshot);
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CVJVar_Slider::Load(CNodeFile::CNode* pNode) -> void {
	pNode = pNode->FirstNode("var");
	while (pNode && Stricmp(pNode->AttrAsString("name"), m_pLabel->Get())) {
		pNode = pNode->NextNode("var");
	}
	if (pNode) {
		m_pTrackBar->SetPos(pNode->AsFloat("value"));
		auto pNodeLink = pNode->FirstNode("link");
		if (pNodeLink) {
			m_pLink = NEW CLink(nullptr, nullptr);
			m_pLink->Load(pNodeLink);
		}
	}

	// ebp-> check for null here, because bLinkable flag doesn't always create this button in VJVar...
	if (m_pBtnLink) {
		m_pBtnLink->SetLinked(m_pLink != nullptr);
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CVJVar_Slider::Save(CNodeFile* pFile) -> void {
	pFile->WriteOpenNode("var", "name=\"" + m_pLabel->Get() + "\"");
	pFile->WriteNode("value", "", m_pTrackBar->GetPos());
	if (m_pLink) {
		m_pLink->Save(pFile, "link");
	}
	pFile->WriteCloseNode();
}
