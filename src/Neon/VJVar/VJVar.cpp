//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"

#include "Controls/MGButton.h"
#include "Controls/MGLabel.h"
#include "CtrlVar.h"
#include "Effects/Effect.h"
#include "LinkSystem/Link.h"
#include "LinkSystem/LinkBehaviour.h"
#include "NeonMain.h"
#include "Sources/Source.h"
#include "VJVar.h"
#include "VJVar_CheckBox.h"
#include "VJVar_ComboBox.h"
#include "VJVar_EditBox.h"
#include "VJVar_Slider.h"



//---------------------------------------------------------------------------//
//	Create
//---------------------------------------------------------------------------//
auto CVJVar::Create(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject) -> CVJVar* {
	switch (pCtrlVar->iType) {
		case TCtrlVar::CHECK_BOX: {
			return NEW CVJVar_CheckBox(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject);
		}
		case TCtrlVar::EDIT_BOX: {
			return NEW CVJVar_EditBox(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject);
		}
		case TCtrlVar::COMBO_BOX: {
			return NEW CVJVar_ComboBox(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject);
		}
		case TCtrlVar::SLIDER: {
			return NEW CVJVar_Slider(pParent, pCtrlVar, pSource, ypos, uSourceID, iVar, iScope, iObject);
		}
		default: {
			return nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	Create
//---------------------------------------------------------------------------//
auto CVJVar::Create(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID) -> CVJVar* {
	switch (pCtrlVar->iType) {
		case TCtrlVar::CHECK_BOX: {
			return NEW CVJVar_CheckBox(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID);
		}
		case TCtrlVar::EDIT_BOX: {
			return NEW CVJVar_EditBox(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID);
		}
		case TCtrlVar::COMBO_BOX: {
			return NEW CVJVar_ComboBox(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID);
		}
		case TCtrlVar::SLIDER: {
			return NEW CVJVar_Slider(pParent, pCtrlVar, pEffect, ypos, uSourceID, iVar, uEffectID);
		}
		default: {
			return nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar::CVJVar(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject)
	: m_uSourceID(uSourceID)
	, m_uEffectID(INVALID_ID)
	, m_iType(TCtrlVar::INVALID)
	, m_iScope(iScope)
	, m_iObject(iObject)
	, m_iVar(iVar)
	, m_pLabel(nullptr)
	, m_pBtnLink(nullptr)
	, m_pControl(nullptr)
	, m_pLink(nullptr)
	, m_pSource(pSource)
	, m_pEffect(nullptr)
{
	ASSERT(pSource);
	ASSERT(pCtrlVar);
	m_pLabel = NEW CMGLabel(pParent, 4, ypos + 2, MGALIGN_NONE, FONT_NORMAL, pCtrlVar->szName);
	if (pCtrlVar->bLinkable) {
		m_pBtnLink = NEW CMGButton(pParent, VJVAR_LINKBTN_XPOS, ypos + 2, CMGButton::LINK, "L", bind(&CVJVar::OnBtnLinkClick, this));
	}
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CVJVar::CVJVar(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID)
	: m_uSourceID(uSourceID)
	, m_uEffectID(uEffectID)
	, m_iType(TCtrlVar::INVALID)
	, m_iScope(INVALID_INDEX)
	, m_iObject(INVALID_INDEX)
	, m_iVar(iVar)
	, m_pLabel(nullptr)
	, m_pBtnLink(nullptr)
	, m_pControl(nullptr)
	, m_pLink(nullptr)
	, m_pSource(nullptr)
	, m_pEffect(pEffect)
{
	ASSERT(pEffect);
	ASSERT(pCtrlVar);
	m_pLabel = NEW CMGLabel(pParent, 4, ypos + 2, MGALIGN_NONE, FONT_NORMAL, pCtrlVar->szName);
	if (pCtrlVar->bLinkable) {
		m_pBtnLink = NEW CMGButton(pParent, VJVAR_LINKBTN_XPOS, ypos + 2, CMGButton::LINK, "L", bind(&CVJVar::OnBtnLinkClick, this));
	}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CVJVar::~CVJVar() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CVJVar\n"));
#endif
	//DELETE_PTR(m_pLabel);	// these will be deleted by their MGui parent, MGPanelVars
	//DELETE_PTR(m_pBtnLink);

	// ebp-> these pointers are injected depenedencies, the actual data
	// is deleted elsewhere.
	m_pControl = nullptr;
	m_pSource = nullptr;
	m_pEffect = nullptr;

	// delete the link
	DELETE_PTR(m_pLink);
}


//---------------------------------------------------------------------------//
//	OnBtnLinkClick
//---------------------------------------------------------------------------//
auto CVJVar::OnBtnLinkClick() -> void {
	// ebp-> assumes that m_pBtnLink has been set since this function is its hook...
	int beh_type = ILinkBehaviour::INVALID;
	switch (m_iType) {
	case TCtrlVar::CHECK_BOX:
		beh_type = ILinkBehaviour::CHECK_BOX;
		break;
	case TCtrlVar::COMBO_BOX:
		beh_type = ILinkBehaviour::COMBO_BOX;
		break;
	case TCtrlVar::SLIDER:
		beh_type = ILinkBehaviour::TRACK_BAR;
		break;
	case TCtrlVar::BUTTON:
		beh_type = ILinkBehaviour::BUTTON;
		break;
	default:
		return;
	}

	g_pAppMain->OpenVarLinker(
		&m_pLink,
		m_pBtnLink->Left(),
		m_pBtnLink->Top(),
		beh_type,
		false,
		m_pControl,
		m_pBtnLink,
		bind(&CVJVar::OnCloseVarLinker, this, _1)
		);
}


//---------------------------------------------------------------------------//
//	OnCloseVarLinker
//	m_pLink is updated on the VarLinker directly
//---------------------------------------------------------------------------//
auto CVJVar::OnCloseVarLinker(CMGWindow::EModalResult iModalResult) -> void {
	if (iModalResult == CMGWindow::MR_OK) {
		// OK
	} else if (iModalResult == CMGWindow::MR_CANCEL) {
		// CANCEL
	}
}
