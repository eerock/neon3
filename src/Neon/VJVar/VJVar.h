//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VJVAR_H
#define NEON_VJVAR_H

#include "Controls/MGWindow.h"

struct TCtrlVar;
class CMGButton;
class CMGControl;
class CMGLabel;
class CLink;

class CSource;
class CEffect;

//#define VJVAR_CONTROL_XPOS	90
#define VJVAR_CONTROL_XPOS		80
#define VJVAR_LINKBTN_XPOS		(VJVAR_CONTROL_XPOS + 180)


class CVJVar {
public:
	static auto Create(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject) -> CVJVar*;
	static auto Create(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID) -> CVJVar*;

	CVJVar(CMGControl* pParent, TCtrlVar* pCtrlVar, CSource* pSource, int ypos, uint uSourceID, int iVar, int iScope, int iObject);
	CVJVar(CMGControl* pParent, TCtrlVar* pCtrlVar, CEffect* pEffect, int ypos, uint uSourceID, int iVar, uint uEffectID);

	virtual ~CVJVar();

	virtual auto Evaluate() -> void = 0;

	virtual auto Load(CNodeFile::CNode* pNode) -> void = 0;
	virtual auto Save(CNodeFile* pFile) -> void = 0;

protected:
	auto IsEffectVar() const -> bool { return m_pEffect != nullptr; }

	uint m_uSourceID;
	uint m_uEffectID;
	int m_iType;
	int m_iScope;
	int m_iObject;
	int m_iVar;
	CMGLabel* m_pLabel;
	CMGButton* m_pBtnLink;
	CMGControl* m_pControl;
	CLink* m_pLink;

	// ebp-> to reduce having to go from the global g_AppMain to get the VJController to get to the 
	// source or effect, we store a pointer to the source or effect here.
	// ebp(2014)-> what about the VJSource?
	CSource* m_pSource;
	CEffect* m_pEffect;

private:	// ebp(2014)-> why are these private?
	auto OnBtnLinkClick() -> void;
	auto OnCloseVarLinker(CMGWindow::EModalResult iModalResult) -> void;
};

#endif//NEON_VJVAR_H
