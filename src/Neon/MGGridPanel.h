//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGGRIDPANEL_H
#define NEON_MGGRIDPANEL_H

#include "MGBase.h"
#include "MGGridItem.h"
#include "Controls/MGGrid.h"

typedef CMGGrid<CMGGridItem> CMGSourceGrid;

class CMGPageControl;

class CMGGridPanel : public CMGControl {
public:
	CMGGridPanel(CMGControl* pParent, int numPages);
	~CMGGridPanel() override;

	auto Clear() -> void;
	auto ClearMaster() -> void;
	auto Update() -> void;

	auto GetMasterGrid() const -> CMGSourceGrid* { return m_pGridMaster; }
	auto GetSourceGrid() const -> CMGSourceGrid* { return m_pGridSource; }
	auto GetGridPage(int iGrid) const -> CMGSourceGrid*;
	auto SelectGridPage(int iGrid) -> void;

	auto NextClip(int iLayer) -> void;
	auto PrevClip(int iLayer) -> void;
	auto RandClip(int iLayer) -> void;

	auto AddSourceToGrid(CMGSource* pSource) -> void;
	auto FindEmptyAndAdd(CMGSource* pSource) -> bool;

	auto AttachSource(CMGSource* pSource, int iLayer) -> void;
	auto UnattachSource(CMGSource* pSource) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGGridPanel\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

private:
	auto UnloadSource(CMGSource* pSource) -> void;
	auto UnloadSourceAt(int x, int y) -> void;

	auto OnSelectGridPage() -> void;
	auto OnGridDragOver(CMGSourceGrid* pGrid, CMGControl* pFrom, int iX, int iY, bool& bAccept) -> void;
	auto OnGridDragDrop(CMGSourceGrid* pGrid, CMGControl* pFrom, int iX, int iY) -> void;
	auto OnGridMouseMove(CMGSourceGrid* pGrid, int iX, int iY) -> void;
	auto OnGridMouseDown(CMGSourceGrid* pGrid, int iX, int iY, int iButton) -> void;
	auto OnGridMouseUp(CMGSourceGrid* pGrid, int iX, int iY, int iButton) -> void;
	auto OnGridMouseDoubleClk(CMGSourceGrid* pGrid, int iX, int iY, int iButton) -> void;
	auto OnGridKeyDown(CMGSourceGrid* pGrid, int vkey) -> void;
	auto OnGridKeyUp(CMGSourceGrid* pGrid, int vkey) -> void;

	auto NumSourcesInCol(int iColumn) -> int;

private:
	CMGPageControl* m_pPCGrid;
	CMGSourceGrid* m_pGridMaster;
	CMGSourceGrid* m_pGridSource;
	std::vector<CMGSourceGrid*> m_vGridSourcePages;
};

#endif//NEON_MGGRIDPANEL_H
