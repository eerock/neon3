//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "AppOptions.h"


// define the static pointer that the service locater uses to find the service instance...
IAppOptionsService* CServiceLocator<IAppOptionsService>::s_pService = nullptr;


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CAppOptions::CAppOptions()
	: m_ptOpts(), m_sName()
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CAppOptions::~CAppOptions() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CAppOptions\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Set(Int)
//---------------------------------------------------------------------------//
auto CAppOptions::Set(std::string const& sParamName, int const& iValue) -> void {
	m_ptOpts.put(sParamName, iValue);
}


//---------------------------------------------------------------------------//
//	Set(String)
//---------------------------------------------------------------------------//
auto CAppOptions::Set(std::string const& sParamName, std::string const& sValue) -> void {
	m_ptOpts.put(sParamName, sValue);
}


//---------------------------------------------------------------------------//
//	Set(Bool)
//---------------------------------------------------------------------------//
auto CAppOptions::Set(std::string const& sParamName, bool const& bValue) -> void {
	m_ptOpts.put(sParamName, bValue);
}


//---------------------------------------------------------------------------//
//	Get(Int)
//---------------------------------------------------------------------------//
auto CAppOptions::Get(std::string const& sParamName, int const& iDefaultValue) -> int {
	return m_ptOpts.get<int>(sParamName, iDefaultValue);
}


//---------------------------------------------------------------------------//
//	Get(String)
//---------------------------------------------------------------------------//
auto CAppOptions::Get(std::string const& sParamName, std::string const& sDefaultValue) -> std::string {
	return m_ptOpts.get<std::string>(sParamName, sDefaultValue);
}


//---------------------------------------------------------------------------//
//	Get(Bool)
//---------------------------------------------------------------------------//
auto CAppOptions::Get(std::string const& sParamName, bool const& bDefaultValue) -> bool {
	return m_ptOpts.get<bool>(sParamName, bDefaultValue);
}


//---------------------------------------------------------------------------//
//	Load values from the specified file
//	ebp-> this function parses the .xml config file
//---------------------------------------------------------------------------//
auto CAppOptions::Load(std::string const& sFilename) -> bool {
	m_sName = sFilename;
	try {
		pt::read_xml(sFilename, m_ptOpts, pt::xml_parser::trim_whitespace);
	} catch(pt::xml_parser_error& e) {
		UNREFERENCED_PARAMETER(e);
		GLOG(("ERROR: Failed to locate config file %s - %s\n", sFilename.c_str(), e.message().c_str()));
		return false;
	}
	return true;
}


//---------------------------------------------------------------------------//
//	Save values to specified file
//---------------------------------------------------------------------------//
auto CAppOptions::Save(std::string const& sFilename) -> void {
	if (!sFilename.empty()) {
		m_sName = sFilename;
	}

	try {
		pt::xml_writer_settings<char> writer_settings('\t', 1);
		pt::write_xml(m_sName, m_ptOpts, std::locale(), writer_settings);
	} catch(pt::xml_parser_error& e) {
		UNREFERENCED_PARAMETER(e);
		GLOG(("ERROR: Failed to save config file %s - %s\n", m_sName.c_str(), e.message().c_str()));
	}
}

