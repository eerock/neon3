//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"

#include "AppOptions.h"
#include "Controls/MGButton.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGComboBox.h"
#include "Controls/MGEditBox.h"
#include "Controls/MGGroupBox.h"
#include "Controls/MGLabel.h"
#include "Controls/MGPageControl.h"
#include "Controls/MGPanel.h"
#include "Controls/MGSpinBox.h"
#include "Controls/MGTrackBar.h"
#include "CtrlVar.h"
#include "DisplayWindow.h"
#include "LinkSystem/Link.h"
#include "NeonMain.h"
#include "MGBeatManager.h"
#include "MGEffects.h"
#include "MGPanelVars.h"
#include "MGSource.h"
#include "VJVar/VJVar.h"

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
namespace filesys = boost::filesystem;


//---------------------------------------------------------------------------//
//	STATIC: Create (from filename)
//---------------------------------------------------------------------------//
auto CMGSource::Create(std::string const& sFile) -> CMGSource* {
	auto pSource = NEW CMGSource(g_pAppMain->VJController());
	if (!pSource->Load(sFile)) {
		DELETE_PTR(pSource);
	} else {
#ifdef _DEBUG
#ifdef VERBOSE_MEM_LOG
		std::string sIndent;
		pSource->PrintHierarchy(sIndent);
#endif
#endif
	}
	return pSource;
}


//---------------------------------------------------------------------------//
//	STATIC: Create (from xml Node)
//---------------------------------------------------------------------------//
auto CMGSource::Create(CNodeFile::CNode* pNode) -> CMGSource* {
	auto pSource = NEW CMGSource(g_pAppMain->VJController());
	if (!pSource->Load(pNode)) {
		DELETE_PTR(pSource);
	} else {
#ifdef _DEBUG
#ifdef VERBOSE_MEM_LOG
		std::string sIndent;
		pSource->PrintHierarchy(sIndent);
#endif
#endif
	}
	return pSource;
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGSource::CMGSource(CVJController* pVJController)
	: CMGControl(nullptr, 0, 0, 0, 0)
	, m_uSourceID(INVALID_ID)
	, m_iLayerAttached(-1)
	, m_iBlend(BL_ALPHABLEND)
	, m_iGridCol(0)
	, m_iGridRow(0)
	, m_bAttached(false)
	, m_sClass()
	, m_sFile()
	, m_sDir()
	, m_sName()
	, m_pBitmap(std::make_unique<TMGSurface>())
	, m_pVJController(pVJController)
	, m_pPanelTitle(nullptr)
	, m_pLTitle(nullptr)
	, m_pEBName(nullptr)
	, m_pGBVars(nullptr)
	, m_pPCVars(nullptr)
	, m_pPageEffects(nullptr)
	, m_pPageVars(nullptr)
	, m_pPageObjects(nullptr)
	, m_pPageOptions(nullptr)
	, m_pBLinkSource(nullptr)
	, m_pLinkSource(nullptr)
	, m_pLSpeed(nullptr)
	, m_pTBSpeed(nullptr)
	, m_pBLinkSpeed(nullptr)
	, m_pBSpeedx1(nullptr)
	, m_pLinkSpeed(nullptr)
	, m_pCBAdjustToBPM(nullptr)
	, m_pSBAdjustToBPM(nullptr)
	, m_pLAlpha(nullptr)
	, m_pBAlphaHalf(nullptr)
	, m_pTBAlpha(nullptr)
	, m_pBLinkAlpha(nullptr)
	, m_pLinkAlpha(nullptr)
	, m_pCBBlendModes(nullptr)
	, m_pBLinkBlend(nullptr)
	, m_pLinkBlend(nullptr)
	, m_pCBScopes(nullptr)
	, m_pCBObjects(nullptr)
	, m_pCBQuality(nullptr)
	, m_pSBFade(nullptr)
	, m_pCHReset(nullptr)
	, m_pEffects(nullptr)
	, m_pVJVars(nullptr)
	, m_pActiveScopePanel(nullptr)
	, m_pScopePanel(nullptr)
{
	m_iAlign = MGALIGN_CLIENT;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGSource::~CMGSource() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGSource\n"));
#endif
	m_pVJController->SourceUnload(m_uSourceID);
	m_pVJController = nullptr;

	if (m_pActiveScopePanel) {
		m_pActiveScopePanel->SetParent(nullptr);
		m_pActiveScopePanel = nullptr;
	}

	// set parent on scope vars so that MGui hierarchy will destruct them
	for (uint i = 0; i < m_vvpVJScopeVars.size(); ++i) {
		for (uint j = 0; j < m_vvpVJScopeVars[i].size(); ++j) {
			m_vvpVJScopeVars[i][j]->SetParent(m_pScopePanel);
		}
		m_vvpVJScopeVars[i].clear();
	}
	m_vvpVJScopeVars.clear();

	UnloadSnapshot();

	DELETE_PTR(m_pLinkSource);
	DELETE_PTR(m_pLinkSpeed);
	DELETE_PTR(m_pLinkAlpha);
	DELETE_PTR(m_pLinkBlend);

	g_pAppMain->RemoveActiveSource();
}


//---------------------------------------------------------------------------//
//	Load
//	ebp-> load a source (generic)
//---------------------------------------------------------------------------//
auto CMGSource::Load(std::string const& sFile) -> bool {
	filesys::path p(sFile);
	m_sName = p.stem().string();
	std::string ext = p.extension().string();
	std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

	if (ext == ".nsd" || ext == ".nly") {
		return LoadNeon(sFile);
	} else if (ext == ".avi" || ext == ".flv" || ext == ".mov" || ext == ".mp4" || ext == ".mpg" || ext == ".mpeg" || ext == ".wmv" || ext == ".webm") {
		return LoadRawMediaFile(sFile, "MpgVideo");
	} else if (ext == ".jpeg" || ext == ".jpg" || ext == ".png" || ext == ".tga") {
		return LoadRawMediaFile(sFile, "Image");
	} else if (ext == ".nil" || ext == ".gif") {
		return LoadRawMediaFile(sFile, "ImageList");
	}
	return (m_uSourceID != INVALID_ID);
}


//---------------------------------------------------------------------------//
//	Load
//	load source from an XML node
//---------------------------------------------------------------------------//
auto CMGSource::Load(CNodeFile::CNode* pNode) -> bool {
	// Load Source data
	m_sClass = pNode->AttrAsString("class");
	m_sFile = pNode->AsString("file");
	m_sName = pNode->AsString("name", m_sName);

	// look for the existence of the directory, fix it if necessary
	filesys::path p(pNode->AsString("dir"));
	if (!p.is_absolute()) {
		if (!filesys::exists(p)) {
			filesys::path d_src(neon::sDirSources);
			d_src /= p;
			p = d_src;
			if (!filesys::exists(p)) {
				GLOG(("ERROR: Path %s is unexpected!\n", p.string()));
				return false;
			}
		}
	}

	m_sDir = p.string();

	//GLOG(("CMGSource::Load(%s\\%s)\n", m_sDir.c_str(), m_sFile.c_str()));

	PushDir(m_sDir);
	m_uSourceID = m_pVJController->SourceLoad(m_sClass, m_sFile);
	PopDir();

	if (m_uSourceID != INVALID_ID) {
		// Load controls
		CreateControls();

		// Load vars, filters and its links
		LoadVars(pNode);
		LoadEffects(pNode);
		LoadLinks(pNode);

		// Load the snapshot after everything else...
		// (this fixed the broken "Color" snapshots)
		auto pSnapshot = std::make_unique<TVJSnapshot>();
		m_pVJController->SourceRenderSnapshot(m_uSourceID, *pSnapshot);
		LoadSnapshot(*pSnapshot, GetBitmap());
	}
	return (m_uSourceID != INVALID_ID);
}


//---------------------------------------------------------------------------//
//	LoadRawMediaFile
//	load a raw image or video file
//---------------------------------------------------------------------------//
auto CMGSource::LoadRawMediaFile(std::string const& sFile, std::string const& sClass) -> bool {
	filesys::path p(sFile);
	m_sClass = sClass;
	m_sDir = p.parent_path().string();
	m_sFile = p.filename().string();

	ASSERT(filesys::exists(p) && filesys::is_regular_file(p));

	PushDir(m_sDir);
	m_uSourceID = m_pVJController->SourceLoad(m_sClass, m_sFile);
	PopDir();

	if (m_uSourceID != INVALID_ID) {
		// Load controls
		CreateControls();

		// Load vars and effects here?

		// Load the snapshot after everything else...
		auto pSnapshot = std::make_unique<TVJSnapshot>();
		m_pVJController->SourceRenderSnapshot(m_uSourceID, *pSnapshot);
		LoadSnapshot(*pSnapshot, GetBitmap());
	}
	return (m_uSourceID != INVALID_ID);
}


//---------------------------------------------------------------------------//
//	LoadNeon
//	load a Neon (nsd, nly) file
//---------------------------------------------------------------------------//
auto CMGSource::LoadNeon(std::string const& sFile) -> bool {
	CNodeFile NodeFile;
	if (!NodeFile.LoadFromFile(sFile)) {
		GLOG(("ERROR: Can't load file %s\n", sFile.c_str()));
		return false;
	}

	// Has nodes?
	auto pNode = NodeFile.FirstNode("layer");
	if (!pNode) {
		return false;
	}

	// Source?
	pNode = pNode->FirstNode("source");
	if (!pNode) {
		return false;
	}

	return Load(pNode);
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGSource::Save(std::string const& sFile) -> void {
	CNodeFile NodeFile;
	if (!NodeFile.WriteOpen(sFile)) {
		GLOG(("ERROR: Can't open %s for writing\n", sFile.c_str()));
		return;
	}

	// <layer>
	NodeFile.WriteOpenNode("layer", "");
	// <source> ... </source>
	Save(&NodeFile);
	// </layer>
	NodeFile.WriteCloseNode();
	NodeFile.WriteClose();
}



//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGSource::Save(CNodeFile* pFile) -> void {
	// <source class="class">
	pFile->WriteOpenNode("source", "class=\"" + m_sClass + "\"");
	pFile->WriteNode("dir", "", m_sDir);	// <dir> ... </dir>
	pFile->WriteNode("file", "", m_sFile);	// <file> ... </file>
	pFile->WriteNode("name", "", m_sName);	// <name> ... </name>

	// Main vars
	// <adjusttobpm>0</adjusttobpm>
	// <adjusttobpmnum>1</adjusttobpmnum>
	pFile->WriteNode("adjusttobpm", "", m_pCBAdjustToBPM->Checked());
	pFile->WriteNode("adjusttobpmnum", "", m_pSBAdjustToBPM->GetValue());

	// Vars
	// <vars>
	pFile->WriteOpenNode("vars", "");

	// FX Vars
	// <source>
	pFile->WriteOpenNode("source", "");
	// Speed
	// <speed><value>1.0</value></speed>
	pFile->WriteOpenNode("speed", "");
	pFile->WriteNode("value", "", m_pTBSpeed->GetPos());
	pFile->WriteCloseNode();
	// Alpha
	// <alpha><value>1.0</value></alpha>
	pFile->WriteOpenNode("alpha", "");
	pFile->WriteNode("value", "", m_pTBAlpha->GetPos());
	pFile->WriteCloseNode();
	// Blend
	// <blend><value>Additive</value></blend>
	pFile->WriteOpenNode("blend", "");
	pFile->WriteNode("value", "", m_pCBBlendModes->GetText());
	pFile->WriteCloseNode();
	// Quality
	// <quality><value>0</value></quality>
	pFile->WriteOpenNode("quality", "");
	pFile->WriteNode("value", "", m_pCBQuality->GetSelected());
	pFile->WriteCloseNode();
	// Fade
	// <fade><value>250</value></fade>
	pFile->WriteOpenNode("fade", "");
	pFile->WriteNode("value", "", m_pSBFade->GetValue());
	pFile->WriteCloseNode();
	// Reset on Attach
	// <reset><value>1</value></reset>
	pFile->WriteOpenNode("reset", "");
	pFile->WriteNode("value", "", m_pCHReset->Checked());
	pFile->WriteCloseNode();
	// VJVars
	// <var name="Name"><value>0</value></var> ...
	m_pVJVars->Save(pFile);
	// End Source
	// </source>
	pFile->WriteCloseNode();

	// Obj Vars
	// <objvars>
	pFile->WriteOpenNode("objvars", "");
	for (size_t i = 0; i < m_vvpVJScopeVars.size(); ++i) {
		for (size_t j = 0; j < m_vvpVJScopeVars[i].size(); ++j) {
			std::stringstream ss;
			ss << "scope=\"" << i << "\" obj=\"" << j << "\"";
			// <objvar scope="0" obj="0">
			pFile->WriteOpenNode("objvar", ss.str());
			// <var name="Name"><value>1</value></var>
			m_vvpVJScopeVars[i][j]->Save(pFile);
			// </objvar>
			pFile->WriteCloseNode();
		}
	}

	// End Obj Vars
	// </objvars>
	pFile->WriteCloseNode();
	// End Vars
	// </vars>
	pFile->WriteCloseNode();

	// <effects> ... </effects>
	SaveEffects(pFile);

	// <links> ... </links>
	SaveLinks(pFile);

	// </source>
	pFile->WriteCloseNode();
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGSource::Update() -> void {
	// Vars
	m_pVJVars->Update();

	// ObjVars
	for (size_t i = 0; i < m_vvpVJScopeVars.size(); ++i) {
		for (size_t j = 0; j < m_vvpVJScopeVars[i].size(); ++j) {
			m_vvpVJScopeVars[i][j]->Update();
		}
	}

	// ebp-> adding these links...
	if (m_pLinkSpeed && m_pLinkSpeed->Evaluate()) {
		m_pTBSpeed->SetPos(m_pLinkSpeed->ValueTrackBar());
	}

	if (m_pLinkAlpha && m_pLinkAlpha->Evaluate()) {
		m_pTBAlpha->SetPos(m_pLinkAlpha->ValueTrackBar());
	}

	if (m_pLinkBlend && m_pLinkBlend->Evaluate()) {
		m_pCBBlendModes->Select(m_pLinkBlend->ValueComboBox());
	}

	if (m_pLinkSource && m_pLinkSource->Evaluate()) {
		if (IsAttached()) {
			g_pAppMain->UnattachSource(this);
		} else {
			g_pAppMain->AttachSource(this, m_iGridCol);
		}
	}

	// Effects
	m_pEffects->Update();
}


//---------------------------------------------------------------------------//
//	Attach
//---------------------------------------------------------------------------//
auto CMGSource::Attach(int iLayer) -> void {
	if (!m_bAttached) {
		m_pVJController->SourceAttach(m_uSourceID, iLayer);
		m_bAttached = true;
	}
	m_iLayerAttached = iLayer;
}


//---------------------------------------------------------------------------//
//	Unattach
//---------------------------------------------------------------------------//
auto CMGSource::Unattach() -> void {
	m_pVJController->SourceUnattach(m_uSourceID);
	m_bAttached = false;
}


//---------------------------------------------------------------------------//
//	Move
//---------------------------------------------------------------------------//
auto CMGSource::Move(int iLayer) -> void {
	if (m_bAttached) {
		m_pVJController->LayerMove(m_iLayerAttached, iLayer);
		m_iLayerAttached = iLayer;
	}
}


//---------------------------------------------------------------------------//
//	SetBlend
//---------------------------------------------------------------------------//
auto CMGSource::SetBlend(int iBlend) -> void {
	if (ValidIndex(iBlend, (int)BL_MODES_MAX)) {
		m_pVJController->SourceSetBlend(m_uSourceID, iBlend);
		m_iBlend = iBlend;
	}
}


//---------------------------------------------------------------------------//
//	LoadSnapshot
//---------------------------------------------------------------------------//
auto CMGSource::LoadSnapshot(TVJSnapshot& snapshot, TMGSurface& surface) -> void {
	glGenTextures(1, &surface.uTextureID);
	glBindTexture(GL_TEXTURE_2D, surface.uTextureID);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, TVJSnapshot::WIDTH, TVJSnapshot::HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, snapshot.m_pData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// fixit: Snapshot needs to be sized appropriately.  Maybe not here, maybe it
	// needs to be stretched in the gui draw code.
	surface.w = TVJSnapshot::WIDTH;
	surface.h = TVJSnapshot::HEIGHT;
	surface.fInvW = 1.f / surface.w;
	surface.fInvH = 1.f / surface.h;
}


//---------------------------------------------------------------------------//
//	UpdateSnapshot
//---------------------------------------------------------------------------//
auto CMGSource::UpdateSnapshot(TVJSnapshot& snapshot) -> void {
	glBindTexture(GL_TEXTURE_2D, GetBitmap().uTextureID);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, TVJSnapshot::WIDTH, TVJSnapshot::HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, snapshot.m_pData);
}


//---------------------------------------------------------------------------//
//	UnloadSnapshot
//---------------------------------------------------------------------------//
auto CMGSource::UnloadSnapshot() -> void {
	if (m_pBitmap != nullptr && m_pBitmap->uTextureID != INVALID_ID) {
		glDeleteTextures(1, &m_pBitmap->uTextureID);
	}
}


//---------------------------------------------------------------------------//
//	CreateControls
//---------------------------------------------------------------------------//
auto CMGSource::CreateControls() -> void {
	// magic fuckin numbers
	m_pPanelTitle = NEW CMGPanel(this, 0, 0, 0, 25, MGALIGN_TOP);
	m_pGBVars = NEW CMGGroupBox(this, 0, 0, 0, 140, MGALIGN_TOP);
	m_pPCVars = NEW CMGPageControl(this, 0, 0, 0, 0, MGALIGN_CLIENT);

	m_pPageEffects = NEW CMGPage(m_pPCVars, "Effects");
	m_pPageOptions = NEW CMGPage(m_pPCVars, "Options");
	m_pPageVars = NEW CMGPage(m_pPCVars, "Vars");
	m_pPageObjects = NEW CMGPage(m_pPCVars, "Objects");

	// Source
	m_pBLinkSource = NEW CMGButton(m_pPanelTitle, 250, 4, CMGButton::LINK, "L", bind(&CMGSource::OnClickLinkSource, this));
	NEW CMGButton(m_pPanelTitle, 274, 0, CMGButton::SAVE, "", bind(&CMGSource::OnClickSave, this));
	m_pEBName = NEW CMGEditBox(m_pPanelTitle, 2, 2, 200, bind(&CMGSource::OnEditSourceName, this));
	m_pLTitle = NEW CMGLabel(m_pPanelTitle, 2, 2, MGALIGN_NONE, FONT_FX_TITLE, m_sName, bind(&CMGSource::OnClickSourceName, this));
	m_pEBName->SetVisible(false);
	m_pEBName->SetText(m_sName);

	// Speed
	m_pLSpeed = NEW CMGLabel(m_pGBVars, 5, 12, MGALIGN_NONE, FONT_NORMAL, "Speed");
	m_pBSpeedx1 = NEW CMGButton(m_pGBVars, 45, 10, CMGButton::SMALL, "x1", bind(&CMGSource::OnClickSpeedx1, this));
	m_pTBSpeed = NEW CMGTrackBar(m_pGBVars, 70, 12, 200, false, bind(&CMGSource::OnChangeSpeed, this));
	m_pBLinkSpeed = NEW CMGButton(m_pGBVars, 274, 12, CMGButton::LINK, "L", bind(&CMGSource::OnClickLinkSpeed, this));
	m_pCBAdjustToBPM = NEW CMGCheckBox(m_pGBVars, 70, 35, "Adjust Speed to BPM", true, false, bind(&CMGSource::OnCheckedAdjustToBpm, this));
	m_pSBAdjustToBPM = NEW CMGSpinBox(m_pGBVars, 207, 35, 39, 0, 999, 1, bind(&CMGSource::OnChangeBpmAdjust, this));
	NEW CMGLabel(m_pGBVars, 67, 2, MGALIGN_NONE, FONT_MINI, "x0");
	NEW CMGLabel(m_pGBVars, 110, 2, MGALIGN_NONE, FONT_MINI, "x1");
	NEW CMGLabel(m_pGBVars, 265, 2, MGALIGN_NONE, FONT_MINI, "x5");

	// Alpha
	m_pLAlpha = NEW CMGLabel(m_pGBVars, 5, 62, MGALIGN_NONE, FONT_NORMAL, "Alpha");
	m_pBAlphaHalf = NEW CMGButton(m_pGBVars, 45, 60, CMGButton::SMALL, ".5", bind(&CMGSource::OnClickAlphaHalf, this));
	m_pTBAlpha = NEW CMGTrackBar(m_pGBVars, 70, 62, 200, false, bind(&CMGSource::OnChangeAlpha, this));
	m_pBLinkAlpha = NEW CMGButton(m_pGBVars, 274, 62, CMGButton::LINK, "L", bind(&CMGSource::OnClickLinkAlpha, this));

	// ebp-> source blend modes!! (Alpha, Add, Sub, Inv, etc)
	NEW CMGLabel(m_pGBVars, 5, 92, MGALIGN_NONE, FONT_NORMAL, "Blend");
	m_pCBBlendModes = NEW CMGComboBox(m_pGBVars, 70, 90, 140, 20, true, bind(&CMGSource::OnChangeBlendMode, this));
	m_pCBBlendModes->Add("Additive");
	m_pCBBlendModes->Add("Alpha");
	m_pCBBlendModes->Add("Subtractive");
	m_pCBBlendModes->Add("Invert");
	m_pCBBlendModes->Add("Invert Destination");
	m_pCBBlendModes->Add("Mask");
	m_pCBBlendModes->Add("Multiply");
	m_pCBBlendModes->Add("Invert Multiply");
	m_pCBBlendModes->Add("Color Multiply");
	m_pCBBlendModes->Add("Darken");
	m_pCBBlendModes->Add("Lighten");
	m_pCBBlendModes->Select(BL_ADDITIVE);	// use additive as default blend mode.
	m_pBLinkBlend = NEW CMGButton(m_pGBVars, 274, 92, CMGButton::LINK, "L", bind(&CMGSource::OnClickLinkBlend, this));

	// Set initial vars
	m_pTBSpeed->SetPos(SpeedToTab(1.f));
	m_pTBAlpha->SetPos(1.f);

	// Vars
	// ebp-> these used to be -1, -1 but that was causing some bugs...
	m_pVJVars = NEW CMGPanelVars(m_pPageVars, m_uSourceID, 0, 0);

	// Objects
	auto pPanelObjectsSup = NEW CMGPanel(m_pPageObjects, 0, 0, 0, 54, MGALIGN_TOP);
	m_pCBScopes = NEW CMGComboBox(pPanelObjectsSup, 80, 4, 140, 100, true, bind(&CMGSource::OnChangeScope, this));
	m_pCBObjects = NEW CMGComboBox(pPanelObjectsSup, 80, 28, 140, 100, true, bind(&CMGSource::OnChangeObject, this));
	m_pScopePanel = NEW CMGPanel(m_pPageObjects, 0, 0, 0, 0, MGALIGN_CLIENT);

	std::vector<std::string> vScopes;
	m_pVJController->SourceGetVarScopes(m_uSourceID, vScopes);
	for (uint i = 0; i < vScopes.size(); ++i) {
		m_pCBScopes->Add(vScopes[i]);
		std::vector<std::string> vObjects;
		std::vector<CMGPanelVars*> Panels;
		m_pVJController->SourceGetVarObjects(m_uSourceID, vObjects, i);
		for (uint j = 0; j < vObjects.size(); ++j) {
			auto pPanel = NEW CMGPanelVars(nullptr, m_uSourceID, i, j);
			Panels.push_back(pPanel);
		}
		m_vvpVJScopeVars.push_back(Panels);
	}

	// Effects
	m_pEffects = NEW CMGEffects(m_pPageEffects, this);

	// Set first
	if (vScopes.size() > 0) {
		m_pCBScopes->Select(0);
	}

	// Options
	auto pAppOpts = CServiceLocator<IAppOptionsService>::GetService();
	NEW CMGLabel(m_pPageOptions, 7, 12, MGALIGN_NONE, FONT_NORMAL, "Render");
	m_pCBQuality = NEW CMGComboBox(m_pPageOptions, 80, 10, 140, 100, true, bind(&CMGSource::OnChangeQuality, this));

	int display_width = pAppOpts->Get("neonconfig.display.rwidth", DISPLAY_WINDOW_WIDTH);
	int display_height = pAppOpts->Get("neonconfig.display.rheight", DISPLAY_WINDOW_HEIGHT);
	{	// full quality dimensions
		std::stringstream ss;
		ss << display_width << " x " << display_height;
		m_pCBQuality->Add(ss.str());
	}

	display_width >>= 1;
	display_height >>= 1;
	{	// custom quality 1 dimensions
		std::stringstream ss;
		ss << display_width << " x " << display_height;
		m_pCBQuality->Add(ss.str());
	}

	display_width >>= 1;
	display_height >>= 1;
	{	// custom quality 2 dimensions
		std::stringstream ss;
		ss << display_width << " x " << display_height;
		m_pCBQuality->Add(ss.str());
	}
	m_pCBQuality->Select(0);

	NEW CMGLabel(m_pPageOptions, 7, 42, MGALIGN_NONE, FONT_NORMAL, "Fade (ms)");
	m_pSBFade = NEW CMGSpinBox(m_pPageOptions, 80, 40, 50, 0, 5000, 50, bind(&CMGSource::OnChangeFadeTime, this));
	m_pSBFade->Set(250);

	m_pCHReset = NEW CMGCheckBox(m_pPageOptions, 7, 70, "Reset time on attach: ", false, true, bind(&CMGSource::OnCheckedReset, this));
}


//---------------------------------------------------------------------------//
//	LoadVars
//---------------------------------------------------------------------------//
auto CMGSource::LoadVars(CNodeFile::CNode* pNode) -> void {
	if (!pNode) {
		return;
	}

	// Main vars
	m_pCBAdjustToBPM->SetChecked(pNode->AsBool("adjusttobpm"));
	m_pSBAdjustToBPM->Set(pNode->AsInt("adjusttobpmnum"));

	pNode = pNode->FirstNode("vars");
	if (!pNode) {
		return;
	}

	// Common vars
	auto pNodeSourceVars = pNode->FirstNode("source");
	if (!pNodeSourceVars) {
		return;
	}

	m_pTBSpeed->SetPos(pNodeSourceVars->FirstNode("speed") ? pNodeSourceVars->FirstNode("speed")->AsFloat("value") : SpeedToTab(1.f));
	m_pTBAlpha->SetPos(pNodeSourceVars->FirstNode("alpha") ? pNodeSourceVars->FirstNode("alpha")->AsFloat("value") : 1.f);

	// changed it to read a string!
	std::string sBlend;
	int iBlend = -1;
	if (pNodeSourceVars->FirstNode("blend")) {
		sBlend = pNodeSourceVars->FirstNode("blend")->AsString("value");
		iBlend = m_pCBBlendModes->IndexOf(sBlend);
	}
	m_pCBBlendModes->Select(iBlend);

	m_pCBQuality->Select(pNodeSourceVars->FirstNode("quality") ? pNodeSourceVars->FirstNode("quality")->AsInt("value") : 0);
	m_pSBFade->Set(pNodeSourceVars->FirstNode("fade") ? pNodeSourceVars->FirstNode("fade")->AsInt("value") : 250);
	m_pCHReset->SetChecked(pNodeSourceVars->FirstNode("reset") ? pNodeSourceVars->FirstNode("reset")->AsBool("value") : true);

	// FX Vars
	m_pVJVars->Load(pNodeSourceVars);

	// Object Vars
	auto pNodeObjVars = pNode->FirstNode("objvars");
	if (!pNodeObjVars) {
		return;
	}

	auto pNodeObjVar = pNodeObjVars->FirstNode("objvar");
	while (pNodeObjVar) {
		long iScope = pNodeObjVar->AttrAsInt("scope", -1);
		int iObj = pNodeObjVar->AttrAsInt("obj", -1);
		if (iScope != -1 && iObj != -1 && iScope < (int)m_vvpVJScopeVars.size() && iObj < (int)m_vvpVJScopeVars[iScope].size()) {
			m_vvpVJScopeVars[iScope][iObj]->Load(pNodeObjVar);
		}
		pNodeObjVar = pNodeObjVar->NextNode("objvar");
	}
}


//---------------------------------------------------------------------------//
//	LoadEffects
//---------------------------------------------------------------------------//
auto CMGSource::LoadEffects(CNodeFile::CNode* pNode) -> void {
	auto pEffects = pNode->FirstNode("effects");
	if (pEffects) {
		m_pEffects->Load(pEffects);
	}
}


//---------------------------------------------------------------------------//
//	LoadLinks
//---------------------------------------------------------------------------//
auto CMGSource::LoadLinks(CNodeFile::CNode* pNode) -> void {
	auto pLinks = pNode->FirstNode("links");
	if (!pLinks) {
		return;
	}

	// check for 'alpha', 'blend', 'source', 'speed' links
	// and set / create links as necessary.
	auto pLinkNode = pLinks->FirstNode("alpha");
	if (pLinkNode) {
		if (!m_pLinkAlpha) {
			m_pLinkAlpha = NEW CLink(nullptr, nullptr);
		}
		m_pLinkAlpha->Load(pLinkNode);
		m_pBLinkAlpha->SetLinked(true);
	}

	pLinkNode = pLinks->FirstNode("blend");
	if (pLinkNode) {
		if (!m_pLinkBlend) {
			m_pLinkBlend = NEW CLink(nullptr, nullptr);
		}
		m_pLinkBlend->Load(pLinkNode);
		m_pBLinkBlend->SetLinked(true);
	}

	pLinkNode = pLinks->FirstNode("source");
	if (pLinkNode) {
		if (!m_pLinkSource) {
			m_pLinkSource = NEW CLink(nullptr, nullptr);
		}
		m_pLinkSource->Load(pLinkNode);
		m_pBLinkSource->SetLinked(true);
	}

	pLinkNode = pLinks->FirstNode("speed");
	if (pLinkNode) {
		if (!m_pLinkSpeed) {
			m_pLinkSpeed = NEW CLink(nullptr, nullptr);
		}
		m_pLinkSpeed->Load(pLinkNode);
		m_pBLinkSpeed->SetLinked(true);
	}
}


//---------------------------------------------------------------------------//
//	SaveVars
//---------------------------------------------------------------------------//
auto CMGSource::SaveVars(CNodeFile* pFile) -> void {
	UNREFERENCED_PARAMETER(pFile);
}


//---------------------------------------------------------------------------//
//	SaveEffects
//---------------------------------------------------------------------------//
auto CMGSource::SaveEffects(CNodeFile* pFile) -> void {
	pFile->WriteOpenNode("effects", "");
	m_pEffects->Save(pFile);
	pFile->WriteCloseNode();
}


//---------------------------------------------------------------------------//
//	SaveLinks
//---------------------------------------------------------------------------//
auto CMGSource::SaveLinks(CNodeFile* pFile) -> void {
	pFile->WriteOpenNode("links", "");
	if (m_pLinkAlpha) {
		m_pLinkAlpha->Save(pFile, "alpha");
	}
	if (m_pLinkBlend) {
		m_pLinkBlend->Save(pFile, "blend");
	}
	if (m_pLinkSource) {
		m_pLinkSource->Save(pFile, "source");
	}
	if (m_pLinkSpeed) {
		m_pLinkSpeed->Save(pFile, "speed");
	}
	pFile->WriteCloseNode();
}


//---------------------------------------------------------------------------//
//	OnClickLinkSource
//---------------------------------------------------------------------------//
auto CMGSource::OnClickLinkSource() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkSource,
		m_pBLinkSource->Left(),
		m_pBLinkSource->Top(),
		ILinkBehaviour::BUTTON,
		m_pBLinkSource,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnChangeSpeed
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeSpeed() -> void {
	if (!m_pCBAdjustToBPM->Checked()) {
		if (m_pTBSpeed) {
			m_pVJController->SourceSetSpeed(m_uSourceID, TabToSpeed(m_pTBSpeed->GetPos()));
		}
	}
}


//---------------------------------------------------------------------------//
//	OnClickLinkSpeed
//---------------------------------------------------------------------------//
auto CMGSource::OnClickLinkSpeed() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkSpeed,
		m_pBLinkSpeed->Left(),
		m_pBLinkSpeed->Top(),
		ILinkBehaviour::TRACK_BAR,
		false,
		m_pTBSpeed,
		m_pBLinkSpeed,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnClickSpeedx1
//---------------------------------------------------------------------------//
auto CMGSource::OnClickSpeedx1() -> void {
	if (!m_pCBAdjustToBPM->Checked()) {
		if (m_pTBSpeed) {
			m_pTBSpeed->SetPos(SpeedToTab(1.f));
		}
	}
}


//---------------------------------------------------------------------------//
//	OnClickAlphaHalf
//---------------------------------------------------------------------------//
auto CMGSource::OnClickAlphaHalf() -> void {
	if (m_pTBAlpha) {
		m_pTBAlpha->SetPos(0.5f);
	}
}


//---------------------------------------------------------------------------//
//	OnCheckedAdjustToBpm
//---------------------------------------------------------------------------//
auto CMGSource::OnCheckedAdjustToBpm() -> void {
	m_pSBAdjustToBPM->Set(Round((m_pVJController->SourceGetLength(m_uSourceID) * 1000.f / g_pAppMain->BeatManager()->GetMasterBpmInv())));
}


//---------------------------------------------------------------------------//
//	OnCheckedReset
//---------------------------------------------------------------------------//
auto CMGSource::OnCheckedReset() -> void {
	m_pVJController->SourceSetReset(m_uSourceID, m_pCHReset->Checked());
}


//---------------------------------------------------------------------------//
//	OnChangeBpmAdjust
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeBpmAdjust() -> void {
	if (m_pCBAdjustToBPM->Checked()) {
		if (m_pSBAdjustToBPM->GetValue() > 0) {
			float speed = m_pVJController->SourceGetLength(m_uSourceID) / (m_pSBAdjustToBPM->GetValue() * g_pAppMain->BeatManager()->GetMasterBpmInv() / 1000.f);
			m_pVJController->SourceSetSpeed(m_uSourceID, speed);
		} else {
			m_pVJController->SourceSetSpeed(m_uSourceID, 0.f);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnChangeAlpha
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeAlpha() -> void {
	if (m_pTBAlpha) {
		m_pVJController->SourceSetAlpha(m_uSourceID, m_pTBAlpha->GetPos());
	}
}


//---------------------------------------------------------------------------//
//	OnClickLinkAlpha
//---------------------------------------------------------------------------//
auto CMGSource::OnClickLinkAlpha() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkAlpha,
		m_pBLinkAlpha->Left(),
		m_pBLinkAlpha->Top(),
		ILinkBehaviour::TRACK_BAR,
		false,
		m_pTBAlpha,
		m_pBLinkAlpha,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnChangeBlendMode
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeBlendMode() -> void {
	SetBlend(m_pCBBlendModes->GetSelected());
}


//---------------------------------------------------------------------------//
//	OnClickLinkBlend
//---------------------------------------------------------------------------//
auto CMGSource::OnClickLinkBlend() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkBlend,
		m_pBLinkBlend->Left(),
		m_pBLinkBlend->Top(),
		ILinkBehaviour::COMBO_BOX,
		false,
		m_pCBBlendModes,
		m_pBLinkBlend,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnChangeScope
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeScope() -> void {
	m_pCBObjects->Clear();

	// Add new objects
	std::vector<std::string> vObjects;
	m_pVJController->SourceGetVarObjects(m_uSourceID, vObjects, m_pCBScopes->GetSelected());
	for (uint j = 0; j < vObjects.size(); ++j) {
		m_pCBObjects->Add(vObjects[j]);
	}

	// Select first
	if (vObjects.size() > 0) {
		m_pCBObjects->Select(0);
	} else {
		if (m_pActiveScopePanel) {
			m_pActiveScopePanel->SetParent(nullptr);
			m_pActiveScopePanel = nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	OnChangeObject
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeObject() -> void {
	if (m_pActiveScopePanel) {
		m_pActiveScopePanel->SetParent(nullptr);
		m_pActiveScopePanel = nullptr;
	}

	if (m_vvpVJScopeVars.size() > 0) {
		m_pActiveScopePanel = m_vvpVJScopeVars[m_pCBScopes->GetSelected()][m_pCBObjects->GetSelected()];
		if (m_pActiveScopePanel) {
			m_pActiveScopePanel->SetParent(m_pScopePanel);
		}
	}

	m_pPageObjects->OnResize();
}


//---------------------------------------------------------------------------//
//	OnChangeQuality
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeQuality() -> void {
	m_pVJController->SourceSetQuality(m_uSourceID, m_pCBQuality->GetSelected());
}


//---------------------------------------------------------------------------//
//	OnChangeFadeTime
//---------------------------------------------------------------------------//
auto CMGSource::OnChangeFadeTime() -> void {
	m_pVJController->SourceSetFade(m_uSourceID, m_pSBFade->GetValue() * 0.001f);
}


//---------------------------------------------------------------------------//
//	GetRelativePath
//---------------------------------------------------------------------------//
//auto GetRelativePath(filesys::path p) -> filesys::path {
//	filesys::path p_dir;
//	bool bMatchNeon = false;
//	bool bMatchSources = false;
//	for (auto it = p.begin(); it != p.end(); ++it) {
//		if (bMatchSources) {
//			p_dir /= *it;
//		}
//		if (!bMatchNeon && *it == neon::sDirNeon) {
//			bMatchNeon = true;
//		}
//		if (bMatchNeon && !bMatchSources && *it == neon::sDirSources) {
//			bMatchSources = true;
//		}
//	}
//	return p_dir;
//}


//---------------------------------------------------------------------------//
//	OnClickSave
//---------------------------------------------------------------------------//
auto CMGSource::OnClickSave() -> void {
	std::string sFile;
	// Save Source Definition...
	if (g_pMGApp->OpenFileSave("Save Neon Source Definition", neon::sDirSources, "Neon Source Definition (*.nsd)\0*.nsd\0\0", "*.nsd", sFile)) {
		filesys::path p(sFile);
		// ebp(2014)-> commented this out because nsd's didn't have a 'dir' written out.
		// instead keep the previously set m_sDir, which seems to work.  probably was a reason I was trying to do this.
		//filesys::path p_dir = GetRelativePath(p);
		//m_sDir = p_dir.parent_path().string();

		if (m_sName.empty()) {
			m_sName = p.stem().string();
		}
		Save(sFile);
		m_pLTitle->Set(m_sName);
		m_pEBName->SetText(m_sName);
	}
}


//---------------------------------------------------------------------------//
//	OnClickSourceName
//---------------------------------------------------------------------------//
auto CMGSource::OnClickSourceName() -> void {
	m_pLTitle->SetVisible(false);
	m_pEBName->SetFocus(true);
	m_pEBName->SetText(m_pLTitle->Get());
	m_pEBName->SetVisible(true);
}


//---------------------------------------------------------------------------//
//	OnEditSourceName
//---------------------------------------------------------------------------//
auto CMGSource::OnEditSourceName() -> void {
	m_pEBName->SetVisible(false);
	if (!m_pEBName->GetText().empty()) {
		GLOG(("New Name: %s\n", m_pEBName->GetText().c_str()));
		m_pLTitle->Set(m_pEBName->GetText());
		m_sName = m_pEBName->GetText();
	}
	m_pLTitle->SetVisible(true);
}


//---------------------------------------------------------------------------//
//	SpeedToTab
//---------------------------------------------------------------------------//
auto CMGSource::SpeedToTab(float fSpeed) -> float {
	return fSpeed * 0.2f;
}


//---------------------------------------------------------------------------//
//	TabToSpeed
//---------------------------------------------------------------------------//
auto CMGSource::TabToSpeed(float fSpeed) -> float {
	return fSpeed * 5.f;
}
