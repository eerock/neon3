//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "VJVar/VJVar.h"
#include "Controls/MGPanel.h"
#include "Controls/MGListBox.h"
#include "MGEffects.h"
#include "MGEffect.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGEffects::CMGEffects(CMGControl* pParent, CMGSource* pSource)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_pPanelSup(nullptr)
	, m_pPanelInf(nullptr)
	, m_pLBEffects(nullptr)
	, m_pActiveEffectVars(nullptr)
{
	m_iAlign = MGALIGN_CLIENT;

	// magic fuckin numbers
	m_pPanelSup = NEW CMGPanel(this, 0, 0, 0, 152, MGALIGN_TOP);
	m_pPanelInf = NEW CMGPanel(this, 0, 0, 0, 0, MGALIGN_CLIENT);
	m_pLBEffects = NEW CMGListBox(m_pPanelSup, 1, 0, 0, 116, MGALIGN_TOP, true);

	m_pLBEffects->m_fnOnSelectItem = bind(&CMGEffects::OnSelectEffect, this, _1);

	for (auto& name : g_pAppMain->EffectList()) {
		m_pLBEffects->Add(name);
	}

	// Effect slots
	for (int i = 0; i < MAX_EFFECTS; ++i) {
		m_apEffects[i] = NEW CMGEffect(m_pPanelSup, this, pSource, 1 + 59 * i, 117, i);	// magic fuckin numbers
	}

	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGEffects::~CMGEffects() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGEffects\n"));
#endif
	if (m_pActiveEffectVars) {
		m_pActiveEffectVars->SetParent(nullptr);
		m_pActiveEffectVars = nullptr;
	}

	//for (int i = 0; i < MAX_EFFECTS; ++i) {
	//	DISPOSE(m_apEffects[i]);
	//}
	//DISPOSE(m_pLBEffects);
	//DISPOSE(m_pPanelInf);
	//DISPOSE(m_pPanelSup);
}


//---------------------------------------------------------------------------//
//	SwapEffects
//---------------------------------------------------------------------------//
auto CMGEffects::SwapEffects(int iSlotSrc, int iSlotDst) -> void {
	ASSERT(ValidIndex(iSlotSrc, MAX_EFFECTS));
	ASSERT(ValidIndex(iSlotDst, MAX_EFFECTS));
	Swap(m_apEffects[iSlotSrc], m_apEffects[iSlotDst]);
}


//---------------------------------------------------------------------------//
//	OnSelectEffect
//---------------------------------------------------------------------------//
auto CMGEffects::OnSelectEffect(CMGListBox* pListBox) -> void {
	std::string sItem;
	if (pListBox->GetSelected(sItem)) {
		pListBox->BeginDrag();
	}
}


//---------------------------------------------------------------------------//
//	ShowEffectVars
//---------------------------------------------------------------------------//
auto CMGEffects::ShowEffectVars(CMGControl* pEffect) -> void {
	if (m_pActiveEffectVars) {
		m_pActiveEffectVars->SetParent(nullptr);
	}

	m_pActiveEffectVars = pEffect;

	if (m_pActiveEffectVars) {
		m_pActiveEffectVars->SetParent(m_pPanelInf);
	}

	GetParent()->OnResize();
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGEffects::Update() -> void {
	for (int i = 0; i < MAX_EFFECTS; ++i) {
		m_apEffects[i]->Update();
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CMGEffects::Load(CNodeFile::CNode* pNode) -> void {
	auto pNodeEffect = pNode->FirstNode("effect");
	while (pNodeEffect) {
		int cell = pNodeEffect->AttrAsInt("cell");
		std::string const& sName = pNodeEffect->AttrAsString("name");
		if (ValidIndex(cell, MAX_EFFECTS) && sName != "") {
			if (m_apEffects[cell]->Load(sName.c_str())) {
				m_apEffects[cell]->Load(pNodeEffect);
			}
		}
		pNodeEffect = pNodeEffect->NextNode("effect");
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGEffects::Save(CNodeFile* pFile) -> void {
	for (int i = 0; i < MAX_EFFECTS; ++i) {
		if (m_apEffects[i]->IsLoaded()) {
			std::stringstream ss;
			ss << "name=\"" << m_apEffects[i]->GetName() << "\" cell=\"" << i << "\"";
			pFile->WriteOpenNode("effect", ss.str());
			m_apEffects[i]->Save(pFile);
			pFile->WriteCloseNode();
		}
	}
}
