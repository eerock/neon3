//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_NEONMAIN_H
#define NEON_NEONMAIN_H

#include "Controls/MGWindow.h"
#include "MGGridPanel.h"

//#define MULTITHREADED
#ifdef MULTITHREADED
#include <thread>
#endif


class CLink;
class CMGApplication;
class CMGBeatManager;
class CMGButton;
class CMGCheckBox;
class CMGComboBox;
class CMGSource;
class CMGGridPanel;
class CMGGroupBox;
class CMGLabel;
class CMGLayer;
class CMGPage;
class CMGPageControl;
class CMGPanel;
class CMGPanelWin;
class CMGTrackBar;
class CMGTreeItem;
class CMGTreeView;
class CMGWindowNewSource;
class CMGWindowVarLinker;
class CMGWindowConfirm;
class CMidiDevice;
class CTimer;
class CVJController;
class CVJVar;


//---------------------------------------------------------------------------//
//	Class CMGAppMain
//---------------------------------------------------------------------------//
class CMGAppMain {
public:
	CMGAppMain();
	~CMGAppMain();

	// core...
	auto Init(THInst hInstance) -> bool;
	auto Update() -> bool;
	auto UpdateMain() -> void;
	auto DrawMain() -> void;

	// project file management...
	auto NewProject() -> void;
	auto LoadProject() -> void;
	auto SaveProject(bool bSaveAs) -> void;
	auto ResetProject() -> void;

	// new and delete source (unfinished)...
	auto NewSource() -> void;
	auto OnNewSource(CMGWindow::EModalResult eResult) -> void;
	auto DelSource() -> void;
	auto OnDelSource(CMGWindow::EModalResult eResult) -> void;

	// attach / unattach to master output layers...
	auto AttachSource(CMGSource* pSource, int iLayer) -> void;
	auto UnattachSource(CMGSource* pSource) -> void;

	// open var linker window...
	auto OpenVarLinker(CLink** pLink, int iX, int iY, int iBehType, bool bOnlyMidiKey, 
		CMGControl* pRControl, CMGButton* pLinkButton, function<void(CMGWindow::EModalResult)> fnOnModalResult) -> void;
	auto OpenVarLinker(CLink** pLink, int iX, int iY, int iBehType, 
		CMGButton* pLinkButton, function<void(CMGWindow::EModalResult)> fnOnModalResult) -> void;
	auto OnBtnLinkClick(CVJVar* pVJVar) -> void;

	auto EffectList() -> std::vector<std::string>& { return m_vEffectsList; }

	// active source...
	auto ShowSource(CMGSource* pSource) -> void;
	auto RemoveActiveSource() -> void;		// kinda hacky
	auto GetActiveSource() const -> CMGSource* { return m_pActiveSource; }

	// active layer...
	auto ShowActiveLayer() -> void;
	auto SetActiveLayer(int iLayer) -> void;
	auto GetActiveLayer() const -> int { return m_iActiveLayer; }

	auto ShowBrowse() -> void;

	// get sub-objects...
	auto SourceTree() const -> CMGTreeView* { return m_pTreeSources; }
	auto BeatManager() const -> CMGBeatManager* { return m_pBeatManager; }
	auto VJController() const -> CVJController* { return m_pVJController; }
	auto Timer() const -> CTimer* { return m_pTimer; }
	auto MidiDevice() const -> CMidiDevice* { return m_pMidiDevice; }

	// quit application...
	auto QuitApplication() -> void;
	auto OnConfirmQuit(CMGWindow::EModalResult eResult) -> void;

	// handle drag-n-dropped files...
	auto DropFiles(uint uNumFiles, std::vector<std::string> const& vsFiles) -> void;

private:
	auto LoadSources() -> void;
	auto LoadSourcesDir(CMGTreeItem* pParent) -> void;
	auto LoadEffectsDir() -> void;
	auto RefreshDirectories() -> void;

	auto CreateBase() -> void;
	auto CreatePanelData() -> void;
	auto CreatePanelTop() -> void;
	auto CreateWindows() -> void;

	auto CreateBrowseTab() -> void;
	auto CreateSourceTab() -> void;
	auto CreateLayerTab() -> void;
	auto CreateOptionsTab() -> void;

	auto RefreshMidiDevices() -> void;
	auto OnChangeMidiDevice() -> void;
	auto OnChangeMidiChannel() -> void;
	auto OnChangeClearColor() -> void;
	auto OnChangeMasterAlpha() -> void;
	auto OnChangeMasterBypass() -> void;
	auto OnLinkMasterAlpha() -> void;
	auto OnLinkMasterBypass() -> void;

	bool m_bOk;
	int m_iActiveLayer;
	std::string m_sCurrentProject;
	CVJController* m_pVJController;
	CMGApplication* m_pApplication;
	CMGSource* m_pActiveSource;
	CTimer* m_pTimer;
	CMidiDevice* m_pMidiDevice;
	CMGTreeView* m_pTreeSources;

	// Base
	CMGGroupBox* m_pTopPanel;
	CMGGroupBox* m_pGBDatos;
	CMGLabel* m_pProjectLabel;

	// BeatManager
	CMGBeatManager* m_pBeatManager;

	// Datas
	CMGPageControl* m_pDataPages;
	CMGPage* m_pPageBrowse;
	CMGPage* m_pPageSource;
	CMGPage* m_pPageLayer;
	CMGPage* m_pPageOptions;

	CMGCheckBox* m_pCHFullScreen;
	CMGButton* m_pBtnResetWindows;
	CMGTrackBar* m_pTBMasterAlpha;
	CMGCheckBox* m_pCHMasterBypass;
	CMGButton* m_pBtnLinkMasterAlpha;
	CMGButton* m_pBtnLinkMasterBypass;
	CLink* m_pLinkMasterAlpha;
	CLink* m_pLinkMasterBypass;
	CMGComboBox* m_pCBMidiDevices;
	CMGComboBox* m_pCBMidiChannel;
	CMGTrackBar* m_pTBClearR;
	CMGTrackBar* m_pTBClearG;
	CMGTrackBar* m_pTBClearB;

	// FXList
	CMGPanel* m_pPanelSourceSup;

	// Layer Tab
	//std::vector<std::unique_ptr<CMGLayer>> m_vLayers;
	CMGLayer* m_pActiveLayer;	// weak pointer
	std::vector<CMGLayer*> m_vLayers;

	// Grid
	CMGGridPanel* m_pGridPanel;

	// Windows
	CMGWindowNewSource* m_pWindowNewSource;
	CMGWindowVarLinker* m_pWindowVarLinker;
	CMGWindowConfirm* m_pWindowConfirmDelSource;
	CMGWindowConfirm* m_pWindowConfirmQuit;

	// Effect list
	std::vector<std::string> m_vEffectsList;
};

// ebp-> Global extern CMGAppMain g_pAppMain!
extern CMGAppMain* g_pAppMain;

#endif//NEON_NEONMAIN_H
