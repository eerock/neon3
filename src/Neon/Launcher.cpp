//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//


#include "NeonPch.h"

#include "AppOptions.h"
#include "DisplayWindow.h"
#include "Launcher.h"
#include "MidiDevice.h"
#include "resource1.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <WinUser.h>
#include <d3d9.h>

// work in progress
#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")
#endif//PLATFORM

static CLauncher* s_pLauncher = nullptr;


//---------------------------------------------------------------------------//
//	STATIC: LauncherDialogProc
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CALLBACK CLauncher::LauncherDialogProc(THWnd hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) -> LRESULT {
	if (s_pLauncher != nullptr) {
		if (s_pLauncher->m_hDlgWnd == 0) {
			s_pLauncher->m_hDlgWnd = hWnd;
		}
		return s_pLauncher->OnMessage(uMsg, wParam, lParam);
	}
	return FALSE;
}
#endif//PLATFORM


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CLauncher::CLauncher()
	: m_bLaunch(false)
	, m_bEnumeratingMonitors(false)
	, m_nAdapters(0)
	, m_nModes(0)
	, m_hDlgWnd(0)
	, m_hComboDevice(0)
	, m_hComboResolution(0)
	, m_tOpts()
#if (PLATFORM == PLATFORM_WINDOWS)
	, m_tMonitorRect()
	, m_hMonitor()
#endif//PLATFORM
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	, m_pD3D(nullptr)
	, m_vModes()
	, m_vAdapters()
#endif//GFX_ENGINE_DIRECT3D
{
	s_pLauncher = this;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLauncher::~CLauncher() {
	s_pLauncher = nullptr;
}


//---------------------------------------------------------------------------//
//	Show
//---------------------------------------------------------------------------//
auto CLauncher::Show(THInst hInstance) -> bool {
#if (PLATFORM == PLATFORM_WINDOWS)

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp-> Direct3D 9
	m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (!m_pD3D) {
		return false;
	}
#endif//GFX_ENGINE_DIRECT3D

	InitCommonControls();
	
	// Create the DialogBox from resources template...
	INT_PTR result = DialogBox(hInstance, MAKEINTRESOURCE(IDD_LAUNCHER_DIALOG), NULL, (DLGPROC)LauncherDialogProc);

	if (result == 1) {
		GLOG(("Launcher Canceled: Bye and good luck with all that.\n"));
	}

	if (result == 0) {
		GLOG(("Launching Neon...\n"));
	}

	SaveData();

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	SafeRelease(&m_pD3D);
#endif//GFX_ENGINE_DIRECT3D

#endif//PLATFORM

	return m_bLaunch;
}


//---------------------------------------------------------------------------//
//	InitDialog
//---------------------------------------------------------------------------//
auto CLauncher::InitDialog() -> void {

	// Load program options
	auto pAppOpts = CServiceLocator<IAppOptionsService>::GetService();

	m_tOpts.m_iDevice = pAppOpts->Get("neonconfig.display.device", 0);				// rename to 'monitor' or 'display'
	m_tOpts.m_bFullscreen = pAppOpts->Get("neonconfig.display.fullscreen", false);	// fullscreen expands from the set res to monitor fullscreen
	m_tOpts.m_bShowFps = pAppOpts->Get("neonconfig.display.showfps", false);		// display FPS text
	m_tOpts.m_iRefreshRate = pAppOpts->Get("neonconfig.display.refresh", 60);		// refresh rate (comes from config)
	m_tOpts.m_iRWidth = pAppOpts->Get("neonconfig.display.rwidth", DISPLAY_WINDOW_WIDTH);		// display rect width
	m_tOpts.m_iRHeight = pAppOpts->Get("neonconfig.display.rheight", DISPLAY_WINDOW_HEIGHT);	// display rect height
	m_tOpts.m_iRLeft = pAppOpts->Get("neonconfig.display.rleft", 0);				// display rect left
	m_tOpts.m_iRTop = pAppOpts->Get("neonconfig.display.rtop", 0);					// display rect top
	m_tOpts.m_iGuiRLeft = pAppOpts->Get("neonconfig.guiwindow.rleft", 0);			// gui window rect left
	m_tOpts.m_iGuiRTop = pAppOpts->Get("neonconfig.guiwindow.rtop", 0);				// gui window rect top
	m_tOpts.m_iDefaultMidiChannel = pAppOpts->Get("neonconfig.midi.channel", 0);	// default midi channel
	m_tOpts.m_sDefaultMidiDevice = pAppOpts->Get("neonconfig.midi.device", CMidiDevice::DEVICE_UNKNOWN);	// default midi device
	m_tOpts.m_bNeonPlayer = pAppOpts->Get("neonconfig.player", false);				// run the LUA neon player

	m_hComboDevice = GetDlgItem(m_hDlgWnd, IDC_COMBO_DEVICE);
	m_hComboResolution = GetDlgItem(m_hDlgWnd, IDC_COMBO_RESOLUTION);

#if (PLATFORM == PLATFORM_WINDOWS)
	{ std::stringstream ss;
	ss << m_tOpts.m_iRWidth;
	SendDlgItemMessage(m_hDlgWnd, IDC_CUSTOM_RES_W, WM_SETTEXT, 0L, (LPARAM)ss.str().c_str());
	}

	{ std::stringstream ss;
	ss << m_tOpts.m_iRHeight;
	SendDlgItemMessage(m_hDlgWnd, IDC_CUSTOM_RES_H, WM_SETTEXT, 0L, (LPARAM)ss.str().c_str());
	}

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// Devices
	m_nAdapters = m_pD3D->GetAdapterCount();
	if (m_tOpts.m_iDevice >= static_cast<int>(m_nAdapters)) {
		m_tOpts.m_iDevice = 0;
	}

	m_vAdapters.resize(m_nAdapters);

	// enumerate adapters (Displays)...
	for (int i = 0; i < static_cast<int>(m_nAdapters); ++i) {
		m_pD3D->GetAdapterIdentifier(i, 0, &m_vAdapters[i]);
		std::stringstream ss;
		ss << m_vAdapters[i].Description << " on " << m_vAdapters[i].DeviceName;

		// add strings...
		ComboBox_AddString(m_hComboDevice, ss.str().c_str());

		// select the desired device from config...
		if (m_tOpts.m_iDevice == i) {
			ComboBox_SetCurSel(m_hComboDevice, i);
		}
	}
#endif//GFX_ENGINE_DIRECT3D

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	// ebp(2014)-> TODO: re-enable modes to make selection easier.
	// add back a radio control for preconfig combo box of selections or
	// custom resolution (and refresh rate) using the text boxes?

	// Update modes
	m_iMode = 0;
	SendDlgItemMessage(m_hDlgWnd, IDC_COMBO_RESOLUTION, CB_RESETCONTENT, 0L, 0L);
	m_nModes = m_pD3D->GetAdapterModeCount(m_tOpts.m_iDevice, D3DFMT_X8R8G8B8);

	for (int i = 0; i < static_cast<int>(m_nModes); ++i) {
		D3DDISPLAYMODE mode;
		auto hr = m_pD3D->EnumAdapterModes(m_tOpts.m_iDevice, D3DFMT_X8R8G8B8, i, &mode);
		if (SUCCEEDED(hr)) {
			if (mode.RefreshRate >= 60) {
				m_vModes.push_back(mode);
			} else {
				continue;
			}
			std::stringstream ss;
			ss << m_vModes.back().Width << " x " << m_vModes.back().Height << " @ " << m_vModes.back().RefreshRate << " Hz";
			ComboBox_AddString(m_hComboResolution, ss.str().c_str());
		}
	}

	ComboBox_SetCurSel(m_hComboResolution, m_iMode);
	m_nModes = m_vModes.size();
#endif//GFX_ENGINE_DIRECT3D

#endif//PLATFORM

	RefreshControls();
}


//---------------------------------------------------------------------------//
//	RefreshControls
//	Refreshes the enabled state of the various dialog controls based on state
//	of the data that has been set.
//---------------------------------------------------------------------------//
auto CLauncher::RefreshControls() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	//EnableWindow(GetDlgItem(m_hDlg, IDC_MFCEDITBROWSE1), m_tOpts.m_bPlayer);
	EnableWindow(GetDlgItem(m_hDlgWnd, IDOK), m_tOpts.m_iRWidth > 10 && m_tOpts.m_iRHeight > 10);
	EnableWindow(GetDlgItem(m_hDlgWnd, IDC_COMBO_DEVICE), m_nAdapters > 1);
	EnableWindow(GetDlgItem(m_hDlgWnd, IDC_COMBO_RESOLUTION), (0 == IsDlgButtonChecked(m_hDlgWnd, IDC_CUSTOM_RESOLUTION)));
	EnableWindow(GetDlgItem(m_hDlgWnd, IDC_CUSTOM_RES_W), (0 != IsDlgButtonChecked(m_hDlgWnd, IDC_CUSTOM_RESOLUTION)));
	EnableWindow(GetDlgItem(m_hDlgWnd, IDC_CUSTOM_RES_H), (0 != IsDlgButtonChecked(m_hDlgWnd, IDC_CUSTOM_RESOLUTION)));
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	NeonEnumMonitors
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CALLBACK NeonEnumMonitors(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData) -> BOOL {
	return s_pLauncher->EnumMonitors(hMonitor, hdcMonitor, lprcMonitor, dwData);
}
#endif//PLATFORM


//---------------------------------------------------------------------------//
//	EnumMonitors
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CLauncher::EnumMonitors(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData) -> BOOL {
	UNREFERENCED_PARAMETER(hdcMonitor);
	UNREFERENCED_PARAMETER(dwData);
	if (m_bEnumeratingMonitors) {
		if (m_hMonitor == hMonitor) {
			m_tMonitorRect = *lprcMonitor;
		} else {
			return true;
		}
	}
	return false;
}
#endif//PLATFORM


//---------------------------------------------------------------------------//
//	SaveData
//---------------------------------------------------------------------------//
auto CLauncher::SaveData() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	auto pAppOpts = CServiceLocator<IAppOptionsService>::GetService();

	m_bEnumeratingMonitors = true;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	m_hMonitor = m_pD3D->GetAdapterMonitor(m_tOpts.m_iDevice);
#endif//GFX_ENGINE_DIRECT3D
	EnumDisplayMonitors(NULL, NULL, (MONITORENUMPROC)NeonEnumMonitors, 0);
	m_bEnumeratingMonitors = false;

	pAppOpts->Set("neonconfig.display.device", m_tOpts.m_iDevice);
	pAppOpts->Set("neonconfig.display.fullscreen", m_tOpts.m_bFullscreen);
	pAppOpts->Set("neonconfig.display.showfps", m_tOpts.m_bShowFps);
	pAppOpts->Set("neonconfig.display.refresh", m_tOpts.m_iRefreshRate);
	pAppOpts->Set("neonconfig.display.rwidth", m_tOpts.m_iRWidth);
	pAppOpts->Set("neonconfig.display.rheight", m_tOpts.m_iRHeight);
	pAppOpts->Set("neonconfig.display.rleft", m_tOpts.m_iRLeft);
	pAppOpts->Set("neonconfig.display.rtop", m_tOpts.m_iRTop);
	pAppOpts->Set("neonconfig.guiwindow.rleft", m_tOpts.m_iGuiRLeft);
	pAppOpts->Set("neonconfig.guiwindow.rtop", m_tOpts.m_iGuiRTop);
	pAppOpts->Set("neonconfig.midi.channel", m_tOpts.m_iDefaultMidiChannel);
	pAppOpts->Set("neonconfig.midi.device", m_tOpts.m_sDefaultMidiDevice);
	pAppOpts->Set("neonconfig.player", m_tOpts.m_bNeonPlayer);
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	GetTextBoxValue
//---------------------------------------------------------------------------//
auto CLauncher::GetTextBoxValue(int iID) -> int {
#if (PLATFORM == PLATFORM_WINDOWS)
	char pBuffer[10];
	int iLen = static_cast<int>(SendDlgItemMessage(m_hDlgWnd, iID, WM_GETTEXTLENGTH, 0L, 0L));
	SendDlgItemMessage(m_hDlgWnd, iID, WM_GETTEXT, Max(iLen + 1, 10), (LPARAM)pBuffer);
	return string_to<int>(pBuffer);
#else
    return 0;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	OnMessage
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CLauncher::OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) -> LRESULT {
	UNREFERENCED_PARAMETER(lParam);

	switch (uMsg) {
		
		// Init Dialog
		case WM_INITDIALOG: {
			InitDialog();
			break;
		}

		// Command
		case WM_COMMAND: {

			int wmId = LOWORD(wParam);
			int wmEvent = HIWORD(wParam);

			// Parse commands
			switch (wmId) {

				// Launch
				case IDOK: {
					m_bLaunch = true;
					EndDialog(m_hDlgWnd, 0);
					break;
				}

				// Cancel
				case IDCANCEL: {
					m_bLaunch = false;
					EndDialog(m_hDlgWnd, 1);
					break;
				}

				// Device Combobox
				case IDC_COMBO_DEVICE: {
					if (wmEvent == CBN_SELENDOK) {
						m_tOpts.m_iDevice = ComboBox_GetCurSel(m_hComboDevice);
						m_iMode = 0;
					}
					break;
				}

				// Prefab Resolution Combobox
				case IDC_COMBO_RESOLUTION: {
					if (wmEvent == CBN_SELENDOK) {
						m_iMode = ComboBox_GetCurSel(m_hComboResolution);
						m_tOpts.m_iRWidth = m_vModes[m_iMode].Width;
						m_tOpts.m_iRHeight = m_vModes[m_iMode].Height;
						m_tOpts.m_iRefreshRate = m_vModes[m_iMode].RefreshRate;
					}
					break;
				}

				// Custom Resolution Checkbox
				case IDC_CUSTOM_RESOLUTION: {
					//bool val = (0 != IsDlgButtonChecked(m_hDlgWnd, IDC_CUSTOM_RESOLUTION));
					break;
				}

				// Display Width
				case IDC_CUSTOM_RES_W: {
					if (wmEvent == EN_CHANGE) {
						m_tOpts.m_iRWidth = GetTextBoxValue(IDC_CUSTOM_RES_W);
					}
					break;
				}

				// Display Height
				case IDC_CUSTOM_RES_H: {
					if (wmEvent == EN_CHANGE) {
						m_tOpts.m_iRHeight = GetTextBoxValue(IDC_CUSTOM_RES_H);
					}
					break;
				}

				// LuaPlayer (NeonPlayer)
				case IDC_NEON_PLAYER: {
					m_tOpts.m_bNeonPlayer = (0 != IsDlgButtonChecked(m_hDlgWnd, IDC_NEON_PLAYER));
					break;
				}

				// Go Fullscreen
				case IDC_GO_FULLSCREEN: {
					m_tOpts.m_bFullscreen = (0 != IsDlgButtonChecked(m_hDlgWnd, IDC_GO_FULLSCREEN));
					break;
				}

				// Show FPS
				case IDC_SHOW_FPS: {
					m_tOpts.m_bShowFps = (0 != IsDlgButtonChecked(m_hDlgWnd, IDC_SHOW_FPS));
					break;
				}

				default: {
					return FALSE;
				}
			}
			break;
		}
		default: {
			return FALSE;
		}
	}

	RefreshControls();

	// handled message!
	return TRUE;	
}
#endif//PLATFORM
