//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "MGAppWindow.h"
#include "MGApplication.h"
#include "MGSurface.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <WinUser.h>
#include <WindowsX.h>
#include <ShellAPI.h>
#endif//PLATFORM


//---------------------------------------------------------------------------//
class CMGAppWindow : public IMGAppWindow {
public:
	~CMGAppWindow() override;
	auto Init(THInst hInst, int iX, int iY, int iWidth, int iHeight) -> bool;

	auto ProcessMessages() -> void override;
	auto Flip() -> void override;

#if (PLATFORM == PLATFORM_WINDOWS)
	auto GetHDC() const -> THdc override {
		return m_hDC;
	}
	auto GetHWnd() const -> THWnd override {
		return m_hWnd;
	}

private:
	THdc m_hDC;
	HGLRC m_hRC;
	THWnd m_hWnd;
#endif//PLATFORM
};


//---------------------------------------------------------------------------//
//	STATIC: HandleFilesDropped
//---------------------------------------------------------------------------//
static auto HandleFilesDropped(HDROP hDrop) -> void {
	uint uMaxFilename = 0;
	uint uNumFiles = DragQueryFile(hDrop, 0xFFFFFFFF, nullptr, 0);
	for (uint u = 0; u < uNumFiles; ++u) {
		uMaxFilename = Max(uMaxFilename, DragQueryFile(hDrop, u, nullptr, 0));
	}
	++uMaxFilename;

	std::vector<std::string> vsFiles;
	char* pBuffer = NEW_ARRAY(char, uMaxFilename);
	for (uint u = 0; u < uNumFiles; ++u) {
		DragQueryFile(hDrop, u, pBuffer, uMaxFilename);
		GLOG(("File %d: %s\n", u, pBuffer));
		vsFiles.push_back(pBuffer);
	}

	POINT tPoint;
	if (DragQueryPoint(hDrop, &tPoint)) {
		GLOG(("Position Dropped: (%d, %d)\n", tPoint.x, tPoint.y));
	}

	g_pAppMain->DropFiles(uNumFiles, vsFiles);

	vsFiles.clear();
	DELETE_ARRAY(pBuffer);
}


//---------------------------------------------------------------------------//
//	STATIC: WindowProc
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
static auto CALLBACK WindowProc(THWnd hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) -> LRESULT {
	switch (Msg) {
		case WM_DESTROY: {
			PostQuitMessage(0);
			return NULL;
		}
		case WM_MOVE:
		case WM_SIZE: {
			g_pMGApp->MustRepaint();
			return NULL;
		}
		case WM_SETFOCUS: {
			break;
		}
		case WM_CLOSE: {
			g_pMGApp->Close();
			return NULL;
		}
		case WM_ACTIVATEAPP: {
			g_pMGApp->MustRepaint();
			return NULL;
		}
		case WM_SETCURSOR: {
			//SetCursor(LoadCursor(0, IDC_ARROW));
			SetCursor(CServiceLocator<IAppSkinService>::GetService()->Cursor(CUR_NORMAL));
			return NULL;
		}
		case WM_KEYDOWN: {
			g_pMGApp->OnVKeyDown((int)wParam);
			return NULL;
		}
		case WM_KEYUP: {
			g_pMGApp->OnVKeyUp((int)wParam);
			return NULL;
		}
		case WM_CHAR: {
			g_pMGApp->OnChar((char)wParam);
			return NULL;
		}
		case WM_MOUSEMOVE: {
			g_pMGApp->OnMouseMove(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			return NULL;
		}
		case WM_LBUTTONDOWN: {
			SetCapture(hWnd);
			g_pMGApp->OnMouseDown(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam), MOUSE_LEFT);
			return NULL;
		}
		case WM_LBUTTONUP: {
			ReleaseCapture();
			g_pMGApp->OnMouseUp(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam), MOUSE_LEFT);
			return NULL;
		}
		case WM_LBUTTONDBLCLK: {
			g_pMGApp->OnMouseDoubleClk(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam), MOUSE_LEFT);
			return NULL;
		}
		case WM_RBUTTONDOWN: {
			SetCapture(hWnd);
			g_pMGApp->OnMouseDown(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam), MOUSE_RIGHT);
			return NULL;
		}
		case WM_RBUTTONUP: {
			ReleaseCapture();
			g_pMGApp->OnMouseUp(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam), MOUSE_RIGHT);
			return NULL;
		}
		case WM_MOUSEWHEEL: {
			POINT p;
			p.x = GET_X_LPARAM(lParam);
			p.y = GET_Y_LPARAM(lParam);
			ScreenToClient(hWnd, &p);

			// ebp-> win api says the hiword of the wParam indicates the distance the wheel is rotated,
			// expressed in multiples of WHEEL_DELTA=120. Positive means wheel was rotated to the right,
			// negative means wheel was rotated left.
			if ((short)HIWORD(wParam) > 0) {
				// forward away from user
				g_pMGApp->OnMouseDown(p.x, p.y, MOUSE_WHEEL_UP);
			} else {
				// backward toward user
				g_pMGApp->OnMouseDown(p.x, p.y, MOUSE_WHEEL_DOWN);
			}
			return NULL;
		}
		case WM_CAPTURECHANGED: {
			break;
		}
		case WM_DROPFILES: {
			HandleFilesDropped((HDROP)wParam);
			break;
		}
		default: {
			break;
		}
	}

	return DefWindowProc(hWnd, Msg, wParam, lParam);
}
#endif//PLATFORM


//---------------------------------------------------------------------------//
//	STATIC: Create
//---------------------------------------------------------------------------//
auto IMGAppWindow::Create(THInst hInst, int iX, int iY, int iWidth, int iHeight) -> IMGAppWindow* {
#if (PLATFORM == PLATFORM_WINDOWS)
	// ebp-> check the window position and if it appears bad, reposition it...
	// todo: this code is good for resetting the window, but the condition upon which it should
	// be reset is not correct.  there should be a button or key command to reset the application
	// window.
	//int iClearanceX = GetSystemMetrics(SM_CXSCREEN) - iWidth;
	//int iClearanceY = GetSystemMetrics(SM_CYSCREEN) - iHeight;
	//if (iX < 0 || iY < 0 || iX > iClearanceX || iY > iClearanceY) {
	//	iX = iClearanceX >> 1;
	//	iY = iClearanceY >> 1;
	//}

	auto pAppWindow = NEW CMGAppWindow;
	if (!pAppWindow->Init(hInst, iX, iY, iWidth, iHeight)) {
		GLOG(("ERROR: Failed to instantiate/initialize CMGAppWindow!\n"));
		DELETE_PTR(pAppWindow);
	}

	return pAppWindow;
#else
	return nullptr;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMGAppWindow::Init(THInst hInst, int iX, int iY, int iWidth, int iHeight) -> bool {
#if (PLATFORM == PLATFORM_WINDOWS)
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
	wc.lpfnWndProc = WindowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = NULL;	//LoadIcon(wc.hInstance, (LPCTSTR)IDI_ICON1);
	wc.hCursor = NULL;	//LoadCursor(wc.hInstance, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = VERSION_NAME;
	wc.lpszClassName = VERSION_NAME;
	RegisterClass(&wc);

	RECT WindowRect;
	WindowRect.left = 0;
	WindowRect.right = iWidth;
	WindowRect.top = 0;
	WindowRect.bottom = iHeight;
	AdjustWindowRectEx(&WindowRect, WS_POPUP, FALSE, WS_EX_APPWINDOW);

	// ebp-> this creates the main GUI window...
	m_hWnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		VERSION_NAME,
		VERSION_LONG,
		WS_POPUP,
		iX, iY,
		WindowRect.right - WindowRect.left,
		WindowRect.bottom - WindowRect.top,
		NULL,
		NULL,
		hInst,
		NULL
	);

	if (!m_hWnd) {
		GLOG(("ERROR: Can't create main window!\n"));
		return false;
	}

	DragAcceptFiles(m_hWnd, true);

	static PIXELFORMATDESCRIPTOR pfd = {	// pfd Tells Windows How We Want Things To Be
		sizeof(PIXELFORMATDESCRIPTOR),		// Size Of This Pixel Format Descriptor
		1,									// Version Number
		PFD_DRAW_TO_WINDOW |				// Format Must Support Window
		PFD_SUPPORT_OPENGL |				// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,					// Must Support Double Buffering
		PFD_TYPE_RGBA,						// Request An RGBA Format
		32,									// Select Our Color Depth
		0, 0, 0, 0, 0, 0,					// Color Bits Ignored
		0,									// No Alpha Buffer
		0,									// Shift Bit Ignored
		0,									// No Accumulation Buffer
		0, 0, 0, 0,							// Accumulation Bits Ignored
		16,									// 16Bit Z-Buffer (Depth Buffer)  
		0,									// No Stencil Buffer
		0,									// No Auxiliary Buffer
		PFD_MAIN_PLANE,						// Main Drawing Layer
		0,									// Reserved
		0, 0, 0								// Layer Masks Ignored
	};

	m_hDC = GetDC(m_hWnd);
	GLuint PixelFormat = ChoosePixelFormat(m_hDC, &pfd);
	if (!SetPixelFormat(m_hDC, PixelFormat, &pfd)) {
		return false;
	}
	m_hRC = wglCreateContext(m_hDC);
	wglMakeCurrent(m_hDC, m_hRC);
#endif//PLATFORM

	return true;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGAppWindow::~CMGAppWindow() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGAppWindow\n"));
#endif
#if (PLATFORM == PLATFORM_WINDOWS)
	if (m_hWnd) {
		DestroyWindow(m_hWnd);
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	ProcessMessages
//---------------------------------------------------------------------------//
auto CMGAppWindow::ProcessMessages() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	MSG Msg;
	while (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	Flip
//---------------------------------------------------------------------------//
auto CMGAppWindow::Flip() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	SwapBuffers(m_hDC);
#endif//PLATFORM
}
