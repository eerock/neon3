//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGSOURCE_H
#define NEON_MGSOURCE_H

#include "Controls/MGControl.h"
#include "MGGridPanel.h"

class CLink;
class CMGButton;
class CMGCheckBox;
class CMGComboBox;
class CMGEditBox;
class CMGEffects;
class CMGGridItem;
class CMGGroupBox;
class CMGLabel;
class CMGPage;
class CMGPageControl;
class CMGPanel;
class CMGPanelVars;
class CMGSpinBox;
class CMGTrackBar;


//---------------------------------------------------------------------------//
//	Class CMGSource
//---------------------------------------------------------------------------//
class CMGSource : public CMGControl {
public:
	static auto Create(std::string const& sFile) -> CMGSource*;
	static auto Create(CNodeFile::CNode* pNode) -> CMGSource*;

	CMGSource(CVJController* pVJController);
	~CMGSource() override;

	auto Load(std::string const& sFile) -> bool;
	auto Load(CNodeFile::CNode* pNode) -> bool;
	auto LoadRawMediaFile(std::string const& sFile, std::string const& sClass) -> bool;
	auto LoadNeon(std::string const& sFile) -> bool;
	auto Save(std::string const& sFile) -> void;
	auto Save(CNodeFile* pNodeFile) -> void;

	auto Update() -> void;

	auto Attach(int Layer) -> void;
	auto Unattach() -> void;
	auto Move(int Layer) -> void;
	auto IsAttached() const -> bool { return m_bAttached; }
	auto GetLayerAttached() const -> int { return m_iLayerAttached; }

	auto SetGridPos(int col, int row) -> void { m_iGridCol = col; m_iGridRow = row; }
	auto SetBlend(int iBlend) -> void;

	auto GetName() const -> std::string const& { return m_sName; }
	auto GetID() const -> uint { return m_uSourceID; }
	auto GetBitmap() -> TMGSurface& { return *(m_pBitmap); }
	auto GetEffects() -> CMGEffects* { return m_pEffects; }

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGSource\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

	auto UpdateSnapshot(TVJSnapshot& snapshot) -> void;

private:
	auto LoadSnapshot(TVJSnapshot& snapshot, TMGSurface& surface) -> void;
	auto UnloadSnapshot() -> void;
	auto CreateControls() -> void;

	auto LoadVars(CNodeFile::CNode* pNode) -> void;
	auto LoadEffects(CNodeFile::CNode* pNode) -> void;
	auto LoadLinks(CNodeFile::CNode* pNode) -> void;
	auto SaveVars(CNodeFile* pFile) -> void;
	auto SaveEffects(CNodeFile* pFile) -> void;
	auto SaveLinks(CNodeFile* pFile) -> void;

	auto OnClickLinkSource() -> void;
	auto OnChangeSpeed() -> void;
	auto OnClickLinkSpeed() -> void;
	auto OnClickSpeedx1() -> void;
	auto OnClickAlphaHalf() -> void;
	auto OnCheckedAdjustToBpm() -> void;
	auto OnCheckedReset() -> void;
	auto OnChangeBpmAdjust() -> void;
	auto OnChangeAlpha() -> void;
	auto OnClickLinkAlpha() -> void;
	auto OnChangeBlendMode() -> void;
	auto OnClickLinkBlend() -> void;
	auto OnChangeScope() -> void;
	auto OnChangeObject() -> void;
	auto OnChangeQuality() -> void;
	auto OnChangeFadeTime() -> void;
	auto OnClickSave() -> void;
	auto OnClickSourceName() -> void;
	auto OnEditSourceName() -> void;
	auto SpeedToTab(float fSpeed) -> float;
	auto TabToSpeed(float fSpeed) -> float;

	// Data
	uint m_uSourceID;		// todo: uint id refactor
	int m_iLayerAttached;
	int m_iGridRow;
	int m_iGridCol;
	int m_iBlend;
	bool m_bAttached;
	std::string m_sClass;	// Class of source, e.g. "MpgVideo"
	std::string m_sFile;	// Filename of the source, e.g. "MyVideo.avi"
	std::string m_sDir;		// Directory where the file resides, e.g. "Media\MyStuff"
	std::string m_sName;	// Friendly name of the source, e.g. "My Clip"
	std::unique_ptr<TMGSurface> m_pBitmap;	// Bitmap for the Snapshot

	// Begin Pointer Hell
	CVJController* m_pVJController;	// Handy pointer to VJC

	// UI
	CMGPanel* m_pPanelTitle;
	CMGLabel* m_pLTitle;
	CMGEditBox* m_pEBName;
	CMGGroupBox* m_pGBVars;
	CMGPageControl* m_pPCVars;
	CMGPage* m_pPageEffects;
	CMGPage* m_pPageVars;
	CMGPage* m_pPageObjects;
	CMGPage* m_pPageOptions;
	CMGButton* m_pBLinkSource;
	CLink* m_pLinkSource;

	// Speed
	CMGLabel* m_pLSpeed;
	CMGTrackBar* m_pTBSpeed;
	CMGButton* m_pBLinkSpeed;
	CMGButton* m_pBSpeedx1;
	CMGCheckBox* m_pCBAdjustToBPM;
	CMGSpinBox* m_pSBAdjustToBPM;
	CLink* m_pLinkSpeed;

	// Alpha
	CMGLabel* m_pLAlpha;
	CMGButton* m_pBAlphaHalf;
	CMGTrackBar* m_pTBAlpha;
	CMGButton* m_pBLinkAlpha;
	CLink* m_pLinkAlpha;

	// Blend
	CMGComboBox* m_pCBBlendModes;
	CMGButton* m_pBLinkBlend;
	CLink* m_pLinkBlend;

	// Objects
	CMGComboBox* m_pCBScopes;
	CMGComboBox* m_pCBObjects;

	// Quality
	CMGComboBox* m_pCBQuality;
	CMGSpinBox* m_pSBFade;
	CMGCheckBox* m_pCHReset;

	// Effects
	CMGEffects* m_pEffects;

	// Vars
	CMGPanelVars* m_pVJVars;
	CMGPanelVars* m_pActiveScopePanel;
	CMGPanel* m_pScopePanel;
	std::vector<std::vector<CMGPanelVars*>> m_vvpVJScopeVars;
};

#endif//NEON_MGSOURCE_H

