//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGHOTKEY_H
#define NEON_MGHOTKEY_H

#include "MidiDevice.h"
#include "Controls/MGControl.h"

class CMGHotKey : public CMGControl, public CMidiObserver {
public:
	CMGHotKey(CMGControl *pParent, int iX, int iY, int iSize, bool bMidi, function<void()> fnOnChange);
	~CMGHotKey() override;

	virtual auto Set(char c) -> void { m_cChar = c; }
	virtual auto Get(char& c) -> void { c = m_cChar; }
	virtual auto OnChar(char c) -> void;

	auto OnMouseDown(int iX, int iY, int iButton) -> void override;

	// MidiObserver interface
	auto Update(IMidiDeviceService*) -> void override;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGHotKey\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	function<void()> m_fnOnChange;
	char m_cChar;
	bool m_bMidi;
};

#endif//NEON_MGHOTKEY_H
