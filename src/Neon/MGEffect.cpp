//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGPanel.h"
#include "Controls/MGListBox.h"
#include "VJVar/VJVar.h"
#include "MGEffect.h"
#include "MGEffects.h"
#include "MGPanelVars.h"
#include "MGSource.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGEffect::CMGEffect(CMGControl* pParent, CMGEffects* pSourceEffects, CMGSource* pSource, int x, int y, int iSlot)
	: CMGControl(pParent, x, y, 58, 34)	// magic fuckin numbers
	, m_iEffectSlot(iSlot)
	, m_uEffectID(INVALID_ID)
	, m_sEffectName()
	, m_sEffectDisplayName()
	, m_pCBEnabled(nullptr)
	, m_pSource(pSource)
	, m_pSourceEffects(pSourceEffects)
	, m_pVJEffectVars(nullptr)
{
	m_iAlign = MGALIGN_NONE;
	m_pCBEnabled = NEW CMGCheckBox(this, 2, 15, "", true, true, bind(&CMGEffect::OnClickEnable, this), NULL);
	ASSERT(m_pCBEnabled);
	m_pCBEnabled->SetVisible(false);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGEffect::~CMGEffect() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGEffect\n"));
#endif
	m_pSourceEffects = nullptr;
	m_pSource = nullptr;
	// ebp-> CMGEffects set the 'Active' filter vars' parent to NULL already, 
	// therefore this VJEffectVars should be unattached from its parent.
	//ASSERT(!m_pVJEffectVars->GetParent());
	DELETE_PTR(m_pVJEffectVars);
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CMGEffect::Load(std::string const& sEffect) -> bool {
	// Unload previous
	Unload();

	// ebp-> in order to get the display name from <filtername> (if it exists),
	// SourceAddEffect below will write the name into the passed string parameter.
	m_sEffectDisplayName = sEffect;

	// Try to load
	m_uEffectID = g_pAppMain->VJController()->SourceAddEffect(m_pSource->GetID(), m_iEffectSlot, m_sEffectDisplayName);
	if (m_uEffectID != INVALID_ID) {
		m_sEffectName = sEffect;
		m_pVJEffectVars = NEW CMGPanelVars(nullptr, m_pSource->GetID(), m_uEffectID);
		m_pCBEnabled->SetVisible(true);
		m_pCBEnabled->SetChecked(true);
#ifdef _DEBUG
#ifdef VERBOSE_MEM_LOG
		std::string sIndent;
		PrintHierarchy(sIndent);
#endif
#endif
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Unload
//---------------------------------------------------------------------------//
auto CMGEffect::Unload() -> void {
	if (IsLoaded()) {
		if (m_pSourceEffects->GetEffectVars() == m_pVJEffectVars) {
			m_pSourceEffects->ShowEffectVars(nullptr);
		}
		g_pAppMain->VJController()->SourceRemoveEffect(m_pSource->GetID(), m_iEffectSlot);
		DELETE_PTR(m_pVJEffectVars);
		m_sEffectName.clear();
		m_sEffectDisplayName.clear();
		m_uEffectID = INVALID_ID;
		m_pCBEnabled->SetVisible(false);
	}
}


//---------------------------------------------------------------------------//
//	SwapWith
//---------------------------------------------------------------------------//
auto CMGEffect::SwapWith(CMGEffect* pOther) -> void {
	ASSERT(m_pSourceEffects == pOther->m_pSourceEffects);
	ASSERT(m_pSource == pOther->m_pSource);

	// swap parents of the checkboxes so their positions change
	// and then swap the pointers.  they should retain whatever
	// state they had before the swap (checked & visible).
	pOther->m_pCBEnabled->SetParent(this);
	m_pCBEnabled->SetParent(pOther);
	m_pCBEnabled->SwapBindings(pOther->m_pCBEnabled);
	Swap(m_pCBEnabled, pOther->m_pCBEnabled);

	m_pSourceEffects->SwapEffects(m_iEffectSlot, pOther->m_iEffectSlot);
	g_pAppMain->VJController()->SourceSwapEffect(m_pSource->GetID(), m_iEffectSlot, pOther->m_iEffectSlot);
	Swap(m_sEffectName, pOther->m_sEffectName);
	Swap(m_sEffectDisplayName, pOther->m_sEffectDisplayName);
	Swap(m_pVJEffectVars, pOther->m_pVJEffectVars);
	Swap(m_uEffectID, pOther->m_uEffectID);

	m_pSourceEffects->Repaint();
}


//---------------------------------------------------------------------------//
//	Show
//---------------------------------------------------------------------------//
auto CMGEffect::Show() -> void {
	if (IsLoaded()) {
		m_pSourceEffects->ShowEffectVars(m_pVJEffectVars);
	}
}


//---------------------------------------------------------------------------//
//	OnClickEnable
//---------------------------------------------------------------------------//
auto CMGEffect::OnClickEnable() -> void {
	// if effect is checked, bypass is off, so we invert the value passed to bypass the effect.
	g_pAppMain->VJController()->SourceBypassEffect(m_pSource->GetID(), m_iEffectSlot, !m_pCBEnabled->Checked());
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGEffect::OnMouseMove(int iX, int iY) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGEffect::OnMouseDown(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT) {
		if (IsLoaded()) {
			Show();
			BeginDrag();
			m_pSourceEffects->Repaint();
		}
		Focus();
	} else if (iButton == MOUSE_RIGHT) {
		Focus();
		if (IsLoaded()) {
			Unload();
			m_pSourceEffects->Repaint();
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGEffect::OnMouseUp(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
}


//---------------------------------------------------------------------------//
//	OnDragOver
//---------------------------------------------------------------------------//
auto CMGEffect::OnDragOver(CMGControl* pSource, int iX, int iY, bool& rAccept) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (pSource == m_pSource->GetEffects()->GetLBEffects() ||
		pSource == m_pSource->GetEffects()->GetEffect(0) ||
		pSource == m_pSource->GetEffects()->GetEffect(1) ||
		pSource == m_pSource->GetEffects()->GetEffect(2) ||
		pSource == m_pSource->GetEffects()->GetEffect(3) ||
		pSource == m_pSource->GetEffects()->GetEffect(4)) {
		rAccept = (pSource != this);
	}
}


//---------------------------------------------------------------------------//
//	OnDragDrop
//---------------------------------------------------------------------------//
auto CMGEffect::OnDragDrop(CMGControl* pSource, int iX, int iY) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (pSource == m_pSource->GetEffects()->GetLBEffects()) {
		std::string sEffect;
		if (m_pSource->GetEffects()->GetLBEffects()->GetSelected(sEffect)) {
			if (Load(sEffect)) {
				Show();
			}
		}
	} else {
		SwapWith((CMGEffect*)pSource);
		Show();
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGEffect::DerivDraw() -> void {
	TRect rect(Left(), Top(), Width(), Height());

	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	if (m_pVJEffectVars && m_pVJEffectVars->GetParent()) {
		neon::gui::Rect(pSkin, rect, COLOR_EDIT_BORDER_ACTIVE);
	} else {
		neon::gui::Rect(pSkin, rect, COLOR_EDIT_BORDER);
	}

	ShrinkRect(rect, 1);

	neon::gui::SetClipRect(rect);

	if (IsLoaded()) {
		if (m_pVJEffectVars && m_pVJEffectVars->GetParent()) {
			neon::gui::FillRect(pSkin, rect, COLOR_EDIT_BG_ACTIVE);
		} else {
			neon::gui::FillRect(pSkin, rect, COLOR_EDIT_BG);
		}
		neon::gui::Text(pSkin, FONT_NORMAL, rect.x + 2, rect.y - 1, 0, 20, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT_ACTIVE, m_sEffectDisplayName);
	} else {
		neon::gui::FillRect(pSkin, rect, COLOR_TAB_BG);
	}

	neon::gui::SetClipRect();
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGEffect::Update() -> void {
	if (IsLoaded()) {
		m_pVJEffectVars->Update();
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CMGEffect::Load(CNodeFile::CNode* pNode) -> void {
	if (IsLoaded()) {
		m_pVJEffectVars->Load(pNode);
		// read the bypass value
		pNode = pNode->FirstNode("bypass");
		if (pNode != nullptr) {
			m_pCBEnabled->SetChecked(!pNode->AsBool("value"));
		}
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGEffect::Save(CNodeFile* pFile) -> void {
	if (IsLoaded()) {
		m_pVJEffectVars->Save(pFile);
		// write the bypass value
		pFile->WriteOpenNode("bypass", "");
		pFile->WriteNode("value", "", !m_pCBEnabled->Checked());
		pFile->WriteCloseNode();
	}
}
