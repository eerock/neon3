//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGBEATMANAGER_H
#define NEON_MGBEATMANAGER_H

#include "Controls/MGControl.h"

class CMGLabel;
class CMGCheckBox;
class CMGPanelFFT;
class CMGTrackBar;
class CMGButton;

//---------------------------------------------------------------------------//
// CBeatTapper
//
//---------------------------------------------------------------------------//
class CBeatTapper {
public:
	CBeatTapper()
		: m_bBeatTapped(false)
		, m_dPrevTapTime(0.0)
		, m_bpmHistory()
		, m_bpmHistory2ndOrder()
	{}

	auto Update(double dTime) -> bool;
	auto GetBpm() const -> double { return m_dBPM; }
	auto GetBpmMean() const -> double { return m_bpmHistory.get(); }
	auto GetBpmMean2ndOrder() const -> double { return m_bpmHistory2ndOrder.get(); }

private:
	bool m_bBeatTapped;
	double m_dPrevTapTime;
	double m_dBPM;

	CRollingMean<double, 4> m_bpmHistory;
	CRollingMean<double, 4> m_bpmHistory2ndOrder;
};


//---------------------------------------------------------------------------//
// CMGBeatManager
//
//---------------------------------------------------------------------------//
class CMGBeatManager : public CMGControl {
public:
	CMGBeatManager(CMGControl* pParent);
	~CMGBeatManager() override;

	auto IsBeat(int iBeat, int iOff) -> bool;
	auto SetMasterBpm(float fBpm) -> void;
	auto GetMasterBpm() const -> float { return m_fMasterBpm; }
	auto GetMasterBpmInv() const -> float { return m_fMasterBpmInv; }

	auto Update() -> void;

	auto Load(CNodeFile::CNode* pNode) -> void;
	auto Save(CNodeFile* pFile) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGBeatManager\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto OnBtnUpBpm() -> void;
	auto OnBtnDnBpm() -> void;
	auto OnBtnFwdBpm() -> void;
	auto OnBtnRewBpm() -> void;
	auto OnBtnResetBpm() -> void;
	auto OnBtnLinkGain() -> void;
	auto OnChangeGain() -> void;
	auto OnChangeEnableFFT() -> void;
	auto OnCloseLinkGain() -> void;

private:
	CTimer m_tTimer;
	double m_dTime;
	double m_dTimeBeat;
	int m_iMasterBeat;
	float m_fMasterBpm;
	float m_fMasterBpmInv;
	bool m_bBeatKilled;
	bool m_bIsBeat;
	int m_iBeatNum;
	CBeatTapper m_tBeatTapper;
	CMGLabel* m_pLabelBpm;
	CMGCheckBox* m_pCBUpdateFFT;	// refactor: these don't belong here
	CMGPanelFFT* m_pPanelFFT;		// it couple's bpm and audio signals.
	CMGTrackBar* m_pGainSlider;
	CMGButton* m_pBtnLinkGain;
	CLink* m_pLinkGain;
};

#endif//NEON_MGBEATMANAGER_H
