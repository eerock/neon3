//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "Launcher.h"
#include "NeonMain.h"
#include "AppOptions.h"
#include "LuaPlayer.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <stdlib.h>
#include <ShellAPI.h>
#include <iostream>
#include <winbase.h>

//#define LOOK_FOR_MEM_LEAKS
#ifdef LOOK_FOR_MEM_LEAKS
#define CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif//LOOK_FOR_MEM_LEAKS

#endif//PLATFORM

// ebp-> have to push/pop compiler warning disable because boost generates them
#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(push)
#pragma warning(disable:4100 4512)
#endif//PLATFORM_WINDOWS
#include <boost/program_options.hpp>
#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(pop)
#endif//PLATFORM

namespace popts = boost::program_options;



static bool s_bEnd = false;
void PeticionEndApp() {
	s_bEnd = true;	// put a breakpoint here to catch when the app ends!
}


//---------------------------------------------------------------------------//
//	errorMessage
//	Shows a dialog box, or if not availble or requested, logs normally.
//---------------------------------------------------------------------------//
void errorMessage(std::string const& errmsg, bool dialog = true) {
#if (PLATFORM == PLATFORM_WINDOWS)
	if (dialog) {
		MessageBox(NULL, errmsg.c_str(), "Fatal Error: " VERSION_NAME, MB_OK | MB_ICONERROR);
	} else
#endif
	{ GLOG(("%s\n", errmsg.c_str())); }
}


//---------------------------------------------------------------------------//
//	RunMainApp
//	Runs Neon in the default application mode
//---------------------------------------------------------------------------//
auto RunMainApp(THInst hInstance) -> void {
	// Run the Main Neon Application...
	std::unique_ptr<CMGAppMain> pAppMain(NEW CMGAppMain);
	if (pAppMain->Init(hInstance)) {
		while (pAppMain->Update()) {}
	} else {
		std::string const errmsg = "ERROR: Can't load " VERSION_LONG ", AppMain failed to init.\n";
		errorMessage(errmsg);
	}
}


//---------------------------------------------------------------------------//
//	RunLuaPlayer
//	Runs Neon as a Player application, reading a lua script for commands
//---------------------------------------------------------------------------//
auto RunLuaPlayer(THInst hInstance) -> void {
	// Launch the LuaPlayer...
	std::unique_ptr<CLuaPlayer> pLuaPlayer(NEW CLuaPlayer);
	if (pLuaPlayer->Init(hInstance)) {
		while (pLuaPlayer->Update() && !s_bEnd) {
			pLuaPlayer->Draw();
		}
	} else {
		std::string const errmsg = "ERROR: Can't load " VERSION_LONG ", Lua Player failed to init.\n";
		errorMessage(errmsg);
	}
}


//---------------------------------------------------------------------------//
//	WinMain
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto WINAPI WinMain(THInst hInstance, THInst hPrevInstance, LPSTR lpCmdLine, INT nShowCmd) -> int {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(nShowCmd);
	if (FAILED(CoInitialize(nullptr))) {
		return -1;
	}
#else
int main(int argc, char* argv[]) {
#endif//PLATFORM

	// ebp-> use boost::program_options to deal with command-line...
	popts::variables_map varmap;
	popts::options_description desc("\nNeon Command-line Options");
	desc.add_options()
		("help", "Show help message")
		("version", "Show version")
		("launcher", "Show launcher dialog at startup")
		("player", "Launch the Player")
		// ebp-> todo: add more command-line options
		;

	// ebp-> parse the command-lines differently...
#if (PLATFORM == PLATFORM_WINDOWS)
	auto args = popts::split_winmain(lpCmdLine);
	popts::store(popts::command_line_parser(args).options(desc).run(), varmap);
#else
	popts::store(popts::parse_command_line(argc, argv, desc), varmap);
#endif//PLATFORM

	popts::notify(varmap);

	if (varmap.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	if (varmap.count("version")) {
		std::cout << VERSION_LONG << std::endl;
		return 1;
	}

	bool bLauncher = false;
	if (varmap.count("launcher")) {
		bLauncher = true;
	}

	bool bPlayer = false;
	if (varmap.count("player")) {
		bPlayer = true;
	}

	bool bMainApp = true;

	// seed RNG...
	srand(static_cast<uint>(time(nullptr)));

	// Load application options from config file...
	auto pAppOpts = NEW CAppOptions;
	if (!pAppOpts->Load("neonconfig.xml")) {
		bLauncher = true;
	}

	CServiceLocator<IAppOptionsService>::RegisterService(pAppOpts);

	// The log filename...
	std::string const sLogFile = VERSION_NAME "_" VERSION_SHORT ".log";

	GLOGSTART(LOGCONSOLE, sLogFile);
	MEMORY_MANAGER_INIT();

	if (bLauncher) {
		auto pLauncher = std::make_unique<CLauncher>();
		if (pLauncher->Show(hInstance)) {
			bPlayer |= pLauncher->LaunchPlayer();
		} else {
			bMainApp = false;
		}
	}

	if (bPlayer) {
		RunLuaPlayer(hInstance);
	} else if (bMainApp) {
		RunMainApp(hInstance);
	}

	MEMORY_MANAGER_END();
	GLOGEND();

	// ebp-> save the config options after we've run the application...
	CServiceLocator<IAppOptionsService>::GetService()->Save();
	CServiceLocator<IAppOptionsService>::RegisterService(nullptr);

	// ebp-> if you want to use this, include crtdbg.h at top
#ifdef LOOK_FOR_MEM_LEAKS
	_CrtDumpMemoryLeaks();
#endif//PLATFORM

#if (PLATFORM == PLATFORM_WINDOWS)
	CoUninitialize();
#endif//PLATFORM

	return 0;
}
