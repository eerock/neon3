//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by launcher.rc
//
#define IDB_BITMAP1                     101
#define IDB_NEON_TITLE                  101
#define IDD_DIALOG1                     102
#define IDD_LAUNCHER_DIALOG             102
#define IDI_ICON1                       106
#define IDC_COMBO_DEVICE                1001
#define IDC_LABEL_DEVICE                1002
#define IDC_CUSTOM_RESOLUTION           1005
#define IDC_COMBO_RESOLUTION            1006
#define IDC_CUSTOM_RES_W                1007
#define IDC_CUSTOM_RES_H                1008
#define IDC_GO_FULLSCREEN               1009
#define IDC_SHOW_FPS                    1010
#define IDC_CHECK4                      1011
#define IDC_NEON_PLAYER                 1011
#define IDC_STATIC_ICON                 1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
