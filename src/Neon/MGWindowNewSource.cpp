//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "MGWindowNewSource.h"
#include "Controls/MGButton.h"
#include "Controls/MGButtonModalResult.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGComboBox.h"
#include "Controls/MGEditBox.h"
#include "Controls/MGGroupBox.h"
#include "Controls/MGLabel.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGWindowNewSource::CMGWindowNewSource()
	: CMGWindow(300, 250, "New Source")
{
	m_pButtonOk = NEW CMGButtonModalResult(this, 140, 220, CMGButton::NORMAL, "Ok", MR_OK);
	m_pButtonCancel = NEW CMGButtonModalResult(this, 220, 220, CMGButton::NORMAL, "Cancel", MR_CANCEL);

	NEW CMGLabel(this, 10, 34, MGALIGN_NONE, FONT_NORMAL, "Class");
	m_pComboBox = NEW CMGComboBox(this, 40, 30, 200, 20, false, NULL);
	m_pComboBox->Add("MpgVideo");
	m_pComboBox->Add("Color");
	m_pComboBox->Add("3DScene");
	m_pComboBox->Add("Flare");
	m_pComboBox->Add("Image");
	m_pComboBox->Add("ImageList");
	m_pComboBox->Add("Message");
	m_pComboBox->Add("Screen");
	m_pComboBox->Select(0);

	NEW CMGLabel(this, 10, 64, MGALIGN_NONE, FONT_NORMAL, "File");
	auto pGroupBox = NEW CMGGroupBox(this, 40, 60, 200, 40);
	m_pButtonBrowse = NEW CMGButton(pGroupBox, 60, 0, CMGButton::NORMAL, "Browse", bind(&CMGWindowNewSource::OnClickBrowseButton, this));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGWindowNewSource::~CMGWindowNewSource() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGWindowNewSource\n"));
#endif
}


//---------------------------------------------------------------------------//
//	OnClickBrowseButton
//---------------------------------------------------------------------------//
auto CMGWindowNewSource::OnClickBrowseButton() -> void {
	std::string sFile;
	auto bOpen = g_pMGApp->OpenFileOpen("Locate Source Media", neon::sDirSources, nullptr, sFile);
	if (bOpen) {
		GLOG(("You selected the file: %s\n", sFile.c_str()));
	}
}
