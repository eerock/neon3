//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGWINDOWVARLINKER_H
#define NEON_MGWINDOWVARLINKER_H

#include "Controls/MGControl.h"

class CMGButton;
class CMGListBox;
class CMGComboBox;
class CMGCheckBox;
class CMGHotKey;
class CMGSpinBox;

class CMGWindowVarLinker : public CMGWindow {
public:
	CMGWindowVarLinker();
	~CMGWindowVarLinker() override;

	virtual auto Show(CLink** ppToLink, int iX, int iY, int iBehType, 
		bool bOnlyMidiKey, CMGControl* pRControl, CMGButton* pLinkButton, function<void(EModalResult)> fnOnModalResult) -> void;
	virtual auto Show(CLink** ppToLink, int iX, int iY, int iBehType, 
		CMGButton* pLinkButton, function<void(CMGWindow::EModalResult)> fnOnModalResult) -> void;
	virtual auto Close(EModalResult eModalResult) -> void;

private:
	auto Reset() -> void;
	auto LoadLink(CLink* pLink) -> void;
	auto CreateLink() -> CLink*;

	auto LoadPresets() -> void;
	auto OnSavePreset() -> void;
	auto LoadPreset(std::string const& sPreset) -> void;

	auto CreateDeviceCtrls() -> void;
	auto CreateActionCtrls() -> void;
	auto SetDevice(CMGGroupBox* pDevice) -> void;
	auto SetAction(CMGGroupBox* pAction) -> void;
	auto ResetDeviceData() -> void;
	auto ResetActionData() -> void;

	// Callbacks
	auto OnClickAccept() -> void;
	auto OnClickCancel() -> void;
	auto OnClickUnlink() -> void;
	auto OnChangeDevice() -> void;
	auto OnChangePreset() -> void;
	auto OnDeletePreset() -> void;
	auto GetPresetExt() const -> std::string const&;

	// CheckBox
	auto OnChange_CHA_Switch() -> void;
	auto OnChange_CHA_DevVal() -> void;
	auto OnChange_CHA_InvDev() -> void;

	// ComboBox
	auto OnChange_CBA_Next() -> void;
	auto OnChange_CBA_Prev() -> void;
	auto OnChange_CBA_Rand() -> void;
	auto OnChange_CBA_Set() -> void;

	// TrackBar
	auto OnChange_TBA_DevVal() -> void;
	auto OnChange_TBA_InvDev() -> void;
	auto OnChange_TBA_Set() -> void;
	auto OnChange_TBA_Add() -> void;
	auto OnChange_TBA_Rand() -> void;
	auto OnChange_TBA_Fade() -> void;
	auto OnChange_TBA_SwapFade() -> void;

	// Button
	auto OnChange_BTNA_Activate() -> void;

private:
	int m_iBehType;
	CLink** m_ppToLink;

	// Related to
	CMGControl* m_pLinkedControl;	// the MGControl linked to
	CMGButton* m_pLinkButton;		// the Link Button that got us here

	// Controls
	CMGListBox* m_pLBDevices;
	CMGGroupBox* m_pCurrentDevice;

	// Beat Device
	CMGGroupBox* m_pBD_Panel;
	CMGSpinBox* m_pBD_Beat;
	CMGSpinBox* m_pBD_Offset;

	// Keyboard Device
	CMGGroupBox* m_pKD_Panel;
	CMGHotKey* m_pKD_HotKey;

	// Midi Note Device
	CMGGroupBox* m_pMND_Panel;
	CMGHotKey* m_pMND_HotKey;

	// Midi Slider Device
	CMGGroupBox* m_pMSD_Panel;
	CMGHotKey* m_pMSD_HotKey;

	// Wave Device
	CMGGroupBox* m_pWD_Panel;
	CMGComboBox* m_pWD_Chan;
	CMGSpinBox* m_pWD_Gain;

	// Presets
	CMGComboBox* m_pCBPresets;
	CMGButton* m_pBtnSavePreset;
	CMGButton* m_pBtnDelPreset;

	// Current Action
	CMGGroupBox* m_pCurrentAction;

	// CheckBox Action
	CMGGroupBox* m_pPanelActionsCheckBox;
	CMGCheckBox* m_pCHA_Switch;
	CMGCheckBox* m_pCHA_DevVal;
	CMGCheckBox* m_pCHA_InvDev;

	// ComboBox Action
	CMGGroupBox* m_pPanelActionsComboBox;
	CMGCheckBox* m_pCBA_Next;
	CMGCheckBox* m_pCBA_Prev;
	CMGCheckBox* m_pCBA_Rand;
	CMGCheckBox* m_pCBA_Set;
	CMGSpinBox* m_pCBA_SetValue;

	// TrackBar Action (button + slider)
	CMGGroupBox* m_pPanelActionsTrackBarA;
	CMGGroupBox* m_pPanelActionsTrackBarF;
	CMGCheckBox* m_pTBA_DevVal;
	CMGCheckBox* m_pTBA_InvDev;
	CMGCheckBox* m_pTBA_Set;
	CMGCheckBox* m_pTBA_Add;
	CMGCheckBox* m_pTBA_Rand;
	CMGCheckBox* m_pTBA_Fade;
	CMGSpinBox* m_pTBA_SetValue;
	CMGSpinBox* m_pTBA_AddValue;
	CMGSpinBox* m_pTBA_FadeFr;
	CMGSpinBox* m_pTBA_FadeTo;
	CMGSpinBox* m_pTBA_FadeLength;

	// Button Action
	CMGGroupBox* m_pPanelActionsButton;
	CMGCheckBox* m_pBTNA_Activate;
};

#endif//NEON_MGWINDOWVARLINKER_H
