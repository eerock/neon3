//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "VJVar/VJVar.h"
#include "MGPanelVars.h"
#include "CtrlVar.h"
#include "Controls/MGScrollBar.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGPanelVars::CMGPanelVars(CMGControl* pParent, uint uSourceID, int iScope, int iObject)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_vpVJVars()
	, m_pScrollBar(nullptr)
{
	m_iAlign = MGALIGN_CLIENT;

	auto pVars = g_pAppMain->VJController()->SourceGetVarCtrls(uSourceID, iScope);

	// ebp-> new code
	auto pSource = g_pAppMain->VJController()->GetSource(uSourceID)->GetSource();
	ASSERT(pSource);

	// If vars, create them
	if (pVars) {
		int iVar = 0;
		int ypos = 20;
		while (pVars[iVar].iType != TCtrlVar::INVALID) {
			// ebp-> new code
			auto pVar = CVJVar::Create(this, &pVars[iVar], pSource, ypos, uSourceID, iVar, iScope, iObject);
			if (pVar) {
				m_vpVJVars.push_back(pVar);
				ypos += 22;
			}
			++iVar;
		}
	}
}



//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGPanelVars::CMGPanelVars(CMGControl* pParent, uint uSourceID, uint uEffectID)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_vpVJVars()
	, m_pScrollBar(nullptr)
{
	m_iAlign = MGALIGN_CLIENT;

	auto pVars = g_pAppMain->VJController()->SourceGetEffectVarCtrls(uSourceID, uEffectID);

	// ebp-> new code
	auto pEffect = g_pAppMain->VJController()->GetSource(uSourceID)->GetEffectByID(uEffectID);
	ASSERT(pEffect);

	// If vars, create them
	if (pVars) {
		int iVar = 0;
		int ypos = 20;
		while (pVars[iVar].iType != TCtrlVar::INVALID) {
			// ebp-> new code
			auto pVar = CVJVar::Create(this, &pVars[iVar], pEffect, ypos, uSourceID, iVar, uEffectID);
			if (pVar) {
				m_vpVJVars.push_back(pVar);
				ypos += 22;
			}
			++iVar;
		}
	}

	//m_pScrollBar = NEW CMGScrollBar(this, true, bind(&CMGPanelVars::OnChangeScroll, this));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGPanelVars::~CMGPanelVars() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGPanelVars\n"));
#endif
	for (size_t i = 0; i < m_vpVJVars.size(); ++i) {
		DELETE_PTR(m_vpVJVars[i]);
	}
	m_vpVJVars.clear();
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGPanelVars::Update() -> void {
	for (size_t i = 0; i < m_vpVJVars.size(); ++i) {
		m_vpVJVars[i]->Evaluate();
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CMGPanelVars::Load(CNodeFile::CNode* pNode) -> void {
	for (size_t i = 0; i < m_vpVJVars.size(); ++i) {
		m_vpVJVars[i]->Load(pNode);
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGPanelVars::Save(CNodeFile* pFile) -> void {
	for (size_t i = 0; i < m_vpVJVars.size(); ++i) {
		m_vpVJVars[i]->Save(pFile);
	}
}
