//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "MGGridPanel.h"
#include "NeonMain.h"
#include "MGSource.h"
#include "Controls/MGGroupBox.h"
#include "Controls/MGPageControl.h"
#include "Controls/MGTreeView.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGGridPanel::CMGGridPanel(CMGControl* pParent, int numPages)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_pPCGrid(nullptr)
	, m_pGridMaster(nullptr)
	, m_pGridSource(nullptr)
	, m_vGridSourcePages(numPages)
{
	m_iAlign = MGALIGN_CLIENT;

	// ebp-> use the remaining rect to calculate sizes for this stuff.
	// pParent size is 1024 x 768
	// remainder should be 718 x 738
	TRect remainder;
	pParent->GetFreeRect(remainder);

#define MASTER_ROW_HEIGHT	(88)
	// box around the output grid...
	auto pGB = NEW CMGGroupBox(this, 0, 0, remainder.w, MASTER_ROW_HEIGHT, MGALIGN_NONE, COLOR_BASE_BORDER_ACTIVE);

	// the master output row (10x1 grid)
	m_pGridMaster = NEW CMGSourceGrid(pGB, NUM_MASTER_LAYERS, 1, 1.2f, 2, 2, remainder.w - 4, MASTER_ROW_HEIGHT - 4);
	//m_pGridMaster = NEW CMGSourceGrid(this, NUM_MASTER_LAYERS, 1, 1.14f, 0, 0, 711, 80);

	for (int i = 0; i < NUM_MASTER_LAYERS; ++i) {
		m_pGridMaster->GetCellData(i, 0)->Init(true);
	}

	// the 10 tabs/pages below...
	m_pPCGrid = NEW CMGPageControl(this, 0, MASTER_ROW_HEIGHT, remainder.w, remainder.h - MASTER_ROW_HEIGHT, MGALIGN_NONE, bind(&CMGGridPanel::OnSelectGridPage, this));

	for (int i = 0; i < NUM_GRID_PAGES; ++i) {
		std::stringstream ss;
		ss << "Page " << i + 1;
		auto pPage = NEW CMGPage(m_pPCGrid, ss.str());

		m_vGridSourcePages[i] = NEW CMGSourceGrid(pPage, GRID_PAGE_COLS, GRID_PAGE_ROWS, 1.14f, 0, 0, remainder.w - 4, remainder.h - MASTER_ROW_HEIGHT - 22 - 4);

		// Assign callbacks
		m_vGridSourcePages[i]->m_fnOnDragOver = bind(&CMGGridPanel::OnGridDragOver, this, _1, _2, _3, _4, _5);
		m_vGridSourcePages[i]->m_fnOnDragDrop = bind(&CMGGridPanel::OnGridDragDrop, this, _1, _2, _3, _4);
		m_vGridSourcePages[i]->m_fnOnMouseMove = bind(&CMGGridPanel::OnGridMouseMove, this, _1, _2, _3);
		m_vGridSourcePages[i]->m_fnOnMouseDown = bind(&CMGGridPanel::OnGridMouseDown, this, _1, _2, _3, _4);
		m_vGridSourcePages[i]->m_fnOnMouseUp = bind(&CMGGridPanel::OnGridMouseUp, this, _1, _2, _3, _4);
		m_vGridSourcePages[i]->m_fnOnMouseDoubleClk = bind(&CMGGridPanel::OnGridMouseDoubleClk, this, _1, _2, _3, _4);
		m_vGridSourcePages[i]->m_fnOnKeyDown = bind(&CMGGridPanel::OnGridKeyDown, this, _1, _2);
		m_vGridSourcePages[i]->m_fnOnKeyUp = bind(&CMGGridPanel::OnGridKeyUp, this, _1, _2);
	}

	SelectGridPage(0);

	// Assign callbacks
	m_pGridMaster->m_fnOnDragOver = bind(&CMGGridPanel::OnGridDragOver, this, _1, _2, _3, _4, _5);
	m_pGridMaster->m_fnOnDragDrop = bind(&CMGGridPanel::OnGridDragDrop, this, _1, _2, _3, _4);
	m_pGridMaster->m_fnOnMouseMove = bind(&CMGGridPanel::OnGridMouseMove, this, _1, _2, _3);
	m_pGridMaster->m_fnOnMouseDown = bind(&CMGGridPanel::OnGridMouseDown, this, _1, _2, _3, _4);
	m_pGridMaster->m_fnOnMouseUp = bind(&CMGGridPanel::OnGridMouseUp, this, _1, _2, _3, _4);
	m_pGridMaster->m_fnOnMouseDoubleClk = bind(&CMGGridPanel::OnGridMouseDoubleClk, this, _1, _2, _3, _4);
	m_pGridMaster->m_fnOnKeyDown = bind(&CMGGridPanel::OnGridKeyDown, this, _1, _2);
	m_pGridMaster->m_fnOnKeyUp = bind(&CMGGridPanel::OnGridKeyUp, this, _1, _2);

	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGGridPanel::~CMGGridPanel() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGGridPanel\n"));
#endif
	ClearMaster();
	m_pGridSource = nullptr;
	// ebp-> everything else should be taken care of by
	// the MGui parent-child relationships.

	// 2014 - what about the vector of grid pages?  check to make sure they are
	// being deleted.  Look for CMGSourceGrid.
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
auto CMGGridPanel::Clear() -> void {
	for (int i = 0; i < NUM_GRID_PAGES; ++i) {
		int cols = m_vGridSourcePages[i]->GetCols();
		int rows = m_vGridSourcePages[i]->GetRows();
		for (int col = 0; col < cols; ++col) {
			for (int row = 0; row < rows; ++row) {
				UnloadSource(m_vGridSourcePages[i]->GetCellData(col, row)->GetSource());
				m_vGridSourcePages[i]->GetCellData(col, row)->SetSource(nullptr);
			}
		}
		m_vGridSourcePages[i]->Clear();
	}
}


//---------------------------------------------------------------------------//
//	ClearMaster
//---------------------------------------------------------------------------//
auto CMGGridPanel::ClearMaster() -> void {
	for (int i = 0; i < NUM_MASTER_LAYERS; ++i) {
		if (m_pGridMaster->GetCellData(i, 0)->GetSource() != nullptr) {
			m_pGridMaster->GetCellData(i, 0)->GetSource()->Unattach();
			m_pGridMaster->GetCellData(i, 0)->SetSource(nullptr);
		}
	}
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGGridPanel::Update() -> void {
	for (int i = 0; i < NUM_GRID_PAGES; ++i) {
		int iCells = m_vGridSourcePages[i]->GetCols() * m_vGridSourcePages[i]->GetRows();
		auto ppCells = m_vGridSourcePages[i]->GetCells();
		for (int c = 0; c < iCells; ++c) {
			ppCells[c]->Update();
		}
	}
}


//---------------------------------------------------------------------------//
//	GetGridPage
//---------------------------------------------------------------------------//
auto CMGGridPanel::GetGridPage(int iGrid) const -> CMGSourceGrid* {
	ASSERT(ValidIndex(iGrid, static_cast<int>(m_vGridSourcePages.size())));
	return m_vGridSourcePages[iGrid];
}



//---------------------------------------------------------------------------//
//	SelectGridPage
//---------------------------------------------------------------------------//
auto CMGGridPanel::SelectGridPage(int iGrid) -> void {
	if (m_pGridSource != nullptr) {
		m_pGridSource->SelectCell();
	}

	ASSERT(ValidIndex(iGrid, NUM_GRID_PAGES));
	m_pGridSource = m_vGridSourcePages[iGrid];
	m_pPCGrid->SetActivePage(iGrid);

	if (m_pGridSource != nullptr) {
		m_pGridSource->SelectCell();
	}
}


//---------------------------------------------------------------------------//
//	NextClip
//---------------------------------------------------------------------------//
auto CMGGridPanel::NextClip(int iLayer) -> void {
	ASSERT(m_pGridMaster);
	ASSERT(m_pGridSource);
	ASSERT(ValidIndex(iLayer, NUM_MASTER_LAYERS));
	if (ValidIndex(iLayer, NUM_MASTER_LAYERS)) {
		// no source attached yet in master grid...
		if (!m_pGridMaster->GetCellData(iLayer, 0)->GetSource()) {
			// attach the first one we find, looking in a forwards direction...
			for (int row = 0; row < GRID_PAGE_ROWS; ++row) {
				auto pMGSource = m_pGridSource->GetCellData(iLayer, row)->GetSource();
				if (pMGSource) {
					pMGSource->Attach(iLayer);
					m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGSource);
					m_pGridSource->SelectCell(iLayer, row);
					break;
				}
			}
			return;
		}

		// there's an source in the master output layer, so we find the 'next' source,
		// unattach the output and attach the next one found...
		for (int row = 0; row < GRID_PAGE_ROWS; ++row) {
			auto pMGCurSource = m_pGridSource->GetCellData(iLayer, row)->GetSource();
			if (pMGCurSource && pMGCurSource->IsAttached()) {
				// then the next cell with data will be attached...
				for (int r = row + 1; r < GRID_PAGE_ROWS; ++r) {
					auto pMGNextSource = m_pGridSource->GetCellData(iLayer, r)->GetSource();
					if (pMGNextSource) {
						pMGCurSource->Unattach();
						pMGNextSource->Attach(iLayer);
						m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGNextSource);
						m_pGridSource->SelectCell(iLayer, r);
						return;
					}
				}

				for (int r = 0; r <= row; ++r) {
					auto pMGNextSource = m_pGridSource->GetCellData(iLayer, r)->GetSource();
					if (pMGNextSource) {
						pMGCurSource->Unattach();
						pMGNextSource->Attach(iLayer);
						m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGNextSource);
						m_pGridSource->SelectCell(iLayer, r);
						return;
					}
				}
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	PrevClip
//---------------------------------------------------------------------------//
auto CMGGridPanel::PrevClip(int iLayer) -> void {
	ASSERT(m_pGridMaster);
	ASSERT(m_pGridSource);
	ASSERT(ValidIndex(iLayer, NUM_MASTER_LAYERS));
	if (ValidIndex(iLayer, NUM_MASTER_LAYERS)) {
		// no source attached yet in master grid...
		if (!m_pGridMaster->GetCellData(iLayer, 0)->GetSource()) {
			// attach the first one we find, looking in a backwards direction...
			for (int row = GRID_PAGE_ROWS - 1; 0 <= row; --row) {
				auto pMGSource = m_pGridSource->GetCellData(iLayer, row)->GetSource();
				if (pMGSource) {
					pMGSource->Attach(iLayer);
					m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGSource);
					m_pGridSource->SelectCell(iLayer, row);
					break;
				}
			}
			return;
		}

		// there's an source in the master output layer, so we find the 'previous' source,
		// unattach the output and attach the previous one found...
		for (int row = 0; row < GRID_PAGE_ROWS; ++row) {
			auto pMGCurSource = m_pGridSource->GetCellData(iLayer, row)->GetSource();
			if (pMGCurSource && pMGCurSource->IsAttached()) {
				// then the 'previous' cell with data will be attached...
				for (int r = row - 1; 0 <= r; --r) {
					auto pMGPrevSource = m_pGridSource->GetCellData(iLayer, r)->GetSource();
					if (pMGPrevSource) {
						pMGCurSource->Unattach();
						pMGPrevSource->Attach(iLayer);
						m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGPrevSource);
						m_pGridSource->SelectCell(iLayer, r);
						return;
					}
				}

				for (int r = GRID_PAGE_ROWS - 1; row <= r; --r) {
					auto pMGPrevSource = m_pGridSource->GetCellData(iLayer, r)->GetSource();
					if (pMGPrevSource) {
						pMGCurSource->Unattach();
						pMGPrevSource->Attach(iLayer);
						m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGPrevSource);
						m_pGridSource->SelectCell(iLayer, r);
						return;
					}
				}
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	RandClip
//---------------------------------------------------------------------------//
auto CMGGridPanel::RandClip(int iLayer) -> void {
	ASSERT(ValidIndex(iLayer, NUM_MASTER_LAYERS));
	if (ValidIndex(iLayer, NUM_MASTER_LAYERS)) {
		if (NumSourcesInCol(iLayer) == 0) {
			return;
		}

		bool bFound = false;
		// no source attached yet in master grid...
		if (!m_pGridMaster->GetCellData(iLayer, 0)->GetSource()) {
			while (!bFound) {
				int row = rand() % GRID_PAGE_ROWS;
				auto pMGSource = m_pGridSource->GetCellData(iLayer, row)->GetSource();
				if (pMGSource) {
					pMGSource->Attach(iLayer);
					m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGSource);
					m_pGridSource->SelectCell(iLayer, row);
					bFound = true;
				}
			}
			return;
		}

		// there's an source in the master output layer, so we unattach it
		// then we find a 'random' source and attach it...
		for (int row = 0; row < GRID_PAGE_ROWS; ++row) {
			auto pMGCurSource = m_pGridSource->GetCellData(iLayer, row)->GetSource();
			if (pMGCurSource && pMGCurSource->IsAttached()) {
				while (!bFound) {
					int r = rand() % GRID_PAGE_ROWS;
					if (r == row) {
						continue;
					}

					auto pMGRandSource = m_pGridSource->GetCellData(iLayer, r)->GetSource();
					if (pMGRandSource) {
						pMGCurSource->Unattach();
						pMGRandSource->Attach(iLayer);
						m_pGridMaster->GetCellData(iLayer, 0)->SetSource(pMGRandSource);
						m_pGridSource->SelectCell(iLayer, r);
						bFound = true;
					}
				}
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	AddSourceToGrid
//---------------------------------------------------------------------------//
auto CMGGridPanel::AddSourceToGrid(CMGSource* pSource) -> void {
	if (!pSource) {
		GLOG(("ERROR: Couldn't add source to grid!\n"));
		return;
	}

	if (!FindEmptyAndAdd(pSource)) {
		for (int page = 0; page < NUM_GRID_PAGES; ++page) {
			SelectGridPage(page);
			if (FindEmptyAndAdd(pSource)) {
				return;
			}
		}
	}

}


//---------------------------------------------------------------------------//
//	FindEmptyAndAdd
//---------------------------------------------------------------------------//
auto CMGGridPanel::FindEmptyAndAdd(CMGSource* pSource) -> bool {
	for (int col = 0; col < GRID_PAGE_COLS; ++col) {
		for (int row = 0; row < GRID_PAGE_ROWS; ++row) {
			if (!m_pGridSource->GetCellData(col, row)->GetSource()) {
				m_pGridSource->GetCellData(col, row)->SetSource(pSource);
				pSource->SetGridPos(col, row);
				return true;
			}
		}
	}
	return false;
}


//---------------------------------------------------------------------------//
//	AttachSource
//---------------------------------------------------------------------------//
auto CMGGridPanel::AttachSource(CMGSource* pSource, int iLayer) -> void {
	auto pMasterItem = m_pGridMaster->GetCellData(iLayer, 0);
	if (pSource && pMasterItem && pSource != pMasterItem->GetSource()) {
		if (pMasterItem->GetSource()) {
			pMasterItem->GetSource()->Unattach();
		}
		pSource->Attach(iLayer);
		pMasterItem->SetSource(pSource);
	}
}


//---------------------------------------------------------------------------//
//	UnattachSource
//---------------------------------------------------------------------------//
auto CMGGridPanel::UnattachSource(CMGSource* pSource) -> void {
	if (pSource && pSource->IsAttached()) {
		auto pMasterItem = m_pGridMaster->GetCellData(pSource->GetLayerAttached(), 0);
		pMasterItem->GetSource()->Unattach();
		pMasterItem->SetSource(nullptr);
	}
}


//---------------------------------------------------------------------------//
//	UnloadSource
//---------------------------------------------------------------------------//
auto CMGGridPanel::UnloadSource(CMGSource* pMGSource) -> void {
	if (pMGSource) {
		if (pMGSource->IsAttached()) {
			m_pGridMaster->GetCellData(pMGSource->GetLayerAttached(), 0)->SetSource(nullptr);
		}

		// ebp-> this fixes a strange heap stomp crash.
		pMGSource->SetParent(nullptr);
		g_pMGApp->FocusItem(nullptr);

		DELETE_PTR(pMGSource);

		// ebp-> after unloading a source from the grid,
		// switch back to the source browser tab.
		g_pAppMain->ShowBrowse();
	}
}


//---------------------------------------------------------------------------//
//	UnloadSourceAt
//---------------------------------------------------------------------------//
auto CMGGridPanel::UnloadSourceAt(int x, int y) -> void {
	auto pGridItem = m_pGridSource->GetCellData(x, y);
	if (pGridItem != nullptr) {
		UnloadSource(pGridItem->GetSource());
	}
}


//---------------------------------------------------------------------------//
//	OnSelectGridPage
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnSelectGridPage() -> void {
	if (m_pGridSource != nullptr) {
		m_pGridSource->SelectCell();
	}

	m_pGridSource = m_vGridSourcePages[m_pPCGrid->GetActivePage()];

	if (m_pGridSource != nullptr) {
		m_pGridSource->SelectCell();
	}
}


//---------------------------------------------------------------------------//
//	OnGridDragOver
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridDragOver(CMGSourceGrid* pDestGrid, CMGControl* pFrom, int iX, int iY, bool& bAccept) -> void {
	int col, row;
	if (pDestGrid->CellAtXY(iX - pDestGrid->Left(), iY - pDestGrid->Top(), col, row)) {
		if (pDestGrid == m_pGridMaster) {
			int scol, srow;
			bAccept = true; //!pDestGrid->GetCellData(col, row)->GetSource();
			if (bAccept) {
				pDestGrid->GetSelectedCell(scol, srow);
				// Master FX
				if (pFrom == pDestGrid) {
					bAccept = (scol != col);
				} else {
					bAccept = (pFrom == m_pGridSource);
				}
			}
		} else {
			// Loaded FX
			bAccept = (pFrom == g_pAppMain->SourceTree() || pFrom == m_pGridMaster || pFrom == pDestGrid);
			bAccept = (bAccept && !pDestGrid->GetCellData(col, row)->GetSource());
		}
	}
}


//---------------------------------------------------------------------------//
//	OnGridDragDrop
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridDragDrop(CMGSourceGrid* pDestGrid, CMGControl* pFrom, int iX, int iY) -> void {
	if (pDestGrid == m_pGridMaster) {
		// Dragged to Master Grid...
		int col, row;
		if (pDestGrid->CellAtXY(iX - pDestGrid->Left(), iY - pDestGrid->Top(), col, row)) {
			if (pFrom == m_pGridSource) {
				auto pSource = m_pGridSource->GetSelectedCell()->GetSource();
				// From Source Grid to Master Grid
				auto pItem = pDestGrid->GetCellData(col, row);
				if (pItem->GetSource()) {
					if (pItem->GetSource() == pSource) {
						return;
					}
					pItem->GetSource()->Unattach();
					pItem->SetSource(nullptr);
				}
				pDestGrid->GetCellData(col, row)->SetSource(pSource);
				// And attach!
				if (pSource->IsAttached()) {
					pDestGrid->GetCellData(pSource->GetLayerAttached(), row)->SetSource(nullptr);
				}
				pSource->Attach(col);
			} else if (pFrom == pDestGrid) {
				// From Master Grid to Master Grid
				auto pSource = pDestGrid->GetSelectedCell()->GetSource();
				pDestGrid->GetSelectedCell()->SetSource(nullptr);
				pDestGrid->GetCellData(col, row)->SetSource(pSource);
				pDestGrid->SelectCell(col, row);
				pSource->Move(col);
			}
		}
	} else {
		// Dragged to Source Grid...
		if (pFrom == g_pAppMain->SourceTree()) {
			// From Tree to Source Grid
			auto pItem = g_pAppMain->SourceTree()->GetSelected();
			if (pItem) {
				// Load new source
				auto pSource = CMGSource::Create(pItem->GetPath());
				if (pSource) {
					int col, row;
					pDestGrid->CellAtXY(iX - pDestGrid->Left(), iY - pDestGrid->Top(), col, row); // This is true, as OnDragOver we said that this pos was correct
					pDestGrid->GetCellData(col, row)->SetSource(pSource);
					pDestGrid->SelectCell(col, row);
					pSource->SetGridPos(col, row);
				}
			}
		} else if (pFrom == pDestGrid) {
			// From Source Grid to Source Grid
			auto pSource = pDestGrid->GetSelectedCell()->GetSource();
			int col, row;
			pDestGrid->CellAtXY(iX - pDestGrid->Left() , iY - pDestGrid->Top(), col, row); // This is true, as OnDragOver we said that this pos was correct
			pDestGrid->GetCellData(col, row)->SetSource(pSource);
			pDestGrid->GetSelectedCell()->SetSource(nullptr);
			pDestGrid->SelectCell(col, row);
			pSource->SetGridPos(col, row);
		}
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnGridMouseMove
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridMouseMove(CMGSourceGrid* pGrid, int iX, int iY) -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	int col, row;
	if (pGrid->CellAtXY(iX - pGrid->Left(), iY - pGrid->Top(), col, row)) {
		pGrid->HoverCell(col, row);
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	OnGridMouseDown
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridMouseDown(CMGSourceGrid* pGrid, int iX, int iY, int iButton) -> void {
	int col, row;
	int gridX = iX - pGrid->Left();
	int gridY = iY - pGrid->Top();
	if (pGrid->CellAtXY(gridX, gridY, col, row)) {
		// ebp-> feature: layer next/prev/rand
		if (iButton == MOUSE_LEFT) {
			g_pAppMain->SetActiveLayer(col);
			g_pAppMain->ShowActiveLayer();
		}

		auto pItem = pGrid->GetCellData(col, row);
		if (pItem->GetSource()) {
			if (iButton == MOUSE_LEFT) {

				// ebp-> check for user clicking tiny buttons on the header of the grid items...
				// yeah, this is getting UGLY UGLY UGLY...
				int inCellX = gridX;
				int inCellY = gridY;
				pGrid->GetPosInCell(inCellX, inCellY);

				if (inCellY <= 14 && inCellY >= 3) {	// magic fuckin numbers
					// these are for the X and the attach/unattach arrow, but they won't scale like this.

					if (pGrid == m_pGridSource) {
						if (inCellX <= 14 && inCellX >= 3) {
							// User clicked the 'X' in the top left corner of the grid item
							UnloadSource(pItem->GetSource());
							pItem->SetSource(nullptr);
							Repaint();
							return;
						}

						if (inCellX <= pGrid->GetCellW() - 3 && inCellX >= pGrid->GetCellW() - 14) {
							// User clicked the arrow int he top right corner of the grid item
							if (pItem->GetSource()->IsAttached()) {
								// unattach from the master output...
								int layer = pItem->GetSource()->GetLayerAttached();
								pItem->GetSource()->Unattach();
								m_pGridMaster->GetCellData(layer, 0)->SetSource(nullptr);
							} else {
								// replace whatever is in the master output with clicked item
								auto pMasterItem = m_pGridMaster->GetCellData(col, 0);
								if (pMasterItem->GetSource()) {
									pMasterItem->GetSource()->Unattach();
								}
								pItem->GetSource()->Attach(col);
								pMasterItem->SetSource(pItem->GetSource());
							}
							g_pAppMain->ShowSource(pItem->GetSource());
							Repaint();
							return;
						}
					}

					if (pGrid == m_pGridMaster) {
						if (inCellX <= pGrid->GetCellW() - 3 && inCellX >= pGrid->GetCellW() - 14) {
							// User clicked the arrow int he top right corner of the grid item
							pItem->GetSource()->Unattach();
							pItem->SetSource(nullptr);
							Repaint();
							return;
						}
					}
				}

				// otherwise, select the source...
				if (pGrid == m_pGridMaster) {
					m_pGridSource->SelectCell();
				} else {
					m_pGridMaster->SelectCell();
				}
				g_pAppMain->ShowSource(pItem->GetSource());
				pGrid->SelectCell(col, row);
				pGrid->BeginDrag();

			} else if (iButton == MOUSE_RIGHT) {
				// right button means unattach source from master or unload source from grid...
				if (pGrid == m_pGridMaster) {
					pItem->GetSource()->Unattach();
					pItem->SetSource(nullptr);
					Repaint();
				}
				//else {
				//	UnloadSource(pItem->GetSource());
				//	pItem->SetSource(nullptr);
				//	Repaint();
				//}
			}
		}
	}
}



//---------------------------------------------------------------------------//
//	OnGridMouseUp
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridMouseUp(CMGSourceGrid* pGrid, int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(pGrid);
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
}


//---------------------------------------------------------------------------//
//	OnGridMouseDoubleClk
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridMouseDoubleClk(CMGSourceGrid* pGrid, int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(pGrid);
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
	int col, row;
	if (pGrid->CellAtXY(iX - pGrid->Left(), iY - pGrid->Top(), col, row)) {
		if (iButton == MOUSE_LEFT && pGrid != m_pGridMaster) {
			auto pItem = pGrid->GetCellData(col, row);
			auto pMasterItem = m_pGridMaster->GetCellData(col, 0);
			if (pItem->GetSource() && pItem->GetSource() != pMasterItem->GetSource()) {
				if (pMasterItem->GetSource()) {
					pMasterItem->GetSource()->Unattach();
				}
				pItem->GetSource()->Attach(col);
				pMasterItem->SetSource(pItem->GetSource());
				return;
			}
		}
	}

	OnGridMouseDown(pGrid, iX, iY, iButton);
}


//---------------------------------------------------------------------------//
//	OnGridKeyDown
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridKeyDown(CMGSourceGrid* pGrid, int vkey) -> void {
	UNREFERENCED_PARAMETER(pGrid);
	UNREFERENCED_PARAMETER(vkey);
}


//---------------------------------------------------------------------------//
//	OnGridKeyUp
//---------------------------------------------------------------------------//
auto CMGGridPanel::OnGridKeyUp(CMGSourceGrid* pGrid, int vkey) -> void {
	UNREFERENCED_PARAMETER(pGrid);
	UNREFERENCED_PARAMETER(vkey);
}


//---------------------------------------------------------------------------//
//	NumSourcesInCol
//---------------------------------------------------------------------------//
auto CMGGridPanel::NumSourcesInCol(int iColumn) -> int {
	int count = 0;
	if (ValidIndex(iColumn, GRID_PAGE_COLS)) {
		for (int row = 0; row < GRID_PAGE_ROWS; ++row) {
			auto pMGSource = m_pGridSource->GetCellData(iColumn, row)->GetSource();
			if (pMGSource) {
				++count;
			}
		}
	}
	return count;
}
