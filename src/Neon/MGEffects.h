//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGEFFECTS_H
#define NEON_MGEFFECTS_H

#include "Controls/MGControl.h"

class CMGSource;
class CMGPanel;
class CMGListBox;
class CMGEffect;

//---------------------------------------------------------------------------//
//	Class CMGEffects
//---------------------------------------------------------------------------//
class CMGEffects : public CMGControl {
public:
	CMGEffects(CMGControl* pParent, CMGSource* pSource);
	~CMGEffects() override;

	auto GetLBEffects() -> CMGListBox* { return m_pLBEffects; }
	auto GetEffect(int i) -> CMGEffect* { ASSERT(ValidIndex(i, MAX_EFFECTS)); return m_apEffects[i]; }
	auto GetEffectVars() -> CMGControl* { return m_pActiveEffectVars; }
	auto ShowEffectVars(CMGControl* pEffect) -> void;

	auto Update() -> void;
	auto Load(CNodeFile::CNode* pNode) -> void;
	auto Save(CNodeFile* pFile) -> void;
	auto SwapEffects(int iSlotSrc, int iSlotDst) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGEffects\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	virtual auto OnSelectEffect(CMGListBox* pListBox) -> void;
	// ebp-> fixit: this is also defined in VJSource.h
	static const int MAX_EFFECTS = 5;

	// Vars
	CMGPanel* m_pPanelSup;
	CMGPanel* m_pPanelInf;
	CMGListBox* m_pLBEffects;

	// Active Effect Vars
	CMGControl* m_pActiveEffectVars;

	// All Effect Vars
	CMGEffect* m_apEffects[MAX_EFFECTS];

	friend class CMGEffect;
};

#endif//NEON_MGEFFECTS_H

