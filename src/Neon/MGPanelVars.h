//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGPANELVARS_H
#define NEON_MGPANELVARS_H

#include "Controls/MGControl.h"

class CMGScrollBar;


//---------------------------------------------------------------------------//
//	Class CMGPanelVars
//---------------------------------------------------------------------------//
class CMGPanelVars : public CMGControl {
public:
	CMGPanelVars(CMGControl* pParent, uint uSourceID, int iScope, int iObject);
	CMGPanelVars(CMGControl* pParent, uint uSourceID, uint uEffectID);
	~CMGPanelVars() override;

	auto Update() -> void;

	auto Load(CNodeFile::CNode* pNode) -> void;
	auto Save(CNodeFile* pFile) -> void;

	auto OnChangeScroll() -> void { Repaint(); }	// unfinished feature

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGPanelVars\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

private:
	std::vector<CVJVar*> m_vpVJVars;
	CMGScrollBar* m_pScrollBar;	// unfinished feature
};

#endif//NEON_MGPANELVARS_H
