//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "MGHotKey.h"
#include "KeyboardDevice.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGHotKey::CMGHotKey(CMGControl* pParent, int iX, int iY, int iSize, bool bMidi, function<void()> fOnChange)
	: CMGControl(pParent, iX, iY, iSize, 20)
	, m_cChar(0)
	, m_bMidi(bMidi)
	, m_fnOnChange(fOnChange)
{
	if (m_bMidi) {
		auto pMidiService = CServiceLocator<IMidiDeviceService>::GetService();
		if (pMidiService) {
			pMidiService->AttachObserver(this);
		}
	}
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGHotKey::~CMGHotKey() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGHotKey\n"));
#endif
	if (m_bMidi) {
		auto pMidiService = CServiceLocator<IMidiDeviceService>::GetService();
		if (pMidiService) {
			pMidiService->DetachObserver(this);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGHotKey::OnMouseDown(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT) {
		SetFocus(true);
	}
}


//---------------------------------------------------------------------------//
//	OnChar
//---------------------------------------------------------------------------//
auto CMGHotKey::OnChar(char c) -> void {
	c = static_cast<char>(toupper(c));
	if (c != m_cChar) {
		if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
			m_cChar = c;
			if (m_fnOnChange) {
				m_fnOnChange();
			}
			Repaint();
		}
	}
}


//---------------------------------------------------------------------------//
//	Update
//	This is part of the MIDI Device Observer interface
//---------------------------------------------------------------------------//
auto CMGHotKey::Update(IMidiDeviceService* pMidiService) -> void {
	if (pMidiService && IsFocused()) {
		char c = pMidiService->GetLastMIDINote();
		if (c != m_cChar) {
			m_cChar = c;
			if (m_fnOnChange) {
				m_fnOnChange();
			}
			Repaint();
		}
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGHotKey::DerivDraw() -> void {
	std::string text = "None";
	if (m_bMidi) {
		if (m_cChar > 0) {
			char pBuf[3];
			Snprintf(pBuf, 3, "%02X", m_cChar);
			text = pBuf;
		}
	} else {
		if (m_cChar >= 0) {
			text.clear();
			//if (m_Ctrl) text = "Ctrl + ";
			//if (m_Shift) text+= "Shift + ";
			text += m_cChar;
		}
	}

	TRect rect(Left(), Top(), Width(), Height());

	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	if (IsFocused()) {
		neon::gui::Rect(pSkin, rect, COLOR_EDIT_BORDER_ACTIVE);
		ShrinkRect(rect, 1);
		neon::gui::FillRect(pSkin, rect, COLOR_EDIT_BG_ACTIVE);
		neon::gui::SetClipRect(rect);
		neon::gui::Text(pSkin, FONT_NORMAL, rect.x + 5, rect.y, 0, rect.h, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT_ACTIVE, text);
		neon::gui::SetClipRect();
	} else {
		neon::gui::Rect(pSkin, rect, COLOR_EDIT_BORDER);
		ShrinkRect(rect, 1);
		neon::gui::FillRect(pSkin, rect, COLOR_EDIT_BG);
		neon::gui::SetClipRect(rect);
		neon::gui::Text(pSkin, FONT_NORMAL, rect.x + 5, rect.y, 0, rect.h, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT, text);
		neon::gui::SetClipRect();
	}
}
