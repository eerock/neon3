//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Neon.rc
//
#define IDI_ICON1                       101
#define IDD_MAINDIALOG                  102
#define IDB_NEONTITLE                   105
#define IDC_IMAGETITLE                  1001
#define IDC_DEVICE_COMBO                1010
#define IDC_FULLSCREEN                  1012
#define IDC_RES_W                       1015
#define IDC_RES_H                       1016
#define IDC_NEONPLAYER                  1024
#define IDC_SHOWFPS                     1028
#define IDC_MFCEDITBROWSE1              1029
#define IDC_LABEL_X                     1032
#define IDC_GROUP_PLAYER                1033
#define IDC_GROUP_OUTPUT                1034
#define IDC_LABEL_DEVICE                1035
#define IDC_LABEL_RESOLUTION            1036

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1037
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
