//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_APPOPTIONS_H
#define NEON_APPOPTIONS_H

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace pt = boost::property_tree;


//---------------------------------------------------------------------------//
//	TAppOptions
//---------------------------------------------------------------------------//
struct TAppOptions {
	int m_iDevice;
	bool m_bFullscreen;
	bool m_bShowFps;
	int m_iRefreshRate;
	int m_iRWidth;
	int m_iRHeight;
	int m_iRLeft;
	int m_iRTop;

	int m_iGuiRLeft;
	int m_iGuiRTop;

	int m_iDefaultMidiChannel;
	std::string m_sDefaultMidiDevice;

	bool m_bNeonPlayer;	// ebp-> Launch the NeonPlayer
};

//---------------------------------------------------------------------------//
//	CAppOptions
//---------------------------------------------------------------------------//
class CAppOptions : public IAppOptionsService {
public:
	CAppOptions();
	~CAppOptions() override;

	auto Set(std::string const& sParamName, int const& iValue) -> void override;
	auto Set(std::string const& sParamName, std::string const& sValue) -> void override;
	auto Set(std::string const& sParamName, bool const& bValue) -> void override;
	auto Get(std::string const& sParamName, int const& iDefaultValue) -> int override;
	auto Get(std::string const& sParamName, std::string const& sDefaultValue) -> std::string override;
	auto Get(std::string const& sParamName, bool const& bDefaultValue) -> bool override;
	auto Load(std::string const& sFilename) -> bool override;
	auto Save(std::string const& sFilename = "") -> void override;

private:
	pt::ptree m_ptOpts;
	std::string m_sName;
};


#endif//NEON_APPOPTIONS_H
