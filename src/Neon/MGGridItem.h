//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGGRIDITEM_H
#define NEON_MGGRIDITEM_H

class CMGSource;


//---------------------------------------------------------------------------//
//	Class CMGGridItem
//---------------------------------------------------------------------------//
class CMGGridItem : public CMGControl {
public:
	CMGGridItem();

	~CMGGridItem() override;

	auto Init(bool bMaster) -> void;
	auto Update() -> void;
	auto Draw(int x, int y, int w, int h, bool bSelected) -> void;

	auto OnMouseDown(int /*iX*/, int /*iY*/, int /*iButton*/) -> void override;

	auto GetSource() const -> CMGSource* { return m_pSource; }
	auto SetSource(CMGSource* pSource) -> void { m_pSource = pSource; }

private:
	bool m_bMaster;
	CMGSource* m_pSource;
};

#endif//NEON_MGGRIDITEM_H
