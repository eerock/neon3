//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "KeyboardDevice.h"
#include "NeonMain.h"
#include "MGBeatManager.h"
#include "Controls/MGLabel.h"
#include "Controls/MGButton.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGTrackBar.h"
#include "MGPanelFFT.h"
#include "LinkSystem/Link.h"

static const float s_fDefaultBpm = 120.0f;


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CBeatTapper::Update(double dTime) -> bool {
	if (CKeyboardDevice::KeyPressed(VK_SPACE)) {
		if (!m_bBeatTapped) {
			m_bBeatTapped = true;
			// calculate the bpm from time between taps...
			m_dBPM = 60.0 / (dTime - m_dPrevTapTime);

			m_bpmHistory.add(m_dBPM);
			m_bpmHistory2ndOrder.add(GetBpmMean());

			//GLOG(("Beat Tap! raw BPM: %f, 1st order mean: %f, 2nd order mean: %f\n", m_dBPM, GetBpmMean(), GetBpmMean2ndOrder()));
			m_dPrevTapTime = dTime;
		}
	} else {
		m_bBeatTapped = false;
	}
	return m_bBeatTapped;
}




//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGBeatManager::CMGBeatManager(CMGControl* pParent)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_tTimer()
	, m_dTime(0.0)
	, m_dTimeBeat(0.0)
	, m_iMasterBeat(0)
	, m_fMasterBpm(0.f)
	, m_fMasterBpmInv(0.f)
	, m_bBeatKilled(false)
	, m_bIsBeat(false)
	, m_iBeatNum(-1)
	, m_tBeatTapper()
	, m_pLabelBpm(nullptr)
	, m_pCBUpdateFFT(nullptr)
	, m_pPanelFFT(nullptr)
	, m_pGainSlider(nullptr)
	, m_pBtnLinkGain(nullptr)
	, m_pLinkGain(nullptr)
{
	m_iAlign = MGALIGN_CLIENT;

	CMGControl* pControl = nullptr;
	m_pLabelBpm = NEW CMGLabel(this, 10, 10, MGALIGN_NONE, FONT_BPM, "");
	pControl = NEW CMGButton(this, 5, 63, CMGButton::SMALL, "+", bind(&CMGBeatManager::OnBtnUpBpm, this));
	pControl = NEW CMGButton(this, 25, 63, CMGButton::SMALL, "-", bind(&CMGBeatManager::OnBtnDnBpm, this));
	//pControl = NEW CMGButton(this, 45, 63, CMGButton::SMALL, "<", bind(&CMGBeatManager::OnBtnRewBpm, this));
	//pControl = NEW CMGButton(this, 65, 63, CMGButton::SMALL, ">", bind(&CMGBeatManager::OnBtnFwdBpm, this));
	//pControl = NEW CMGButton(this, 85, 63, CMGButton::SMALL, "R", bind(&CMGBeatManager::OnBtnResetBpm, this));

	m_pBtnLinkGain = NEW CMGButton(this, 282, 66, CMGButton::LINK, "L", bind(&CMGBeatManager::OnBtnLinkGain, this));

	// magic fuckin numbers
	m_pPanelFFT = NEW CMGPanelFFT(this, 149, 2, 130, 60, 15);
	
	m_pGainSlider = NEW CMGTrackBar(this, 282, 2, 60, true, bind(&CMGBeatManager::OnChangeGain, this));
	m_pGainSlider->SetPos(0.8f);
	
	m_pCBUpdateFFT = NEW CMGCheckBox(this, 150, 63, "Enable FFT", true, false, bind(&CMGBeatManager::OnChangeEnableFFT, this));
	m_pCBUpdateFFT->SetChecked(false);

	m_tTimer.Reset();

	SetMasterBpm(s_fDefaultBpm);
	OnBtnResetBpm();
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGBeatManager::~CMGBeatManager() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGBeatManager\n"));
#endif
}


//---------------------------------------------------------------------------//
//	SetMasterBpm
//---------------------------------------------------------------------------//
auto CMGBeatManager::SetMasterBpm(float fBpm) -> void {
	fBpm = Round(fBpm * 10.f) / 10.f;
	m_fMasterBpm = fBpm;
	m_fMasterBpmInv = 60000.f / fBpm;

	std::stringstream ss;
	ss << std::setprecision(1) << std::fixed;	// "%4.1f"
	ss << fBpm << " bpm";
	m_pLabelBpm->Set(ss.str());
}


//---------------------------------------------------------------------------//
//	IsBeat
//---------------------------------------------------------------------------//
auto CMGBeatManager::IsBeat(int iBeat, int iOff) -> bool {
	return (!m_bBeatKilled && m_bIsBeat && (((m_iBeatNum - iOff) % iBeat) == 0));
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGBeatManager::Update() -> void {
	// Beat
	m_dTime = m_tTimer.Get();

	// Beat number
	int iBeat = (int)((m_dTime - m_dTimeBeat) * 1000.f / m_fMasterBpmInv);
	m_bIsBeat = (iBeat > m_iBeatNum);
	m_iBeatNum = iBeat;

	// Beat Tapper - tap the TAB key to set the bpm
	if (m_tBeatTapper.Update(m_dTime)) {
		SetMasterBpm(static_cast<float>(m_tBeatTapper.GetBpmMean2ndOrder()));
	}

	// "Enable FFT"
	if (m_pCBUpdateFFT->Checked()) {
		m_pPanelFFT->Update(g_pAppMain->VJController()->GetFFT());
	} else {
		m_pPanelFFT->Update(nullptr);
	}
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CMGBeatManager::Load(CNodeFile::CNode* pNode) -> void {
	UNREFERENCED_PARAMETER(pNode);
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGBeatManager::Save(CNodeFile* pFile) -> void {
	UNREFERENCED_PARAMETER(pFile);
}


//---------------------------------------------------------------------------//
//	OnBtnUpBpm
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnBtnUpBpm() -> void {
	SetMasterBpm(m_fMasterBpm + 0.1f);
}


//---------------------------------------------------------------------------//
//	OnBtnDnBpm
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnBtnDnBpm() -> void {
	SetMasterBpm(m_fMasterBpm - 0.1f);
}


//---------------------------------------------------------------------------//
//	OnBtnFwdBpm
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnBtnFwdBpm() -> void {
	// ebp-> todo: what would forward do to the bpm?
	//SetMasterBpm(m_fMasterBpm + 1.f);
}


//---------------------------------------------------------------------------//
//	OnBtnRewBpm
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnBtnRewBpm() -> void {
	// ebp-> todo: what would rewind do to the bpm?
	// hmm, it might've advanced the beat number it's currently on, like you
	// just nudged bpm to match, but you're on beat 3 and audio is on beat 1.
	// click these to nudge the beat number by one beat.
	//SetMasterBpm(m_fMasterBpm - 1.f);
}


//---------------------------------------------------------------------------//
//	OnBtnResetBpm
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnBtnResetBpm() -> void {
	m_tTimer.Reset();
}


//---------------------------------------------------------------------------//
//	OnChangeGain
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnChangeGain() -> void {
	g_pAppMain->VJController()->SetGain(5.f - m_pGainSlider->GetPos() * 5.f);
}


//---------------------------------------------------------------------------//
//	OnBtnLinkGain
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnBtnLinkGain() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkGain,
		m_pBtnLinkGain->Left(),
		m_pBtnLinkGain->Top(),
		ILinkBehaviour::TRACK_BAR,
		true,
		m_pGainSlider,
		m_pBtnLinkGain,
		bind(&CMGBeatManager::OnCloseLinkGain, this)
	);
}


//---------------------------------------------------------------------------//
//	OnCloseLinkGain
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnCloseLinkGain() -> void {
}


//---------------------------------------------------------------------------//
//	OnChangeEnableFFT
//---------------------------------------------------------------------------//
auto CMGBeatManager::OnChangeEnableFFT() -> void {
	CSound* pSound = g_pAppMain->VJController()->GetSound();
	if (m_pCBUpdateFFT->Checked()) {
		pSound->Resume();
	} else {
		pSound->Pause();
	}
}