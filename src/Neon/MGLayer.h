//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGLAYER_H
#define NEON_MGLAYER_H

#include "Controls/MGControl.h"

class CMGButton;
class CMGCheckBox;
class CMGGridPanel;
class CMGTrackBar;
class CVJLayer;

class CMGLayer : public CMGControl {
public:
	CMGLayer(CMGControl* pParent, int iIndex, CVJLayer* pVJLayer, CMGGridPanel* pGridPanel);
	~CMGLayer() override;

	auto Load(CNodeFile::CNode* pNode) -> bool;
	auto Save(CNodeFile* pNodeFile) -> void;
	auto Update() -> void;		// needed for updating Links

	// Callbacks
	auto OnSetAlpha() -> void;
	auto OnBypass() -> void;
	auto OnBtnLinkAlpha() -> void;
	auto OnBtnLinkBypass() -> void;

	auto OnBtnNext() -> void;
	auto OnBtnPrev() -> void;
	auto OnBtnRand() -> void;
	auto OnBtnLinkNext() -> void;
	auto OnBtnLinkPrev() -> void;
	auto OnBtnLinkRand() -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGLayer\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

private:
	int m_iLayerIndex;		// layer index

	CVJLayer* m_pVJLayer;		// pointer to VJLayer, for alpha and bypass
	CMGGridPanel* m_pGridPanel;	// pointer to MGGridPanel, to execute layer next/prev/rand

	CMGLabel* m_pLayerTitle;	// to identify layer

	CMGTrackBar* m_pLayerAlpha;		// layer Alpha slider
	CMGCheckBox* m_pLayerBypass;	// layer Bypass toggle

	CMGButton* m_pBtnLinkAlpha;		// linking button for layer Alpha
	CMGButton* m_pBtnLinkBypass;	// linking button for layer Bypass

	CLink* m_pLinkAlpha;	// link for layer Alpha
	CLink* m_pLinkBypass;	// link for layer Bypass

	// ebp-> note: for these buttons to be linkable
	// and not be VJVars, there's an awful lot of
	// code to set up a linked button manually.
	// Maybe look into ways of packaging it up, in 
	// the same manner as VJVars do it.  Would it
	// be too much work to modify VJVars to handle
	// these?

	CMGButton* m_pBtnNext;	// finds next clip in layer (column in ui) and switches to it
	CMGButton* m_pBtnPrev;	// finds prev clip in layer (column in ui) and switches to it	
	CMGButton* m_pBtnRand;	// finds rand clip in layer (column in ui) and switches to it

	CMGButton* m_pBtnLinkNext;	// linking button for 'next clip'
	CMGButton* m_pBtnLinkPrev;	// linking button for 'prev clip'
	CMGButton* m_pBtnLinkRand;	// linking button for 'rand clip'

	CLink* m_pLinkNext;	// link for 'next clip'
	CLink* m_pLinkPrev;	// link for 'prev clip'
	CLink* m_pLinkRand;	// link for 'rand clip'
};

#endif//NEON_MGLAYER_H
