//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"

#include "AppOptions.h"
#include "Controls/MGButton.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGComboBox.h"
#include "Controls/MGGroupBox.h"
#include "Controls/MGLabel.h"
#include "Controls/MGPageControl.h"
#include "Controls/MGPanel.h"
#include "Controls/MGPanelWin.h"
#include "Controls/MGTrackBar.h"
#include "Controls/MGTreeView.h"
#include "Controls/MGWindow.h"
#include "DisplayWindow.h"
#include "KeyboardDevice.h"
#include "LinkSystem/Link.h"
#include "MGApplication.h"
#include "MGBeatManager.h"
#include "MGGridPanel.h"
#include "MGLayer.h"
#include "MGSource.h"
#include "MGWindowConfirm.h"
#include "MGWindowNewSource.h"
#include "MGWindowVarLinker.h"
#include "MidiDevice.h"
#include "VJController.h"
#include "VJLayer.h"


#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
namespace filesys = boost::filesystem;

#ifdef MULTITHREADED
std::thread tDraw;
#endif

// ebp-> Global CMGAppMain g_pAppMain!
CMGAppMain* g_pAppMain = nullptr;

#define APP_MAIN_WIDTH		(1152)	//(1024)
#define APP_MAIN_HEIGHT		(864)	//(768)


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGAppMain::CMGAppMain()
	: m_bOk(false)
	, m_iActiveLayer(-1)
	, m_sCurrentProject()
	, m_pVJController(nullptr)
	, m_pApplication(nullptr)
	, m_pActiveSource(nullptr)
	, m_pTimer(nullptr)
	, m_pMidiDevice(nullptr)
	, m_pTopPanel(nullptr)
	, m_pGBDatos(nullptr)
	, m_pProjectLabel(nullptr)
	, m_pBeatManager(nullptr)
	, m_pDataPages(nullptr)
	, m_pPageBrowse(nullptr)
	, m_pPageSource(nullptr)
	, m_pPageLayer(nullptr)
	, m_pPageOptions(nullptr)
	, m_pCHFullScreen(nullptr)
	, m_pBtnResetWindows(nullptr)
	, m_pTBMasterAlpha(nullptr)
	, m_pCHMasterBypass(nullptr)
	, m_pBtnLinkMasterAlpha(nullptr)
	, m_pBtnLinkMasterBypass(nullptr)
	, m_pLinkMasterAlpha(nullptr)
	, m_pLinkMasterBypass(nullptr)
	, m_pCBMidiDevices(nullptr)
	, m_pCBMidiChannel(nullptr)
	, m_pTBClearR(nullptr)
	, m_pTBClearG(nullptr)
	, m_pTBClearB(nullptr)
	, m_pPanelSourceSup(nullptr)
	, m_pActiveLayer(nullptr)
	, m_vLayers(NUM_MASTER_LAYERS)
	, m_pTreeSources(nullptr)
	, m_pGridPanel(nullptr)
	, m_pWindowNewSource(nullptr)
	, m_pWindowVarLinker(nullptr)
	, m_pWindowConfirmQuit(nullptr)
	, m_vEffectsList()
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGAppMain::~CMGAppMain() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGAppMain\n"));
#endif
	// weak pointers...
	if (m_pActiveLayer) {
		m_pActiveLayer->SetParent(nullptr);
	}
	m_pActiveLayer = nullptr;

	for (auto layer : m_vLayers) {
		DELETE_PTR(layer);
	}
	m_vLayers.clear();

	// !!!!!
	ShowSource(nullptr);

	DELETE_PTR(m_pApplication);		// ok - this holds the root of MGui hierarchy
	DELETE_PTR(m_pWindowNewSource);	// ok - not directly linked to MGui hierarchy
	DELETE_PTR(m_pWindowVarLinker);	// ok - not directly linked to MGui hierarchy
	DELETE_PTR(m_pWindowConfirmQuit);

	// ebp-> deleting the VJController before the MGApplication exposes some
	// order of operation bugs in the shutdown sequence.
	DELETE_PTR(m_pVJController);	// ok - object

	CServiceLocator<IMidiDeviceService>::RegisterService(nullptr);

	DELETE_PTR(m_pTimer);			// ok - object

	DELETE_PTR(m_pLinkMasterAlpha);
	DELETE_PTR(m_pLinkMasterBypass);

	g_pAppMain = nullptr;
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMGAppMain::Init(THInst hInstance) -> bool {
	g_pAppMain = this;

	auto pAppOpts = CServiceLocator<IAppOptionsService>::GetService();

	GLOG((VERSION_LONG " :\n"));
	GLOG(("> code update by neon@eerock.com\n"));
	GLOG(("> fx update by charles@photropik.com\n"));

	// init Keyboard...
	CKeyboardDevice::Init();

	// MIDI Devices...
	CServiceLocator<IMidiDeviceService>::RegisterService(NEW CMidiDevice);

	// Timer...
	m_pTimer = NEW CTimer;

	// Application Interface...
	GLOG(("Initializing Application...\n"));
	m_pApplication = NEW CMGApplication;
	if (!m_pApplication->Init(hInstance, APP_MAIN_WIDTH, APP_MAIN_HEIGHT)) {
		GLOG(("ERROR: Couldn't instantiate new CMGApplication!\n"));
		DELETE_PTR(m_pApplication);
		return false;
	}

	// display width and height
	int display_width = pAppOpts->Get("neonconfig.display.rwidth", DISPLAY_WINDOW_WIDTH);
	int display_height = pAppOpts->Get("neonconfig.display.rheight", DISPLAY_WINDOW_HEIGHT);
	TGraphicsMode tGraphicsMode(
		pAppOpts->Get("neonconfig.display.device", 0),
		display_width,
		display_height,
		32,
		pAppOpts->Get("neonconfig.display.refresh", 60),
		false,	//pAppOpts->Get("neonconfig.display.fullscreen", false),
		false
	);
	TVectorI2 tCustomTex1(
		display_width >> 1,
		display_height >> 1
	);
	TVectorI2 tCustomTex2(
		display_width >> 2,
		display_height >> 2
	);
	bool bCanMoveWindow = true;
	bool bShowMouse = true;

	GLOG(("Initializing VJController...\n"));
	m_pVJController = NEW CVJController;
	if (!m_pVJController->Init(
			0,
			NUM_MASTER_LAYERS,
			tGraphicsMode,
			tCustomTex1,
			tCustomTex2,
			bCanMoveWindow,
			bShowMouse)) {
		GLOG(("ERROR: Couldn't instantiate new CVJController!\n"));
		DELETE_PTR(m_pVJController);
		DELETE_PTR(m_pApplication);
		return false;
	}

	CreateBase();
	CreatePanelData();
	CreatePanelTop();
	CreateWindows();

	GLOG(("Initializing MIDI Devices...\n"));
	RefreshMidiDevices();

	m_pTimer->Reset();

	//m_pVJController->GetSound()->Load("01-SquareDance.wav");
	//m_pVJController->GetSound()->Play(0.f);
	m_pVJController->GetSound()->Record();

	GLOG(("Refreshing Directories...\n"));
	RefreshDirectories();

#if (PLATFORM == PLATFORM_WINDOWS)
	GLOG(("Showing the Window...\n"));
	ShowWindow(m_pApplication->GetHWnd(), SW_SHOWNORMAL);
	UpdateWindow(m_pApplication->GetHWnd());
	SetFocus(m_pApplication->GetHWnd());
#endif//PLATFORM

	GLOG(("Initialization Okay!\n"));
	GLOG(("Have fun!\n"));

	m_pApplication->Repaint();
	m_pApplication->MustRepaint();

#ifdef _DEBUG
#ifdef VERBOSE_MEM_LOG
	m_pApplication->PrintMGuiHierarchy();
#endif
#endif

#ifdef MULTITHREADED
	GLOG(("Starting Draw Thread\n"));
	tDraw = std::thread(&CMGAppMain::DrawMain, *this);
#endif
	
	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGAppMain::Update() -> bool {
	//GLOG(("CMGAppMain::Update()\n"));
	// do main update (separate thread)
	CKeyboardDevice::Update();
	UpdateMain();

	// do main draw (separate thread)
#ifndef MULTITHREADED
	DrawMain();
#endif

	return !m_pApplication->WantClose();
}


//---------------------------------------------------------------------------//
//	UpdateMain
//---------------------------------------------------------------------------//
auto CMGAppMain::UpdateMain() -> void {
	// BeatManager
	m_pBeatManager->Update();

	if (m_pLinkMasterAlpha && m_pLinkMasterAlpha->Evaluate()) {
		float fPos = m_pLinkMasterAlpha->ValueTrackBar();
		m_pTBMasterAlpha->SetPos(fPos);
	}

	if (m_pLinkMasterBypass && m_pLinkMasterBypass->Evaluate()) {
		bool bChecked = m_pLinkMasterBypass->ValueCheckBox();
		m_pCHMasterBypass->SetChecked(bChecked);
	}

	// Update GUI app
	m_pApplication->Update();

	// PanelGrid Update
	m_pGridPanel->Update();

	// Update all layers...
	for (int i = 0; i < NUM_MASTER_LAYERS; ++i) {
		m_vLayers[i]->Update();
	}

	auto pAppWindow = m_pVJController->GetDisplayWindow();
	if (pAppWindow && (pAppWindow->IsFullScreen() != m_pCHFullScreen->Checked())) {
		m_pCHFullScreen->SetChecked(pAppWindow->IsFullScreen());
	}

	if (m_pVJController) {
		m_pVJController->Update();
	}
}


//---------------------------------------------------------------------------//
//	DrawMain
//---------------------------------------------------------------------------//
auto CMGAppMain::DrawMain() -> void {
#ifdef MULTITHREADED
	while (!m_pApplication->WantClose()) {
		while (!g_DisplayDevice.IsOk()) {}
#endif
		if (m_pVJController) {
			m_pVJController->Draw();
		}
#ifdef MULTITHREADED
	}
#endif
}


//---------------------------------------------------------------------------//
//	NewProject
//	ebp-> todo: upon 'New Project', should ask to save the current project, if changed
//---------------------------------------------------------------------------//
auto CMGAppMain::NewProject() -> void {
	ResetProject();
}


//---------------------------------------------------------------------------//
//	LoadProject
//	ebp-> load *.NPJ files (Neon project files)
//---------------------------------------------------------------------------//
auto CMGAppMain::LoadProject() -> void {
	std::string sFile;
	// Load neon project
	bool bOpen = g_pMGApp->OpenFileOpen("Open Project", neon::sDirProjects, "Neon Project File (*.npj)\0*.npj\0\0", sFile);
	if (bOpen) {
		NewProject();

		GLOG(("Loading %s...\n", sFile.c_str()));

		CNodeFile NodeFile;
		if (!NodeFile.LoadFromFile(sFile)) {
			GLOG(("ERROR: Can't load project file %s\n", sFile.c_str()));
			return;
		}

		// Has project node?
		auto pNodeProj = NodeFile.FirstNode("project");
		if (!pNodeProj) {
			return;
		}

		// Get number of layout nodes = num grid sources.
		// initialize a new vjloader...
		int numSources = pNodeProj->GetNumNodes("layout");

		auto pVJLoader = std::make_unique<CVJLoader>(numSources);

		// Layout
		auto pNodeLay = pNodeProj->FirstNode("layout");
		while (pNodeLay) {
			// Correct layout?
			int tab = pNodeLay->AttrAsInt("tab", -1);
			int col = pNodeLay->AttrAsInt("col", -1);
			int row = pNodeLay->AttrAsInt("row", -1);
			if (tab != -1 && col != -1 && row != -1) {
				auto pNodeFX = pNodeLay->FirstNode("source");
				auto pSource = CMGSource::Create(pNodeFX);
				// If ok, add
				if (pSource) {
					m_pGridPanel->GetGridPage(tab)->GetCellData(col, row)->SetSource(pSource);
					pSource->SetGridPos(col, row);
				}
			}
			// Next node
			pNodeLay = pNodeLay->NextNode("layout");

			// advance the vjloader and draw...
			pVJLoader->Next();
			pVJLoader->Draw(&g_DisplayDevice);
		}

		// Load Layers...
		auto pNodeLayer = pNodeProj->FirstNode("layer");
		while (pNodeLayer) {
			int col = pNodeLayer->AttrAsInt("col", -1);
			if (col != -1) {
				// load the MGLayer...
				ASSERT(ValidIndex(col, NUM_MASTER_LAYERS));
				m_vLayers[col]->Load(pNodeLayer);
			}
			pNodeLayer = pNodeLayer->NextNode("layer");
		}

		// Load Links (move to a function)...
		auto pNodeLinks = pNodeProj->FirstNode("links");
		if (pNodeLinks) {
			auto pNode = pNodeLinks->FirstNode("masteralpha");
			if (pNode) {
				if (!m_pLinkMasterAlpha) {
					m_pLinkMasterAlpha = NEW CLink(nullptr, nullptr);
				}
				m_pLinkMasterAlpha->Load(pNode);
				m_pBtnLinkMasterAlpha->SetLinked(true);
			}

			pNode = pNodeLinks->FirstNode("masterbypass");
			if (pNode) {
				if (!m_pLinkMasterBypass) {
					m_pLinkMasterBypass = NEW CLink(nullptr, nullptr);
				}
				m_pLinkMasterBypass->Load(pNode);
				m_pBtnLinkMasterBypass->SetLinked(true);
			}
		}

		// ebp-> set the name of the project upon loading it...
		m_pProjectLabel->Set(sFile);
		m_sCurrentProject = sFile;
	}
}


//---------------------------------------------------------------------------//
//	SaveProject
//	ebp-> save *.NPJ files (Neon project files)
//---------------------------------------------------------------------------//
auto CMGAppMain::SaveProject(bool bSaveAs) -> void {
	// Save neon project
	if (m_sCurrentProject.empty() || bSaveAs) {
		std::string sFile;
		if (!g_pMGApp->OpenFileSave("Save Project", neon::sDirProjects, "Neon Project File (*.npj)\0*.npj\0\0", "*.npj", sFile)) {
			return;
		}
		m_sCurrentProject = sFile;
	}

	CNodeFile NodeFile;
	if (!NodeFile.WriteOpen(m_sCurrentProject)) {
		GLOG(("ERROR: Can't open %s for writing\n", m_sCurrentProject.c_str()));
		return;
	}
	NodeFile.WriteOpenNode("project", "");

	// Save sources for all grid pages
	for (int pg = 0; pg < NUM_GRID_PAGES; ++pg) {
		auto pGrid = m_pGridPanel->GetGridPage(pg);
		int rows = pGrid->GetRows();
		int cols = pGrid->GetCols();
		for (int c = 0; c < cols; ++c) {
			for (int r = 0; r < rows; ++r) {
				auto pSource = pGrid->GetCellData(c, r)->GetSource();
				if (pSource) {
					// Save source in specified tab, col and row
					std::stringstream ss;
					ss << "tab=\"" << pg << "\" col=\"" << c << "\" row=\"" << r << "\"";
					NodeFile.WriteOpenNode("layout", ss.str());	// layout (grid)
					pSource->Save(&NodeFile);					// source
					NodeFile.WriteCloseNode();					// layout (grid)
				}
			}
		}
	}

	// Save Layer Links
	for (int col = 0; col < NUM_MASTER_LAYERS; ++col) {
		std::stringstream ss;
		ss << " col=\"" << col << "\"";
		NodeFile.WriteOpenNode("layer", ss.str());		// layer col=0
		NodeFile.WriteOpenNode("links", "");			// links
		m_vLayers[col]->Save(&NodeFile);
		NodeFile.WriteCloseNode();						// links
		NodeFile.WriteCloseNode();						// layer
	}

	// Save Master Links
	NodeFile.WriteOpenNode("links", "");	// links
	if (m_pLinkMasterAlpha) {
		m_pLinkMasterAlpha->Save(&NodeFile, "masteralpha");
	}
	if (m_pLinkMasterBypass) {
		m_pLinkMasterBypass->Save(&NodeFile, "masterbypass");
	}
	NodeFile.WriteCloseNode();	// links

	NodeFile.WriteCloseNode();	// project
	NodeFile.WriteClose();		// end

	// ebp-> set the name of the project upon saving it...
	m_pProjectLabel->Set(m_sCurrentProject);
}


//---------------------------------------------------------------------------//
//	ResetProject
//---------------------------------------------------------------------------//
auto CMGAppMain::ResetProject() -> void {
	m_pGridPanel->Clear();
	m_pGridPanel->SelectGridPage(0);
	m_pBtnLinkMasterAlpha->SetLinked(false);
	m_pBtnLinkMasterBypass->SetLinked(false);
	DELETE_PTR(m_pLinkMasterAlpha);
	DELETE_PTR(m_pLinkMasterBypass);
	m_sCurrentProject.clear();
	m_pProjectLabel->Set(m_sCurrentProject);
}


//---------------------------------------------------------------------------//
//	NewSource
//---------------------------------------------------------------------------//
auto CMGAppMain::NewSource() -> void {
	// ebp-> todo: implement New Source functionality
	//if (!m_pWindowNewSource->IsVisible()) {
	//	m_pWindowNewSource->Show(
	//		(APP_MAIN_WIDTH - m_pWindowNewSource->Width()) >> 1,
	//		(APP_MAIN_HEIGHT - m_pWindowNewSource->Height()) >> 1,
	//		bind(&CMGAppMain::OnNewSource, this, _1)
	//	);
	//}
}


//---------------------------------------------------------------------------//
//	OnNewSource
//---------------------------------------------------------------------------//
auto CMGAppMain::OnNewSource(CMGWindow::EModalResult eResult) -> void {
	switch (eResult) {
		case CMGWindow::MR_OK: {
			break;
		}
		case CMGWindow::MR_CANCEL: {
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	DelSource
//---------------------------------------------------------------------------//
auto CMGAppMain::DelSource() -> void {
	// ebp-> todo: implement Delete Source functionality
	//if (!m_pTreeSources->GetSelected() || m_pTreeSources->GetSelected()->IsFolder()) {
	//	return;
	//}

	//if (!m_pWindowConfirmDelSource->IsVisible()) {
	//	m_pWindowConfirmDelSource->Show(
	//		(APP_MAIN_WIDTH - m_pWindowConfirmDelSource->Width()) >> 1,
	//		(APP_MAIN_HEIGHT - m_pWindowConfirmDelSource->Height()) >> 1,
	//		bind(&CMGAppMain::OnDelSource, this, _1)
	//	);
	//}
}


//---------------------------------------------------------------------------//
//	OnDelSource
//---------------------------------------------------------------------------//
auto CMGAppMain::OnDelSource(CMGWindow::EModalResult eResult) -> void {
	switch (eResult) {
		case CMGWindow::MR_OK: {
			break;
		}
		case CMGWindow::MR_CANCEL: {
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	AttachSource
//	ebp-> this is kinda hacky but the only way to get links from the MGSource
//	to trigger actions in the grid panel.
//---------------------------------------------------------------------------//
auto CMGAppMain::AttachSource(CMGSource* pSource, int iLayer) -> void {
	m_pGridPanel->AttachSource(pSource, iLayer);
}


//---------------------------------------------------------------------------//
//	UnattachSource
//	ebp-> this is kinda hacky but the only way to get links from the MGSource
//	to trigger actions in the grid panel.
//---------------------------------------------------------------------------//
auto CMGAppMain::UnattachSource(CMGSource* pSource) -> void {
	m_pGridPanel->UnattachSource(pSource);
}


//---------------------------------------------------------------------------//
//	OpenVarLinker
//---------------------------------------------------------------------------//
auto CMGAppMain::OpenVarLinker(
		CLink** ppToLink,
		int iX, int iY,
		int iBehType,
		bool bOnlyMidiKey,
		CMGControl* pRControl,
		CMGButton* pLinkButton,
		function<void(CMGWindow::EModalResult)> fnOnModalResult) -> void {

	if (!m_pWindowVarLinker->IsVisible()) {
		m_pWindowVarLinker->Show(
			ppToLink,
			iX, iY,
			iBehType,
			bOnlyMidiKey,
			pRControl,
			pLinkButton,
			fnOnModalResult
		);
	}
}


//---------------------------------------------------------------------------//
//	OpenVarLinker
//---------------------------------------------------------------------------//
auto CMGAppMain::OpenVarLinker(
		CLink** ppToLink,
		int iX, int iY,
		int iBehType,
		CMGButton* pLinkButton,
		function<void(CMGWindow::EModalResult)> fnOnModalResult) -> void {

	if (!m_pWindowVarLinker->IsVisible()) {
		m_pWindowVarLinker->Show(
			ppToLink,
			iX, iY,
			iBehType,
			pLinkButton,
			fnOnModalResult
		);
	}
}


//---------------------------------------------------------------------------//
//	OnBtnLinkClick
//---------------------------------------------------------------------------//
auto CMGAppMain::OnBtnLinkClick(CVJVar* pVJVar) -> void {
	UNREFERENCED_PARAMETER(pVJVar);
}


//---------------------------------------------------------------------------//
//	ShowSource
//---------------------------------------------------------------------------//
auto CMGAppMain::ShowSource(CMGSource* pSource) -> void {
	if (m_pActiveSource != nullptr) {
		m_pActiveSource->SetParent(nullptr);
		m_pActiveSource->SetVisible(false);
	}

	m_pActiveSource = pSource;

	if (m_pActiveSource != nullptr) {
		m_pActiveSource->SetParent(m_pPageSource);
		m_pActiveSource->SetVisible(true);
	}

	m_pPageSource->OnResize();
	m_pPageSource->Repaint();

	if (m_pActiveSource != nullptr) {
		m_pDataPages->SetActivePage(m_pPageSource);
	}
}


//---------------------------------------------------------------------------//
//	RemoveActiveSource
//	ebp-> fixes a bug where deleting a source and clicking on a different one
//	would crash because m_pPageSource is bad.
//---------------------------------------------------------------------------//
auto CMGAppMain::RemoveActiveSource() -> void {
	if (m_pActiveSource != nullptr) {
		m_pActiveSource->SetParent(nullptr);
		m_pActiveSource->SetVisible(false);
	}

	m_pActiveSource = nullptr;
}


//---------------------------------------------------------------------------//
//	ShowActiveLayer
//---------------------------------------------------------------------------//
auto CMGAppMain::ShowActiveLayer() -> void {
	// m_iActiveLayer can be -1 here
	ASSERT(m_iActiveLayer < NUM_MASTER_LAYERS);

	if (m_pActiveLayer != nullptr) {
		m_pActiveLayer->SetParent(nullptr);
		m_pActiveLayer->SetVisible(false);
	}

	if (m_iActiveLayer < 0) {
		m_pActiveLayer = nullptr;
	}
	else {
		m_pActiveLayer = m_vLayers[m_iActiveLayer];
	}

	if (m_pActiveLayer != nullptr) {
		m_pActiveLayer->SetParent(m_pPageLayer);
		m_pActiveLayer->SetVisible(true);
	}

	m_pPageLayer->OnResize();
	m_pPageLayer->Repaint();

	m_pDataPages->SetActivePage(m_pPageLayer);
}


//---------------------------------------------------------------------------//
//	SetActiveLayer
//---------------------------------------------------------------------------//
auto CMGAppMain::SetActiveLayer(int iLayer) -> void {
	if (ValidIndex(iLayer, NUM_MASTER_LAYERS)) {
		m_iActiveLayer = iLayer;
	}
	else {
		m_iActiveLayer = -1;
	}
}


//---------------------------------------------------------------------------//
//	ShowBrowse
//---------------------------------------------------------------------------//
auto CMGAppMain::ShowBrowse() -> void {
	m_pDataPages->SetActivePage(m_pPageBrowse);
}


//---------------------------------------------------------------------------//
//	QuitApplication
//---------------------------------------------------------------------------//
auto CMGAppMain::QuitApplication() -> void {
	if (!m_pWindowConfirmQuit->IsVisible()) {
		m_pWindowConfirmQuit->Show(
			(APP_MAIN_WIDTH - m_pWindowConfirmQuit->Width()) >> 1,
			(APP_MAIN_HEIGHT - m_pWindowConfirmQuit->Height()) >> 1,
			bind(&CMGAppMain::OnConfirmQuit, this, _1)
		);
	}
}


//---------------------------------------------------------------------------//
//	OnConfirmQuit
//---------------------------------------------------------------------------//
auto CMGAppMain::OnConfirmQuit(CMGWindow::EModalResult eResult) -> void {
	switch (eResult) {
		case CMGWindow::MR_OK: {
			GLOG(("Goodbye!\n"));
#ifdef MULTITHREADED
			tDraw.join();
#endif
			m_pApplication->Close();
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	DropFiles
//---------------------------------------------------------------------------//
auto CMGAppMain::DropFiles(uint uNumFiles, std::vector<std::string> const& vsFiles) -> void {
	auto pVJLoader = std::make_unique<CVJLoader>(uNumFiles);
	for (uint i = 0; i < uNumFiles; ++i) {
		m_pGridPanel->AddSourceToGrid(CMGSource::Create(vsFiles[i]));
		pVJLoader->Next();
		pVJLoader->Draw(&g_DisplayDevice);
	}
}


//---------------------------------------------------------------------------//
//	LoadSources
//---------------------------------------------------------------------------//
auto CMGAppMain::LoadSources() -> void {
	m_pTreeSources->Clear();

	auto pSourcesDir = NEW CMGTreeItem(nullptr, neon::sDirSources, neon::sDirSources, true);
	LoadSourcesDir(pSourcesDir);

	pSourcesDir->Expand();
	m_pTreeSources->SetRootItem(pSourcesDir);
	m_pTreeSources->Update();
}


//---------------------------------------------------------------------------//
//	LoadSourcesDir
//	ebp-> load files (source layout files)
//---------------------------------------------------------------------------//
auto CMGAppMain::LoadSourcesDir(CMGTreeItem* pParent) -> void {
	ASSERT(pParent);
	filesys::path p(pParent->GetPath());
	filesys::directory_iterator d_end;
	for (filesys::directory_iterator d_iter(p); d_iter != d_end; ++d_iter) {
		// this will be the filename.ext or the name of the directory
		std::string name(d_iter->path().filename().string());
		if (filesys::is_directory(*d_iter) && !filesys::is_empty(*d_iter)) {
			auto pDir = NEW CMGTreeItem(pParent, name, d_iter->path().string(), true);
			LoadSourcesDir(pDir);
		}
		else {
			// get file extension, this is a first measure in determining file contents, need to peek inside to be sure (todo).
			std::string ext(d_iter->path().extension().string());
			std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
			if (ext == ".jpeg" ||
				ext == ".jpg" ||
				ext == ".png" ||
				ext == ".tga" ||
				ext == ".avi" ||
				ext == ".flv" ||
				ext == ".mov" ||
				ext == ".mp4" ||
				ext == ".mpg" ||
				ext == ".mpeg" ||
				ext == ".wmv" ||
				ext == ".webm" ||
				ext == ".gif" ||
				ext == ".nil" ||
				ext == ".nly" ||
				ext == ".nsd") {
				NEW CMGTreeItem(pParent, name, d_iter->path().string(), false);
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	LoadEffectsDir
//	ebp-> load effect files (.nfx files)
//---------------------------------------------------------------------------//
auto CMGAppMain::LoadEffectsDir() -> void {
	m_vEffectsList.clear();
	filesys::path p(neon::sDirEffects);
	filesys::directory_iterator d_end;
	for (filesys::directory_iterator d_iter(p); d_iter != d_end; ++d_iter) {
		if (filesys::is_regular_file(*d_iter)) {
			std::string stem(d_iter->path().stem().string());
			std::string ext(d_iter->path().extension().string());
			std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
			if (ext == ".nfx") {
				m_vEffectsList.push_back(stem);
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	RefreshDirectories
//---------------------------------------------------------------------------//
auto CMGAppMain::RefreshDirectories() -> void {
	LoadSources();
	LoadEffectsDir();
}


//---------------------------------------------------------------------------//
//	CreateBase
//---------------------------------------------------------------------------//
auto CMGAppMain::CreateBase() -> void {
	// ebp-> todo: redo the size of UI layout so the gridpanel is more evenly sized.
	auto pRootPanel = NEW CMGPanel(nullptr, 0, 0, APP_MAIN_WIDTH, APP_MAIN_HEIGHT, MGALIGN_CLIENT);
	m_pTopPanel = NEW CMGGroupBox(pRootPanel, 0, 0, 0, 30, MGALIGN_TOP);

	auto pDataPanel = NEW CMGPanel(pRootPanel, 0, 0, 306, 0, MGALIGN_LEFT);
	m_pGridPanel = NEW CMGGridPanel(pRootPanel, NUM_GRID_PAGES);

	auto pBeatMgrPanel = NEW CMGGroupBox(pDataPanel, 0, 0, 0, 88, MGALIGN_TOP);
	m_pBeatManager = NEW CMGBeatManager(pBeatMgrPanel);
	m_pDataPages = NEW CMGPageControl(pDataPanel, 0, 0, 0, 0, MGALIGN_CLIENT);

	m_pApplication->SetRootItem(pRootPanel);
}


//---------------------------------------------------------------------------//
//	CreatePanelData
//	ebp-> Panel 'Datos' which has the tabs for Browse, Source, Layer, Options
//---------------------------------------------------------------------------//
auto CMGAppMain::CreatePanelData() -> void {
	m_pPageBrowse = NEW CMGPage(m_pDataPages, "Browse");
	m_pPageSource = NEW CMGPage(m_pDataPages, "Source");
	m_pPageLayer = NEW CMGPage(m_pDataPages, "Layer");
	m_pPageOptions = NEW CMGPage(m_pDataPages, "Options");

	CreateBrowseTab();
	CreateSourceTab();
	CreateLayerTab();
	CreateOptionsTab();
}


//---------------------------------------------------------------------------//
//	CreatePanelTop
//	ebp-> Panel at the top of the application, load/save and close buttons.
//---------------------------------------------------------------------------//
auto CMGAppMain::CreatePanelTop() -> void {
	auto pPanelR = NEW CMGPanel(m_pTopPanel, 0, 0, 25, 0, MGALIGN_LEFT);
	NEW CMGButton(pPanelR, 3, 3, CMGButton::SMALL, "X", bind(&CMGAppMain::QuitApplication, this));
	auto pPanel = NEW CMGPanelWin(m_pTopPanel, 0, 0, 0, 0, MGALIGN_CLIENT);
	NEW CMGLabel(pPanel, 0, 0, MGALIGN_RIGHT, FONT_NORMAL, VERSION_LONG);

	NEW CMGButton(pPanel, 1, 1, CMGButton::NORMAL, "New Project", bind(&CMGAppMain::NewProject, this));
	NEW CMGButton(pPanel, 1 + 75 + 2, 1, CMGButton::NORMAL, "Load Project", bind(&CMGAppMain::LoadProject, this));
	NEW CMGButton(pPanel, 1 + 150 + 4, 1, CMGButton::NORMAL, "Save Project", bind(&CMGAppMain::SaveProject, this, false));
	NEW CMGButton(pPanel, 1 + 225 + 6, 1, CMGButton::NORMAL, "Save As...", bind(&CMGAppMain::SaveProject, this, true));
	m_pProjectLabel = NEW CMGLabel(pPanel, 1 + 300 + 12, 0, MGALIGN_NONE, FONT_NORMAL, "");
}


//---------------------------------------------------------------------------//
//	CreateWindows
//---------------------------------------------------------------------------//
auto CMGAppMain::CreateWindows() -> void {
	m_pWindowNewSource = NEW CMGWindowNewSource;
	m_pWindowConfirmDelSource = NEW CMGWindowConfirm("Confirm Delete", "Delete Source?");
	m_pWindowVarLinker = NEW CMGWindowVarLinker;
	m_pWindowConfirmQuit = NEW CMGWindowConfirm("Confirm Quit", "Quit Neon?");
}


//---------------------------------------------------------------------------//
//	CreateBrowseTab
//---------------------------------------------------------------------------//
auto CMGAppMain::CreateBrowseTab() -> void {
	//m_pPanelSourceSup = NEW CMGPanel(m_pPageBrowse, 0, 0, 0, 26, MGALIGN_TOP);
	//NEW CMGButton(m_pPanelSourceSup, 1, 1, CMGButton::NORMAL, "New", bind(&CMGAppMain::NewSource, this));
	//NEW CMGButton(m_pPanelSourceSup, 78, 1, CMGButton::NORMAL, "Delete", bind(&CMGAppMain::DelSource, this));
	//NEW CMGButton(m_pPanelSourceSup, 274, 1, CMGButton::REFRESH, "", bind(&CMGAppMain::LoadSources, this));
	m_pTreeSources = NEW CMGTreeView(m_pPageBrowse, 0, 0, 0, 0, MGALIGN_CLIENT);
	NEW CMGButton(m_pTreeSources, 274, 3, CMGButton::REFRESH, "", bind(&CMGAppMain::LoadSources, this));
}


//---------------------------------------------------------------------------//
//	CreateSourceTab
//---------------------------------------------------------------------------//
auto CMGAppMain::CreateSourceTab() -> void {
	// this tab is filled in when user selects a source in the grid
	// nothing to do here (yet).
}


//---------------------------------------------------------------------------//
//	CreateLayerTab
//---------------------------------------------------------------------------//
auto CMGAppMain::CreateLayerTab() -> void {
	for (int i = 0; i < NUM_MASTER_LAYERS; ++i) {
		// ebp-> create these with NULL parent at first.  When we activate and
		// make visible, we set the parent to be the Layer page.
		m_vLayers[i] = NEW CMGLayer(nullptr, i, m_pVJController->GetLayer(i), m_pGridPanel);
		m_vLayers[i]->SetVisible(false);
	}
}


//---------------------------------------------------------------------------//
//	CreateOptionsTab
//---------------------------------------------------------------------------//
auto CMGAppMain::CreateOptionsTab() -> void {
	// ebp-> adding a checkbox to the options page for switching FPS display on/off
	int x = 10;
	int x2 = 75;
	int y = 10;

	CMGControl* pCtrl = NEW CMGCheckBox(m_pPageOptions, x, y, "Show FPS", true, m_pVJController->GetDrawDebug(), bind(&CVJController::ToggleDrawDebug, m_pVJController));
	y += 8;
	m_pBtnResetWindows = NEW CMGButton(m_pPageOptions, 140, y, CMGButton::NORMAL, "Reset Output", bind(&CVJController::ResetDisplayWindow, m_pVJController));
	y += 8;
	m_pCHFullScreen = NEW CMGCheckBox(m_pPageOptions, x, y, "Fullscreen", true, false, NULL, bind(&CVJController::SetFullScreen, m_pVJController, _1));

	y += 30;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "MIDI Device");
	m_pCBMidiDevices = NEW CMGComboBox(m_pPageOptions, x2, y - 2, 220, 20, true, bind(&CMGAppMain::OnChangeMidiDevice, this));
	y += 30;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "MIDI Channel");
	m_pCBMidiChannel = NEW CMGComboBox(m_pPageOptions, x2, y - 2, 36, 20, true, bind(&CMGAppMain::OnChangeMidiChannel, this));
	m_pCBMidiChannel->Add("1");
	m_pCBMidiChannel->Add("2");
	m_pCBMidiChannel->Add("3");
	m_pCBMidiChannel->Add("4");
	m_pCBMidiChannel->Add("5");
	m_pCBMidiChannel->Add("6");
	m_pCBMidiChannel->Add("7");
	m_pCBMidiChannel->Add("8");
	m_pCBMidiChannel->Add("9");
	m_pCBMidiChannel->Add("10");
	m_pCBMidiChannel->Add("11");
	m_pCBMidiChannel->Add("12");
	m_pCBMidiChannel->Add("13");
	m_pCBMidiChannel->Add("14");
	m_pCBMidiChannel->Add("15");
	m_pCBMidiChannel->Add("16");

	y += 30;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "Clear Red");
	m_pTBClearR = NEW CMGTrackBar(m_pPageOptions, x2, y, 200, false, bind(&CMGAppMain::OnChangeClearColor, this));
	y += 16;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "Clear Green");
	m_pTBClearG = NEW CMGTrackBar(m_pPageOptions, x2, y, 200, false, bind(&CMGAppMain::OnChangeClearColor, this));
	y += 16;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "Clear Blue");
	m_pTBClearB = NEW CMGTrackBar(m_pPageOptions, x2, y, 200, false, bind(&CMGAppMain::OnChangeClearColor, this));

	y += 30;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "MASTER OUTPUT");
	y += 20;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "Alpha");
	m_pTBMasterAlpha = NEW CMGTrackBar(m_pPageOptions, x2, y, 200, false, bind(&CMGAppMain::OnChangeMasterAlpha, this));

	m_pBtnLinkMasterAlpha = NEW CMGButton(m_pPageOptions, 276, y, CMGButton::LINK, "L", bind(&CMGAppMain::OnLinkMasterAlpha, this));

	y += 20;
	pCtrl = NEW CMGLabel(m_pPageOptions, x, y, MGALIGN_NONE, FONT_NORMAL, "Bypass");
	m_pCHMasterBypass = NEW CMGCheckBox(m_pPageOptions, x2, y, "", true, false, bind(&CMGAppMain::OnChangeMasterBypass, this));

	m_pBtnLinkMasterBypass = NEW CMGButton(m_pPageOptions, 276, y, CMGButton::LINK, "L", bind(&CMGAppMain::OnLinkMasterBypass, this));

	m_pTBMasterAlpha->SetPos(1.f);
	// ebp-> todo: make a button to reset the window positions.
}


//---------------------------------------------------------------------------//
//	RefreshMidiDevices
//---------------------------------------------------------------------------//
auto CMGAppMain::RefreshMidiDevices() -> void {
	std::vector<std::string> vsDevices;
	CMidiDevice::EnumDevices(vsDevices);

	m_pCBMidiDevices->Clear();
	if (vsDevices.size() > 0) {
		auto pAppOpts = CServiceLocator<IAppOptionsService>::GetService();
		std::string sPrefferedMidiDevice = pAppOpts->Get("neonconfig.midi.device", CMidiDevice::DEVICE_UNKNOWN);
		int iPreferredMidiChannel = pAppOpts->Get("neonconfig.midi.channel", 0);
		int iMidiIndex = -1;
		for (size_t i = 0; i < vsDevices.size(); ++i) {
			m_pCBMidiDevices->Add(vsDevices[i]);
			if (vsDevices[i] == sPrefferedMidiDevice) {
				iMidiIndex = static_cast<int>(i);
				m_pCBMidiDevices->Select(iMidiIndex);
				m_pCBMidiChannel->Select(iPreferredMidiChannel);
			}
		}

		if (iMidiIndex < 0) {
			m_pCBMidiDevices->Select(0);	// this will initialize the midi device.
			m_pCBMidiChannel->Select(0);
		}

	} else {
		m_pCBMidiDevices->Add(CMidiDevice::DEVICE_UNKNOWN);
		m_pCBMidiDevices->Select(0);
		m_pCBMidiDevices->SetEnabled(false);
		m_pCBMidiChannel->SetEnabled(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChangeMidiDevice
//---------------------------------------------------------------------------//
auto CMGAppMain::OnChangeMidiDevice() -> void {
	int iDevice = m_pCBMidiDevices->GetSelected();
	if (m_pCBMidiDevices->GetText() != CMidiDevice::DEVICE_UNKNOWN) {
		CServiceLocator<IMidiDeviceService>::GetService()->SelectDevice(iDevice);
		//m_pMidiDevice->SelectDevice(iDevice);
		GLOG(("MIDI Device [ %s ] selected.\n", m_pCBMidiDevices->GetText().c_str()));
		CServiceLocator<IAppOptionsService>::GetService()->Set("neonconfig.midi.device", m_pCBMidiDevices->GetText());
	}
}


//---------------------------------------------------------------------------//
//	OnChangeMidiChannel
//---------------------------------------------------------------------------//
auto CMGAppMain::OnChangeMidiChannel() -> void {
	int iChannel = m_pCBMidiChannel->GetSelected();
	CServiceLocator<IMidiDeviceService>::GetService()->SelectChannel(iChannel);
	GLOG(("MIDI Channel [ %d ] selected.\n", iChannel + 1));
	CServiceLocator<IAppOptionsService>::GetService()->Set("neonconfig.midi.channel", iChannel);
}


//---------------------------------------------------------------------------//
//	OnChangeClearColor
//---------------------------------------------------------------------------//
auto CMGAppMain::OnChangeClearColor() -> void {
	uint uColor = HARD_COLOR_ARGB(
		255,
		static_cast<int>(m_pTBClearR->GetPos() * 255.f),
		static_cast<int>(m_pTBClearG->GetPos() * 255.f),
		static_cast<int>(m_pTBClearB->GetPos() * 255.f)
	);
	m_pVJController->SetClearColor(uColor);
}


//---------------------------------------------------------------------------//
//	OnChangeMasterAlpha
//---------------------------------------------------------------------------//
auto CMGAppMain::OnChangeMasterAlpha() -> void {
	m_pVJController->SetMasterAlpha(m_pTBMasterAlpha->GetPos());
}


//---------------------------------------------------------------------------//
//	OnChangeMasterBypass
//---------------------------------------------------------------------------//
auto CMGAppMain::OnChangeMasterBypass() -> void {
	m_pVJController->SetMasterBypass(m_pCHMasterBypass->Checked());
}


//---------------------------------------------------------------------------//
//	OnLinkMasterAlpha
//---------------------------------------------------------------------------//
auto CMGAppMain::OnLinkMasterAlpha() -> void {
	OpenVarLinker(
		&m_pLinkMasterAlpha,
		m_pBtnLinkMasterAlpha->Left(),
		m_pBtnLinkMasterAlpha->Top(),
		ILinkBehaviour::TRACK_BAR,
		false,
		m_pTBMasterAlpha,
		m_pBtnLinkMasterAlpha,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnLinkMasterBypass
//---------------------------------------------------------------------------//
auto CMGAppMain::OnLinkMasterBypass() -> void {
	OpenVarLinker(
		&m_pLinkMasterBypass,
		m_pBtnLinkMasterBypass->Left(),
		m_pBtnLinkMasterBypass->Top(),
		ILinkBehaviour::CHECK_BOX,
		false,
		m_pCHMasterBypass,
		m_pBtnLinkMasterBypass,
		NULL
	);
}
