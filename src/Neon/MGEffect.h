//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGEFFECT_H
#define NEON_MGEFFECT_H

#include "Controls/MGControl.h"

class CMGSource;
class CMGEffects;
class CMGPanelVars;

//---------------------------------------------------------------------------//
//	Class CMGEffect
//---------------------------------------------------------------------------//
class CMGEffect : public CMGControl {
public:
	CMGEffect(CMGControl* pParent, CMGEffects* pEffects, CMGSource* pSource, int x, int y, int iSlot);
	~CMGEffect() override;

	virtual auto OnClickEnable() -> void;
	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	auto OnDragOver(CMGControl* pSource, int iX, int iY, bool& rAccept) -> void override;
	auto OnDragDrop(CMGControl* pSource, int iX, int iY) -> void override;

	virtual auto IsLoaded() const -> bool { return !m_sEffectName.empty(); }
	virtual auto Load(std::string const& sEffectName) -> bool;
	virtual auto Unload() -> void;
	virtual auto Show() -> void;

	auto GetName() const -> std::string const& { return m_sEffectName; }
	auto GetDisplayName() const -> std::string const& { return m_sEffectDisplayName; }
	auto Update() -> void;
	auto Load(CNodeFile::CNode* pNode) -> void;
	auto Save(CNodeFile* pFile) -> void;
	auto SwapWith(CMGEffect* pOther) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGEffect\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

	int m_iEffectSlot;
	uint m_uEffectID;
	std::string m_sEffectName;
	std::string m_sEffectDisplayName;
	CMGCheckBox* m_pCBEnabled;
	CMGSource* m_pSource;
	CMGEffects* m_pSourceEffects;
	CMGPanelVars* m_pVJEffectVars;
};

#endif//NEON_MGEFFECT_H
