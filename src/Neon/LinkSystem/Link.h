//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINK_H
#define NEON_LINK_H

#include "LinkDevice.h"
#include "LinkBehaviour.h"

class CLink {
public:
	CLink(ILinkDevice* pDevice, ILinkBehaviour* pBehaviour);
	~CLink();

	auto Evaluate() -> bool;

	auto ValueCheckBox() const -> bool;
	auto ValueComboBox() const -> int;
	auto ValueTrackBar() const -> float;
	auto ValueButton() const -> bool;

	auto Device() const -> ILinkDevice* { return m_pDevice; }
	auto Behaviour() const -> ILinkBehaviour* { return m_pBehaviour; }

	auto Load(CNodeFile::CNode* pNode) -> void;
	auto Save(CNodeFile* pFile, std::string const& sNodeName) -> void;
	auto Clone() const -> CLink*;

protected:
	ILinkDevice* m_pDevice;
	ILinkBehaviour* m_pBehaviour;
};

#endif//NEON_LINK_H
