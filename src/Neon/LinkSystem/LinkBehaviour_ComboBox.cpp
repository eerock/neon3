//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice.h"
#include "LinkBehaviour_ComboBox.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkBehaviour_ComboBox::~CLinkBehaviour_ComboBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkBehaviour_ComboBox\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkBehaviour_ComboBox::Evaluate(ILinkDevice* pDevice) -> bool {
	bool bActive = false;
	if (pDevice->Evaluate()) {
		bActive = pDevice->Value() == 1;

		if (bActive) {
			switch (m_iAction) {
				case NEXT: {
					m_iValue = (++m_iValue % m_nNumItems);
					break;
				}
				case PREV: {
					if (--m_iValue < 0) {
						m_iValue = m_nNumItems - 1;
					}
					break;
				}
				case RAND: {
					if (m_nNumItems > 1) {
						int iValue;
						do {
							iValue = rand() % m_nNumItems;
						} while(m_iValue == iValue);
						m_iValue = iValue;
					}
					break;
				}
				case SET: {
					m_iValue = m_iSetValue;
					break;
				}
				default: {
					return false;
				}
			}
		}
	}
	return bActive;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkBehaviour_ComboBox::Load(CNodeFile::CNode* pNode) -> void {
	m_iAction = pNode->AsInt("b_action");
	m_nNumItems = pNode->AsInt("b_numitems");
	m_iSetValue = pNode->AsInt("b_setvalue");
	m_iValue = pNode->AsInt("b_value");
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkBehaviour_ComboBox::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("b_action", "", m_iAction);
	pFile->WriteNode("b_numitems", "", m_nNumItems);
	pFile->WriteNode("b_setvalue", "", m_iSetValue);
	pFile->WriteNode("b_value", "", m_iValue);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkBehaviour_ComboBox::Clone() const -> ILinkBehaviour* {
	return NEW CLinkBehaviour_ComboBox(m_iAction, m_iValue, m_nNumItems, m_iSetValue);
}
