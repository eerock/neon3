//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKDEVICE_WAVE_H
#define NEON_LINKDEVICE_WAVE_H

#include "LinkDevice.h"

// ebp-> same as beat, use a publisher/subscriber pattern to get the FFT channel data.

//---------------------------------------------------------------------------//
//	Class CLinkDevice_Wave
//---------------------------------------------------------------------------//
class CLinkDevice_Wave : public ILinkDevice {
public:
	CLinkDevice_Wave()
		: m_iChan(0)
		, m_fGain(1.f)
		, m_fValue(0.f)
	{}
	~CLinkDevice_Wave() override;

	auto Type() const -> int override {
		return ILinkDevice::WAVE;
	}
	auto Value() const -> float override {
		return m_fValue;
	}
	auto Evaluate() -> bool override;

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkDevice* override;

	auto Set(int iChan, float fGain) -> void;
	auto Get(int& iChan, float& fGain) -> void;

protected:
	int m_iChan;
	float m_fGain;
	float m_fValue;
};

#endif//NEON_LINKDEVICE_WAVE_H
