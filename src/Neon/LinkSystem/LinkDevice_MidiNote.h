//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKDEVICE_MIDINOTE_H
#define NEON_LINKDEVICE_MIDINOTE_H

#include "LinkDevice.h"


//---------------------------------------------------------------------------//
//	Class CLinkDevice_MidiNote
//---------------------------------------------------------------------------//
class CLinkDevice_MidiNote : public ILinkDevice {
public:
	CLinkDevice_MidiNote()
		: m_cNote(0)
		, m_bPressed(false)
		, m_fPressure(0.f)
	{}
	~CLinkDevice_MidiNote() override;

	auto Type() const -> int override {
		return ILinkDevice::MIDI_NOTE;
	}
	auto Value() const -> float override {
		return m_bPressed ? /*m_fPressure*/ 1.f : 0.f;
	}
	auto Evaluate() -> bool override;

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkDevice* override;

	auto Set(uchar cNote) -> void;
	auto Get(uchar& cNote) -> void;

protected:
	uchar m_cNote;
	bool m_bPressed;
	float m_fPressure;
};

#endif//NEON_LINKDEVICE_MIDINOTE_H
