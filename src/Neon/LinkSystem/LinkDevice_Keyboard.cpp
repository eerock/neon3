//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice_Keyboard.h"
#include "KeyboardDevice.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkDevice_Keyboard::~CLinkDevice_Keyboard() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkDevice_Keyboard\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkDevice_Keyboard::Evaluate() -> bool {
	bool bRes = false;
	bool bPressed = CKeyboardDevice::KeyPressed(m_cChar);
	if (bPressed) {
		if (!m_bPressed) {
			bRes = true;
		}
	}
	m_bPressed = bPressed;
	return bRes;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkDevice_Keyboard::Load(CNodeFile::CNode* pNode) -> void {
	m_cChar = static_cast<char>(pNode->AsInt("d_char"));
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkDevice_Keyboard::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("d_char", "", m_cChar);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkDevice_Keyboard::Clone() const -> ILinkDevice* {
	auto pDevice = NEW CLinkDevice_Keyboard;
	*pDevice = *this;
	return pDevice;
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CLinkDevice_Keyboard::Set(char c) -> void {
	m_cChar = c;
	m_bPressed = false;
}


//---------------------------------------------------------------------------//
//	Get
//---------------------------------------------------------------------------//
auto CLinkDevice_Keyboard::Get(char& c) -> void {
	c = m_cChar;
}
