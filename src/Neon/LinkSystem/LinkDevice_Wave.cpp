//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "VJController.h"
#include "LinkDevice_Wave.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkDevice_Wave::~CLinkDevice_Wave() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkDevice_Wave\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkDevice_Wave::Evaluate() -> bool {
	float fValue = g_pAppMain->VJController()->GetFFTChannel(m_iChan) * m_fGain;
	bool bRes = (fValue != m_fValue);
	m_fValue = fValue;
	return bRes;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkDevice_Wave::Load(CNodeFile::CNode* pNode) -> void {
	m_iChan = pNode->AsInt("d_chan");
	m_fGain = pNode->AsFloat("d_gain");
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkDevice_Wave::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("d_chan", "", m_iChan);
	pFile->WriteNode("d_gain", "", m_fGain);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkDevice_Wave::Clone() const -> ILinkDevice* {
	auto pDevice = NEW CLinkDevice_Wave;
	ASSERT(pDevice);
	*pDevice = *this;
	return pDevice;
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CLinkDevice_Wave::Set(int iChan, float fGain) -> void {
	m_iChan = iChan;
	m_fGain = fGain;
}


//---------------------------------------------------------------------------//
//	Get
//---------------------------------------------------------------------------//
auto CLinkDevice_Wave::Get(int& iChan, float& fGain) -> void {
	iChan = m_iChan;
	fGain = m_fGain;
}
