//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice.h"
#include "LinkBehaviour_Button.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkBehaviour_Button::~CLinkBehaviour_Button() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkBehaviour_Button\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkBehaviour_Button::Evaluate(ILinkDevice* pDevice) -> bool {
	if (pDevice->Evaluate()) {
		bool bActive = pDevice->Value() >= 0.5f;
		switch (m_iAction) {
			case ACTIVATE: {
				m_bPressed = bActive;
				return true;
			}
			default: {
				break;
			}
		}
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkBehaviour_Button::Load(CNodeFile::CNode* pNode) -> void {
	m_iAction = pNode->AsInt("b_action");
	m_bPressed = false;
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkBehaviour_Button::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("b_action", "", m_iAction);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkBehaviour_Button::Clone() const -> ILinkBehaviour* {
	return NEW CLinkBehaviour_Button(m_iAction);
}
