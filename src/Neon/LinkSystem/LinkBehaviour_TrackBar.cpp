//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice.h"
#include "LinkBehaviour_TrackBar.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkBehaviour_TrackBar::~CLinkBehaviour_TrackBar() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkBehaviour_TrackBar\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkBehaviour_TrackBar::Evaluate(ILinkDevice* pDevice) -> bool {
	if (pDevice->Evaluate()) {
		bool bActive = pDevice->Value() == 1;
		switch (m_iAction) {
			case DEVICE_VALUE: {
				m_fValue = pDevice->Value();
				break;
			}
			case INV_DEVICE_VALUE: {
				m_fValue = 1.f - pDevice->Value();
				break;
			}
			case SET: {
				if (bActive) {
					m_fValue = m_fSetValue;
				}
				break;
			}
			case ADD: {
				if (bActive) {
					m_fValue = Clamp(m_fValue + m_fAddValue, 0.f, 1.f);
				}
				break;
			}
			case RAND: {
				m_fValue = (rand() & 1023) * 1.f / 1023.f;
				break;
			}
			case FADE: {
				m_fValue = m_fFadeFr;
				m_fFadeIni = g_pAppMain->Timer()->Get();
				m_bFadeDone = false;
				break;
			}
			default: {
				break;
			}
		}
		return true;
	}

	// Evaluate fading
	if (m_iAction == FADE && !m_bFadeDone) {
		float fTime = g_pAppMain->Timer()->Get() - m_fFadeIni;
		if (fTime > m_fFadeLength) {
			m_bFadeDone = true;
			m_fValue = m_fFadeTo;
		} else {
			m_fValue = m_fFadeFr + fTime * (m_fFadeTo - m_fFadeFr) / m_fFadeLength;
		}
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkBehaviour_TrackBar::Load(CNodeFile::CNode* pNode) -> void {
	m_fFadeIni = 0.f;
	m_iAction = pNode->AsInt("b_action");
	m_fFadeFr = pNode->AsFloat("b_fadefr");
	m_fFadeTo = pNode->AsFloat("b_fadeto");
	m_fFadeLength = pNode->AsFloat("b_fadelength");
	m_bFadeDone = pNode->AsBool("b_fadedone");
	m_fSetValue = pNode->AsFloat("b_setvalue");
	m_fAddValue = pNode->AsFloat("b_addvalue");
	m_fValue = pNode->AsFloat("b_value");
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkBehaviour_TrackBar::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("b_action", "", m_iAction);
	pFile->WriteNode("b_fadefr", "", m_fFadeFr);
	pFile->WriteNode("b_fadeto", "", m_fFadeTo);
	pFile->WriteNode("b_fadelength", "", m_fFadeLength);
	pFile->WriteNode("b_fadedone", "", m_bFadeDone);
	pFile->WriteNode("b_setvalue", "", m_fSetValue);
	pFile->WriteNode("b_addvalue", "", m_fAddValue);
	pFile->WriteNode("b_value", "", m_fValue);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkBehaviour_TrackBar::Clone() const -> ILinkBehaviour* {
	return NEW CLinkBehaviour_TrackBar(m_iAction, m_fValue);
}
