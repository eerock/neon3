//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice_MidiNote.h"
#include "MidiDevice.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkDevice_MidiNote::~CLinkDevice_MidiNote() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkDevice_MidiNote\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiNote::Evaluate() -> bool {
	auto pMidi = CServiceLocator<IMidiDeviceService>::GetService();
	if (pMidi) {
		// ebp-> todo: this needs an overhaul, basically interpret MIDI note on/off messages.
		auto tMidiKey = pMidi->GetMIDIKey(m_cNote);
		bool bRes = false;
		if (tMidiKey.cMsg >> 4 == 0x9) {	// note on
			bRes = !m_bPressed;
			m_bPressed = true;
			if (bRes) {
				m_fPressure = static_cast<float>(tMidiKey.cValue) / 127.f;
				GLOG(("Note On!\n"));
			}
		}

		if (tMidiKey.cMsg >> 4 == 0x8) {	// note off
			//bRes = m_bPressed;
			m_bPressed = false;
			//if (bRes)
			//{
			//	m_fPressure = static_cast<float>(tMidiKey.cValue) / 127.f;
			//	GLOG(("Note Off!\n"));
			//}
		}

		return bRes;

	// this method will capture MIDI buttons that have on/off values of 127/0.
	// it will not interpret my OP-1 keyboard notes as presses because the note off message has
	// a non-zero velocity.
		//m_fPressure = static_cast<float>(tMidiKey.cValue) / 127.f;
		//bool bPressed = tMidiKey.cValue > 0;
		//bool bRes = (bPressed != m_bPressed);
		//m_bPressed = bPressed;

	// this will capture my OP-1 notes where note on is at velocity 100, and note off is at 64.
	// the problem is that it interprets both note on and off as pressed keys.
		//float fPressure = static_cast<float>(tMidiKey.cValue) / 127.f;
		//bool bRes = (m_fPressure != fPressure);
		//m_fPressure = fPressure;
		//m_bPressed = tMidiKey.cValue > 0;

	// Okay, third version.  After looking at the other LinkDevices, their Evaluate functions
	// return true if the core value we're interested in changes.  That's it.  In the case of Midi Note
	// and Midi Slider they can be considered equivalent.  So we ignore bPressed.
		//float fPressure = static_cast<float>(tMidiKey.cValue) / 127.f;
		//bool bRes = (m_fPressure != fPressure);
		//m_fPressure = fPressure;

		//if (bRes)
		//{
		//	GLOG(("MIDI NOTE PRESSED, pressure = %f!\n", m_fPressure));
		//}
		//return bRes;
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiNote::Load(CNodeFile::CNode* pNode) -> void {
	m_cNote = static_cast<uchar>(pNode->AsInt("d_note"));
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiNote::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("d_note", "", m_cNote);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiNote::Clone() const -> ILinkDevice* {
	auto pDevice = NEW CLinkDevice_MidiNote;
	ASSERT(pDevice);
	*pDevice = *this;
	return pDevice;
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiNote::Set(uchar cNote) -> void {
	m_cNote = cNote;
	m_bPressed = false;
	m_fPressure = 0.f;
}


//---------------------------------------------------------------------------//
//	Get
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiNote::Get(uchar& cNote) -> void {
	cNote = m_cNote;
}
