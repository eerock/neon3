//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKBEHAVIOUR_BUTTON_H
#define NEON_LINKBEHAVIOUR_BUTTON_H

#include "LinkBehaviour.h"


//---------------------------------------------------------------------------//
//	Class CLinkBehaviour_Button
//---------------------------------------------------------------------------//
class CLinkBehaviour_Button : public ILinkBehaviour {
public:
	enum {
		ACTIVATE,
	};

	CLinkBehaviour_Button(int eAction)
		: m_iAction(eAction)
		, m_bPressed(false)
	{}
	~CLinkBehaviour_Button() override;

	auto Type() const -> int override {
		return BUTTON;
	}
	auto Action() const -> int override {
		return m_iAction;
	}
	auto Evaluate(ILinkDevice* pDevice) -> bool override;
	auto Value() const -> float override {
		return m_bPressed ? 1.f : 0.f;
	}

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkBehaviour* override;

protected:
	int m_iAction;
	bool m_bPressed;
};

#endif//NEON_LINKBEHAVIOUR_BUTTON_H
