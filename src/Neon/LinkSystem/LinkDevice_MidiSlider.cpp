//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice_MidiSlider.h"
#include "MidiDevice.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkDevice_MidiSlider::~CLinkDevice_MidiSlider() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkDevice_MidiSlider\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiSlider::Evaluate() -> bool {
	auto pMidi = CServiceLocator<IMidiDeviceService>::GetService();
	if (pMidi) {
		// ebp-> todo: this needs an overhaul, basically interpret MIDI note on/off messages.
		auto tMidiKey = pMidi->GetMIDIKey(m_cNote);

		float fValue = static_cast<float>(tMidiKey.cValue) / 127.f;
		bool bRes = (m_fValue != fValue);
		m_fValue = fValue;

		if (bRes) {
			GLOG(("MIDI SLIDER CHANGED, value = %f!\n", m_fValue));
		} 
		return bRes;
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiSlider::Load(CNodeFile::CNode* pNode) -> void {
	m_cNote = static_cast<uchar>(pNode->AsInt("d_note"));
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiSlider::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("d_note", "", m_cNote);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiSlider::Clone() const -> ILinkDevice* {
	auto pDevice = NEW CLinkDevice_MidiSlider;
	ASSERT(pDevice);
	*pDevice = *this;
	return pDevice;
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiSlider::Set(uchar cNote) -> void {
	m_cNote = cNote;
	m_fValue = 0.f;
}


//---------------------------------------------------------------------------//
//	Get
//---------------------------------------------------------------------------//
auto CLinkDevice_MidiSlider::Get(uchar& cNote) -> void {
	cNote = m_cNote;
}
