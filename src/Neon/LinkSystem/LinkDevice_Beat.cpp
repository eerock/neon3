//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "MGBeatManager.h"
#include "LinkDevice_Beat.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkDevice_Beat::~CLinkDevice_Beat() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkDevice_Beat\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkDevice_Beat::Evaluate() -> bool {
	//GLOG(("Beat Evaluate - beat:%d, offset:%d\n", m_iBeat, m_iOffset));
	// ebp-> todo: remove dependency on g_pAppMain.  Either pass in a pointer
	// to the BeatManager or refactor to a publisher/subscriber pattern.
	bool bBeat = g_pAppMain->BeatManager()->IsBeat(m_iBeat, m_iOffset);
	bool bRes = (bBeat != m_bIsBeat);
	m_bIsBeat = bBeat;
	return bRes;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkDevice_Beat::Load(CNodeFile::CNode* pNode) -> void {
	m_iBeat = pNode->AsInt("d_beat");
	m_iOffset = pNode->AsInt("d_offs");
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkDevice_Beat::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("d_offs", "", m_iOffset);
	pFile->WriteNode("d_beat", "", m_iBeat);
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkDevice_Beat::Clone() const -> ILinkDevice* {
	auto pDevice = NEW CLinkDevice_Beat;
	ASSERT(pDevice);
	*pDevice = *this;
	return pDevice;
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CLinkDevice_Beat::Set(int iBeat, int iOffset) -> void {
	m_iBeat = iBeat;
	m_iOffset = iOffset;
}


//---------------------------------------------------------------------------//
//	Get
//---------------------------------------------------------------------------//
auto CLinkDevice_Beat::Get(int& iBeat, int& iOffset) -> void {
	iBeat = m_iBeat;
	iOffset = m_iOffset;
}
