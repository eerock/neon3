//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKBEHAVIOUR_H
#define NEON_LINKBEHAVIOUR_H

class ILinkBehaviour {
public:
	enum ELinkBehaviourType {
		INVALID = -1,
		CHECK_BOX = 0,
		COMBO_BOX,
		TRACK_BAR,
		BUTTON,
		MAX_BEHAVIOUR_TYPES,
	};

public:
	virtual ~ILinkBehaviour() {}

	virtual auto Type() const -> int = 0;
	virtual auto Action() const -> int = 0;
	virtual auto Value() const -> float = 0;
	virtual auto Evaluate(ILinkDevice* pDevice) -> bool = 0;

	virtual auto Load(CNodeFile::CNode* pNode) -> void = 0;
	virtual auto Save(CNodeFile* pFile) -> void = 0;
	virtual auto Clone() const -> ILinkBehaviour* = 0;
};

#endif//NEON_LINKBEHAVIOUR_H
