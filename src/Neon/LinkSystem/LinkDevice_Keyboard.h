//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKDEVICE_KEYBOARD_H
#define NEON_LINKDEVICE_KEYBOARD_H

#include "LinkDevice.h"


//---------------------------------------------------------------------------//
//	Class CLinkDevice_Keyboard
//---------------------------------------------------------------------------//
class CLinkDevice_Keyboard : public ILinkDevice {
public:
	CLinkDevice_Keyboard()
		: m_cChar(0)
		, m_bPressed(false)
	{}
	~CLinkDevice_Keyboard() override;

	auto Type() const -> int override {
		return ILinkDevice::KEYBOARD;
	}
	auto Value() const -> float override {
		return m_bPressed ? 1.f : 0.f;
	}
	auto Evaluate() -> bool override;

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkDevice* override;

	auto Set(char c) -> void;
	auto Get(char& c) -> void;

protected:
	char m_cChar;
	bool m_bPressed;
};

#endif//NEON_LINKDEVICE_KEYBOARD_H
