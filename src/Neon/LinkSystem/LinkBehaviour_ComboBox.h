//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKBEHAVIOUR_COMBOBOX_H
#define NEON_LINKBEHAVIOUR_COMBOBOX_H

#include "LinkBehaviour.h"


//---------------------------------------------------------------------------//
//	Class CLinkBehaviour_ComboBox
//---------------------------------------------------------------------------//
class CLinkBehaviour_ComboBox : public ILinkBehaviour {
public:
	enum {
		NEXT,
		PREV,
		RAND,
		SET,
	};

public:
	CLinkBehaviour_ComboBox(int eAction, int iValue, int iNumItems, int iSetValue)
		: m_iAction(eAction)
		, m_iValue(iValue)
		, m_nNumItems(iNumItems)
		, m_iSetValue(iSetValue)
	{}
	~CLinkBehaviour_ComboBox() override;

	auto Type() const -> int override {
		return COMBO_BOX;
	}
	auto Action() const -> int override {
		return m_iAction;
	}
	auto Value() const -> float override {
		return (float)m_iValue;
	}
	auto Evaluate(ILinkDevice* pDevice) -> bool override;

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkBehaviour* override;

	auto GetSetValue () const -> int { return m_iSetValue; }

protected:
	int m_iAction;
	int m_nNumItems;
	int m_iSetValue;
	int m_iValue;
};

#endif//NEON_LINKBEHAVIOUR_COMBOBOX_H
