//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKBEHAVIOUR_TRACKBAR_H
#define NEON_LINKBEHAVIOUR_TRACKBAR_H

#include "LinkBehaviour.h"

//---------------------------------------------------------------------------//
//	Class CLinkBehaviour_TrackBar
//---------------------------------------------------------------------------//
class CLinkBehaviour_TrackBar : public ILinkBehaviour {
public:
	enum {
		DEVICE_VALUE,
		INV_DEVICE_VALUE,
		SET,
		ADD,
		RAND,
		FADE,
	};

public:
	CLinkBehaviour_TrackBar(int eAction, float fValue)
		: m_iAction(eAction)
		, m_fValue(fValue)
		, m_fSetValue(0.f)
		, m_fAddValue(0.f)
		, m_fFadeIni(0.f)
		, m_fFadeFr(0.f)
		, m_fFadeTo(0.f)
		, m_fFadeLength(1.f)
		, m_bFadeDone(false)
	{}
	~CLinkBehaviour_TrackBar() override;

	auto Type() const -> int override {
		return TRACK_BAR;
	}
	auto Action() const -> int override {
		return m_iAction;
	}
	auto Value() const -> float override {
		return m_fValue;
	}
	auto Evaluate(ILinkDevice* pDevice) -> bool override;

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkBehaviour* override;

	auto SetFadeValues(float fFrom, float fTo, float fLength) -> void { m_fFadeFr = fFrom; m_fFadeTo = fTo; m_fFadeLength = fLength; }
	auto SetSetValues(float fSetValue) -> void { m_fSetValue = fSetValue; }
	auto SetAddValues(float fAddValue) -> void { m_fAddValue = fAddValue; }

	auto GetFadeValues(float &fFrom, float &fTo, float &fLength) -> void { fFrom = m_fFadeFr; fTo = m_fFadeTo; fLength = m_fFadeLength; }
	auto GetSetValues(float &fSetValue) -> void { fSetValue = m_fSetValue; }
	auto GetAddValues(float &fAddValue) -> void { fAddValue = m_fAddValue; }

protected:
	int m_iAction;
	float m_fValue;
	float m_fSetValue;
	float m_fAddValue;
	float m_fFadeIni;
	float m_fFadeFr;
	float m_fFadeTo;
	float m_fFadeLength;
	bool m_bFadeDone;
};

#endif//NEON_LINKBEHAVIOUR_TRACKBAR_H
