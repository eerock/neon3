//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "Link.h"
#include "LinkDevice_Beat.h"
#include "LinkDevice_Keyboard.h"
#include "LinkDevice_MidiNote.h"
#include "LinkDevice_MidiSlider.h"
#include "LinkDevice_Wave.h"
#include "LinkBehaviour_Button.h"
#include "LinkBehaviour_CheckBox.h"
#include "LinkBehaviour_ComboBox.h"
#include "LinkBehaviour_TrackBar.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CLink::CLink(ILinkDevice* pDevice, ILinkBehaviour* pBehaviour)
	: m_pDevice(pDevice)
	, m_pBehaviour(pBehaviour)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLink::~CLink() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLink\n"));
#endif
	DELETE_PTR(m_pDevice);
	DELETE_PTR(m_pBehaviour);
}


//---------------------------------------------------------------------------//
//	Evaluate
//	Return true if link value changed since last call
//---------------------------------------------------------------------------//
auto CLink::Evaluate() -> bool {
	return m_pBehaviour->Evaluate(m_pDevice);
}


//---------------------------------------------------------------------------//
//	ValueCheckBox
//---------------------------------------------------------------------------//
auto CLink::ValueCheckBox() const -> bool {
	//return (m_pBehaviour->Value() > 0.f);
	return (m_pBehaviour->Value() >= 0.5f);
}


//---------------------------------------------------------------------------//
//	ValueComboBox
//---------------------------------------------------------------------------//
auto CLink::ValueComboBox() const -> int {
	return (int)m_pBehaviour->Value();
}


//---------------------------------------------------------------------------//
//	ValueTrackBar
//---------------------------------------------------------------------------//
auto CLink::ValueTrackBar() const -> float {
	return m_pBehaviour->Value();
}


//---------------------------------------------------------------------------//
//	ValueButton
//---------------------------------------------------------------------------//
auto CLink::ValueButton() const -> bool {
	return (m_pBehaviour->Value() >= 0.5f);
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLink::Load(CNodeFile::CNode* pNode) -> void {
	DELETE_PTR(m_pDevice);
	DELETE_PTR(m_pBehaviour);

	// Load Device
	switch (pNode->AttrAsInt("device")) {
		case ILinkDevice::BEAT: {
			m_pDevice = NEW CLinkDevice_Beat;
			break;
		}
		case ILinkDevice::KEYBOARD: {
			m_pDevice = NEW CLinkDevice_Keyboard;
			break;
		}
		case ILinkDevice::MIDI_CLOCK: {
			m_pDevice = NEW CLinkDevice_Beat;
			break;
		}
		case ILinkDevice::MIDI_NOTE: {
			m_pDevice = NEW CLinkDevice_MidiNote;
			break;
		}
		case ILinkDevice::MIDI_SLIDER: {
			m_pDevice = NEW CLinkDevice_MidiSlider;
			break;
		}
		case ILinkDevice::TIMER: {
			m_pDevice = NEW CLinkDevice_Beat;
			break;
		}
		case ILinkDevice::WAVE: {
			m_pDevice = NEW CLinkDevice_Wave;
			break;
		}
		default: {
			break;
		}
	}

	if (m_pDevice) {
		m_pDevice->Load(pNode);
	}

	// Load Behaviour
	switch (pNode->AttrAsInt("behaviour")) {
		case ILinkBehaviour::CHECK_BOX: {
			m_pBehaviour = NEW CLinkBehaviour_CheckBox(0, false);
			break;
		}
		case ILinkBehaviour::COMBO_BOX: {
			m_pBehaviour = NEW CLinkBehaviour_ComboBox(0, 0, 0, 0);
			break;
		}
		case ILinkBehaviour::TRACK_BAR: {
			m_pBehaviour = NEW CLinkBehaviour_TrackBar(0, 0);
			break;
		}
		case ILinkBehaviour::BUTTON: {
			m_pBehaviour = NEW CLinkBehaviour_Button(0);
			break;
		}
		default: {
			break;
		}
	}

	if (m_pBehaviour) {
		m_pBehaviour->Load(pNode);
	}
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLink::Save(CNodeFile* pFile, std::string const& sNodeName) -> void {
	std::stringstream ss;
	ss << "device=\"" << m_pDevice->Type() << "\" behaviour=\"" << m_pBehaviour->Type() << "\"";
	pFile->WriteOpenNode(sNodeName, ss.str());
	m_pDevice->Save(pFile);
	m_pBehaviour->Save(pFile);
	pFile->WriteCloseNode();
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLink::Clone() const -> CLink* {
	return NEW CLink(m_pDevice->Clone(), m_pBehaviour->Clone());
}
