//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "LinkDevice.h"
#include "LinkBehaviour_CheckBox.h"


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLinkBehaviour_CheckBox::~CLinkBehaviour_CheckBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLinkBehaviour_CheckBox\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Evaluate
//---------------------------------------------------------------------------//
auto CLinkBehaviour_CheckBox::Evaluate(ILinkDevice* pDevice) -> bool {
	if (pDevice->Evaluate()) {
		bool bActive = pDevice->Value() > 0.5f;
		switch (m_iAction) {
			case SWITCH: {
				// Switch only when device is activated
				if (bActive) {
					m_bValue = !m_bValue;
					return true;
				}
				break;
			}
			case DEVICE_VALUE: {
				m_bValue = bActive;
				return true;
			}
			case INV_DEVICE_VALUE: {
				m_bValue = !bActive;
				return true;
			}
			default: {
				break;
			}
		}
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CLinkBehaviour_CheckBox::Load(CNodeFile::CNode* pNode) -> void {
	m_iAction = pNode->AsInt("b_action");
	m_bValue = pNode->AsBool("b_value");
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CLinkBehaviour_CheckBox::Save(CNodeFile* pFile) -> void {
	pFile->WriteNode("b_action", "", m_iAction);
	pFile->WriteNode("b_value", "", m_bValue );
}


//---------------------------------------------------------------------------//
//	Clone
//---------------------------------------------------------------------------//
auto CLinkBehaviour_CheckBox::Clone() const -> ILinkBehaviour* {
	return NEW CLinkBehaviour_CheckBox(m_iAction, m_bValue);
}
