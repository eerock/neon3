//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LINKBEHAVIOUR_CHECKBOX_H
#define NEON_LINKBEHAVIOUR_CHECKBOX_H

#include "LinkBehaviour.h"


//---------------------------------------------------------------------------//
//	Class CLinkBehaviour_CheckBox
//---------------------------------------------------------------------------//
class CLinkBehaviour_CheckBox : public ILinkBehaviour {
public:
	enum {
		SWITCH,
		DEVICE_VALUE,
		INV_DEVICE_VALUE,
	};

public:
	CLinkBehaviour_CheckBox(int eAction, bool bInitValue)
		: m_iAction(eAction)
		, m_bValue(bInitValue)
	{}
	~CLinkBehaviour_CheckBox() override;

	auto Type() const -> int override {
		return CHECK_BOX;
	}
	auto Action() const -> int override {
		return m_iAction;
	}
	auto Value() const -> float override {
		return m_bValue ? 1.f : 0.f;
	}
	auto Evaluate(ILinkDevice* pDevice) -> bool override;

	auto Load(CNodeFile::CNode* pNode) -> void override;
	auto Save(CNodeFile* pFile) -> void override;
	auto Clone() const -> ILinkBehaviour* override;

protected:
	int m_iAction;
	bool m_bValue;
};

#endif//NEON_LINKBEHAVIOUR_CHECKBOX_H
