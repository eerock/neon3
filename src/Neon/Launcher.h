//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LAUNCHER_H
#define NEON_LAUNCHER_H

#include "AppOptions.h"


//---------------------------------------------------------------------------//
//	Class CLauncher
//---------------------------------------------------------------------------//
class CLauncher {
public:
	CLauncher();
	~CLauncher();

	auto Show(THInst hInstance) -> bool;
	auto LaunchPlayer() const -> bool { return m_tOpts.m_bNeonPlayer; }

#if (PLATFORM == PLATFORM_WINDOWS)
	auto OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) -> LRESULT;
	auto EnumMonitors(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData) -> BOOL;
	auto OnKeyDown(int iKey) -> bool;
#endif//PLATFORM

private:
	auto InitDialog() -> void;
	auto RefreshControls() -> void;
	auto SaveData() -> void;
	auto GetTextBoxValue(int iID) -> int;

#if (PLATFORM == PLATFORM_WINDOWS)
	static auto CALLBACK LauncherDialogProc(THWnd hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) -> LRESULT;
#endif//PLATFORM

	// Launcher Dialog
	bool m_bLaunch;
	bool m_bEnumeratingMonitors;
	size_t m_nAdapters;
	size_t m_nModes;
	int m_iMode;
	THWnd m_hDlgWnd;
	THWnd m_hComboDevice;
	THWnd m_hComboResolution;

	// App Options
	TAppOptions m_tOpts;

	// Win Data
#if (PLATFORM == PLATFORM_WINDOWS)
	RECT m_tMonitorRect;
	HMONITOR m_hMonitor;
#endif//PLATFORM

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3D* m_pD3D;
	std::vector<D3DDISPLAYMODE> m_vModes;
	std::vector<D3DADAPTER_IDENTIFIER9> m_vAdapters;
#endif//GFX_ENGINE_DIRECT3D
};

#endif//NEON_LAUNCHER_H
