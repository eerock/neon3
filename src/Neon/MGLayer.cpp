//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "MGLayer.h"
#include "VJLayer.h"

#include "Controls/MGButton.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGLabel.h"
#include "Controls/MGPanel.h"
#include "Controls/MGTrackBar.h"

#include "LinkSystem/Link.h"

//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGLayer::CMGLayer(CMGControl* pParent, int iIndex, CVJLayer* pVJLayer, CMGGridPanel* pGridPanel)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_iLayerIndex(iIndex)
	, m_pVJLayer(pVJLayer)
	, m_pGridPanel(pGridPanel)
	, m_pLayerTitle(nullptr)
	, m_pLayerAlpha(nullptr)
	, m_pLayerBypass(nullptr)
	, m_pBtnLinkAlpha(nullptr)
	, m_pBtnLinkBypass(nullptr)
	, m_pLinkAlpha(nullptr)
	, m_pLinkBypass(nullptr)
	, m_pBtnNext(nullptr)
	, m_pBtnPrev(nullptr)
	, m_pBtnRand(nullptr)
	, m_pBtnLinkNext(nullptr)
	, m_pBtnLinkPrev(nullptr)
	, m_pBtnLinkRand(nullptr)
	, m_pLinkNext(nullptr)
	, m_pLinkPrev(nullptr)
	, m_pLinkRand(nullptr)
{
	ASSERT(m_pVJLayer);
	ASSERT(m_pGridPanel);

	m_iAlign = MGALIGN_CLIENT;

	int x = 10;
	int y = 25;

	std::stringstream ss;
	ss << "Layer " << m_iLayerIndex + 1;

	CMGPanel* pPanel = NEW CMGPanel(this, 0, 0, 0, y, MGALIGN_TOP);
	m_pLayerTitle = NEW CMGLabel(pPanel, x, 5, MGALIGN_NONE, FONT_FX_TITLE, ss.str());

	y += 10;
	NEW CMGLabel(this, x + 5, y, MGALIGN_NONE, FONT_NORMAL, "Alpha");
	m_pLayerAlpha = NEW CMGTrackBar(this, 70, y, 200, false, bind(&CMGLayer::OnSetAlpha, this));
	m_pLayerAlpha->SetPos(1.f);
	m_pBtnLinkAlpha = NEW CMGButton(this, 276, y, CMGButton::LINK, "L", bind(&CMGLayer::OnBtnLinkAlpha, this));

	y += 30;
	NEW CMGLabel(this, x + 5, y, MGALIGN_NONE, FONT_NORMAL, "Bypass");
	m_pLayerBypass = NEW CMGCheckBox(this, 70, y - 4, "", false, false, bind(&CMGLayer::OnBypass, this));
	m_pLayerBypass->SetChecked(false);
	m_pBtnLinkBypass = NEW CMGButton(this, 276, y, CMGButton::LINK, "L", bind(&CMGLayer::OnBtnLinkBypass, this));

	y += 30;
	m_pBtnNext = NEW CMGButton(this, x, y, CMGButton::NORMAL, "Next Clip", bind(&CMGLayer::OnBtnNext, this));
	m_pBtnLinkNext = NEW CMGButton(this, x + 80, y + 4, CMGButton::LINK, "L", bind(&CMGLayer::OnBtnLinkNext, this));

	y += 30;
	m_pBtnPrev = NEW CMGButton(this, x, y, CMGButton::NORMAL, "Prev Clip", bind(&CMGLayer::OnBtnPrev, this));
	m_pBtnLinkPrev = NEW CMGButton(this, x + 80, y + 4, CMGButton::LINK, "L", bind(&CMGLayer::OnBtnLinkPrev, this));

	y += 30;
	m_pBtnRand = NEW CMGButton(this, x, y, CMGButton::NORMAL, "Rand Clip", bind(&CMGLayer::OnBtnRand, this));
	m_pBtnLinkRand = NEW CMGButton(this, x + 80, y + 4, CMGButton::LINK, "L", bind(&CMGLayer::OnBtnLinkRand, this));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGLayer::~CMGLayer() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGLayer\n"));
#endif
	m_pVJLayer = nullptr;
	m_pGridPanel = nullptr;
	DELETE_PTR(m_pLinkAlpha);
	DELETE_PTR(m_pLinkBypass);
	DELETE_PTR(m_pLinkNext);
	DELETE_PTR(m_pLinkPrev);
	DELETE_PTR(m_pLinkRand);
}


//---------------------------------------------------------------------------//
//	Load
//---------------------------------------------------------------------------//
auto CMGLayer::Load(CNodeFile::CNode* pNode) -> bool {
	auto pNodeLinks = pNode->FirstNode("links");
	if (pNodeLinks) {
		auto pNodeLink = pNodeLinks->FirstNode("layeralpha");
		if (pNodeLink) {
			if (!m_pLinkAlpha) {
				m_pLinkAlpha = NEW CLink(nullptr, nullptr);
			}
			m_pLinkAlpha->Load(pNodeLink);
			m_pBtnLinkAlpha->SetLinked(true);
		}

		pNodeLink = pNodeLinks->FirstNode("layerbypass");
		if (pNodeLink) {
			if (!m_pLinkBypass) {
				m_pLinkBypass = NEW CLink(nullptr, nullptr);
			}
			m_pLinkBypass->Load(pNodeLink);
			m_pBtnLinkBypass->SetLinked(true);
		}

		pNodeLink = pNodeLinks->FirstNode("layernext");
		if (pNodeLink) {
			if (!m_pLinkNext) {
				m_pLinkNext = NEW CLink(nullptr, nullptr);
			}
			m_pLinkNext->Load(pNodeLink);
			m_pBtnLinkNext->SetLinked(true);
		}

		pNodeLink = pNodeLinks->FirstNode("layerprev");
		if (pNodeLink) {
			if (!m_pLinkPrev) {
				m_pLinkPrev = NEW CLink(nullptr, nullptr);
			}
			m_pLinkPrev->Load(pNodeLink);
			m_pBtnLinkPrev->SetLinked(true);
		}

		pNodeLink = pNodeLinks->FirstNode("layerrand");
		if (pNodeLink) {
			if (!m_pLinkRand) {
				m_pLinkRand = NEW CLink(nullptr, nullptr);
			}
			m_pLinkRand->Load(pNodeLink);
			m_pBtnLinkRand->SetLinked(true);
		}
	}
	return true;
}


//---------------------------------------------------------------------------//
//	Save
//---------------------------------------------------------------------------//
auto CMGLayer::Save(CNodeFile* pNodeFile) -> void {
	if (m_pLinkAlpha) {
		m_pLinkAlpha->Save(pNodeFile, "layeralpha");
	}
	if (m_pLinkBypass) {
		m_pLinkBypass->Save(pNodeFile, "layerbypass");
	}
	if (m_pLinkNext) {
		m_pLinkNext->Save(pNodeFile, "layernext");
	}
	if (m_pLinkPrev) {
		m_pLinkPrev->Save(pNodeFile, "layerprev");
	}
	if (m_pLinkRand) {
		m_pLinkRand->Save(pNodeFile, "layerrand");
	}
}


//---------------------------------------------------------------------------//
//	Update
//	evaluate the links, if the links give a positive value, 
//	trigger a button press...
//---------------------------------------------------------------------------//
auto CMGLayer::Update() -> void {
	if (m_pLinkAlpha && m_pLinkAlpha->Evaluate()) {
		float fPos = m_pLinkAlpha->ValueTrackBar();
		m_pLayerAlpha->SetPos(fPos);
	}

	if (m_pLinkBypass && m_pLinkBypass->Evaluate()) {
		bool bChecked = m_pLinkBypass->ValueCheckBox();
		m_pLayerBypass->SetChecked(bChecked);
	}

	if (m_pLinkNext && m_pLinkNext->Evaluate()) {
		if (m_pLinkNext->ValueButton()) {
			m_pBtnNext->Trigger();
		}
	}

	if (m_pLinkPrev && m_pLinkPrev->Evaluate()) {
		if (m_pLinkPrev->ValueButton()) {
			m_pBtnPrev->Trigger();
		}
	}

	if (m_pLinkRand && m_pLinkRand->Evaluate()) {
		if (m_pLinkRand->ValueButton()) {
			m_pBtnRand->Trigger();
		}
	}
}


//---------------------------------------------------------------------------//
//	OnSetAlpha
//---------------------------------------------------------------------------//
auto CMGLayer::OnSetAlpha() -> void {
	m_pVJLayer->SetAlpha(m_pLayerAlpha->GetPos());
}


//---------------------------------------------------------------------------//
//	OnBypass
//---------------------------------------------------------------------------//
auto CMGLayer::OnBypass() -> void {
	m_pVJLayer->SetBypass(m_pLayerBypass->Checked());
}


//---------------------------------------------------------------------------//
//	OnBtnLinkAlpha
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnLinkAlpha() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkAlpha,
		m_pBtnLinkAlpha->Left(),	// could we just use Mouse X & Y?
		m_pBtnLinkAlpha->Top(),
		ILinkBehaviour::TRACK_BAR,
		false,
		m_pLayerAlpha,
		m_pBtnLinkAlpha,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnBtnLinkBypass
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnLinkBypass() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkBypass,
		m_pBtnLinkBypass->Left(),
		m_pBtnLinkBypass->Top(),
		ILinkBehaviour::CHECK_BOX,
		false,
		m_pLayerBypass,
		m_pBtnLinkBypass,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnBtnNext
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnNext() -> void {
	m_pGridPanel->NextClip(m_iLayerIndex);
}


//---------------------------------------------------------------------------//
//	OnBtnPrev
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnPrev() -> void {
	m_pGridPanel->PrevClip(m_iLayerIndex);
}


//---------------------------------------------------------------------------//
//	OnBtnRand
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnRand() -> void {
	m_pGridPanel->RandClip(m_iLayerIndex);
}


//---------------------------------------------------------------------------//
//	OnBtnLinkNext
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnLinkNext() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkNext,
		m_pBtnLinkNext->Left(),
		m_pBtnLinkNext->Top(),
		ILinkBehaviour::BUTTON,
		false,
		m_pBtnNext,
		m_pBtnLinkNext,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnBtnLinkPrev
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnLinkPrev() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkPrev,
		m_pBtnLinkPrev->Left(),
		m_pBtnLinkPrev->Top(),
		ILinkBehaviour::BUTTON,
		false,
		m_pBtnPrev,
		m_pBtnLinkPrev,
		NULL
	);
}


//---------------------------------------------------------------------------//
//	OnBtnLinkRand
//---------------------------------------------------------------------------//
auto CMGLayer::OnBtnLinkRand() -> void {
	g_pAppMain->OpenVarLinker(
		&m_pLinkRand,
		m_pBtnLinkRand->Left(),
		m_pBtnLinkPrev->Top(),
		ILinkBehaviour::BUTTON,
		false,
		m_pBtnRand,
		m_pBtnLinkRand,
		NULL
	);
}

