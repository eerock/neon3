//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"

#include "Controls/MGButton.h"
#include "Controls/MGCheckBox.h"
#include "Controls/MGComboBox.h"
#include "Controls/MGEditBox.h"
#include "Controls/MGGroupBox.h"
#include "Controls/MGLabel.h"
#include "Controls/MGListBox.h"
#include "Controls/MGPanel.h"
#include "Controls/MGSpinBox.h"
#include "Controls/MGTrackBar.h"
#include "LinkSystem/Link.h"
#include "LinkSystem/LinkBehaviour_Button.h"
#include "LinkSystem/LinkBehaviour_CheckBox.h"
#include "LinkSystem/LinkBehaviour_ComboBox.h"
#include "LinkSystem/LinkBehaviour_TrackBar.h"
#include "LinkSystem/LinkDevice_Beat.h"
#include "LinkSystem/LinkDevice_Keyboard.h"
#include "LinkSystem/LinkDevice_MidiNote.h"
#include "LinkSystem/LinkDevice_MidiSlider.h"
#include "LinkSystem/LinkDevice_Wave.h"
#include "NeonMain.h"
#include "MGHotKey.h"
#include "MGWindowVarLinker.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGWindowVarLinker::CMGWindowVarLinker()
	: CMGWindow(500, 232, "Var Linker")
	, m_iBehType(0)
	, m_ppToLink(nullptr)
	, m_pLinkedControl(nullptr)
	, m_pLinkButton(nullptr)
	, m_pLBDevices(nullptr)
	, m_pCurrentDevice(nullptr)
	, m_pBD_Panel(nullptr)
	, m_pBD_Beat(nullptr)
	, m_pBD_Offset(nullptr)
	, m_pKD_Panel(nullptr)
	, m_pKD_HotKey(nullptr)
	, m_pMND_Panel(nullptr)
	, m_pMND_HotKey(nullptr)
	, m_pMSD_Panel(nullptr)
	, m_pMSD_HotKey(nullptr)
	, m_pWD_Panel(nullptr)
	, m_pWD_Chan(nullptr)
	, m_pWD_Gain(nullptr)
	, m_pCBPresets(nullptr)
	, m_pBtnSavePreset(nullptr)
	, m_pBtnDelPreset(nullptr)
	, m_pCurrentAction(nullptr)
	, m_pPanelActionsCheckBox(nullptr)
	, m_pCHA_Switch(nullptr)
	, m_pCHA_DevVal(nullptr)
	, m_pCHA_InvDev(nullptr)
	, m_pPanelActionsComboBox(nullptr)
	, m_pCBA_Next(nullptr)
	, m_pCBA_Prev(nullptr)
	, m_pCBA_Rand(nullptr)
	, m_pCBA_Set(nullptr)
	, m_pCBA_SetValue(nullptr)
	, m_pPanelActionsTrackBarA(nullptr)
	, m_pPanelActionsTrackBarF(nullptr)
	, m_pTBA_DevVal(nullptr)
	, m_pTBA_InvDev(nullptr)
	, m_pTBA_Set(nullptr)
	, m_pTBA_Add(nullptr)
	, m_pTBA_Rand(nullptr)
	, m_pTBA_Fade(nullptr)
	, m_pTBA_SetValue(nullptr)
	, m_pTBA_AddValue(nullptr)
	, m_pTBA_FadeFr(nullptr)
	, m_pTBA_FadeTo(nullptr)
	, m_pTBA_FadeLength(nullptr)
	, m_pPanelActionsButton(nullptr)
	, m_pBTNA_Activate(nullptr)
{
	// Accept/Cancel changes
	NEW CMGButton(this, 400, 140, CMGButton::NORMAL, " Link ", bind(&CMGWindowVarLinker::OnClickAccept, this));
	NEW CMGButton(this, 400, 170, CMGButton::NORMAL, "Cancel", bind(&CMGWindowVarLinker::OnClickCancel, this));
	NEW CMGButton(this, 400, 200, CMGButton::NORMAL, "Unlink", bind(&CMGWindowVarLinker::OnClickUnlink, this));

	// Presets
	m_pCBPresets = NEW CMGComboBox(this, 380, 21, 116, 100, true, bind(&CMGWindowVarLinker::OnChangePreset, this));
	m_pBtnSavePreset = NEW CMGButton(this, 380, 46, CMGButton::BLEND, " Save ", bind(&CMGWindowVarLinker::OnSavePreset, this));
	m_pBtnDelPreset  = NEW CMGButton(this, 457, 46, CMGButton::BLEND, "Delete", bind(&CMGWindowVarLinker::OnDeletePreset, this));

	// Create device and action controls
	CreateDeviceCtrls();
	CreateActionCtrls();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGWindowVarLinker::~CMGWindowVarLinker() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGWindowVarLinker\n"));
#endif
	// ebp-> fixit: is this correct?  What if one of these has its Parent set to something?
	// try opening the VarLinker window then closing Neon, see what happens.
	// Dispose All
	DELETE_PTR(m_pBD_Panel);
	DELETE_PTR(m_pKD_Panel);
	DELETE_PTR(m_pMND_Panel);
	DELETE_PTR(m_pMSD_Panel);
	DELETE_PTR(m_pWD_Panel);
	DELETE_PTR(m_pPanelActionsCheckBox);
	DELETE_PTR(m_pPanelActionsComboBox);
	DELETE_PTR(m_pPanelActionsTrackBarA);
	DELETE_PTR(m_pPanelActionsTrackBarF);
	DELETE_PTR(m_pPanelActionsButton);
}


//---------------------------------------------------------------------------//
//	CreateDeviceCtrls
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::CreateDeviceCtrls() -> void {
	// Devices list
	m_pLBDevices = NEW CMGListBox(this, 2, 21, 116, 209, MGALIGN_NONE, false);
	m_pLBDevices->Clear();
	m_pLBDevices->m_fnOnSelectItem = bind(&CMGWindowVarLinker::OnChangeDevice, this);

	// Current device
	m_pCurrentDevice = nullptr;

	// Beat Device
	m_pBD_Panel = NEW CMGGroupBox(nullptr, 120, 20, 244, 56);
	NEW CMGLabel(m_pBD_Panel, 8, 20, MGALIGN_NONE, FONT_NORMAL, "Beat");
	m_pBD_Beat = NEW CMGSpinBox(m_pBD_Panel, 40,18, 40, 1, 99, 1, NULL);
	NEW CMGLabel(m_pBD_Panel, 100, 20, MGALIGN_NONE, FONT_NORMAL, "Offset");
	m_pBD_Offset = NEW CMGSpinBox(m_pBD_Panel, 140, 18, 40, 0, 99, 1, NULL);

	// Keyboard Device
	m_pKD_Panel = NEW CMGGroupBox(nullptr, 120, 20, 244, 56);
	NEW CMGLabel(m_pKD_Panel, 8, 20, MGALIGN_NONE, FONT_NORMAL, "HotKey");
	m_pKD_HotKey = NEW CMGHotKey(m_pKD_Panel, 55, 18, 70, false, NULL);

	// MIDI Note Device
	m_pMND_Panel = NEW CMGGroupBox(nullptr, 120, 20, 244, 56);
	NEW CMGLabel(m_pMND_Panel, 8, 20, MGALIGN_NONE, FONT_NORMAL, "MIDI Note");
	m_pMND_HotKey = NEW CMGHotKey(m_pMND_Panel, 55, 18, 70, true, NULL);

	// MIDI Slider Device
	m_pMSD_Panel = NEW CMGGroupBox(nullptr, 120, 20, 244, 56);
	NEW CMGLabel(m_pMSD_Panel, 8, 20, MGALIGN_NONE, FONT_NORMAL, "MIDI Slider");
	m_pMSD_HotKey = NEW CMGHotKey(m_pMSD_Panel, 60, 18, 70, true, NULL);

	// Wave Device
	m_pWD_Panel = NEW CMGGroupBox(nullptr, 120, 20, 244, 56);
	NEW CMGLabel(m_pWD_Panel, 8, 20, MGALIGN_NONE, FONT_NORMAL, "Chan");
	m_pWD_Chan = NEW CMGComboBox(m_pWD_Panel, 40, 18, 60, 100, false, NULL);
	m_pWD_Chan->Add("Global");
	m_pWD_Chan->Add("1");
	m_pWD_Chan->Add("2");
	m_pWD_Chan->Add("3");
	m_pWD_Chan->Add("4");
	m_pWD_Chan->Add("5");
	NEW CMGLabel(m_pWD_Panel, 108, 20, MGALIGN_NONE, FONT_NORMAL, "Gain");
	m_pWD_Gain = NEW CMGSpinBox(m_pWD_Panel, 140, 18, 40, 0, 1000, 10, NULL);
}


//---------------------------------------------------------------------------//
//	CreateActionCtrls
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::CreateActionCtrls() -> void {
	m_pCurrentAction = nullptr;

	// CheckBox Action
	m_pPanelActionsCheckBox = NEW CMGGroupBox(nullptr, 120, 77, 244, 154);
	m_pCHA_Switch = NEW CMGCheckBox(m_pPanelActionsCheckBox, 8, 18, "Switch", true, false, bind(&CMGWindowVarLinker::OnChange_CHA_Switch, this));
	m_pCHA_DevVal = NEW CMGCheckBox(m_pPanelActionsCheckBox, 8, 48, "Device Value", true, false, bind(&CMGWindowVarLinker::OnChange_CHA_DevVal, this));
	m_pCHA_InvDev = NEW CMGCheckBox(m_pPanelActionsCheckBox, 8, 78, "Inverse Device Value", true, false, bind(&CMGWindowVarLinker::OnChange_CHA_InvDev, this));

	// ComboBox Action
	m_pPanelActionsComboBox = NEW CMGGroupBox(nullptr, 120, 77, 244, 154);
	m_pCBA_Next = NEW CMGCheckBox(m_pPanelActionsComboBox, 8, 18, "Next", true, false, bind(&CMGWindowVarLinker::OnChange_CBA_Next, this));
	m_pCBA_Prev = NEW CMGCheckBox(m_pPanelActionsComboBox, 8, 48, "Previous", true, false, bind(&CMGWindowVarLinker::OnChange_CBA_Prev, this));
	m_pCBA_Rand = NEW CMGCheckBox(m_pPanelActionsComboBox, 8, 78, "Random", true, false, bind(&CMGWindowVarLinker::OnChange_CBA_Rand, this));
	m_pCBA_Set = NEW CMGCheckBox(m_pPanelActionsComboBox, 8, 108, "Set Item", true, false, bind(&CMGWindowVarLinker::OnChange_CBA_Set, this));
	m_pCBA_SetValue = NEW CMGSpinBox(m_pPanelActionsComboBox, 95, 108, 30, 0, 1, 1, NULL);

	// TrackBar Action (Slider)
	m_pPanelActionsTrackBarF = NEW CMGGroupBox(nullptr, 120, 77, 244, 154);
	m_pTBA_DevVal = NEW CMGCheckBox(m_pPanelActionsTrackBarF, 8, 48, "Device Value", true, false, bind(&CMGWindowVarLinker::OnChange_TBA_DevVal, this));
	m_pTBA_InvDev = NEW CMGCheckBox(m_pPanelActionsTrackBarF, 8, 78, "Inverse Device Value", true, false, bind(&CMGWindowVarLinker::OnChange_TBA_InvDev, this));

	// TrackBar Action (Button)
	m_pPanelActionsTrackBarA = NEW CMGGroupBox(nullptr, 120, 77, 244, 154);
	m_pTBA_Set = NEW CMGCheckBox(m_pPanelActionsTrackBarA, 8, 8, "Set Value", true, false, bind(&CMGWindowVarLinker::OnChange_TBA_Set, this));
	m_pTBA_Add = NEW CMGCheckBox(m_pPanelActionsTrackBarA, 8, 38, "Add Value", true, false, bind(&CMGWindowVarLinker::OnChange_TBA_Add, this));
	m_pTBA_Rand = NEW CMGCheckBox(m_pPanelActionsTrackBarA, 8, 68, "Rand", true, false, bind(&CMGWindowVarLinker::OnChange_TBA_Rand, this));
	m_pTBA_Fade = NEW CMGCheckBox(m_pPanelActionsTrackBarA, 8, 98, "Fade", true, false, bind(&CMGWindowVarLinker::OnChange_TBA_Fade, this));
	m_pTBA_SetValue = NEW CMGSpinBox(m_pPanelActionsTrackBarA, 82,  8, 40, 0, 100, 5, NULL);
	m_pTBA_AddValue = NEW CMGSpinBox(m_pPanelActionsTrackBarA, 82, 38, 40, -100, 100, 5, NULL);
	m_pTBA_FadeFr = NEW CMGSpinBox(m_pPanelActionsTrackBarA, 65, 98, 40, 0, 100, 5, NULL);
	m_pTBA_FadeTo = NEW CMGSpinBox(m_pPanelActionsTrackBarA, 135, 98, 40, 0, 100, 5, NULL);
	m_pTBA_FadeLength = NEW CMGSpinBox(m_pPanelActionsTrackBarA, 65, 128, 50, 0, 5000, 50, NULL);

	NEW CMGLabel(m_pPanelActionsTrackBarA, 110, 101, MGALIGN_NONE, FONT_NORMAL, "To");
	NEW CMGLabel(m_pPanelActionsTrackBarA, 120, 131, MGALIGN_NONE, FONT_NORMAL, "Length (ms)");
	NEW CMGButton(m_pPanelActionsTrackBarA, 180, 98, CMGButton::SMALL, "<>", bind(&CMGWindowVarLinker::OnChange_TBA_SwapFade, this));

	// Button Action
	m_pPanelActionsButton = NEW CMGGroupBox(nullptr, 120, 77, 244, 154);
	m_pBTNA_Activate = NEW CMGCheckBox(m_pPanelActionsButton, 8, 18, "Activate", true, true, bind(&CMGWindowVarLinker::OnChange_BTNA_Activate, this));
}


//---------------------------------------------------------------------------//
//	Show
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::Show(CLink** ppToLink, int iX, int iY, int iBehType, bool bOnlyMidiKey, CMGControl *pRControl, CMGButton *pLinkButton, function<void(EModalResult)> fnOnModalResult) -> void {
	m_iBehType = iBehType;
	m_ppToLink = ppToLink;
	m_pLinkedControl = pRControl;
	m_pLinkButton = pLinkButton;
	m_pCBPresets->SetVisible(!bOnlyMidiKey);
	m_pBtnSavePreset->SetVisible(!bOnlyMidiKey);
	m_pBtnDelPreset->SetVisible(!bOnlyMidiKey);

	// Select devices for this action
	m_pLBDevices->Clear();
	switch (m_iBehType) {
		case ILinkBehaviour::CHECK_BOX: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			//m_pLBDevices->Add("MIDI Clock");
			m_pLBDevices->Add("MIDI Note");
			//m_pLBDevices->Add("Timer");
			SetAction(m_pPanelActionsCheckBox);
			break;
		}
		case ILinkBehaviour::COMBO_BOX: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			//m_pLBDevices->Add("MIDI Clock");
			m_pLBDevices->Add("MIDI Note");
			m_pLBDevices->Add("MIDI Slider");
			//m_pLBDevices->Add("Timer");
			// ebp-> todo: the combo box actions should be different if a MIDI Slider is used.
			// it should be the Device Value / Inverse Device Value pair.
			SetAction(m_pPanelActionsComboBox);
			break;
		}
		case ILinkBehaviour::TRACK_BAR: {
			if (bOnlyMidiKey) {
				m_pLBDevices->Add("Keyboard");
				m_pLBDevices->Add("MIDI Note");
				m_pLBDevices->Add("MIDI Slider");
			} else {
				m_pLBDevices->Add("Beat");
				m_pLBDevices->Add("Keyboard");
				//m_pLBDevices->Add("MIDI Clock");
				m_pLBDevices->Add("MIDI Note");
				m_pLBDevices->Add("MIDI Slider");
				//m_pLBDevices->Add("Timer");
				m_pLBDevices->Add("Wave FFT");
			}
			break;
		}
		case ILinkBehaviour::BUTTON: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			m_pLBDevices->Add("MIDI Note");
			SetAction(m_pPanelActionsButton);
			break;
		}
		default: {
			break;
		}
	}

	m_pLBDevices->Select(0);
	m_pCBPresets->Select(0);
	LoadLink(*m_ppToLink);
	LoadPresets();
	// Finally show
	CMGWindow::Show(iX, iY, fnOnModalResult);
}


//---------------------------------------------------------------------------//
//	Show
//	ebp-> this is awful, refactor this code, avoid duplication.
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::Show(CLink** ppToLink, int iX, int iY,	int iBehType, CMGButton* pLinkButton, function<void(CMGWindow::EModalResult)> fnOnModalResult) -> void {
	m_iBehType = iBehType;
	m_ppToLink = ppToLink;
	m_pLinkButton = pLinkButton;
	m_pLinkedControl = nullptr;
	m_pCBPresets->SetVisible(true);
	m_pBtnSavePreset->SetVisible(true);
	m_pBtnDelPreset->SetVisible(true);

	// Select devices for this action
	m_pLBDevices->Clear();
	switch (m_iBehType) {
		case ILinkBehaviour::CHECK_BOX: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			//m_pLBDevices->Add("MIDI Clock");
			m_pLBDevices->Add("MIDI Note");
			//m_pLBDevices->Add("Timer");
			SetAction(m_pPanelActionsCheckBox);
			break;
		}
		case ILinkBehaviour::COMBO_BOX: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			//m_pLBDevices->Add("MIDI Clock");
			m_pLBDevices->Add("MIDI Note");
			m_pLBDevices->Add("MIDI Slider");
			//m_pLBDevices->Add("Timer");
			// ebp-> todo: the combo box actions should be different if a MIDI Slider is used.
			// it should be the Device Value / Inverse Device Value pair.
			SetAction(m_pPanelActionsComboBox);
			break;
		}
		case ILinkBehaviour::TRACK_BAR: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			//m_pLBDevices->Add("MIDI Clock");
			m_pLBDevices->Add("MIDI Note");
			m_pLBDevices->Add("MIDI Slider");
			//m_pLBDevices->Add("Timer");
			m_pLBDevices->Add("Wave FFT");
			break;
		}
		case ILinkBehaviour::BUTTON: {
			m_pLBDevices->Add("Beat");
			m_pLBDevices->Add("Keyboard");
			m_pLBDevices->Add("MIDI Note");
			SetAction(m_pPanelActionsButton);
			break;
		}
		default: {
			break;
		}
	}

	m_pLBDevices->Select(0);
	m_pCBPresets->Select(0);
	LoadLink(*m_ppToLink);
	LoadPresets();
	// Finally show
	CMGWindow::Show(iX, iY, fnOnModalResult);
}

//---------------------------------------------------------------------------//
//	Close
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::Close(EModalResult eModalResult) -> void {
	// Close device
	SetDevice(nullptr);
	// Close action
	SetAction(nullptr);
	CMGWindow::Close(eModalResult);
}


//---------------------------------------------------------------------------//
//	LoadLink
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::LoadLink(CLink* pLink) -> void {
	if (!pLink) {
		return;
	}

	// Load Device values
	switch (pLink->Device()->Type()) {
		// Beat Device
		case ILinkDevice::BEAT: {
			if (m_pLBDevices->Select("Beat")) {
				int iBeat, iOffs;
				((CLinkDevice_Beat*)(pLink->Device()))->Get(iBeat, iOffs);
				m_pBD_Beat->Set(iBeat);
				m_pBD_Offset->Set(iOffs);
			}
			break;
		}
		// Keyboard Device
		case ILinkDevice::KEYBOARD: {
			if(m_pLBDevices->Select("Keyboard")) {
				char c;
				((CLinkDevice_Keyboard*)(pLink->Device()))->Get(c);
				m_pKD_HotKey->Set(c);
			}
			break;
		}
		// MIDI Note
		case ILinkDevice::MIDI_NOTE: {
			if (m_pLBDevices->Select("MIDI Note")) {
				uchar c;
				((CLinkDevice_MidiNote*)(pLink->Device()))->Get(c);
				m_pMND_HotKey->Set(c);
			}
			break;
		}
		// MIDI Slider
		case ILinkDevice::MIDI_SLIDER: {
			if (m_pLBDevices->Select("MIDI Slider")) {
				uchar c;
				((CLinkDevice_MidiSlider*)(pLink->Device()))->Get(c);
				m_pMSD_HotKey->Set(c);
			}
			break;
		}
		// Wave Device
		case ILinkDevice::WAVE: {
			if (m_pLBDevices->Select("Wave FFT")) {
				int iChan;
				float fGain;
				((CLinkDevice_Wave*)(pLink->Device()))->Get(iChan, fGain);
				m_pWD_Chan->Select(iChan);
				m_pWD_Gain->Set(Floor(fGain * 100.f));
			}
			break;
		}
		default: {
			break;
		}
	}

	// Load Link values
	switch (pLink->Behaviour()->Type()) {
		case ILinkBehaviour::CHECK_BOX: {
			// CheckBox
			CLinkBehaviour_CheckBox* pLB = (CLinkBehaviour_CheckBox*)pLink->Behaviour();
			switch (pLB->Action()) {
				case CLinkBehaviour_CheckBox::SWITCH: {
					m_pCHA_Switch->SetChecked(true);
					break;
				}
				case CLinkBehaviour_CheckBox::DEVICE_VALUE: {
					m_pCHA_DevVal->SetChecked(true);
					break;
				}
				case CLinkBehaviour_CheckBox::INV_DEVICE_VALUE: {
					m_pCHA_InvDev->SetChecked(true);
					break;
				}
				default: {
					break;
				}
			}
			break;
		}
		case ILinkBehaviour::COMBO_BOX: {
			// ComboBox
			CLinkBehaviour_ComboBox* pLB = (CLinkBehaviour_ComboBox*)pLink->Behaviour();
			switch (pLB->Action()) {
				case CLinkBehaviour_ComboBox::NEXT: {
					m_pCBA_Next->SetChecked(true);
					break;
				}
				case CLinkBehaviour_ComboBox::PREV: {
					m_pCBA_Prev->SetChecked(true);
					break;
				}
				case CLinkBehaviour_ComboBox::RAND: {
					m_pCBA_Rand->SetChecked(true);
					break;
				}
				case CLinkBehaviour_ComboBox::SET: {
					m_pCBA_Set->SetChecked(true);
					break;
				}
				default: {
					break;
				}
			}
			m_pCBA_SetValue->Set(pLB->GetSetValue());
			break;
		}
		case ILinkBehaviour::TRACK_BAR: {
			// TrackBar
			CLinkBehaviour_TrackBar* pLB = (CLinkBehaviour_TrackBar*)pLink->Behaviour();
			switch (pLB->Action()) {
				case CLinkBehaviour_TrackBar::DEVICE_VALUE: {
					m_pTBA_DevVal->SetChecked(true);
					break;
				}
				case CLinkBehaviour_TrackBar::INV_DEVICE_VALUE: {
					m_pTBA_InvDev->SetChecked(true);
					break;
				}
				case CLinkBehaviour_TrackBar::SET: {
					m_pTBA_Set->SetChecked(true);
					break;
				}
				case CLinkBehaviour_TrackBar::ADD: {
					m_pTBA_Add->SetChecked(true);
					break;
				}
				case CLinkBehaviour_TrackBar::RAND: {
					m_pTBA_Rand->SetChecked(true);
					break;
				}
				case CLinkBehaviour_TrackBar::FADE: {
					m_pTBA_Fade->SetChecked(true);
					break;
				}
				default: {
					break;
				}
			}

			float fFadeFr, fFadeTo, fFadeLength, fSetValue, fAddValue;
			pLB->GetFadeValues(fFadeFr, fFadeTo, fFadeLength);
			pLB->GetSetValues(fSetValue);
			pLB->GetAddValues(fAddValue);
			m_pTBA_SetValue->Set(static_cast<int>(fSetValue * 100.f));
			m_pTBA_AddValue->Set(static_cast<int>(fAddValue * 100.f));
			m_pTBA_FadeFr->Set(static_cast<int>(fFadeFr * 100.f));
			m_pTBA_FadeTo->Set(static_cast<int>(fFadeTo * 100.f));
			m_pTBA_FadeLength->Set(static_cast<int>(fFadeLength * 1000.f));
			break;
		}
		case ILinkBehaviour::BUTTON: {
			CLinkBehaviour_Button* pLB = (CLinkBehaviour_Button*)pLink->Behaviour();
			switch (pLB->Action()) {
				case CLinkBehaviour_Button::ACTIVATE: {
					m_pBTNA_Activate->SetChecked(true);
					break;
				}
				default: {
					break;
				}
			}
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	CreateLink
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::CreateLink() -> CLink* {
	// Select device
	ILinkDevice* pDevice = nullptr;
	if (m_pCurrentDevice == m_pBD_Panel) {
		// Beat Device
		auto pLD = NEW CLinkDevice_Beat;
		pLD->Set(m_pBD_Beat->GetValue(), m_pBD_Offset->GetValue());
		pDevice = pLD;
	} else if (m_pCurrentDevice == m_pKD_Panel) {
		// Keyboard Device
		auto pLD = NEW CLinkDevice_Keyboard;
		char c;
		m_pKD_HotKey->Get(c);
		pLD->Set(c);
		pDevice = pLD;
	} else if (m_pCurrentDevice == m_pMND_Panel) {
		// MIDI Note Device
		auto pLD = NEW CLinkDevice_MidiNote;
		char c;
		m_pMND_HotKey->Get(c);
		pLD->Set(c);
		pDevice = pLD;
	} else if (m_pCurrentDevice == m_pMSD_Panel) {
		// MIDI Slider Device
		auto pLD = NEW CLinkDevice_MidiSlider;
		char c;
		m_pMSD_HotKey->Get(c);
		pLD->Set(c);
		pDevice = pLD;
	} else if (m_pCurrentDevice == m_pWD_Panel) {
		// Wave Device
		auto pLD = NEW CLinkDevice_Wave;
		pLD->Set(m_pWD_Chan->GetSelected(), m_pWD_Gain->GetValue() * 0.01f);
		pDevice = pLD;
	}

	// Select action
	ILinkBehaviour* pBehaviour = nullptr;
	if (m_pCurrentAction == m_pPanelActionsCheckBox) {
		// CheckBox Action
		int eAction = -1;
		if (m_pCHA_Switch->Checked()) {
			eAction = CLinkBehaviour_CheckBox::SWITCH;
		}
		if (m_pCHA_DevVal->Checked()) {
			eAction = CLinkBehaviour_CheckBox::DEVICE_VALUE;
		}
		if (m_pCHA_InvDev->Checked()) {
			eAction = CLinkBehaviour_CheckBox::INV_DEVICE_VALUE;
		}
		auto pLB = NEW CLinkBehaviour_CheckBox(eAction, m_pLinkedControl ? ((CMGCheckBox*)m_pLinkedControl)->Checked() : false);
		pBehaviour = pLB;
	} else if (m_pCurrentAction == m_pPanelActionsComboBox) {
		// ComboBox Action
		int eAction = -1;
		if (m_pCBA_Next->Checked()) {
			eAction = CLinkBehaviour_ComboBox::NEXT;
		}
		if (m_pCBA_Prev->Checked()) {
			eAction = CLinkBehaviour_ComboBox::PREV;
		}
		if (m_pCBA_Rand->Checked()) {
			eAction = CLinkBehaviour_ComboBox::RAND;
		}
		if (m_pCBA_Set->Checked()) {
			eAction = CLinkBehaviour_ComboBox::SET;
		}
		auto pLB = NEW CLinkBehaviour_ComboBox(
			eAction,
			m_pLinkedControl ? ((CMGComboBox*)m_pLinkedControl)->GetSelected() : 0,
			m_pLinkedControl ? ((CMGComboBox*)m_pLinkedControl)->GetNumItems() : 0,
			m_pCBA_SetValue->GetValue()
		);
		pBehaviour = pLB;
	} else if (m_pCurrentAction == m_pPanelActionsTrackBarA) {
		int eAction = -1;
		// TrackBar Action (Button)
		if (m_pTBA_Set->Checked()) {
			eAction = CLinkBehaviour_TrackBar::SET;
		}
		if (m_pTBA_Add->Checked()) {
			eAction = CLinkBehaviour_TrackBar::ADD;
		}
		if (m_pTBA_Fade->Checked()) {
			eAction = CLinkBehaviour_TrackBar::FADE;
		}
		if (m_pTBA_Rand->Checked()) {
			eAction = CLinkBehaviour_TrackBar::RAND;
		}
		auto pLB = NEW CLinkBehaviour_TrackBar(eAction, m_pLinkedControl ? ((CMGTrackBar*)m_pLinkedControl)->GetPos() : 0.f);
		pLB->SetSetValues(m_pTBA_SetValue->GetValue() * 0.01f);
		pLB->SetAddValues(m_pTBA_AddValue->GetValue() * 0.01f);
		pLB->SetFadeValues(m_pTBA_FadeFr->GetValue() * 0.01f, m_pTBA_FadeTo->GetValue() * 0.01f, m_pTBA_FadeLength->GetValue() * 0.001f);
		pBehaviour = pLB;
	} else if (m_pCurrentAction == m_pPanelActionsTrackBarF) {
		// TrackBar Action (Slider)
		int eAction = -1;
		if (m_pTBA_DevVal->Checked()) {
			eAction = CLinkBehaviour_TrackBar::DEVICE_VALUE;
		}
		if (m_pTBA_InvDev->Checked()) {
			eAction = CLinkBehaviour_TrackBar::INV_DEVICE_VALUE;
		}
		auto pLB = NEW CLinkBehaviour_TrackBar(eAction, m_pLinkedControl ? ((CMGTrackBar*)m_pLinkedControl)->GetPos() : 0.f);
		pBehaviour = pLB;
	} else if (m_pCurrentAction == m_pPanelActionsButton) {
		// Button Action
		int eAction = -1;
		if (m_pBTNA_Activate->Checked()) {
			eAction = CLinkBehaviour_Button::ACTIVATE;
		}
		auto pLB = NEW CLinkBehaviour_Button(eAction);
		pBehaviour = pLB;
	}

	return NEW CLink(pDevice, pBehaviour);
}


//---------------------------------------------------------------------------//
//	LoadPresets
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::LoadPresets() -> void {
	TFindFileResult Res;
	PushDir(neon::sDirPresets);
	neon::FindFileInit(Res, "*" + GetPresetExt());
	m_pCBPresets->Clear();
	m_pCBPresets->Add("");
	while (neon::FindFileNext(Res)) {
		if (!Res.bDir) {
			m_pCBPresets->Add(Res.sFilename.substr(0, Res.sFilename.size() - 4));
		}
	}
	m_pCBPresets->Select(0);
	neon::FindFileClose(Res);
	PopDir();
}


//---------------------------------------------------------------------------//
//	OnSavePreset
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnSavePreset() -> void {
	// Save Var Link Preset
	std::string sFile;
	bool bSave = false;
	switch (m_iBehType) {
		case ILinkBehaviour::CHECK_BOX: {
			bSave = g_pMGApp->OpenFileSave("Save Link Preset", neon::sDirPresets, "CheckBox Link Preset (*.lph)\0*.lph\0\0", "*.lph", sFile);
			break;
		}
		case ILinkBehaviour::COMBO_BOX: {
			bSave = g_pMGApp->OpenFileSave("Save Link Preset", neon::sDirPresets, "ComboBox Link Preset (*.lpc)\0*.lpc\0\0", "*.lpc", sFile);
			break;
		}
		case ILinkBehaviour::TRACK_BAR: {
			bSave = g_pMGApp->OpenFileSave("Save Link Preset", neon::sDirPresets, "TrackBar Link Preset (*.lpt)\0*.lpt\0\0", "*.lpt", sFile);
			break;
		}
		case ILinkBehaviour::BUTTON: {
			bSave = g_pMGApp->OpenFileSave("Save Link Preset", neon::sDirPresets, "Button Link Preset (*.lpb)\0*.lpb\0\0", "*.lpb", sFile);
			break;
		}
		default: {
			break;
		}
	}

	if (bSave) {
		CNodeFile NodeFile;
		NodeFile.WriteOpen(sFile);
		auto pLink = CreateLink();
		pLink->Save(&NodeFile, "linkpreset");
		DELETE_PTR(pLink);
		NodeFile.WriteClose();

		// Update presets
		LoadPresets();
	}
}


//---------------------------------------------------------------------------//
//	LoadPreset
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::LoadPreset(std::string const& sPreset) -> void {
	CNodeFile NodeFile;
	if (NodeFile.LoadFromFile(neon::sDirPresets + "\\" + sPreset + GetPresetExt())) {
		auto pNode = NodeFile.FirstNode("linkpreset");
		if (pNode) {
			auto pLink = NEW CLink(nullptr, nullptr);
			pLink->Load(pNode);
			LoadLink(pLink);
			DELETE_PTR(pLink);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnChangePreset
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChangePreset() -> void {
	if (m_pCBPresets->GetSelected() > 0) {
		LoadPreset(m_pCBPresets->GetText());
	}
}


//---------------------------------------------------------------------------//
//	OnDeletePreset
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnDeletePreset() -> void {
	if (m_pCBPresets->GetSelected() > 0) {
#if (PLATFORM == PLATFORM_WINDOWS)
		DeleteFile((neon::sDirPresets + "\\" + m_pCBPresets->GetText() + GetPresetExt()).c_str());
#endif//PLATFORM
		LoadPresets();
	}
}


//---------------------------------------------------------------------------//
//	GetPresetExt
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::GetPresetExt() const -> std::string const& {
	static std::string sExt;
	switch (m_iBehType) {
		case ILinkBehaviour::CHECK_BOX: {
			sExt = ".lph";
			break;
		}
		case ILinkBehaviour::COMBO_BOX: {
			sExt = ".lpc";
			break;
		}
		case ILinkBehaviour::TRACK_BAR: {
			sExt = ".lpt";
			break;
		}
		case ILinkBehaviour::BUTTON: {
			sExt = ".lpb";
			break;
		}
		default: {
			break;
		}
	}
	return sExt;
}


//---------------------------------------------------------------------------//
//	OnClickAccept
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnClickAccept() -> void {
	// Free previous link
	if (*m_ppToLink) {
		DELETE_PTR(*m_ppToLink);
	}
	// New One
	*m_ppToLink = CreateLink();
	m_pLinkButton->SetLinked(true);
	Close(MR_OK);
}


//---------------------------------------------------------------------------//
//	OnClickCancel
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnClickCancel() -> void {
	Close(MR_CANCEL);
}


//---------------------------------------------------------------------------//
//	OnClickUnlink
//	ebp-> todo: Create an 'Unlink' button
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnClickUnlink() -> void {
	if (*m_ppToLink) {
		DELETE_PTR(*m_ppToLink);
	}
	m_pLinkButton->SetLinked(false);
	Close(MR_NONE);
}


//---------------------------------------------------------------------------//
//	OnChangeDevice
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChangeDevice() -> void {
	std::string selected;
	if (m_pLBDevices->GetSelected(selected)) {
		if (selected == "Beat") {
			SetDevice(m_pBD_Panel);
			if (m_iBehType == ILinkBehaviour::TRACK_BAR) {
				SetAction(m_pPanelActionsTrackBarA);
			}
		} else if (selected == "Keyboard") {
			SetDevice(m_pKD_Panel);
			if (m_iBehType == ILinkBehaviour::TRACK_BAR) {
				SetAction(m_pPanelActionsTrackBarA);
			}
		} else if (selected == "MIDI Clock") {
			//SetDevice(m_pMCD_Panel);
			if (m_iBehType == ILinkBehaviour::TRACK_BAR) {
				SetAction(m_pPanelActionsTrackBarA);
			}
		} else if (selected == "MIDI Note") {
			SetDevice(m_pMND_Panel);
			if (m_iBehType == ILinkBehaviour::TRACK_BAR) {
				SetAction(m_pPanelActionsTrackBarA);
			}
		} else if (selected == "MIDI Slider") {
			SetDevice(m_pMSD_Panel);
			if (m_iBehType == ILinkBehaviour::TRACK_BAR) {
				SetAction(m_pPanelActionsTrackBarF);
			}
		} else if (selected == "Timer") {
			//SetDevice(m_pTD_Panel);	// not implemented yet
			if (m_iBehType == ILinkBehaviour::TRACK_BAR) {
				SetAction(m_pPanelActionsTrackBarA);
			}
		} else if (selected == "Wave FFT") {
			SetDevice(m_pWD_Panel);
			SetAction(m_pPanelActionsTrackBarF);
		}
	} else {
		SetDevice(nullptr);
	}
}


//---------------------------------------------------------------------------//
//	SetDevice
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::SetDevice(CMGGroupBox* pDevice) -> void {
	if (pDevice == m_pCurrentDevice) {
		return;
	}

	if (m_pCurrentDevice) {
		m_pCurrentDevice->SetParent(nullptr);
		m_pCurrentDevice = nullptr;
	}

	m_pCurrentDevice = pDevice;

	if (pDevice) {
		m_pCurrentDevice->SetParent(this);
		ResetDeviceData();
	}
}


//---------------------------------------------------------------------------//
//	SetAction
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::SetAction(CMGGroupBox* pAction) -> void {
	if (pAction == m_pCurrentAction) {
		return;
	}

	if (m_pCurrentAction) {
		m_pCurrentAction->SetParent(nullptr);
		m_pCurrentAction = nullptr;
	}

	m_pCurrentAction = pAction;

	if (pAction) {
		m_pCurrentAction->SetParent(this);
		ResetActionData();
	}
}


//---------------------------------------------------------------------------//
//	ResetDeviceData
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::ResetDeviceData() -> void {
	if (m_pCurrentDevice == m_pBD_Panel) {
		m_pBD_Beat->Set(1);
		m_pBD_Offset->Set(0);
	} else if (m_pCurrentDevice == m_pKD_Panel) {
		// ebp-> todo: how to reset the keyboard device data?  set the char to none?
	} else if (m_pCurrentDevice == m_pMND_Panel) {
		// ebp-> todo: how to reset the MIDI Note device data? 
	} else if (m_pCurrentDevice == m_pMSD_Panel) {
		// ebp-> todo: how to reset the MIDI Slider device data?
		// set the hotkey to "None"?
	} else if (m_pCurrentDevice == m_pWD_Panel) {
		m_pWD_Chan->Select(0);
		m_pWD_Gain->Set(100);
	}
}


//---------------------------------------------------------------------------//
//	ResetActionData
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::ResetActionData() -> void {
	if (m_pCurrentAction == m_pPanelActionsCheckBox) {
		m_pCHA_Switch->SetChecked(true);
	} else if (m_pCurrentAction == m_pPanelActionsComboBox) {
		m_pCBA_Next->SetChecked(true);
		m_pCBA_SetValue->Set(0);
	} else if (m_pCurrentAction == m_pPanelActionsTrackBarA) {
		m_pTBA_Set->SetChecked(true);
		m_pTBA_SetValue->Set(100);
		m_pTBA_AddValue->Set(10);
		m_pTBA_FadeFr->Set(0);
		m_pTBA_FadeTo->Set(100);
		m_pTBA_FadeLength->Set(250);
	} else if (m_pCurrentAction == m_pPanelActionsTrackBarF) {
		m_pTBA_DevVal->SetChecked(true);
	} else if (m_pCurrentAction == m_pPanelActionsButton) {
		m_pBTNA_Activate->SetChecked(true);
	}
}


//---------------------------------------------------------------------------//
//	 OnChange_CHA_Switch
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CHA_Switch() -> void {
	if (!m_pCHA_Switch->Checked()) {
		if (!m_pCHA_DevVal->Checked() && !m_pCHA_InvDev->Checked()) {
			m_pCHA_Switch->SetChecked(true);
		}
	} else {
		m_pCHA_DevVal->SetChecked(false);
		m_pCHA_InvDev->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_CHA_DevVal
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CHA_DevVal() -> void {
	if (!m_pCHA_DevVal->Checked()) {
		if (!m_pCHA_Switch->Checked() && !m_pCHA_InvDev->Checked()) {
			m_pCHA_DevVal->SetChecked(true);
		}
	} else {
		m_pCHA_Switch->SetChecked(false);
		m_pCHA_InvDev->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_CHA_InvDev
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CHA_InvDev() -> void {
	if (!m_pCHA_InvDev->Checked()) {
		if (!m_pCHA_DevVal->Checked() && !m_pCHA_Switch->Checked()) {
			m_pCHA_InvDev->SetChecked(true);
		}
	} else {
		m_pCHA_DevVal->SetChecked(false);
		m_pCHA_Switch->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_CBA_Next
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CBA_Next() -> void {
	if (!m_pCBA_Next->Checked()) {
		if (!m_pCBA_Prev->Checked() && !m_pCBA_Rand->Checked() && !m_pCBA_Set->Checked()) {
			m_pCBA_Next->SetChecked(true);
		}
	} else {
		m_pCBA_Prev->SetChecked(false);
		m_pCBA_Rand->SetChecked(false);
		m_pCBA_Set->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_CBA_Prev
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CBA_Prev() -> void {
	if (!m_pCBA_Prev->Checked()) {
		if (!m_pCBA_Next->Checked() && !m_pCBA_Rand->Checked() && !m_pCBA_Set->Checked()) {
			m_pCBA_Prev->SetChecked(true);
		}
	} else {
		m_pCBA_Next->SetChecked(false);
		m_pCBA_Rand->SetChecked(false);
		m_pCBA_Set->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_CBA_Rand
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CBA_Rand() -> void {
	if (!m_pCBA_Rand->Checked()) {
		if (!m_pCBA_Prev->Checked() && !m_pCBA_Next->Checked() && !m_pCBA_Set->Checked()) {
			m_pCBA_Rand->SetChecked(true);
		}
	} else {
		m_pCBA_Prev->SetChecked(false);
		m_pCBA_Next->SetChecked(false);
		m_pCBA_Set->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_CBA_Set
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_CBA_Set() -> void {
	if (!m_pCBA_Set->Checked()) {
		if (!m_pCBA_Prev->Checked() && !m_pCBA_Rand->Checked() && !m_pCBA_Next->Checked()) {
			m_pCBA_Set->SetChecked(true);
		}
	} else {
		m_pCBA_Prev->SetChecked(false);
		m_pCBA_Rand->SetChecked(false);
		m_pCBA_Next->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_TBA_DevVal
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_DevVal() -> void {
	if (!m_pTBA_DevVal->Checked()) {
		if (!m_pTBA_InvDev->Checked()) {
			m_pTBA_DevVal->SetChecked(true);
		}
	} else {
		m_pTBA_InvDev->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_TBA_InvDev
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_InvDev() -> void {
	if (!m_pTBA_InvDev->Checked()) {
		if (!m_pTBA_DevVal->Checked()) {
			m_pTBA_InvDev->SetChecked(true);
		}
	} else {
		m_pTBA_DevVal->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_TBA_Set
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_Set() -> void {
	if (!m_pTBA_Set->Checked()) {
		if (!m_pTBA_Add->Checked() && !m_pTBA_Rand->Checked() && !m_pTBA_Fade->Checked()) {
			m_pTBA_Set->SetChecked(true);
		}
	} else {
		m_pTBA_Add->SetChecked(false);
		m_pTBA_Rand->SetChecked(false);
		m_pTBA_Fade->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_TBA_Add
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_Add() -> void {
	if (!m_pTBA_Add->Checked()) {
		if (!m_pTBA_Set->Checked() && !m_pTBA_Rand->Checked() && !m_pTBA_Fade->Checked()) {
			m_pTBA_Add->SetChecked(true);
		}
	} else {
		m_pTBA_Set->SetChecked(false);
		m_pTBA_Rand->SetChecked(false);
		m_pTBA_Fade->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_TBA_Rand
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_Rand() -> void {
	if (!m_pTBA_Rand->Checked()) {
		if (!m_pTBA_Set->Checked() && !m_pTBA_Add->Checked() && !m_pTBA_Fade->Checked()) {
			m_pTBA_Rand->SetChecked(true);
		}
	} else {
		m_pTBA_Set->SetChecked(false);
		m_pTBA_Add->SetChecked(false);
		m_pTBA_Fade->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	 OnChange_TBA_Fade
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_Fade() -> void {
	if (!m_pTBA_Fade->Checked()) {
		if (!m_pTBA_Set->Checked() && !m_pTBA_Add->Checked() && !m_pTBA_Rand->Checked()) {
			m_pTBA_Fade->SetChecked(true);
		}
	} else {
		m_pTBA_Set->SetChecked(false);
		m_pTBA_Add->SetChecked(false);
		m_pTBA_Rand->SetChecked(false);
	}
}


//---------------------------------------------------------------------------//
//	OnChange_TBA_SwapFade
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_TBA_SwapFade() -> void {
	int fr = m_pTBA_FadeFr->GetValue();
	int to = m_pTBA_FadeTo->GetValue();
	m_pTBA_FadeFr->Set(to);
	m_pTBA_FadeTo->Set(fr);
}


//---------------------------------------------------------------------------//
//	OnChange_BTNA_Activate
//---------------------------------------------------------------------------//
auto CMGWindowVarLinker::OnChange_BTNA_Activate() -> void {
	// ebp-> nothing else in this actions list, so keep Activate true...
	if (!m_pBTNA_Activate->Checked()) {
		m_pBTNA_Activate->SetChecked(true);
	}
}
