//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "MGPanelFFT.h"
#include "Controls/MGLabel.h"
#include "Controls/MGButton.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGPanelFFT::CMGPanelFFT(CMGControl* pParent, int iX, int iY, int iW, int iH, int iBars)
	: CMGControl(pParent, iX, iY, iW, iH)
	, m_bPaint(false)
	, m_iBars(iBars)
{
	m_iAlign = MGALIGN_NONE;
	memset(m_afFFT, 0, FFT_SIZE_256 * sizeof(float));
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGPanelFFT::~CMGPanelFFT() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGPanelFFT\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Update 
//---------------------------------------------------------------------------//
auto CMGPanelFFT::Update(float const* pFFT) -> void {
	m_bPaint = true;
	if (pFFT) {
		memcpy(m_afFFT, pFFT, FFT_SIZE_256 * sizeof(float));
	} else {
		memset(m_afFFT, 0, FFT_SIZE_256 * sizeof(float));
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGPanelFFT::DerivDraw() -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::FillRect(pSkin, TRect(Left(), Top(), Width(), Height()), COLOR_EDIT_BG);
	neon::gui::Rect(pSkin, TRect(Left(), Top(), Width(), Height()), COLOR_EDIT_BORDER);

	float fBar = 255.f / m_iBars;
	float fWidth = (float)(Width() - 3) / m_iBars;

	// todo: rework this after initial portaudio / fft checkin.
	for (int i = 0; i < m_iBars; ++i) {
		int fr = Floor(fBar * (i + 0));
		int to = Floor(fBar * (i + 1));
		float fLevel = 0.f;
		for (int j = fr; j <= to; ++j) {
			if (m_afFFT[j] > fLevel) {
				fLevel = m_afFFT[j];
			}
		}

		float r = fLevel;
		float g = 1 - r;
		int x0 = Floor(fWidth * (i + 0)) + Left() + 2;
		int x1 = Floor(fWidth * (i + 1) - 1) + Left() + 2;
		int h = Floor(fLevel * (Height() - 4));
		int y0 = Top() + (Height() - 2) - h;

		neon::gui::GradRect(TRect(x0, y0, x1 - x0, h), MGColor(r, g, 0), MGColor(0, 1, 0));
	}
}
