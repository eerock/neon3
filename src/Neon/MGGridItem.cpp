//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "NeonPch.h"
#include "NeonMain.h"
#include "MGSource.h"
#include "Controls/MGGrid.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGGridItem::CMGGridItem()
	: CMGControl(nullptr, 0, 0, 0, 0), m_pSource(nullptr), m_bMaster(false)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGGridItem::~CMGGridItem() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGGridItem\n"));
#endif
	DELETE_PTR(m_pSource);
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMGGridItem::Init(bool bMaster) -> void {
	m_bMaster = bMaster;
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGGridItem::Update() -> void {
	if (m_pSource) {
		m_pSource->Update();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGGridItem::OnMouseDown(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
	//if (iButton == MOUSE_RIGHT) {
	//	GLOG(("W00T!  Grid Item %s clicked at (%d, %d)!\n", m_pSource ? m_pSource->GetName() : "<Empty>", iX, iY));
	//}
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CMGGridItem::Draw(int x, int y, int w, int h, bool bSelected) -> void {
	UNREFERENCED_PARAMETER(w);
	UNREFERENCED_PARAMETER(h);
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	const int headerH = 16;

	if (m_bMaster) {
		// MASTER GRID
		if (!m_pSource) {
			// No source loaded
			return;
		}

		// top grid item header
		neon::gui::DrawItem(pSkin, TRect(70, 160, 70, 15), TRect(x, y, w, headerH));

		// snapshot image
		neon::gui::Stretch(m_pSource->GetBitmap(), TRect(x + 1, y + headerH, w - 2, h - headerH));

		// Draw the text...
		neon::gui::SetClipRect(TRect(x, y + h - 20, w, 15));
		neon::gui::Text(pSkin, FONT_MINI, x + 3, y + h - 20 + 3, 0, 12, CMGFont::LEFT, CMGFont::VCENTER, COLOR_EDIT_BORDER, m_pSource->GetName());
		neon::gui::Text(pSkin, FONT_MINI, x + 2, y + h - 20 + 2, 0, 12, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT_ACTIVE, m_pSource->GetName());
		neon::gui::SetClipRect();

		// border
		if (bSelected) {
			neon::gui::Rect(pSkin, TRect(x, y + headerH, w, h - headerH), COLOR_EDIT_BG_ACTIVE);
		}

	} else {
		// NORMAL GRID
		if (!m_pSource) {
			return;
		}

		// top grid item header
		if (m_pSource->IsAttached()) {
			neon::gui::DrawItem(pSkin, TRect(0, 160, 70, 15), TRect(x, y, w, headerH));
		} else {
			neon::gui::DrawItem(pSkin, TRect(70, 175, 70, 15), TRect(x, y, w, headerH));
		}

		// snapshot image
		neon::gui::Stretch(m_pSource->GetBitmap(), TRect(x + 1, y + headerH, w - 2, h - headerH));

		// Draw the text...
		neon::gui::SetClipRect(TRect(x, y + h - 20, w, 15));
		if (m_pSource->IsAttached()) {
			neon::gui::Text(pSkin, FONT_MINI, x + 3, y + h - 20 + 3, 0, 12, CMGFont::LEFT, CMGFont::VCENTER, COLOR_EDIT_BORDER, m_pSource->GetName());
			neon::gui::Text(pSkin, FONT_MINI, x + 2, y + h - 20 + 2, 0, 12, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT, m_pSource->GetName());
		} else {
			neon::gui::Text(pSkin, FONT_MINI, x + 3, y + h - 20 + 3, 0, 12, CMGFont::LEFT, CMGFont::VCENTER, COLOR_EDIT_BORDER, m_pSource->GetName());
			neon::gui::Text(pSkin, FONT_MINI, x + 2, y + h - 20 + 2, 0, 12, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT_ACTIVE, m_pSource->GetName());
		}
		neon::gui::SetClipRect();

		// border
		if (bSelected) {
			neon::gui::Rect(pSkin, TRect(x, y + headerH, w, h - headerH), COLOR_EDIT_BG_ACTIVE);
		}
	}
}
