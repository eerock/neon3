//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGSURFACE_H
#define NEON_MGSURFACE_H

#include "Base.h"
#include "tga.h"

struct MGColor {
	MGColor() : r(0.f), g(0.f), b(0.f) {}
	MGColor(float _r, float _g, float _b) : r(_r), g(_g), b(_b) {}
	float r;
	float g;
	float b;
};

struct TMGSurface {
	TMGSurface() : uTextureID(INVALID_ID), w(0), h(0), fInvW(0.f), fInvH(0.f), tTga() {}
	uint uTextureID;
	int w;
	int h;
	float fInvW;
	float fInvH;
	LTGA tTga;
};

#endif//NEON_MGSURFACE_H
