//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//


#include "MGPch.h"
#include "MGSkin.h"
#include "MGDisplay.h"
#include "Xml.h"

static const float s_fColorScale = 1.f / 255.f;

IAppSkinService* CServiceLocator<IAppSkinService>::s_pService = nullptr;


//---------------------------------------------------------------------------//
//	SafeColor
//---------------------------------------------------------------------------//
static auto SafeColor(TiXmlElement* pElem, std::string const& sChild,
					  MGColor const& Default = MGColor()) -> MGColor {
	MGColor uRes = Default;
	std::string const& sColor = neon::SafeGet<std::string>(pElem, sChild.c_str());
	if (!sColor.empty()) {
		uint color;
		std::stringstream ss(sColor);
		ss >> std::hex >> color;
		uRes.r = static_cast<float>((color >> 16) & 0xFF) * s_fColorScale;
		uRes.g = static_cast<float>((color >> 8) & 0xFF) * s_fColorScale;
		uRes.b = static_cast<float>((color) & 0xFF) * s_fColorScale;
	}
	return uRes;
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGSkin::CMGSkin()
	: m_pInterface(nullptr)
{
	for (int i = 0; i < MAX_FONTS; ++i) {
		m_apFonts[i] = nullptr;
	}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGSkin::~CMGSkin() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGSkin\n"));
#endif
	neon::gui::FreeImg(std::move(m_pInterface));
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMGSkin::Init(THInst hInstance, std::string const& sDir) -> bool {
	UNREFERENCED_PARAMETER(hInstance);
	bool bRes = false;

	PushDir(sDir);

	// Load Interface image...
	m_pInterface = neon::gui::LoadTGAImg("interface.tga");

	// Load Interface colors description...
	TiXmlDocument XMLDoc("interface.xml");
	if (XMLDoc.LoadFile() && XMLDoc.FirstChild("interface")) {
		bRes = true;

		auto pMain = XMLDoc.FirstChild("interface");

		// Load Colors
		auto pColors = pMain->FirstChild("colors")->ToElement();
		m_atColors[COLOR_FONT_LABEL] = SafeColor(pColors, "font_label");
		m_atColors[COLOR_FONT_BUTTON] = SafeColor(pColors, "font_button");
		m_atColors[COLOR_FONT_WINDOW_TITLE] = SafeColor(pColors, "font_window_title");
		m_atColors[COLOR_FONT_EDIT] = SafeColor(pColors, "font_edit");
		m_atColors[COLOR_FONT_EDIT_ACTIVE] = SafeColor(pColors, "font_edit_active");
		m_atColors[COLOR_TAB_BG] = SafeColor(pColors, "tab_bg");
		m_atColors[COLOR_BASE_BG] = SafeColor(pColors, "base_bg");
		m_atColors[COLOR_BASE_BORDER] = SafeColor(pColors, "base_border");
		m_atColors[COLOR_BASE_BORDER_ACTIVE] = SafeColor(pColors, "base_border_active");
		m_atColors[COLOR_GRID_BG] = SafeColor(pColors, "grid_bg");
		m_atColors[COLOR_GRID_BG_ACTIVE] = SafeColor(pColors, "grid_bg_active");
		m_atColors[COLOR_GRID_BORDER] = SafeColor(pColors, "grid_border");
		m_atColors[COLOR_GRID_BORDER_ACTIVE] = SafeColor(pColors, "grid_border_active");
		m_atColors[COLOR_GRID_CELL_ATTACHED] = SafeColor(pColors, "grid_cell_attached");
		m_atColors[COLOR_GRID_CELL_HOVER] = SafeColor(pColors, "grid_cell_hover");
		m_atColors[COLOR_EDIT_BG] = SafeColor(pColors, "edit_bg");
		m_atColors[COLOR_EDIT_BG_ACTIVE] = SafeColor(pColors, "edit_bg_active");
		m_atColors[COLOR_EDIT_BORDER] = SafeColor(pColors, "edit_border");
		m_atColors[COLOR_EDIT_BORDER_ACTIVE] = SafeColor(pColors, "edit_border_active");

		// Load Fonts
		m_apFonts[FONT_MINI] = std::make_unique<CMGFont>();
		m_apFonts[FONT_MINI]->Init("Arial", 4, 12, false, false, true);
		m_apFonts[FONT_NORMAL] = std::make_unique<CMGFont>();
		m_apFonts[FONT_NORMAL]->Init("Arial", 5, 14, false, false, true);
		m_apFonts[FONT_WINDOW_TITLE] = std::make_unique<CMGFont>();
		m_apFonts[FONT_WINDOW_TITLE]->Init("Arial", 6, 14, true, false, true);
		m_apFonts[FONT_FX_TITLE] = std::make_unique<CMGFont>();
		m_apFonts[FONT_FX_TITLE]->Init("Arial", 10, 20, true, false, true);
		m_apFonts[FONT_BPM] = std::make_unique<CMGFont>();
		m_apFonts[FONT_BPM]->Init("Arial", 14, 32, true, false, true);

#if (PLATFORM == PLATFORM_WINDOWS)
		// ebp-> todo: !!!! implement cursors
		// Load Cursors
		m_atCursors[CUR_NORMAL] = ::LoadCursorFromFile("normal.cur");
		m_atCursors[CUR_DRAGDROP] = ::LoadCursorFromFile("dragdrop.cur");
		m_atCursors[CUR_DRAGDROP_NO] = ::LoadCursorFromFile("dragdrop_x2.cur");
#endif//PLATFORM

	} else {
		GLOG(("ERROR: Can't load skin \"%s\"\n", sDir.c_str()));
	}

	PopDir();

	return bRes;
}
