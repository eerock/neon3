//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGSpinBox.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGSpinBox::CMGSpinBox(CMGControl* pParent, int iX, int iY, int iSize, int iMin, int iMax, int iStep, function<void()> fnOnChange)
	: CMGControl(pParent, iX, iY, iSize, 20)
	, m_iMouseY(0)
	, m_iCurrentValue(0)
	, m_iValue(0)
	, m_iMax(iMax)
	, m_iMin(iMin)
	, m_iStep(iStep)
	, m_iButtonPressed(NONE)
	, m_fnOnChange(fnOnChange)
{
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGSpinBox::~CMGSpinBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGSpinBox\n"));
#endif
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGSpinBox::OnMouseMove(int iX, int iY) -> void {
	UNREFERENCED_PARAMETER(iX);
	if (m_iButtonPressed != NONE) {
		int iV = ((m_iMouseY - iY) >> 3) * m_iStep;
		Set(iV + m_iCurrentValue);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGSpinBox::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		iX -= Left();
		iY -= Top();
		if (iX > (Width() - 12)) {
			if (iY < 10) {
				Set(m_iValue + m_iStep);
				m_iButtonPressed = UP;
				m_iMouseY = iY + Top();
				m_iCurrentValue = GetValue();
			} else {
				Set(m_iValue - m_iStep);
				m_iButtonPressed = DOWN;
				m_iMouseY = iY + Top();
				m_iCurrentValue = GetValue();
			}
			Focus();
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGSpinBox::OnMouseUp(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY); 
	if (iButton == MOUSE_LEFT) {
		m_iButtonPressed = NONE;
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CMGSpinBox::Set(int iValue) -> void {
	int iOld = m_iValue;
	m_iValue = Clamp(iValue, m_iMin, m_iMax);
	if (iOld != m_iValue) {
		if (m_fnOnChange) {
			m_fnOnChange();
		}
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	SetMinMax
//---------------------------------------------------------------------------//
auto CMGSpinBox::SetMinMax(int iMin, int iMax) -> void {
	m_iMin = iMin;
	m_iMax = iMax;
	Set(m_iValue);
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGSpinBox::DerivDraw() -> void {
	std::stringstream ss;
	ss << m_iValue;

	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::Rect(pSkin, TRect(Left(), Top(), Width(), 20), COLOR_EDIT_BORDER);
	neon::gui::FillRect(pSkin, TRect(Left() + 1, Top() + 1, Width() - 2, 18), COLOR_EDIT_BG);
	neon::gui::SetClipRect(TRect(Left() + 1, Top() + 1, Width() - 2, Height() - 2));
	neon::gui::Text(pSkin, FONT_NORMAL, Left() + 3, Top(), 0, 20, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT, ss.str());

	// magic fuckin numbers

	// Up button
	if (m_iButtonPressed == UP) {
		neon::gui::DrawItem(pSkin, TRect(91, 70, 11, 9), TRect(Left() + Width() - 12, Top() + 1, 11, 9));
	} else {
		neon::gui::DrawItem(pSkin, TRect(80, 70, 11, 9), TRect(Left() + Width() - 12, Top() + 1, 11, 9));
	}

	// Down button
	if (m_iButtonPressed == DOWN) {
		neon::gui::DrawItem(pSkin, TRect(91, 79, 11, 9), TRect(Left() + Width() - 12, Top() + 10, 11, 9));
	} else {
		neon::gui::DrawItem(pSkin, TRect(80, 79, 11, 9), TRect(Left() + Width() - 12, Top() + 10, 11, 9));
	}

	neon::gui::SetClipRect();
}
