//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGBUTTON_H
#define NEON_MGBUTTON_H

#include "MGControl.h"

class CMGButton : public CMGControl {
public:
	enum EButtonType {
		NORMAL,
		MEDIUM,
		SMALL,
		REFRESH,
		SAVE,
		BLEND,
		LINK,
		MAX_BUTTONS,
	};

	CMGButton(CMGControl* pParent, int iX, int iY, int Type, std::string const& sCaption, function<void()> fnOnClick);
	~CMGButton() override;

	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;

	auto Trigger() const -> void;

	auto IsLinked() const -> bool { return m_bLinked; }
	auto SetLinked(bool bLinked) -> void { m_bLinked = bLinked; Repaint(); }

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGButton\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

protected:
	std::string m_sCaption;
	bool m_bPressed;
	bool m_bLinked;
	int m_iType;
	function<void()> m_fnOnClick;
};

#endif//NEON_MGBUTTON_H
