//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGSPINBOX_H
#define NEON_MGSPINBOX_H

#include "MGControl.h"

class CMGSpinBox : public CMGControl {
public:
	CMGSpinBox(CMGControl* pParent, int iX, int iY, int iSize, int iMin, int iMax, int iStep, function<void()> fnOnChange);
	~CMGSpinBox() override;

	virtual auto GetValue() const -> int { return m_iValue; }
	virtual auto Set(int iValue) -> void;
	virtual auto SetMinMax(int iMin, int iMax) -> void;

	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;

	virtual auto OnChange() -> void {}

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGSpinBox\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	enum {
		NONE,
		UP,
		DOWN,
	};

	int m_iMouseY;
	int m_iCurrentValue;
	int m_iValue;
	int m_iMax;
	int m_iMin;
	int m_iStep;
	int m_iButtonPressed;
	function<void()> m_fnOnChange;
};

#endif//NEON_MGSPINBOX_H
