//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGLabel.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGLabel::CMGLabel(CMGControl* pParent, int iX, int iY, uint Align, int iFont, std::string const& sText)
	: CMGControl(pParent, iX, iY, 0, 0)
	, m_fnOnClick(NULL)
	, m_iFont(iFont)
{
	m_iAlign = Align;
	Set(sText);
	OnResize();
}


CMGLabel::CMGLabel(CMGControl* pParent, int iX, int iY, uint iAlign, int iFont, std::string const& sText, function<void()> fnOnClick)
	: CMGControl(pParent, iX, iY, 0, 0)
	, m_fnOnClick(fnOnClick)
	, m_iFont(iFont)
{
	m_iAlign = iAlign;
	Set(sText);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGLabel::~CMGLabel() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGLabel\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Set
//---------------------------------------------------------------------------//
auto CMGLabel::Set(std::string const& sText) -> void {
	m_sText = sText;
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	SetSize(pSkin->Font(m_iFont).TextLength(m_sText) + 2, pSkin->Font(m_iFont).TextHeight());
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGLabel::OnMouseDown(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT) {
		if (m_fnOnClick) {
			m_fnOnClick();
		}
	}
}

//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGLabel::DerivDraw() -> void {
	neon::gui::Text(
		CServiceLocator<IAppSkinService>::GetService(),
		m_iFont,
		Left(), Top(), 0, Height(),
		CMGFont::LEFT,
		CMGFont::VCENTER,
		COLOR_FONT_EDIT,
		m_sText.c_str()
		);
}
