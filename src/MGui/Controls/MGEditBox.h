//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGEDITBOX_H
#define NEON_MGEDITBOX_H

#include "MGControl.h"


//---------------------------------------------------------------------------//
//	Class CMGEditBox
//	maybe this class should be initialized with a max string length (instead
//	of iSize in pixels) because the actual width can be calculated from
//	the max string length and the text won't run off the end.
//
//	Need to fix selection drawing.
//---------------------------------------------------------------------------//
class CMGEditBox : public CMGControl {
public:
	CMGEditBox(CMGControl* pParent, int iX, int iY, int iSize, function<void()> fnOnChange);
	~CMGEditBox() override;

	virtual auto SetText(std::string const& sText) -> void;
	virtual auto GetText() const -> std::string const& { return m_sText; }
	virtual auto OnChange() -> void;
	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	auto OnMouseDoubleClk(int iX, int iY, int iButton) -> void override;
	auto OnVKeyDown(int iVKey) -> void override;
	auto OnChar(char c) -> void override;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGEditBox\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	function<void()> m_fnOnChange;
	std::string m_sText;
	int m_iPos;
	int m_iPosSel;
	bool m_bSelecting;

	virtual auto GetPosFromChar(int iChar) -> int;
	virtual auto UpdateCursorPos(int iX) -> void;
	virtual auto DeleteSelected() -> void;
};

#endif//NEON_MGEDITBOX_H
