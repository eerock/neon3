//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGPAGECONTROL_H
#define NEON_MGPAGECONTROL_H

#include "MGControl.h"

class CMGPageControl;

//---------------------------------------------------------------------------//
//	Class CMGPage
//---------------------------------------------------------------------------//
class CMGPage : public CMGControl {
public:
	CMGPage(CMGPageControl* pPageControl, std::string const& sCaption);
	~CMGPage() override;

	auto GetCaption() -> std::string const& { return m_sCaption; }

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGPage\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	std::string m_sCaption;
};


//---------------------------------------------------------------------------//
//	Class CMGPageControl
//---------------------------------------------------------------------------//
class CMGPageControl : public CMGControl {
public:
	CMGPageControl(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align = MGALIGN_NONE, function<void()> fnOnSelectPage = NULL);
	~CMGPageControl() override;

	auto SetActivePage(int Page) -> void;
	auto SetActivePage(CMGPage* pPage) -> void;
	auto AddPage(CMGPage* pPage) -> void;
	auto GetActivePage() const -> int { return m_iActivePage; }

	auto OnMouseDown(int iX, int iY, int iButton) -> void override;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGPageControl\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	std::vector<CMGPage*> m_vPages;
	CMGPage* m_pActivePage;
	int m_iActivePage;
	function<void()> m_fnOnSelectPage;
};

#endif//NEON_MGPAGECONTROL_H
