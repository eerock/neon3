//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGTREEVIEW_H
#define NEON_MGTREEVIEW_H

#include "MGControl.h"

class CMGTreeView;
class CMGTreeItem;
class CMGScrollBar;

//---------------------------------------------------------------------------//
// Tree Item
//
//---------------------------------------------------------------------------//
class CMGTreeItem {
public:
	CMGTreeItem(CMGTreeItem* pParent, std::string const& sText, std::string const& sPath, bool Folder);
	~CMGTreeItem();

	auto Expand(bool bAll = false) -> void;
	auto Collapse(bool bAll = false) -> void;

	auto IsFolder() const -> bool { return m_bFolder; }
	auto IsExpanded() const -> bool { return m_bExpanded; }
	auto GetText() const -> std::string const& { return m_sText; }
	auto GetPath() const -> std::string const& { return m_sPath; }

	auto GetItemAt(int iX, int iY) -> CMGTreeItem*;
	auto GetParent() const -> CMGTreeItem* { return m_pParent; }

	auto SetPos(int iX, int iY) -> void;
	auto SetSelected(bool Selected) -> void { m_bSelected = Selected; }

	auto Left() const -> int { return m_tRect.x; }
	auto Top() const -> int { return m_tRect.y; }
	auto Width() const -> int { return m_tRect.w; }
	auto Height() const -> int { return m_tRect.h; }

	auto Draw(int iYOffset) -> void;

	auto GetTotalHeight() const -> int { return m_iTotalHeight; }
	auto GetSonsHeight() const -> int { return m_iSonsHeight; }
	auto GetLastHeight() const -> int { return m_iLastHeight; }

	auto CalcHeight() -> int;

#ifdef _DEBUG
	virtual auto PrintHierarchy (std::string const& sIndent) const -> void {
		GLOG(("%sMGTreeItem\n", sIndent.c_str()));
		std::string sChildIndent = sIndent + "  ";
		for (auto& ch : m_vChildren) {
			ch->PrintHierarchy(sChildIndent);
		}
		//CListIter<CMGTreeItem*> oIter(m_rgpSons.Head());
		//while (oIter.GetNode()) {
		//	auto pItem = *oIter;
		//	pItem->PrintHierarchy(sChildIndent);
		//	++oIter;
		//}
	}
#endif

private:
	bool m_bSelected;
	bool m_bExpanded;
	bool m_bFolder;
	int m_iTotalHeight;
	int m_iSonsHeight;
	int m_iLastHeight;
	std::string m_sText;
	std::string m_sPath;
	TRect m_tRect;
	CMGTreeItem* m_pParent;
	//CList<CMGTreeItem*> m_rgpSons;
	std::vector<CMGTreeItem*> m_vChildren;
};



//---------------------------------------------------------------------------//
// Tree View
//
//---------------------------------------------------------------------------//
class CMGTreeView : public CMGControl {
public:
	CMGTreeView(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align = MGALIGN_NONE);
	~CMGTreeView() override;

	auto Update() -> void;
	auto Clear() -> void;
	auto SetRootItem(CMGTreeItem* pItem) -> void { m_pRoot = pItem; }
	auto GetRoot() const -> CMGTreeItem* { return m_pRoot; }
	auto GetSelected() const -> CMGTreeItem* { return m_pSelected; }

	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	auto OnMouseDoubleClk(int iX, int iY, int iButton) -> void override;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGTreeView\n", sIndent.c_str()));
		if (m_pRoot) {
			std::string sChildIndent = sIndent + "  ";
			m_pRoot->PrintHierarchy(sChildIndent);
		}
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	virtual auto OnChangeScroll() -> void;
	auto DerivDraw() -> void override;
	auto GetItemAt(int iX, int iY) -> CMGTreeItem*;

private:
	auto SelectItem(CMGTreeItem* pItem) -> void;

	CMGTreeItem* m_pRoot;
	CMGTreeItem* m_pSelected;
	CMGScrollBar* m_pScrollBar;
};

#endif//NEON_MGTREEVIEW_H
