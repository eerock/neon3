//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGCOMBOBOX_H
#define NEON_MGCOMBOBOX_H

#include "MGControl.h"

class CMGScrollBar;

class CMGComboBox : public CMGControl
{
public:
	CMGComboBox(CMGControl* pParent, int iX, int iY, int iWidth, int iMaxHeight, bool bNotifyAlways, function<void()> fOnChange);
	~CMGComboBox() override;

	virtual auto Clear() -> void;
	virtual auto Add(std::string const& sText) -> void;
	virtual auto IndexOf(std::string const& sText) -> int;
	virtual auto Select(int iIndex) -> void;
	virtual auto GetSelected() const -> int { return m_iSelected; }
	virtual auto GetText() const -> std::string const& { return m_sText; }
	virtual auto GetNumItems() const -> int { return static_cast<int>(m_vStrings.size()); }
	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	virtual auto OnChange() -> void {}
	auto OnChangeScroll() -> void;
	auto SetUnfolded(bool bUnfolded) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGComboBox\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	function<void()> m_fnOnChange;
	std::vector<std::string> m_vStrings;
	std::string m_sText;
	int m_iSelected;
	bool m_bUnfolded;
	bool m_bButtonPressed;
	bool m_bNotifyAlways;
	uint m_uMaxHeight;
	uint m_uItemHeight;
	uint m_uUnfoldedHeight;
	CMGScrollBar* m_pScrollBar;
};

#endif//NEON_MGCOMBOBOX_H

