//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGRADIOGROUP_H
#define NEON_MGRADIOGROUP_H

#include "MGControl.h"

class CMGRadioGroup : public CMGControl {
public:
	CMGRadioGroup(CMGControl* pParent, int iX, int iY, int iW, int iH);
	~CMGRadioGroup() override;

	auto Add(std::string const& sItem) -> void;
	auto Clear() -> void;

	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;

	auto Select(int iItem) -> void;
	auto Select(std::string const& sText) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGRadioGroup\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

	function<void(CMGRadioGroup* pRadioGroup)> m_fnOnSelectItem;

protected:
	auto DerivDraw() -> void override;
	auto GetItemIndexAt(int iX, int iY) -> int;

private:
	int m_iItemHeight;
	int m_iSelected;
	std::vector<std::string> m_vsItems;
};

#endif//NEON_MGRADIOGROUP_H
