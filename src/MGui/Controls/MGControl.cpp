//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGControl.h"

//#define DEBUG_DRAW_BORDERS

//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGControl::CMGControl(CMGControl* pParent, int iX, int iY, int iW, int iH)
	: m_sName()
	, m_iAlign(MGALIGN_NONE)
	, m_bAddRectAlign(true)
	, m_bVisible(true)
	, m_bEnabled(true)
	, m_bFocused(false)
	, m_tRect(iX, iY, iW, iH)
	, m_tFreeRect(iX, iY, iW, iH)
	, m_tBorder(0, 0, 0, 0)
	, m_pParent(nullptr)
	, m_vChildren()
{
	m_tColor = CServiceLocator<IAppSkinService>::GetService()->Color(COLOR_BASE_BG);
	m_iColor = COLOR_BASE_BG;
	SetParent(pParent);
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGControl::~CMGControl() {
#ifdef DESTRUCTOR_VERBOSE
	//GLOG(("~CMGControl\n"));
#endif
	for (auto it = m_vChildren.begin(); it != m_vChildren.end(); ++it) {
		DELETE_PTR((*it));
	}
	m_vChildren.clear();
}


//---------------------------------------------------------------------------//
//	SetParent
//---------------------------------------------------------------------------//
auto CMGControl::SetParent(CMGControl* pParent) -> void {
	if (m_pParent != nullptr) {
		m_pParent->DelSon(this);
	}

	m_pParent = pParent;

	if (m_pParent != nullptr) {
		m_pParent->AddSon(this);
	}
}


//---------------------------------------------------------------------------//
//	AddSon
//---------------------------------------------------------------------------//
auto CMGControl::AddSon(CMGControl* pSon) -> void {
	m_vChildren.push_back(pSon);
}


//---------------------------------------------------------------------------//
//	DelSon
//---------------------------------------------------------------------------//
auto CMGControl::DelSon(CMGControl* pSon) -> void {
	for (auto it = m_vChildren.begin(); it != m_vChildren.end(); ++it) {
		if ((*it) == pSon) {
			it = m_vChildren.erase(it);
			return;
		}
	}
}


//---------------------------------------------------------------------------//
//	AddRectAlign
//	Se llama cuando un hijo con align ya ha ajustado su tamaño para que los
//	sucesivos hijos puedan adaptarse tambien
//	es-en: Called when a child has adjusted to align its size to accommodate
//	successive children can also
//---------------------------------------------------------------------------//
auto CMGControl::AddRectAlign(CMGControl* pItem) -> void {
	switch (pItem->GetAlign()) {
		case MGALIGN_NONE: {
			break;
		}
		case MGALIGN_LEFT: {
			m_tFreeRect.x += pItem->Width();
			m_tFreeRect.w -= pItem->Width();
			break;
		}
		case MGALIGN_RIGHT: {
			m_tFreeRect.w -= pItem->Width();
			break;
		}
		case MGALIGN_TOP: {
			m_tFreeRect.y += pItem->Height();
			m_tFreeRect.h -= pItem->Height();
			break;
		}
		case MGALIGN_BOTTOM: {
			m_tFreeRect.h -= pItem->Height();
			break;
		}
		case MGALIGN_CLIENT: {
			SetRect(m_tFreeRect, 0, 0, 0, 0);
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetItemAt
//	ebp-> should this return a shared_ptr?
//---------------------------------------------------------------------------//
auto CMGControl::GetItemAt(int iX, int iY) -> CMGControl* {
	if (IsInside(iX, iY)) {
		// Mirar si algun hijo esta dentro
		// es-en: see if any child is inside.
		for (auto& ch : m_vChildren) {
			auto pInside = ch->GetItemAt(iX, iY);
			if (pInside) {
				return pInside;
			}
		}
		return this;
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	IsInside
//---------------------------------------------------------------------------//
auto CMGControl::IsInside(int iX, int iY) -> bool {
	return (
		IsEnabled() &&
		IsVisible() &&
		(iX >= Left()) && 
		(iX <= (Left() + Width())) && 
		(iY >= Top()) && 
		(iY <= (Top() + Height()))
	);
}


//---------------------------------------------------------------------------//
//	UpdateSize
//---------------------------------------------------------------------------//
auto CMGControl::UpdateSize() -> void {
	//if (m_iAlign != MGALIGN_NONE) {
		TRect tRect;
		if (m_pParent) {
			m_pParent->GetFreeRect(tRect);
		} else {
			SetRect(tRect, 0, 0, neon::gui::GetWidth(), neon::gui::GetHeight());
		}

		switch (m_iAlign) {
			case MGALIGN_LEFT: {
				SetPos(tRect.x, tRect.y);
				SetSize(Width(), tRect.h);
				break;
			}
			case MGALIGN_RIGHT: {
				SetPos(tRect.x + tRect.w - Width(), tRect.y);
				SetSize(Width(), tRect.h);
				break;
			}
			case MGALIGN_TOP: {
				SetPos(tRect.x, tRect.y);
				SetSize(tRect.w, Height());
				break;
			}
			case MGALIGN_BOTTOM: {
				SetPos(tRect.x, tRect.y + tRect.h - Height());
				SetSize(tRect.w, Height());
				break;
			}
			case MGALIGN_CLIENT: {
				SetPos(tRect.x, tRect.y);
				SetSize(tRect.w, tRect.h);
				break;
			}
			default: {
				break;
			}
		}
	//}
}


//---------------------------------------------------------------------------//
//	UpdateRect
//---------------------------------------------------------------------------//
auto CMGControl::UpdateRect() -> void {
	// Update
	if (m_iAlign != MGALIGN_NONE && m_pParent && m_bAddRectAlign) {
		m_pParent->AddRectAlign(this);
	}

	// Reset del freerect
	m_tFreeRect = TRect(0, 0, m_tRect.w, m_tRect.h);
	m_tFreeRect.x += m_tBorder.x;
	m_tFreeRect.y += m_tBorder.y;
	m_tFreeRect.w -= m_tBorder.x + m_tBorder.w;
	m_tFreeRect.h -= m_tBorder.y + m_tBorder.h;
}


//---------------------------------------------------------------------------//
//	OnResize
//---------------------------------------------------------------------------//
auto CMGControl::OnResize() -> void {
	UpdateSize();
	UpdateRect();

	// Resize sons
	for (auto& ch : m_vChildren) {
		ch->OnResize();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGControl::OnMouseMove(int iX, int iY) -> void {
	for (auto& ch : m_vChildren) {
		if (ch->IsInside(iX, iY)) {
			ch->OnMouseMove(iX, iY);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDoubleClk
//---------------------------------------------------------------------------//
auto CMGControl::OnMouseDoubleClk(int iX, int iY, int iButton) -> void {
	// ebp-> default behavior is same as mouse down...
	OnMouseDown(iX, iY, iButton);
}


//---------------------------------------------------------------------------//
//	OnDragOver
//---------------------------------------------------------------------------//
auto CMGControl::OnDragOver(CMGControl* pSource, int iX, int iY, bool& bAccept) -> void {
	UNREFERENCED_PARAMETER(pSource);
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	bAccept = false;
}


//---------------------------------------------------------------------------//
//	OnDragDrop
//---------------------------------------------------------------------------//
auto CMGControl::OnDragDrop(CMGControl* pSource, int iX, int iY) -> void {
	UNREFERENCED_PARAMETER(pSource);
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
}


//---------------------------------------------------------------------------//
//	BeginDrag
//---------------------------------------------------------------------------//
auto CMGControl::BeginDrag() -> void {
	g_pMGApp->DragItem(this);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CMGControl::Draw() -> void {
	if (!IsVisible()) {
		return;
	}

	// Draw de la derivada
	DerivDraw();

#ifdef DEBUG_DRAW_BORDERS
	neon::gui::Rect(CServiceLocator<IAppSkinService>::GetService(), TRect(Left(), Top(), Width(), Height()), COLOR_GRID_CELL_HOVER);
	//neon::gui::Rect(CServiceLocator<IAppSkinService>::GetService(), TRect(m_tFreeRect.x, m_tFreeRect.y + m_tFreeRect.h, m_tFreeRect.w, m_tFreeRect.h), COLOR_GRID_CELL_HOVER);
#endif//DEBUG_DRAW_BORDERS

	// Draw the children
	for (auto& ch : m_vChildren) {
		if (ch->m_bVisible) {
			ch->Draw();
		}
	}
}


//---------------------------------------------------------------------------//
//	Repaint
//---------------------------------------------------------------------------//
auto CMGControl::Repaint() -> void {
	g_pMGApp->RepaintItem(this);
}


//---------------------------------------------------------------------------//
//	Focus
//---------------------------------------------------------------------------//
auto CMGControl::Focus() -> void {
	g_pMGApp->FocusItem(this);
	Repaint();
}
