//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGGroupBox.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
//CMGGroupBox::CMGGroupBox(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align, MGColor const& cBorderColor)
CMGGroupBox::CMGGroupBox(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align, int iBorderColor)
	: CMGControl(pParent, iX, iY, iW, iH) {
	m_iAlign = Align;
	m_tColor = CServiceLocator<IAppSkinService>::GetService()->Color(COLOR_BASE_BG);
	//m_BorderColor = cBorderColor;
	m_iBorderColor = iBorderColor;
	SetRect(m_tBorder, 2, 2, 2, 2);	// magic fuckin numbers
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGGroupBox::~CMGGroupBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGGroupBox\n"));
#endif
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGGroupBox::DerivDraw() -> void {
	neon::gui::Rect(CServiceLocator<IAppSkinService>::GetService(), TRect(Left() + 1, Top() + 1, Width() - 2, Height() - 2), m_iBorderColor);
}
