//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGListBox.h"
#include "MGScrollBar.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGListBox::CMGListBox(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align, bool bScrollBar)
	: CMGControl(pParent, iX, iY, iW, iH)
	, m_fnOnSelectItem(NULL)
	, m_fnOnDragOver(NULL)
	, m_fnOnDragDrop(NULL)
	, m_iItemHeight(0)
	, m_iSelected(-1)
	, m_vsItems()
	, m_pScrollBar(nullptr)
{
	m_sName = "ListBox";
	m_iAlign = Align;
	m_tBorder = TRect(2, 2, 2, 2);
	m_iItemHeight = CServiceLocator<IAppSkinService>::GetService()->Font(FONT_NORMAL).TextHeight() + 2;
	m_pScrollBar = bScrollBar ? NEW CMGScrollBar(this, true, bind(&CMGListBox::OnChangeScroll, this)) : nullptr;
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGListBox::~CMGListBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGListBox\n"));
#endif
	//DISPOSE(m_pScrollBar);
}


//---------------------------------------------------------------------------//
//	Add
//---------------------------------------------------------------------------//
auto CMGListBox::Add(std::string const& sItem) -> void {
	m_vsItems.push_back(sItem);
	Update();
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
auto CMGListBox::Clear() -> void {
	m_iSelected = -1;
	m_vsItems.clear();
	Update();
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGListBox::Update() -> void {
	if (m_pScrollBar) {
		m_pScrollBar->SetVisible(GetTotalHeight() > Height());
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	GetTotalHeight
//---------------------------------------------------------------------------//
auto CMGListBox::GetTotalHeight() -> int {
	return (static_cast<int>(m_vsItems.size()) * m_iItemHeight);
}


//---------------------------------------------------------------------------//
//	GetItemIndexAt
//---------------------------------------------------------------------------//
auto CMGListBox::GetItemIndexAt(int iX, int iY) -> int {
	int iIndex = -1;
	iX -= Left();
	iY -= Top();
	if (iX >= 0 && iX < Width() && iY >= 0 && iY < Height()) {
		if (GetTotalHeight() >= Height() || iY < GetTotalHeight()) {
			int Offset = iY - 2 + (m_pScrollBar && m_pScrollBar->IsVisible() ? (int)(m_pScrollBar->GetPos() * (GetTotalHeight() - Height() + 4)) : 0);
			iIndex = int((float)Offset / (float)m_iItemHeight);
		}
	}
	return iIndex;
}


//---------------------------------------------------------------------------//
//	Select
//---------------------------------------------------------------------------//
auto CMGListBox::Select(int iItem) -> void {
	m_iSelected = iItem;
	if (m_fnOnSelectItem) {
		m_fnOnSelectItem(this);
	}
	Update();
}


//---------------------------------------------------------------------------//
//	Select
//---------------------------------------------------------------------------//
auto CMGListBox::Select(std::string const& sText) -> bool {
	for (size_t i = 0; i < m_vsItems.size(); i++) {
		if (sText == m_vsItems[i]) {
			Select(static_cast<int>(i));
			return true;
		}
	}
	Select(0);
	return false;
}


//---------------------------------------------------------------------------//
//	GetSelected
//---------------------------------------------------------------------------//
auto CMGListBox::GetSelected(std::string& rSelected) -> bool {
	if (m_iSelected != -1) {
		rSelected = m_vsItems[m_iSelected];
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGListBox::OnMouseMove(int iX, int iY) -> void {
	CMGControl::OnMouseMove(iX, iY);
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGListBox::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		int iItem  = GetItemIndexAt(iX, iY);
		if (iItem != -1) {
			Select(iItem);
		}
		SetFocus(true);
	} else if (m_pScrollBar && m_pScrollBar->IsVisible()) {
#define LIST_BOX_SCROLL_SPEED		(0.02f)
		if (iButton == MOUSE_WHEEL_UP) {
			m_pScrollBar->SetPos(m_pScrollBar->GetPos() - LIST_BOX_SCROLL_SPEED);
		} else if (iButton == MOUSE_WHEEL_DOWN) {
			m_pScrollBar->SetPos(m_pScrollBar->GetPos() + LIST_BOX_SCROLL_SPEED);
		}
	}
	Update();
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGListBox::OnMouseUp(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
}


//---------------------------------------------------------------------------//
//	OnDragOver
//---------------------------------------------------------------------------//
auto CMGListBox::OnDragOver(CMGControl* pSource, int iX, int iY, bool& Accept) -> void {
	Accept = false;
	if (m_fnOnDragOver) {
		m_fnOnDragOver(this, pSource, iX, iY, Accept);
	}
}


//---------------------------------------------------------------------------//
//	OnDragDrop
//---------------------------------------------------------------------------//
auto CMGListBox::OnDragDrop(CMGControl* pSource, int iX, int iY) -> void {
	if (m_fnOnDragDrop) {
		m_fnOnDragDrop(this, pSource, iX, iY);
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGListBox::DerivDraw() -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::FillRect(pSkin, TRect(Left() + 1, Top() + 1, Width() - 2, Height() - 2), COLOR_EDIT_BG);
	neon::gui::Rect(pSkin, TRect(Left(), Top(), Width(), Height()), COLOR_EDIT_BORDER);
	neon::gui::SetClipRect(TRect(Left() + 1, Top() + 1, Width() - 2, Height() - 2));

	// Draw all lines
	int iX = Left();
	int iY = Top();
	int Offset = iY + 1 - (m_pScrollBar && m_pScrollBar->IsVisible() ? (int)(m_pScrollBar->GetPos() * (GetTotalHeight() - Height() + 4)) : 0);
	for (size_t i = 0; i < m_vsItems.size(); i++) {
		if (Offset > (iY - m_iItemHeight) && Offset < (iY + Height())) {
			if (static_cast<int>(i) == m_iSelected) {
				if (m_pScrollBar) {
					neon::gui::FillRect(pSkin, TRect(iX + 1, Offset, Width() - 18, m_iItemHeight), COLOR_EDIT_BG_ACTIVE);
				} else {
					neon::gui::FillRect(pSkin, TRect(iX + 1, Offset, Width() - 2, m_iItemHeight), COLOR_EDIT_BG_ACTIVE);
				}
				neon::gui::Text(pSkin, FONT_NORMAL, iX + 2, Offset, 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT_ACTIVE, m_vsItems[i]);
			} else {
				neon::gui::Text(pSkin, FONT_NORMAL, iX + 2, Offset, 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT, m_vsItems[i]);
			}
		}
		Offset += m_iItemHeight;
	}
	neon::gui::SetClipRect();
}


//---------------------------------------------------------------------------//
//	OnChangeScroll
//---------------------------------------------------------------------------//
auto CMGListBox::OnChangeScroll() -> void {
	Repaint();
}
