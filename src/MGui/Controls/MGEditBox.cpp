//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "KeyboardDevice.h"
#include "MGEditBox.h"

//
// The functionality of editing text tries to ensure that m_iPos and m_iPosSel stay
// equal to each other.  More precicely, m_iPosSel follows m_iPos.  When they become
// different, a selection is being made.  m_iPos is the position of the cursor and
// during selection m_iPosSel will mark the position of the other end of the selection.
//

//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGEditBox::CMGEditBox(CMGControl* pParent, int iX, int iY, int iSize, function<void()> fnOnChange)
	: CMGControl(pParent, iX, iY, iSize, 20)
	, m_fnOnChange(fnOnChange)
	, m_sText()
	, m_iPos(0)
	, m_iPosSel(0)
	, m_bSelecting(false)
{
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGEditBox::~CMGEditBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGEditBox\n"));
#endif
}


//---------------------------------------------------------------------------//
//	SetText
//---------------------------------------------------------------------------//
auto CMGEditBox::SetText(std::string const& sText) -> void {
	m_sText = sText;
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGEditBox::OnMouseMove(int iX, int iY) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);

	m_bSelecting &= m_bSelecting;
	if (m_bSelecting) {
		//GLOG(("Selecting\n"));
		UpdateCursorPos(iX);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGEditBox::OnMouseDown(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT) {
		m_bSelecting = true;
		// begin selecting
		UpdateCursorPos(iX);
		m_iPosSel = m_iPos;
		SetFocus(true);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGEditBox::OnMouseUp(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT) {
		if (m_bSelecting) {
			m_bSelecting = false;
			// end selecting
			UpdateCursorPos(iX);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDoubleClk
//---------------------------------------------------------------------------//
auto CMGEditBox::OnMouseDoubleClk(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
	if (IsFocused()) {
		m_iPosSel = 0;
		m_iPos = static_cast<int>(m_sText.length());
	}
}


//---------------------------------------------------------------------------//
//	OnVKeyDown
//	Handles non-ascii key down events
//---------------------------------------------------------------------------//
auto CMGEditBox::OnVKeyDown(int iVKey) -> void {
	//GLOG(("OnVKeyDown %d", iVKey));
#if (PLATFORM == PLATFORM_WINDOWS)
	if (IsFocused()) {
		switch (iVKey) {
			case VK_DELETE: { // Delete
				if (m_iPos != m_iPosSel) {
					DeleteSelected();
				} else if (m_iPos < static_cast<int>(m_sText.length())) {
					m_sText.erase(m_iPos, 1);
				}
				break;
			}
			case VK_RIGHT: { // Right key
				if (m_iPos < static_cast<int>(m_sText.length())) {
					++m_iPos;
				}
				break;
			}
			case VK_LEFT: { // Left key
				if (m_iPos > 0) {
					--m_iPos;
				}
				break;
			}
			case VK_HOME: { // Home key
				m_iPos = 0;
				break;
			}
			case VK_END: { // End key
				m_iPos = static_cast<int>(m_sText.length());
				break;
			}
			default: {
				return;
			}
		}

		// if not pressing shift, can match the selection position with the cursor.
		if (!CKeyboardDevice::ModifierPressed(CKeyboardDevice::SHIFT)) {
			m_iPosSel = m_iPos;
		}

		Repaint();
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	OnChar
//	Handles ascii keydown events
//---------------------------------------------------------------------------//
auto CMGEditBox::OnChar(char c) -> void {
	//GLOG(("OnChar %c\n", c));
#if (PLATFORM == PLATFORM_WINDOWS)
	if (IsFocused()) {
		switch (c) {
			case VK_BACK: { // Backspace
				if (m_iPos != m_iPosSel) {
					DeleteSelected();
				} else if (m_iPos > 0) {
					m_sText.erase(m_iPos-- - 1, 1);
					m_iPosSel = m_iPos;
				}
				break;
			}
			case VK_RETURN: { // Return
				m_iPosSel = m_iPos;
				SetText(m_sText);	// ebp-> redundant?
				SetFocus(false);
				OnChange();
				break;
			}
			case VK_TAB: { // Tab
				// ignore
				break;
			}
			default: { // Ascii Text
				if (m_iPos != m_iPosSel) {
					DeleteSelected();
				}
				m_sText.insert(m_iPos++, std::string(1, c));
				m_iPosSel = m_iPos;
				break;
			}
		}
		Repaint();
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	OnChange
//---------------------------------------------------------------------------//
auto CMGEditBox::OnChange() -> void {
	if (m_fnOnChange) {
		m_fnOnChange();
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGEditBox::DerivDraw() -> void {

	// font_edit = EEEEEE
	// edit_bg = 2B2B2B
	// edit_border = 000000

	// ebp-> new code (allows for basic text selection)...
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	int iColorFont = COLOR_FONT_EDIT;	// text color
	int iColorBG = COLOR_EDIT_BG;		// bg of edit box
	int iColorSel = COLOR_BASE_BORDER;	// selection highlight
	int iColorBorder = COLOR_GRID_CELL_HOVER;	// border and cursor
	MGColor colorFont(pSkin->Color(iColorFont));
	MGColor colorBG(pSkin->Color(iColorBG));
	MGColor colorBorder(pSkin->Color(iColorBorder));

	int left = Left();
	int top = Top();
	int width = Width();
	int height = Height();

	neon::gui::Rect(pSkin, TRect(left, top, width, 20), iColorBorder);
	neon::gui::FillRect(pSkin, TRect(left + 1, top + 1, width - 2, 18), iColorBG);
	neon::gui::SetClipRect(TRect(left + 1, top + 1, width - 2, height - 2));

	if (m_iPos == m_iPosSel) {
		neon::gui::Text(pSkin, FONT_NORMAL, left + 3, top, 0, 20, CMGFont::LEFT, CMGFont::VCENTER, iColorFont, m_sText);
	} else {
		int one = Min(m_iPos, m_iPosSel);
		int two = Max(m_iPos, m_iPosSel);
		int sel_beg = left + GetPosFromChar(one);
		int sel_end = left + GetPosFromChar(two);
		neon::gui::FillRect(pSkin, TRect(sel_beg, top + 1, sel_end - sel_beg, height - 2), iColorSel);
		neon::gui::Text(pSkin, FONT_NORMAL, left + 3, top, 0, 20, CMGFont::LEFT, CMGFont::VCENTER, iColorFont, m_sText.substr(0, one));
		neon::gui::Text(pSkin, FONT_NORMAL, sel_beg, top, 0, 20, CMGFont::LEFT, CMGFont::VCENTER, iColorBorder, m_sText.substr(one, two));
		neon::gui::Text(pSkin, FONT_NORMAL, sel_end, top, 0, 20, CMGFont::LEFT, CMGFont::VCENTER, iColorFont, m_sText.substr(two));
	}

	// draw the text cursor...
	if (IsFocused()) {
		int x = left + GetPosFromChar(m_iPos);
		neon::gui::HLine(pSkin, x - 2, top + 3, 4, iColorBorder);
		neon::gui::HLine(pSkin, x - 2, top + 18, 4, iColorBorder);
		neon::gui::VLine(pSkin, x, top + 2, 16, iColorBorder);
	}

	// disable clip...
	neon::gui::SetClipRect();
}


//---------------------------------------------------------------------------//
//	GetPosFromChar
//	Based on character position within the string, determine the raw gui X pos.
//---------------------------------------------------------------------------//
auto CMGEditBox::GetPosFromChar(int iChar) -> int {
	return 3 + CServiceLocator<IAppSkinService>::GetService()->Font(FONT_NORMAL).TextLength(m_sText, iChar);
}


//---------------------------------------------------------------------------//
//	UpdateCursorPos
//	Based on raw X position sent in from mouse movements and gui, determine 
//	the position in the string to set the cursor.
//---------------------------------------------------------------------------//
auto CMGEditBox::UpdateCursorPos(int iX) -> void {
	m_iPos = static_cast<int>(m_sText.length());
	iX -= Left();
	for (uint i = 0; i <= m_sText.length(); ++i) {
		if (GetPosFromChar(i) > iX) {
			m_iPos = i;
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	DeleteSelected
//---------------------------------------------------------------------------//
auto CMGEditBox::DeleteSelected() -> void {
	int iMin = Min(m_iPos, m_iPosSel);
	int iMax = Max(m_iPos, m_iPosSel);
	m_sText.erase(iMin, iMax - iMin);
	m_iPosSel = m_iPos = iMin;
}
