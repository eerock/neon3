//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGGROUPBOX_H
#define NEON_MGGROUPBOX_H

#include "MGControl.h"

// ebp-> is this the same as a CMGPanel, just with a colored border?
class CMGGroupBox : public CMGControl {
public:
	//CMGGroupBox(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align, MGColor const& cBorderColor);
	CMGGroupBox(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align = MGALIGN_NONE, int iBorderColor = COLOR_BASE_BORDER);
	~CMGGroupBox() override;

protected:
	auto DerivDraw() -> void override;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGGroupBox\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

private:
	//const char* m_pCaption;
	//MGColor m_BorderColor;
	int m_iBorderColor;
};

#endif//NEON_MGGROUPBOX_H
