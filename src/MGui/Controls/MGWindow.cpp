//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGWindow.h"
#include "MGDisplay.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGWindow::CMGWindow(int iW, int iH, std::string const& sCaption)
	: CMGControl(nullptr, 0, 0, iW, iH)
	, m_sCaption(sCaption)
	, m_bMoving(false)
	, m_iAntX(0)
	, m_iAntY(0)
	, m_iMoveOffX(0)
	, m_iMoveOffY(0)
	, m_fnOnModalResult(NULL)
{
	SetRect(m_tBorder, 1, 20, 1, 1);
	SetVisible(false);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGWindow::~CMGWindow() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGWindow\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Show
//---------------------------------------------------------------------------//
auto CMGWindow::Show(int iX, int iY, function<void(EModalResult eModalResult)> fOnModalResult) -> void {
	m_fnOnModalResult = fOnModalResult;
	g_pMGApp->PopupWindow(this, iX, iY);
}


//---------------------------------------------------------------------------//
//	Close
//---------------------------------------------------------------------------//
auto CMGWindow::Close(EModalResult eModalResult) -> void {
	g_pMGApp->PopupWindow(nullptr);
	if (m_fnOnModalResult) {
		m_fnOnModalResult(eModalResult);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGWindow::OnMouseMove(int iX, int iY) -> void {
	if (m_bMoving) {
		UpdatePos(iX + m_iMoveOffX, iY + m_iMoveOffY);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGWindow::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		m_iMoveOffX = m_tRect.x - iX;
		m_iMoveOffY = m_tRect.y - iY;
		m_iAntX = -1;
		m_iAntY = -1;
		m_bMoving = true;
		Focus();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGWindow::OnMouseUp(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT && m_bMoving) {
		m_bMoving = false;
		g_pMGApp->MustRepaint();
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGWindow::DerivDraw() -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::FillRect(pSkin, TRect(Left(), Top(), Width(), 20), COLOR_BASE_BORDER);
	neon::gui::FillRect(pSkin, TRect(Left(), Top() + 20, Width(), Height() - 20), COLOR_BASE_BG);
	neon::gui::Rect(pSkin, TRect(Left(), Top(), Width(), Height()), COLOR_BASE_BORDER);
	neon::gui::Text(pSkin, FONT_WINDOW_TITLE, Left() + 10, Top(), 0, 20, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_WINDOW_TITLE, m_sCaption);
}


//---------------------------------------------------------------------------//
//	UpdatePos
//---------------------------------------------------------------------------//
auto CMGWindow::UpdatePos(int iX, int iY) -> void {
	iX = Max(iX, 0);
	iY = Max(iY, 0);
	iX = Min(iX, neon::gui::GetWidth() - Width());
	iY = Min(iY, neon::gui::GetHeight() - Height());
	SetPos(iX, iY);

	//if (m_iAntX != -1) {
	//	neon::gui::XorRect(TRect(m_iAntX, m_iAntY, Width(), Height()), 0x8F8F8F);
	//}
	//m_iAntX = iX;
	//m_iAntY = iY;
	//neon::gui::XorRect(TRect(iX, iY, Width(), Height()), 0x8F8F8F);
	//Repaint();

	g_pMGApp->MustRepaint();
}
