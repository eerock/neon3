//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGWINDOW_H
#define NEON_MGWINDOW_H

#include "MGControl.h"

class CMGWindow : public CMGControl {
public:
	enum EModalResult {
		MR_NONE,
		MR_OK,
		MR_CANCEL,
	};

public:
	CMGWindow(int iW, int iH, std::string const& sCaption);
	~CMGWindow() override;

	virtual auto Show(int iX, int iY, function<void(EModalResult eModalResult)> fnOnModalResult) -> void;
	virtual auto Close(EModalResult eModalResult = MR_OK) -> void;

	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	virtual auto OnKeyDown(int /*vkey*/) -> void {}
	virtual auto OnKeyUp(int /*vkey*/) -> void {}

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGWindow\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;
	auto UpdatePos(int iX, int iY) -> void;

private:
	std::string m_sCaption;
	bool m_bMoving;
	int m_iAntX;
	int m_iAntY;
	int m_iMoveOffX;
	int m_iMoveOffY;
	function<void(EModalResult eModalResult)> m_fnOnModalResult;
};

#endif//NEON_MGWINDOW_H
