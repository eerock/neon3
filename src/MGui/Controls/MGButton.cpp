//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGButton.h"

struct TButtonInfo {
	int w;
	int h;
	int y;
	int x;
	int xfocus;
	int xpress;
	int xlink;
	int xlinkfocus;
	int xlinkpress;
};

static TButtonInfo s_atInfoBitmaps[CMGButton::MAX_BUTTONS] = {
//	  w    h    y    x   xfo xpr xlnk  xlf  xlp
	{ 75, 24,   0,   0,  75, 150,   0,  75, 150 },	// NORMAL
	{ 75, 24,   0,   0,  75, 150,   0,  75, 150 },	// MEDIUM
	{ 20, 20, 120,   0,   0,  20,  40,  40,  60 },	// SMALL
	{ 25, 25,  90,  75,  75, 100,  75,  75, 100 },	// REFRESH
	{ 25, 25,  90, 125, 125, 150, 125, 125, 150 },	// SAVE
	{ 40, 20, 140,   0,   0,  40,  40,  40,  40 },	// BLEND
	{ 16, 16, 120,  80,  80,  96, 112, 112, 128 },	// LINK
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGButton::CMGButton(CMGControl* pParent, int iX, int iY, int iType, std::string const& sCaption, function<void()> fnOnClick)
	: CMGControl(pParent, iX, iY, 0, 0)
	, m_sCaption(sCaption)
	, m_bPressed(false)
	, m_bLinked(false)
	, m_iType(iType)
	, m_fnOnClick(fnOnClick)
{
	ASSERT(ValidIndex(iType, (int)MAX_BUTTONS));
	SetSize(s_atInfoBitmaps[m_iType].w, s_atInfoBitmaps[m_iType].h);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGButton::~CMGButton() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGButton\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Trigger
//---------------------------------------------------------------------------//
auto CMGButton::Trigger() const -> void {
	if (m_fnOnClick) {
		m_fnOnClick();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGButton::OnMouseDown(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	if (iButton == MOUSE_LEFT) {
		m_bPressed = true;
		Focus();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGButton::OnMouseUp(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		if (m_bPressed && IsInside(iX, iY)) {
			Trigger();
		}
		m_bPressed = false;
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGButton::DerivDraw() -> void {
	int x = 0;
	if (m_bLinked) {
		if (m_bPressed) {
			x = s_atInfoBitmaps[m_iType].xlinkpress;
		} else if (IsFocused()) {
			x = s_atInfoBitmaps[m_iType].xlinkfocus;
		} else {
			x = s_atInfoBitmaps[m_iType].xlink;
		}
	} else {
		if (m_bPressed) {
			x = s_atInfoBitmaps[m_iType].xpress;
		} else if (IsFocused()) {
			x = s_atInfoBitmaps[m_iType].xfocus;
		} else {
			x = s_atInfoBitmaps[m_iType].x;
		}
	}

	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::DrawItem(pSkin, TRect(x, s_atInfoBitmaps[m_iType].y, Width(), Height()), TRect(Left(), Top(), Width(), Height()));

	if (!m_sCaption.empty()) {
		neon::gui::Text(pSkin, FONT_NORMAL, Left() + (m_bPressed ? 1 : 0), Top() + (m_bPressed ? 1 : 0), Width(), Height(), CMGFont::HCENTER, CMGFont::VCENTER, COLOR_FONT_BUTTON, m_sCaption);
	}
}
