//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGTreeView.h"
#include "MGScrollBar.h"


//---------------------------------------------------------------------------//
//	Tree Item
//---------------------------------------------------------------------------//


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGTreeItem::CMGTreeItem(CMGTreeItem* pParent, std::string const& sText, std::string const& sPath, bool bFolder)
	: m_bSelected(false)
	, m_bExpanded(false)
	, m_bFolder(bFolder)
	, m_iTotalHeight(0)
	, m_iSonsHeight(0)
	, m_iLastHeight(0)
	, m_sText(sText)
	, m_sPath(sPath)
	, m_tRect()
	, m_pParent(pParent)
	, m_vChildren()
{
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	m_tRect = TRect(0, 0, pSkin->Font(FONT_NORMAL).TextLength(m_sText) + 2, pSkin->Font(FONT_NORMAL).TextHeight() + 2);
	if (pParent != nullptr) {
		pParent->m_vChildren.push_back(this);
	}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGTreeItem::~CMGTreeItem() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGTreeItem\n"));
#endif
	for (auto it = m_vChildren.begin(); it != m_vChildren.end(); ++it) {
		DELETE_PTR((*it));
	}
	m_vChildren.clear();
}


//---------------------------------------------------------------------------//
//	SetPos
//---------------------------------------------------------------------------//
auto CMGTreeItem::SetPos(int iX, int iY) -> void {
	m_tRect.x = iX;
	m_tRect.y = iY;
	if (m_bExpanded) {
		for (auto& ch : m_vChildren) {
			ch->SetPos(iX + 18, iY + Height());
			iY += ch->GetTotalHeight();
		}
	}
}


//---------------------------------------------------------------------------//
//	GetHeightSons
//---------------------------------------------------------------------------//
auto CMGTreeItem::CalcHeight() -> int {
	m_iLastHeight = 0;
	m_iSonsHeight = 0;
	if (m_bExpanded) {
		for (auto& ch : m_vChildren) {
			m_iLastHeight = m_iSonsHeight;
			m_iSonsHeight += ch->CalcHeight();
		}
	}
	m_iTotalHeight = m_iSonsHeight + Height() + 2;
	return m_iTotalHeight;
}


//---------------------------------------------------------------------------//
//	Expand
//---------------------------------------------------------------------------//
auto CMGTreeItem::Expand(bool bAll) -> void {
	m_bExpanded = true;
	if (bAll) {
		for (auto& ch : m_vChildren) {
			ch->Expand(true);
		}
	}
}


//---------------------------------------------------------------------------//
//	Collapse
//---------------------------------------------------------------------------//
auto CMGTreeItem::Collapse(bool bAll) -> void {
	m_bExpanded = false;
	if (bAll) {
		for (auto& ch : m_vChildren) {
			ch->Collapse(true);
		}
	}
}


//---------------------------------------------------------------------------//
//	GetItemAt
//---------------------------------------------------------------------------//
auto CMGTreeItem::GetItemAt(int iX, int iY) -> CMGTreeItem* {
	if (iX >= Left() && iX < (Left() + Width() + 38) && iY >= Top() && iY < (Top() + Height())) {
		return this;
	}
	if (m_bExpanded) {
		for (auto& ch : m_vChildren) {
			auto pItemAt = ch->GetItemAt(iX, iY);
			if (pItemAt != nullptr) {
				return pItemAt;
			}
		}
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CMGTreeItem::Draw(int iYOffset) -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	int iX = Left();
	int iY = Top() - iYOffset;
	// Icon + Lines
	neon::gui::HLine(iX + 7, iY + 8, 22, pSkin->Color(COLOR_EDIT_BORDER));
	// Item
	if (m_bSelected) {
		neon::gui::FillRect(TRect(iX + 37, iY, m_tRect.w, m_tRect.h), pSkin->Color(COLOR_EDIT_BG_ACTIVE));
		neon::gui::Text(pSkin, FONT_NORMAL, iX + 38, iY + 1, 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT_ACTIVE, m_sText);
	} else {
		neon::gui::Text(pSkin, FONT_NORMAL, iX + 38, iY + 1, 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT, m_sText);
	}

	if (!m_vChildren.empty()) {
		if (m_bExpanded) {
			// Line + box
			neon::gui::DrawItem(pSkin, TRect(189, 30, 9, 9), TRect(iX + 3, iY + 4, 9, 9));
			neon::gui::VLine(pSkin, iX + 25, iY + 13, GetLastHeight() + 12, COLOR_EDIT_BORDER);
			// Draw Children
			for (auto& ch : m_vChildren) {
				ch->Draw(iYOffset);
			}
		} else {
			neon::gui::DrawItem(pSkin, TRect(180, 30, 9, 9), TRect(iX + 3, iY + 4, 9, 9));
		}
	}

	if (m_bFolder) {
		neon::gui::DrawItem(pSkin, TRect(0, 200, 16, 16), TRect(iX + 18, iY + 1, 16, 16));	// magic fuckin numbers
	} else {
		neon::gui::DrawItem(pSkin, TRect(16, 200, 16, 16), TRect(iX + 18, iY + 1, 16, 16));
	}
}




//---------------------------------------------------------------------------//
//	Tree View
//---------------------------------------------------------------------------//


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGTreeView::CMGTreeView(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align)
	: CMGControl(pParent, iX, iY, iW, iH)
	, m_pRoot(nullptr)
	, m_pSelected(nullptr)
	, m_pScrollBar(nullptr)
{
	m_iAlign = Align;
	m_tBorder = TRect(2, 2, 2, 2);	// magic fuckin numbers
	OnResize();
	m_pScrollBar = NEW CMGScrollBar(this, true, bind(&CMGTreeView::OnChangeScroll, this));
	ASSERT(m_pScrollBar);
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGTreeView::~CMGTreeView() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGTreeView\n"));
#endif
	m_pSelected = nullptr;
	DELETE_PTR(m_pRoot);
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGTreeView::Update() -> void {
	m_pRoot->CalcHeight();
	m_pRoot->SetPos(Left() + 4, Top() + 4);	// magic fuckin numbers
	m_pScrollBar->SetVisible(m_pRoot->GetTotalHeight() > Height());
	Repaint();
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
auto CMGTreeView::Clear() -> void {
	m_pSelected = nullptr;
	DELETE_PTR(m_pRoot);
	SetRootItem(nullptr);
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGTreeView::OnMouseMove(int iX, int iY) -> void {
	CMGControl::OnMouseMove(iX, iY);
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGTreeView::OnMouseDown(int iX, int iY, int iButton) -> void {
	CMGControl::OnMouseDown(iX, iY, iButton);

	if (iButton == MOUSE_LEFT) {
		int iOffset = m_pScrollBar->IsVisible() ? (int)(m_pScrollBar->GetPos() * (m_pRoot->GetTotalHeight() - Height())) : 0;
		auto pItemAt = GetItemAt(iX, iY + iOffset);
		if (pItemAt) {
			if (pItemAt->IsFolder()) {
				if (iX < (pItemAt->Left() + 12)) {	// magic fuckin numbers
					if (pItemAt->IsExpanded()) {
						pItemAt->Collapse();
					} else {
						pItemAt->Expand();
					}
				} else {
					SelectItem(pItemAt);
				}
			} else {
				if (iX >= (pItemAt->Left() + 12)) {	// magic fuckin numbers
					SelectItem(pItemAt);
					BeginDrag();
				}
			}
		}
	} else if (m_pScrollBar->IsVisible()) {
#define TREE_VIEW_SCROLL_SPEED		(0.1f)
		if (iButton == MOUSE_WHEEL_UP) {
			m_pScrollBar->SetPos(m_pScrollBar->GetPos() - TREE_VIEW_SCROLL_SPEED);
		} else if (iButton == MOUSE_WHEEL_DOWN) {
			m_pScrollBar->SetPos(m_pScrollBar->GetPos() + TREE_VIEW_SCROLL_SPEED);
		}
	}
	Update();
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGTreeView::OnMouseUp(int iX, int iY, int iButton) -> void {
	UNREFERENCED_PARAMETER(iX);
	UNREFERENCED_PARAMETER(iY);
	UNREFERENCED_PARAMETER(iButton);
}


//---------------------------------------------------------------------------//
//	OnMouseDoubleClk
//---------------------------------------------------------------------------//
auto CMGTreeView::OnMouseDoubleClk(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		int iOffset = m_pScrollBar->IsVisible() ? (int)(m_pScrollBar->GetPos() * (m_pRoot->GetTotalHeight() - Height())) : 0;
		auto pItemAt = GetItemAt(iX, iY + iOffset);
		if (pItemAt) {
			if (pItemAt->IsFolder()) {
				if (pItemAt->IsExpanded()) {
					pItemAt->Collapse();
				} else {
					pItemAt->Expand();
				}
				Update();
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGTreeView::DerivDraw() -> void {
	int iOffset = m_pScrollBar->IsVisible() ? (int)(m_pScrollBar->GetPos() * (m_pRoot->GetTotalHeight() - Height() + 4)) : 0;
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::FillRect(pSkin, TRect(Left() + 1, Top() + 1, Width() - 2, Height() - 2), COLOR_EDIT_BG);
	neon::gui::Rect(pSkin, TRect(Left() + 1, Top() + 1, Width() - 2, Height() - 2), COLOR_EDIT_BORDER);
	neon::gui::SetClipRect(TRect(Left() + 2, Top() + 6, Width() - 4, Height() - 8));
	m_pRoot->Draw(iOffset);
	neon::gui::SetClipRect();
}


//---------------------------------------------------------------------------//
//	OnChangeScroll
//---------------------------------------------------------------------------//
auto CMGTreeView::OnChangeScroll() -> void {
	Repaint();
}


//---------------------------------------------------------------------------//
//	SelectItem
//---------------------------------------------------------------------------//
auto CMGTreeView::SelectItem(CMGTreeItem* pItem) -> void {
	if (m_pSelected) {
		m_pSelected->SetSelected(false);
	}

	m_pSelected = pItem;

	if (m_pSelected) {
		m_pSelected->SetSelected(true);
	}
}


//---------------------------------------------------------------------------//
//	GetItemAt
//---------------------------------------------------------------------------//
auto CMGTreeView::GetItemAt(int iX, int iY) -> CMGTreeItem* {
	return m_pRoot->GetItemAt(iX, iY);
}
