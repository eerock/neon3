//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGCONTROL_H
#define NEON_MGCONTROL_H

#include "MGBase.h"
#include "MGDisplay.h"

class CMGControl {
public:
	CMGControl(CMGControl* pParent, int iX, int iY, int iW, int iH);
	virtual ~CMGControl();

	auto Draw() -> void;
	virtual auto Repaint() -> void;
	virtual auto Focus() -> void;

	auto GetName() const -> std::string const& { return m_sName; }
	auto GetParent() -> CMGControl* { return m_pParent; }
	auto GetTop() -> CMGControl* { return m_pParent ? m_pParent->GetTop() : this; }
	auto GetAlign() const -> uint { return m_iAlign; }
	auto GetItemAt(int iX, int iY) -> CMGControl*;

	auto Left() const -> int { return m_tRect.x + (m_pParent ? m_pParent->Left() : 0); }
	auto Top() const -> int { return m_tRect.y + (m_pParent ? m_pParent->Top() : 0); }
	auto Width() const -> int { return m_tRect.w; }
	auto Height() const -> int { return m_tRect.h; }
	auto CenterX() const -> int { return Left() + (Width() >> 1); }
	auto CenterY() const -> int { return Top() + (Height() >> 1); }

	auto IsInside(int iX, int iY) -> bool;
	auto IsVisible() const -> bool { return m_bVisible && (!m_pParent || m_pParent->IsVisible()); }
	auto IsEnabled() const -> bool { return m_bEnabled; }
	auto IsFocused() const -> bool { return m_bFocused; }

	auto SetParent(CMGControl* pParent) -> void;
	auto SetVisible(bool bVisible) -> void { m_bVisible = bVisible; if (bVisible) { Repaint(); } }
	auto SetEnabled(bool bEnabled) -> void { m_bEnabled = bEnabled; if (bEnabled) { Repaint(); } }
	auto SetFocus(bool bFocused) -> void { m_bFocused = bFocused; if (bFocused) { Focus(); } }
	auto SetPos(int iLeft, int iTop) -> void { m_tRect.x = iLeft; m_tRect.y = iTop; }
	auto SetSize(int iWidth, int iHeight) -> void { m_tRect.w = iWidth; m_tRect.h = iHeight; }
	auto SetColor(MGColor const& color) -> void { m_tColor = color; }
	auto SetBorder(int iBorder) -> void { SetRect(m_tBorder, iBorder, iBorder, iBorder, iBorder); }

	//auto GetRect() const -> TRect const& { return m_tRect; }
	// need one that returns TRect(Left(), Top(), Width(), Height());
	auto GetFreeRect(TRect& Rect) const -> void { Rect = m_tFreeRect; }

	virtual auto OnResize() -> void;
	virtual auto OnMouseMove(int iX, int iY) -> void;
	virtual auto OnMouseDown(int /*iX*/, int /*iY*/, int /*iButton*/) -> void {}
	virtual auto OnMouseUp(int /*iX*/, int /*iY*/, int /*iButton*/) -> void {}
	virtual auto OnMouseDoubleClk(int /*iX*/, int /*iY*/, int /*iButton*/) -> void;

	virtual auto OnVKeyDown(int /*iVKey*/) -> void {}
	virtual auto OnVKeyUp(int /*iVKey*/) -> void {}
	virtual auto OnChar(char /*c*/) -> void {}
	virtual auto OnDragOver(CMGControl* pSource, int iX, int iY, bool& Accept) -> void;
	virtual auto OnDragDrop(CMGControl* pSource, int iX, int iY) -> void;

	auto BeginDrag() -> void;

#ifdef _DEBUG
	virtual auto PrintHierarchy(std::string const& sIndent) const -> void {
		std::string sChildIndent = sIndent + "  ";
		for (auto& ch : m_vChildren) {
			ch->PrintHierarchy(sChildIndent);
		}
	}
#endif

protected:
	auto AddRectAlign(CMGControl* pItem) -> void;
	auto UpdateSize() -> void;
	auto UpdateRect() -> void;
	virtual auto DerivRun() -> void {}
	virtual auto DerivDraw() -> void {}

	std::string m_sName;
	int m_iAlign;
	bool m_bAddRectAlign;
	bool m_bVisible;
	bool m_bEnabled;
	bool m_bFocused;
	TRect m_tRect;
	TRect m_tFreeRect;
	TRect m_tBorder;
	MGColor m_tColor;
	int m_iColor;
	CMGControl* m_pParent;	// weak pointer to parent
	std::vector<CMGControl*> m_vChildren;
	//std::vector<std::unique_ptr<CMGControl>> m_vChildren;
	//std::vector<std::shared_ptr<CMGControl>> m_vChildren;

private:
	auto AddSon(CMGControl* pSon) -> void;
	auto DelSon(CMGControl* pSon) -> void;
};

#endif//NEON_MGCONTROL_H
