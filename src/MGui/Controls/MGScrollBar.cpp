//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGScrollBar.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGScrollBar::CMGScrollBar(CMGControl* pParent, bool bVertical, function<void()> fnOnChange)
	: CMGControl(pParent, 0, 0, 15, 15)
	, m_bVertical(bVertical)
	, m_fPos(0.f)
	, m_uPressed(NONE)
	, m_fnOnChange(fnOnChange)
{
	m_iAlign = bVertical ? MGALIGN_RIGHT : MGALIGN_BOTTOM;
	SetPos(0.f);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGScrollBar::CMGScrollBar(CMGControl* pParent, bool bVertical, int iX, int iY, int iSize, function<void()> fnOnChange)
	: CMGControl(pParent, iX, iY, 0, 0)
	, m_bVertical(bVertical)
	, m_fPos(0.f)
	, m_uPressed(NONE)
	, m_fnOnChange(fnOnChange)
{
	if (bVertical) {
		SetSize(15, iSize);
	} else {
		SetSize(iSize, 15);
	}
	SetPos(0.f);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGScrollBar::~CMGScrollBar() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGScrollBar\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGScrollBar::Update() -> void {
	m_fPos = Clamp(m_fPos, 0.f, 1.f);
	if (m_fnOnChange) {
		m_fnOnChange();
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	GetPosRealTB
//---------------------------------------------------------------------------//
auto CMGScrollBar::GetPosRealTB() const -> int {
	if (m_bVertical) {
		return (int)(Top() + 15 + (m_fPos * (Height() - 45)));
	} else {
		return (int)(Left() + 15 + (m_fPos * (Width() - 45)));
	}
}


//---------------------------------------------------------------------------//
//	UpdateFromTB
//---------------------------------------------------------------------------//
auto CMGScrollBar::UpdateFromTB(int iPos) -> void {
	if (m_bVertical) {
		m_fPos = ((float)(iPos - Top() - 15) / (float)(Height() - 45));
	} else {
		m_fPos = ((float)(iPos - Left() - 15) / (float)(Width() - 45));
	}
	m_fPos = Clamp(m_fPos, 0.f, 1.f);
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGScrollBar::OnMouseMove(int iX, int iY) -> void {
	if (m_uPressed == TRACKBAR) {
		if (m_bVertical) {
			UpdateFromTB(iY - 7);
		} else {
			UpdateFromTB(iX - 7);
		}
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGScrollBar::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		// ebp-> unreferenced variable
		//int pos = GetPosRealTB();
		if (m_bVertical) {
			// Vertical
			if (iY < (Top() + 15)) {
				m_uPressed = LEFTTOP;
				m_fPos -= 0.05f;
			} else if (iY >= (Top() + Height() - 15)) {
				m_uPressed = RIGHTBOTTOM;
				m_fPos += 0.05f;
			} else { //if (iY >= pos && iY < (pos + 15))
				m_uPressed = TRACKBAR;
				UpdateFromTB(iY - 7);
			}
			Update();
		} else {
			// Horizontal
			if (iX < (Left() + 15)) {
				m_uPressed = LEFTTOP;
				m_fPos -= 0.05f;
			} else if (iX >= (Left() + Width() - 15)) {
				m_uPressed = RIGHTBOTTOM;
				m_fPos += 0.05f;
			} else { //if (iX >= pos && iX < (pos + 15))
				m_uPressed = TRACKBAR;
				UpdateFromTB(iY - 7);
			}
			Update();
		}
		SetFocus(true);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGScrollBar::OnMouseUp(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		if (m_uPressed == TRACKBAR) {
			if (m_bVertical) {
				UpdateFromTB(iY - 7);
			} else {
				UpdateFromTB(iX - 7);
			}
			Update();
		} else {
			Repaint();
		}
		m_uPressed = NONE;
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGScrollBar::DerivDraw() -> void {
	int x, y, w, h;
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	if (m_bVertical) {
		if (m_uPressed == LEFTTOP) {
			neon::gui::DrawItem(pSkin, TRect(95, 30, 15, 15), TRect(Left(), Top(), 15, 15));
		} else {
			neon::gui::DrawItem(pSkin, TRect(80, 30, 15, 15), TRect(Left(), Top(), 15, 15));
		}

		if (m_uPressed == RIGHTBOTTOM) {
			neon::gui::DrawItem(pSkin, TRect(125, 30, 15, 15), TRect(Left(), Top() + Height() - 15, 15, 15));
		} else {
			neon::gui::DrawItem(pSkin, TRect(110, 30, 15, 15), TRect(Left(), Top() + Height() - 15, 15, 15));
		}

		// ebp-> what?
		x = Left();
		w = Width();
		y = Top() + 15;
		h = Height() - 30;
	} else {
		if (m_uPressed == LEFTTOP) {
			neon::gui::DrawItem(pSkin, TRect(95, 45, 15, 15), TRect(Left(), Top(), 15, 15));
		} else {
			neon::gui::DrawItem(pSkin, TRect(80, 45, 15, 15), TRect(Left(), Top(), 15, 15));
		}

		if (m_uPressed == RIGHTBOTTOM) {
			neon::gui::DrawItem(pSkin, TRect(125, 45, 15, 15), TRect(Left() + Width() - 15, Top(), 15, 15));
		} else {
			neon::gui::DrawItem(pSkin, TRect(110, 45, 15, 15), TRect(Left() + Width() - 15, Top(), 15, 15));
		}

		// ebp-> what?
		x = Left() + 15;
		w = Width() - 30;
		y = Top();
		h = Height();
	}

	neon::gui::FillRect(pSkin, TRect(x, y, w, h), COLOR_BASE_BG);
	int pos = GetPosRealTB();
	int posb = (m_uPressed == TRACKBAR ? 155 : 140);
	if (m_bVertical) {
		neon::gui::DrawItem(pSkin, TRect(posb, 30, 15, 15), TRect(Left(), (int)pos, 15, 15));
	} else {
		neon::gui::DrawItem(pSkin, TRect(posb, 30, 15, 15), TRect((int)pos, Top(), 15, 15));
	}
}
