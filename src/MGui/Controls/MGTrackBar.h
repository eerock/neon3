//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGTRACKBAR_H
#define NEON_MGTRACKBAR_H

#include "MGControl.h"


//---------------------------------------------------------------------------//
//	Class CMGTrackBar
//---------------------------------------------------------------------------//
class CMGTrackBar : public CMGControl {
public:
	CMGTrackBar(CMGControl* pParent, int iX, int iY, int iSize, bool Vertical, function<void()> fnOnChange);
	~CMGTrackBar() override;

	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;

	auto GetPos() const -> float { return m_fPos; }
	auto SetPos(float fPos) -> void { m_fPos = fPos; Update(); }

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGTrackBar\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	auto Update() -> void;
	auto UpdateFromTB(int iPos) -> void;
	auto GetPosRealTB() const -> int;

	bool m_bVertical;
	bool m_bPressed;
	float m_fPos;
	function<void()> m_fnOnChange;
};

#endif//NEON_MGTRACKBAR_H
