//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGComboBox.h"
#include "MGScrollBar.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGComboBox::CMGComboBox(CMGControl* pParent, int iX, int iY, int iWidth, int iMaxHeight, bool bNotifyAlways, function<void()> fnOnChange)
	: CMGControl(pParent, iX, iY, iWidth, 20)
	, m_fnOnChange(fnOnChange)
	, m_vStrings()
	, m_sText()
	, m_iSelected(0)
	, m_bUnfolded(false)
	, m_bButtonPressed(false)
	, m_bNotifyAlways(bNotifyAlways)
	, m_uMaxHeight(iMaxHeight)
	, m_uItemHeight(0)
	, m_uUnfoldedHeight(0)
	, m_pScrollBar(nullptr)
{
	OnResize();
	m_uItemHeight = CServiceLocator<IAppSkinService>::GetService()->Font(FONT_NORMAL).TextHeight() + 2;
	m_uUnfoldedHeight = m_uItemHeight * m_uMaxHeight + 1;
	m_pScrollBar = NEW CMGScrollBar(this, true, Width() - 16, 20, m_uUnfoldedHeight - 2, bind(&CMGComboBox::OnChangeScroll, this));
	m_pScrollBar->SetVisible(false);
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGComboBox::~CMGComboBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGComboBox\n"));
#endif
	Clear();
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
auto CMGComboBox::Clear() -> void {
	m_vStrings.clear();
	m_sText.clear();
	m_iSelected = -1;
}


//---------------------------------------------------------------------------//
//	Add
//---------------------------------------------------------------------------//
auto CMGComboBox::Add(std::string const& sText) -> void {
	m_vStrings.push_back(sText);
	if (m_vStrings.size() == 1) {
		m_iSelected = 0;
	}
}


//---------------------------------------------------------------------------//
//	IndexOf
//---------------------------------------------------------------------------//
auto CMGComboBox::IndexOf(std::string const& sText) -> int {
	int i = 0;
	for (auto& str : m_vStrings) {
		if (str == sText) {
			return i;
		}
		++i;
	}
	return -1;
}


//---------------------------------------------------------------------------//
//	Select
//---------------------------------------------------------------------------//
auto CMGComboBox::Select(int iIndex) -> void {
	std::string sNew;
	if (ValidIndex(iIndex, static_cast<int>(m_vStrings.size()))) {
		sNew = m_vStrings[iIndex];
	} else {
		iIndex = 0;
	}
	m_iSelected = iIndex;
	
	if (m_bNotifyAlways || sNew != m_sText) {
		m_sText = sNew;
		if (m_fnOnChange) {
			m_fnOnChange();
		}
	} else {
		m_sText = sNew;
	}
	SetUnfolded(false);
}


//---------------------------------------------------------------------------//
//	SetUnfolded
//---------------------------------------------------------------------------//
auto CMGComboBox::SetUnfolded(bool bUnfolded) -> void {
	if (!m_vStrings.empty()) {
		m_bUnfolded = bUnfolded;
		if (m_bUnfolded) {
			if (m_vStrings.size() > m_uMaxHeight) {
				m_pScrollBar->SetVisible(true);
			}
			uint uUnfoldedHeight = static_cast<uint>(m_vStrings.size()) * m_uItemHeight + 1;
			uUnfoldedHeight = Min(uUnfoldedHeight, m_uUnfoldedHeight);
			SetSize(Width(), 20 + uUnfoldedHeight);
			g_pMGApp->TopItem(this);
		} else {
			SetSize(Width(), 20);
			m_pScrollBar->SetVisible(false);
			g_pMGApp->TopItem(nullptr);
		}
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGComboBox::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (m_vStrings.empty()) {
		return;
	}

	if (iButton == MOUSE_LEFT) {
		if (m_bUnfolded) {
			if (IsInside(iX, iY)) {
				iY -= Top();
				iX -= Left();
				if (iY > 20) {
					if (m_pScrollBar->IsVisible()) {
						if (iX < (Width() - 16)) {
							int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
							Select((iY - 21 + Offset) / (m_uItemHeight));
						} else {
							m_pScrollBar->OnMouseDown(iX + Left(), iY + Top(), iButton);
						}
					} else {
						int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
						Select((iY - 21 + Offset) / (m_uItemHeight));
					}
				} else {
					SetUnfolded(false);
				}
			} else {
				SetUnfolded(false);
			}
		} else {
			m_bButtonPressed = true;
			SetUnfolded(true);
		}
		SetFocus(true);
	}
	else if (m_pScrollBar->IsVisible()) {
#define COMBO_BOX_SCROLL_SPEED		(0.1f)
		if (iButton == MOUSE_WHEEL_UP) {
			m_pScrollBar->SetPos(m_pScrollBar->GetPos() - COMBO_BOX_SCROLL_SPEED);
			iX -= Left();
			iY -= Top();
			if (iX > Left() && iX < Width() && iY > 20) {
				int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
				int iOldSelected = m_iSelected;
				m_iSelected = (iY - 21 + Offset) / m_uItemHeight;
				if (iOldSelected != m_iSelected) {
					Repaint();
				}
			}
		} else if (iButton == MOUSE_WHEEL_DOWN) {
			m_pScrollBar->SetPos(m_pScrollBar->GetPos() + COMBO_BOX_SCROLL_SPEED);
			iX -= Left();
			iY -= Top();
			if (iX > Left() && iX < Width() && iY > 20) {
				int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
				int iOldSelected = m_iSelected;
				m_iSelected = (iY - 21 + Offset) / m_uItemHeight;
				if (iOldSelected != m_iSelected) {
					Repaint();
				}
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGComboBox::OnMouseUp(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		m_bButtonPressed = false;
		if (IsInside(iX, iY)) {
			iY -= Top();
			iX -= Left();
			if (iY > 20) {
				if (m_pScrollBar->IsVisible()) {
					if (iX < (Width() - 16)) {
						int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
						Select((iY - 21 + Offset) / (m_uItemHeight));
					} else {
						m_pScrollBar->OnMouseDown(iX + Left(), iY + Top(), iButton);
					}
				} else {
					int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
					Select((iY - 21 + Offset) / (m_uItemHeight));
				}
			}
		}
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGComboBox::OnMouseMove(int iX, int iY) -> void {
	if (m_bUnfolded) {
		iX -= Left();
		iY -= Top();
		if (iX >= 0 && iX < (Width()) && iY > 20) {
			int Offset = (int)(m_pScrollBar->GetPos() * m_uUnfoldedHeight);
			int iOldSelected = m_iSelected;
			m_iSelected = (iY - 21 + Offset) / m_uItemHeight;
			if (iOldSelected != m_iSelected) {
				Repaint();
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGComboBox::DerivDraw() -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	float fDepth = m_bUnfolded ? 0.1f : 0.f;
	neon::gui::Rect(pSkin, TRect(Left(), Top(), Width(), 20), COLOR_EDIT_BORDER, fDepth);
	neon::gui::FillRect(pSkin, TRect(Left() + 1, Top() + 1, Width() - 2, 18), COLOR_EDIT_BG, fDepth);
	neon::gui::SetClipRect(TRect(Left() + 1, Top() + 1, Width() - 16, 18));
	neon::gui::Text(pSkin, FONT_NORMAL, Left() + 3, Top(), 0, 20, CMGFont::LEFT, CMGFont::VCENTER, COLOR_FONT_EDIT, m_sText);
	neon::gui::SetClipRect();

	// Button pressed
	if (m_bButtonPressed) {
		neon::gui::DrawItem(pSkin, TRect(15, 70, 15, 18), TRect(Left() + Width() - 16, Top() + 1, 15, 18), fDepth);
	} else {
		neon::gui::DrawItem(pSkin, TRect(0, 70, 15, 18), TRect(Left() + Width() - 16,Top() + 1, 15, 18), fDepth);
	}

	if (m_bUnfolded) {
		uint uUnfoldedHeight = static_cast<uint>(m_vStrings.size()) * m_uItemHeight + 1;
		uUnfoldedHeight = Min(uUnfoldedHeight, m_uUnfoldedHeight);
		neon::gui::Rect(pSkin, TRect(Left(), Top() + 19, Width(), uUnfoldedHeight), COLOR_EDIT_BORDER, fDepth);
		neon::gui::FillRect(pSkin, TRect(Left() + 1, Top() + 20, Width() - 2, uUnfoldedHeight - 2), COLOR_EDIT_BG, fDepth);
		neon::gui::SetClipRect(TRect(Left() + 1, Top() + 20, Width() - 2, uUnfoldedHeight - 2));

		int iY = Top() + 20 - (int)(m_pScrollBar->GetPos() * uUnfoldedHeight);
		int i = 0;
		for (auto& str : m_vStrings) {
			if (i == m_iSelected) {
				neon::gui::FillRect(pSkin, TRect(Left() + 1, iY, Width() - 17, m_uItemHeight), COLOR_EDIT_BG_ACTIVE, fDepth);
				neon::gui::Text(pSkin, FONT_NORMAL, Left() + 3, iY + 1, 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT_ACTIVE, str);
			} else {
				neon::gui::Text(pSkin, FONT_NORMAL, Left() + 3, iY + 1, 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT, str);
			}
			iY += m_uItemHeight;
			++i;
		}

		neon::gui::SetClipRect();
	}
}


//---------------------------------------------------------------------------//
//	OnChangeScroll
//---------------------------------------------------------------------------//
auto CMGComboBox::OnChangeScroll() -> void {
	Repaint();
}
