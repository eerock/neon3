//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGCheckBox.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGCheckBox::CMGCheckBox(CMGControl* pParent, int iX, int iY, std::string const& sCaption, bool bIconLeft, bool bDefaultState, function<void()> fnOnChange, function<void(bool)> fnOnChangeAlt)
	: CMGControl(pParent, iX, iY, 0, 0)
	, m_bChecked(bDefaultState)
	, m_bIconLeft(bIconLeft)
	, m_sCaption(sCaption)
	, m_iTextW(0)
	, m_iTextH(0)
	, m_fnOnChange(fnOnChange)
	, m_fnOnChangeAlt(fnOnChangeAlt)
{
	if (!m_sCaption.empty()) {
		auto pSkin = CServiceLocator<IAppSkinService>::GetService();
		m_iTextW = pSkin->Font(FONT_NORMAL).TextLength(m_sCaption);
		m_iTextH = pSkin->Font(FONT_NORMAL).TextHeight();
		SetSize(m_iTextW + 16, 20);
	} else {
		SetSize(15, 20);
	}
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGCheckBox::~CMGCheckBox() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGCheckBox\n"));
#endif
}


//---------------------------------------------------------------------------//
//	OnChange
//---------------------------------------------------------------------------//
auto CMGCheckBox::OnChange() -> void {
	if (m_fnOnChange) {
		m_fnOnChange();
	}

	if (m_fnOnChangeAlt) {
		m_fnOnChangeAlt(m_bChecked);
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGCheckBox::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		iX -= Left();
		iY -= Top();
		if (0 <= iX && iX < Width() && 0 <= iY && iY < Height()) {
			m_bChecked = !m_bChecked;
			OnChange();
		}
		SetFocus(true);
	}
}


//---------------------------------------------------------------------------//
//	SwapBindings
//---------------------------------------------------------------------------//
auto CMGCheckBox::SwapBindings(CMGCheckBox* pOther) -> void {
	Swap(m_fnOnChange, pOther->m_fnOnChange);
	Swap(m_fnOnChangeAlt, pOther->m_fnOnChangeAlt);
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGCheckBox::DerivDraw() -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	int iX = Left();
	if (m_bIconLeft) {
		if (!m_sCaption.empty()) {
			neon::gui::Text(pSkin, FONT_NORMAL, Left() + 16, Top() + ((Height() - m_iTextH) >> 1), 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT, m_sCaption);
		}
	} else {
		if (!m_sCaption.empty()) {
			neon::gui::Text(pSkin, FONT_NORMAL, Left() + 1,  Top() + ((Height() - m_iTextH) >> 1), 0, 0, CMGFont::LEFT, CMGFont::TOP, COLOR_FONT_EDIT, m_sCaption);
		}
		iX += m_iTextW + 2;
	}

	if (m_bChecked) {
		if (m_bEnabled) {
			neon::gui::DrawItem(pSkin, TRect(26, 30, 13, 13), TRect(iX, Top() + 4, 13, 13));
		} else {
			neon::gui::DrawItem(pSkin, TRect(39, 30, 13, 13), TRect(iX, Top() + 4, 13, 13));
		}
	} else {
		if (m_bEnabled) {
			neon::gui::DrawItem(pSkin, TRect(0, 30, 13, 13), TRect(iX, Top() + 4, 13, 13));
		} else {
			neon::gui::DrawItem(pSkin, TRect(13, 30, 13, 13), TRect(iX, Top() + 4, 13, 13));
		}
	}
}
