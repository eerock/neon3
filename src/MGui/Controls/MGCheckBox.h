//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGCHECKBOX_H
#define NEON_MGCHECKBOX_H

#include "MGControl.h"

class CMGCheckBox : public CMGControl {
public:
	CMGCheckBox(CMGControl* pParent, int iX, int iY, std::string const& sCaption, bool bIconLeft, bool bDefaultState, function<void()> fnOnChange, function<void(bool)> fnOnChangeAlt = NULL);
	~CMGCheckBox() override;

	virtual auto Checked() const -> bool { return m_bChecked; }
	virtual auto SetChecked(bool bChecked) -> void { m_bChecked = bChecked; OnChange(); }
	virtual auto GetCaption() const -> std::string const& { return m_sCaption; }

	virtual auto OnChange() -> void;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;

	auto SwapBindings(CMGCheckBox* pOther) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGCheckBox\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

protected:
	auto DerivDraw() -> void override;

private:
	bool m_bChecked;
	bool m_bIconLeft;
	std::string m_sCaption;
	int m_iTextW;
	int m_iTextH;
	function<void()> m_fnOnChange;
	function<void(bool)> m_fnOnChangeAlt;
};

#endif//NEON_MGCHECKBOX_H
