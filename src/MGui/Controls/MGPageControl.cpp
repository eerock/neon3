//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGPageControl.h"


//---------------------------------------------------------------------------//
//	Page
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGPage::CMGPage(CMGPageControl* pPageControl, std::string const& sCaption)
	: CMGControl(pPageControl, 0, 0, 0, 0)
	, m_sCaption(sCaption)
{
	m_iAlign = MGALIGN_CLIENT;
	m_bAddRectAlign = false;
	pPageControl->AddPage(this);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGPage::~CMGPage() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGPage\n"));
#endif
}



//---------------------------------------------------------------------------//
//	Page Control
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGPageControl::CMGPageControl(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align, function<void()> fnOnSelectPage)
	: CMGControl(pParent, iX, iY, iW, iH)
	, m_vPages()
	, m_pActivePage(nullptr)
	, m_iActivePage(-1)
	, m_fnOnSelectPage(fnOnSelectPage)
{
	m_iAlign = Align;
	SetRect(m_tBorder, 2, 23, 2, 2);	// magic fuckin numbers
	//SetRect(m_tBorder, 0, 20, 0, 0);	// magic fuckin numbers
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGPageControl::~CMGPageControl() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGPageControl\n"));
#endif
	m_pActivePage = nullptr;
	// ebp-> the Pages already have a pointer back to their parent, i.e. this PageControl,
	// so we let the MGui hierarchy take care of deleting the pages.  So here we can just
	// clear the list of list nodes.
	m_vPages.clear();
}


//---------------------------------------------------------------------------//
//	AddPage
//---------------------------------------------------------------------------//
auto CMGPageControl::AddPage(CMGPage* pPage) -> void {
	m_vPages.push_back(pPage);

	g_pMGApp->RepaintItem(this);

	if (m_pActivePage != nullptr) {
		pPage->SetEnabled(false);
		pPage->SetVisible(false);
	} else {
		SetActivePage(pPage);
	}
}


//---------------------------------------------------------------------------//
//	SetActivePage
//---------------------------------------------------------------------------//
auto CMGPageControl::SetActivePage(int iPage) -> void {
	if (ValidIndex(static_cast<size_t>(iPage), m_vPages.size())) {
		SetActivePage(m_vPages[iPage]);
	}
}


//---------------------------------------------------------------------------//
//	SetActivePage
//---------------------------------------------------------------------------//
auto CMGPageControl::SetActivePage(CMGPage* pPage) -> void {
	if (pPage != m_pActivePage) {
		if (m_pActivePage) {
			m_pActivePage->SetEnabled(false);
			m_pActivePage->SetVisible(false);
		}

		m_pActivePage = pPage;

		if (m_pActivePage) {
			m_pActivePage->SetEnabled(true);
			m_pActivePage->SetVisible(true);
		}

		m_pActivePage->Repaint();
		
		// Active page
		int i = 0;
		m_iActivePage = -1;
		for (auto& pg : m_vPages) {
			if (pg == pPage) {
				m_iActivePage = i;
				break;
			}
			++i;
		}
		
		// Notify
		if (m_fnOnSelectPage) {
			m_fnOnSelectPage();
		}
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGPageControl::DerivDraw() -> void {
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::FillRect(pSkin, TRect(Left(), Top(), Width(), Height()), COLOR_BASE_BG);
	neon::gui::Rect(pSkin, TRect(Left() + 1, Top() + 1,  Width() - 2, Height() - 2), COLOR_BASE_BORDER);
	int nPages = static_cast<int>(m_vPages.size());
	if (nPages > 0) {
		int iX = Left() + 2;
		int iY = Top() + 2;
		//int iX = Left();
		//int iY = Top();

		int iW = Width() / nPages;
		int i = 0;

		for (auto& pg : m_vPages) {
			if (i == (nPages - 1)) {
				iW = (Left() + Width() - 2) - iX;
				//iW = (Left() + Width()) - iX;
			}

			// El no activo lo pintamos de otro color y le hacemos una ralla
			if (m_pActivePage != pg) {
				neon::gui::FillRect(pSkin, TRect(iX, iY, iW, 20), COLOR_TAB_BG);
				neon::gui::HLine(pSkin, iX, Top() + 22, iW, COLOR_BASE_BORDER);
			}

			if (!pg->GetCaption().empty()) {
				neon::gui::Text(pSkin, FONT_NORMAL, iX, iY, iW, 20, CMGFont::HCENTER, CMGFont::VCENTER, COLOR_FONT_LABEL, pg->GetCaption().c_str());
			}

			if (i > 0) {
				neon::gui::VLine(pSkin, iX, iY, 20, COLOR_BASE_BORDER);
			}
			iX += iW;
			i++;
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGPageControl::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		iX -= Left();
		iY -= Top();
		if (1 < iX && iX < (Width() - 2) && 1 < iY && iY < 24) {
			int nPages = static_cast<int>(m_vPages.size());
			if (nPages > 0) {
				int iW = (Width() - 4) / nPages;
				SetActivePage((iX - 3) / iW);
			}
		}
		SetFocus(true);
	}
}
