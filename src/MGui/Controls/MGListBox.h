//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGLISTBOX_H
#define NEON_MGLISTBOX_H

#include "MGControl.h"

class CMGScrollBar;

// ebp-> todo: make a getsize function.  add more size checks internally (i.e. when selecting via index)
// what about a 'Remove' function?

class CMGListBox : public CMGControl {
public:
	CMGListBox(CMGControl* pParent, int iX, int iY, int iW, int iH, int Align, bool bScrollBar);
	~CMGListBox() override;

	auto Add(std::string const& sItem) -> void;
	auto Clear() -> void;
	auto GetSelected(std::string& rSelected) -> bool;
	auto Select(int iItem) -> void;
	auto Select(std::string const& sText) -> bool;

	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	auto OnDragOver(CMGControl* pSource, int iX, int iY, bool& Accept) -> void override;
	auto OnDragDrop(CMGControl* pSource, int iX, int iY) -> void override;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGListBox\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

public:
	function<void(CMGListBox* pListBox)> m_fnOnSelectItem;
	function<void(CMGListBox* pListBox, CMGControl* pSource, int iX, int iY, bool& Accept)> m_fnOnDragOver;
	function<void(CMGListBox* pListBox, CMGControl* pSource, int iX, int iY)> m_fnOnDragDrop;

protected:
	virtual auto OnChangeScroll() -> void;
	auto DerivDraw() -> void override;
	auto Update() -> void;
	auto GetItemIndexAt(int iX, int iY) -> int;
	auto GetTotalHeight() -> int;

private:
	int m_iItemHeight;
	int m_iSelected;
	std::vector<std::string> m_vsItems;
	CMGScrollBar* m_pScrollBar;
};

#endif//NEON_MGLISTBOX_H
