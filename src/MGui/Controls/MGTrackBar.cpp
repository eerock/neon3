//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGTrackBar.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGTrackBar::CMGTrackBar(CMGControl* pParent, int iX, int iY, int iSize, bool bVertical, function<void()> fnOnChange)
	: CMGControl(pParent, iX, iY, 0, 0)
	, m_bVertical(bVertical)
	, m_bPressed(false)
	, m_fPos(0.f)
	, m_fnOnChange(fnOnChange)
{
	if (m_bVertical) {
		SetSize(16, iSize);
	} else {
		SetSize(iSize, 16);
	}
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGTrackBar::~CMGTrackBar() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGTrackBar\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGTrackBar::Update() -> void {
	m_fPos = Clamp(m_fPos, 0.f, 1.f);
	if (m_fnOnChange) {
		m_fnOnChange();
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	GetPosRealTB
//---------------------------------------------------------------------------//
auto CMGTrackBar::GetPosRealTB() const -> int {
	if (m_bVertical) {
		return (int)(Top() + (m_fPos * (Height() - 12)));
	} else {
		return (int)(Left() + (m_fPos * (Width() - 12)));
	}
}


//---------------------------------------------------------------------------//
//	UpdateFromTB
//---------------------------------------------------------------------------//
auto CMGTrackBar::UpdateFromTB(int iPos) -> void {
	if (m_bVertical) {
		m_fPos = ((float)(iPos - Top()) / (float)(Height() - 12));
	} else {
		m_fPos = ((float)(iPos - Left()) / (float)(Width() - 12));
	}
	m_fPos = Clamp(m_fPos, 0.f, 1.f);
	if (m_fnOnChange) {
		m_fnOnChange();
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGTrackBar::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		int pos = GetPosRealTB();
		if (m_bVertical) {
			m_bPressed = (iY >= pos && iY < (pos + 12));
		} else {
			m_bPressed = (iX >= pos && iX < (pos + 12));
		}
		// Si no le hemos dado al TB ponemos la Pos donde hemos clicado y dejamos apretado
		if (!m_bPressed) {
			if (m_bVertical) {
				UpdateFromTB(iY - 6);
			} else {
				UpdateFromTB(iX - 6);
			}
			m_bPressed = true;
		}
		Focus();
	}
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGTrackBar::OnMouseMove(int iX, int iY) -> void {
	if (m_bPressed) {
		iX = Max(iX, 0);
		iY = Max(iY, 0);
		if (m_bVertical) {
			UpdateFromTB(iY - 6);
		} else {
			UpdateFromTB(iX - 6);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGTrackBar::OnMouseUp(int iX, int iY, int iButton) -> void {
	if (iButton == MOUSE_LEFT) {
		if (m_bPressed) {
			if (m_bVertical) {
				UpdateFromTB(iY - 6);
			} else {
				UpdateFromTB(iX - 6);
			}
			Update();
		} else {
			Repaint();
		}
		m_bPressed = false;
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
auto CMGTrackBar::DerivDraw() -> void {
	int pos = GetPosRealTB();
	int posb = m_bPressed ? 170 : 150;
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	neon::gui::FillRect(pSkin, TRect(Left(), Top(), Width(), Height()), COLOR_BASE_BG);
	if (m_bVertical) {
		neon::gui::Rect(pSkin, TRect(Left() + 6, Top(), 4, Height()), COLOR_EDIT_BORDER);
		neon::gui::Rect(pSkin, TRect(Left() + 7, Top() + 1, 2, Height() - 2), COLOR_EDIT_BG);
		neon::gui::DrawItem(pSkin, TRect(posb, 76, 16, 12), TRect(Left(), (int)pos, 16, 12));
	} else {
		neon::gui::Rect(pSkin, TRect(Left(), Top() + 6, Width(), 4), COLOR_EDIT_BORDER);
		neon::gui::Rect(pSkin, TRect(Left() + 1, Top() + 7, Width() - 2, 2), COLOR_EDIT_BG);
		neon::gui::DrawItem(pSkin, TRect(posb, 60, 12, 16), TRect((int)pos, Top(), 12, 16));
	}
}
