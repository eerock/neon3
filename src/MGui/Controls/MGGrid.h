//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGGRID_H
#define NEON_MGGRID_H

#include "MGControl.h"


//---------------------------------------------------------------------------//
//	Class CMGGrid<T>
//---------------------------------------------------------------------------//
template <class T>
class CMGGrid : public CMGControl {
public:
	CMGGrid(CMGControl* pParent, int iCellsW, int iCellsH, float Aspect, int Align);
	CMGGrid(CMGControl* pParent, int iCellsW, int iCellsH, float Aspect, int iX, int iY, int iW, int iH);
	~CMGGrid() override;

	auto SetSize(int cols, int rows) -> void;

	virtual auto GetCols() const -> int { return m_nCols; }
	virtual auto GetRows() const -> int { return m_nRows; }
	virtual auto GetCells() const -> T** { return m_ppItems; }

	virtual auto Clear() -> void;
	virtual auto GetCellData(int col, int row) -> T*;
	virtual auto CellAtXY(int x, int y, int& col, int& row) -> bool;
	virtual auto GetPosInCell(int& x, int& y) const -> void;
	virtual auto GetCellW() const -> float { return m_fCellW; }
	virtual auto GetCellH() const -> float { return m_fCellH; }

	virtual auto GetSelectedCell() const -> T* { return m_pCellSelected; }
	virtual auto GetSelectedCell(int& col, int& row) -> bool { col = m_iCellSelectedCol; row = m_iCellSelectedRow; return (nullptr != m_pCellSelected); }
	virtual auto SelectCell(int col = -1, int row = -1) -> void;

	virtual auto GetHoveredCell() const -> T* { return m_pCellHovered; }
	virtual auto HoverCell(int col = -1, int row = -1) -> void;

	auto OnResize() -> void override;
	auto OnDragOver(CMGControl* pSource, int iX, int iY, bool& Accept) -> void override;
	auto OnDragDrop(CMGControl* pSource, int iX, int iY) -> void override;
	auto OnMouseMove(int iX, int iY) -> void override;
	auto OnMouseDown(int iX, int iY, int iButton) -> void override;
	auto OnMouseUp(int iX, int iY, int iButton) -> void override;
	auto OnMouseDoubleClk(int iX, int iY, int iButton) -> void override;
	auto OnKeyDown(int vKey) -> void;
	auto OnKeyUp(int vKey) -> void;

#ifdef _DEBUG
	auto PrintHierarchy(std::string const& sIndent) const -> void override {
		GLOG(("%sMGGrid\n", sIndent.c_str()));
		CMGControl::PrintHierarchy(sIndent);
	}
#endif

	function<void()> m_fnOnResize;
	function<void(CMGGrid<T>* pGrid, CMGControl* pSource, int col, int row, bool& Accept)> m_fnOnDragOver;
	function<void(CMGGrid<T>* pGrid, CMGControl* pSource, int col, int row)> m_fnOnDragDrop;
	function<void(CMGGrid<T>* pGrid, int iX, int iY)> m_fnOnMouseMove;
	function<void(CMGGrid<T>* pGrid, int iX, int iY, int iButton)> m_fnOnMouseDown;
	function<void(CMGGrid<T>* pGrid, int iX, int iY, int iButton)> m_fnOnMouseUp;
	function<void(CMGGrid<T>* pGrid, int iX, int iY, int iButton)> m_fnOnMouseDoubleClk;
	function<void(CMGGrid<T>* pGrid, int vKey)> m_fnOnKeyDown;
	function<void(CMGGrid<T>* pGrid, int vKey)> m_fnOnKeyUp;

	MGColor m_tColorLines;
	MGColor m_tColorHover;

protected:
	auto DerivDraw() -> void override;
	auto GetTotalHeight() -> int;

	T** m_ppItems;
	T* m_pCellSelected;
	T* m_pCellHovered;
	int m_iCellSelectedCol;
	int m_iCellSelectedRow;
	int m_iCellHoveredCol;
	int m_iCellHoveredRow;
	float m_fGridAspectRatio;
	float m_fCellW;
	float m_fCellH;
	int m_nCols;
	int m_nRows;
	int m_iTotalWidth;
	int m_iTotalHeight;
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
template<class T>
CMGGrid<T>::CMGGrid(CMGControl* pParent, int iCellsW, int iCellsH, float Aspect, int Align)
	: CMGControl(pParent, 0, 0, 0, 0)
	, m_fnOnResize(NULL)
	, m_fnOnDragOver(NULL)
	, m_fnOnDragDrop(NULL)
	, m_fnOnMouseMove(NULL)
	, m_fnOnMouseDown(NULL)
	, m_fnOnMouseUp(NULL)
	, m_fnOnMouseDoubleClk(NULL)
	, m_fnOnKeyDown(NULL)
	, m_fnOnKeyUp(NULL)
	, m_ppItems(nullptr)
	, m_pCellSelected(nullptr)
	, m_pCellHovered(nullptr)
	, m_iCellSelectedCol(-1)
	, m_iCellSelectedRow(-1)
	, m_iCellHoveredCol(-1)
	, m_iCellHoveredRow(-1)
	, m_fGridAspectRatio(Aspect)
	, m_fCellW(0.f)
	, m_fCellH(0.f)
	, m_nCols(0)
	, m_nRows(0)
	, m_iTotalWidth(0)
	, m_iTotalHeight(0)
{
	m_tBorder = TRect(1, 1, 1, 1);
	m_iAlign = Align;
	m_tColor = g_pMGApp->Skin()->Color(COLOR_GRID_BG);
	m_iColor = COLOR_GRID_BG;
	m_tColorLines = g_pMGApp->Skin()->Color(COLOR_GRID_BORDER);
	m_tColorHover = g_pMGApp->Skin()->Color(COLOR_GRID_CELL_HOVER);
	SelectCell();
	SetSize(iCellsW, iCellsH);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
template<class T>
CMGGrid<T>::CMGGrid(CMGControl* pParent, int iCellsW, int iCellsH, float Aspect, int iX, int iY, int iW, int iH)
	: CMGControl(pParent, iX, iY, iW, iH)
	, m_fnOnResize(NULL)
	, m_fnOnDragOver(NULL)
	, m_fnOnDragDrop(NULL)
	, m_fnOnMouseMove(NULL)
	, m_fnOnMouseDown(NULL)
	, m_fnOnMouseUp(NULL)
	, m_fnOnMouseDoubleClk(NULL)
	, m_fnOnKeyDown(NULL)
	, m_fnOnKeyUp(NULL)
	, m_ppItems(nullptr)
	, m_pCellSelected(nullptr)
	, m_pCellHovered(nullptr)
	, m_iCellSelectedCol(-1)
	, m_iCellSelectedRow(-1)
	, m_iCellHoveredCol(-1)
	, m_iCellHoveredRow(-1)
	, m_fGridAspectRatio(Aspect)
	, m_fCellW(0.f)
	, m_fCellH(0.f)
	, m_nCols(0)
	, m_nRows(0)
	, m_iTotalWidth(0)
	, m_iTotalHeight(0)
{
	m_tBorder = TRect(1, 1, 1, 1);
	m_iColor = COLOR_GRID_BG;
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	m_tColor = pSkin->Color(COLOR_GRID_BG);
	m_tColorLines = pSkin->Color(COLOR_GRID_BORDER);
	m_tColorHover = pSkin->Color(COLOR_GRID_CELL_HOVER);
	SelectCell();
	SetSize(iCellsW, iCellsH);
	OnResize();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
template<class T>
CMGGrid<T>::~CMGGrid() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGGrid<T> - aka CMGSourceGrid\n"));
#endif
	// Delete
	if (m_ppItems) {
		for (int i = 0; i < m_nCols * m_nRows; ++i) {
			DELETE_PTR(m_ppItems[i]);
		}
		DELETE_ARRAY(m_ppItems);
	}
}


//---------------------------------------------------------------------------//
//	Clear
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::Clear() -> void {
	// Delete Old...
	if (m_ppItems) {
		for (int i = 0; i < m_nCols * m_nRows; ++i) {
			DELETE_PTR(m_ppItems[i]);
		}
		DELETE_ARRAY(m_ppItems);
	}

	// Create New...
	m_ppItems = NEW_ARRAY(T*, m_nCols * m_nRows);

	for (int col = 0; col < m_nCols; ++col) {
		for (int row = 0; row < m_nRows; ++row) {
			m_ppItems[row * m_nCols + col] = NEW T;
		}
	}
}


//---------------------------------------------------------------------------//
//	CellAtXY
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::CellAtXY(int x, int y, int& col, int& row) -> bool {
	x = Floor((float)x / m_fCellW);
	y = Floor((float)y / m_fCellH);
	if (0 <= x && x < m_nCols && 0 <= y && y < m_nRows) {
		col = x;
		row = y;
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------//
//	GetPosInCell
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::GetPosInCell(int& x, int& y) const -> void {
	int row = Floor((float)x / m_fCellW);
	int col = Floor((float)y / m_fCellH);
	x -= int((float)row * m_fCellW);
	y -= int((float)col * m_fCellH);
}


//---------------------------------------------------------------------------//
//	GetCellData
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::GetCellData(int col, int row) -> T* {
	if (0 <= col && col < m_nCols && 0 <= row && row < m_nRows) {
		return m_ppItems[row * m_nCols + col];
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	SelectCell
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::SelectCell(int col, int row) -> void {
	m_pCellSelected = GetCellData(col, row);
	m_iCellSelectedCol = col;
	m_iCellSelectedRow = row;
	Repaint();
}


//---------------------------------------------------------------------------//
//	HoverCell
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::HoverCell(int col, int row) -> void {
	m_pCellHovered = GetCellData(col, row);
	m_iCellHoveredCol = col;
	m_iCellHoveredRow = row;
	Repaint();
}


//---------------------------------------------------------------------------//
//	SetSize
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::SetSize(int cols, int rows) -> void {
	m_nCols = cols;
	m_nRows = rows;
	Clear();
}


//---------------------------------------------------------------------------//
//	OnResize
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnResize() -> void {
	UpdateSize();
	m_fCellW = static_cast<float>(Width()) / m_nCols;
	//m_fCellH = m_fCellW * m_fGridAspectRatio;
	m_fCellH = static_cast<float>(Height()) / m_nRows;
	m_iTotalHeight = Height();
	if (m_iAlign == MGALIGN_TOP) {
		m_tRect.h = m_iTotalHeight;
	}

	m_iTotalWidth = Width();
	UpdateRect();

	// Resize sons
	for (auto& ch : m_vChildren) {
		ch->OnResize();
	}

	if (m_fnOnResize) {
		m_fnOnResize();
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnDragOver
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnDragOver(CMGControl* pSource, int iX, int iY, bool& Accept) -> void {
	Accept = false;
	if (m_fnOnDragOver) {
		m_fnOnDragOver(this, pSource, iX, iY, Accept);
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnDragDrop
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnDragDrop(CMGControl* pSource, int iX, int iY) -> void {
	if (m_fnOnDragDrop) {
		m_fnOnDragDrop(this, pSource, iX, iY);
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnMouseMove(int iX, int iY) -> void {
	// ebp-> todo: implement tracking of mouse movement highlights active cell
	//GLOG(("MouseMove in MGGrid (%d, %d)\n", iX, iY));
	if (m_fnOnMouseMove) {
		m_fnOnMouseMove(this, iX, iY);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnMouseDown(int iX, int iY, int iButton) -> void {
	if (m_fnOnMouseDown) {
		m_fnOnMouseDown(this, iX, iY, iButton);
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnMouseUp(int iX, int iY, int iButton) -> void {
	if (m_fnOnMouseUp) {
		m_fnOnMouseUp(this, iX, iY, iButton);
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnMouseDoubleClk
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnMouseDoubleClk(int iX, int iY, int iButton) -> void {
	if (m_fnOnMouseDoubleClk) {
		m_fnOnMouseDoubleClk(this, iX, iY, iButton);
	}
	Repaint();
}


//---------------------------------------------------------------------------//
//	OnKeyDown
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnKeyDown(int vKey) -> void {
	if (m_fnOnKeyDown) {
		m_fnOnKeyDown(this, vKey);
	}
}


//---------------------------------------------------------------------------//
//	OnKeyUp
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::OnKeyUp(int vKey) -> void {
	if (m_fnOnKeyUp) {
		m_fnOnKeyUp(this, vKey);
	}
}


//---------------------------------------------------------------------------//
//	DerivDraw
//---------------------------------------------------------------------------//
template<class T>
auto CMGGrid<T>::DerivDraw() -> void {
	int i, j;

	TRect rect(Left(), Top(), m_iTotalWidth, m_iTotalHeight);

	// Draw an inner border...
	ShrinkRect(rect, 1);
	neon::gui::Rect(rect, m_tColorLines);

	float x = static_cast<float>(rect.x);
	float y = static_cast<float>(rect.y);

	ShrinkRect(rect, 1);
	neon::gui::SetClipRect(rect);

	// Draw the grid lines...
	for (i = 1; i < m_nRows; ++i) {
		neon::gui::HLine((int)x, (int)(y + i * m_fCellH), m_iTotalWidth, m_tColorLines);
	}
	for (i = 1; i < m_nCols; ++i) {
		neon::gui::VLine((int)(x + i * m_fCellW), (int)y, m_iTotalHeight, m_tColorLines);
	}

	// Draw Items
	for (i = 0; i < m_nCols; ++i) {
		for (j = 0; j < m_nRows; ++j) {
			//if (i == m_iCellHoveredCol && j == m_iCellHoveredRow) {
			//	// draw a highlight border around cell...
			//	neon::gui::Rect(TRect(
			//		Floor(x + i * m_fCellW),
			//		Floor(y + j * m_fCellH) - iOffset,
			//		Floor(m_fCellW) - 1,
			//		Floor(m_fCellH) - 1),
			//		m_tColorHover
			//	);
			//	neon::gui::SetClipRect(TRect(
			//		Floor(x + i * m_fCellW) + 1,
			//		Floor(y + j * m_fCellH) - iOffset + 1,
			//		Floor(m_fCellW) - 1 - 1,
			//		Floor(m_fCellH) - 1 - 1
			//	));
			//}

			auto pItem = GetCellData(i, j);
			if (pItem) {
				pItem->Draw(
					Floor(x + i * m_fCellW),
					Floor(y + j * m_fCellH),
					Floor(m_fCellW),
					Floor(m_fCellH),
					m_pCellSelected == pItem
					);
			}
		}
	}
	neon::gui::SetClipRect();
}


#endif//NEON_MGGRID_H
