//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGBUTTONMODALRESULT_H
#define NEON_MGBUTTONMODALRESULT_H

#include "MGButton.h"
#include "MGWindow.h"

class CMGButtonModalResult : public CMGButton {
public:
	CMGButtonModalResult(CMGWindow* pParent, int iX, int iY, int Type, std::string const& sCaption, CMGWindow::EModalResult iModalResult)
		: CMGButton(pParent, iX, iY, Type, sCaption, 0)
		, m_eModalResult(iModalResult)
		, m_pWindow(pParent)
	{}

	auto OnMouseUp(int iX, int iY, int iButton) -> void override {
		if (iButton == MOUSE_LEFT) {
			if (m_bPressed && IsInside(iX, iY)) {
				m_pWindow->Close(m_eModalResult);
			}
			m_bPressed = false;
			Repaint();
		}
	}

protected:
	CMGWindow* m_pWindow;
	CMGWindow::EModalResult m_eModalResult;
};

#endif//NEON_MGBUTTONMODALRESULT_H
