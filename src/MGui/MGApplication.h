//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGAPPLICATION_H
#define NEON_MGAPPLICATION_H

class IMGAppWindow;
class CMGWindow;
class CMGControl;
class CMGSkin;


//---------------------------------------------------------------------------//
//	CMGApplication
//---------------------------------------------------------------------------//
class CMGApplication {
public:
	CMGApplication();
	~CMGApplication();

	auto Init(THInst hInstance, int w, int h) -> bool;

	auto Update() -> void;
	auto MoveMainWindow(int iOffX, int iOffY) -> void;

	auto OnResize(int w, int h) -> void;
	auto OnMouseMove(int x, int y) -> void;
	auto OnMouseDown(int x, int y, int button) -> void;
	auto OnMouseUp(int x, int y, int button) -> void;
	auto OnMouseDoubleClk(int x, int y, int button) -> void;
	auto OnVKeyDown(int vkey) -> void;
	auto OnVKeyUp(int vkey) -> void;
	auto OnChar(char c) -> void;

	auto Repaint() -> void;
	auto MustRepaint() -> void { m_bRepaint = true; }
	auto MustFlip() -> void { m_bFlip = true; }
	auto Flip() -> void;
	auto SetRootItem(CMGControl* pItem) -> void;
	auto FocusItem(CMGControl* pItem) -> void;
	auto TopItem(CMGControl* pItem) -> void;
	auto RepaintItem(CMGControl* pItem) -> void;
	auto DragItem(CMGControl* pItem) -> void;
	auto PopupWindow(CMGWindow* pWindow, int iX = -1, int iY = -1) -> void;
	auto GetItemAt(int iX, int iY) -> CMGControl*;

	auto OpenFileOpen(std::string const& sTitle, std::string const& sInitialDir, char const* sWildcard, std::string& sFile) -> bool;
	auto OpenFileSave(std::string const& sTitle, std::string const& sInitialDir, char const* sWildcard, std::string const& sDefExt, std::string& sFile) -> bool;

	auto Close() -> void { m_bWantClose = true; }
	auto WantClose() const -> bool { return m_bWantClose; }

	auto GetHInst() const -> THInst { return m_hInst; }
	auto GetHWnd() const -> THWnd { return m_hWnd; }
	auto GetHDC() const -> THdc { return m_hDC; }

#ifdef _DEBUG
	auto PrintMGuiHierarchy() const -> void;
#endif

private:
	THInst m_hInst;
	THWnd m_hWnd;
	THdc m_hDC;
	bool m_bOk;
	bool m_bMovingWindow;
	bool m_bRepaint;
	bool m_bFlip;
	bool m_bWantClose;
	bool m_bDragAccepted;
	TRect m_tWinRect;
	int m_iPosMouseX;
	int m_iPosMouseY;
	IMGAppWindow* m_pAppWindow;
	CMGControl* m_pItemRoot;
	CMGControl* m_pItemFocused;
	CMGControl* m_pItemTop;
	CMGControl* m_pItemDragged;
	CMGWindow* m_pItemPopup;
	std::vector<CMGControl*> m_vRepaintItems;
};

#endif//NEON_MGAPPLICATION_H
