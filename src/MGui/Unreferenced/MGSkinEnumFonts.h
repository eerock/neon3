//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
// File: MGSkinEnumFonts.h
//
// ITEM( Enumeration, FontName, Width, Height, Bold, Italic, Anti-Alias )
//---------------------------------------------------------------------------//

ITEM(FONT_MINI, "Arial", 4, 12, false, false, true)
ITEM(FONT_NORMAL, "Arial", 5, 14, false, false, true)
ITEM(FONT_WINDOW_TITLE, "Arial", 6, 14, true, false, true)
ITEM(FONT_FX_TITLE, "Arial", 10, 20, true, false, true)
ITEM(FONT_BPM, "Arial", 14, 32, true, false, true)

//ITEM(FONT_MINI, "Arial", 4, 12, false, false, true)
//ITEM(FONT_NORMAL, "Tahoma", 5, 14, false, false, true)
//ITEM(FONT_WINDOW_TITLE, "Tahoma", 6, 14, true, false, true)
//ITEM(FONT_FX_TITLE, "Tahoma", 10, 20, true, false, true)
//ITEM(FONT_BPM, "Tahoma", 14, 32, true, false, true)
