//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

ITEM(COLOR_FONT_LABEL,			"font_label")
ITEM(COLOR_FONT_BUTTON,			"font_button")
ITEM(COLOR_FONT_WINDOW_TITLE,	"font_window_title")
ITEM(COLOR_FONT_EDIT,			"font_edit")
ITEM(COLOR_FONT_EDIT_ACTIVE,	"font_edit_active")
ITEM(COLOR_TAB_BG,				"tab_bg")
ITEM(COLOR_BASE_BG,				"base_bg")				// main background
ITEM(COLOR_BASE_BORDER,			"base_border")
ITEM(COLOR_BASE_BORDER_ACTIVE,	"base_border_active")
ITEM(COLOR_GRID_BG,				"grid_bg")
ITEM(COLOR_GRID_BG_ACTIVE,		"grid_bg_active")
ITEM(COLOR_GRID_BORDER,			"grid_border")
ITEM(COLOR_GRID_BORDER_ACTIVE,	"grid_border_active")
ITEM(COLOR_GRID_CELL_ATTACHED,	"grid_cell_attached")
ITEM(COLOR_GRID_CELL_HOVER,		"grid_cell_hover")		// mouse hover over grid cell, highlight
ITEM(COLOR_EDIT_BG,				"edit_bg")
ITEM(COLOR_EDIT_BG_ACTIVE,		"edit_bg_active")
ITEM(COLOR_EDIT_BORDER,			"edit_border")			// Mgui border color, control border
ITEM(COLOR_EDIT_BORDER_ACTIVE,	"edit_border_active")
