//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGApplication.h"
#include "MGAppWindow.h"
#include "MGDisplay.h"
#include "Controls/MGControl.h"
#include "Controls/MGWindow.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <commdlg.h>
#include <GL/gl.h>
#else
#include <OpenGL/gl.h>
#endif//PLATFORM

// ebp-> Global CMGApplication g_pMGApp!
CMGApplication* g_pMGApp;


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGApplication::CMGApplication()
	: m_hInst(0)
	, m_hWnd(0)
	, m_hDC(0)
	, m_bOk(false)
	, m_bMovingWindow(false)
	, m_bRepaint(false)
	, m_bFlip(false)
	, m_bWantClose(false)
	, m_bDragAccepted(false)
	, m_tWinRect()
	, m_iPosMouseX(0)
	, m_iPosMouseY(0)
	, m_pAppWindow(nullptr)
	, m_pItemRoot(nullptr)
	, m_pItemFocused(nullptr)
	, m_pItemTop(nullptr)
	, m_pItemDragged(nullptr)
	, m_pItemPopup(nullptr)
	, m_vRepaintItems()
{}

//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMGApplication::Init(THInst hInstance, int w, int h) -> bool {
	g_pMGApp = this;
	m_hInst = hInstance;
	m_bMovingWindow = false;
	m_iPosMouseX = 0;
	m_iPosMouseY = 0;
	SetRect(m_tWinRect, 0, 0, w, h);
	m_bRepaint = true;
	m_bFlip = true;
	m_bWantClose = false;
	m_bDragAccepted = false;
	m_pItemRoot = nullptr;
	m_pItemFocused = nullptr;
	m_pItemTop = nullptr;
	m_pItemDragged = nullptr;
	m_pItemPopup = nullptr;

	// ebp-> restore to old window position if one was stored...
	m_tWinRect.x = CServiceLocator<IAppOptionsService>::GetService()->Get("neonconfig.guiwindow.rleft", 0);
	m_tWinRect.y = CServiceLocator<IAppOptionsService>::GetService()->Get("neonconfig.guiwindow.rtop", 0);

	// AppWindow
	m_pAppWindow = IMGAppWindow::Create(m_hInst, m_tWinRect.x, m_tWinRect.y, m_tWinRect.w, m_tWinRect.h);
	if (!m_pAppWindow) {
		return false;
	}

	m_hWnd = m_pAppWindow->GetHWnd();
	m_hDC = m_pAppWindow->GetHDC();

	auto pSkin = new CMGSkin;
	if (!pSkin->Init(m_hInst, "Skins\\Default")) {
		DELETE_PTR(pSkin);
		return false;
	}

	CServiceLocator<IAppSkinService>::RegisterService(pSkin);

	if (!neon::gui::Init(m_hInst, m_tWinRect.x, m_tWinRect.y, m_tWinRect.w, m_tWinRect.h)) {
		return false;
	}

	return true;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGApplication::~CMGApplication() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGApplication\n"));
#endif
	CServiceLocator<IAppOptionsService>::GetService()->Set("neonconfig.guiwindow.rleft", m_tWinRect.x);
	CServiceLocator<IAppOptionsService>::GetService()->Set("neonconfig.guiwindow.rtop", m_tWinRect.y);

	m_pItemFocused = nullptr;
	m_pItemTop = nullptr;
	m_pItemDragged = nullptr;
	m_pItemPopup = nullptr;
	m_vRepaintItems.clear();

	CServiceLocator<IAppSkinService>::RegisterService(nullptr);

	// ebp-> deleting the Display before the MGui root exposes some
	// order of operation bugs in the shutdown sequence.

	DELETE_PTR(m_pItemRoot);	// ebp-> root of MGui
	DELETE_PTR(m_pAppWindow);
}


//---------------------------------------------------------------------------//
//	MoveMainWindow
//---------------------------------------------------------------------------//
auto CMGApplication::MoveMainWindow(int x, int y) -> void {
	m_bMovingWindow = true;
	m_iPosMouseX = x;
	m_iPosMouseY = y;
}


//---------------------------------------------------------------------------//
//	OnResize
//---------------------------------------------------------------------------//
auto CMGApplication::OnResize(int w, int h) -> void {
	m_tWinRect.w = w;
	m_tWinRect.h = h;
	neon::gui::OnResize(w, h);
	if (m_pItemRoot) {
		m_pItemRoot->OnResize();
	}
	MustRepaint();
}


//---------------------------------------------------------------------------//
//	OnMouseMove
//---------------------------------------------------------------------------//
auto CMGApplication::OnMouseMove(int x, int y) -> void {
	// If moving main window, handle special case
	if (m_bMovingWindow) {
		m_tWinRect.x += x - m_iPosMouseX;
		m_tWinRect.y += y - m_iPosMouseY;

#if (PLATFORM == PLATFORM_WINDOWS)
		MoveWindow(m_pAppWindow->GetHWnd(), m_tWinRect.x, m_tWinRect.y, m_tWinRect.w, m_tWinRect.h, true);
#endif//PLATFORM
		return;
	}

	// Other cases, treat normally
	CMGControl* pItem = nullptr;
	if (m_pItemTop) {
		pItem = m_pItemTop->GetItemAt(x, y);
		if (!pItem) {
			pItem = m_pItemTop;
		}

		pItem->OnMouseMove(x, y);

	} else {
		pItem = GetItemAt(x, y);
		if (pItem) {
			pItem->OnMouseMove(x, y);
			if (m_pItemDragged) {
				pItem->OnDragOver(m_pItemDragged, x, y, m_bDragAccepted);
#if (PLATFORM == PLATFORM_WINDOWS)
				auto pSkin = CServiceLocator<IAppSkinService>::GetService();
				if (m_bDragAccepted) {
					::SetCursor(pSkin->Cursor(CUR_DRAGDROP));
				} else {
					::SetCursor(pSkin->Cursor(CUR_DRAGDROP_NO));
				}
#endif//PLATFORM
			}
		}
	}

	if (m_pItemFocused && (m_pItemFocused != pItem)) {
		m_pItemFocused->OnMouseMove(x, y);
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDown
//---------------------------------------------------------------------------//
auto CMGApplication::OnMouseDown(int x, int y, int button) -> void {
	CMGControl* pItem = nullptr;
	if (m_pItemTop) {
		pItem = m_pItemTop->GetItemAt(x, y);
		if (!pItem) {
			pItem = m_pItemTop;
		}

		pItem->OnMouseDown(x, y, button);

		if (m_pItemFocused && (m_pItemFocused != pItem) && m_pItemFocused->IsInside(x, y)) {
			m_pItemFocused->OnMouseDown(x, y, button);
		}
	} else {
		pItem = GetItemAt(x, y);
		if (pItem) {
			pItem->OnMouseDown(x, y, button);
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseUp
//---------------------------------------------------------------------------//
auto CMGApplication::OnMouseUp(int x, int y, int button) -> void {
	// If moving main window, handle special case
	if (m_bMovingWindow) {
		m_bMovingWindow = false;
		m_tWinRect.x += x - m_iPosMouseX;
		m_tWinRect.y += y - m_iPosMouseY;

#if (PLATFORM == PLATFORM_WINDOWS)
		MoveWindow(m_pAppWindow->GetHWnd(), m_tWinRect.x, m_tWinRect.y, m_tWinRect.w, m_tWinRect.h, true);
#endif//PLATFORM
		return;
	}

	// Other cases, treat normally
	CMGControl* pItem = nullptr;
	if (m_pItemTop) {
		pItem = m_pItemTop->GetItemAt(x, y);
		if (!pItem) {
			pItem = m_pItemTop;
		}
		pItem->OnMouseUp(x, y, button);

		if (m_pItemFocused && (m_pItemFocused != pItem)) {
			m_pItemFocused->OnMouseUp(x, y, button);
		}
	} else {
		if (m_pItemDragged) {
			if (m_bDragAccepted) {
				pItem = GetItemAt(x, y);
				if (pItem) {
					pItem->OnDragDrop(m_pItemDragged, x, y);
				}
			}
			DragItem(nullptr);
		} else {
			if (m_pItemFocused) {
				m_pItemFocused->OnMouseUp(x, y, button);
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	OnMouseDoubleClk
//---------------------------------------------------------------------------//
auto CMGApplication::OnMouseDoubleClk(int x, int y, int button) -> void {
	CMGControl* pItem = nullptr;
	if (m_pItemTop) {
		pItem = m_pItemTop->GetItemAt(x, y);
		if (!pItem) {
			pItem = m_pItemTop;
		}
	} else {
		pItem = GetItemAt(x, y);
	}
	if (pItem) {
		pItem->OnMouseDoubleClk(x, y, button);
	}
}


//---------------------------------------------------------------------------//
//	OnVKeyDown
//---------------------------------------------------------------------------//
auto CMGApplication::OnVKeyDown(int vkey) -> void {
	if (m_pItemFocused) {
		m_pItemFocused->OnVKeyDown(vkey);
	}
}


//---------------------------------------------------------------------------//
//	OnVKeyUp
//---------------------------------------------------------------------------//
auto CMGApplication::OnVKeyUp(int vkey) -> void {
	if (m_pItemFocused) {
		m_pItemFocused->OnVKeyUp(vkey);
	}
}


//---------------------------------------------------------------------------//
//	OnChar
//---------------------------------------------------------------------------//
auto CMGApplication::OnChar(char c) -> void {
	if (m_pItemFocused) {
		m_pItemFocused->OnChar(c);
	}
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CMGApplication::Update() -> void {
	// Repaint if needed
	m_pAppWindow->ProcessMessages();
	if (m_bRepaint || !m_vRepaintItems.empty()) {
		m_vRepaintItems.clear();	// this is dumb...
		Repaint();
		Flip();
	}
}


//---------------------------------------------------------------------------//
//	Repaint
//---------------------------------------------------------------------------//
auto CMGApplication::Repaint() -> void {
	m_bRepaint = false;
	neon::gui::FillRect(
		CServiceLocator<IAppSkinService>::GetService(),
		TRect(0, 0, neon::gui::GetWidth(), neon::gui::GetHeight()),
		COLOR_BASE_BG
	);

	if (m_pItemRoot) {
		m_pItemRoot->Draw();
	}

	if (m_pItemPopup) {
		m_pItemPopup->Draw();
	}

	if (m_pItemTop) {
		m_pItemTop->Draw();
	}
}


//---------------------------------------------------------------------------//
//	Flip
//---------------------------------------------------------------------------//
auto CMGApplication::Flip() -> void {
	m_pAppWindow->Flip();
	m_bFlip = false;
}


//---------------------------------------------------------------------------//
//	GetItemAt
//---------------------------------------------------------------------------//
auto CMGApplication::GetItemAt(int iX, int iY) -> CMGControl* {
	CMGControl* pItem = nullptr;
	if (m_pItemPopup) {
		pItem = m_pItemPopup->GetItemAt(iX, iY);
	}
	if (!pItem) {
		pItem = m_pItemRoot->GetItemAt(iX, iY);
	}
	return pItem;
}


//---------------------------------------------------------------------------//
//	SetRootItem
//---------------------------------------------------------------------------//
auto CMGApplication::SetRootItem(CMGControl* pItem) -> void {
	m_pItemRoot = pItem;
}


//---------------------------------------------------------------------------//
//	FocusItem
//---------------------------------------------------------------------------//
auto CMGApplication::FocusItem(CMGControl* pItem) -> void {
	if (m_pItemFocused) {
		RepaintItem(m_pItemFocused);
		if (pItem != m_pItemFocused) {
			m_pItemFocused->SetFocus(false);
		}
	}
	m_pItemFocused = pItem;
}


//---------------------------------------------------------------------------//
//	TopItem
//---------------------------------------------------------------------------//
auto CMGApplication::TopItem(CMGControl* pItem) -> void {
	if (m_pItemTop) {
		RepaintItem(m_pItemTop);
	}
	m_pItemTop = pItem;
}


//---------------------------------------------------------------------------//
//	RepaintItem
//---------------------------------------------------------------------------//
auto CMGApplication::RepaintItem(CMGControl* pItem) -> void {
	// this is dumb.  above if there's any items in repaint, it simply clears the list and repaints.
	// so this doesn't need to 
	if (std::find(m_vRepaintItems.begin(), m_vRepaintItems.end(), pItem) == m_vRepaintItems.end()) {
		m_vRepaintItems.push_back(pItem);
	}
	m_bRepaint = true;
}


//---------------------------------------------------------------------------//
//	DragItem
//---------------------------------------------------------------------------//
auto CMGApplication::DragItem(CMGControl* pItem) -> void {
	m_pItemDragged = pItem;
	m_bDragAccepted = false;

#if (PLATFORM == PLATFORM_WINDOWS)
	auto pSkin = CServiceLocator<IAppSkinService>::GetService();
	::SetCursor(pItem ? pSkin->Cursor(CUR_DRAGDROP_NO) : pSkin->Cursor(CUR_NORMAL));
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	PopupWindow
//---------------------------------------------------------------------------//
auto CMGApplication::PopupWindow(CMGWindow* pItem, int iX, int iY) -> void {
	if (!m_pItemPopup ^ !pItem) {	// what is this??
		if (iX != -1 || iY != -1) {
			if (iX + pItem->Width() > m_tWinRect.w) {
				iX = m_tWinRect.w - pItem->Width();
			}
			if (iY + pItem->Height() >  m_tWinRect.h) {
				iY =  m_tWinRect.h - pItem->Height();
			}
			pItem->SetPos(iX, iY);
		}

		if (m_pItemPopup) {
			m_pItemPopup->SetVisible(false);
		}

		m_pItemPopup = pItem;

		if (m_pItemPopup) {
			m_pItemPopup->SetVisible(true);
		}
		Repaint();
	}
}


//---------------------------------------------------------------------------//
//	OpenFileOpen
//	Open file for reading
//---------------------------------------------------------------------------//
auto CMGApplication::OpenFileOpen(
		std::string const& sTitle,
		std::string const& sInitialDir,
		char const* sWildcard,
		std::string& sFile) -> bool {

#if (PLATFORM == PLATFORM_WINDOWS)
	sFile.clear();
	PushDir(".");
	char pBuffer[MAX_PATH] = { 0 };
	OPENFILENAME ofn;
	memset(&ofn, 0, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hInstance = m_hInst;
	ofn.hwndOwner = m_hWnd;
	ofn.lpstrInitialDir = sInitialDir.c_str();
	ofn.lpstrFilter = sWildcard;
	ofn.lpstrFile = pBuffer;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrTitle = sTitle.c_str();
	ofn.nFilterIndex = 1;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NONETWORKBUTTON;

	if (GetOpenFileName(&ofn)) {
		sFile = ofn.lpstrFile;
	}

	PopDir();

	return !sFile.empty();
#else
	return false;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	OpenFileSave
//	Open file for writing
//---------------------------------------------------------------------------//
auto CMGApplication::OpenFileSave(
		std::string const& sTitle,
		std::string const& sInitialDir,
		char const* sWildcard,
		std::string const& sDefExt,
		std::string& sFile) -> bool {

#if (PLATFORM == PLATFORM_WINDOWS)
	sFile.clear();
	PushDir(".");
	char pBuffer[MAX_PATH] = { 0 };
	OPENFILENAME ofn;
	memset(&ofn, 0, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hInstance = m_hInst;
	ofn.hwndOwner = m_hWnd;
	ofn.lpstrInitialDir = sInitialDir.c_str();
	ofn.lpstrFilter = sWildcard;
	ofn.lpstrFile = pBuffer;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrTitle = sTitle.c_str();
	ofn.lpstrDefExt = sDefExt.c_str();
	ofn.nFilterIndex = 1;
	ofn.Flags = OFN_NONETWORKBUTTON | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST;

	if (GetSaveFileName(&ofn)) {
		sFile = ofn.lpstrFile;
	}

	PopDir();

	return !sFile.empty();
#else
	return false;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	PrintMGuiHierarchy
//---------------------------------------------------------------------------//
#ifdef _DEBUG
auto CMGApplication::PrintMGuiHierarchy() const -> void {
#ifdef VERBOSE_MEM_LOG
	if (m_pItemRoot) {
		std::string sRootIndent;
		m_pItemRoot->PrintHierarchy(sRootIndent);
	}
#endif
}
#endif
