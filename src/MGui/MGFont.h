//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGFONT_H
#define NEON_MGFONT_H

#include "MGBase.h"
#include "MGSurface.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <GL/gl.h>
#else
#include <OpenGL/gl.h>
#endif//PLATFORM

class CMGFont {
public:
	enum EAlign {
		LEFT,
		RIGHT,
		HCENTER,
		TOP,
		BOTTOM,
		VCENTER,
	};

public:
	CMGFont();
	~CMGFont();

	auto Init(std::string const& sFont, int Width, int Height, bool bBold, bool bItalic, bool bAntialias) -> bool;
	auto IsOk() -> bool { return m_bOk; }
	auto Print(int x, int y, int w, int h, int halign, int valign, MGColor const& Color, std::string const& sText) const -> void;
	auto TextLength(std::string const& sText, int iLen = -1) const -> int;

#if (PLATFORM == PLATFORM_WINDOWS)
	auto TextHeight() const -> int { return m_tTextMet.tmHeight; }
#else
	auto TextHeight() const -> int { return m_tTextHeight; }
#endif//PLATFORM

private:
	bool m_bOk;
	TMGSurface m_tSurface;
	uint m_iDisplayList;
#if (PLATFORM == PLATFORM_WINDOWS)
	TEXTMETRIC m_tTextMet;
#else
	int m_tTextWidth;
	int m_tTextHeight;
#endif//PLATFORM
	int m_aiCharWidths[256];
};

#endif//NEON_MGFONT_H
