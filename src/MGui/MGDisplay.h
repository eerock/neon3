//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGDISPLAY_H
#define NEON_MGDISPLAY_H

#include "MGBase.h"
#include "MGSkin.h"

namespace neon { namespace gui {
	auto Init(THInst hInstance, int iX, int iY, int iW, int iH) -> bool;
	auto Release() -> void;
	auto OnResize(int iW, int iH) -> void;
	auto GetWidth() -> int;
	auto GetHeight() -> int;
	auto ResizeViewport(int w, int h) -> void;
	auto SetClipRect(TRect const& tRect) -> void;
	auto SetClipRect() -> void;
	auto Draw(TMGSurface const& surface, int x, int y, float fDepth = 0.f) -> void;
	auto Stretch(TMGSurface const& surface, TRect const& tSrc, TRect const& tDst, float fDepth = 0.f) -> void;
	auto Stretch(TMGSurface const& surface, TRect const& tDst, float fDepth = 0.f) -> void;
	auto DrawItem(IAppSkinService* pSkin, TRect const& tSrc, TRect const& tDst, float fDepth = 0.f) -> void;
	auto FillRect(TRect const& tSrc, MGColor const& tColor, float fDepth = 0.f) -> void;
	auto FillRect(IAppSkinService* pSkin, TRect const& tSrc, int iColor, float fDepth = 0.f) -> void;
	auto GradRect(TRect const& tSrc, MGColor const& tColor0, MGColor const& tColor1, float fDepth = 0.f) -> void;
	auto GradRect(IAppSkinService* pSkin, TRect const& tSrc, int iColor0, int iColor1, float fDepth = 0.f) -> void;
	auto Rect(TRect const& tSrc, MGColor const& tColor, float fDepth = 0.f) -> void;
	auto Rect(IAppSkinService* pSkin, TRect const& tSrc, int iColor, float fDepth = 0.f) -> void;
	auto HLine(int x, int y, int w, MGColor const& tColor, float fDepth = 0.f) -> void;
	auto HLine(IAppSkinService* pSkin, int x, int y, int w, int iColor, float fDepth = 0.f) -> void;
	auto VLine(int x, int y, int h, MGColor const& tColor, float fDepth = 0.f) -> void;
	auto VLine(IAppSkinService* pSkin, int x, int y, int h, int iColor, float fDepth = 0.f) -> void;
	auto Text(IAppSkinService* pSkin, int iFont, int x, int y, int w, int h, int halign, int valign, MGColor const& Color, const std::string& sText) -> void;
	auto Text(IAppSkinService* pSkin, int iFont, int x, int y, int w, int h, int halign, int valign, int iColor, const std::string& sText) -> void;
	auto LoadTGAImg(std::string const& sFile) -> std::unique_ptr<TMGSurface>;
	auto FreeImg(std::unique_ptr<TMGSurface> pSurface) -> void;
}}

#endif//NEON_MGDISPLAY_H
