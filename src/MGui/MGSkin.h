//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGSKIN_H
#define NEON_MGSKIN_H

#include "MGBase.h"

//---------------------------------------------------------------------------//
// Skin Colors
enum ESkinColor {
	COLOR_FONT_LABEL,
	COLOR_FONT_BUTTON,
	COLOR_FONT_WINDOW_TITLE,
	COLOR_FONT_EDIT,
	COLOR_FONT_EDIT_ACTIVE,
	COLOR_TAB_BG,
	COLOR_BASE_BG,
	COLOR_BASE_BORDER,
	COLOR_BASE_BORDER_ACTIVE,
	COLOR_GRID_BG,
	COLOR_GRID_BG_ACTIVE,
	COLOR_GRID_BORDER,
	COLOR_GRID_BORDER_ACTIVE,
	COLOR_GRID_CELL_ATTACHED,
	COLOR_GRID_CELL_HOVER,
	COLOR_EDIT_BG,
	COLOR_EDIT_BG_ACTIVE,
	COLOR_EDIT_BORDER,
	COLOR_EDIT_BORDER_ACTIVE,
	MAX_COLORS,
};

//---------------------------------------------------------------------------//
// Skin Fonts
enum ESkinFont {
	FONT_MINI,
	FONT_NORMAL,
	FONT_WINDOW_TITLE,	// these names
	FONT_FX_TITLE,		// are very
	FONT_BPM,			// specific.
	MAX_FONTS,
};

//---------------------------------------------------------------------------//
// Skin Cursors
enum ESkinCursor {
	CUR_NORMAL,
	CUR_DRAGDROP,
	CUR_DRAGDROP_NO,
	MAX_CURSORS,
};

//--------------------------------------------------------------------------//
class IAppSkinService {
public:
	virtual ~IAppSkinService() {}
	virtual auto Interface() const -> TMGSurface const& = 0;
	virtual auto Color(int index) const -> MGColor const& = 0;
	virtual auto Font(int index) const -> CMGFont const& = 0;
	virtual auto Cursor(int index) const -> THCursor const& = 0;
};

//--------------------------------------------------------------------------//
class CMGSkin : public IAppSkinService {
public:
	CMGSkin();
	CMGSkin(THInst hInstance, std::string const& sDir);
	~CMGSkin() override;

	auto Init(THInst hInstance, std::string const& sDir) -> bool;

	auto Interface() const -> TMGSurface const& override {
		return *(m_pInterface);
	}
	auto Color(int index) const -> MGColor const& override {
		return m_atColors[index];
	}
	auto Font(int index) const -> CMGFont const& override {
		return *(m_apFonts[index]);
	}
	auto Cursor(int index) const -> THCursor const& override {
		return m_atCursors[index];
	}

private:
	THCursor m_atCursors[MAX_CURSORS];
	MGColor m_atColors[MAX_COLORS];
	std::unique_ptr<CMGFont> m_apFonts[MAX_FONTS];
	std::unique_ptr<TMGSurface> m_pInterface;
};

#endif//NEON_MGSKIN_H
