//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGFont.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <GL/gl.h>
#else
#include <OpenGL/gl.h>
#endif//PLATFORM


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMGFont::CMGFont()
	: m_bOk(false)
	, m_tSurface()
	, m_iDisplayList(0)
{}

//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMGFont::Init(std::string const& sFont, int iWidth, int iHeight, bool bBold, bool bItalic, bool bAntialias) -> bool {
#if (PLATFORM == PLATFORM_WINDOWS)
	UNREFERENCED_PARAMETER(bAntialias);
	m_iDisplayList = glGenLists(96);
	HFONT Font = CreateFont(
		iHeight,
		iWidth,
		0,
		0,
		bBold ? FW_BOLD : FW_NORMAL,
		bItalic,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		FF_DONTCARE | DEFAULT_PITCH,
		sFont.c_str()
	);

	HDC hDC = (HDC)g_pMGApp->GetHDC();
	HFONT oldfont = (HFONT)SelectObject(hDC, Font);
	wglUseFontBitmaps(hDC, 32, 96, m_iDisplayList);
	ABC abc[256];
	GetTextMetrics(hDC, &m_tTextMet);
	GetCharABCWidths(hDC, 0, 255, abc);
	for (int i = 0; i < 256; i++) {
		m_aiCharWidths[i] = abc[i].abcA + abc[i].abcB + abc[i].abcC;
	}
	SelectObject(hDC, oldfont);
	DeleteObject(Font);
#else
	// ebp-> todo: implement OSX TextMetrics and Font handling.
	m_tTextWidth = iWidth;
	m_tTextHeight = iHeight;
#endif//PLATFORM
	return true;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMGFont::~CMGFont() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMGFont\n"));
#endif
	glDeleteLists(m_iDisplayList, 96);
}


//---------------------------------------------------------------------------//
//	Print
//---------------------------------------------------------------------------//
auto CMGFont::Print(int x, int y, int w, int h, int halign, int valign, MGColor const& tColor, std::string const& sText) const -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	// HAlign
	if (halign != LEFT) {
		int length = TextLength(sText);
		if (halign == HCENTER) {
			x += (w - length) >> 1;
		} else {
			x = w - length;
		}
	}
	// VAlign
	if (valign != TOP) {
		if (valign == VCENTER) {
			y += (h - m_tTextMet.tmHeight) >> 1;
		} else {
			y = h - m_tTextMet.tmHeight;
		}
	}

	// ebp-> more openGL stuff?  is it only used for text or what?
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_COLOR,GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	glColor4f(tColor.r, tColor.g, tColor.b, 1);
	glRasterPos2i(x, y + m_tTextMet.tmAscent);
	glPushAttrib(GL_LIST_BIT);
	glListBase(m_iDisplayList - 32);
	glCallLists(static_cast<GLsizei>(sText.length()), GL_UNSIGNED_BYTE, sText.c_str());
	glPopAttrib();
	glDisable(GL_BLEND);
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	TextLength
//---------------------------------------------------------------------------//
auto CMGFont::TextLength(std::string const& sText, int iLen) const -> int {
#if (PLATFORM == PLATFORM_WINDOWS)
	if (iLen < 0) {
		iLen = static_cast<int>(sText.length());
	}
	int textLength = 0;
	for (int i = 0; i < iLen; i++) {
		textLength += m_aiCharWidths[sText[i]];
	}
	return textLength;
#else
    return 0;
#endif//PLATFORM
}
