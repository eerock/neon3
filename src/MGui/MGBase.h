//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MGBASE_H
#define NEON_MGBASE_H

#include "Base.h"
#include "MGApplication.h"
#include "MGSurface.h"
#include "MGFont.h"

#define __PLACEMENT_NEW_INLINE
#include <boost/function.hpp>
#include <boost/bind.hpp>
#undef __PLACEMENT_NEW_INLINE

using namespace boost;
using boost::function;
using boost::bind;

// ebp-> Globals extern CMGApplication g_pMGApp!
extern CMGApplication* g_pMGApp;

typedef enum {
	MOUSE_LEFT = 1,
	MOUSE_RIGHT,
	MOUSE_MIDDLE,
	MOUSE_WHEEL_UP,
	MOUSE_WHEEL_DOWN,
	MOUSE_NUM_BUTTONS,
} EMouseButton;

typedef enum {
	MGALIGN_NONE,		// what does this mean?  zero-extent?
	MGALIGN_CLIENT,		// this, i assume, means child just fills the parent.
	MGALIGN_LEFT,		// align child left inside parent
	MGALIGN_RIGHT,		// align child right inside parent
	MGALIGN_TOP,		// align child top inside parent
	MGALIGN_BOTTOM,		// align child bottom inside parent
} EAlign;

#endif//NEON_MGBASE_H
