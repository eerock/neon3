//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MGPch.h"
#include "MGDisplay.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <gl/gl.h>
#else
#include <OpenGL/gl.h>
#endif//PLATFORM

namespace neon { namespace gui {

	int width = 0;
	int height = 0;

	auto Init(THInst hInstance, int iX, int iY, int iW, int iH) -> bool {
		UNREFERENCED_PARAMETER(hInstance);
		UNREFERENCED_PARAMETER(iY);
		UNREFERENCED_PARAMETER(iX);

#ifdef _DEBUG
		auto pString = glGetString(GL_VENDOR);
		GLOG(("OpenGL Vendor: %s\n", pString));
		pString = glGetString(GL_VERSION);
		GLOG(("OpenGL Version: %s\n", pString));
		//pString = glGetString(GL_EXTENSIONS);
		//GLOG_LARGE_MSG(("OpenGL Extensions: %s\n", pString));
#endif
		glClearColor(0.f, 0.f, 0.f, 1.f);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_GEQUAL);
		glShadeModel(GL_SMOOTH);
		glDisable(GL_TEXTURE_2D);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		OnResize(iW, iH);

		return true;
	}

	auto Release() -> void {
		// what to release?  these are namespace functions
	}

	auto OnResize(int iW, int iH) -> void {
		width = iW;
		height = iH;
		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.f, width, height, 0.f, -1.f, 1.f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glClear(GL_COLOR_BUFFER_BIT);
	}

	auto GetWidth() -> int {
		return width;
	}

	auto GetHeight() -> int {
		return height;
	}

	auto ResizeViewport(int w, int h) -> void {
		glViewport(0, 0, w, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.f, w, h, 0.f, -1.f, 1.f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glClear(GL_COLOR_BUFFER_BIT);
	}

	auto SetClipRect(TRect const& tRect) -> void {
		glScissor(tRect.x, height - (tRect.y + tRect.h), tRect.w, tRect.h);
		glEnable(GL_SCISSOR_TEST);
	}

	auto SetClipRect() -> void {
		glDisable(GL_SCISSOR_TEST);
	}

	auto Draw(TMGSurface const& surface, int x, int y, float fDepth) -> void {
		Stretch(surface, TRect(0, 0, surface.w, surface.h), TRect(x, y, surface.w, surface.h), fDepth);
	}

	auto Stretch(TMGSurface const& surface, TRect const& tDst, float fDepth) -> void {
		Stretch(surface, TRect(0, 0, surface.w, surface.h), tDst, fDepth);
	}

	auto Stretch(TMGSurface const& surface, TRect const& tSrc, TRect const& tDst, float fDepth) -> void {
		if (surface.uTextureID == INVALID_ID) {
			return;
		}

		glAlphaFunc(GL_GREATER, 0.1f);		// Set Alpha Testing     (disable blending)
		glEnable(GL_ALPHA_TEST);			// Enable Alpha Testing  (disable blending)

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, surface.uTextureID);
		glDisable(GL_DEPTH_TEST);
		glColor4f(1.f, 1.f, 1.f, 1.f);

		float const x0 = tSrc.x * surface.fInvW;
		float const y0 = tSrc.y * surface.fInvH;
		float const x1 = (tSrc.x + tSrc.w) * surface.fInvW;
		float const y1 = (tSrc.y + tSrc.h) * surface.fInvH;

		glBegin(GL_QUADS);
		glTexCoord2f(x0, y0);
		glVertex3f((float)tDst.x, (float)tDst.y, fDepth);
		glTexCoord2f(x1, y0);
		glVertex3f((float)tDst.x + (float)tDst.w, (float)tDst.y, fDepth);
		glTexCoord2f(x1, y1);
		glVertex3f((float)tDst.x + (float)tDst.w, (float)tDst.y + (float)tDst.h, fDepth);
		glTexCoord2f(x0, y1);
		glVertex3f((float)tDst.x, (float)tDst.y + (float)tDst.h, fDepth);
		glEnd();

		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
	}

	auto DrawItem(IAppSkinService* pSkin, TRect const& tSrc, TRect const& tDst, float fDepth) -> void {
		Stretch(pSkin->Interface(), tSrc, tDst, fDepth);
	}

	auto FillRect(TRect const& tSrc, MGColor const& tColor, float fDepth) -> void {
		glColor3f(tColor.r, tColor.g, tColor.b);
		glBegin(GL_QUADS);
		glVertex3i(tSrc.x, tSrc.y, (int)fDepth);
		glVertex3i(tSrc.x + tSrc.w, tSrc.y, (int)fDepth);
		glVertex3i(tSrc.x + tSrc.w, tSrc.y + tSrc.h, (int)fDepth);
		glVertex3i(tSrc.x, tSrc.y + tSrc.h, (int)fDepth);
		glEnd();
	}

	auto FillRect(IAppSkinService* pSkin, TRect const& tSrc, int iColor, float fDepth) -> void {
		FillRect(tSrc, pSkin->Color(iColor), fDepth);
	}

	auto GradRect(TRect const& tSrc, MGColor const& tColor0, MGColor const& tColor1, float fDepth) -> void {
		glBegin(GL_QUADS);
		glColor3f(tColor0.r, tColor0.g, tColor0.b);
		glVertex3i(tSrc.x, tSrc.y, (int)fDepth);
		glVertex3i(tSrc.x + tSrc.w, tSrc.y, (int)fDepth);
		glColor3f(tColor1.r, tColor1.g, tColor1.b);
		glVertex3i(tSrc.x + tSrc.w, tSrc.y + tSrc.h, (int)fDepth);
		glVertex3i(tSrc.x, tSrc.y + tSrc.h, (int)fDepth);
		glEnd();
	}

	auto GradRect(IAppSkinService* pSkin, TRect const& tSrc, int iColor0, int iColor1, float fDepth) -> void {
		GradRect(tSrc, pSkin->Color(iColor0), pSkin->Color(iColor1), fDepth);
	}

	auto Rect(TRect const& tSrc, MGColor const& tColor, float fDepth) -> void {
		glColor3f(tColor.r, tColor.g, tColor.b);

		float const x0 = tSrc.x + 0.5f;
		float const x1 = tSrc.x + tSrc.w - 0.5f;
		float const y0 = tSrc.y + 0.5f;
		float const y1 = tSrc.y + tSrc.h - 0.5f;

		glBegin(GL_LINE_STRIP);
		glVertex3f(x0, y0, fDepth);
		glVertex3f(x1, y0, fDepth);
		glVertex3f(x1, y1, fDepth);
		glVertex3f(x0, y1, fDepth);
		glVertex3f(x0, y0, fDepth);
		glEnd();
	}

	auto Rect(IAppSkinService* pSkin, TRect const& tSrc, int iColor, float fDepth) -> void {
		Rect(tSrc, pSkin->Color(iColor), fDepth);
	}

	auto HLine(int x, int y, int w, MGColor const& tColor, float fDepth) -> void {
		glColor3f(tColor.r, tColor.g, tColor.b);
		glBegin(GL_LINES);
		glVertex3f(x + 0.5f, y - 0.5f, fDepth);
		glVertex3f(x + w - 0.5f, y - 0.5f, fDepth);
		glEnd();
	}

	auto HLine(IAppSkinService* pSkin, int x, int y, int w, int iColor, float fDepth) -> void {
		HLine(x, y, w, pSkin->Color(iColor), fDepth);
	}

	auto VLine(int x, int y, int h, MGColor const& tColor, float fDepth) -> void {
		glColor3f(tColor.r, tColor.g, tColor.b);
		glBegin(GL_LINES);
		glVertex3f(x - 0.5f, y + 0.5f, fDepth);
		glVertex3f(x - 0.5f, y + h - 0.5f, fDepth);
		glEnd();
	}

	auto VLine(IAppSkinService* pSkin, int x, int y, int h, int iColor, float fDepth) -> void {
		VLine(x, y, h, pSkin->Color(iColor), fDepth);
	}

	auto Text(IAppSkinService* pSkin, int iFont, int x, int y, int w, int h, int halign, int valign, MGColor const& tColor, std::string const& sText) -> void {
		pSkin->Font(iFont).Print(x, y, w, h, halign, valign, tColor, sText);
	}

	auto Text(IAppSkinService* pSkin, int iFont, int x, int y, int w, int h, int halign, int valign, int iColor, std::string const& sText) -> void {
		pSkin->Font(iFont).Print(x, y, w, h, halign, valign, pSkin->Color(iColor), sText);
	}

	auto LoadTGAImg(std::string const& sFile) -> std::unique_ptr<TMGSurface> {
		std::unique_ptr<TMGSurface> pSurface = std::make_unique<TMGSurface>();
		pSurface->tTga.LoadFromFile(sFile);
		glGenTextures(1, &pSurface->uTextureID);
		glBindTexture(GL_TEXTURE_2D, pSurface->uTextureID);
		glTexImage2D(GL_TEXTURE_2D, 0, 4, pSurface->tTga.GetImageWidth(), pSurface->tTga.GetImageHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, pSurface->tTga.GetPixels());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		pSurface->w = pSurface->tTga.GetImageWidth();
		pSurface->h = pSurface->tTga.GetImageHeight();
		pSurface->fInvW = 1.f / pSurface->w;
		pSurface->fInvH = 1.f / pSurface->h;
		return pSurface;
	}

	auto FreeImg(std::unique_ptr<TMGSurface> pSurface) -> void {
		if (pSurface != nullptr && pSurface->uTextureID != INVALID_ID) {
			glDeleteTextures(1, &pSurface->uTextureID);
		}
	}
}} // namespace gui, namespace neon