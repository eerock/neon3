//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "Sources/FFMpegDecoder.h"

// Neon's resource header for icon id
#include "../Neon/resource1.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#include <WinUser.h>
#include <WindowsX.h>
#endif

#define WINDOW_TITLE	"Output"
#define WINDOW_NAME		VERSION_LONG " " WINDOW_TITLE

auto PeticionEndApp() -> void;

//---------------------------------------------------------------------------//
//	Global
//---------------------------------------------------------------------------//
// ebp-> Global CDisplayDevice g_DisplayDevice!
CDisplayDevice g_DisplayDevice;


//---------------------------------------------------------------------------//
//	s_WndProc
//
//	s_WndProc is static and as such can be used as the main WNDPROC function.
//	It will use GWL_USERDATA to set/get the pointer to the CDisplayWindow then 
//	call a non-static member WndProc function.
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CALLBACK CDisplayWindow::s_WndProc(THWnd hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) -> LRESULT {
	CDisplayWindow* self = nullptr;
	if (Msg == WM_CREATE) {
		auto lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
		self = reinterpret_cast<CDisplayWindow*>(lpcs->lpCreateParams);
		self->m_hWnd = hWnd;
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(self));
		return NULL;
	} else {
		self = reinterpret_cast<CDisplayWindow*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
		if (!self) {
			return DefWindowProc(hWnd, Msg, wParam, lParam);
		}
	}

	return self->WndProc(Msg, wParam, lParam);
}

//---------------------------------------------------------------------------//
//	WndProc
//	non-static member function
//---------------------------------------------------------------------------//
auto CDisplayWindow::WndProc(UINT Msg, WPARAM wParam, LPARAM lParam) -> LRESULT {
	switch (Msg) {
		case WM_DESTROY: {
			PostQuitMessage(0);
			return NULL;
		}
		case WM_MOVE: {
			//GLOG(("Attempting to Move window to (%d, %d)\n", GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
			//return NULL;
			break;
		}
		case WM_SIZE: {
			//GLOG(("Attempting to Size window to (%d, %d)\n", GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
			//return NULL;
			break;
		}
		case WM_CLOSE: {
			return NULL;
		}
		case WM_ACTIVATEAPP: {
			ShowCursor(ShowMouse());
			SetActive(wParam ? true : false);
			return NULL;
		}
		case WM_SETCURSOR: {
			SetCursor(ShowMouse() ? LoadCursor(0, IDC_ARROW) : NULL);
			return NULL;
		}
		case WM_KEYDOWN: {
			InputKey((int)wParam);
			return NULL;
		}
		case WM_MOUSEMOVE: {
			MouseMove(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			return NULL;
		}
		case WM_LBUTTONDOWN: {
			SetCapture(m_hWnd);
			LButtonDown(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			return NULL;
		}
		case WM_LBUTTONUP: {
			ReleaseCapture();
			LButtonUp(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			return NULL;
		}
		case WM_LBUTTONDBLCLK: {
			SwitchFullScreen(!IsFullScreen());
			return NULL;
		}
		default: {
			break;
		}
	}
	return DefWindowProc(m_hWnd, Msg, wParam, lParam);
}
#endif


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CDisplayWindow::CDisplayWindow()
	: m_bOk(false)
	, m_bIsActive(false)
	, m_hInst(0)
	, m_hWnd(0)
	, m_hWndMain(0)
	, m_tModo()
	, m_tWindow()
	, m_bShowMouse(false)
	, m_bUpdateDevice(false)
	, m_bCanMove(false)
	, m_bMovingWindow(false)
	, m_iOldWindowPosX(0)
	, m_iOldWindowPosY(0)
	, m_iMousePosX(0)
	, m_iMousePosY(0)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CDisplayWindow::Init(TGraphicsMode const& tMode,
						  TRect const& rtWindow,
						  bool bCanMove,
						  THInst hInst,
						  THWnd hWndMain,
						  bool bShowMouse) -> bool {
	m_tModo = tMode;
	m_bCanMove = bCanMove;
	m_bMovingWindow = false;
	m_bShowMouse = bShowMouse;

	m_bUpdateDevice = false;
	m_iOldWindowPosX = 0;
	m_iOldWindowPosY = 0;

	m_tWindow = rtWindow;
	m_hWndMain = hWndMain;
	if (InitWindow(hInst, m_tModo.bFullScreen)) {
		// Inits
		if (g_DisplayDevice.Init(m_tModo, GetHandle())) {
			// ebp(2014)-> what was this all about?
			//if (tMode.bFullScreen) {
			//	SetWindowLongPtr(m_hWnd, GWLP_WNDPROC, (LONG_PTR)s_WndProc);
			//}
			CFFMpegDecoder::InitSubsystem();
			m_bOk = true;
		} else {
			GLOG(("ERROR: Can't initialize DisplayDevice\n"));
		}
	} else {
		GLOG(("ERROR: Can't initialize output window\n"));
	}

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CDisplayWindow::~CDisplayWindow() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CDisplayWindow\n"));
#endif
	if (IsOk()) {

		CFFMpegDecoder::EndSubsystem();

		// ebp-> todo: refactor this (maybe make it a pointer)
		g_DisplayDevice.End();

#if (PLATFORM == PLATFORM_WINDOWS)
		DestroyWindow(m_hWnd);
		ShowCursor(TRUE);
		CoUninitialize();
#endif
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	InitWindow
//---------------------------------------------------------------------------//
auto CDisplayWindow::InitWindow(THInst hInst, bool bFullscreen) -> bool {
#if (PLATFORM == PLATFORM_WINDOWS)
	ShowCursor(FALSE);
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wc.lpfnWndProc = (WNDPROC)s_WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = NULL;	//LoadIcon(wc.hInstance, (LPCTSTR)IDI_ICON1);
	wc.hCursor = NULL;	//LoadCursor(hInst, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = WINDOW_NAME;
	wc.lpszClassName = WINDOW_NAME;
	RegisterClass(&wc);

	int w = bFullscreen ? 0 : m_tWindow.w;
	int h = bFullscreen ? 0 : m_tWindow.h;

	// ebp-> this creates the graphics output window...
	m_hWnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		WINDOW_NAME,
		WINDOW_TITLE,
		WS_POPUP,
		m_tWindow.x,
		m_tWindow.y,
		w, h,
		NULL,
		NULL,
		hInst,
		this	// important for getting pointer to CDisplayWindow in the static WNDPROC!
	);

	//RECT Rect;
	//GetWindowRect(m_hWnd, &Rect);
	//m_tWindow.x = Rect.left;
	//m_tWindow.y = Rect.top;
	//m_tWindow.w = Rect.right - Rect.left;
	//m_tWindow.h = Rect.bottom - Rect.top;

	if (!m_hWnd) {
		GLOG(("ERROR: Can't create master output window!\n"));
		return false;
	}

	m_hInst = hInst;
	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);
	SetFocus(m_hWnd);
	ShowCursor(ShowMouse());

	return true;
#else
	return false;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	AdjustWindow
//---------------------------------------------------------------------------//
auto CDisplayWindow::AdjustWindow() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	/*
	if (!bFullScreen) {
		RECT rectWindow;
		RECT rectClient;
		long lCoorxWin, lCooryWin;

		GetWindowRect(m_hWnd, &rectWindow);
		GetClientRect(m_hWnd, &rectClient);

		rectClient.right -= m_tWindow.w;
		rectClient.bottom -= m_tWindow.h;
		rectWindow.right -= (rectClient.right - rectClient.left);
		rectWindow.bottom -= (rectClient.bottom - rectClient.top);

		// intenta centrar la ventana
		lCoorxWin = (GetSystemMetrics(SM_CXSCREEN) - 640) /2;
		lCooryWin = (GetSystemMetrics(SM_CYSCREEN) - 480) /2;

		GetWindowRect(m_hWnd, &rectClient);
		lCoorxWin = rectClient.left;
		lCooryWin = rectClient.top;

		LONG lWinLong = GetWindowLong(m_hWnd, GWL_STYLE);
		lWinLong |= (WS_BORDER | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU);
		//lWinLong = g_dwWindowStyle;

		SetWindowLong(
			m_hWnd,
			GWL_STYLE,
			lWinLong
		);
		BOOL b = SetWindowPos(
			m_hWnd,
			HWND_NOTOPMOST, 
			lCoorxWin, lCooryWin, 
			rectWindow.right - rectWindow.left,
			rectWindow.bottom - rectWindow.top, 
			SWP_SHOWWINDOW | SWP_FRAMECHANGED
		);
	} else {
		long lWinLong = GetWindowLong(m_hWnd, GWL_STYLE);
		lWinLong &= ~(WS_BORDER | WS_CAPTION);
		SetWindowLong(m_hWnd, GWL_STYLE, lWinLong);
		SetWindowPos(
			m_hWnd,
			HWND_TOPMOST,
			0, 0,
			GetSystemMetrics(SM_CXSCREEN),
			GetSystemMetrics(SM_CYSCREEN),
			SWP_NOZORDER | SWP_NOACTIVATE
		);
	}

	RedrawWindow(m_hWnd, 0, 0, RDW_FRAME | RDW_INVALIDATE);
	*/
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	ResetWindow
//---------------------------------------------------------------------------//
auto CDisplayWindow::ResetWindow() -> void {
	m_tWindow.x = 0;
	m_tWindow.y = 0;
#if (PLATFORM == PLATFORM_WINDOWS)
	MoveWindow(m_hWnd, m_tWindow.x, m_tWindow.y, m_tWindow.w, m_tWindow.h, true);
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	ProcessMessages
//---------------------------------------------------------------------------//
auto CDisplayWindow::ProcessMessages() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	MSG Msg;
	while (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE)) {
		if (Msg.message == WM_QUIT) {
			PeticionEndApp();
		}
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
		// Send messages to child
		if (m_hWndMain) {
			SendMessage(m_hWndMain, Msg.message, Msg.wParam, Msg.lParam);
		}
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	InputKey
//---------------------------------------------------------------------------//
auto CDisplayWindow::InputKey(int iKey) -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	if (iKey == VK_ESCAPE) {
		//PeticionEndApp();
		// ebp-> don't end the app when user hits escape.
		// besides, this function wasn't even implemented, so...
		// instead let's release fullscreen.
		if (IsFullScreen()) {
			SwitchFullScreen(false);
		}
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	LButtonDown
//---------------------------------------------------------------------------//
auto CDisplayWindow::LButtonDown(int iX, int iY) -> void {
	if (m_bCanMove) {
		m_bMovingWindow = true;
		m_iMousePosX = iX;
		m_iMousePosY = iY;
	}
}


//---------------------------------------------------------------------------//
//	LButtonUp
//---------------------------------------------------------------------------//
auto CDisplayWindow::LButtonUp(int iX, int iY) -> void {
	if (m_bCanMove && m_bMovingWindow) {
		m_bMovingWindow = false;
		m_tWindow.x += iX - m_iMousePosX; 
		m_tWindow.y += iY - m_iMousePosY;
#if (PLATFORM == PLATFORM_WINDOWS)
		MoveWindow(m_hWnd, m_tWindow.x, m_tWindow.y, m_tWindow.w, m_tWindow.h, true);
#endif//PLATFORM
	}
}



//---------------------------------------------------------------------------//
//	LButtonUp
//---------------------------------------------------------------------------//
auto CDisplayWindow::MouseMove(int iX, int iY) -> void {
	if (m_bMovingWindow) {
		m_tWindow.x += iX - m_iMousePosX;
		m_tWindow.y += iY - m_iMousePosY;
#if (PLATFORM == PLATFORM_WINDOWS)
		MoveWindow(m_hWnd, m_tWindow.x, m_tWindow.y, m_tWindow.w, m_tWindow.h, true);
#endif//PLATFORM
	}
}


//---------------------------------------------------------------------------//
//	SwitchFullScreen
//---------------------------------------------------------------------------//
auto CDisplayWindow::SwitchFullScreen(bool bFullScreen) -> void {
	m_bUpdateDevice = true;
	m_tModo.bFullScreen = bFullScreen;
}


//---------------------------------------------------------------------------//
//	UpdateFullScreen
//---------------------------------------------------------------------------//
auto CDisplayWindow::UpdateFullScreen() -> void {
	if (m_bUpdateDevice) {
		m_bUpdateDevice = false;
		m_bCanMove = !m_tModo.bFullScreen;
		if (m_tModo.bFullScreen) {
			// ebp(2014)-> instead of caching the old window pos,
			// should use a new RECT for the fullscreen values,
			// keep the m_tWindow RECT size the same and only update
			// it's position when dragged.
			m_iOldWindowPosX = m_tWindow.x;
			m_iOldWindowPosY = m_tWindow.y;

#if (PLATFORM == PLATFORM_WINDOWS)
			//GetSystemMetrics()
			HMONITOR hMonitor = MonitorFromWindow(m_hWnd, 0); //MONITOR_DEFAULTTONEAREST
			MONITORINFOEX mi;
			mi.cbSize = sizeof(mi);
			GetMonitorInfo(hMonitor, (MONITORINFO*)&mi);
			SetDebugErrorLevel(0);
			SetRect(m_tWindow, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top);
			//GetSystemMetrics(SM_CMONITORS) // Num monitores
			SetWindowPos(m_hWnd, HWND_TOPMOST, m_tWindow.x, m_tWindow.y, m_tWindow.w, m_tWindow.h, SWP_SHOWWINDOW | SWP_NOZORDER | SWP_NOACTIVATE);
		} else {
			SetRect(m_tWindow, m_iOldWindowPosX, m_iOldWindowPosY, m_tModo.iWidth, m_tModo.iHeight);
			SetWindowPos(m_hWnd, NULL, m_tWindow.x, m_tWindow.y, m_tWindow.w, m_tWindow.h, SWP_SHOWWINDOW | SWP_NOZORDER | SWP_NOACTIVATE);
			ShowWindow(m_hWnd, SW_SHOW);
#endif//PLATFORM
		}
	}
}


//---------------------------------------------------------------------------//
//	SetTitle
//---------------------------------------------------------------------------//
auto CDisplayWindow::SetTitle(std::string const& sTitle) -> void {
	SetWindowText(m_hWnd, sTitle.c_str());
}
