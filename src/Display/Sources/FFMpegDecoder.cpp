//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FFMpegDecoder.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#ifdef __cplusplus
extern "C" {
#endif//__cplusplus
#include <stdint.h>
#ifdef __cplusplus
}
#endif//__cplusplus
#endif

//#define MPG_FORMAT			PIX_FMT_RGB565
//#define MPG_FORMAT			PIX_FMT_RGB24
#define MPG_FORMAT				PIX_FMT_RGB32


//---------------------------------------------------------------------------//
//	InitSubsytem
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::InitSubsystem() -> void {
//#if (PLATFORM == PLATFORM_WINDOWS)
	av_register_all();
//#endif
}


//---------------------------------------------------------------------------//
//	EndSubsystem
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::EndSubsystem() -> void {
//#if (PLATFORM == PLATFORM_WINDOWS)
	// teardown...?
//#endif
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFFMpegDecoder::CFFMpegDecoder()
	: m_bOk(false)
	, m_fLength(0.f)
	, m_fStart(0.f)
	, m_fEnd(0.f)
	, m_iWidth(0)
	, m_iHeight(0)
	, m_iFrame(0)
	, m_iCurrentFrame(0)
	, m_iCachedFrame(0)
	, m_iBytesRemaining(0)
	, m_iVideoStreamIndex(0)
#if (PLATFORM == PLATFORM_WINDOWS)
	, m_pFormatCtx(nullptr)
	, m_pCodecCtx(nullptr)
	, m_pCodec(nullptr)
	, m_pFrame(nullptr)
	, m_pFrameRGB(nullptr)
	, m_tPacket()
	, m_pOptionsDict(nullptr)
	, m_pSwsCtx(nullptr)
#endif
	, m_pRawData(nullptr)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::Init(char const* pszFile) -> bool {
//#if (PLATFORM == PLATFORM_WINDOWS)
	// Open video file
	if (avformat_open_input(&m_pFormatCtx, pszFile, nullptr, nullptr) != 0) {
		GLOG(("ERROR: FFMpeg can't open stream file %s\n", pszFile));
		return false;
	}

	// Retrieve stream information
	if (avformat_find_stream_info(m_pFormatCtx, nullptr) < 0) {
		GLOG(("ERROR: FFMpeg can't retrieve stream information for file %s\n", pszFile));
		return false;
	}

	// Output debug information (solo en debug)
#ifdef _DEBUG
	av_dump_format(m_pFormatCtx, 0, pszFile, 0);
#endif

	// Find the first video stream
	m_iVideoStreamIndex = -1;
	for (uint i = 0; i < m_pFormatCtx->nb_streams; ++i) {
		if(m_pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			m_iVideoStreamIndex = i;
			break;
		}
	}

	if (m_iVideoStreamIndex == -1) {
		GLOG(("ERROR: FFMpeg can't find video stream on file %s\n", pszFile));
		return false;
	}

	// Get a pointer to the codec context for the video stream
	m_pCodecCtx = m_pFormatCtx->streams[m_iVideoStreamIndex]->codec;

	// Find the decoder for the video stream
	m_pCodec = avcodec_find_decoder(m_pCodecCtx->codec_id);
	if (!m_pCodec) {
		GLOG(("ERROR: FFMpeg can't find the decoder needed for file %s\n", pszFile));
		return false;
	}

	// ebp-> this ffmpeg tutorial says this CODEC_FLAG_TRUNCATED flag doesn't need to be added anymore...
	// http://dranger.com/ffmpeg/tutorial01.html
	// Inform the codec that we can handle truncated bitstreams -- i.e.,
	// bitstreams where frame boundaries can fall in the middle of packets
	//if (pCodec->capabilities & CODEC_CAP_TRUNCATED) {
	//	m_pCodecCtx->flags |= CODEC_FLAG_TRUNCATED;
	//}

	// Open codec
	if (avcodec_open2(m_pCodecCtx, m_pCodec, &m_pOptionsDict) < 0) {
		GLOG(("ERROR: FFMpeg can't open codec for file %s\n", pszFile));
		return false;
	}

	// ebp-> since this was marked as a 'hack', and the frame_rate(_base) vars have changed, disabling this code.
	// ebp-> this 'hack' was also referenced in the aforementioned tutorial link.
	// Hack to correct wrong frame rates that seem to be generated by some codecs
	//if (m_pCodecCtx->frame_rate > 1000 && m_pCodecCtx->frame_rate_base == 1) {
	//	m_pCodecCtx->frame_rate_base = 1000;
	//}

	// Datos
	m_iWidth = m_pCodecCtx->width;
	m_iHeight = m_pCodecCtx->height;
	m_fLength = (float)m_pFormatCtx->duration / AV_TIME_BASE;
	m_fStart = (float)m_pFormatCtx->start_time / AV_TIME_BASE;
	m_fEnd = m_fStart + m_fLength;

	// Allocate an AVFrame structure
	m_pFrame = av_frame_alloc();
	m_pFrameRGB = av_frame_alloc();
	if (!m_pFrame || !m_pFrameRGB) {
		GLOG(("ERROR: FFMpeg can't allocate AVFrame!\n"));
		return false;
	}

	// ebp-> bugfix: this was in the wrong place and was responsible for the memory
	// allocations steadily climbing while frames were being decoded.
	// ebp-> todo: this function is also deprected, use sws_alloc_context & sws_init_context instead.
	m_pSwsCtx = sws_getContext(
		m_iWidth, m_iHeight,
		m_pCodecCtx->pix_fmt,
		m_iWidth, m_iHeight,
		MPG_FORMAT,
			//SWS_BILINEAR,
			SWS_BICUBIC,
			//SWS_GAUSS,
		nullptr, nullptr, nullptr
	);

	av_init_packet(&m_tPacket);

	m_iBytesRemaining = 0;
	m_tPacket.data = nullptr;
	m_tPacket.size = 0;
	m_iCachedFrame = -1;
	m_iCurrentFrame = -1;
	m_iFrame = 0;
	m_bOk = true;
//#endif

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFFMpegDecoder::~CFFMpegDecoder() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFFMpegDecoder\n"));
#endif
	if (IsOk()) {
//#if (PLATFORM == PLATFORM_WINDOWS)
		av_free(m_pFrame);
		av_free(m_pFrameRGB);
		avcodec_close(m_pCodecCtx);
		avformat_close_input(&m_pFormatCtx);

		sws_freeContext(m_pSwsCtx);

		m_pFormatCtx = nullptr;
		m_pCodecCtx = nullptr;
		m_pFrame = nullptr;
		m_pFrameRGB = nullptr;
		m_pSwsCtx = nullptr;
//#endif
	}
}


//---------------------------------------------------------------------------//
//	Run
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::Run(float fTime) -> void {
//#if (PLATFORM == PLATFORM_WINDOWS)
	m_iFrame = static_cast<long>((double)fTime / av_q2d(m_pCodecCtx->time_base) / (double)m_pCodecCtx->ticks_per_frame);
	//GLOG(("Current Frame = %d, Calculated Frame = %d,  Time = %f\n", m_iCurrentFrame, m_iFrame, fTime));

	// # 1
	// This is the old code, but it seems incomplete
	// this code hits whenever the video reaches the end to form a loop...
	// in the future, if we add more complex playback behavior, this check and 
	// the seek timestamp value might need to change to a frame-accurate system.
	//if (m_iCurrentFrame > m_iFrame) {
	//	//GLOG(("Seek to beginning\n"));
	//	av_seek_frame(m_pFormatCtx, m_iVideoStreamIndex, 0, AVSEEK_FLAG_ANY);
	//	avcodec_flush_buffers(m_pCodecCtx);
	//	m_iCurrentFrame = -1;
	//}

	// # 2
	// This kinda works for seeking and setting the Start/End
	//if (abs(m_iCurrentFrame - m_iFrame) > 4) {
	//if (m_iCurrentFrame > m_iFrame || m_iFrame - m_iCurrentFrame > 4) {
	//	uint flag = AVSEEK_FLAG_FRAME;
	//	if (m_iCurrentFrame > m_iFrame) {
	//		flag |= AVSEEK_FLAG_BACKWARD;
	//	}
	//	//if (av_seek_frame(m_pFormatCtx, m_iVideoStreamIndex, int64_t(m_iFrame), flag) < 0) {
	//	//	GLOG(("ERROR: av_seek_frame returned an error\n"));
	//	//}
	//	if (avformat_seek_file(m_pFormatCtx, m_iVideoStreamIndex, 0, int64_t(m_iFrame), int64_t(m_iFrame), flag) < 0) {
	//		GLOG(("ERROR: avformat_seek_file returned an error\n"));
	//	}
	//	avcodec_flush_buffers(m_pCodecCtx);
	//	m_iCurrentFrame = m_iFrame - 1;
	//}

	// # 3 
	// This works with the resetting time on attach thing, as far as keeping
	// the playback position correct is concerned, but it doesn't take into
	// account the skipping ahead over large amounts of time.
	if (m_iCurrentFrame > m_iFrame) {
		uint flag = AVSEEK_FLAG_FRAME | AVSEEK_FLAG_BACKWARD;
		if (av_seek_frame(m_pFormatCtx, m_iVideoStreamIndex, int64_t(m_iFrame), flag) < 0) {
			GLOG(("ERROR: ffmpeg.av_seek_frame returned an error code.\n"));
		}
		avcodec_flush_buffers(m_pCodecCtx);
		m_iCurrentFrame = m_iFrame - 1;
	}
//#endif
}


//---------------------------------------------------------------------------//
//	SetTexture
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::SetTexture(CTexture* pTextura) -> void {
//#if (PLATFORM == PLATFORM_WINDOWS)
	TSurfaceDesc Desc;
	if (pTextura->Lock(nullptr, Desc)) {
		//int numBytes = 
		avpicture_fill((AVPicture*)m_pFrameRGB, (uchar*)Desc.pBits, MPG_FORMAT, m_iWidth, m_iHeight);
		pTextura->Unlock();
	}
//#endif
}


//---------------------------------------------------------------------------//
//	GetFrame
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::GetFrame(CTexture *pTextura) -> bool {
	bool bRes = false;

//#if (PLATFORM == PLATFORM_WINDOWS)
	if (m_iFrame == m_iCachedFrame) {
		return true;
	}

	//if (m_iFrame - m_iCurrentFrame > 4) {
	//	uint flag = AVSEEK_FLAG_FRAME;
	//	if (av_seek_frame(m_pFormatCtx, m_iVideoStreamIndex, int64_t(m_iFrame), flag) < 0) {
	//		GLOG(("ERROR: av_seek_frame returned an error\n"));
	//	}
	//	//if (avformat_seek_file(m_pFormatCtx, m_iVideoStreamIndex, INT64_MIN, int64_t(m_iFrame), INT64_MAX, flag) < 0) {
	//	//	GLOG(("ERROR: avformat_seek_file returned an error at frame %d\n", m_iFrame));
	//	//}
	//	avcodec_flush_buffers(m_pCodecCtx);
	//	m_iCurrentFrame = m_iFrame - 1;
	//}

	while (m_iCurrentFrame != m_iFrame) {
		if (!GetNextFrame2()) {
			return false;
		}
	}

	m_iCachedFrame = m_iFrame;

	// Lock de la textura y rellenado
	// es-en: Lock the texture and fill
	TSurfaceDesc Desc;
	if (pTextura->Lock(nullptr, Desc)) {
		// software scale...
		sws_scale(
			m_pSwsCtx,
			m_pFrame->data,
			m_pFrame->linesize,
			0,
			m_pCodecCtx->height,
			m_pFrameRGB->data,
			m_pFrameRGB->linesize
			);

		pTextura->Unlock();
		bRes = true;
	}
//#endif

	return bRes;
}


//---------------------------------------------------------------------------//
//	GetNextFrame2 - NEW VERSION
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::GetNextFrame2() -> bool {
//#if (PLATFORM == PLATFORM_WINDOWS)
	int frameFinished = 0;
	//int bytesDecoded = 0;

	while (av_read_frame(m_pFormatCtx, &m_tPacket) >= 0) {
		// is this packet from a video stream??
		if (m_tPacket.stream_index == m_iVideoStreamIndex) {
			// okay, decode the video frame...
			//bytesDecoded = 
			avcodec_decode_video2(m_pCodecCtx, m_pFrame, &frameFinished, &m_tPacket);

			// did we finish the frame??
			if (frameFinished) {
				++m_iCurrentFrame;
				av_free_packet(&m_tPacket);
				return true;
			}
		}

		// free the packet and continue...
		av_free_packet(&m_tPacket);
	}

	// ebp-> todo: there is a slight pause when a video loops, either there is a 
	// missing frame that needs decoding or something is wrong with the looping code.

	//if (m_tPacket.stream_index == m_iVideoStreamIndex) {
	//	avcodec_decode_video2(m_pCodecCtx, m_pFrame, &frameFinished, &m_tPacket);
	//	av_free_packet(&m_tPacket);
	//}


//#endif
	return false;
}


//---------------------------------------------------------------------------//
//	GetNextFrame - OLD Version (for reference, I guess)
//---------------------------------------------------------------------------//
auto CFFMpegDecoder::GetNextFrame() -> bool {
//#if (PLATFORM == PLATFORM_WINDOWS)
	// Decode packets until we have decoded a complete frame
	int bytesDecoded = 0;
	int frameFinished = 0;

	bool bBreak = false;
	while (!bBreak) {
		// Work on the current packet until we have decoded all of it
		while (m_iBytesRemaining > 0) {
			// Decode the next chunk of data
			bytesDecoded = avcodec_decode_video2(m_pCodecCtx, m_pFrame, &frameFinished, &m_tPacket);

			// Was there an error?
			if (bytesDecoded < 0) {
				return false;
			}

			m_iBytesRemaining -= bytesDecoded;
			m_pRawData += bytesDecoded;

			// Did we finish the current frame? Then we can return
			if (frameFinished) {
				m_iCurrentFrame++;
				return true;
			}
		}

		// Read the next packet, skipping all packets that aren't for this
		// stream
		do {
			// Free old packet
			if (m_tPacket.data != nullptr) {
				av_free_packet(&m_tPacket);
			}

			// Read new packet
			if (av_read_frame(m_pFormatCtx, &m_tPacket) < 0) {
				//goto loop_exit;
				bBreak = true;
				break;
			}

		} while (m_tPacket.stream_index != m_iVideoStreamIndex);

		if (bBreak) {
			break;
		}

		m_iBytesRemaining = m_tPacket.size;
		m_pRawData = m_tPacket.data;
	}

//loop_exit:

	// Decode the rest of the last frame
	bytesDecoded = avcodec_decode_video2(m_pCodecCtx, m_pFrame, &frameFinished, &m_tPacket);
//#endif
	return false;
}
