//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "ImageList.h"


enum EImageListVar {
	VAR_FILL_MODE,
	VAR_RED,
	VAR_GREEN,
	VAR_BLUE,
	VAR_SPEED,
};

// ebp-> todo: remove this and re-define this in Image.h
enum EFillMode {
	STRETCH_FILL,
	STRETCH_NONE,
	STRETCH_TOW,
	STRETCH_TOH,
	NUM_FILL_MODES,
	FXS_IMAGE = 0,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_FILL_MODE, "Fill Mode", false, NUM_FILL_MODES, { "Fill", "None", "To Width", "To Height" } },
	{ TCtrlVar::SLIDER, VAR_RED, "Red", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_GREEN, "Green", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_BLUE, "Blue", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_SPEED, "Speed", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};

static char const* s_Scopes[] = {
	"Image",
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CImageList::CImageList()
	: m_nNumImages(0)
	, m_iImage(0)
	, m_fClipDuration(0.f)
	, m_fAdvanceTime(0.f)
	, m_fClipTime(0.f)
	, m_pImages(nullptr)
	, m_sDirName()
	, m_vsImageNames()
	, m_bUpdate(false)
	, m_uColor(0)
	, m_bGif(false)
{}


//---------------------------------------------------------------------------//
//	Init
//	ebp-> todo: this could use some refactoring.  the code is a bit large.
//---------------------------------------------------------------------------//
auto CImageList::Init(std::string const& sFile) -> bool {
	m_bOk = false;
	m_pImages = nullptr;
	m_iImage = 0;
	m_bUpdate = true;
	m_fClipTime = 0.f;
	m_fAdvanceTime = 0.f;

	m_sName = sFile;

	if ((m_sName.length() > 4) && (m_sName.find(".gif", 0) < m_sName.length())) {
		return InitGif();
	} else if ((m_sName.length() > 4) && (m_sName.find(".nil", 0) < m_sName.length())) {
		return InitNil();
	} else { // check if dir
		return InitDir();
	}
}


//---------------------------------------------------------------------------//
//	GIF image, commonly used as an animation, we just need to decode the
//	gif frames into textures, add them to the texture manager, 
//---------------------------------------------------------------------------//
auto CImageList::InitGif() -> bool {
	m_bGif = true;
	// ebp-> call function to decode gif into a bunch of Textures...
	auto pTextures = CTexture::sLoadFromGIF(m_sName, m_nNumImages);
	if (!pTextures) {
		return false;
	}

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	m_fClipDuration = 0.f;
	m_uColor = HARD_COLOR_ARGB(255, 255, 255, 255);
	m_pImages = NEW_ARRAY(TImage, m_nNumImages);

	for (int i = 0; i < m_nNumImages; ++i) {
		ASSERT(pTextures[i]);
		auto pImage = &m_pImages[i];
		pImage->fDuration = pTextures[i]->GetDuration();
		m_fClipDuration += pImage->fDuration;

		pImage->uTextureID = pMatMgr->AddTexture(pTextures[i]);
		if (pImage->uTextureID == CMaterialManager::INVALID) {
			GLOG(("ERROR: Can't load frame %d for image %s.\n", i, m_sName.c_str()));
		}
	}

	DELETE_ARRAY(pTextures);
	//GLOG(("GIF %s Loaded OK!\n", m_szName.c_str()));
	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	InitNIL - initialize 'Neon Image List'
//---------------------------------------------------------------------------//
auto CImageList::InitNil() -> bool {
	m_bGif = false;
	CFile File;
	if (File.Open(m_sName.c_str(), "r")) {
		// ebp-> might be able to factor away nLines...
		std::string sLine;
		int nLines = 0;
		do {
			File.ReadLn(sLine);
			++nLines;
		} while (!File.Eof());

		// Volver al principio
		File.SeekToBeg();
		// Guardar los nombres
		m_nNumImages = 0;
		m_vsImageNames.clear();
		for (int i = 0; i < nLines; ++i) {
			File.ReadLn(sLine);

			size_t len = sLine.length();

			// Si es un fichero valido
			// es-en: If it is a valid file
			if (len > 4) {
				m_vsImageNames.push_back(sLine);
			}
		}
		File.Close();

		// Si hay alguna imagen, rula bien todo
		// es-en: If there are any images, ??rula?? well all
		if (nLines > 0) {
			auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
			m_pImages = NEW_ARRAY(TImage, nLines);

			for (int i = 0; i < nLines; ++i) {
				auto pImage = &m_pImages[i];
				pImage->uTextureID = pMatMgr->AddTexture(m_vsImageNames[i]);
				if (pImage->uTextureID != CMaterialManager::INVALID) {
					++m_nNumImages;
				} else {
					GLOG(("ERROR: Can't load image file %s\n", m_sName.c_str()));
				}

				pImage->iStretchMode = STRETCH_FILL;
				pImage->vPos = TVector2();
			}
			m_bOk = true;

		} else {
			GLOG(("ERROR: No image found for an Image\n"));
		}
	} else {
		GLOG(("ERROR: Can't open image list file %s\n", m_sName.c_str()));
	}
	return IsOk();
}


//---------------------------------------------------------------------------//
//	InitDir
//---------------------------------------------------------------------------//
auto CImageList::InitDir() -> bool {
	m_bGif = false;
	return IsOk();
	// Solo una imagen
	// es-en: Only one image
	//m_nNumImages = 1;
	//m_pImages = NEW_ARRAY(TImage, m_nNumImages);
	//m_vsImageNames.push_back(sFile.c_str());
	//m_bGif = false;
	// Si hay alguna imagen, rula bien todo
	// es-en: If there are any images, ??rula?? well all
	//if (m_nNumImages > 0) {
	//	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	//	// Images
	//	for (int i = 0; i < m_nNumImages; i++) {
	//		auto pImage = &m_pImages[i];
	//		pImage->uTextureID = pMatMgr->AddTexture(m_vsImageNames[i]);
	//		if (pImage->uTextureID == CMaterialManager::INVALID) {
	//			GLOG(("ERROR: Can't load image file %s\n", sFile.c_str()));
	//		}
	//		pImage->iStretchMode = STRETCH_FILL;
	//		pImage->vPos = TVector2();
	//		pImage->vPosIni= TVector2();
	//		pImage->vSpeed = TVector2();
	//		pImage->vXform = TVector2();
	//	}
	//	m_bOk = true;
	//} else {
	//	GLOG(("ERROR: No image found for an Image\n"));
	//	m_bOk = false;
	//}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CImageList::~CImageList() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CImageList\n"));
#endif
	if (IsOk()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		for (int i = 0; i < m_nNumImages; ++i) {
			if (m_pImages[i].uTextureID != CMaterialManager::INVALID) {
				pMatMgr->RemoveTexture(m_pImages[i].uTextureID);
				m_pImages[i].uTextureID = CMaterialManager::INVALID;
			}
		}
		DELETE_ARRAY(m_pImages);
	}
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CImageList::SetTime(float fTime) -> void {
	CSource::SetTime(fTime);

	m_fClipTime += m_fTimeFrame;
	if (m_fClipTime < 0.f) {
		m_fAdvanceTime = 0.f;
		m_fClipTime = m_fTime;
	}

	//m_pImages[m_iImage].vPos.x = m_pImages[m_iImage].vPosIni.x + m_pImages[m_iImage].vSpeed.x * m_fTime;
	//m_pImages[m_iImage].vPos.y = m_pImages[m_iImage].vPosIni.y + m_pImages[m_iImage].vSpeed.y * m_fTime;

	//if ((m_pImages[m_iImage].vSpeed.x != 0.f) || (m_pImages[m_iImage].vSpeed.y != 0.f)) {
	//	m_bUpdate = true;
	//}

	// ebp-> todo: something like this:
	// m_iImage = GetFrameAtTime(c_fClipTime);
	// where we calculate a frame index from
	// a clip time, that way it's done correctly.
	// then we could probably get
	// rid of the advance time variable

	// use this or something more accurately based on the timing to advance frames in the Image list...

	float fFrameDuration = 0.f;
	if (m_bGif) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		auto pTex = pMatMgr->GetTexture(m_pImages[m_iImage].uTextureID);
		fFrameDuration = pTex->GetDuration();
	} else {
		fFrameDuration = m_pImages[m_iImage].fDuration;
	}

	//GLOG(("Time: %f    Clip time: %f   Frame Time: %f   Gif Time: %f\n", m_fTime, m_fClipTime, m_fAdvanceTime, pTex->GetDuration() + m_fAdvanceTime));
	if (m_fClipTime >= fFrameDuration + m_fAdvanceTime) {
		m_fAdvanceTime += fFrameDuration;
		m_iImage = (++m_iImage % m_nNumImages);
		if (m_iImage == 0) {
			m_fAdvanceTime = 0.f;
			m_fClipTime = 0.f;
		}
	}
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CImageList::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (!m_bUpdate) {
		return;
	}

	auto pImage = &m_pImages[m_iImage];
	ASSERT(pImage);

	pDD->SetRenderTarget(uTargetTexID);

	if (m_bClear || m_bFirstClear) {
		pDD->Clear(true, true, 0, 1.f);
		m_bFirstClear = false;
	}

	// Si la imagen no se cargó bien, salimos
	// es-en: If the image didn't load well, we...
	if (pImage->uTextureID == CMaterialManager::INVALID) {
		return;
	}

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pTex = pMatMgr->GetTexture(pImage->uTextureID);
	ASSERT(pTex);

	// Tamaño
	// es-en: Size
	float fW = static_cast<float>(pDD->ViewportWidth());
	float fH = static_cast<float>(pDD->ViewportHeight());
	float fTexW = static_cast<float>(pTex->GetWidth());
	float fTexH = static_cast<float>(pTex->GetHeight());
	TVector2 vPos = pImage->vPos;
	TVector2 vSize;

	switch (pImage->iStretchMode) {
		case STRETCH_FILL: {
			vSize.x = fW;
			vSize.y = fH;
			break;
		}
		case STRETCH_NONE: {
			vSize.x = fTexW;
			vSize.y = fTexH;
			break;
		}
		case STRETCH_TOW: {
			vSize.x = fW;
			vSize.y = fW * fTexH / fTexW;
			break;
		}
		case STRETCH_TOH: {
			vSize.x = fH * fTexW / fTexH;
			vSize.y = fH;
			break;
		}
		default: {
			break;
		}
	}

	// ebp-> center the position if the dimensions are smaller than the screen.
	if (vSize.x < fW) {
		vPos.x = (fW - vSize.x) * 0.5f;
	}
	if (vSize.y < fH) {
		vPos.y = (fH - vSize.y) * 0.5f;
	}

	// Quad con textura
	// es-en: Textured Quad
	pMatMgr->SetTexture(pImage->uTextureID, 0);

	// RenderStates especificos
	// es-en: Specific RenderStates
	pDD->ApplyBasicShader();
	pDD->SetBilinearFiltering(0, true);

	neon::DrawQuad(pDD, TVector2(vPos.x, vPos.y), TVector2(vPos.x + vSize.x, vPos.y + vSize.y));

	//m_bUpdate = false;
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iTextureTgt);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	GetVarScopes
//---------------------------------------------------------------------------//
auto CImageList::GetVarScopes(std::vector<std::string>& vScopes) const -> void {
	vScopes.clear();
	vScopes.push_back("Image");
}


//---------------------------------------------------------------------------//
//	GetVarObjects
//---------------------------------------------------------------------------//
auto CImageList::GetVarObjects(std::vector<std::string>& vObjects, int iScope) const -> void {
	UNREFERENCED_PARAMETER(iScope);
	vObjects.clear();
	vObjects = m_vsImageNames;
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CImageList::GetVarCtrls(int iScope) const -> TCtrlVar* {
	// ebp-> hmm, -1 is getting passed in from MGEffect.cpp
	// where it does m_VJVars = NEW CMGPanelVars(m_PageVars, m_ID, -1, -1);
	// this makes the Controls not appear for ImageLists.  How to fix?
	//if (iScope == FXS_IMAGE) {
	//	return s_Vars;
	//} else {
	//	return nullptr;
	//}
	UNREFERENCED_PARAMETER(iScope);
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CImageList::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_FILL_MODE: {
			m_bFirstClear = true;
			// ebp-> loop through the TImages and set the stretch mode on all of them...
			for (int i = 0; i < m_nNumImages; ++i) {
				m_pImages[i].iStretchMode = *(static_cast<int const*>(pData));
			}
			break;
		}
		case VAR_RED: {
			HARD_COLOR_SET_R(m_uColor, static_cast<uchar>(*(static_cast<float const*>(pData)) * 255.f));
			break;
		}
		case VAR_GREEN: {
			HARD_COLOR_SET_G(m_uColor, static_cast<uchar>(*(static_cast<float const*>(pData)) * 255.f));
			break;
		}
		case VAR_BLUE: {
			HARD_COLOR_SET_B(m_uColor, static_cast<uchar>(*(static_cast<float const*>(pData)) * 255.f));
			break;
		}
		case VAR_SPEED: {
			if (!m_bGif) {
				for (int i = 0; i < m_nNumImages; ++i) {
					m_pImages[i].fDuration = *(static_cast<float const*>(pData)) * 5.f;
				}
			}
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CImageList::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	auto pImage = &m_pImages[iObj];
	static float val = 0.f;
	switch (iVar) {
		case VAR_FILL_MODE: {
			return &pImage->iStretchMode;
		}
		case VAR_RED: {
			val = HARD_COLOR_GET_R(m_uColor) * 1.f / 255.f;
			return &val;
		}
		case VAR_GREEN: {
			val = HARD_COLOR_GET_G(m_uColor) * 1.f / 255.f;
			return &val;
		}
		case VAR_BLUE: {
			val = HARD_COLOR_GET_B(m_uColor) * 1.f / 255.f;
			return &val;
		}
		case VAR_SPEED: {
			if (!m_bGif) {
				val = pImage->fDuration;
			}
			return &val;
		}
		default: {
			return nullptr;
		}
	}
}
