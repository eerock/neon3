//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FFMpegDecoder.h"

#include "MpgVideo.h"
#include "Xml.h"
#include <cmath>

enum EMpgVideoVar {
	VAR_RED,
	VAR_GREEN,
	VAR_BLUE,
	VAR_START,
	VAR_END,
	VAR_CROP_L,
	VAR_CROP_R,
	VAR_CROP_T,
	VAR_CROP_B,
};


static TCtrlVar s_Vars[] = {
	{ TCtrlVar::SLIDER, VAR_RED, "Red", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_GREEN, "Green", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_BLUE, "Blue", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_START, "Start", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_END, "End", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_CROP_L, "Crop L", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_CROP_R, "Crop R", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_CROP_T, "Crop T", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_CROP_B, "Crop B", false, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMpgVideo::CMpgVideo()
	: m_pVideo(nullptr)
	, m_uTextureID(CMaterialManager::INVALID)
	, m_uColor(0)
	, m_fCropL(0.f)
	, m_fCropR(0.f)
	, m_fCropT(0.f)
	, m_fCropB(0.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMpgVideo::~CMpgVideo() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMpgVideo\n"));
#endif
	if (IsOk()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->RemoveTexture(m_uTextureID);
		DELETE_PTR(m_pVideo);
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMpgVideo::Init(std::string const& sFile) -> bool {
	m_bOk = false;
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	if (sFile != "") {
		m_fLength = 0.f;
		m_fStart = 0.f;
		m_fEnd = 1.f;
		m_pVideo = NEW CFFMpegDecoder;
		if (m_pVideo->Init(sFile.c_str())) {
			auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
			m_uTextureID = pMatMgr->AddTexture(m_pVideo->GetWidth(), m_pVideo->GetHeight(), HARD_TEX_FORMAT_A8R8G8B8);
			if (m_uTextureID == CMaterialManager::INVALID) {
				DELETE_PTR(m_pVideo);
				GLOG(("ERROR: Can't initialize source texture for MPEG file %s\n", sFile.c_str()));
				m_bOk = false;
				return m_bOk;
			}

			m_pVideo->SetTexture(pMatMgr->GetTexture(m_uTextureID));
			m_fLength = m_pVideo->Length();
			m_fStart = 0.f;
			m_fEnd = 1.f;
			m_fCropL = 0.f;
			m_fCropR = 1.f;
			m_fCropT = 0.f;
			m_fCropB = 1.f;
			// ebp-> do we allow reverse playback?
			// does this flag even work?
			m_bAllowRevPlayback = false;
			m_uColor = HARD_COLOR_ARGB(255, 255, 255, 255);
			m_bOk = true;
		} else {
			DELETE_PTR(m_pVideo);
			GLOG(("ERROR: Can't instantiate/initialize MPEG file %s\n", sFile.c_str()));
		}
	} else {
		GLOG(("ERROR: File not specified for an MpgVideo instance\n"));
	}
#else
	UNREFERENCED_PARAMETER(sFile);
#endif//GFX_ENGINE

	return IsOk();
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CMpgVideo::SetTime(float fTime) -> void {
	CSource::SetTime(fTime);

	// Para la reproduccion hacia atras
	// es-en: for backward playback
	//while (m_fTime < 0.f) {
	//	m_fTime += m_fLength;
	//}
	//while (m_fTime >= m_fLength) {
	//	m_fTime -= m_fLength;
	//}

	// Attempt at doing Start/End cropped playback
	if (m_fStart < m_fEnd) {
		while (m_fTime < (m_fLength * m_fStart)) {
			m_fTime += GetLength();
		}
		while (m_fTime >= (m_fLength * m_fEnd)) {
			m_fTime -= GetLength();
		}
	} else {
		while (m_fTime < 0.f) {
			m_fTime += m_fLength;
		}
		while (m_fTime >= m_fLength) {
			m_fTime -= m_fLength;
		}
	}

	m_pVideo->Run(m_fTime);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CMpgVideo::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	pDD->SetRenderTarget(uTargetTexID);

	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pTextura = pMatMgr->GetTexture(m_uTextureID);
	if (!m_pVideo->GetFrame(pTextura)) {
		return;
	}

	// Quad con textura
	pMatMgr->SetTexture(m_uTextureID, 0);

	pDD->ApplyBasicShader();
	pD3D->SetRenderState(D3DRS_CLIPPING, FALSE);
	pD3D->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	pD3D->SetRenderState(D3DRS_TEXTUREFACTOR, m_uColor);
	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TFACTOR);

	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);

	pD3D->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	pD3D->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

	pD3D->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	pD3D->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

	pD3D->SetVertexShader(nullptr);
	pD3D->SetPixelShader(nullptr);

	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	neon::DrawQuadUV(pDD, TVector2(m_fCropL, m_fCropT), TVector2(m_fCropR, m_fCropB), false);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iTextureTgt);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	GetVars
//---------------------------------------------------------------------------//
auto CMpgVideo::GetVarCtrls(int iScope) const -> TCtrlVar* {
	UNREFERENCED_PARAMETER(iScope);
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CMpgVideo::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_RED: {
			HARD_COLOR_SET_R(m_uColor, static_cast<uchar>(*(static_cast<float const*>(pData)) * 255.f));
			break;
		}
		case VAR_GREEN: {
			HARD_COLOR_SET_G(m_uColor, static_cast<uchar>(*(static_cast<float const*>(pData)) * 255.f));
			break;
		}
		case VAR_BLUE: {
			HARD_COLOR_SET_B(m_uColor, static_cast<uchar>(*(static_cast<float const*>(pData)) * 255.f));
			break;
		}
		case VAR_START: {
			m_fStart = *(static_cast<float const*>(pData));
			//GLOG(("Start/End: %f , %f\n", m_fStart, m_fEnd));
			break;
		}
		case VAR_END: {
			m_fEnd = *(static_cast<float const*>(pData));
			//GLOG(("Start/End: %f , %f\n", m_fStart, m_fEnd));
			break;
		}
		case VAR_CROP_L: {
			m_fCropL = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_CROP_R: {
			m_fCropR = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_CROP_T: {
			m_fCropT = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_CROP_B: {
			m_fCropB = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CMpgVideo::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	static float c;
	switch (iVar) {
		case VAR_RED: {
			c = HARD_COLOR_GET_R(m_uColor) * 1.f / 255.f;
			return (&c);
		}
		case VAR_GREEN: {
			c = HARD_COLOR_GET_G(m_uColor) * 1.f / 255.f;
			return (&c);
		}
		case VAR_BLUE: {
			c = HARD_COLOR_GET_B(m_uColor) * 1.f / 255.f;
			return (&c);
		}
		case VAR_START: {
			return &m_fStart;
		}
		case VAR_END: {
			return &m_fEnd;
		}
		case VAR_CROP_L: {
			return &m_fCropL;
		}
		case VAR_CROP_R: {
			return &m_fCropR;
		}
		case VAR_CROP_T: {
			return &m_fCropT;
		}
		case VAR_CROP_B: {
			return &m_fCropB;
		}
		default: {
			return nullptr;
		}
	}
}
