//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"

#include "Color.h"
#include "FFMpegDecoder.h"
#include "Flare.h"
#include "Image.h"
#include "ImageList.h"
#include "Message.h"
#include "MpgVideo.h"
#include "Scene3D.h"
#include "Screen.h"
#include "Source.h"


//---------------------------------------------------------------------------//
//	STATIC factory: Create
//	Crea un efecto de la clase especificada
//	es-en: Creates an effect of the specified class
//---------------------------------------------------------------------------//
auto CSource::Create(std::string const& sClass, std::string const& sFile) -> CSource* {
	CSource* pSource = nullptr;

	if (pSource == nullptr && Stricmp("MpgVideo", sClass) == 0) {
		pSource = NEW CMpgVideo;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("ImageList", sClass) == 0) {
		pSource = NEW CImageList;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("Image", sClass) == 0) {
		pSource = NEW CImage;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("Color", sClass) == 0) {
		pSource = NEW CColor;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("3DScene", sClass) == 0) {
		pSource = NEW CScene3D;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("Screen", sClass) == 0) {
		pSource = NEW CScreen;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("Flare", sClass) == 0) {
		pSource = NEW CFlare;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr && Stricmp("Message", sClass) == 0) {
		pSource = NEW CMessage;
		if (!pSource->Init(sFile)) {
			DELETE_PTR(pSource);
		}
	}
	if (pSource == nullptr) {
		GLOG(("ERROR: Can't create Source from file %s (class %s)\n", sFile.c_str(), sClass.c_str()));
	}

	return pSource;
}


//---------------------------------------------------------------------------//
//	CSource
//---------------------------------------------------------------------------//
CSource::CSource()
	: m_bOk(false)
	, m_bFirstClear(true)
	, m_bClear(true)
	, m_bMotionBlur(false)
	, m_bAllowRevPlayback(true)
	, m_fMotionIntens(0.f)
	, m_fTime(0.f)
	, m_fTimePrev(0.f)
	, m_fTimeFrame(0.f)
	, m_fLength(0.f)
	, m_fStart(0.f)
	, m_fEnd(1.f)
{}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CSource::SetTime(float fTime) -> void {
	m_fTimePrev = m_fTime;
	m_fTime = fTime;
	m_fTimeFrame = m_fTime - m_fTimePrev;
}
