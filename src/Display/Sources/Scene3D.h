//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_SCENE3D_H
#define NEON_SCENE3D_H

#include "Source.h"

class CDisplayDevice;
class CGEScene3D;


class CScene3D : public CSource {
public:
	CScene3D();
	~CScene3D() override;

	auto Init(std::string const& sFile) -> bool override;

	auto SetTime(float fTime) -> void override;
	auto Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void override;

	auto GetVarScopes(std::vector<std::string>& Scopes) const -> void override;
	auto GetVarObjects(std::vector<std::string>& Objects, int iScope) const -> void override;

	auto GetVarCtrls(int iScope) const -> TCtrlVar* override;
	auto SetVar(int iScope, int iObj, int iVar, void const* pData) -> void override;
	auto GetVar(int iScope, int iObj, int iVar) -> void const* override;

protected:
	CGEScene3D* m_pScene3D;
	int m_TexturaBlur;			// ebp-> is this used?  looks like it was added from older version by shine.3p
	float m_fAlphaBackground;
	float m_fR;
	float m_fG;
	float m_fB;
};

#endif//NEON_SCENE3D_H
