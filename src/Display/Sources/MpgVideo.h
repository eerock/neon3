//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MPGVIDEO_H
#define NEON_MPGVIDEO_H

#include "Source.h"

class CFFMpegDecoder;


//---------------------------------------------------------------------------//
//	Class CMpgVideo
//---------------------------------------------------------------------------//
class CMpgVideo : public CSource {
public:
	CMpgVideo();
	~CMpgVideo() override;

	auto Init(std::string const& sFile) -> bool override;

	auto SetTime(float fTime) -> void override;
	auto Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void override;

	auto GetVarCtrls(int iScope) const -> TCtrlVar* override;
	auto SetVar(int iScope, int iObj, int iVar, void const* pData) -> void override;
	auto GetVar(int iScope, int iObj, int iVar) -> void const* override;

private:
	CFFMpegDecoder* m_pVideo;
	uint m_uTextureID;
	uint m_uColor;
	float m_fCropL;
	float m_fCropR;
	float m_fCropT;
	float m_fCropB;
};

#endif//NEON_MPGVIDEO_H
