//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_SOURCE_H
#define NEON_SOURCE_H

#include "GEGraphics.h"

struct TCtrlVar;


//---------------------------------------------------------------------------//
//	Class CSource
//---------------------------------------------------------------------------//
class CSource {
public:
	enum {
		FXSCOPE_MAIN = -1,
	};

	static auto Create(std::string const& sClass, std::string const& sFile) -> CSource*;

	CSource();
	virtual ~CSource() {}

	virtual auto Init(std::string const& sFile) -> bool = 0;

	auto IsOk() const -> bool { return  m_bOk; }
	auto GetLength() const -> float { return  m_fLength * (m_fEnd - m_fStart); }
	auto GetPos() const -> float { return  m_fTime; }

	virtual auto SetTime(float fTime) -> void;
	virtual auto Draw(CDisplayDevice*, uint uTargetTexID) -> void = 0;

	virtual auto GetVarScopes(std::vector<std::string>& vScopes) const -> void { vScopes.clear(); }
	virtual auto GetVarObjects(std::vector<std::string>& vObjects, int /*iScope*/) const -> void { vObjects.clear(); }
	virtual auto GetVarCtrls(int /*iScope*/ = FXSCOPE_MAIN) const -> TCtrlVar* { return nullptr; }

	// ebp-> todo: make these go pData, iVar, iScope, iObject, and give iScope and iObj default values of -1
	virtual auto SetVar(int iScope, int iObj, int iVar, void const* pData) -> void = 0;
	virtual auto GetVar(int iScope, int iObj, int iVar) -> void const* = 0;

	virtual auto OnAttached() -> void {}
	virtual auto OnUnattached() -> void {}

	virtual auto SnapshotNeedsUpdate() const -> bool { return false; }

protected:
	bool m_bOk;
	bool m_bFirstClear;
	bool m_bClear;
	bool m_bMotionBlur;
	bool m_bAllowRevPlayback;
	float m_fMotionIntens;
	float m_fTime;
	float m_fTimePrev;
	float m_fTimeFrame;
	float m_fLength;
	float m_fStart;
	float m_fEnd;
};

#endif//NEON_SOURCE_H
