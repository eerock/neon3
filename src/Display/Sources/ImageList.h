//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_IMAGELIST_H
#define NEON_IMAGELIST_H

#include "Image.h"


//---------------------------------------------------------------------------//
//	Class CImageList
//---------------------------------------------------------------------------//
class CImageList : public CImage {
public:
	CImageList();
	~CImageList() override;

	auto Init(std::string const& sFile) -> bool override;

	auto SetTime(float fTime) -> void override;
	auto Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void override;

	auto GetVarScopes(std::vector<std::string>& vScopes) const -> void override;
	auto GetVarObjects(std::vector<std::string>& vObjects, int iScope) const -> void override;

	auto GetVarCtrls(int iScope) const -> TCtrlVar* override;
	auto SetVar(int iScope, int iObj, int iVar, void const* pData) -> void override;
	auto GetVar(int iScope, int iObj, int iVar) -> void const* override;

protected:
	int m_nNumImages;			// Number of images in the list
	int m_iImage;				// Current image index
	float m_fClipDuration;		// Duration of this image clip
	float m_fAdvanceTime;		// When to advance to the next frame
	float m_fClipTime;			// Playback time within clip
	TImage* m_pImages;			// Array of TImage's, representing the loaded image textures 
	std::string m_sDirName;		// Name of the directory
	bool m_bUpdate;				// Whether the Image list should Draw itself or not.
	uint m_uColor;				// Color
	bool m_bGif;				// Whether this is a Gif
	std::vector<std::string> m_vsImageNames;	// Filenames of the images, if a directory or *.nil is used.
	// ebp-> (actually it's constantly updating, this disables draw so should this be m_bDraw?  whatevs)

private:
	bool InitGif();
	bool InitNil();
	bool InitDir();
};

#endif//NEON_IMAGELIST_H
