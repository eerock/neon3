//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MESSAGE_H
#define NEON_MESSAGE_H

#include "Source.h"

class CMessage : public CSource {
public:
	CMessage();
	~CMessage() override;

	auto Init(std::string const& sFile) -> bool override;

	//virtual auto OnReset () -> void;
	//virtual auto OnCommand (TCommand* pComm) -> void;
	//virtual auto Run (float fTime) -> void;
	//virtual auto Draw (CDisplayDevice *pDD) -> void;

	auto SetTime(float fTime) -> void override;
	auto Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void override;

	auto GetVarCtrls(int iScope) const -> TCtrlVar* override;
	auto SetVar(int iScope, int iObj, int iVar, void const* pData) -> void override;
	auto GetVar(int iScope, int iObj, int iVar) -> void const* override;

protected:
	enum {
		MAX_MESSAGE = 256,
		ST_INACTIVE = 0,
		ST_RISING,
		ST_ACTIVE,
		ST_FALLING,
	};

	int m_iState;
	float m_fVelY;
	float m_fVelYDesired;
	float m_fPosY;
	float m_fPosYActive;
	float m_fPosYInactive;
	uint m_uColor;
	char m_pMessage[MAX_MESSAGE];
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	LPD3DXFONT m_pFont;
#endif//GFX_ENGINE_DIRECT3D
	uint m_uTexBackgroundID;				// es-en: TexBackground (bg texture)

private:
	auto SetState(int iState) -> void;
};

#endif//NEON_MESSAGE_H
