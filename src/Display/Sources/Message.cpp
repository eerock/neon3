//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "Message.h"

enum EMessageVar {
	VAR_TEXT,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::EDIT_BOX, VAR_TEXT, "Text", false, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMessage::CMessage()
	: m_iState(0)
	, m_fVelY(0.f)
	, m_fVelYDesired(0.f)
	, m_fPosY(0.f)
	, m_fPosYActive(0.f)
	, m_fPosYInactive(0.f)
	, m_uColor(0)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	, m_pFont(nullptr)
#endif//GFX_ENGINE_DIRECT3D
	, m_uTexBackgroundID(CMaterialManager::INVALID)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMessage::~CMessage() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMessage\n"));
#endif
	if (IsOk()) {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		SafeRelease(&m_pFont);
#endif//GFX_ENGINE_DIRECT3D
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->RemoveTexture(m_uTexBackgroundID);
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMessage::Init(std::string const& sFile) -> bool {
	if (!sFile.empty()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		m_uTexBackgroundID = pMatMgr->AddTexture(sFile);
		if (m_uTexBackgroundID != CMaterialManager::INVALID) {
			GLOG(("ERROR: Can't load texture %s.\n", sFile.c_str()));
			m_bOk = false;
			return m_bOk;
		}
	}

	SetState(ST_ACTIVE);

	memset(m_pMessage, 0, MAX_MESSAGE);

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	D3DXCreateFont(
		g_DisplayDevice.GetD3DDevice(),		// pDevice
		24,									// Height
		0,									// Width
		FW_BOLD,							// Weight
		0,									// MipLevels
		FALSE,								// Italic
		DEFAULT_CHARSET,					// CharSet
		OUT_DEFAULT_PRECIS,					// OutputPrecision
		DEFAULT_QUALITY,					// Quality
		DEFAULT_PITCH | FF_DONTCARE,		// PitchAndFamily
		TEXT("Times New Roman"),			// pFaceName
		&m_pFont							// ppFont
	);
#endif//GFX_ENGINE_DIRECT3D

	m_fPosYActive = g_DisplayDevice.ViewportHeight() - 100.f;
	m_fPosYInactive = g_DisplayDevice.ViewportHeight() +  10.f;
	m_fPosY = m_fPosYActive;
	m_fVelY = 0.f;
	m_fVelYDesired = 0.f;

	m_bOk = true;
	return m_bOk;
}

// OnCommand:
//	if strcmp(cmd, 'down')
//		setstate(down)
//	else if strcmp(cmd, 'up')
//		setstate(up)
//
// in xml:
//
//<commands>
//	<command>
//		<command>Down</command>
//		<name>Down</name>
//		<off>0</off>
//		<beat>0</beat>
//	</command>
//	<command>
//		<command>Up</command>
//		<name>Up</name>
//		<off>0</off>
//		<beat>0</beat>
//	</command>
//</commands>


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CMessage::SetTime(float fTime) -> void {
	CSource::SetTime(fTime);

	if (m_fVelY < m_fVelYDesired) {
		m_fVelY += 0.011f * m_fTimeFrame * 1000.f;
		if (m_fVelY > m_fVelYDesired) {
			m_fVelY = m_fVelYDesired;
		}
	} else if (m_fVelY > m_fVelYDesired) {
		m_fVelY -= 0.011f * m_fTimeFrame * 1000.f;
		if (m_fVelY < m_fVelYDesired) {
			m_fVelY = m_fVelYDesired;
		}
	}

	m_fPosY += m_fVelY;

	switch (m_iState) {
		case ST_RISING: {
			if (m_fPosY < m_fPosYActive) {
				m_fVelYDesired = 0.f;
				m_fVelY = 0.f;
				m_iState = ST_ACTIVE;
			}
			break;
		}
		case ST_FALLING: {
			if (m_fPosY > m_fPosYInactive) {
				m_fVelYDesired = 0.f;
				m_fVelY = 0.f;
				m_iState = ST_INACTIVE;
			}
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CMessage::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	pDD->SetRenderTarget(uTargetTexID);
	if (m_bFirstClear || m_bClear) {
		pDD->Clear(true, true, 0, 1.f);
		m_bFirstClear = false;
	}

	float fW = static_cast<float>(pDD->ViewportWidth());
	//float fH = static_cast<float>(pDD->ViewportHeight());	// unreferenced variable

	// Create a rectangle to indicate where on the screen it should be drawn
	RECT rct;
	rct.left = 30;
	rct.right = (LONG)fW - 80;
	rct.top = (LONG)m_fPosY;
	rct.bottom= rct.top + 80;
	RECT rctSombra = rct;
	rctSombra.left += 2;
	rctSombra.right += 2;
	rctSombra.top += 2;
	rctSombra.bottom += 2;

	pDD->ApplyBasicShader();
	
	if (m_uTexBackgroundID != CMaterialManager::INVALID) {
		auto pD3D = pDD->GetD3DDevice();
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		pDD->SetBilinearFiltering(0, false);
		auto pTex = pMatMgr->GetTexture(m_uTexBackgroundID);
		pMatMgr->SetTexture(m_uTexBackgroundID, 0);
		neon::DrawQuadUV(
			pDD,
			TVector2(rct.left - 10.f, rct.top - 5.f),
			TVector2(rct.right + 10.f, rct.bottom + 5.f),
			TVector2(),
			TVector2((float)(rct.right - rct.left + 1) / pTex->GetWidth(), (float)(rct.bottom - rct.top + 1) / pTex->GetHeight())
		);
	} else {
		neon::DrawQuadFlat(pDD, TVector2(rct.left - 10.f, rct.top - 5.f), TVector2(rct.right + 10.f, rct.bottom + 5.f), HARD_COLOR_ARGB(255, 255, 96, 64), 0.f);
	}

	m_pFont->DrawText(NULL, m_pMessage, -1, &rctSombra, DT_WORDBREAK, HARD_COLOR_ARGB(255, 0, 0, 0));
	m_pFont->DrawText(NULL, m_pMessage, -1, &rct, DT_WORDBREAK, HARD_COLOR_ARGB(255, 255, 255, 255));
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iTextureTgt);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	SetState
//---------------------------------------------------------------------------//
auto CMessage::SetState(int iEstado) -> void {
	switch (iEstado) {
		case ST_INACTIVE: {
			m_fPosY = m_fPosYInactive;
			m_iState = iEstado;
			break;
		}
		case ST_RISING: {
			if (m_iState == ST_INACTIVE) {
				m_fVelYDesired = -5.f;
				m_fVelY = 0.f;
				m_iState = iEstado;
			}
			break;
		}
		case ST_FALLING: {
			if (m_iState == ST_ACTIVE) {
				m_fVelYDesired = 5.f;
				m_fVelY = 0.f;
				m_iState = iEstado;
			}
			break;
		}
		case ST_ACTIVE: {
			m_fVelYDesired = 0.f;
			m_fVelY = 0.f;
			m_iState = iEstado;
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CMessage::GetVarCtrls(int iScope) const -> TCtrlVar* {
	UNREFERENCED_PARAMETER(iScope);
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CMessage::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_TEXT: {
			m_pMessage[0] = 0;
			if (pData) {
				Strncpy(m_pMessage, static_cast<char const*>(pData), MAX_MESSAGE);
			}
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CMessage::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_TEXT: {
			return m_pMessage;
		}
		default: {
			return nullptr;
		}
	}
}
