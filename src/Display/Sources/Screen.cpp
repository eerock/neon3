//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "Screen.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CScreen::CScreen()
	: CSource()
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CScreen::~CScreen() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CScreen\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CScreen::Init(std::string const& sFile) -> bool {
	UNREFERENCED_PARAMETER(sFile);
	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Draw
//
//	ebp-> here's what I think this does: takes a screenshot to the texture.
//	that's why it set's render target to -1, and it looks like StretchRect
//	applies the 'back buffer' to the destination texture's 'surface'.
//
//	ebp(2014)-> when you create one of these and attach it, it just grabs 
//	whatever is drawn in the layers underneath it.  What's cool is you can
//	apply effects to this 'Screen' source while keeping the underlying sources
//	free of effects.  That way you can create something with just effects and
//	apply it over all kinds of different sources and the effects will look 
//	similar.
//---------------------------------------------------------------------------//
auto CScreen::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	auto pDst = pMatMgr->GetTexture(uTargetTexID);

	pDD->SetRenderTarget(CMaterialManager::INVALID);
	pD3D->StretchRect(pDD->GetBackBufferSurface(), nullptr, pDst->GetSurfaceD3D(), nullptr, D3DTEXF_POINT);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iTextureTgt);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CScreen::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	UNREFERENCED_PARAMETER(iVar);
	UNREFERENCED_PARAMETER(pData);
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CScreen::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	UNREFERENCED_PARAMETER(iVar);
	return nullptr;
}
