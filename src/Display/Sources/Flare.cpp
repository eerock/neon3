//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "Flare.h"

enum EFlareVar {
	VAR_RAYS,
	VAR_WAVE_LENGTH,
	VAR_WAVE_INTENSITY,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::SLIDER, VAR_RAYS, "Rays", false, 0, {NULL} },
	{ TCtrlVar::CHECK_BOX, VAR_WAVE_LENGTH, "Wave Length", false, 0, {NULL} },
	{ TCtrlVar::CHECK_BOX, VAR_WAVE_INTENSITY, "Wave Intens", false, 0, {NULL} },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFlare::CFlare()
	: m_iRays(0)
	, m_uMaterialID(CMaterialManager::INVALID)
	, m_bLengthWave(false)
	, m_bIntensWave(false)
{
	for (int i = 0; i < MAX_RAYS; ++i) {
		m_fRandoms[i] = 0.f;
	}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFlare::~CFlare() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFlare\n"));
#endif
	if (IsOk()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->RemoveMaterial(m_uMaterialID);
		m_uMaterialID = CMaterialManager::INVALID;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFlare::Init(std::string const& sFile) -> bool {
	if (!sFile.empty()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		m_uMaterialID = pMatMgr->AddMaterial(sFile);
		if (m_uMaterialID == CMaterialManager::INVALID) {
			GLOG(("ERROR: Can't load material %s.\n", sFile));
			m_bOk = false;
			return m_bOk;
		}
	}

	const float invMax = 1.f / static_cast<float>(RAND_MAX);
	for (int i = 0; i < MAX_RAYS; ++i) {
		m_fRandoms[i] = static_cast<float>(rand()) * invMax;
	}

	m_bLengthWave = false;
	m_bIntensWave = false;

	m_iRays = 0;
	m_bOk = true;

	return IsOk();
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFlare::SetTime(float fTime) -> void {
	CSource::SetTime(fTime);
	//GLOG(("m_iRays = %d\n", m_iRays));
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CFlare::Draw(CDisplayDevice *pDD, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	pDD->SetRenderTarget(uTargetTexID);
	if (m_bClear) {
		pDD->Clear(true, true, 0, 1.f);
	}

	float fW = static_cast<float>(pDD->ViewportWidth());
	float fH = static_cast<float>(pDD->ViewportHeight());

	float w = 4.f / fW;
	float h = -4.f / fH;
	float dw = -2.f;
	float dh = 2.f;

	float fFar = 2.f;
	float fNear = 0.001f;
	float Q = fFar / (fFar - fNear);

	D3DXMATRIX identity;
	D3DXMATRIX m;

	m._11 = w;		m._12 = 0.f;	m._13 = 0.f;		m._14 = 0.f;
	m._21 = 0.f;	m._22 = h;		m._23 = 0.f;		m._24 = 0.f;
	m._31 = 0.f;	m._32 = 0.f;	m._33 = Q;			m._34 = 1.f;
	m._41 = dw;		m._42 = dh;		m._43 = -Q * fNear;	m._44 = 1.f;

	auto pD3D = pDD->GetD3DDevice();
	D3DXMatrixIdentity(&identity);
	pD3D->SetTransform(D3DTS_VIEW, &identity);
	pD3D->SetTransform(D3DTS_WORLD, &identity);
	pD3D->SetTransform(D3DTS_PROJECTION, &m); 

	auto pVertices = static_cast<TVertex_HCV_XYZ_F*>(pDD->LockVertexBuffer(HCV_XYZ_F, m_iRays * 3));

	pDD->ApplyBasicShader();
	// Strength
	float fStrength = 1.f;
	if (m_bLengthWave) {
		CEngine3D::GetRegisterData(CEngine3D::V_WAVE, &fStrength);
		fStrength = 0.3f + 1.f * fStrength;
	}
	// Intens
	float fIntens = 0.3f;
	if (m_bIntensWave) {
		CEngine3D::GetRegisterData(CEngine3D::V_WAVE, &fIntens);
		fIntens = + 1.f * fIntens;
		if (fIntens > 1.f) {
			fIntens = 1.f;
		}
	}

	uint uColor = HARD_COLOR_ARGB(255, (uchar)(fIntens * 0xAF), (uchar)(fIntens * 0xAF), (uchar)(fIntens * 0x8F));

	for (int i = 0; i < m_iRays; ++i) {
		float fRayLength = m_fRandoms[i] * (240.f + 200.f * cosf(3.f * m_fTime + i * i)) * fStrength;
		float fRayArc = m_fRandoms[i] * (0.2f + 0.01f * sinf(m_fTime + i));
		float fRayOffset = m_fRandoms[i] * (0.1f * m_fTime + 6.23f * cosf(13.f * i));
		float fAngle = cosf(fRayOffset);
		fRayOffset = fabsf(fmodf(fRayOffset, 3.1416f * 2.f));
		if (fRayOffset >= 3.1416f && fRayOffset < 2.0f * 3.1416f) {
			fRayLength *= fabsf(fAngle);
		}

		float fx = 320.f;	// hardcoded x, y positions?
		float fy = 240.f;	// magic fuckin numbers

		HCV_SET_XYZ(pVertices, fx, fy, 1.f);
		HCV_SET_UV0(pVertices, 0.f, 1.f);
		HCV_SET_COLOR(pVertices, uColor)
		pVertices++;

		HCV_SET_XYZ(pVertices, fx + fRayLength * cosf(fRayOffset), fy + fRayLength * sinf(fRayOffset), 1.f);
		HCV_SET_UV0(pVertices, 0.f, 0.f);
		HCV_SET_COLOR(pVertices, 0)
		pVertices++;

		HCV_SET_XYZ(pVertices, fx + fRayLength * cosf(fRayOffset + fRayArc), fy + fRayLength * sinf(fRayOffset + fRayArc), 1.f);
		HCV_SET_UV0(pVertices, 0.f, 0.f);
		HCV_SET_COLOR(pVertices, 0)
		pVertices++;
	}
	pDD->UnlockVertexBuffer(HCV_XYZ_F);


	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pMat = pMatMgr->GetMaterial(m_uMaterialID);
	if (pMat) {
		auto uPasses = pMat->BeginDraw(0);
		for (uint uPass = 0; uPass < uPasses; ++uPass) {
			pMat->BeginPass(uPass);
			pDD->DrawPrimitive(HCV_XYZ_F, HARD_PRIM_TRIANGLELIST, m_iRays);
			pMat->EndPass();
		}
		pMat->EndDraw();
	}
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iTextureTgt);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFlare::GetVarCtrls(int iScope) const -> TCtrlVar* {
	UNREFERENCED_PARAMETER(iScope);
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFlare::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_RAYS: {
			int rays = static_cast<int>(*(static_cast<float const*>(pData)) * MAX_RAYS);
			rays = Clamp(rays, 0, static_cast<int>(MAX_RAYS));	// is it necessary to clamp?
			m_iRays = rays;
			break;
		}
		case VAR_WAVE_LENGTH: {
			m_bLengthWave = *(static_cast<bool const*>(pData));
			break;
		}
		case VAR_WAVE_INTENSITY: {
			m_bIntensWave = *(static_cast<bool const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFlare::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_RAYS: {
			return &m_iRays;
		}
		case VAR_WAVE_LENGTH: {
			return &m_bLengthWave;
		}
		case VAR_WAVE_INTENSITY: {
			return &m_bIntensWave;
		}
		default: {
			return nullptr;
		}
	}
}
