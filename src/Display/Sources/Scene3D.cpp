//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "GEScene3D.h"
#include "Scene3D.h"


enum EScene3DVar {
	VAR_CAMERA,
	VAR_CLEAR,
	VAR_ALPHA,
	VAR_RED,
	VAR_GREEN,
	VAR_BLUE,
	VAR_MOTION_BLUR,
	VAR_MOTION_INTENSITY,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_CAMERA, "Camera", true, 0, { 0 } },
	{ TCtrlVar::CHECK_BOX, VAR_CLEAR, "Clear", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_ALPHA, "Alpha", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_RED, "Red", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_GREEN, "Green", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_BLUE, "Blue", true, 0, { 0 } },
	{ TCtrlVar::CHECK_BOX, VAR_MOTION_BLUR, "Motion Blur", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_MOTION_INTENSITY, "Motion Intens", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CScene3D::CScene3D()
	: m_pScene3D(nullptr)
	, m_TexturaBlur(0)
	, m_fAlphaBackground(0.f)
	, m_fR(0.f)
	, m_fG(0.f)
	, m_fB(0.f)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CScene3D::Init(std::string const& sFile) -> bool {
	m_bOk = true;
	m_pScene3D = NEW CGEScene3D;
	if (!m_pScene3D->Init(sFile)) {
		DELETE_PTR(m_pScene3D);
		GLOG(("ERROR: Can't load 3D Scene from file %s\n", sFile.c_str()));
		m_bOk = false;
	} else {
		m_fAlphaBackground = m_fR = m_fG = m_fB = 0.f;
		m_bClear = true;
		m_bMotionBlur = false;
		m_fMotionIntens = 0.f;
		m_fLength = m_pScene3D->GetLength();
		// Rellenar el ComboBox de las camaras
		s_Vars[0].iNumOptions = m_pScene3D->GetNumCameras();
		for (int i = 0; i < m_pScene3D->GetNumCameras(); i++) {
			Strncpy(s_Vars[0].aszOptions[i], m_pScene3D->GetCameraNames()[i].c_str(), 16);
		}

		if (m_pScene3D) {
			m_pScene3D->SetTime(0.f);
		}
	}

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CScene3D::~CScene3D() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CScene3D\n"));
#endif
	if (IsOk()) {
		DELETE_PTR(m_pScene3D);
	}
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CScene3D::SetTime(float fTime) -> void {
	CSource::SetTime(fTime);
	if (m_pScene3D) {
		m_pScene3D->SetTime(fTime);
	}
}



//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CScene3D::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
	float fSinTime = sinf(m_fTime);
	CEngine3D::SetRegisterData(CEngine3D::V_TIME, &m_fTime);
	CEngine3D::SetRegisterData(CEngine3D::V_SINTIME, &fSinTime);

	pDD->SetRenderTarget(uTargetTexID);
	if (m_bFirstClear || (m_bClear && !m_bMotionBlur)) {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
		pDD->Clear(true, true, HARD_COLOR_ARGB((uchar)(m_fAlphaBackground * 255.f), (uchar)(m_fR * 255.f), (uchar)(m_fG * 255.f), (uchar)(m_fB * 255.f)), 1.f);
#endif//GFX_ENGINE_DIRECT3D
		m_bFirstClear = false;
	} else {
		// Guardar el frame del motion blur
		if (m_bMotionBlur) {
			CEffects::DoMotionBlurPass1(pDD, uTargetTexID);
		}
		pDD->Clear(false, true, 0, 1.f);
	}

	if (m_pScene3D) {
		m_pScene3D->Draw(pDD);
	}

	// Aplicar el frame anterior con la intensidad que toca
	if (m_bMotionBlur) {
		CEffects::DoMotionBlurPass2(pDD, uTargetTexID, m_fMotionIntens * 0.4f + 0.55f);
	}
}


//---------------------------------------------------------------------------//
//	GetVarScopes
//---------------------------------------------------------------------------//
auto CScene3D::GetVarScopes(std::vector<std::string>& vScopes) const -> void {
	vScopes.clear();
	vScopes.push_back("Objects");
	vScopes.push_back("Lights");
	vScopes.push_back("Cameras");
	vScopes.push_back("Fog");
}


//---------------------------------------------------------------------------//
//	GetVarScopes
//---------------------------------------------------------------------------//
auto CScene3D::GetVarObjects(std::vector<std::string>& vObjects, int iScope) const -> void {
	m_pScene3D->GetVarObjects(vObjects, iScope);
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CScene3D::GetVarCtrls(int iScope) const -> TCtrlVar* {
	if (iScope == FXSCOPE_MAIN) {
		return s_Vars;
	}
	else {
		return m_pScene3D->GetVars(iScope);
	}
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CScene3D::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	if (iScope == FXSCOPE_MAIN) {
		switch (iVar) {
			case 0: {
				m_pScene3D->SetCamera(*(static_cast<int const*>(pData)));
				break;
			}
			case 1: {
				m_bClear = *(static_cast<bool const*>(pData));
				break;
			}
			case 2: {
				m_fAlphaBackground = *(static_cast<float const*>(pData));
				break;
			}
			case 3: {
				m_fR = *(static_cast<float const*>(pData));
				break;
			}
			case 4: {
				m_fG = *(static_cast<float const*>(pData));
				break;
			}
			case 5: {
				m_fB = *(static_cast<float const*>(pData));
				break;
			}
			case 6: {
				m_bMotionBlur = *(static_cast<bool const*>(pData));
				break;
			}
			case 7: {
				m_fMotionIntens = *(static_cast<float const*>(pData));
				break;
			}
			default: {
				break;
			}
		}
	}
	else {
		m_pScene3D->SetVar(iScope, iObj, iVar, pData);
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CScene3D::GetVar(int iScope, int iObj, int iVar) -> void const* {
	static int iCam;
	if (iScope == FXSCOPE_MAIN) {
		switch (iVar) {
			case 0: {
				iCam = m_pScene3D->GetActiveCamera();
				return &iCam;
			}
			case 1: {
				return &m_bClear;
			}
			case 2: {
				return &m_fAlphaBackground;
			}
			case 3: {
				return &m_fR;
			}
			case 4: {
				return &m_fG;
			}
			case 5: {
				return &m_fB;
			}
			case 6: {
				return &m_bMotionBlur;
			}
			case 7: {
				return &m_fMotionIntens;
			}
			default: {
				return nullptr;
			}
		}
	}
	else {
		return m_pScene3D->GetVar(iScope, iObj, iVar);
	}
}
