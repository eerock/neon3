//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "Image.h"

enum EImageVar {
	VAR_FILL_MODE,
	//VAR_SCALE,
	VAR_POSX,
	VAR_POSY,
};

enum EFillMode {
	STRETCH_FILL,
	STRETCH_NONE,
	STRETCH_TOW,
	STRETCH_TOH,
	NUM_FILL_MODES,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_FILL_MODE, "Fill Mode", false, NUM_FILL_MODES, { "Fill", "None", "To Width", "To Height" } },
	// ebp-> these would be handy...
	//{ TCtrlVar::SLIDER, VAR_SCALE, "Scale", false, 0, {0} },
	{ TCtrlVar::SLIDER, VAR_POSX, "PosX", false, 0, {0} },	// from 0 to width (what about allowing any position?  an edit box?)
	{ TCtrlVar::SLIDER, VAR_POSY, "PosY", false, 0, {0} },	// from 0 to height (same thing, move to edit box?)
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CImage::CImage()
	: m_tImage()
	, m_sName()
	//, m_fScale(1.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CImage::~CImage() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CImage\n"));
#endif
	if (IsOk()) {
		if (m_tImage.uTextureID != CMaterialManager::INVALID) {
			auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
			pMatMgr->RemoveTexture(m_tImage.uTextureID);
		}
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CImage::Init(std::string const& sFile) -> bool {
	m_sName = sFile;

	// Image
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	m_tImage.uTextureID = pMatMgr->AddTexture(sFile);

	if (m_tImage.uTextureID != CMaterialManager::INVALID) {
		m_tImage.iStretchMode = STRETCH_FILL;
		m_tImage.vPos = TVector2();
		m_bOk = true;
	}
	else {
		GLOG(("ERROR: Can't load texture %s\n", sFile.c_str()));
	}

	return m_bOk;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CImage::SetTime(float fTime) -> void {
	CSource::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CImage::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
	ASSERT(m_tImage.uTextureID != CMaterialManager::INVALID);
	pDD->SetRenderTarget(uTargetTexID);
	pDD->Clear(true, false, 0, 1.f);

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pTex = pMatMgr->GetTexture(m_tImage.uTextureID);

	TVector2 vPos = m_tImage.vPos;
	TVector2 vSize;

	// Tamaño
	// es-en: Size
	float fW = static_cast<float>(pDD->ViewportWidth());
	float fH = static_cast<float>(pDD->ViewportHeight());
	float fTexW = static_cast<float>(pTex->GetWidth());
	float fTexH = static_cast<float>(pTex->GetHeight());

	switch (m_tImage.iStretchMode) {
		case STRETCH_FILL: {
			vSize.x = fW;
			vSize.y = fH;
			break;
		}
		case STRETCH_NONE: {
			vSize.x = fTexW;
			vSize.y = fTexH;
			break;
		}
		case STRETCH_TOW: {
			vSize.x = fW;
			vSize.y = fW * fTexH / fTexW;
			break;
		}
		case STRETCH_TOH: {
			vSize.x = fH * fTexW / fTexH;
			vSize.y = fH;
			break;
		}
		default: {
			break;
		}
	}

	// ebp-> center the position if the dimensions are smaller than the screen.
	if (vSize.x < fW) {
		vPos.x = (fW - vSize.x) * 0.5f;
	}
	if (vSize.y < fH) {
		vPos.y = (fH - vSize.y) * 0.5f;
	}

	//vPos.x += (fW + vSize.x) * (m_tImage.vPos.x - 0.5f);
	//vPos.y += (fH + vSize.y) * (m_tImage.vPos.y - 0.5f);

	// Textured Quad
	pMatMgr->SetTexture(m_tImage.uTextureID, 0);

	// RenderStates specifics
	pDD->ApplyBasicShader();
	pDD->SetBilinearFiltering(0, true);

	neon::DrawQuad(pDD, TVector2(vPos.x, vPos.y), TVector2(vPos.x + vSize.x, vPos.y + vSize.y));
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CImage::GetVarCtrls(int iScope) const -> TCtrlVar* {
	UNREFERENCED_PARAMETER(iScope);
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CImage::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	if (!pData) {
		return;
	}

	switch (iVar) {
		case VAR_FILL_MODE: {
			m_tImage.iStretchMode = *(static_cast<int const*>(pData));
			break;
		}
		case VAR_POSX: {
			m_tImage.vPos.x = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_POSY: {
			m_tImage.vPos.y = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CImage::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);

	switch (iVar) {
		case VAR_FILL_MODE: {
			return &m_tImage.iStretchMode;
		}
		case VAR_POSX: {
			return &static_cast<float>(m_tImage.vPos.x);
		}
		case VAR_POSY: {
			return &static_cast<float>(m_tImage.vPos.y);
		}
		default: {
			return nullptr;
		}
	}
}
