//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FFMPEGDECODER_H
#define NEON_FFMPEGDECODER_H

#include "Base.h"
#if (PLATFORM == PLATFORM_WINDOWS)
// ebp-> control.h is a WinSDK thing
#include "Control.h"
#endif

#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(push)
#pragma warning(disable:4244)
#ifdef __cplusplus
extern "C" {
#define __STDC_CONSTANT_MACROS
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
}
#endif//__cplusplus
#pragma warning(pop)
#endif

class CTexture;

class CFFMpegDecoder {
public:
	CFFMpegDecoder();
	~CFFMpegDecoder();

	auto Init(char const* pszFile) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto Run(float fTime) -> void;

	auto SetTexture(CTexture* pTextura) -> void;

	auto Length() const -> float { return m_fLength; }
	auto TimeToFrame(float fTime) -> int;
	auto GetFrame(CTexture* pTextura) -> bool;
	auto GetWidth() const -> int { return m_iWidth; }
	auto GetHeight() const -> int { return m_iHeight; }

public:
	static auto InitSubsystem() -> void;
	static auto EndSubsystem() -> void;

private:
	auto GetNextFrame() -> bool;
	auto GetNextFrame2() -> bool;

private:
	bool m_bOk;
	float m_fLength;
	float m_fStart;
	float m_fEnd;
	int m_iWidth;
	int m_iHeight;
	long m_iFrame;
	int m_iCurrentFrame;
	int m_iCachedFrame;
	int m_iBytesRemaining;		// unused
	int m_iVideoStreamIndex;

#if (PLATFORM == PLATFORM_WINDOWS)
	AVFormatContext* m_pFormatCtx;
	AVCodecContext* m_pCodecCtx;
	AVCodec* m_pCodec;
	AVFrame* m_pFrame;
	AVFrame* m_pFrameRGB;
	AVPacket m_tPacket;
	AVDictionary* m_pOptionsDict;
	SwsContext* m_pSwsCtx;
#endif

	uint8_t* m_pRawData;
};

#endif//NEON_FFMPEGDECODER_H
