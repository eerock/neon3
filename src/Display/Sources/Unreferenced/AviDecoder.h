//---------------------------------------------------------------------------//
//
// Neon v2.5.2a
//
// Copyright © 2006,2008 Jordi Ros <shine.3p@gmail.com>
// www.neonv2.com / www.xplsv.com
//
// Copyright © 2012 Eric Phister <eerock@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef __AVIDECODER_H__
#define __AVIDECODER_H__

#define USE_AVI_VIDEO

#if defined(USE_AVI_VIDEO)

#if (PLATFORM == PLATFORM_WINDOWS)
#undef CDECL
#define CDECL __cdecl
#pragma warning(push)
#pragma warning(disable:4201)
#include <vfw.h>
#pragma warning(pop)
#undef CDECL
#define CDECL
#endif

class CTextura;

class CAviDecoder {
public:
				CAviDecoder	();
	~			CAviDecoder	() { End(); }

	bool		Init		(char const* pcFile, uint uBpp);
	void		End			();

	float		Length		();
	int			TimeToFrame	(float fTime);
	bool		GetFrame	(int iFrame, CTextura* pTextura);
	int			GetWidth	() const { return m_iWidth; }
	int			GetHeight	() const { return m_iHeight; }

public:
	static void	InitSubsystem();
	static void	EndSubsystem();

private:
	int					m_iLength;
	int					m_iWidth;
	int					m_iHeight;
	char*				m_pB;
	char*				m_pI;
	uint				m_uBpp;
#if (PLATFORM == PLATFORM_WINDOWS)
	PAVISTREAM			m_pAvi;
	PGETFRAME			m_pFrame;
	HGLOBAL				m_pHIn;
	HGLOBAL				m_pHOut;
	BITMAPINFOHEADER*	m_pBInfoIn;
	BITMAPINFOHEADER*	m_pBInfoOut;
	HIC					m_hIC;
#endif
	uint				m_uLengthBInfoIn;
};

#endif//USE_AVI_VIDEO

#endif//__AVIDECODER_H__
