//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef ITEM

#include "FFMpegDecoder.h"
#if defined(USE_MPEG_VIDEO)
#include "MpgVideo.h"
#endif

#include "Color.h"
#include "Flare.h"
#include "Image.h"
#include "ImageList.h"
#include "Message.h"
#include "Scene3D.h"
#include "Screen.h"

/*
// ebp-> these look like todo's, for playing through VMR (Video Mixing Renderer), flash video, capturing from cameras, etc.
#include "AviDecoder.h"
#if defined(USE_AVI_VIDEO)
#include "AviVideo.h"
#endif//USE_AVI_VIDEO
#include "Video.h"
#include "FlashPlayer.h"
#include "Capture.h"
*/

#else

#if defined(USE_MPEG_VIDEO)
ITEM("MpgVideo", MPG, CMpgVideo)
#endif//USE_MPEG_VIDEO

ITEM("Color", COLOR, CColor)
ITEM("3DScene", 3D, CScene3D)
ITEM("Flare", FLARE, CFlare)
ITEM("Image", IMAGE, CImage)
ITEM("ImageList", IMAGE, CImageList)
ITEM("Message", MESSAGE, CMessage)
ITEM("Screen", SCREEN, CScreen)

/*
// ebp-> these look like todo's, for playing VMR Video, flash video, capturing from cameras, etc.
#if defined(USE_AVI_VIDEO)
ITEM("AviVideo", AVI16, CAviVideo(16))
ITEM("AviVideo24", AVI24, CAviVideo(24))	// ebp-> i added this
ITEM("AviVideo32", AVI32, CAviVideo(32))
#endif//USE_AVI_VIDEO
ITEM("Video", VIDEO, CVideo)
ITEM("Flash", FLASH, CFlash)
ITEM("Capture", CAPTURE, CCapture)
*/

#endif
