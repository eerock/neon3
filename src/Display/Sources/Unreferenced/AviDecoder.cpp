//---------------------------------------------------------------------------//
//
// Neon v2.5.2a
//
// Copyright © 2006,2008 Jordi Ros <shine.3p@gmail.com>
// www.neonv2.com / www.xplsv.com
//
// Copyright © 2012 Eric Phister <eerock@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "AviDecoder.h"

#if defined(USE_AVI_VIDEO)

//---------------------------------------------------------------------------//
// InitSubsytem
//
//---------------------------------------------------------------------------//
void CAviDecoder::InitSubsystem() {
#if (PLATFORM == PLATFORM_WINDOWS)
	AVIFileInit();
#endif
}


//---------------------------------------------------------------------------//
// ShutdownSubsystem
//
//---------------------------------------------------------------------------//
void CAviDecoder::EndSubsystem() {
#if (PLATFORM == PLATFORM_WINDOWS)
	AVIFileExit();
#endif
}


//---------------------------------------------------------------------------//
// Constructor
//
//---------------------------------------------------------------------------//
CAviDecoder::CAviDecoder()
	: m_iLength(0)
	, m_iWidth(0)
	, m_iHeight(0)
	, m_pB(NULL)
	, m_pI(NULL)
	, m_uBpp(0)
#if (PLATFORM == PLATFORM_WINDOWS)
	, m_pAvi(NULL)
	, m_pFrame(NULL)
	, m_pHIn(NULL)
	, m_pHOut(NULL)
	, m_pBInfoIn(NULL)
	, m_pBInfoOut(NULL)
	, m_hIC(0)
#endif
	, m_uLengthBInfoIn(0)
{}


//---------------------------------------------------------------------------//
// Init
//
//---------------------------------------------------------------------------//
bool CAviDecoder::Init(char const* pszFilename, uint uBpp) {
#if (PLATFORM == PLATFORM_WINDOWS)
	m_pBInfoIn  = NULL;
	m_pBInfoOut = NULL;

	if (AVIStreamOpenFromFile(&m_pAvi, pszFilename, streamtypeVIDEO, 0, OF_READ, NULL) != 0) {
		GLOG(("ERR: AVIStreamOpenFromFile failed for %s\n", pszFilename));
		return false;
	}
	AVISTREAMINFO streamInfo;
	AVIStreamInfo(m_pAvi, &streamInfo, sizeof(streamInfo));  

	m_uBpp = uBpp;
	if (uBpp > 0) {
		AVISTREAMINFO psi;
		AVIStreamInfo(m_pAvi, &psi, sizeof(psi));			// Reads Information About The Stream Into psi
		m_iWidth = psi.rcFrame.right - psi.rcFrame.left;	// Width Is Right Side Of Frame Minus Left
		m_iHeight = psi.rcFrame.bottom - psi.rcFrame.top;	// Height Is Bottom Of Frame Minus Top

		BITMAPINFOHEADER bmih;
		memset(&bmih, 0, sizeof(BITMAPINFOHEADER));
		bmih.biSize = sizeof (BITMAPINFOHEADER);			// Size Of The BitmapInfoHeader
		bmih.biPlanes = 1;									// Bitplanes	
		bmih.biBitCount = static_cast<WORD>(m_uBpp);		// Bits Format We Want (24 Bit, 3 Bytes)
		bmih.biWidth = m_iWidth;							// Width We Want (rX Pixels)
		bmih.biHeight = m_iHeight;							// Height We Want (rX Pixels)
		bmih.biCompression = BI_RGB;						// Requested Mode = RGB

		if ((m_pFrame = AVIStreamGetFrameOpen(m_pAvi, &bmih)) == NULL) {
			GLOG(("ERR: AVIStreamGetFrameOpen failed to load in %dbpp for %s\n", uBpp, pszFilename));
			if ((m_pFrame = AVIStreamGetFrameOpen(m_pAvi, NULL)) == NULL) {
				GLOG(("ERR: AVIStreamLength failed for %s\n", pszFilename));
				End();
				return false;
			}
		}
	} else {
		if ((m_pFrame = AVIStreamGetFrameOpen(m_pAvi, NULL)) == NULL) {
			GLOG(("ERR: AVIStreamLength failed for %s\n", pszFilename));
			End();
			return false;
		}
	}

	LPBITMAPINFOHEADER pBitmap = (LPBITMAPINFOHEADER)AVIStreamGetFrame(m_pFrame, 0);
	m_iWidth = pBitmap->biWidth;
	m_iHeight = pBitmap->biHeight;

	m_iLength = AVIStreamLength(m_pAvi);
	if (m_iLength == -1) {
		GLOG(("ERR: AVIStreamLength failed for %s\n", pszFilename));
		End();
		return false;
	}

	return true;
    
#else
    return false;
#endif
}


//---------------------------------------------------------------------------//
// End
//
//---------------------------------------------------------------------------//
void CAviDecoder::End() {
#if (PLATFORM == PLATFORM_WINDOWS)
	if (m_pFrame) {
		AVIStreamGetFrameClose(m_pFrame);
		m_pFrame = NULL;
	}

	if (m_pAvi) {
		AVIStreamRelease(m_pAvi);
		m_pAvi = NULL;
	}
#endif
}


//---------------------------------------------------------------------------//
// TimeToFrame (devuelve el frame que hay que poner en el tiempo dado)
//
//---------------------------------------------------------------------------//
int CAviDecoder::TimeToFrame(float fTime) {
#if (PLATFORM == PLATFORM_WINDOWS)
	return AVIStreamTimeToSample(m_pAvi, int(fTime * 1000.0f));
#else
    return 0;
#endif
}


//---------------------------------------------------------------------------//
// Length
//
//---------------------------------------------------------------------------//
float CAviDecoder::Length() {
#if (PLATFORM == PLATFORM_WINDOWS)
	int iFrames = AVIStreamLength(m_pAvi);
	int iTime = AVIStreamSampleToTime(m_pAvi, iFrames);
	return (float(iTime) * 0.001f);
#else
    return 0.f;
#endif
}


//---------------------------------------------------------------------------//
// GetFrame
//
//---------------------------------------------------------------------------//
bool CAviDecoder::GetFrame(int iFrame, CTextura* pTextura) {
	bool bRes = false;
#if (PLATFORM == PLATFORM_WINDOWS)
	BITMAPINFOHEADER* pBitmap = (LPBITMAPINFOHEADER)AVIStreamGetFrame(m_pFrame, iFrame);
	if (pBitmap) {
		char* pData = (char*)pBitmap + pBitmap->biSize + pBitmap->biClrUsed * 2;	
		bRes = true;
		TSurfaceDesc Desc;
		TRect Rect(0, 0, m_iWidth, m_iHeight);
		if (pTextura->Lock(&Rect, Desc)) {
			ASSERT(pBitmap->biBitCount == m_uBpp);
			// ebp-> allow for handling of different bpp's...
			//memcpy(Desc.pBits, pData, m_iWidth * m_iHeight * (m_Bpp == 32 ? 4 : 2));
			memcpy(Desc.pBits, pData, m_iWidth * m_iHeight * (m_uBpp >> 3));
			pTextura->Unlock();
		}
	}
#endif
	return bRes;
}

#endif//USE_AVI_VIDEO
