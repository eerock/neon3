//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "Color.h"

enum EColorVar {
	VAR_RED,
	VAR_GREEN,
	VAR_BLUE,
	VAR_ALPHA,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::SLIDER, VAR_RED, "Red", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_GREEN, "Green", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_BLUE, "Blue", false, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_ALPHA, "Alpha", false, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CColor::CColor()
	: m_fRed(0.f)
	, m_fGreen(0.f)
	, m_fBlue(0.f)
	, m_fAlpha(0.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CColor::~CColor() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CColor\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CColor::Init(std::string const& sFile) -> bool {
	UNREFERENCED_PARAMETER(sFile);
	m_bOk = true;
	m_fRed = 1.f;
	m_fGreen = 1.f;
	m_fBlue = 1.f;
	m_fAlpha = 1.f;
	return IsOk();
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CColor::Draw(CDisplayDevice* pDD, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	uint uColor = HARD_COLOR_ARGB(
		static_cast<uint>(m_fAlpha * 255.f),
		static_cast<uint>(m_fRed * 255.f),
		static_cast<uint>(m_fGreen * 255.f),
		static_cast<uint>(m_fBlue * 255.f)
	);
	pDD->SetRenderTarget(uTargetTexID);
	pDD->Clear(true, false, uColor, 0.f);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uTargetTexID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CColor::GetVarCtrls(int iScope) const -> TCtrlVar* {
	UNREFERENCED_PARAMETER(iScope);
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CColor::SetVar(int iScope, int iObj, int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_RED: {
			m_fRed = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_GREEN: {
			m_fGreen = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_BLUE: {
			m_fBlue = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_ALPHA: {
			m_fAlpha = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CColor::GetVar(int iScope, int iObj, int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iScope);
	UNREFERENCED_PARAMETER(iObj);
	switch (iVar) {
		case VAR_RED: {
			return &m_fRed;
		}
		case VAR_GREEN: {
			return &m_fGreen;
		}
		case VAR_BLUE: {
			return &m_fBlue;
		}
		case VAR_ALPHA: {
			return &m_fAlpha;
		}
		default: {
			return nullptr;
		}
	}
}
