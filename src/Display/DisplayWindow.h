//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_DISPLAYWINDOW_H
#define NEON_DISPLAYWINDOW_H

#include "Base.h"
#include "GEDisplayDevice.h"

#define DISPLAY_WINDOW_WIDTH	(1280)
#define DISPLAY_WINDOW_HEIGHT	(720)


//---------------------------------------------------------------------------//
//	Class DisplayWindow
//---------------------------------------------------------------------------//
class CDisplayWindow {
public:
	CDisplayWindow();
	~CDisplayWindow();

	auto Init(TGraphicsMode const& rtMode, TRect const& rtWindow, bool bCanMove, THInst hInst, THWnd hWndMain, bool bShowMouse) -> bool;
	auto IsOk() const -> bool { return m_bOk; }

	auto GetHandle() const -> THWnd { return m_hWnd; }
	auto GetActive() const -> bool { return m_bIsActive; }
	auto SetActive(bool bActive) -> void { m_bIsActive = bActive; }
	auto AdjustWindow() -> void;
	auto ResetWindow() -> void;

	auto InputKey(int iKey) -> void;
	auto ProcessMessages() -> void;

	auto LButtonDown(int iX, int iY) -> void;
	auto LButtonUp(int iX, int iY) -> void;
	auto MouseMove(int iX, int iY) -> void;
	auto SwitchFullScreen(bool bFullScreen) -> void;
	auto UpdateFullScreen() -> void;
	auto IsFullScreen() const -> bool { return m_tModo.bFullScreen; }
	auto ShowMouse() const -> bool { return m_bShowMouse; }
	auto SetTitle(std::string const& sTitle) -> void;

	auto GetWindowRect() const -> TRect const& { return m_tWindow; }

private:
	auto InitWindow(THInst hInst, bool bFullscreen) -> bool;

#if (PLATFORM == PLATFORM_WINDOWS)
	static auto CALLBACK s_WndProc(THWnd hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) -> LRESULT;
	auto WndProc(UINT Msg, WPARAM wParam, LPARAM lParam) -> LRESULT;
#endif//PLATFORM

	auto BorraVars() -> void;
	auto LiberaVars() -> void;

	bool m_bOk;
	bool m_bIsActive;

	THInst m_hInst;
	THWnd m_hWnd;
	THWnd m_hWndMain;
	THdc m_hDC;

	TGraphicsMode m_tModo;
	TRect m_tWindow;
	bool m_bShowMouse;
	bool m_bUpdateDevice;
	bool m_bCanMove;
	bool m_bMovingWindow;
	int m_iOldWindowPosX;
	int m_iOldWindowPosY;
	int m_iMousePosX;
	int m_iMousePosY;
};

#endif//NEON_DISPLAYWINDOW_H
