//---------------------------------------------------------------------------//
// File: Demo.h
//
//---------------------------------------------------------------------------//

#ifndef DEMO_H
#define DEMO_H

#include "Lista.h"

class CEffect;
class CDisplayDevice;
struct TCommand;

class CDemo
{
public:
					CDemo			() { m_bOk = false; }
	~				CDemo			() { End(); }

	bool			Load			(char const* pszFile);
	void			End				();
	bool			IsOk			() const { return m_bOk; }

	void			Reset			();
	void			SetTime			(float fTime);
	void			Run				(float fRunTime);
	void			Draw			(CDisplayDevice* pDD);

	CEffect*		GetEffectByName	(char const* pszNombre);

private:

	void			BorraVars		();
	void			LiberaVars		();

private:

	bool			m_bOk;
	float			m_fTime;
	CList<CEffect*>	m_ListaEfectos;
	CList<TCommand*> m_ListaComandos;
};

#endif
