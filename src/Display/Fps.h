//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FPS_H
#define NEON_FPS_H


//---------------------------------------------------------------------------//
//	FPS Timer
//---------------------------------------------------------------------------//
class CFPSTimer {
public:
	auto Init () -> void;
	auto Frame () -> void;
	auto GetFPS () -> float { return m_fFPS; }

protected:
	float m_fFPS;
	uint m_uFrames;
	uint m_uCurrentFPS;
	float m_fTime;
#if (PLATFORM == PLATFORM_WINDOWS)
	LARGE_INTEGER m_TimerFrecuency;
#endif//PLATFORM
};


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
inline auto CFPSTimer::Init() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	QueryPerformanceFrequency(&m_TimerFrecuency);

	m_fFPS = 0;
	m_uFrames = 0;

	LARGE_INTEGER pc;
	QueryPerformanceCounter(&pc);
	m_fTime = (float)pc.QuadPart / (float)m_TimerFrecuency.QuadPart;
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	Frame
//---------------------------------------------------------------------------//
inline auto CFPSTimer::Frame() -> void {
	++m_uFrames;
#if (PLATFORM == PLATFORM_WINDOWS)
	LARGE_INTEGER pc;
	QueryPerformanceCounter(&pc);
	float fTime = (float)pc.QuadPart * 1000.f / (float)m_TimerFrecuency.QuadPart;
	if ((fTime - m_fTime) >= 1000.f) {
		m_fFPS = m_uFrames * 1000.f / (fTime - m_fTime);
		m_uFrames = 0;
		m_fTime = fTime;
	}
#endif//PLATFORM
}

#endif//NEON_FPS_H
