//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXDll.h"

#define EFFECT_FORMAT_VERSION 3

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)

//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXDll::GetVarCtrls() const -> TCtrlVar* {
	return m_pFnGetVarCtrls(m_uID);
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXDll::SetVar(int iVar, void const* pData) -> void {
	m_pFnSetVar(m_uID, iVar, pData);
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXDll::GetVar(int iVar) -> void const* {
	return m_pFnGetVar(m_uID, iVar);
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXDll::CFXDll()
	: m_hLibrary(0)
	, m_uID(0)
	, m_pFnInit(nullptr)
	, m_pFnEnd(nullptr)
	, m_pFnSetTime(nullptr)
	, m_pFnApply(nullptr)
	, m_pFnGetVarCtrls(nullptr)
	, m_pFnSetVar(nullptr)
	, m_pFnGetVar(nullptr)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXDll::Init(CNodeFile::CNode* pNode) -> bool {
	m_bOk = false;
	if (!pNode) {
		return false;
	}

	// ebp-> todo: test this code, and we might have to do pNode = pNode->FirstNode("library")
	std::string const& sFile = pNode->AsString("library");
	if (sFile.empty()) {
		GLOG(("ERROR: No library (dll) specified.\n"));
		return false;
	}

	m_hLibrary = LoadLibrary(sFile.c_str());
	if (!m_hLibrary) {
		GLOG(("ERROR: Unable to load library %s.\n", sFile.c_str()));
		return false;
	}

	if (!LoadFunctions()) {
		GLOG(("ERROR: FXDll version from file %s is not compatible with current version (%d).\n", sFile.c_str(), EFFECT_FORMAT_VERSION));
		FreeLibrary(m_hLibrary);
		return false;
	}

	// Effect init
	// ebp-> how does the CNodeFile::CNode pointer get translated to a vector<pair<string, string> > ?
	int iErr = m_pFnInit(EFFECT_FORMAT_VERSION, this, g_DisplayDevice.GetD3DDevice(), pNode, &m_uID);
	if (iErr) {
		if (iErr == -1) {
			GLOG(("ERROR: FXDll version from file %s is not compatible with this one (%d).\n", sFile.c_str(), EFFECT_FORMAT_VERSION));
		} else {
			GLOG(("ERROR: FXDll can't load library %s.  Return code = %d.\n", sFile.c_str(), iErr));
		}
		FreeLibrary(m_hLibrary);
		return false;
	}

	return (m_bOk = true);
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXDll::~CFXDll() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXDll\n"));
#endif
	if (IsOk()) {
		m_pFnEnd(m_uID);
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXDll::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
	m_pFnSetTime(m_uID, fTime);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CFXDll::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	UNREFERENCED_PARAMETER(pDD);
	static TEffectTgt EffectTgt;
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	EffectTgt.pTexSrc = pMatMgr->GetTexture(uSourceTexID)->GetTexturaD3D();
	EffectTgt.pTexTgt = pMatMgr->GetTexture(uTargetTexID)->GetTexturaD3D();
	return m_pFnApply(m_uID, &EffectTgt);
}


//---------------------------------------------------------------------------//
//	Load Functions
//---------------------------------------------------------------------------//
auto CFXDll::LoadFunctions() -> bool {
#if (PLATFORM == PLATFORM_WINDOWS)
	if (nullptr == (m_pFnInit = (EffectDLL_Init)GetProcAddress(m_hLibrary, "Effect_Init"))) {
		return false;
	}
	if (nullptr == (m_pFnEnd = (EffectDLL_End)GetProcAddress(m_hLibrary, "Effect_End"))) {
		return false;
	}
	if (nullptr == (m_pFnSetTime = (EffectDLL_SetTime)GetProcAddress(m_hLibrary, "Effect_SetTime"))) {
		return false;
	}
	if (nullptr == (m_pFnApply = (EffectDLL_Apply)GetProcAddress(m_hLibrary, "Effect_Apply"))) {
		return false;
	}
	if (nullptr == (m_pFnGetVarCtrls = (EffectDLL_GetVarCtrls)GetProcAddress(m_hLibrary, "Effect_GetVarCtrls"))) {
		return false;
	}
	if (nullptr == (m_pFnSetVar = (EffectDLL_SetVar)GetProcAddress(m_hLibrary, "Effect_SetVar"))) {
		return false;
	}
	if (nullptr == (m_pFnGetVar = (EffectDLL_GetVar)GetProcAddress(m_hLibrary, "Effect_GetVar"))) {
		return false;
	}
	return true;
#else
	return false;
#endif
}

#endif//GFX_ENGINE_DIRECT3D
