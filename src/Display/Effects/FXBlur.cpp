//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXBlur.h"

enum EBlurVar {
	VAR_BLUR_TYPE,
	VAR_RADIO,
	VAR_ANIM,
	VAR_ANIM_HRANGE,
	VAR_ANIM_VRANGE,
	VAR_ANIM_SPEED,
	VAR_INTENSITY,
};

enum EBlurType {
	RADIAL,
	BOX,
	BOXPS,
	HORZ,
	VERT,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_BLUR_TYPE, "BlurType", true, 5, { "Radial", "Box", "Box (PS2.0)", "Horz", "Vert" } },
	{ TCtrlVar::SLIDER, VAR_RADIO, "Radio", true, 0, { 0 } },
	{ TCtrlVar::CHECK_BOX, VAR_ANIM, "Anim", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_ANIM_HRANGE, "AnimHRange", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_ANIM_VRANGE, "AnimVRange", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_ANIM_SPEED, "AnimSpeed", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_INTENSITY, "Intensity", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXBlur::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXBlur::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_BLUR_TYPE: {
			m_iType = *(static_cast<int const*>(pData));
			break;
		}
		case VAR_RADIO: {
			m_fRadio = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_ANIM: {
			m_bAnimacion = *(static_cast<bool const*>(pData));
			break;
		}
		case VAR_ANIM_HRANGE: {
			m_vRangoAnimacion.x = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_ANIM_VRANGE: {
			m_vRangoAnimacion.y = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_ANIM_SPEED: {
			m_fAnimSpeed = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_INTENSITY: {
			m_fIntensAct = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXBlur::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_BLUR_TYPE: {
			return &m_iType;
		}
		case VAR_RADIO: {
			return &m_fRadio;
		}
		case VAR_ANIM: {
			return &m_bAnimacion;
		}
		case VAR_ANIM_HRANGE: {
			return &m_vRangoAnimacion.x;
		}
		case VAR_ANIM_VRANGE: {
			return &m_vRangoAnimacion.y;
		}
		case VAR_ANIM_SPEED: {
			return &m_fAnimSpeed;
		}
		case VAR_INTENSITY: {
			return &m_fIntensAct;
		}
		default: {
			break;
		}
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXBlur::CFXBlur()
	: m_iType(0)
	, m_bAnimacion(false)
	, m_iState(0)
	, m_iSteps(0)
	, m_fRadio(0.f)
	, m_fIntens(0.f)
	, m_fIntensAct(0.f)
	, m_fAnimSpeed(0.f)
	, m_vRangoAnimacion()
	, m_vPosCentro()
	, m_vPos()
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXBlur::Init(CNodeFile::CNode* pNode) -> bool {
	UNREFERENCED_PARAMETER(pNode);
	m_bOk = true;
	m_iType = BOX;
	m_bAnimacion = true;
	m_iSteps = 12;
	m_vPosCentro.x = 0.5f;
	m_vPosCentro.y = 0.5f;
	m_fAnimSpeed = 1.f;
	m_vRangoAnimacion.x = 1.f;
	m_vRangoAnimacion.y = 1.f;
	m_fRadio = 0.5f;
	m_fIntens = 0.5f;
	m_fIntensAct = m_fIntens;

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXBlur::~CFXBlur() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXBlur\n"));
#endif
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXBlur::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
	if (m_bAnimacion) {
		float fV = m_fTime * m_fAnimSpeed;
		m_vPos.x = (m_vRangoAnimacion.x * (sinf(fV * 6.2f) * 0.28f + cosf(fV * 5.4f) * 0.20f)) + m_vPosCentro.x;
		m_vPos.y = (m_vRangoAnimacion.y * (cosf(fV * 4.7f) * 0.17f + sinf(fV * 6.7f) * 0.26f)) + m_vPosCentro.y;
	} else {
		m_vPos.x = m_vPosCentro.x;
		m_vPos.y = m_vPosCentro.y;
	}
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CFXBlur::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	UNREFERENCED_PARAMETER(uTargetTexID);
	switch (m_iType) {
		case BOX: {
			CEffects::DoBoxBlur(pDD, uSourceTexID, m_fIntensAct * 32.f);
			break;
		}
		case BOXPS: {
			CEffects::DoBlur(pDD, uSourceTexID, m_fIntensAct);
			break;
		}
		case RADIAL: {
			CEffects::DoRadialBlurSteps(pDD, uSourceTexID, m_fRadio * 80.f, m_fIntensAct, m_iSteps, m_vPos);
			break;
		}
		case HORZ: {
			CEffects::DoHorzBlur(pDD, uSourceTexID, m_fIntensAct);
			break;
		}
		case VERT: {
			CEffects::DoVertBlur(pDD, uSourceTexID, m_fIntensAct);
			break;
		}
		default: {
			return false;
		}
	}
	return true;
}
