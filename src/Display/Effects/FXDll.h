//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FXDLL_H
#define NEON_FXDLL_H

#include "Effect.h"

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
struct TEffectTgt {
	// ebp-> Direct3D 9
	IDirect3DTexture9* pTexSrc;
	IDirect3DTexture9* pTexTgt;
};

struct TCtrlVar;	// forward

//---------------------------------------------------------------------------//
// ebp-> Direct3D 9
typedef int (__cdecl* EffectDLL_Init) (int iVersion, void* pEffectInstance, IDirect3DDevice9* pD3DDevice, CNodeFile::CNode* pNode, uint* pID);
typedef int (__cdecl* EffectDLL_End) (uint uID);
typedef void (__cdecl* EffectDLL_SetTime) (uint uID, float fTime);
typedef bool (__cdecl* EffectDLL_Apply) (uint uID, TEffectTgt* pEffectTgt);
typedef TCtrlVar* (__cdecl* EffectDLL_GetVarCtrls) (uint uID);
typedef void (__cdecl* EffectDLL_SetVar) (uint uID, int iVar, void const* pData);
typedef void const* (__cdecl* EffectDLL_GetVar) (uint uID, int iVar);


//---------------------------------------------------------------------------//
//	Class CFXDll
//---------------------------------------------------------------------------//
class CFXDll : public CEffect {
public:
	CFXDll();
	~CFXDll() override;

	auto Init(CNodeFile::CNode* pNode) -> bool override;

	auto SetTime(float fTime) -> void override;
	auto Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool override;

	auto GetVarCtrls() const -> TCtrlVar* override;
	auto SetVar(int iVar, void const* pData) -> void override;
	auto GetVar(int iVar) -> void const* override;

private:
	auto LoadFunctions() -> bool;

	THInst m_hLibrary;
	uint m_uID;

	// Callback Functions
	EffectDLL_Init m_pFnInit;
	EffectDLL_End m_pFnEnd;
	EffectDLL_SetTime m_pFnSetTime;
	EffectDLL_Apply m_pFnApply;
	EffectDLL_GetVarCtrls m_pFnGetVarCtrls;
	EffectDLL_SetVar m_pFnSetVar;
	EffectDLL_GetVar m_pFnGetVar;
};
#endif//GFX_ENGINE_DIRECT3D

#endif//NEON_FXDLL_H
