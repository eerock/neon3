//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXSpriter.h"

enum ESpriterVar {
	VAR_MIN_SIZE,
	VAR_MAX_SIZE,
	VAR_RESOLUTION,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::SLIDER, VAR_MIN_SIZE, "Min Size", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_MAX_SIZE, "Max Size", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_RESOLUTION, "Resolution", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXSpriter::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXSpriter::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_MIN_SIZE: {
			m_fMinSize = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_MAX_SIZE: {
			m_fMaxSize = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_RESOLUTION: {
			m_fResolution = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXSpriter::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_MIN_SIZE: {
			return &m_fMinSize;
		}
		case VAR_MAX_SIZE: {
			return &m_fMaxSize;
		}
		case VAR_RESOLUTION: {
			return &m_fResolution;
		}
		default: {
			return nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXSpriter::CFXSpriter()
	: m_uSpriteTextureID(CMaterialManager::INVALID)
	, m_fResolution(0.f)
	, m_fMinSize(0.f)
	, m_fMaxSize(0.f)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXSpriter::Init(CNodeFile::CNode* pNode) -> bool {
	if (!pNode) {
		return false;
	}
	m_fMinSize = 0.1f;
	m_fMaxSize = 0.5f;
	m_fResolution = 0.5f;
	m_uSpriteTextureID = CMaterialManager::INVALID;

	// ebp-> todo: pNode = pNode->FirstNode("sprite"), loop through multiple sprite images,
	// set sprite through combo box.
	std::string const& sFile = pNode->AsString("sprite");
	if (!sFile.empty()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		m_uSpriteTextureID = pMatMgr->AddTexture(sFile);
		if (m_uSpriteTextureID != CMaterialManager::INVALID) {
			m_bOk = true;
		} else {
			GLOG(("ERROR: Can't load sprite %s for CFXSpriter.\n", sFile.c_str()));
		}
	} else {
		GLOG(("ERROR: Can't find sprite tag for CFXSpriter.\n"));
	}
	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXSpriter::~CFXSpriter() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXSpriter\n"));
#endif
	if (IsOk()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		pMatMgr->RemoveTexture(m_uSpriteTextureID);
	}
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXSpriter::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CFXSpriter::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	CEffects::DoSpriter(pDD, uSourceTexID, uTargetTexID, m_uSpriteTextureID, m_fResolution, m_fMinSize * 20.f + 1.f, m_fMaxSize * 75.f + m_fMinSize);
	return m_bOk;
}
