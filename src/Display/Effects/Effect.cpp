//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"

// Effect types...
#include "FXBasic.h"
#include "FXBlur.h"
#include "FXDist.h"
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
#include "FXDLL.h"
#endif//GFX_ENGINE_DIRECT3D
#include "FXGlow.h"
#include "FXPixelate.h"
#include "FXRGB.h"
#include "FXShader.h"
#include "FXSpriter.h"


//---------------------------------------------------------------------------//
// STATIC: Create
//
// Carga un filtro desde un fichero NFX (OptionsFile)
// es-en: Load a filter from a file (.NFX)
//---------------------------------------------------------------------------//
auto CEffect::Create(std::string& sFile) -> CEffect* {
	CEffect* pEffect = nullptr;
	CNodeFile NodeFile;
	if (NodeFile.LoadFromFile(sFile + ".nfx")) {
		auto pNodeEffect = NodeFile.FirstNode("effect");
		if (pNodeEffect) {
			// ebp-> remember, this parameter is no longer const, so we're writing over the real passed in string here.
			sFile = pNodeEffect->AsString("name", sFile);

			std::string const& sEffectClass = pNodeEffect->AttrAsString("class");
			if (!sEffectClass.empty()) {

				if (pEffect == nullptr && Stricmp("FXShader", sEffectClass) == 0) {
					pEffect = NEW CFXShader;
				}

				if (pEffect == nullptr && Stricmp("FXSpriter", sEffectClass) == 0) {
					pEffect = NEW CFXSpriter;
				}

#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
				if (pEffect == nullptr && Stricmp("FXDll", sEffectClass) == 0) {
					pEffect = NEW CFXDll;
				}
#endif//GFX_ENGINE_DIRECT3D

				if (pEffect == nullptr && Stricmp("FXBlur", sEffectClass) == 0) {
					pEffect = NEW CFXBlur;
				}

				if (pEffect == nullptr && Stricmp("FXDist", sEffectClass) == 0) {
					pEffect = NEW CFXDist;
				}

				if (pEffect == nullptr && Stricmp("FXGlow", sEffectClass) == 0) {
					pEffect = NEW CFXGlow;
				}

				if (pEffect == nullptr && Stricmp("FXPixelate", sEffectClass) == 0) {
					pEffect = NEW CFXPixelate;
				}

				if (pEffect == nullptr && Stricmp("FXRGB", sEffectClass) == 0) {
					pEffect = NEW CFXRGB;
				}

				if (pEffect == nullptr && Stricmp("FXBasic", sEffectClass) == 0) {
					pEffect = NEW CFXBasic;
				}

				if (pEffect == nullptr) {
					GLOG(("ERROR: Unknown Effect class '%s'\n", sEffectClass.c_str()));
				}
				// initialize the filter data from the xml specification... 
				else if (!pEffect->Init(pNodeEffect)) {
					DELETE_PTR(pEffect);
					GLOG(("ERROR: Can't initialize Effect '%s'\n", sEffectClass.c_str()));
				}

			} else {
				GLOG(("ERROR: Effect class type not specified in file '%s.nfx'.  Check file and try again.\n", sFile.c_str()));
			}
		}

	} else {
		GLOG(("ERROR: Failed to load Effect from file %s.nfx\n", sFile.c_str()));
	}

	return pEffect;
}


//---------------------------------------------------------------------------//
// Constructor
//
//---------------------------------------------------------------------------//
CEffect::CEffect()
	: m_bOk(false)
	, m_fTime(0.f)
	, m_fTimePrev(0.f)
	, m_fTimeFrame(0.f)
{}


//---------------------------------------------------------------------------//
// SetTime
//
//---------------------------------------------------------------------------//
auto CEffect::SetTime(float fTime) -> void {
	m_fTimePrev = m_fTime;
	m_fTime = fTime;
	m_fTimeFrame = m_fTime - m_fTimePrev;
}
