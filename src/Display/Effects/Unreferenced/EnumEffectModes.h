//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
//	File: EnumEffectModes.h
//
//	Aqui se define la lista de comandos y su String asociado. Es un poco
//	mas tedioso que pasar directamente el string, pero tambien mas rapido,
//	y tampoco se añaden tantos comandos como para que esto sea coñazo (o eso 
//	espero)
//	es-en: Here the list of commands and their associated Strings defined.
//	It is a bit more tedious than directly passing the string, but also faster,
//	nor many commands as to make this a pain in the ass (I hope) are added.
//---------------------------------------------------------------------------//

#ifdef ITEM

ITEM(FM_ALPHABLEND,      "AlphaBlend")
ITEM(FM_ADDITIVE,        "Additive")
ITEM(FM_SUBTRACTIVE,     "Subtractive")
ITEM(FM_INVERT,          "Invert")
ITEM(FM_INVERTDEST,      "InvertDest")
ITEM(FM_MASK,            "Mask")
ITEM(FM_MULTIPLY,        "Multiply")

#endif//ITEM

