//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef ITEM

#include "FXBasic.h"				// works!
#include "FXBlur.h"					// works!
#include "FXDist.h"					// works!
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
#include "FXDLL.h"					// works!
#endif//GFX_ENGINE_DIRECT3D
#include "FXGlow.h"					// works!
#include "FXPixelate.h"				// untested
#include "FXRGB.h"					// not working!
#include "FXShader.h"				// untested
#include "FXSpriter.h"				// works!


/*
// ebp-> todo: enable some of these, once the basic ones above are working...
#include "FXFreeframe.h"			// not working!
#include "FXScanner.h"
#include "FXHeat.h"
*/

#else

ITEM("FXBasic", CFXBasic)
ITEM("FXBlur", CFXBlur)
ITEM("FXDist", CFXDist)
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
ITEM("FXDll", CFXDll)
#endif//GFX_ENGINE_DIRECT3D
ITEM("FXGlow", CFXGlow)
ITEM("FXPixelate", CFXPixelate)
ITEM("FXRGB", CFXRGB)
ITEM("FXShader", CFXShader)
ITEM("FXSpriter", CFXSpriter)

/*
ITEM("FXFreeframe", CFXFreeframe)
ITEM("FXScanner", CFXScanner)
ITEM("FXHeat", CFXHeat)
*/

#endif

