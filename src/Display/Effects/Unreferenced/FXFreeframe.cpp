//---------------------------------------------------------------------------//
// Neon v2.5
// Copyright (C) 2006,2008 Jordi Ros <shine.3p@gmail.com>
// www.neonv2.com / www.xplsv.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXFreeframe.h"


//---------------------------------------------------------------------------//
// GetVarCtrls
//
//---------------------------------------------------------------------------//
TCtrlVar* CFXFreeframe::GetVarCtrls() const {
	return m_pVars;
}


//---------------------------------------------------------------------------//
// SetVar
//
//---------------------------------------------------------------------------//
void CFXFreeframe::SetVar(int iVar, void* pData) {
	SetParameterStruct tParam;
	tParam.ParameterNumber = iVar;
	memcpy(&tParam.NewParameterValue, pData, 4);
	m_pPlugMain(FF_SETPARAMETER, (uint)&tParam, m_uInstance);
}


//---------------------------------------------------------------------------//
// GetVar
//
//---------------------------------------------------------------------------//
void* CFXFreeframe::GetVar(int iVar) {
	plugMainUnion retval = m_pPlugMain(FF_GETPARAMETER, iVar, m_uInstance);
	return &retval.fvalue;
}


//---------------------------------------------------------------------------//
// Constructor
//
//---------------------------------------------------------------------------//
CFXFreeframe::CFXFreeframe()
	: CEffect()
	, m_uInstance(0)
	, m_nParams(0)
	, m_iWidth(0)
	, m_iHeight(0)
	, m_iTextura(0)
	, m_iTextura2(0)
	, m_hLibrary(0)
	, m_pPlugMain(NULL)
	, m_pVars(NULL)
{}


//---------------------------------------------------------------------------//
// Init
//
//---------------------------------------------------------------------------//
bool CFXFreeframe::Init(CNodeFile::CNode* pNode) {
	m_bOk = false;
	if (!pNode) {
		return false;
	}

	//GLOG(("Sizeof DWORD = %d\n", sizeof(DWORD)));
	//GLOG(("Sizeof void* = %d\n", sizeof(void*)));

	std::string const& sPlugin = pNode->AsString("plugin", "");
	if (sPlugin == "") {
		GLOG(("ERR: No plugin (dll) specified.\n"));
		return false;
	}

	m_hLibrary = LoadLibrary(sPlugin.c_str());
	if (!m_hLibrary) {
		GLOG(("ERR: Unable to load plugin %s.\n", sPlugin.c_str()));
		return false;
	}

	// now that the dll is loaded, we can further initialize the filter...
	m_iWidth = pNode->AsInt("width", 1024);
	m_iHeight = pNode->AsInt("height", 768);

	plugMainUnion hRetval = { 0 };

	// set and prepare to call the plugin's Main function...
	m_pPlugMain = (FncPlugMain)GetProcAddress(m_hLibrary, "plugMain");
	if (m_pPlugMain) {
		hRetval = m_pPlugMain(FF_INITIALISE, 0, 1);
		if (hRetval.ivalue == FF_SUCCESS) {
			IMaterialManagerService* pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
			ASSERT(pMatMgr);
			m_iTextura = pMatMgr->AddTextura(m_iWidth, m_iHeight, HARD_TEX_FORMAT_A8R8G8B8, false, true, false);
			m_iTextura2 = pMatMgr->AddTextura(m_iWidth, m_iHeight, HARD_TEX_FORMAT_A8R8G8B8, false, false, false, CTextura::SYSTEMMEM);

			VideoInfoStruct tVInfo;
			tVInfo.BitDepth = 2;
			tVInfo.FrameWidth = m_iWidth;
			tVInfo.FrameHeight = m_iHeight;
			tVInfo.Orientation = FF_ORIENTATION_TL;

			hRetval = m_pPlugMain(FF_INSTANTIATEGL, (uint)&tVInfo, 0);
			if (hRetval.ivalue != FF_FAIL) {
				m_uInstance = hRetval.ivalue;

				hRetval = m_pPlugMain(FF_GETNUMPARAMETERS, 0, m_uInstance);
				m_nParams = hRetval.ivalue;

				// Parameters...
				m_pVars = NEW_ARRAY(TCtrlVar, m_nParams + 1);	// +1 for a terminator
				ASSERT(m_pVars);
				// ebp-> zero the new'ed array...
				memset(m_pVars, 0, sizeof(TCtrlVar) * m_nParams + 1);

				for (uint i = 0; i < m_nParams; ++i) {
					m_pVars[i].iType = TCtrlVar::SLIDER;
					m_pVars[i].iNum = i;

					hRetval = m_pPlugMain(FF_GETPARAMETERNAME, i, m_uInstance);
					Strncpy(m_pVars[i].szName, hRetval.svalue, TCtrlVar::SIZE_NAME_STR);
					m_pVars[i].bLinkable = true;
					m_pVars[i].iNumOptions = 0;

					//hRetval = m_pPlugMain(FF_GETPARAMETERDEFAULT, i, m_uInstance);
					//SetVar(i, &hRetval.fvalue); // how do you know it's float?  can we check dest?
				}

				// Terminator...
				m_pVars[m_nParams].iType = TCtrlVar::INVALID;
				m_bOk = true;
			}
		}

		// find out what went wrong...
		if (!IsOk()) {
			if (hRetval.ivalue == FF_FAIL) {
				GLOG(("ERR: Freeframe plugin %s could not be initialised / instantiated.\n", sPlugin.c_str()));
			} else {
				GLOG(("ERR: Freeframe plugin %s could not be loaded, error code %d.\n", sPlugin.c_str(), hRetval.ivalue));
			}

			FreeLibrary(m_hLibrary);
		}
	} else {
		GLOG(("ERR: Can't find Freeframe function 'plugMain' in plugin %s.\n", sPlugin.c_str()));
	}

	return IsOk();
}


//---------------------------------------------------------------------------//
// End
//
//---------------------------------------------------------------------------//
void CFXFreeframe::End() {
	if (IsOk()) {
		FreeLibrary(m_hLibrary);

		IMaterialManagerService* pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		ASSERT(pMatMgr);
		pMatMgr->RemoveTextura(m_iTextura);
		pMatMgr->RemoveTextura(m_iTextura2);

		DELETE_ARRAY(m_pVars);

		// ebp-> is this necessary?
		CEffect::End();
	}
}


//---------------------------------------------------------------------------//
// SetTime
//
//---------------------------------------------------------------------------//
void CFXFreeframe::SetTime(float fTime) {
	CEffect::SetTime(fTime);
	m_pPlugMain(FF_SETTIME, (DWORD)&fTime, m_uInstance);
}


//---------------------------------------------------------------------------//
// Draw
//
// ebp-> todo: get this code working..  maybe the bug isn't here and it's
// elsewhere, say, in the DLL code.  will need to get the FFGLSDK_1_5 project
// up and running anyways since we want to be able to build the DLLs ourselves
// and debug them.  This function is kind of a mess, but it looks more or less
// like it's doing the right thing.
//---------------------------------------------------------------------------//
bool CFXFreeframe::Apply(CDisplayDevice* pDD, int iTexSrc, int iTexTgt) {
	D3DDEVICE* pD3D = pDD->GetD3DDevice();

	pDD->ApplyBasicShader();
	pDD->EndScene();

	// Copiar la textura
	// es-en: Copy the texture
	IMaterialManagerService* pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	ASSERT(pMatMgr);
	CTextura* pTexSrc = pMatMgr->GetTextura(iTexSrc);
	CTextura* pTexDst = pMatMgr->GetTextura(m_iTextura);
	CTextura* pTexDst2 = pMatMgr->GetTextura(m_iTextura2);
	CTextura* pTexTgt = pMatMgr->GetTextura(iTexTgt);

	RECT tRectSrc = { 0, 0, pTexSrc->GetWidth(), pTexSrc->GetHeight() };
	RECT tRectDst = { 0, 0, pTexDst->GetWidth(), pTexDst->GetHeight() };
	RECT tRectTgt = { 0, 0, pTexTgt->GetWidth(), pTexTgt->GetHeight() };

	// Stretch o copy directamente si el tamaño es el mismo
	// es-en: Stretch or copy directly if the size is the same
	if (m_iWidth != pTexSrc->GetWidth() || m_iHeight != pTexSrc->GetHeight()) {
		// Src -> Dst
		pD3D->StretchRect(pTexSrc->GetSurfaceD3D(), &tRectSrc, pTexDst->GetSurfaceD3D(), &tRectDst, D3DTEXF_POINT);
		// Dst -> Dst2
		pD3D->GetRenderTargetData(pTexDst->GetSurfaceD3D(), pTexDst2->GetSurfaceD3D());
	} else {
		// Src -> Dst2
		pD3D->GetRenderTargetData(pTexSrc->GetSurfaceD3D(), pTexDst2->GetSurfaceD3D());
	}

	// Llamar el process del filtro
	// es-en: Call the Freeframe filter process
	// ebp-> process Freeframe filter into Dst2
	TSurfaceDesc Desc;
	if (pTexDst2->Lock(NULL, Desc)) {
		// ebp-> if you're going to call it this way, better use Freeframe 1.0...
		m_pPlugMain(FF_PROCESSFRAME, (DWORD)Desc.pBits, m_uInstance);
		// ebp-> if you're going to use FreeframeGL (1.5), you need to adapt the code to this
		// calling convention, which involved changing the second parameter.  May have cascading changes elsewhere.
		//m_pPlugMain(FF_PROCESSOPENGL, (DWORD)Desc.pBits, m_uInstance);
		pTexDst2->Unlock();
	}

	// ebp-> this is the new code...
	// Stretch o copy directamente si el tamaño es el mismo
	// es-en: Stretch or copy directly if the size is the same
	//if (m_iWidth != pTexSrc->GetWidth() || m_iHeight != pTexSrc->GetHeight())
// ebp-> this might need to check the Tgt width/height
	if (m_iWidth != pTexTgt->GetWidth() || m_iHeight != pTexTgt->GetHeight()) {
		// Dst2 -> Dst
		pD3D->UpdateSurface(pTexDst2->GetSurfaceD3D(), NULL, pTexDst->GetSurfaceD3D(), NULL);
		// Dst -> Tgt
		pD3D->StretchRect(pTexDst->GetSurfaceD3D(), &tRectDst, pTexTgt->GetSurfaceD3D(), &tRectTgt, D3DTEXF_POINT);
	} else {
		// Dst2 -> Tgt
		POINT p = { 0, 0 };
		pD3D->UpdateSurface(pTexDst2->GetSurfaceD3D(), &tRectDst, pTexTgt->GetSurfaceD3D(), &p);
		//pD3D->UpdateSurface(pTexDst2->GetSurfaceD3D(), NULL, pTexTgt->GetSurfaceD3D(), NULL);
	}

	pDD->BeginScene();

	// ebp-> add new code here... (hmm)
	//pDD->SetRenderTarget(-1);
	pDD->SetRenderTarget(iTexTgt);
	//pDD->SetBilinearFiltering(0, false);
	//pDD->SetBilinearFiltering(1, false);
	//pDD->SetBilinearFiltering(2, false);
	//pDD->SetBilinearFiltering(3, false);
	//pMatMgr->SetTextura(iTexTgt, 0);
	//pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	//pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	neon::DrawQuad(pDD);

	return true;

	/*
	// ebp-> this is the old code, it'd be good to understand it...
	// Actualizar textura
	pD3D->UpdateSurface(pTexDst2->GetSurfaceD3D(), NULL, pTexDst->GetSurfaceD3D(), NULL);
	// Y pintar
	pDD->SetRenderTarget(-1);
	pDD->BeginScene();
	//ApplyMode(pDD, pRenderTgt, fAlphaGlobal);
	pMatMgr->SetTextura(m_iTextura, 0);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	DrawQuad(pDD);
	*/
}
