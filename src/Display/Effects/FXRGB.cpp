//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXRGB.h"
#include "GEEffects.h"


enum ERgbVar {
	VAR_INTENSITY_X,
	VAR_INTENSITY_Y,
	VAR_RANDOM,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::SLIDER, VAR_INTENSITY_X, "IntensX", true, 0, {0} },
	{ TCtrlVar::SLIDER, VAR_INTENSITY_Y, "IntensY", true, 0, {0} },
	{ TCtrlVar::SLIDER, VAR_RANDOM, "Random", true, 0, {0} },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXRGB::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXRGB::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_INTENSITY_X: {
			m_fIntensX = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_INTENSITY_Y: {
			m_fIntensY = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_RANDOM: {
			m_fRandom = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXRGB::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_INTENSITY_X: {
			return &m_fIntensX;
		}
		case VAR_INTENSITY_Y: {
			return &m_fIntensY;
		}
		case VAR_RANDOM: {
			return &m_fRandom;
		}
		default: {
			return nullptr;
		}
	}
}


// This effect doesn't work, find out why


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXRGB::CFXRGB()
	: m_fIntensX(0.f)
	, m_fIntensY(0.f)
	, m_fRandom(0.f)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXRGB::Init(CNodeFile::CNode* pNode) -> bool {
	UNREFERENCED_PARAMETER(pNode);

	m_fIntensX = 0.f;
	m_fIntensY = 0.f;
	m_fRandom = 0.f;

	m_bOk = true;
	return m_bOk;
}

//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXRGB::~CFXRGB() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXRGB\n"));
#endif
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXRGB::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CFXRGB::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
	// ebp-> todo: finish implementation of RGB filter...
	//	maybe compare it against the Rgb.fx shader!
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	auto uTexID = CEffects::GetTexture(CEffects::TEX_TARGET);
	pDD->SetRenderTarget(uTexID);
	pDD->Clear(true, true, 0, 1.f);

	pDD->ApplyBasicShader();
	pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	//pMatMgr->SetTexture(uTargetTexID, 0);
	pMatMgr->SetTexture(uSourceTexID, 0);

	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, 6 * 3));

	// 3 channels
	TViewport const &vp = pDD->GetViewport();
	auto vo0 = TVector2((float)vp.x0, (float)vp.y0);
	auto vo1 = TVector2((float)vp.x1, (float)vp.y1);
	for (int i = 0; i < 3; ++i) {
		static float pOff[3] = { 25.f / pDD->Width(), 19.f / pDD->Width(), -35.f / pDD->Width() };
		static uint pCol[3] = { HARD_COLOR_ARGB(255, 255, 0, 0), HARD_COLOR_ARGB(255, 0, 255, 0), HARD_COLOR_ARGB(255, 0, 0, 255)};
		float fOffX = (pOff[i] * m_fIntensX + ((((rand() & 127) - 64) * (1.f / 3.f) * m_fRandom) / pDD->Width()));
		float fOffY = (pOff[i] * m_fIntensY + ((((rand() & 127) - 64) * (1.f / 3.f) * m_fRandom) / pDD->Height()));
		neon::PrepareVertices(
			pVertices + i * 6,
			TVector2(vo0.x + fOffX, vo0.y + fOffY),
			TVector2(vo1.x + fOffX, vo1.y + fOffY),
			pCol[i]
		);
	}
	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, 6);

	// Dibujar con el filtro y el modo seleccionados
	// es-en: Draw with the selected mode filter
	pDD->SetRenderTarget(CMaterialManager::INVALID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uTexID, 0);
	pDD->SetBilinearFiltering(0, true);

	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(iTexSrc);
	UNREFERENCED_PARAMETER(iTexTgt);
#endif//GFX_ENGINE
	return IsOk();
}
