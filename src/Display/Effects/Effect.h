//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_EFFECT_H
#define NEON_EFFECT_H

class CDisplayDevice;
struct TCtrlVar;

class CEffect {
public:
	static auto Create(std::string& sFile) -> CEffect*;

	CEffect();
	virtual ~CEffect() { m_bOk = false; }

	virtual auto Init(CNodeFile::CNode* pNode) -> bool = 0;

	virtual auto SetTime(float fTime) -> void;
	virtual auto Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool = 0;

	virtual auto GetVarCtrls() const -> TCtrlVar* = 0;
	virtual auto SetVar(int iVar, void const* pData) -> void = 0;
	virtual auto GetVar(int iVar) -> void const* = 0;

	auto IsOk() const -> bool { return m_bOk; }

protected:
	bool m_bOk;
	float m_fTime;
	float m_fTimePrev;
	float m_fTimeFrame;
};

#endif//NEON_EFFECT_H
