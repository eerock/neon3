//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FXSHADER_H
#define NEON_FXSHADER_H

#include "Effect.h"

struct TCustomVar;
struct TCtrlVar;


//---------------------------------------------------------------------------//
//	Class CFXShader
//---------------------------------------------------------------------------//
class CFXShader : public CEffect {
public:
	CFXShader();
	~CFXShader() override;

	auto Init(CNodeFile::CNode* pNode) -> bool override;

	auto SetTime(float fTime) -> void override;
	auto Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool override;

	auto GetVarCtrls() const -> TCtrlVar* override;
	auto SetVar(int iVar, void const* pData) -> void override;
	auto GetVar(int iVar) -> void const* override;

protected:
	static const int MAX_SHADER_TEXTURES = 3;
	bool m_bEnabled;
	uint m_uShaderID;
	uint m_uTextureIDs[MAX_SHADER_TEXTURES];
	int m_iNumTextures;
	int m_iNumVars;
	TCustomVar* m_pVars;
	TCtrlVar* m_pCtrlVars;

private:
	auto Draw(CDisplayDevice* pDD, TVector2 const& uv0, TVector2 const& uv1) -> void;
//	auto InitCtrlVar(int iVar, std::string const& sVarClass, std::string const& sVar) -> bool;
//	auto InitCtrlVar(int iVar, std::string const& sCtrlClass, std::string const& sName, std::string const& sRegister, std::string const& sValue) -> bool;
	auto InitCtrlVar(int iVar, std::string const& sCtrlClass, std::string const& sName, std::string const& sRegister, std::string const& sValue, float fMin = 0.f, float fMax = 1.f) -> bool;
};

#endif//NEON_FXSHADER_H
