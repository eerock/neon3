//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXGlow.h"


enum EGlowVar {
	VAR_GLOW_TYPE,
	VAR_GLOW_INTENSITY,
};

enum EGlowType {
	MIPMAP,
	PIXELSHADER,
	PSYCHO,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_GLOW_TYPE, "GlowType", true, 3, { "MipMap", "Pixel (PS2.0)", "Psycho (PS2.0)" } },
	{ TCtrlVar::SLIDER, VAR_GLOW_INTENSITY, "GlowIntensity", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXGlow::CFXGlow()
	: m_iType(0)
	, m_fIntens(0.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXGlow::~CFXGlow() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXGlow\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXGlow::Init(CNodeFile::CNode* pNode) -> bool {
	UNREFERENCED_PARAMETER(pNode);
	m_iType = 1;
	m_fIntens = 1.f;
	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXGlow::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CFXGlow::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	UNREFERENCED_PARAMETER(uTargetTexID);
	switch (m_iType) {
		case MIPMAP: {
			CEffects::DoGlowMipmap(pDD, uSourceTexID, m_fIntens);
			break;
		}
		case PIXELSHADER: {
			CEffects::DoGlowPS(pDD, uSourceTexID, m_fIntens);
			break;
		}
		case PSYCHO: {
			CEffects::DoGlowPsycho(pDD, uSourceTexID, m_fIntens);
			break;
		}
		default: {
			return false;
		}
	}
	return true;
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXGlow::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXGlow::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_GLOW_TYPE: {
			m_iType = *(static_cast<int const*>(pData));
			break;
		}
		case VAR_GLOW_INTENSITY: {
			m_fIntens = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXGlow::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_GLOW_TYPE: {
			return &m_iType;
		}
		case VAR_GLOW_INTENSITY: {
			return &m_fIntens;
		}
		default: {
			break;
		}
	}
	return nullptr;
}
