//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXDist.h"

#define PLASMA_X  (18)
#define PLASMA_Y  (15)
#define PLASMA_W  (1.f / PLASMA_X)
#define PLASMA_H  (1.f / PLASMA_Y)

enum EDistVar {
	VAR_TYPE,
	VAR_SPEED,
	VAR_INTENSITY,
	VAR_VAR_1,
	VAR_VAR_2,
	VAR_VAR_3,
	VAR_VAR_4,
	VAR_VAR_5,
	VAR_VAR_6,
};

enum EDistType {
	DIST_CRYSTAL = 0,
	DIST_CIRCCRYSTAL,
	DIST_SIN,
	DIST_POLAR,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_TYPE, "Type", true, 4, { "Crystal", "CircCrystal", "Sin", "Polar" } },
	{ TCtrlVar::SLIDER, VAR_SPEED, "Speed", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_INTENSITY, "Intens", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_VAR_1, "Var 1", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_VAR_2, "Var 2", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_VAR_3, "Var 3", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_VAR_4, "Var 4", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_VAR_5, "Var 5", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_VAR_6, "Var 6", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXDist::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXDist::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_TYPE: {
			m_iType = *(static_cast<int const*>(pData));
			break;
		}
		case VAR_SPEED: {
			m_fSpeed = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_INTENSITY: {
			m_fIntens = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_VAR_1: {
			m_fVars[0] = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_VAR_2: {
			m_fVars[1] = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_VAR_3: {
			m_fVars[2] = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_VAR_4: {
			m_fVars[3] = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_VAR_5: {
			m_fVars[4] = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_VAR_6: {
			m_fVars[5] = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXDist::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_TYPE: {
			return &m_iType;
		}
		case VAR_SPEED: {
			return &m_fSpeed;
		}
		case VAR_INTENSITY: {
			return &m_fIntens;
		}
		case VAR_VAR_1: {
			return &m_fVars[0];
		}
		case VAR_VAR_2: {
			return &m_fVars[1];
		}
		case VAR_VAR_3: {
			return &m_fVars[2];
		}
		case VAR_VAR_4: {
			return &m_fVars[3];
		}
		case VAR_VAR_5: {
			return &m_fVars[4];
		}
		case VAR_VAR_6: {
			return &m_fVars[5];
		}
		default: {
			break;
		}
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXDist::CFXDist()
	: CEffect()
	, m_iType(0)
	, m_fSpeed(0.f)
	, m_fIntens(0.f)
{
	for (int i = 0; i < 6; ++i) {
		m_fVars[i] = 0.f;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXDist::Init(CNodeFile::CNode* pNode) -> bool {
	UNREFERENCED_PARAMETER(pNode);
	m_bOk = true;
	// ebp-> todo: read the actual nodefile (xml) and unpack the values into this filter object
	m_iType = DIST_CRYSTAL;
	m_fSpeed = 0.5f;
	m_fIntens = 0.f;
	for (int i = 0; i < 6; ++i) {
		m_fVars[i] = 0.f;
	}
	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXDist::~CFXDist() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXDist\n"));
#endif
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXDist::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CFXDist::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	switch (m_iType) {
		case DIST_CRYSTAL: {
			DrawCrystal(pDD, uSourceTexID, uTargetTexID);
			break;
		}
		case DIST_CIRCCRYSTAL: {
			DrawCircCrystal(pDD, uSourceTexID, uTargetTexID);
			break;
		}
		case DIST_SIN: {
			DrawSin(pDD, uSourceTexID, uTargetTexID);
			break;
		}
		case DIST_POLAR: {
			DrawPolar(pDD, uSourceTexID, uTargetTexID);
			break;
		}
		default: {
			return false;
		}
	}
	return true;
}


//---------------------------------------------------------------------------//
//	DrawCrystal
//---------------------------------------------------------------------------//
auto CFXDist::DrawCrystal(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	float fTime = m_fTime * m_fSpeed;

	// Dibujar con el filtro y el modo seleccionados
	pDD->SetRenderTarget(uTargetTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uSourceTexID, 0);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	pDD->SetBilinearFiltering(0, false);

	auto tViewport = pDD->GetViewport();
	auto pIndices = pDD->LockIndexBuffer(PLASMA_X * PLASMA_Y * 6);
	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, (PLASMA_X + 1) * (PLASMA_Y + 1)));
	auto pTemp = pVertices;
	for (int i = 0; i <= PLASMA_Y; ++i) {
		for (int j = 0; j <= PLASMA_X; ++j) {
			float fX = j * PLASMA_W;
			float fY = i * PLASMA_H;
			
			HCV_SET_XYZW(pTemp, fX * (tViewport.x1 - tViewport.x0 + 1) + tViewport.x0, fY * (tViewport.y1 - tViewport.y0 + 1) + tViewport.y0, 0.f, 1.f);
			float fU = fX + ((cosf(fTime * 5.74f + (fX - 0.5f) * m_fVars[0] * 50.f) * m_fVars[1] * 70.f) +
				(sinf(fTime * 5.74f + (fY - 0.5f) * m_fVars[0] * 50.f) * m_fVars[1] * 70.f)) * m_fIntens * 2.f / pDD->Width ();
			float fV = fY + ((cosf(fTime * 5.74f + (fX - 0.5f) * m_fVars[2] * 50.f) * m_fVars[3] * 70.f) +
				(sinf(fTime * 5.74f + (fY - 0.5f) * m_fVars[2] * 50.f) * m_fVars[3] * 70.f)) * m_fIntens * 2.f / pDD->Height();
			HCV_SET_UV0(pTemp, fU, fV);
			HCV_SET_ARGB(pTemp, 255, 255, 255, 255);
			
			if (i < PLASMA_Y && j < PLASMA_X) {
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)));
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 2);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
			}
			pTemp++;
		}
	}
	pDD->UnlockIndexBuffer();
	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawIndexedPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, PLASMA_X * PLASMA_Y * 2);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DrawCircCrystal
//---------------------------------------------------------------------------//
auto CFXDist::DrawCircCrystal(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	float fTime = m_fTime * m_fSpeed;

	// Dibujar con el filtro y el modo seleccionados
	pDD->SetRenderTarget(uTargetTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uSourceTexID, 0);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	pDD->SetBilinearFiltering(0, false);

	auto tViewport = pDD->GetViewport();
	auto pIndices  = pDD->LockIndexBuffer(PLASMA_X*PLASMA_Y*6);
	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, (PLASMA_X + 1) * (PLASMA_Y + 1)));
	auto pTemp = pVertices;
	for (int i = 0; i <= PLASMA_Y; ++i) {
		for (int j = 0; j <= PLASMA_X; ++j) {
			float fX = j * PLASMA_W;
			float fY = i * PLASMA_H;
			HCV_SET_XYZW(pTemp, fX * (tViewport.x1 - tViewport.x0 + 1) + tViewport.x0, fY * (tViewport.y1 - tViewport.y0 + 1) + tViewport.y0, 0.f, 1.f);
			float fU = fX + (cosf(fTime * 5.74f + (fX - 0.5f) * (fY - 0.5f) * m_fVars[0] * 50.f) * m_fVars[1] * 70.f) * m_fIntens * 2.f / pDD->Width ();
			float fV = fY + (cosf(fTime * 5.45f + (fX - 0.5f) * (fY - 0.5f) * m_fVars[2] * 50.f) * m_fVars[3] * 70.f) * m_fIntens * 2.f / pDD->Height();
			HCV_SET_UV0(pTemp, fU, fV);
			HCV_SET_ARGB(pTemp, 255, 255, 255, 255);
			if (i < PLASMA_Y && j < PLASMA_X) {
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)));
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 2);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
			}
			pTemp++;
		}
	}
	pDD->UnlockIndexBuffer();
	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawIndexedPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, PLASMA_X * PLASMA_Y * 2);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DrawSin
//---------------------------------------------------------------------------//
auto CFXDist::DrawSin(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	float fTime = m_fTime * m_fSpeed;

	// Dibujar con el filtro y el modo seleccionados
	pDD->SetRenderTarget(uTargetTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uSourceTexID, 0);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	pDD->SetBilinearFiltering(0, false);

	auto tViewport = pDD->GetViewport();
	auto pIndices = pDD->LockIndexBuffer(PLASMA_X * PLASMA_Y * 6);
	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, (PLASMA_X + 1) * (PLASMA_Y + 1)));
	auto pTemp = pVertices;
	for (int i = 0; i <= PLASMA_Y; ++i) {
		for (int j = 0; j <= PLASMA_X; ++j) {
			float fX = j * PLASMA_W;
			float fY = i * PLASMA_H;
			HCV_SET_XYZW(pTemp, fX * (tViewport.x1 - tViewport.x0 + 1) + tViewport.x0, fY * (tViewport.y1 - tViewport.y0 + 1) + tViewport.y0, 0.f, 1.f);
			float fU = fX + (cosf(fTime * 10.f + (fX - 0.5f) * (fY - 0.5f) * 10.f * m_fVars[1]) * m_fVars[0] * 50.f / pDD->Width ()) * m_fIntens;
			float fV = fY + (cosf(fTime * 10.f + (fY - 0.5f) * (fX - 0.5f) * 10.f * m_fVars[3]) * m_fVars[2] * 50.f / pDD->Height()) * m_fIntens;
			HCV_SET_UV0(pTemp, fU, fV);
			HCV_SET_ARGB(pTemp, 255, 255, 255, 255);
			if (i < PLASMA_Y && j < PLASMA_X) {
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)));
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 2);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
			}
			pTemp++;
		}
	}
	pDD->UnlockIndexBuffer();
	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawIndexedPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, PLASMA_X * PLASMA_Y * 2);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DrawPolar
//---------------------------------------------------------------------------//
auto CFXDist::DrawPolar(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	UNREFERENCED_PARAMETER(uSourceTexID);
	auto pD3D = pDD->GetD3DDevice();
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	//float fTime = m_fTime * m_fSpeed;		// ebp-> unreferenced variable

	// Dibujar con el filtro y el modo seleccionados
	pDD->SetRenderTarget (uTargetTexID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(uTargetTexID, 0);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	pD3D->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	pDD->SetBilinearFiltering (0, false);

	auto tViewport = pDD->GetViewport();
	auto pIndices = pDD->LockIndexBuffer(PLASMA_X * PLASMA_Y * 6);
	auto pVertices = static_cast<TVertex_HCV_XYZ_T*>(pDD->LockVertexBuffer(HCV_XYZ_T, (PLASMA_X + 1) * (PLASMA_Y + 1)));
	auto pTemp = pVertices;
	for (int i = 0; i <= PLASMA_Y; ++i) {
		for (int j = 0; j <= PLASMA_X; ++j) {
			float fX = j * PLASMA_W;
			float fY = i * PLASMA_H;
			HCV_SET_XYZW(pTemp, fX * (tViewport.x1 - tViewport.x0 + 1) + tViewport.x0, fY * (tViewport.y1 - tViewport.y0 + 1) + tViewport.y0, 0.f, 1.f);
			fX -= 0.5f;
			fY -= 0.5f;
			float fMag = sqrtf(fX * fX + fY * fY) * 2.f * m_fVars[0];
			// angle = arc tangent of y/x
			float fAngle = (atan2(fY, fX) * m_fVars[1]) * 1.f / 6.28319f;
			while (fAngle >= 1.f) {
				fAngle -= 1.f;
			}
			while (fAngle < 0.f) {
				fAngle += 1.f;
			}
			while (fMag > 1.f) {
				fMag -= 1.f;
			}
			while (fMag < 0.f) {
				fMag += 1.f;
			}
			HCV_SET_UV0(pTemp, fAngle * m_fIntens + ((fX + 0.5f) * (1 - m_fIntens)), fMag * m_fIntens + ((fY + 0.5f) * (1 - m_fIntens)));
			HCV_SET_ARGB(pTemp, 255, 255, 255, 255);
			if (i < PLASMA_Y && j < PLASMA_X) {
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)));
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + 1);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 2);
				*pIndices++ = static_cast<ushort>(j + (i * (PLASMA_X + 1)) + PLASMA_X + 1);
			}
			pTemp++;
		}
	}

	pDD->UnlockIndexBuffer();
	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawIndexedPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, PLASMA_X * PLASMA_Y * 2);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
#endif//GFX_ENGINE
}


//---------------------------------------------------------------------------//
//	DrawFish
//---------------------------------------------------------------------------//
/*
auto CFXDist::DrawFish(CDisplayDevice *pDD, TRenderTgt *pRenderTgt, float fAlphaGlobal) -> void {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();

	float fTime = m_fTime * m_fSpeed;

	// Dibujar con el filtro y el modo seleccionados
	pDD->SetRenderTarget(CMaterialManager::INVALID);
	pDD->ApplyBasicShader();
	pMatMgr->SetTexture(iTexSrc, 0);
	ApplyMode(pDD, pRenderTgt, fAlphaGlobal);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pDD->SetBilinearFiltering(0, false);

	TViewport const& tViewport = pDD->GetViewport();
	ushort* pIndices  = pDD->LockIndexBuffer(PLASMA_X * PLASMA_Y * 6);
	TVertex_HCV_XYZ_T* pVertices = (TVertex_HCV_XYZ_T*)pDD->LockVertexBuffer(HCV_XYZ_T, (PLASMA_X+1)*(PLASMA_Y+1));
	TVertex_HCV_XYZ_T* pTemp = pVertices;
	for (int i = 0; i <= PLASMA_Y; i++) {
		for (int j = 0; j <= PLASMA_X; j++) {
			float fX = j * PLASMA_W;
			float fY = i * PLASMA_H;
			HCV_SET_XYZ(pTemp, fX * (tViewport.x1-tViewport.x0) + tViewport.x0, fY * (tViewport.y1-tViewport.y0) + tViewport.y0, 1.f);
			float fLength = sqrtf((fX-0.5f) * (fX-0.5f) + (fY-0.5f) * (fY-0.5f));
			float fU = fX * (1.f - m_fIntens) + fY * m_fIntens;
			float fV = fY * (1.f - m_fIntens) + (sinf((fX-0.5f) * m_fVars[0] * 10.f * fLength) * m_fIntens);
			HCV_SET_UV0(pTemp, fU, fV);
			HCV_SET_ARGB(pTemp, 255, 255, 255, 255);
			if (i < PLASMA_Y && j < PLASMA_X) {
				*pIndices++ = j+(i*(PLASMA_X+1));
				*pIndices++ = j+(i*(PLASMA_X+1))+1;
				*pIndices++ = j+(i*(PLASMA_X+1))+PLASMA_X+1;
				*pIndices++ = j+(i*(PLASMA_X+1))+1;
				*pIndices++ = j+(i*(PLASMA_X+1))+PLASMA_X+2;
				*pIndices++ = j+(i*(PLASMA_X+1))+PLASMA_X+1;
			}
			pTemp++;
		}
	}
	pDD->UnlockIndexBuffer();
	pDD->UnlockVertexBuffer(HCV_XYZ_T);
	pDD->DrawIndexedPrimitive(HCV_XYZ_T, HARD_PRIM_TRIANGLELIST, PLASMA_X*PLASMA_Y*2);
#endif//GFX_ENGINE_DIRECT3D
}
*/