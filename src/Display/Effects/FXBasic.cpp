//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXBasic.h"

// Vars
static TCtrlVar s_Vars[] = {
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXBasic::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXBasic::SetVar(int iVar, void const* pData) -> void {
	UNREFERENCED_PARAMETER(iVar);
	UNREFERENCED_PARAMETER(pData);
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXBasic::GetVar(int iVar) -> void const* {
	UNREFERENCED_PARAMETER(iVar);
	return nullptr;
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXBasic::CFXBasic()
	: CEffect()
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXBasic::Init(CNodeFile::CNode* pNode) -> bool {
	UNREFERENCED_PARAMETER(pNode);
	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXBasic::~CFXBasic() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXBasic\n"));
#endif
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXBasic::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CFXBasic::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	// ebp-> this is a pass-through filter
	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	pMatMgr->SetTexture(uSourceTexID, 0);
	pDD->ApplyBasicShader();
	pDD->SetRenderTarget(uTargetTexID);
	neon::DrawQuad(pDD);
	return IsOk();
}
