//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXPixelate.h"


enum EPixelateVar {
	VAR_WIREFRAME,
	VAR_INTENSITY,
};

static TCtrlVar s_Vars[] = {
	{ TCtrlVar::CHECK_BOX, VAR_WIREFRAME, "Wireframe", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_INTENSITY, "Intensity", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


// This doesn't work, find out why.


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXPixelate::CFXPixelate()
	: m_iType(0)
	, m_bWireFrame(false)
	, m_fIntens(0.f)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXPixelate::~CFXPixelate() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXPixelate\n"));
#endif
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXPixelate::Init(CNodeFile::CNode* pNode) -> bool {
	UNREFERENCED_PARAMETER(pNode);

	m_bWireFrame = false;
	m_fIntens = 0.5f;

	m_bOk = true;
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXPixelate::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//	ebp-> this is unfinished
//---------------------------------------------------------------------------//
auto CFXPixelate::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
#if (GFX_ENGINE == GFX_ENGINE_DIRECT3D)
	auto pD3D = pDD->GetD3DDevice();

	float fIntens = (1.f - m_fIntens) * (1.f - m_fIntens) * 0.9f + 0.1f;
	float fU, fV;
	uint uTexID = CEffects::DoPixelate(pDD, uSourceTexID, fIntens, fU, fV);

	// Dibujar en wireframe
	if (m_bWireFrame) {
		//float fDist = pEffectSource->iW * fIntens;
		//for (int i = )
	}

	// Dibujar con el filtro y el modo seleccionados
	pDD->SetRenderTarget(CMaterialManager::INVALID);
	pDD->ApplyBasicShader();

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	pMatMgr->SetTexture(uTexID, 0);
	//ApplyMode(pDD, uTargetTexID, fAlphaGlobal);

	pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TFACTOR);
	pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
	pDD->SetBilinearFiltering(0, false);

	//DrawQuadUV(pDD, TVector2(pRenderTgt->vUV0.x * fU, pRenderTgt->vUV0.y * fV), TVector2(pRenderTgt->vUV1.x * fU, pRenderTgt->vUV1.y * fV));
	pDD->SetRenderTarget(uTargetTexID);
	neon::DrawQuad(pDD);
#else
	UNREFERENCED_PARAMETER(pDD);
	UNREFERENCED_PARAMETER(uSourceTexID);
	UNREFERENCED_PARAMETER(uTargetTexID);
#endif//GFX_ENGINE
	return m_bOk;
}


//---------------------------------------------------------------------------//
//	GetVars
//---------------------------------------------------------------------------//
auto CFXPixelate::GetVarCtrls() const -> TCtrlVar* {
	return s_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXPixelate::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_WIREFRAME: {
			m_bWireFrame = *(static_cast<bool const*>(pData));
			break;
		}
		case VAR_INTENSITY: {
			m_fIntens = *(static_cast<float const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXPixelate::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_WIREFRAME: {
			return &m_bWireFrame;
		}
		case VAR_INTENSITY: {
			return &m_fIntens;
		}
		default: {
			return nullptr;
		}
	}
}
