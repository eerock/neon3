//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FXDIST_H
#define NEON_FXDIST_H

#include "Effect.h"

class CFXDist : public CEffect {
public:
	CFXDist();
	~CFXDist() override;

	auto Init(CNodeFile::CNode* pNode) -> bool override;

	auto SetTime(float fTime) -> void override;
	auto Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool override;

	auto GetVarCtrls() const -> TCtrlVar* override;
	auto SetVar(int iVar, void const* pData) -> void override;
	auto GetVar(int iVar) -> void const* override;

private:
	auto DrawCrystal(CDisplayDevice*, uint, uint) -> void;
	auto DrawCircCrystal(CDisplayDevice*, uint, uint) -> void;
	auto DrawSin(CDisplayDevice*, uint, uint) -> void;
	auto DrawPolar(CDisplayDevice*, uint, uint) -> void;
	auto DrawPlasma(CDisplayDevice*, uint, uint) -> void;
	auto DrawFish(CDisplayDevice*, uint, uint) -> void;
	auto DrawMov(CDisplayDevice*, uint, uint) -> void;
	auto DrawFlip(CDisplayDevice*, uint, uint) -> void;

private:
	int m_iType;
	float m_fSpeed;
	float m_fIntens;
	float m_fVars[6];
};

#endif//NEON_FXDIST_H
