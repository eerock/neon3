//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "DisplayPch.h"
#include "FXShader.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFXShader::CFXShader()
	: m_bEnabled(false)
	, m_uShaderID(CMaterialManager::INVALID)
	, m_iNumTextures(0)
	, m_iNumVars(0)
	, m_pVars(nullptr)
	, m_pCtrlVars(nullptr)
{
	for (int i = 0; i < MAX_SHADER_TEXTURES; ++i) {
		m_uTextureIDs[i] = CMaterialManager::INVALID;
	}
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFXShader::~CFXShader() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFXShader\n"));
#endif
	if (IsOk()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		for (int i = 0; i < MAX_SHADER_TEXTURES; ++i) {
			pMatMgr->RemoveTexture(m_uTextureIDs[i]);
			m_uTextureIDs[i] = CMaterialManager::INVALID;
		}
		m_iNumTextures = 0;

		pMatMgr->RemoveShader(m_uShaderID);

		DELETE_ARRAY(m_pCtrlVars);
		DELETE_ARRAY(m_pVars);

		m_bOk =false;
	}
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CFXShader::Init(CNodeFile::CNode* pNode) -> bool {
	m_bOk = false;

	std::string const& sShaderFilename = pNode->AsString("shader");
	if (!sShaderFilename.empty()) {
		auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
		m_uShaderID = pMatMgr->AddShader(sShaderFilename);
		if (m_uShaderID != CMaterialManager::INVALID) {
			// ebp-> todo feature: add image list or dir as combo box...

			// Look for and load any referenced textures...
			auto pTextureNode = pNode->FirstNode("texture");
			while (pTextureNode) {
				std::string const& sTextureName = pTextureNode->AttrAsString("file");
				if (!sTextureName.empty()) {
					// add to the material manager...
					m_uTextureIDs[m_iNumTextures] = pMatMgr->AddTexture(sTextureName, 0);
					if (m_uTextureIDs[m_iNumTextures] == CMaterialManager::INVALID) {
						GLOG(("ERROR: Failed to load texture %s referenced in %s in slot %d.\n", sTextureName.c_str(), sShaderFilename.c_str(), m_iNumTextures));
					}

					if (++m_iNumTextures >= 3) {
						break;
					}
				}

				pTextureNode = pTextureNode->NextNode("texture");
			}

			// now parse through the vars...
			m_iNumVars = 0;
			auto pVarNode = pNode->FirstNode("var");
			while (pVarNode) {
				++m_iNumVars;
				pVarNode = pVarNode->NextNode("var");
			}

			// ebp-> why + 2?	because it puts one in for the techniques combo box and one for the TCtrlVar::INVALID final element.
			m_pCtrlVars = NEW_ARRAY(TCtrlVar, m_iNumVars + 2);
			memset(m_pCtrlVars, 0, sizeof(TCtrlVar) * (m_iNumVars + 2));
			// ebp-> why + 1?	because it puts a dummy one in for the techniques combo box
			m_pVars = NEW_ARRAY(TCustomVar, m_iNumVars + 1);
			memset(m_pVars, 0, sizeof(TCustomVar) * (m_iNumVars + 1));

			// First Var = techniques combo box
			auto pShader = pMatMgr->GetShader(m_uShaderID);
			m_pCtrlVars[0].iType = TCtrlVar::COMBO_BOX;
			m_pCtrlVars[0].iNum = 0;
			m_pCtrlVars[0].iNumOptions = pShader->GetNumTechniques();
			m_pCtrlVars[0].bLinkable = false;
			Strncpy(m_pCtrlVars[0].szName, "Technique", TCtrlVar::SIZE_NAME_STR);
			m_pCtrlVars[0].szName[TCtrlVar::SIZE_NAME_STR] = 0;	// null-terminate
			for (int i = 0; i < m_pCtrlVars[0].iNumOptions; ++i) {
				Strncpy(m_pCtrlVars[0].aszOptions[i], pShader->GetTechniqueName(i), TCtrlVar::SIZE_OPTION_STR);
				m_pCtrlVars[0].aszOptions[i][TCtrlVar::SIZE_OPTION_STR] = 0;	// null-terminate
			}
			m_pVars[0].Init(0, -1);

			// Custom vars
			int iVar = 1;
			pVarNode = pNode->FirstNode("var");

			// ebp-> fixit: I don't think this parses through combo boxes well...
			// check against Sobel filter.
			while (pVarNode) {
				std::string sCtrlClass = pVarNode->AttrAsString("varclass");
				std::string sName = pVarNode->AsString("name");
				std::string sRegister = pVarNode->AsString("register");
				std::string sValue = pVarNode->AsString("value");

				// ebp-> add min/max values to SLIDER varclass if they exist
				float fMin = pVarNode->AsFloat("min", 0.f);
				float fMax = pVarNode->AsFloat("max", 1.f);

				InitCtrlVar(iVar++, sCtrlClass, sName, sRegister, sValue, fMin, fMax);
				pVarNode = pVarNode->NextNode("var");
			}

			// Ultimo elemento
			m_pCtrlVars[m_iNumVars + 1].iType = TCtrlVar::INVALID;
			m_bOk = true;
		}
	}
	return IsOk();
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CFXShader::SetTime(float fTime) -> void {
	CEffect::SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CFXShader::Apply(CDisplayDevice* pDD, uint uSourceTexID, uint uTargetTexID) -> bool {
	// CustomVars para el shader
	uint uTechnique = m_pVars[0].iValue;
	for (int i = 1; i <= m_iNumVars; ++i) {
		if (m_pVars[i].iRegister) {
			float f = m_pVars[i].GetFloat();
			CEngine3D::SetRegisterData(m_pVars[i].iRegister, &f);
		}
	}

	// Draw shader to target
	pDD->ApplyBasicShader();
	pDD->SetRenderTarget(uTargetTexID);
	pDD->SetBilinearFiltering(0, true);
	pDD->SetBilinearFiltering(1, true);
	pDD->SetBilinearFiltering(2, true);
	pDD->SetBilinearFiltering(3, true);

	auto pMatMgr = CServiceLocator<IMaterialManagerService>::GetService();
	auto pShader = pMatMgr->GetShader(m_uShaderID);
	uint uNumPasses = pShader->BeginDraw(uTechnique);
	for (uint uPass = 0; uPass < uNumPasses; ++uPass) {
		pMatMgr->SetTexture(uSourceTexID, 0);
		for (int i = 0; i < MAX_SHADER_TEXTURES; ++i) {
			pMatMgr->SetTexture(m_uTextureIDs[i], i + 1);
		}
		pShader->BeginPass(uPass);
		Draw(pDD, TVector2(), TVector2(1.f, 1.f));
		pShader->EndPass();
	}
	pShader->EndDraw();

	return true;
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CFXShader::Draw(CDisplayDevice* pDD, TVector2 const& uv0, TVector2 const& uv1) -> void {
	pDD->SetIdentity();
	// Screen aligned quad, para que asi el filtro pueda tener un VS
	auto v0 = TVector2(-1.f, 1.f);
	auto v1 = TVector2(1.f, -1.f);
	auto pVertices = static_cast<TVertex_HCV_XYZ_S*>(pDD->LockVertexBuffer(HCV_XYZ_S, 4));

	HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
	HCV_SET_XYZ(pVertices, v0.x, v0.y, 0.f);
	HCV_SET_UV0(pVertices, uv0.x, uv0.y);
	HCV_SET_UV1(pVertices, uv0.x, uv0.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
	HCV_SET_XYZ(pVertices, v1.x, v0.y, 0.f);
	HCV_SET_UV0(pVertices, uv1.x, uv0.y);
	HCV_SET_UV1(pVertices, uv1.x, uv0.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
	HCV_SET_XYZ(pVertices, v1.x, v1.y, 0.f);
	HCV_SET_UV0(pVertices, uv1.x, uv1.y);
	HCV_SET_UV1(pVertices, uv1.x, uv1.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
	HCV_SET_XYZ(pVertices, v0.x, v1.y, 0.f);
	HCV_SET_UV0(pVertices, uv0.x, uv1.y);
	HCV_SET_UV1(pVertices, uv0.x, uv1.y);
	++pVertices;

	pDD->UnlockVertexBuffer(HCV_XYZ_S);
	pDD->DrawPrimitive(HCV_XYZ_S, HARD_PRIM_TRIANGLEFAN, 2);
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CFXShader::GetVarCtrls() const -> TCtrlVar* {
	return m_pCtrlVars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CFXShader::SetVar(int iVar, void const* pData) -> void {
	m_pVars[iVar].SetValue(pData);
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CFXShader::GetVar(int iVar) -> void const* {
	return m_pVars[iVar].GetValue();
}


//---------------------------------------------------------------------------//
//	InitCtrlVar
//---------------------------------------------------------------------------//
auto CFXShader::InitCtrlVar(int iVar, std::string const& sCtrlClass, std::string const& sName, std::string const& sRegister, std::string const& sValue, float fMin, float fMax) -> bool {
	int iType = TCtrlVar::GetCtrlVarType(sCtrlClass);
	switch (iType) {
		// Check Box
		case TCtrlVar::CHECK_BOX: {
			m_pCtrlVars[iVar].iType = TCtrlVar::CHECK_BOX;
			m_pCtrlVars[iVar].iNum = iVar;
			m_pCtrlVars[iVar].bLinkable = true;
			m_pCtrlVars[iVar].iNumOptions = 0;
			Strncpy(m_pCtrlVars[iVar].szName, sName.c_str(), TCtrlVar::SIZE_NAME_STR);
			m_pCtrlVars[iVar].szName[TCtrlVar::SIZE_NAME_STR] = 0;	// null-terminate for safety
			m_pVars[iVar].Init(string_to<bool>(sValue), CEngine3D::GetRegisterByName(sRegister));
			break;
		}
		// Combo Box
		case TCtrlVar::COMBO_BOX: {
			m_pCtrlVars[iVar].iType = TCtrlVar::COMBO_BOX;
			m_pCtrlVars[iVar].iNum = iVar;
			m_pCtrlVars[iVar].bLinkable = true;
			m_pCtrlVars[iVar].iNumOptions = 0;
			Strncpy(m_pCtrlVars[iVar].szName, sName.c_str(), TCtrlVar::SIZE_NAME_STR);
			m_pCtrlVars[iVar].szName[TCtrlVar::SIZE_NAME_STR] = 0;	// null-terminate for correctness

			// ebp-> todo, fixit: there needs to be some code that parses through the nfx
			// and sends all the combo box options through.
			//do {
			//	int iValueAnt = iValue;
			//	iValue = sVar.find(",", iValue + 1);
			//	m_pCtrlVars[iVar].iNumOptions++;
			//	if (iValue <= 0) {
			//		strcpy(m_pCtrlVars[iVar].aszOptions[j], sVar.substr(iValueAnt + 1, sVar.size() - iValueAnt - 1).c_str());
			//	} else {
			//		strcpy(m_pCtrlVars[iVar].aszOptions[j], sVar.substr(iValueAnt + 1, iValue - iValueAnt - 1).c_str());
			//	}
			//	j++;
			//} while (iValue > 0);

			m_pVars[iVar].Init(string_to<int>(sValue), CEngine3D::GetRegisterByName(sRegister));
			break;
		}
		// Slider val
		case TCtrlVar::SLIDER: {
			m_pCtrlVars[iVar].iType = TCtrlVar::SLIDER;
			m_pCtrlVars[iVar].iNum = iVar;
			m_pCtrlVars[iVar].bLinkable = true;
			m_pCtrlVars[iVar].iNumOptions = 0;
			Strncpy(m_pCtrlVars[iVar].szName, sName.c_str(), TCtrlVar::SIZE_NAME_STR);
			m_pCtrlVars[iVar].szName[TCtrlVar::SIZE_NAME_STR] = 0;	// null-terminate for safety and correctness ;)
			m_pVars[iVar].Init(string_to<float>(sValue), CEngine3D::GetRegisterByName(sRegister));
			m_pVars[iVar].SetMinMax(fMin, fMax);
			break;
		}
		default: {
			return false;
		}
	}

	return true;
}


//---------------------------------------------------------------------------//
//	InitCtrlVar
//		Old Code
//---------------------------------------------------------------------------//
/*
auto CFXShader::InitCtrlVar(int iVar, std::string const& sVarClass, std::string const& sVar) -> bool {
	// ebp-> apparently this assumes sVar coming in will be like this:
	//		"VAR1,IntensY,0,"
	int iReg = sVar.find(",", 0);
	if (iReg <= 0) {
		return false;
	}
	int iName = sVar.find(",", iReg + 1);
	if (iName <= 0) {
		return false;
	}
	int iValue = sVar.find(",", iName + 1);
	if (iValue <= 0) {
		return false;
	}
	int j = 0;

	//switch (GetCtrlVarType(sVar.substr(0, iReg)))
	switch (GetCtrlVarType(sVarClass)) {
		// CHECK BOX
		case TCtrlVar::CHECK_BOX: {
			m_pCtrlVars[iVar].iType = TCtrlVar::CHECK_BOX;
			m_pCtrlVars[iVar].iNum = iVar;
			m_pCtrlVars[iVar].bLinkable = true;
			m_pCtrlVars[iVar].iNumOptions = 0;
			strcpy(m_pCtrlVars[iVar].szName, sVar.substr(iName + 1, iValue - iName - 1).c_str());
			m_pVars[iVar].Init(strtobool(sVar.substr(iValue + 1, sVar.size() - iValue - 1)), CEngine3D::GetRegisterByName(sVar.substr(iReg + 1, iName - iReg - 1)));
			break;
		}
		// COMBO_BOX
		case TCtrlVar::COMBO_BOX: {
			m_pCtrlVars[iVar].iType = TCtrlVar::COMBO_BOX;
			m_pCtrlVars[iVar].iNum = iVar;
			m_pCtrlVars[iVar].bLinkable = true;
			m_pCtrlVars[iVar].iNumOptions = 0;
			strcpy(m_pCtrlVars[iVar].szName, sVar.substr(iName + 1, iValue - iName - 1).c_str());
			do {
				int iValueAnt = iValue;
				iValue = sVar.find(",", iValue + 1);
				m_pCtrlVars[iVar].iNumOptions++;
				if (iValue <= 0) {
					strcpy(m_pCtrlVars[iVar].aszOptions[j], sVar.substr(iValueAnt + 1, sVar.size() - iValueAnt - 1).c_str());
				} else {
					strcpy(m_pCtrlVars[iVar].aszOptions[j], sVar.substr(iValueAnt + 1, iValue - iValueAnt - 1).c_str());
				}
				j++;
			} while (iValue > 0);
			m_pVars[iVar].Init(0, CEngine3D::GetRegisterByName(sVar.substr(iReg + 1, iName - iReg - 1)));
			break;
		}
		// SLIDER: CustomVar=Float
		case TCtrlVar::SLIDER: {
			m_pCtrlVars[iVar].iType = TCtrlVar::SLIDER;
			m_pCtrlVars[iVar].iNum = iVar;
			m_pCtrlVars[iVar].bLinkable = true;
			m_pCtrlVars[iVar].iNumOptions = 0;
			strcpy(m_pCtrlVars[iVar].szName, sVar.substr(iName + 1, iValue - iName - 1).c_str());
			m_pVars[iVar].Init(strtofloat(sVar.substr(iValue + 1, sVar.size() - iValue - 1)), CEngine3D::GetRegisterByName(sVar.substr(iReg + 1, iName - iReg - 1)));
			break;
		}
		default: {
			break;
		}
	}
	return true;
}
*/
