//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Str.h"

static int s_iDirStack = 0;
static std::string s_pDirStack[100];



//---------------------------------------------------------------------------//
auto Stricmp(std::string const& s1, std::string const& s2) -> int {
	return s1.compare(s2);
}


//---------------------------------------------------------------------------//
//	This is supposed to take sStr, which is a filepath, and split it into
//	two strings, sDir and sFile.
//---------------------------------------------------------------------------//
auto SplitPath(std::string const& sStr, std::string& sDir, std::string& sFile) -> void {
	sDir.clear();
	sFile = sStr;
	for (auto i = sStr.length(); i > 0; --i) {
		if (sStr[i-1] == '\\' || sStr[i-1] == '/') {
			sDir  = sStr.substr(0, i);
			sFile = sStr.substr(i + 1, sStr.length() - i - 1);
			return;
		}
	}
}

//---------------------------------------------------------------------------//
auto PushDir(std::string const& sDir) -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	char pDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pDir);
	s_pDirStack[s_iDirStack++] = pDir;
	SetCurrentDirectory(sDir.c_str());
#endif//PLATFORM
}

//---------------------------------------------------------------------------//
auto PopDir() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	SetCurrentDirectory(s_pDirStack[--s_iDirStack].c_str());
#endif//PLATFORM
}

