//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_SOUND_H
#define NEON_SOUND_H

// any platform should be able to use portaudio, but bass is
// windows only.
#define USE_PORTAUDIO_SOUND

#ifdef USE_PORTAUDIO_SOUND
#include "portaudio.h"

typedef float SAMPLE;
typedef float REC_SAMPLE;

typedef struct {
	bool bFreshData;
	bool bPaused;
	int maxFrameIndex;
	int channels;
	REC_SAMPLE* recordedSamples;
} paRecData;

#endif//USE_PORTAUDIO_SOUND


enum EFreqChannels {
	FREQ_CHANNELS = 6, // 0 = global, 1-5 = predefined frequencies
};

enum EFFTSize {
	FFT_SIZE_128 = (1 << 7),
	FFT_SIZE_256 = (1 << 8),
	FFT_SIZE_512 = (1 << 9),
	FFT_SIZE_1024 = (1 << 10),
};

enum EWindowFunction {
	WINDOW_RECTANGULAR,		// w(n) = 1
	WINDOW_TRIANGULAR,		// w(n) = 1 - |(n - (N-1)/2) / (L/2)|
	WINDOW_HAMMING,			// w(n) = a - b cos (2*PI*n / (N-1))	with a = 0.54 and b = 1-a = 0.46
	WINDOW_HANNING,			// w(n) = 0.5 * (1 - cos (2*PI*n / (N-1))
	WINDOW_BLACKMAN,		// w(n) = a0 - a1 cos (2*PI*n / (N-1)) + a2 cos (4*PI*n / (N-1))	with a0 = 0.42659, a1 = 0.49656, a2 = 0.076849
};


class CSound {
public:
	CSound(THWnd hWnd);
	~CSound();

	auto Load(std::string const& sFile) -> void;
	auto Update() -> void;
	auto Record() -> void;
	auto Play(float fStart) -> void;
	auto Stop() -> void;
	auto Pause() -> void;
	auto Resume() -> void;

	auto SetFFTSize(int iSize) -> void;
	auto GetFFT(float* pFFT, int iSize = FFT_SIZE_512, float fGain = 1.f) -> bool;

	auto EnumerateDevices(std::vector<std::string>& vDevices) -> void;

private:
	bool m_bOk;
	bool m_bPlaying;
	bool m_bRecording;
	int m_iFFTSize;

#ifdef USE_BASS_SOUND
	HSTREAM m_hStream;
	HRECORD m_hRecord;
#endif//USE_BASS_SOUND

#ifdef USE_PORTAUDIO_SOUND
	// ...
	PaStreamParameters m_tRecParameters;
	PaStream* m_pRecStream;
	paRecData m_tRecData;

	SAMPLE m_fft_data[FFT_SIZE_1024 * 2];
	std::vector<SAMPLE> m_window;
	
#endif//USE_PORTAUDIO_SOUND
};

#endif//NEON_SOUND_H
