//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include <fcntl.h>
#include <io.h>

//----------------------------------------------------------------------------
// Log que muestra cadenas en el Output del VisualStudio
//----------------------------------------------------------------------------
#if defined(LOG_ENABLED)

static int s_iLogType = LOGCONSOLE;
static int s_iLogStrings = 0;
static FILE* s_hLogFile = nullptr;
static FILE* s_hConOut = nullptr;
static FILE* s_hConIn = nullptr;
static FILE* s_hConErr = nullptr;
static THWnd s_hListBoxLog;
static std::ofstream s_Stream;

static const short MAX_CONSOLE_LINES = 1000;

//---------------------------------------------------------------------------//
//	MakeConsole
//	Creates a Console window and reroutes stdout to it
//	http://support.microsoft.com/kb/105305
//---------------------------------------------------------------------------//
auto MakeConsole() -> void {
	int hConHandle;
	AllocConsole();
	
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

	// redirect unbuffered STDOUT to the console
	long handle_out = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(handle_out, _O_TEXT);
	s_hConOut = _fdopen(hConHandle, "w");
	*stdout = *s_hConOut;
	setvbuf(stdout, nullptr, _IONBF, 0);

	// redirect STDIN to the console
	long handle_in = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(handle_in, _O_TEXT);
	s_hConIn = _fdopen(hConHandle, "w");
	*stdin = *s_hConIn;
	setvbuf(stdin, nullptr, _IONBF, 0);

	// redirect STDERR to the console
	//long handle_err = (long)GetStdHandle(STD_ERROR_HANDLE);
	//hConHandle = _open_osfhandle(handle_err, _O_TEXT);
	//s_hConErr = _fdopen(hConHandle, "w");
	//*stderr = *s_hConErr;
	//setvbuf(stderr, nullptr, _IONBF, 0);

	std::ios::sync_with_stdio();
}


//---------------------------------------------------------------------------//
//	_glogStart
//---------------------------------------------------------------------------//
auto _glogStart(int iType, std::string const& sLogFile) -> void {
	s_iLogType = iType;

	// log file specified, so set up the file...
	if (!sLogFile.empty()) {
#if (PLATFORM == PLATFORM_WINDOWS)
		errno_t err = 0;
		if((err = fopen_s(&s_hLogFile, sLogFile.c_str(), "wt+")) != 0) {
#else
		if((s_hLogFile = fopen(sLogFile.c_str(), "wt+")) == nullptr) {
#endif//PLATFORM
			return;
		}
	}


	// Open log window...
	switch (s_iLogType) {

		case LOGCONSOLE: {
			MakeConsole();
			break;
		}

		// ebp-> todo: position the log window from output window...
		case LOGLISTBOX: {
#if (PLATFORM == PLATFORM_WINDOWS)
			s_hListBoxLog = CreateWindow(TEXT("ListBox"), TEXT("Log"), WS_VSCROLL | WS_TABSTOP, 5, 5, 600, 300, NULL, NULL, NULL, NULL);
			ShowWindow(s_hListBoxLog, SW_SHOW);
#endif//PLATFORM
			s_iLogStrings = 0;
			break;
		}

		case LOGEDITBOX: {
#if (PLATFORM == PLATFORM_WINDOWS)
			s_hListBoxLog = CreateWindowEx(
				WS_EX_CLIENTEDGE,
				TEXT("Edit"),
				TEXT(""),
				WS_VISIBLE | WS_SYSMENU | WS_VSCROLL /*| WS_HSCROLL*/ | ES_MULTILINE | ES_READONLY | ES_AUTOVSCROLL,
				5, 725, 600, 300,
				NULL, NULL, NULL, NULL
			);
			ShowWindow(s_hListBoxLog, SW_SHOW);
#endif//PLATFORM
			s_iLogStrings = 0;
			break;
		}

		default: {
			break;
		}
	}

	GLOG(("---------------------------------------\n"));
	GLOG((" Start Application \n"));
	GLOG(("---------------------------------------\n"));
}

//---------------------------------------------------------------------------//
//	_log
//---------------------------------------------------------------------------//
auto _log(char* fmt, ...) -> void {

#define MAX_LOG_SIZE	(1024)
	static char out[MAX_LOG_SIZE];
	va_list body;
	va_start(body, fmt);
	Vsnprintf(out, MAX_LOG_SIZE, fmt, body);
	va_end(body);

	if (s_hLogFile != nullptr) {
		fprintf(s_hLogFile, out);
		fflush(s_hLogFile);
	}

	// Send log message to log window...
	switch (s_iLogType) {

		case LOGCONSOLE: {
			printf("%s", out);
			break;
		}

		case LOGLISTBOX: {
			size_t i, iLen;
			iLen = strlen(out);
			for (i = 0; i < iLen; ++i) {
				if (out[i] == '\n' || out[i] == '\r') {
					out[i] = 0;
				}
			}

#if (PLATFORM == PLATFORM_WINDOWS)
			SendMessage(s_hListBoxLog, LB_ADDSTRING, 0, (LPARAM)out);
			SendMessage(s_hListBoxLog, LB_SETCURSEL, s_iLogStrings, 0);
			++s_iLogStrings;
#endif//PLATFORM
			break;
		}

		case LOGEDITBOX: {
#if (PLATFORM == PLATFORM_WINDOWS)
			int ndx = GetWindowTextLength(s_hListBoxLog);
			SendMessage(s_hListBoxLog, EM_SETSEL, (WPARAM)ndx, (LPARAM)ndx);
			SendMessage(s_hListBoxLog, EM_REPLACESEL, 0, (LPARAM)((LPSTR)out));
			++s_iLogStrings;
			SendMessage(s_hListBoxLog, EM_SCROLLCARET, 0, 0);
			// ebp-> Ouch!  This doesn't work, the Edit Control doesn't scroll.
#endif//PLATFORM
			break;
		}

		default: {
			break;
		}
	}
}

//---------------------------------------------------------------------------//
//	_logLargeMsg
//---------------------------------------------------------------------------//
auto _logLargeMsg(char* fmt, ...) -> void {

#define MAX_LARGE_LOG_SIZE		(8096)
	static char out[MAX_LARGE_LOG_SIZE];
	va_list body;
	va_start(body, fmt);
	Vsnprintf(out, MAX_LARGE_LOG_SIZE, fmt, body);
	va_end(body);

	if (s_hLogFile != nullptr) {
		fprintf(s_hLogFile, out);
		fflush(s_hLogFile);
	}

	// Send log message to log window...
	switch (s_iLogType) {

		case LOGCONSOLE: {
			printf("%s", out);
			break;
		}

		case LOGLISTBOX: {
			size_t i, iLen;
			iLen = strlen(out);
			for (i = 0; i < iLen; ++i) {
				if (out[i] == '\n' || out[i] == '\r') {
					out[i] = 0;
				}
			}

#if (PLATFORM == PLATFORM_WINDOWS)
			SendMessage(s_hListBoxLog, LB_ADDSTRING, 0, (LPARAM)out);
			SendMessage(s_hListBoxLog, LB_SETCURSEL, s_iLogStrings, 0);
			++s_iLogStrings;
#endif//PLATFORM
			break;
		}

		case LOGEDITBOX: {
#if (PLATFORM == PLATFORM_WINDOWS)
			int ndx = GetWindowTextLength(s_hListBoxLog);
			SendMessage(s_hListBoxLog, EM_SETSEL, (WPARAM)ndx, (LPARAM)ndx);
			SendMessage(s_hListBoxLog, EM_REPLACESEL, 0, (LPARAM)((LPSTR)out));
			++s_iLogStrings;
#endif//PLATFORM
			break;
		}

		default: {
			break;
		}
	}
}

//---------------------------------------------------------------------------//
//	_glogEnd
//---------------------------------------------------------------------------//
auto _glogEnd() -> void {

	GLOG(("---------------------------------------\n"));
	GLOG((" End Application\n"));
	GLOG(("---------------------------------------\n"));
	
	if (s_hLogFile) {
		fclose(s_hLogFile);
		s_hLogFile = nullptr;
	}

	switch (s_iLogType) {

		case LOGCONSOLE: {
			fclose(s_hConOut);
			fclose(s_hConIn);
			//fclose(s_hConErr);
			break;
		}

		case LOGLISTBOX: {
#if (PLATFORM == PLATFORM_WINDOWS)
			DestroyWindow(s_hListBoxLog);
#endif//PLATFORM
			break;
		}

		case LOGEDITBOX: {
#if (PLATFORM == PLATFORM_WINDOWS)
			DestroyWindow(s_hListBoxLog);
#endif//PLATFORM
			break;
		}

		default: {
			break;
		}
	}
}

#endif//LOG_ENABLED
