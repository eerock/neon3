//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_STR_H
#define NEON_STR_H

#include "Base.h"
#include <sstream>

// ebp-> todo: replace uses of this with a Strncmp where possible.
auto Stricmp(std::string const& p1, std::string const& p2) -> int;

#if (PLATFORM == PLATFORM_WINDOWS)
#define	Snprintf		sprintf_s
#define Sscanf			sscanf_s
#define Cstricmp		_stricmp
#define Vsnprintf		vsprintf_s
#define Strncpy			strncpy_s
#else
#define	Snprintf		snprintf
#define Sscanf			sscanf
#define Cstricmp		strcmp
#define Vsnprintf		vsnprintf
#define Strncpy			strncpy
#endif//PLATFORM

template <typename T>
inline auto to_string(T const& t) -> std::string {
	std::stringstream ss;
	ss << t;
	return ss.str();
}

template <typename T>
inline auto string_to(std::string const& str) -> T {
	T val;
	std::stringstream ss(str);
	ss >> val;
	return val;
}

// unused
auto SplitPath(std::string const& sStr, std::string& sDir, std::string& sFile) -> void;

auto PushDir(std::string const& sDir) -> void;
auto PopDir() -> void;

#endif//NEON_STR_H
