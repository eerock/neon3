//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_KEYBOARDDEVICE_H
#define NEON_KEYBOARDDEVICE_H

#include "Base.h"

class CKeyboardDevice {
public:
	enum EKeyModifiers {
		NONE = 0,
		SHIFT =	1 << 0,
		CTRL =	1 << 1,
		ALT = 1 << 2,
	};

	static auto Init () -> bool;
	static auto Update () -> void;
	static auto KeyPressed (int iKey) -> bool;
	static auto KeyPressedWithModifier (int iKey, int iMod) -> bool;
	static auto ModifierPressed (int iMod) -> bool;

private:
	static uchar s_modifier;
	static uchar s_aucKeyboardState[256];
};

#endif//NEON_KEYBOARDDEVICE_H
