//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FINDFILES_H
#define NEON_FINDFILES_H

#if (PLATFORM == PLATFORM_WINDOWS)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif//PLATFORM

struct TFindFileResult {
	bool bFirst;
#if (PLATFORM == PLATFORM_WINDOWS)
	WIN32_FIND_DATA tFindData;
	HANDLE hHandle;
#endif//PLATFORM
	std::string sFilename;
	bool bDir;
};

namespace neon {

auto FindFileInit(TFindFileResult& Result, std::string const& sWildcard) -> void;
auto FindFileNext(TFindFileResult& Result) -> bool;
auto FindFileClose(TFindFileResult& Result) -> void;

} //namespace neon

#endif//NEON_FINDFILES_H
