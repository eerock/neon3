//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_PLATFORM_H
#define NEON_PLATFORM_H

#ifndef PLATFORM

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define PLATFORM_WINDOWS			1
#if defined(WIN64) || defined(_WIN64)
#define PLATFORM_WINDOWS64			2
#endif
#define PLATFORM					PLATFORM_WINDOWS
#define PLATFORM_NAME				"Windows"
#define PLATFORM_PATH_SEPARATOR		"\\"
// ebp-> todo: further defines for specific OS version might be useful
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__CYGWIN__)
#define PLATFORM_LINUX				3
#define PLATFORM					PLATFORM_LINUX
#define PLATFORM_NAME				"Linux"
#define PLATFORM_PATH_SEPARATOR		"/"
#elif defined(__APPLE__) || defined(__MACH__)
#define PLATFORM_MAC				4
#define PLATFORM					PLATFORM_MAC
#define PLATFORM_NAME				"Mac"
#define PLATFORM_PATH_SEPARATOR		"/"
#else
#error Platform is not recognized!
#endif

#endif//!PLATFORM

#endif//NEON_PLATFORM_H
