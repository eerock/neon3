//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "KeyboardDevice.h"

uchar CKeyboardDevice::s_aucKeyboardState[256] = { 0 };
uchar CKeyboardDevice::s_modifier = 0;


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CKeyboardDevice::Init() -> bool {
	Update();
	return true;
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CKeyboardDevice::Update() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
	if (GetKeyboardState(s_aucKeyboardState)) {
		s_modifier = NONE;

		if (KeyPressed(VK_CONTROL)) {
			s_modifier |= CTRL;
		}
		if (KeyPressed(VK_MENU)) {
			s_modifier |= ALT;
		}
		if (KeyPressed(VK_SHIFT)) {
			s_modifier |= SHIFT;
		}
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	KeyPressed
//---------------------------------------------------------------------------//
auto CKeyboardDevice::KeyPressed(int iKey) -> bool {
	return (s_aucKeyboardState[iKey] & 0x80) != 0;
}


//---------------------------------------------------------------------------//
//	ModifierPressed
//---------------------------------------------------------------------------//
auto CKeyboardDevice::ModifierPressed(int iMod) -> bool {
	return (s_modifier & iMod) != 0;
}


//---------------------------------------------------------------------------//
//	KeyPressedWithModifier
//---------------------------------------------------------------------------//
auto CKeyboardDevice::KeyPressedWithModifier(int iKey, int iMod) -> bool {
	return KeyPressed(iKey) && ModifierPressed(iMod);
}
