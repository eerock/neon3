//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef __LISTA_H__
#define __LISTA_H__

#include "Mem.h"

template <class T> class CList;
template <class T> class CListNode;
template <class T> class CListIter;

//---[ List Node ]-----------------------------------------------------------//
template <class T>
class CListNode {
public:
	friend class CList<T>;
	friend class CListIter<T>;

	CListNode(T const& inObj) : m_Obj(inObj), m_pNext(nullptr), m_pPrev(nullptr) {}
	auto Get() const -> T const& { return m_Obj; }	// read-only
	auto Get() -> T& { return m_Obj; }				// read-write
	auto GetNextNode() const -> CListNode* { return m_pNext; }
	auto GetPrevNode() const -> CListNode* { return m_pPrev; }

protected:
	T m_Obj;
	CListNode<T>* m_pNext;
	CListNode<T>* m_pPrev;
};

//---[ List Iterator ]-------------------------------------------------------//
template <class T>
class CListIter {
public:
	CListIter() : m_pCurrentNode(nullptr) {}	// ebp-> todo: try forcing the pointer to actually point to something makes it more robust.
	CListIter(CListNode<T>* pStartNode) : m_pCurrentNode(pStartNode) {}
	auto operator++() -> CListIter<T>& {
		ASSERT(m_pCurrentNode != nullptr);
		m_pCurrentNode = m_pCurrentNode->m_pNext;
		return *this;
	}
	auto operator--() -> CListIter<T>& {
		ASSERT(m_pCurrentNode != nullptr);
		m_pCurrentNode = m_pCurrentNode->m_pPrev;
		return *this;
	}
	auto operator->() const -> T& {
		ASSERT(m_pCurrentNode != nullptr);
		return m_pCurrentNode->Get();
	}
	auto operator*() const -> T& {
		ASSERT(m_pCurrentNode != nullptr);
		return m_pCurrentNode->Get();
	}
	auto GetNode() const -> CListNode<T>* {
		return m_pCurrentNode;
	}
	auto Init(CList<T> const& inList) -> void {
		m_pCurrentNode = (CListNode<T>*)inList.Head();
	}

private:
	CListNode<T>* m_pCurrentNode;
};

//---[ List ]----------------------------------------------------------------//
template <class T>
class CList {
public:
	CList() : m_pHead(nullptr), m_pTail(nullptr), m_iSize(0) {}
	virtual~ CList() {
		Clear();
	}
	// Add/Remove methods
	auto Insert(T const& inObj) -> bool {
		return InsertTail(inObj);
	}
	auto InsertHead(T const& inObj) -> bool {
		auto pNewNode = NEW CListNode<T>(inObj);
		if (!pNewNode) {
			return false;
		}
		if (m_pHead) {
			pNewNode->m_pNext = m_pHead;
			m_pHead->m_pPrev = pNewNode;
			m_pHead = pNewNode;
		} else {
			m_pHead = m_pTail = pNewNode;
		}
		++m_iSize;
		return true;
	}
	auto InsertTail(T const& inObj) -> bool {
		// ebp-> a lot of time spent here, can we optimize?
		auto pNewNode = NEW CListNode<T>(inObj);
		if (!pNewNode) {
			return false;
		}
		if (m_pTail) {
			pNewNode->m_pPrev = m_pTail;
			m_pTail->m_pNext = pNewNode;
			m_pTail = pNewNode;
		} else {
			m_pHead = m_pTail = pNewNode;
		}
		++m_iSize;
		return true;
	}
	auto Head() const -> CListNode<T>* {
		return m_pHead;
	}
	auto Tail() const -> CListNode<T>* {
		return m_pTail;
	}
	auto Remove(T const& inObj) -> bool {
		auto pNode = m_pHead;
		while (pNode) {
			if (pNode->Get() == inObj) {
				RemoveNode(pNode);
				return true;
			}
			pNode = pNode->m_pNext;
		}
		return false;
	}
	auto RemoveNode(CListNode<T>* pNode) -> void {
		if (m_iSize == 1 && pNode == m_pHead) {
			Clear();
			return;
		}
		if (pNode == m_pHead) {
			m_pHead = m_pHead->m_pNext;
			m_pHead->m_pPrev = nullptr;
		} else if (pNode == m_pTail) {
			m_pTail = m_pTail->m_pPrev;
			m_pTail->m_pNext = nullptr;
		} else {
			pNode->m_pNext->m_pPrev = pNode->m_pPrev;
			pNode->m_pPrev->m_pNext = pNode->m_pNext;
		}
		DELETE_PTR(pNode);
		--m_iSize;
	}
	auto Clear() -> void {
		// ebp-> a lot of time spent here, can we optimize?
		while (m_pHead) {
			auto pNodeDel = m_pHead;
			m_pHead = m_pHead->m_pNext;
			DELETE_PTR(pNodeDel);
		}
		m_pHead = m_pTail = nullptr;
		m_iSize = 0;
	}
	auto ClearAndDeleteObjs() -> void {
		// ebp-> it's still strange to have to clean up like this,
		// because the objects are NEW'ed externally, why
		// is this List class semi-responsible for DELETE_PTR-ing
		// them?
		while (m_pHead) {
			auto pNodeDel = m_pHead;
			m_pHead = m_pHead->m_pNext;
			DELETE_PTR(pNodeDel->Get());
			DELETE_PTR(pNodeDel);
		}
		m_pHead = m_pTail = nullptr;
		m_iSize = 0;
	}
	auto Count() const -> int { return m_iSize; }
	auto IsEmpty() const -> bool { return m_iSize == 0; }
	auto Contains(T const& inObj) const -> bool {
		auto pNode = m_pHead;
		while (pNode) {
			if (pNode->Get() == inObj) {
				return true;
			}
			pNode = pNode->m_pNext;
		}
		return false;
	}

private:
	CListNode<T>* m_pHead;
	CListNode<T>* m_pTail;
	int m_iSize;
};

#endif//__LISTA_H__
