//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "FindFiles.h"


namespace neon {

//---------------------------------------------------------------------------//
// FindFileInit
// 
//---------------------------------------------------------------------------//
auto FindFileInit(TFindFileResult& Result, std::string const& sWildcard) -> void {
    // ebp-> todo: implement file handles for OSX
#if (PLATFORM == PLATFORM_WINDOWS)
	Result.hHandle = FindFirstFile(sWildcard.c_str(), &Result.tFindData);
#endif//PLATFORM
	Result.bFirst = true;
}


//---------------------------------------------------------------------------//
// FindFileNext
// 
//---------------------------------------------------------------------------//
auto FindFileNext(TFindFileResult& Result) -> bool {
	bool bRes = false;
    // ebp-> todo: implement find files for OSX
#if (PLATFORM == PLATFORM_WINDOWS)
	if (Result.hHandle != INVALID_HANDLE_VALUE) {
		if (Result.bFirst) {
			Result.bFirst = false;
			bRes = true;
		} else {
			bRes = FindNextFile(Result.hHandle, &Result.tFindData) != 0;
		}

		if (bRes) {
			Result.bDir = (Result.tFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
			Result.sFilename = Result.tFindData.cFileName;
		}
	}
#endif//PLATFORM
	return bRes;
}


//---------------------------------------------------------------------------//
// FindFileClose
// 
//---------------------------------------------------------------------------//
auto FindFileClose(TFindFileResult& Result) -> void {
    // ebp-> todo: implement file close for OSX
#if (PLATFORM == PLATFORM_WINDOWS)
	if (Result.hHandle != INVALID_HANDLE_VALUE) {
		FindClose(Result.hHandle);
	}
#endif//PLATFORM
}

} //namespace neon
