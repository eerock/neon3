//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "File.h"


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CFile::CFile()
	: m_file()
	, m_size(0)
	, m_flags(0)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CFile::~CFile() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CFile\n"));
#endif
	Close();
}

//---------------------------------------------------------------------------//
//	Open
//---------------------------------------------------------------------------//
auto CFile::Open(std::string const& sFile, std::string const& sAttribs) -> bool {
	if (!m_file.is_open()) {
		int mode = 0;
		if (sAttribs.find('r') != std::string::npos) {
			mode |= std::ios::in;
			m_flags |= READ;
		}
		if (sAttribs.find('w') != std::string::npos) {
			mode |= std::ios::out | std::ios::trunc;
			m_flags |= WRITE;
		}
		if (sAttribs.find('b') != std::string::npos) {
			mode |= std::ios::binary;
			m_flags |= BINARY;
		}

		if ((m_flags & READ) && (m_flags & WRITE)) {
			GLOG(("ERROR: CFile doesn't allow both 'r' and 'w' file modes.\n"));
			return false;
		}

		m_file.open(sFile.c_str(), mode);	// open the file
		if (m_file.is_open()) {
			Seek(0, END);
			m_size = Tell();
			Seek(0, BEGIN);
			return true;
		}
	}
	return false;
}


//---------------------------------------------------------------------------//
//	Close
//---------------------------------------------------------------------------//
auto CFile::Close() -> void {
	if (m_file.is_open()) {
		m_file.close();
		m_size = 0;
		m_flags = 0;
	}
}


//---------------------------------------------------------------------------//
//	Read
//---------------------------------------------------------------------------//
auto CFile::Read(void* pData, size_t size) -> size_t {
	ASSERT(m_file.is_open() && (m_flags & READ) && (m_flags & BINARY));
	m_file.read(static_cast<char*>(pData), size);
	return size;
}


//---------------------------------------------------------------------------//
//	ReadLn
//---------------------------------------------------------------------------//
auto CFile::ReadLn(std::string& sLine) -> int {
	ASSERT(m_file.is_open() && (m_flags & READ) && !(m_flags & BINARY));
	std::getline(m_file, sLine);
	return !Eof();
}


//---------------------------------------------------------------------------//
//	Write
//---------------------------------------------------------------------------//
auto CFile::Write(void const* pData, size_t size) -> size_t {
	ASSERT(m_file.is_open() && (m_flags & WRITE) && (m_flags & BINARY));
	m_file.write(static_cast<char const*>(pData), size);
	return size;
}


//---------------------------------------------------------------------------//
//	WriteLn
//---------------------------------------------------------------------------//
auto CFile::WriteLn(std::string const& sLine) -> int {
	ASSERT(m_file.is_open() && (m_flags & WRITE) && !(m_flags & BINARY));
	m_file << sLine << std::endl;
	return !Eof();
}

//---------------------------------------------------------------------------//
//	Write
//---------------------------------------------------------------------------//
auto CFile::Write(std::string const& sStr) -> int {
	ASSERT(m_file.is_open() && (m_flags & WRITE) && !(m_flags & BINARY));
	m_file << sStr;
	return !Eof();
}


//---------------------------------------------------------------------------//
//	GetPos
//---------------------------------------------------------------------------//
auto CFile::GetPos() -> int {
	if (m_flags & READ) {
		return (int)m_file.tellg();
	}
	else if (m_flags & WRITE) {
		return (int)m_file.tellp();
	}
	return 0;
}


//---------------------------------------------------------------------------//
//	Seek
//---------------------------------------------------------------------------//
auto CFile::Seek(int iOffset, int iMode) -> void {
	switch (iMode) {
	case BEGIN:
		if (m_flags & READ) {
			m_file.seekg(iOffset, std::ios::beg);
		}
		else if (m_flags & WRITE) {
			m_file.seekp(iOffset, std::ios::beg);
		}
		break;

	case CURRENT:
		if (m_flags & READ) {
			m_file.seekg(iOffset, std::ios::cur);
		}
		else if (m_flags & WRITE) {
			m_file.seekp(iOffset, std::ios::cur);
		}
		break;

	case END:
		if (m_flags & READ) {
			m_file.seekg(iOffset, std::ios::end);
		}
		else if (m_flags & WRITE) {
			m_file.seekp(iOffset, std::ios::end);
		}
		break;

	default:
		break;
	}
}


//---------------------------------------------------------------------------//
//	Tell
//---------------------------------------------------------------------------//
auto CFile::Tell() -> int {
	if (m_flags & READ) {
		return static_cast<int>(m_file.tellg());
	}
	else if (m_flags & WRITE) {
		return static_cast<int>(m_file.tellp());
	}
	else {
		return 0;
	}
}

//---------------------------------------------------------------------------//
//	Eof
//---------------------------------------------------------------------------//
auto CFile::Eof() -> bool {
	return m_file.eof();
}
