//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MEM_H
#define NEON_MEM_H

// MEMORY_MANAGER is used to override new and delete and provide custom functions.
// turn it off the define to go back to default memory routines.
//#define MEMORY_MANAGER

// VERBOSE_MEM_LOG when defined will log address, file, and line of any memory
// allocated or deallocated in the application code.  It is also used when
// printing the GUI hierarchy.
//#define VERBOSE_MEM_LOG

// DESTRUCTOR_VERBOSE is mine, prints out a log whenever a destructor
// is called, for tracking what got deleted, search for "~ClassName" in the log.
//#define DESTRUCTOR_VERBOSE

// Memory Manager
#ifdef MEMORY_MANAGER

auto MemoryManagerInit() -> void;
auto MemoryManagerEnd() -> void;
#define MEMORY_MANAGER_INIT()	MemoryManagerInit()
#define MEMORY_MANAGER_END()	MemoryManagerEnd()

void* __cdecl operator new(size_t, char const*, int);
void* __cdecl operator new[](size_t, char const*, int);
void  __cdecl operator delete(void*, char const*, int);
void  __cdecl operator delete[](void*, char const*, int);

#define NEW					new(__FILE__, __LINE__)
#define NEW_ARRAY(c, n)		new(__FILE__, __LINE__) c[(n)]

#ifdef VERBOSE_MEM_LOG

#define DELETE_PTR(p) { \
	GLOG(("%s(%d): delete 0x%08x...", __FILE__, __LINE__, (p))); \
	operator delete((p), __FILE__, __LINE__); \
	(p) = nullptr; \
	GLOG(("Done!\n")); \
}

#define DELETE_ARRAY(pa) { \
	GLOG(("%s(%d): delete [] 0x%08x...", __FILE__, __LINE__, (pa))); \
	operator delete[]((pa), __FILE__, __LINE__); \
	(pa) = nullptr; \
	GLOG(("Done!\n")); \
}

#else
//!defined VERBOSE_MEM_LOG

#define DELETE_PTR(p) { \
	operator delete((p), __FILE__, __LINE__); \
	(p) = nullptr; \
}

#define DELETE_ARRAY(pa) { \
	operator delete[]((pa), __FILE__, __LINE__); \
	(pa) = nullptr; \
}

#endif//VERBOSE_MEM_LOG

#else
//!defined MEMORY_MANAGER

// turn fancy stuff off
#define MEMORY_MANAGER_INIT()	
#define MEMORY_MANAGER_END()	

#define NEW						new
#define NEW_ARRAY(c, n)			new c[(n)]

#define DELETE_PTR(p)	{ \
	delete (p); \
	(p) = nullptr; \
}

#define DELETE_ARRAY(pa)	{ \
	delete [] (pa); \
	(pa) = nullptr; \
}

#endif//MEMORY_MANAGER

// this isn't used, but might be useful
#define DELETE_ARRAY_POINTERS(pa, num)	{ \
	if (pa) { \
		for (uint i = 0; i < (num); ++i) { \
			DELETE_PTR(pa[i]); \
		} \
		DELETE_ARRAY(pa); \
	} \
}

#endif//NEON_MEM_H
