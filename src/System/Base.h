//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_BASE_H
#define NEON_BASE_H

#include "platform.h"
#include "version.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers
#include <objbase.h>
#include <windows.h>
#endif

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <map>
#include <memory>
#include <new>
#include <sstream>
#include <stack>
#include <string>
#include <vector>

//---------------------------------------------------------------------------//
// Types
//---------------------------------------------------------------------------//
typedef unsigned char		uchar;
typedef unsigned short		ushort;
typedef unsigned int		uint;

#if (PLATFORM == PLATFORM_WINDOWS)
typedef unsigned __int64	uint64;
typedef HDC					THdc;
typedef HWND				THWnd;
typedef HINSTANCE			THInst;
typedef HCURSOR				THCursor;
#else
typedef unsigned long long	uint64;
typedef int*				THdc;
typedef int*				THWnd;
typedef int*				THInst;
typedef uint				THCursor;
#define UNREFERENCED_PARAMETER(x)	void(x)
#endif

const uint INVALID_ID = 0;
const int INVALID_INDEX = -1;

//---------------------------------------------------------------------------//
// Includes
//---------------------------------------------------------------------------//
#include "File.h"
#include "FindFiles.h"
#include "Log.h"
#include "Mem.h"
#include "NeonAssert.h"
#include "NeonMath.h"
#include "NodeFile.h"
#include "Str.h"
#include "Timer.h"
#include "Utils.h"

#endif//NEON_BASE_H
