//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_MIDIDEVICE_H
#define NEON_MIDIDEVICE_H

#include "Base.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(push)
#pragma warning(disable:4201)
#include <mmsystem.h>
#pragma warning(pop)
#endif//PLATFORM

// ebp-> todo: implement MIDI device for OSX
class IMidiDeviceService;

//---------------------------------------------------------------------------//
struct MIDIKey {
	uchar cMsg;
	uchar cValue;
};

//---------------------------------------------------------------------------//
class CMidiObserver {
public:
	virtual ~CMidiObserver() {}
	virtual auto Update(IMidiDeviceService*) -> void = 0;
};

//---------------------------------------------------------------------------//
class IMidiDeviceService {
	// ebp-> this section defines part of a Service Locator pattern,
	// although Observers may use it too.
public:
	virtual auto GetMIDIKey(uchar cNote) const -> MIDIKey const& = 0;
	virtual auto SelectDevice(int iDevice) -> void = 0;
	virtual auto SelectChannel(int iChannel) -> void = 0;

	// ebp-> this section implements an Observer pattern...
public:
	virtual auto GetLastMIDINote() const -> uchar const& = 0;

	virtual ~IMidiDeviceService() {
		m_vMidiObservers.clear();
	}

	auto AttachObserver(CMidiObserver* pObserver) -> void {
		m_vMidiObservers.push_back(pObserver);
	}

	auto DetachObserver(CMidiObserver* pObserver) -> void {
		for (auto it = m_vMidiObservers.begin(); it != m_vMidiObservers.end(); ++it) {
			if ((*it) == pObserver) {
				m_vMidiObservers.erase(it);
				return;
			}
		}
		// ebp-> can probably simplify it to this:
		//m_vMidiObservers.erase(std::find(m_vMidiObservers.begin(), m_vMidiObservers.end(), pObserver));
		//m_vMidiObservers.erase(std::remove(m_vMidiObservers.begin(), m_vMidiObservers.end(), pObserver), m_vMidiObservers.end());
	}

protected:
	std::vector<CMidiObserver*> m_vMidiObservers;

	auto NotifyObservers() -> void {
		for (auto& obs : m_vMidiObservers) {
			obs->Update(this);
		}
	}
};



//---------------------------------------------------------------------------//
class CMidiDevice : public IMidiDeviceService {
public:
	static auto EnumDevices(std::vector<std::string>& vDevices) -> void;

	CMidiDevice();
	virtual ~CMidiDevice();

	auto Init(int iDevice) -> bool;
	auto Release() -> void;

	auto SelectDevice(int iDevice) -> void;
	auto SelectChannel(int iChannel) -> void;

	virtual auto GetMIDIKey(uchar cNote) const -> MIDIKey const&;
	virtual auto GetLastMIDINote() const -> uchar const& { return m_cLastNote; }

	static std::string const DEVICE_UNKNOWN;

private:
	bool	m_bOk;
	MIDIKey	m_atNoteValues[256];
	uchar	m_cLastNote;

	int		m_iDeviceIndex;
	int		m_iChannel;

#if (PLATFORM == PLATFORM_WINDOWS)
	HMIDIIN	m_hMidiIn;
	static auto CALLBACK MidiDataCallback(HMIDIIN hMidiIn, UINT wMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2) -> void;
	auto OnMidiData(UINT wMsg, DWORD_PTR dwParam1, DWORD_PTR dwParam2) -> void;
#else
	//auto OnMidiData(uint uMsg, uint uParam1, uint uParam2) -> void;
#endif//PLATFORM
};

#endif//NEON_MIDIDEVICE_H
