//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_NEONMATH_H
#define NEON_NEONMATH_H

#include <cmath>

#define MATH_E			(2.71828182845904523536)
#define MATH_PI			(3.14159265358979323846)

#define IS_POW_2(X)		((X) != 0 && !((X) & ((X) - 1)))

//---------------------------------------------------------------------------//
inline auto Floor(double x) -> int {
#if (PLATFORM == PLATFORM_WINDOWS) && !defined(PLATFORM_WINDOWS64)
	int i;
	static const float f = -0.5f;
	__asm {
		fld    x
		fadd   st,st(0)
		fadd   f
		fistp  i
		sar    i,1
	};
	return (i);
#else
	return static_cast<int>(floor(x));
#endif
}

//---------------------------------------------------------------------------//
inline auto Ceil(double x) -> int {
#if (PLATFORM == PLATFORM_WINDOWS) && !defined(PLATFORM_WINDOWS64)
	int i;
	static const float f = -0.5f;
	__asm {
		fld    x
		fadd   st,st(0)
		fsubr  f
		fistp  i
		sar    i,1
	};
	return (-i);
#else
	return static_cast<int>(ceil(x));
#endif
}

//---------------------------------------------------------------------------//
inline auto Round(double x) -> int {
#if (PLATFORM == PLATFORM_WINDOWS) && !defined(PLATFORM_WINDOWS64)
	int i;
	static const float f = 0.5f;
	__asm {
		fld    x
			fadd   st,st(0)
			fadd   f
			fistp  i
			sar    i,1
	};
	return (i);
	//#elif (PLATFORM == PLATFORM_MAC)
	//	int i;
	//	static const float f = 0.5f;
	//	__asm__(
	//		"fld	x\n\t"
	//		"fadd	st,st(0)\n\t"
	//		"fadd	f\n\t"
	//		"fistp	i\n\t"
	//		"sar	i,1\n\t"
	//	);
	//	return (i);
#else
	return x < 0.0 ? Ceil(x - 0.5) : Floor(x + 0.5);
#endif
}


//---------------------------------------------------------------------------//
// CRollingMean
//
// calculates a rolling average over N samples
//---------------------------------------------------------------------------//
template <typename T, size_t N>
class CRollingMean {
public:
	CRollingMean() : m_sum(0), m_iLast(0) {}

	auto add(const T& val) -> void {
		if (m_iLast < N) {
			m_arr[m_iLast++] = val;
			m_sum += val;
		} else {
			T& last = m_arr[m_iLast++ % N];
			m_sum += val - last;
			last = val;
		}
	}

	auto get() const -> T {
		if (m_iLast < N) {
			return m_sum / m_iLast;
		}
		return m_sum / N;
	}

private:
	T m_arr[N];
	T m_sum;
	int m_iLast;
};

#endif//NEON_NEONMATH_H
