//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_NEONASSERT_H
#define NEON_NEONASSERT_H

#if defined(_DEBUG)

#include <cassert>

auto LogAssertFailure(char const*, char const*, uint) -> void;
auto LogAssertFailure(char const*, char const*, char const*, uint) -> void;

#ifndef ASSERT
#define ASSERT(exp)			(void)((exp) || (LogAssertFailure(#exp, __FILE__, __LINE__), 0)); assert(exp)
#endif
#ifndef ASSERTM
#define ASSERTM(exp, msg)	(void)((exp) || (LogAssertFailure(#exp, msg, __FILE__, __LINE__), 0)); assert(exp)
#endif

#else

#ifndef ASSERT
#define ASSERT(exp)			
#endif
#ifndef ASSERTM
#define ASSERTM(exp, msg)	
#endif
#define NASSERT

#endif//_DEBUG

#endif//NEON_NEONASSERT_H
