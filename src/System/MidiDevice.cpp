//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "MidiDevice.h"

#define MIDI_ERROR_BUF_SIZE		(64)

static char s_acMidiError[MIDI_ERROR_BUF_SIZE];
static MMRESULT s_iMidiErr = 0;

IMidiDeviceService* CServiceLocator<IMidiDeviceService>::s_pService = nullptr;

std::string const CMidiDevice::DEVICE_UNKNOWN = "No MIDI Device";


//---------------------------------------------------------------------------//
//	STATIC: EnumDevices
//---------------------------------------------------------------------------//
auto CMidiDevice::EnumDevices(std::vector<std::string>& vDevices) -> void {
	vDevices.clear();

#if (PLATFORM == PLATFORM_WINDOWS)
	MIDIINCAPS midiCaps;
	int iNumDevices = midiInGetNumDevs();
	for (int i = 0; i < iNumDevices; ++i) {
		s_iMidiErr = midiInGetDevCaps(i, &midiCaps, sizeof(MIDIINCAPS));
		if (s_iMidiErr != MMSYSERR_NOERROR) {
			midiInGetErrorText(s_iMidiErr, s_acMidiError, MIDI_ERROR_BUF_SIZE);
			GLOG(("MIDI ERROR: %s\n", s_acMidiError));
			continue;
		}
		vDevices.push_back(midiCaps.szPname);
	}
#endif//PLATFORM
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CMidiDevice::CMidiDevice()
	: m_bOk(false)
	, m_iDeviceIndex(-1)
	, m_iChannel(0)
{}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CMidiDevice::~CMidiDevice() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CMidiDevice\n"));
#endif
	Release();
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CMidiDevice::Init(int iDevice) -> bool {
	m_bOk = false;
	ASSERT(iDevice >= 0);

	m_iDeviceIndex = -1;

	memset(m_atNoteValues, 0, sizeof(MIDIKey) * 256);

#if (PLATFORM == PLATFORM_WINDOWS)
	s_iMidiErr = midiInOpen(&m_hMidiIn, iDevice, (DWORD_PTR)MidiDataCallback, (DWORD_PTR)this, CALLBACK_FUNCTION);
	if (s_iMidiErr == MMSYSERR_NOERROR) {
		s_iMidiErr = midiInStart(m_hMidiIn);
		if (s_iMidiErr == MMSYSERR_NOERROR) {
			m_iDeviceIndex = iDevice;
			m_bOk = true;
			return m_bOk;
		} else {
			midiInGetErrorText(s_iMidiErr, s_acMidiError, MIDI_ERROR_BUF_SIZE);
			GLOG(("MIDI ERROR: %s\n", s_acMidiError));

			s_iMidiErr = midiInClose(m_hMidiIn);
			if (s_iMidiErr != MMSYSERR_NOERROR) {
				midiInGetErrorText(s_iMidiErr, s_acMidiError, MIDI_ERROR_BUF_SIZE);
				GLOG(("MIDI ERROR: %s\n", s_acMidiError));
			}
		}
	} else {
		midiInGetErrorText(s_iMidiErr, s_acMidiError, MIDI_ERROR_BUF_SIZE);
		GLOG(("MIDI ERROR: %s\n", s_acMidiError));
	}
#endif//PLATFORM

	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Release
//---------------------------------------------------------------------------//
auto CMidiDevice::Release() -> void {
	if (m_bOk) {
#if (PLATFORM == PLATFORM_WINDOWS)
		s_iMidiErr = midiInStop(m_hMidiIn);
		if (s_iMidiErr != MMSYSERR_NOERROR) {
			midiInGetErrorText(s_iMidiErr, s_acMidiError, MIDI_ERROR_BUF_SIZE);
			GLOG(("MIDI ERROR: %s\n", s_acMidiError));
		}

		s_iMidiErr = midiInClose(m_hMidiIn);
		if (s_iMidiErr != MMSYSERR_NOERROR) {
			midiInGetErrorText(s_iMidiErr, s_acMidiError, MIDI_ERROR_BUF_SIZE);
			GLOG(("MIDI ERROR: %s\n", s_acMidiError));
		}
#endif//PLATFORM
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	SelectDevice
//---------------------------------------------------------------------------//
auto CMidiDevice::SelectDevice(int iDevice) -> void {
	ASSERT(iDevice >= 0);
	if (m_iDeviceIndex != iDevice) {
		Release();
		Init(iDevice);
	}
}


//---------------------------------------------------------------------------//
//	SelectChannel
//---------------------------------------------------------------------------//
auto CMidiDevice::SelectChannel(int iChannel) -> void {
	ASSERT(ValidIndex(iChannel, 16));
	m_iChannel = iChannel;
}


//---------------------------------------------------------------------------//
//	GetMIDIKey
//---------------------------------------------------------------------------//
auto CMidiDevice::GetMIDIKey(uchar cNote) const -> MIDIKey const& {
	return m_atNoteValues[cNote];
}


//---------------------------------------------------------------------------//
//	STATIC: Midi Data Callback
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CALLBACK CMidiDevice::MidiDataCallback(HMIDIIN hMidiIn, UINT wMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2) -> void {
	auto pMidiDevice = (CMidiDevice*)dwInstance;
	if (pMidiDevice && pMidiDevice->m_bOk && pMidiDevice->m_hMidiIn == hMidiIn) {
		pMidiDevice->OnMidiData(wMsg, dwParam1, dwParam2);
	}
}
#endif//PLATFORM


//---------------------------------------------------------------------------//
//	OnMidiData
//	The data seems to be as follows:
//	wMsg:		Always comes through as 963, or b1111000011 (Ignored for now)
//	dwParam1:	MIDI Key
//	dwParam2:	Elapsed Time (in milliseconds)
//---------------------------------------------------------------------------//
#if (PLATFORM == PLATFORM_WINDOWS)
auto CMidiDevice::OnMidiData(UINT wMsg, DWORD_PTR dwMidiMessage, DWORD_PTR dwTimestamp) -> void {
	UNREFERENCED_PARAMETER(wMsg);
	UNREFERENCED_PARAMETER(dwTimestamp);

	// reference of MIDI status messages:
	// http://www.midimountain.com/midi/midi_status.htm

	if (dwMidiMessage == 248) {
		// ebp-> filter out an 'always sent' MIDI message...
		// is this clock data?
		return;
	}

	uchar cValue = (dwMidiMessage >> 16) & 0xff;
	uchar cNote = (dwMidiMessage >> 8) & 0xff;
	uchar cMsg = (dwMidiMessage & 0xff);

	m_atNoteValues[cNote].cMsg = cMsg;
	m_atNoteValues[cNote].cValue = cValue;

	m_cLastNote = cNote;

	NotifyObservers();

	// ebp-> todo: look at MIDI specs, keep this MIDI data as virgin data,
	// and let clients of this code read and interpret it how they wish.

	GLOG(("MIDI: timestamp = %f, Msg = %02X, Note = %02X, Value = %02X (%d)\n", 
		static_cast<float>(dwTimestamp) * 0.001f, cMsg, cNote, cValue, cValue));
}
#endif//PLATFORM
