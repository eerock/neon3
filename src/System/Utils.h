//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_UTILS_H
#define NEON_UTILS_H

#include <memory>

// http://stackoverflow.com/questions/1008019/c-singleton-design-pattern/1008289#1008289
//class Singleton {
//public:
//	static Singleton& getInstance() {
//		static Singleton instance; // Guaranteed to be destroyed.
//		// Instantiated on first use.
//		return instance;
//	}
//private:
//	Singleton() {}	// Constructor? (the {} brackets) are needed here.
//	// Dont forget to declare these two. You want to make sure they
//	// are unaccessable otherwise you may accidently get copies of
//	// your singleton appearing.
//	Singleton(Singleton const&);		// Don't Implement
//	void operator=(Singleton const&);	// Don't implement
//};


//---------------------------------------------------------------------------//
// Generic Service Locator Pattern
//---------------------------------------------------------------------------//
// todo(C++11)-> make this use smart pointers properly!
template <class T>
class CServiceLocator {
public:
	static T* GetService() {
		return s_pService;
	}
	static void RegisterService(T* pService) {
		if (pService != nullptr) {
			s_pService = pService;
		} else {
			DELETE_PTR(s_pService);
		}
	}
private:
	static T* s_pService;
};


//template <class T>
//class CServiceLocator2 {
//public:
//	static T& GetService() {
//		ASSERT(s_pService != nullptr);
//		return *(s_pService);
//	}
//	// clients should do this:
//	// auto pSkin = std::unique_ptr<MGSkin>(NEW MGSkin);
//	// CServiceLocator<IAppSkinService>::RegisterService(std::move(pSkin));	// invalidates pSkin!
//	static void RegisterService(std::unique_ptr<T> pService) {
//		s_pService = pService;
//	}
//private:
//	static std::unique_ptr<T> s_pService;
//};


//template <class T>
//class CServiceLocator3 {
//public:
//	static std::unique_ptr<T> const& GetService() {
//		ASSERT(s_pService != nullptr);
//		return s_pService;
//	}
//	static void RegisterService(std::unique_ptr<T> pService) {
//		s_pService = pService;
//	}
//private:
//	static std::unique_ptr<T> s_pService;
//};



//---------------------------------------------------------------------------//
// Application Options Service Interface
//---------------------------------------------------------------------------//
class IAppOptionsService {
public:
	virtual ~IAppOptionsService() {}
	virtual auto Set(std::string const&, int const&) -> void = 0;
	virtual auto Set(std::string const&, std::string const&) -> void = 0;
	virtual auto Set(std::string const&, bool const&) -> void = 0;
	virtual auto Get(std::string const&, int const&) -> int = 0;
	virtual auto Get(std::string const&, std::string const&) -> std::string = 0;
	virtual auto Get(std::string const&, bool const&) -> bool = 0;
	virtual auto Load(std::string const&) -> bool = 0;
	virtual auto Save(std::string const& sName = "") -> void = 0;
};

//---------------------------------------------------------------------------//
struct TRect {
	TRect() : x(0), y(0), w(0), h(0) {}
	TRect(int _x, int _y, int _w, int _h)
		: x(_x), y(_y), w(_w), h(_h)
	{}
	int x, y, w, h;
};

//---------------------------------------------------------------------------//
inline auto SetRect(TRect& Rect, int _x, int _y, int _w, int _h) -> void {
	Rect.x = _x;
	Rect.y = _y;
	Rect.w = _w;
	Rect.h = _h;
}

//---------------------------------------------------------------------------//
inline auto ShrinkRect(TRect& Rect, int numPixels) -> void {
	Rect.x += numPixels;
	Rect.y += numPixels;
	Rect.w -= numPixels << 1;
	Rect.h -= numPixels << 1;
}

//---------------------------------------------------------------------------//
inline auto GrowRect(TRect& Rect, int numPixels) -> void {
	Rect.x -= numPixels;
	Rect.y -= numPixels;
	Rect.w += numPixels << 1;
	Rect.h += numPixels << 1;
}

//---------------------------------------------------------------------------//
template <typename T>
inline auto Min(T a, T b) -> T {
	return a < b ? a : b;
}

//---------------------------------------------------------------------------//
template <typename T>
inline auto Max(T a, T b) -> T {
	return b < a ? a : b;
}

//---------------------------------------------------------------------------//
template <typename T>
inline auto Clamp(T v, T a, T b) -> T {
	return (v < a ? a : (b < v ? b : v));
}

//---------------------------------------------------------------------------//
template <typename T>
inline auto Swap(T& x, T& y) -> void {
	T tmp = x;
	x = y;
	y = tmp;
}

//---------------------------------------------------------------------------//
template <typename T>
inline auto ValidIndex(T const& v, T const& size) -> bool {
	return (0 <= v && v < size);
}

#endif//NEON_UTILS_H
