//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "OscDevice.h"

#if defined(USE_OSC)

static int OSCPort = 7000;
static std::string s_sOSCHost("localhost");


//---------------------------------------------------------------------------//
//	TestSendOSC
//---------------------------------------------------------------------------//
auto TestSendOSC() -> void {
#define MTU_SIZE	(1600)
	char buffer[MTU_SIZE];
	osc::OutboundPacketStream p(buffer, MTU_SIZE);

	IpEndpointName OSCConnection(s_sOSCHost.c_str(), OSCPort);
	UdpTransmitSocket socket(OSCConnection);

	p.Clear();
	p << osc::BeginBundle();
	p << osc::BeginMessage("/osc/float/") << 5.f << osc::EndMessage;
	p << osc::EndBundle;
	socket.Send(p.Data(), p.Size());
}


//---------------------------------------------------------------------------//
//	TestReceiveOSC
//---------------------------------------------------------------------------//
auto TestReceiveOSC() -> void {
	OSCPacketListener listener;
	UdpListeningReceiveSocket socket(IpEndpointName(IpEndpointName::ANY_ADDRESS, OSCPort), &listener);
	socket.Run();
}

#endif//USE_OSC
