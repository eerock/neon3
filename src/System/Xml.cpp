//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "Xml.h"

namespace neon {

//---------------------------------------------------------------------------//
//	SafeString
//---------------------------------------------------------------------------//
auto SafeString(TiXmlElement* pElem, char const* pChild, char const* pDefault) -> char const* {
	auto pData = pElem->FirstChild(pChild);
	return ((pData && pData->FirstChild()) ? pData->FirstChild()->Value() : pDefault);
}

//---------------------------------------------------------------------------//
// GetNumSameChilds
//---------------------------------------------------------------------------//
auto GetNumSameChilds(TiXmlNode* pNode, char const* pChild) -> int {
	int iNum = 0;
	TiXmlNode* pIter = nullptr;
	while ((pIter = pNode->IterateChildren(pChild, pIter)) != nullptr)	{
		++iNum;
	}
	return iNum;
}

} //namespace neon
