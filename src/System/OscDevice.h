//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_OSCDEVICE_H
#define NEON_OSCDEVICE_H

#if (PLATFORM == PLATFORM_WINDOWS)	// && !defined(PLATFORM_WINDOWS64)
#define USE_OSC
#endif

#if defined(USE_OSC)

#include "Base.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(push)
#pragma warning(disable:4100)
#endif//PLATFORM_WINDOWS

#include "osc/OscOutboundPacketStream.h"
#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"
#include "ip/IpEndpointName.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(pop)
#endif//PLATFORM_WINDOWS


//---------------------------------------------------------------------------//
//	Class OSCPacketListener
//---------------------------------------------------------------------------//
class OSCPacketListener
	: public osc::OscPacketListener
{
protected:
	virtual auto ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint) -> void {
		UNREFERENCED_PARAMETER(remoteEndpoint);
		try {
			if (Stricmp(std::string(m.AddressPattern()), std::string("/test1")) == 0) {
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				float flt;
				args >> flt >> osc::EndMessage;
			}
		}
		catch (osc::Exception& e) {
			UNREFERENCED_PARAMETER(e);
			GLOG(("ERROR: error while parsing OSC message: %s : %s\n", m.AddressPattern(), e.what()));
		}
	}
};


//---------------------------------------------------------------------------//
auto TestSendOSC() -> void;
auto TestReceiveOSC() -> void;

#endif//USE_OSC

#endif//NEON_OSCDEVICE_H
