//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "Sound.h"



//---------------------------------------------------------------------------//
//
//	P O R T A U D I O   I M P L E M E N T A T I O N
//
//---------------------------------------------------------------------------//

#ifdef USE_PORTAUDIO_SOUND

#define SAMPLE_RATE			(44100)
#define FRAMES_PER_BUFFER	(512)

static int numCallbacksCalled = 0;


//---------------------------------------------------------------------------//
//	four1 - Cooley Tukey FFT
//---------------------------------------------------------------------------//
void four1(SAMPLE* data, unsigned long nn) {
	unsigned long n, mmax, m, j, istep, i;
	SAMPLE wtemp, wr, wpr, wpi, wi, theta;
	SAMPLE tempr, tempi;

	// reverse-binary reindexing
	n = nn << 1;
	j = 1;
	for (i = 1; i < n; i += 2) {
		if (j > i) {
			Swap(data[j - 1], data[i - 1]);
			Swap(data[j], data[i]);
		}
		m = nn;
		while (m >= 2 && j > m) {
			j -= m;
			m >>= 1;
		}
		j += m;
	};

	// here begins the Danielson-Lanczos section
	mmax = 2;
	while (n > mmax) {
		istep = mmax << 1;
		theta = static_cast<SAMPLE>(-2.0 * MATH_PI / mmax);
		wtemp = static_cast<SAMPLE>(sin(0.5 * theta));
		wpr = static_cast<SAMPLE>(-2.0 * wtemp * wtemp);
		wpi = static_cast<SAMPLE>(sin(theta));
		wr = 1.0;
		wi = 0.0;
		for (m = 1; m < mmax; m += 2) {
			for (i = m; i <= n; i += istep) {
				j = i + mmax;
				tempr = wr * data[j - 1] - wi * data[j];
				tempi = wr * data[j] + wi * data[j - 1];

				data[j - 1] = data[i - 1] - tempr;	// double -> float truncation
				data[j] = data[i] - tempi;			// double -> float truncation
				data[i - 1] += tempr;				// double -> float truncation
				data[i] += tempi;					// double -> float truncation
			}
			wtemp = wr;
			wr += wr * wpr - wi * wpi;
			wi += wi * wpr + wtemp * wpi;
		}
		mmax = istep;
	}
}


//---------------------------------------------------------------------------//
//	paRecCallback
//---------------------------------------------------------------------------//
static auto paRecCallback(
	void const* input,
	void* output,
	unsigned long framesPerBuffer,
	PaStreamCallbackTimeInfo const* timeInfo,
	PaStreamCallbackFlags statusFlags,
	void* userData
	) -> int {

	UNREFERENCED_PARAMETER(output);
	UNREFERENCED_PARAMETER(timeInfo);
	UNREFERENCED_PARAMETER(statusFlags);

	// cast to our recorder data type...
	auto pRecData = reinterpret_cast<paRecData*>(userData);
	auto pRead = reinterpret_cast<REC_SAMPLE const*>(input);
	auto pWrite = &pRecData->recordedSamples[0];
	auto nSamples = framesPerBuffer * pRecData->channels;

	// write the incoming data to the buffer in our structure...
	if (input != nullptr && !pRecData->bPaused) {
		for (unsigned long i = 0; i < nSamples; ++i) {
			*pWrite++ = *pRead++;
		}
	}
	else {
		for (unsigned long i = 0; i < nSamples; ++i) {
			*pWrite++ = 0;
		}
	}

	pRecData->bFreshData = true;

	++numCallbacksCalled;

	return paContinue;
}


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CSound::CSound(THWnd hWnd)
	: m_bOk(false)
	, m_bPlaying(false)
	, m_bRecording(false)
	, m_iFFTSize(FFT_SIZE_512)
	, m_tRecParameters()
	, m_pRecStream(nullptr)
	, m_window()
{
	UNREFERENCED_PARAMETER(hWnd);
	PaError err = Pa_Initialize();
	if (err != paNoError) {
		GLOG(("ERROR: Portaudio failed to initialize: %s\n", Pa_GetErrorText(err)));
		return;
	}

	// enumerate audio input/output devices...
	std::vector<std::string> vDevices;
	EnumerateDevices(vDevices);
	GLOG(("Here's the result of enumerating audio devices...\n"));
	for (auto& dev : vDevices) {
		GLOG(("%s\n", dev.c_str()));
	}
	
#define CHANNELS_MONO	(1)
#define CHANNELS_STEREO	(2)
	const int nChannels = CHANNELS_STEREO;

	// Recording setup...
	m_tRecParameters.channelCount = nChannels;
	m_tRecParameters.sampleFormat = paFloat32;
	m_tRecParameters.device = Pa_GetDefaultInputDevice();
	if (m_tRecParameters.device == paNoDevice) {
		GLOG(("WARNING: Portaudio failed to find a default recording device.\n"));
		return;
	}
	m_tRecParameters.suggestedLatency =
		Pa_GetDeviceInfo(m_tRecParameters.device)->defaultLowInputLatency;
	m_tRecParameters.hostApiSpecificStreamInfo = nullptr;

	// allocate the buffer of samples (what about double buffering the data?)
	m_tRecData.maxFrameIndex = FRAMES_PER_BUFFER; // 5 * 44100; // seconds * sample_rate
	int numSamples = m_tRecData.maxFrameIndex * nChannels;
	m_tRecData.recordedSamples = NEW_ARRAY(REC_SAMPLE, numSamples);
	m_tRecData.channels = nChannels;
	m_tRecData.bFreshData = false;
	m_tRecData.bPaused = false;

	SetFFTSize(m_iFFTSize);

	m_bOk = true;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CSound::~CSound() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CSound\n"));
#endif//DESTRUCTOR_VERBOSE
	if (m_bOk) {

		Stop();

		PaError err = Pa_CloseStream(m_pRecStream);
		if (err != paNoError) {
			GLOG(("ERROR: Portaudio failed to close Rec stream: %s\n", Pa_GetErrorText(err)));
		}
		m_pRecStream = nullptr;
		
		err = Pa_Terminate();
		if (err != paNoError) {
			GLOG(("ERROR: Portaudio failed to terminate: %s\n", Pa_GetErrorText(err)));
		}

		// delete the recording buffer...
		DELETE_ARRAY(m_tRecData.recordedSamples);

		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	Load
//	loads an audio file for playback.
//---------------------------------------------------------------------------//
auto CSound::Load(std::string const& sFile) -> void {
	UNREFERENCED_PARAMETER(sFile);
}


//---------------------------------------------------------------------------//
//	Update
//	todo: need to ensure m_iFFTSize is <= to the frames per buffer size.
//---------------------------------------------------------------------------//
auto CSound::Update() -> void {
	if (m_bOk && m_tRecData.bFreshData) {
		// use precalculated window function to reduce FFT spectral leakage.
		for (int i = 0; i < m_iFFTSize; ++i) {
			m_fft_data[2 * i] = m_tRecData.recordedSamples[i * m_tRecData.channels] * m_window[i];
			m_fft_data[2 * i + 1] = 0;
		}

		four1(m_fft_data, m_iFFTSize);

		m_tRecData.bFreshData = false;
	}
}


//---------------------------------------------------------------------------//
//	Record
//---------------------------------------------------------------------------//
auto CSound::Record() -> void {
	PaError err = Pa_OpenStream(
		&m_pRecStream,
		&m_tRecParameters,
		nullptr,
		SAMPLE_RATE,
		FRAMES_PER_BUFFER,
		paClipOff,
		paRecCallback,
		&m_tRecData);

	if (err != paNoError) {
		GLOG(("ERROR: Portaudio failed to create Rec stream: %s\n", Pa_GetErrorText(err)));
		return;
	}

	err = Pa_StartStream(m_pRecStream);
	if (err != paNoError) {
		GLOG(("ERROR: Portaudio failed to start the Rec stream: %s\n", Pa_GetErrorText(err)));
		return;
	}

	m_bRecording = true;
}


//---------------------------------------------------------------------------//
//	Play
//---------------------------------------------------------------------------//
auto CSound::Play(float fStart) -> void {
	UNREFERENCED_PARAMETER(fStart);
}


//---------------------------------------------------------------------------//
//	Stop
//---------------------------------------------------------------------------//
auto CSound::Stop() -> void {
	PaError err = Pa_IsStreamActive(m_pRecStream);
	if (err == 1) {
		err = Pa_StopStream(m_pRecStream);
		if (err != paNoError) {
			GLOG(("ERROR: Portaudio failed to stop the Rec stream: %s\n", Pa_GetErrorText(err)));
		}
		m_bRecording = false;
	}
}


//---------------------------------------------------------------------------//
//	Pause
//---------------------------------------------------------------------------//
auto CSound::Pause() -> void {
	m_tRecData.bPaused = true;
}


//---------------------------------------------------------------------------//
//	Resume
//---------------------------------------------------------------------------//
auto CSound::Resume() -> void {
	m_tRecData.bPaused = false;
}


//---------------------------------------------------------------------------//
//	SetFFTSize
//---------------------------------------------------------------------------//
auto CSound::SetFFTSize(int iSize) -> void {
	// assert iSize is power of 2
	ASSERT(IS_POW_2(iSize));
	if (iSize > 0 && iSize != m_window.size()) {
		m_iFFTSize = iSize;
		m_window.resize(m_iFFTSize);


		// todo: allow selection of different window types, when it gets changed
		// recalculate the window function.  Have a separate call to change
		// the window function which this function will call automatically when
		// the fftsize changes.  The window type will stay the same but since
		// it was resized, it should be recalculated.
		//
		// hanning window(n) = 0.5 * (1 - cos(2*PI*n/(N-1)))
		// 

		// just do Hanning window for now...
		double const d = 2.0 * MATH_PI / (m_iFFTSize - 1);
		for (int i = 0; i < m_window.size(); ++i) {
			m_window[i] = static_cast<SAMPLE>(0.5 * (1.0 - cos(d * i)));
		}
	}
}


//---------------------------------------------------------------------------//
//	GetFFT
//	write FFT data into pFFT of length iSize.
//---------------------------------------------------------------------------//
auto CSound::GetFFT(float* pFFT, int iSize, float fGain) -> bool {
	for (int i = 0; i < iSize; ++i) {
		pFFT[i] = fGain * sqrtf(m_fft_data[2 * i] * m_fft_data[2 * i] + m_fft_data[2 * i + 1] * m_fft_data[2 * i + 1]);
	}
	return true;
}


//---------------------------------------------------------------------------//
//	EnumerateDevices
//---------------------------------------------------------------------------//
auto CSound::EnumerateDevices(std::vector<std::string>& vDevices) -> void {
	vDevices.clear();

	int nNumDevices = 0;
	PaDeviceInfo const* pDeviceInfo;

	nNumDevices = Pa_GetDeviceCount();
	if (nNumDevices < 0) {
		GLOG(("ERROR: Portaudio failed to enumerate any audio devices!\n"));
		return;
	}

	for (int i = 0; i < nNumDevices; ++i) {
		GLOG(("\n"));
		int defaultFound = 0;
		pDeviceInfo = Pa_GetDeviceInfo(i);
		if (i == Pa_GetDefaultInputDevice()) {
			GLOG(("[ Default Input "));
			defaultFound = 1;
		}
		else if (i == Pa_GetHostApiInfo(pDeviceInfo->hostApi)->defaultInputDevice) {
			GLOG(("[ Default %s Input", Pa_GetHostApiInfo(pDeviceInfo->hostApi)->name));
			defaultFound = 1;
		}

		if (i == Pa_GetDefaultOutputDevice()) {
			GLOG((defaultFound ? "," : "["));
			GLOG((" Default Output "));
			defaultFound = 1;
		}
		else if (i == Pa_GetHostApiInfo(pDeviceInfo->hostApi)->defaultOutputDevice) {
			GLOG((defaultFound ? "," : "["));
			GLOG((" Default %s Output ", Pa_GetHostApiInfo(pDeviceInfo->hostApi)->name));
			defaultFound = 1;
		}

		if (defaultFound) {
			GLOG(("]:\n"));
		}

		GLOG(("Device Name: %s\n", pDeviceInfo->name));
		GLOG(("Host API: %s\n", Pa_GetHostApiInfo(pDeviceInfo->hostApi)->name));
		GLOG(("Inputs: %d    Outputs: %d\n", pDeviceInfo->maxInputChannels, pDeviceInfo->maxOutputChannels));
		GLOG(("Default Sample Rate: %f\n", pDeviceInfo->defaultSampleRate));
		vDevices.push_back(pDeviceInfo->name);
	}
	GLOG(("\n"));
}

#endif//USE_PORTAUDIO_SOUND
