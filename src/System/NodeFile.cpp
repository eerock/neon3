//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"


//---------------------------------------------------------------------------//
//	GetNumNodes
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::GetNumNodes(std::string const& sName) -> int {
	int iNum = 0;
	TiXmlNode* pIter = nullptr;
	if (sName.empty()) {
		while ((pIter = IterateChildren(pIter)) != nullptr) {
			++iNum;
		}
	} else {
		while ((pIter = IterateChildren(sName.c_str(), pIter)) != nullptr) {
			++iNum;
		}
	}
	return iNum;
}


//---------------------------------------------------------------------------//
//	FirstNode
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::FirstNode(std::string const& sName) -> CNode* {
	if (sName.empty()) {
		return static_cast<CNode*>(FirstChild());
	} else {
		return static_cast<CNode*>(FirstChild(sName.c_str()));
	}
}


//---------------------------------------------------------------------------//
//	NextNode
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::NextNode(std::string const& sName) -> CNode* {
	if (sName.empty()) {
		return static_cast<CNode*>(NextSibling());
	} else {
		return static_cast<CNode*>(NextSibling(sName.c_str()));
	}
}


//---------------------------------------------------------------------------//
//	AsString
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AsString(std::string const& sName, std::string const& sDefault) -> std::string {
	auto pNode = FirstNode(sName);
	if (pNode) {
		pNode = pNode->FirstNode();
		if (pNode && pNode->Value()) {
			return pNode->Value();
		}
	}
	return sDefault;
}


//---------------------------------------------------------------------------//
//	AsInt
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AsInt(std::string const& sName, int iDefault) -> int {
	auto pNode = FirstNode(sName);
	if (pNode) {
		pNode = pNode->FirstNode();
		if (pNode && pNode->Value()) {
			return string_to<int>(pNode->Value());
		}
	}
	return iDefault;
}


//---------------------------------------------------------------------------//
//	AsFloat
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AsFloat(std::string const& sName, float fDefault) -> float {
	auto pNode = FirstNode(sName);
	if (pNode) {
		pNode = pNode->FirstNode();
		if (pNode && pNode->Value()) {
			return string_to<float>(pNode->Value());
		}
	}
	return fDefault;
}


//---------------------------------------------------------------------------//
//	AsBool
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AsBool(std::string const& sName, bool bDefault) -> bool {
	auto pNode = FirstNode(sName);
	if (pNode) {
		pNode = pNode->FirstNode();
		if (pNode && pNode->Value()) {
			return string_to<bool>(pNode->Value());
		}
	}
	return bDefault;
}


//---------------------------------------------------------------------------//
//	AttrAsString
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AttrAsString(std::string const& sName, std::string const& sDefault) -> std::string {
	auto pData = ToElement()->Attribute(sName.c_str());
	return (pData ? pData : sDefault);
}


//---------------------------------------------------------------------------//
//	AttrAsInt
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AttrAsInt(std::string const& sName, int iDefault) -> int {
	auto pData = ToElement()->Attribute(sName.c_str());
	return (pData ? string_to<int>(pData) : iDefault);
}


//---------------------------------------------------------------------------//
//	AttrAsFloat
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AttrAsFloat(std::string const& sName, float fDefault) -> float {
	auto pData = ToElement()->Attribute(sName.c_str());
	return (pData ? string_to<float>(pData) : fDefault);
}


//---------------------------------------------------------------------------//
//	AttrAsBool
//---------------------------------------------------------------------------//
auto CNodeFile::CNode::AttrAsBool(std::string const& sName, bool bDefault) -> bool {
	auto pData = ToElement()->Attribute(sName.c_str());
	return (pData ? string_to<bool>(pData) : bDefault);
}


//---------------------------------------------------------------------------//
//	LoadFromFile
//---------------------------------------------------------------------------//
auto CNodeFile::LoadFromFile(std::string const& sFile) -> bool {
	return m_xml_file.LoadFile(sFile.c_str());
}


//---------------------------------------------------------------------------//
//	FirstNode
//---------------------------------------------------------------------------//
auto CNodeFile::FirstNode(std::string const& sName) -> CNode* {
	if (sName.empty()) {
		return static_cast<CNode*>(m_xml_file.FirstChild());
	} else {
		return static_cast<CNode*>(m_xml_file.FirstChild(sName.c_str()));
	}
}


//---------------------------------------------------------------------------//
//	WriteOpen
//---------------------------------------------------------------------------//
auto CNodeFile::WriteOpen(std::string const& sFile) -> bool {
	while (m_node_stack.size() > 0) {
		m_node_stack.pop();
	}
	return m_out_file.Open(sFile, "w");
}


//---------------------------------------------------------------------------//
//	WriteClose
//---------------------------------------------------------------------------//
auto CNodeFile::WriteClose() -> void {
	while (m_node_stack.size() > 0) {
		m_node_stack.pop();
	}
	m_out_file.Close();
}


//---------------------------------------------------------------------------//
//	WriteOpenNode
//---------------------------------------------------------------------------//
auto CNodeFile::WriteOpenNode(std::string const& sName, std::string const& sAttribs) -> void {
	WriteTabs(m_node_stack.size());
	std::string sLine;
	if (sAttribs.empty()) {
		sLine = "<" + sName + ">";
	} else {
		sLine = "<" + sName + " " + sAttribs + ">";
	}
	m_out_file.WriteLn(sLine);
	m_node_stack.push(sName);
}


//---------------------------------------------------------------------------//
//	WriteCloseNode
//---------------------------------------------------------------------------//
auto CNodeFile::WriteCloseNode() -> void {
	std::string sName = m_node_stack.top();
	m_node_stack.pop();
	WriteTabs(m_node_stack.size());
	std::string sLine = "</" + sName + ">";
	m_out_file.WriteLn(sLine);
}


//---------------------------------------------------------------------------//
//	WriteNode
//---------------------------------------------------------------------------//
auto CNodeFile::WriteNode(std::string const& sName, std::string const& sAttribs, std::string const& sValue) -> void {
	WriteTabs(m_node_stack.size());
	std::string str;
	if (sAttribs.empty()) {
		str = "<" + sName + ">" + sValue + "</" + sName + ">";
	} else {
		str = "<" + sName + " " + sAttribs + ">" + sValue + "</" + sName + ">";
	}
	m_out_file.WriteLn(str);
}


//---------------------------------------------------------------------------//
//	WriteNode
//---------------------------------------------------------------------------//
auto CNodeFile::WriteNode(std::string const& sName, std::string const& sAttribs, int iValue) -> void {
	WriteTabs(m_node_stack.size());
	std::stringstream ss;
	if (sAttribs.empty()) {
		ss << "<" << sName << ">" << iValue << "</" << sName << ">";
	} else {
		ss << "<" << sName << " " << sAttribs << ">" << iValue << "</" << sName << ">";
	}
	m_out_file.WriteLn(ss.str());
}


//---------------------------------------------------------------------------//
//	WriteNode
//---------------------------------------------------------------------------//
auto CNodeFile::WriteNode(std::string const& sName, std::string const& sAttribs, float fValue) -> void {
	WriteTabs(m_node_stack.size());
	std::stringstream ss;
	if (sAttribs.empty()) {
		ss << "<" << sName << ">" << fValue << "</" << sName << ">";
	} else {
		ss << "<" << sName << " " << sAttribs << ">" << fValue << "</" << sName << ">";
	}
	m_out_file.WriteLn(ss.str());
}


//---------------------------------------------------------------------------//
//	WriteNode
//---------------------------------------------------------------------------//
auto CNodeFile::WriteNode(std::string const& sName, std::string const& sAttribs, bool bValue) -> void {
	WriteTabs(m_node_stack.size());
	std::stringstream ss;
	int val = (bValue ? 1 : 0);
	if (sAttribs.empty()) {
		ss << "<" << sName << ">" << val << "</" << sName << ">";
	} else {
		ss << "<" << sName << " " << sAttribs << ">" << val << "</" << sName << ">";
	}
	m_out_file.WriteLn(ss.str());
}


//---------------------------------------------------------------------------//
//	WriteTabs
//---------------------------------------------------------------------------//
auto CNodeFile::WriteTabs(size_t iTabs) -> void {
	iTabs <<= 1;
	for (size_t i = 0; i < iTabs; ++i) {
		m_out_file.Write(" ");
	}
}
