//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_VERSION_H
#define NEON_VERSION_H

#define VERSION_NAME		"neon"
#define VERSION_SHORT		"v3.0.0a"
#define VERSION_LONG		VERSION_NAME " " VERSION_SHORT

#define VERSION_DEV_LINE	"alpha"

namespace neon {
	const int VERSION_MAJOR = 3;
	const int VERSION_MINOR = 0;
	const int VERSION_BUGFIX = 0;
} //namespace neon

#endif//NEON_VERSION_H
