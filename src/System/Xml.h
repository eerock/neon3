//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_XML_H
#define NEON_XML_H

#include "tinyxml.h"

namespace neon {

template <typename T>
auto SafeGet(TiXmlElement* pElem, char const* pChild, T const tDefault = T()) -> T {
	auto pData = pElem->FirstChild(pChild);
	return ((pData && pData->FirstChild()) ? string_to<T>(pData->FirstChild()->Value()) : tDefault);
}

template <typename T>
auto SafeGetAttr(TiXmlElement* pElem, char const* pAttr, T const tDefault = T()) -> T {
	auto pData = pElem->Attribute(pAttr);
	return (pData ? string_to<T> : tDefault);
}

auto SafeString(TiXmlElement* pElem, char const* pChild, char const* pDefault = nullptr) -> char const*;

auto GetNumSameChilds(TiXmlNode* pNode, char const* pChild) -> int;

} //namespace neon

#endif//NEON_XML_H
