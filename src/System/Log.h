//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LOG_H
#define NEON_LOG_H


enum ELogType {
	LOGCONSOLE,
	LOGLISTBOX,
	LOGEDITBOX,
};


#ifdef _DEBUG
#define LOG_ENABLED
#endif//_DEBUG

// to enable log for release build...
//#define LOG_ENABLED

#ifdef LOG_ENABLED

auto _glogStart(int type, std::string const& sLogFile) -> void;
auto _glogEnd() -> void;
auto _log(char* fmt, ...) -> void;
auto _logLargeMsg(char* fmt, ...) -> void;

#define GLOGSTART(type, file)		_glogStart(type, file)
#define GLOGEND()					_glogEnd()
#define GLOG(msg)					_log msg
#define GLOG_LARGE_MSG(msg)			_logLargeMsg msg

#else// !LOG_ENABLED

#define GLOGSTART(type, lParam)		
#define GLOGEND()					
#define GLOG(msg)	
#define GLOG_LARGE_MSG(msg)			

#endif//LOG_ENABLED

#endif//NEON_LOG_H
