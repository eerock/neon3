//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_TIMER_H
#define NEON_TIMER_H

#include <Time.h>

//---------------------------------------------------------------------------//
class CTimer {
public:
	CTimer() : m_fTimeIni(0.f) {
#if (PLATFORM == PLATFORM_WINDOWS)
		QueryPerformanceFrequency(&m_fTimerFrecuency);
#endif//PLATFORM
		Reset();
	}

	auto Reset() -> void {
#if (PLATFORM == PLATFORM_WINDOWS)
		LARGE_INTEGER pc;
		QueryPerformanceCounter(&pc);
		m_fTimeIni = ((float)pc.QuadPart / (float)m_fTimerFrecuency.QuadPart);
#endif//PLATFORM
	}

	auto Get() -> float {
#if (PLATFORM == PLATFORM_WINDOWS)
		LARGE_INTEGER pc;
		QueryPerformanceCounter(&pc);
		return ((float)pc.QuadPart / (float)m_fTimerFrecuency.QuadPart) - m_fTimeIni;
#else
		return 0.f;
#endif//PLATFORM
	}

private:
#if (PLATFORM == PLATFORM_WINDOWS)
	LARGE_INTEGER m_fTimerFrecuency;
#else
	float m_fTimerFrecuency;
#endif//PLATFORM
	float m_fTimeIni;
}; 

#endif//NEON_TIMER_H
