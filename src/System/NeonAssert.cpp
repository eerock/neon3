//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"

#ifdef _DEBUG

// from windows example
#ifndef Assert
#if defined(_DEBUG) || defined(DEBUG)
#define Assert(b) if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}
#else
#define Assert(b)
#endif//DEBUG || _DEBUG
#endif//Assert

auto LogAssertFailure(char const* pszExpr, char const* pszFile, uint uLine) -> void {
	GLOG(("Assertion Failed: %s\n", pszExpr));
	GLOG(("  %s(%d)\n", pszFile, uLine));
}

auto LogAssertFailure(char const* pszExpr, char const* pszMessage, char const* pszFile, uint uLine) -> void {
	GLOG(("Assertion Failed: %s\n", pszExpr));
	GLOG(("  %s(%d) - \"%s\"\n", pszFile, uLine, pszMessage));
}

#endif//_DEBUG
