//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_FILE_H
#define NEON_FILE_H

#include <fstream>

class CFile {
public:
	enum {
		BEGIN,
		CURRENT,
		END,
	};

	enum {	// Flags
		READ = 1 << 0,
		WRITE = 1 << 1,
		BINARY = 1 << 2,
	};

	CFile ();
	~CFile ();

	auto Open(std::string const& sFile, std::string const& sAttribs) -> bool;	// prefer
	auto Close() -> void;
	auto Flush() -> void;
	auto Eof() -> bool;

	auto Read(void* pData, size_t size) -> size_t;	// prefer
	auto ReadLn(std::string& sLine) -> int;	// prefer
	auto Write(void const* pData, size_t size) -> size_t;	// prefer
	auto WriteLn(std::string const& sLine) -> int;	// prefer
	auto Write(std::string const& sStr) -> int;		// prefer

	// Get
	auto GetPos() -> int;
	auto GetSize() const -> size_t { return m_size; }

	// Seek
	auto Seek(int iOffset, int iMode) -> void;
	auto SeekToBeg() -> void { Seek(0, BEGIN); }
	auto SeekToEnd() -> void { Seek(0, END); }

	// Tell
	auto Tell() -> int;

private:
	std::fstream m_file;
	size_t m_size;
	uint m_flags;
};

#endif//NEON_FILE_H
