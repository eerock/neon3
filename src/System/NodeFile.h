//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_NODEFILE_H
#define NEON_NODEFILE_H

#include "tinyxml.h"


//---------------------------------------------------------------------------//
//	Class CNodeFile and CNode
//---------------------------------------------------------------------------//
class CNodeFile {
public:
	class CNode : public TiXmlNode {
	public:
		CNode() : TiXmlNode(TINYXML_ELEMENT) {}
		auto GetNumNodes(std::string const& sName = "") -> int;
		auto FirstNode(std::string const& sName = "") -> CNode*;
		auto NextNode(std::string const& sName = "") -> CNode*;
		auto AsString(std::string const& sName, std::string const& sDefault = "") -> std::string;
		auto AsInt(std::string const& sName, int iDefault = 0) -> int;
		auto AsFloat(std::string const& sName, float fDefault = 0.f) -> float;
		auto AsBool(std::string const& sName, bool bDefault = false) -> bool;
		auto AttrAsString(std::string const& sName, std::string const& sDefault = "") -> std::string;
		auto AttrAsInt(std::string const& sName, int iDefault = 0) -> int;
		auto AttrAsFloat(std::string const& sName, float fDefault = 0.f) -> float;
		auto AttrAsBool(std::string const& sName, bool bDefault = false) -> bool;
	};

	// Read
	auto LoadFromFile(std::string const& sFile) -> bool;
	auto FirstNode(std::string const& sName) -> CNode*;

	// Write
	auto WriteOpen(std::string const& sFile) -> bool;
	auto WriteClose() -> void;
	auto WriteOpenNode(std::string const& sName, std::string const& sAttribs) -> void;
	auto WriteCloseNode() -> void;
	auto WriteNode(std::string const& sName, std::string const& sAttribs, std::string const& sValue) -> void;
	auto WriteNode(std::string const& sName, std::string const& sAttribs, int iValue) -> void;
	auto WriteNode(std::string const& sName, std::string const& sAttribs, float fValue) -> void;
	auto WriteNode(std::string const& sName, std::string const& sAttribs, bool bValue) -> void;

private:
	auto WriteTabs(size_t iTabs) -> void;

	// Read
	TiXmlDocument m_xml_file;

	// Write
	CFile m_out_file;

	// Node stack
	std::stack<std::string> m_node_stack;
};

#endif//NEON_NODEFILE_H
