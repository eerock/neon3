//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_TIMEPROFILE_H
#define NEON_TIMEPROFILE_H

#include "Base.h"
#include "Timer.h"

//enum ETimeProfiles {
//	PROFILE_UPDATE_GUI,
//	PROFILE_UPDATE_APP,
//	PROFILE_UPDATE_OUTPUT,
//	PROFILE_DRAW_OUTPUT,
//	PROFILE_MAX,
//};

class CTimeProfile {
public:
	auto Begin() -> void {
		m_tTimer.Reset();
	}

	auto End() -> void {
		m_fTime = m_tTimer.Get() * 1000.f;
		m_tTimes.add(m_fTime);
	}

	auto Last() -> float {
		return m_fTime;
	}
	auto Average() -> float {
		return m_tTimes.get();
	}

private:
	CRollingMean<float, 60> m_tTimes;
	float m_fTime;
	CTimer m_tTimer;
}; 

#endif//NEON_TIMEPROFILE_H
