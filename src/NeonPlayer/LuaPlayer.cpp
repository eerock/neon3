//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "Event.h"
#include "LuaPlayer.h"
#include "LuaFile.h"
#include "VJController.h"
#include "VJLoader.h"
#include "DisplayWindow.h"
#include "Xml.h"

#if (PLATFORM == PLATFORM_WINDOWS)
#pragma warning(disable:4244)
#endif//PLATFORM_WINDOWS

// ebp-> static cpp global, is this okay?
// this pointer is now private to this file
static CLuaPlayer* s_pLuaPlayer = nullptr;


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CLuaPlayer::CLuaPlayer()
	: m_bOk(false)
	, m_fStartTime(0.f)
	, m_fDuration(0.f)
	, m_pVJController(nullptr)
	, m_vEvents()
	, m_vEventsSorted()
	, m_vEventsRunning()
	, m_eventIter()
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CLuaPlayer::Init(THInst hInst) -> bool {
	UNREFERENCED_PARAMETER(hInst);

	GLOG(("Loading " VERSION_NAME " player " VERSION_SHORT " :\n"));

	m_bOk = false;
	s_pLuaPlayer = this;

	IAppOptionsService* pAppOpts = CServiceLocator<IAppOptionsService>::GetService();

	int display_width = pAppOpts->Get("neonconfig.display.rwidth", DISPLAY_WINDOW_WIDTH);
	int display_height = pAppOpts->Get("neonconfig.display.rheight", DISPLAY_WINDOW_HEIGHT);

	TGraphicsMode tGraphicsMode(
		pAppOpts->Get("neonconfig.display.device", 0),
		display_width,
		display_height,
		32,
		pAppOpts->Get("neonconfig.display.refresh", 60),
		false,	//pAppOpts->Get("neonconfig.display.fullscreen", false),
		false
	);

	TVectorI2 tCustomTex1(
		display_width >> 1,
		display_height >> 1
	);
	TVectorI2 tCustomTex2(
		display_width >> 2,
		display_height >> 2
	);
	bool bCanMoveWindow = true;
	bool bShowMouse = false;

	GLOG(("Initailizing VJController...\n"));
	m_pVJController = NEW CVJController;
	if (!m_pVJController->Init(
			0,
			NUM_MASTER_LAYERS,
			tGraphicsMode,
			tCustomTex1,
			tCustomTex2,
			bCanMoveWindow,	// can move window
			bShowMouse)) {	// show mouse
		GLOG(("ERROR: Couldn't initialize VJController!\n"));
		DELETE_PTR(m_pVJController);
		return m_bOk;
	}

	GLOG(("Reading LUA Script...\n"));
	// create a lua file...
	std::unique_ptr<CLuaFile> pLuaFile(NEW CLuaFile);
	// read and execute the lua file...
	m_bOk = pLuaFile->Init("\\demo.lua");
	// check the result...
	if (m_bOk) {
		GLOG((VERSION_NAME " player initialized!\n"));
		GLOG(("**** BEGIN PLAYING EVENTS ****\n"));
	} else {
		DELETE_PTR(m_pVJController);
	}

	return m_bOk;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLuaPlayer::~CLuaPlayer() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLuaPlayer\n"));
#endif
	DELETE_PTR(m_pVJController);
}


//---------------------------------------------------------------------------//
//	Update
//---------------------------------------------------------------------------//
auto CLuaPlayer::Update() -> bool {
	float fTime = m_pVJController->GetTime() + m_fStartTime;
	//GLOG(("Time: %f\n", fTime));

	// Procesar los eventos pendientes
	// es-en: Process pending events
	if (m_eventIter != m_vEventsSorted.end()) {
		auto ev = (*m_eventIter);
		while (0.f <= ev->fTime && ev->fTime < fTime) {
			ExecEventImm(ev, fTime);
			if (++m_eventIter != m_vEventsSorted.end()) {
				ev = (*m_eventIter);
			}
			else {
				break;
			}
		}
	}

	// Procesar los eventos con duracion
	// es-en: Process Events with duration
	for (auto& ev : m_vEventsRunning) {
		ExecEventRun(ev, fTime);
	}

	// Borrar los eventos con duracion caducados
	// es-en: Delete events with expired duration
	for (auto it = m_vEventsRunning.begin(); it != m_vEventsRunning.end(); ) {
		auto ev = (*it);
		if (fTime > ev->ExpireTime()) {
			it = m_vEventsRunning.erase(it);
			DELETE_PTR(ev);
		}
		else {
			++it;
		}
	}

	// Ruunnnnnnnnn
	m_pVJController->Update();

	//GLOG(("Player - FTime: %f  Duration: %f\n", fTime, m_fDuration));
	return (fTime <= m_fDuration);
}


//---------------------------------------------------------------------------//
//	Draw
//---------------------------------------------------------------------------//
auto CLuaPlayer::Draw() -> void {
	m_pVJController->Draw();
}


//---------------------------------------------------------------------------//
//	GetValue (Float)
//---------------------------------------------------------------------------//
auto GetValue(CNodeFile::CNode* pNode, std::string const& sSon, float fDef) -> float {
	auto pSon = pNode->FirstNode(sSon);
	if (pSon) {
		return neon::SafeGet<float>(pSon->ToElement(), "value", fDef);
	}
	return fDef;
}


//---------------------------------------------------------------------------//
//	GetValue (Integer)
//---------------------------------------------------------------------------//
auto GetValue(CNodeFile::CNode* pNode, std::string const& sSon, int iDef) -> int {
	auto pSon = pNode->FirstNode(sSon);
	if (pSon) {
		return neon::SafeGet<int>(pSon->ToElement(), "value", iDef);
	}
	return iDef;
}


//---------------------------------------------------------------------------//
//	GetValue (String)
//---------------------------------------------------------------------------//
auto GetValue(CNodeFile::CNode* pNode, std::string const& sSon, char const* sDef) -> char const* {
	auto pSon = pNode->FirstNode(sSon);
	if (pSon) {
		return neon::SafeString(pSon->ToElement(), "value", sDef);
	}
	return sDef;
}


//---------------------------------------------------------------------------//
//	Setup
//---------------------------------------------------------------------------//
auto CLuaPlayer::Setup(float fDuration, float fStartTime, int iSources, bool bEnableFPS) -> void {
	GLOG(("NeonPlayer Setup(%f, %f, %d, %s)\n", fDuration, fStartTime, iSources, bEnableFPS ? "fps" : "no fps"));
	UNREFERENCED_PARAMETER(iSources);
	m_fDuration = fDuration;
	m_fStartTime = fStartTime;

	// enable/disable the FPS counter display
	m_pVJController->DrawDebug(bEnableFPS);
}


//---------------------------------------------------------------------------//
//	Start
//---------------------------------------------------------------------------//
auto CLuaPlayer::Start() -> void {
	GLOG(("NeonPlayer Start()\n"));
	// Sort events
	TEventComparator ec;
	std::sort(m_vEvents.begin(), m_vEvents.end(), ec);
	m_vEventsSorted = std::move(m_vEvents);
	m_eventIter = m_vEventsSorted.begin();

	// ebp-> turned off sound for now... 
	//m_pVJController->GetSound()->Play(m_fStartTime);
	m_pVJController->Reset();
}


//---------------------------------------------------------------------------//
//	LoadMusic
//---------------------------------------------------------------------------//
auto CLuaPlayer::LoadMusic(std::string const& sFile) -> void {
	GLOG(("NeonPlayer LoadMusic(%s)\n", sFile.c_str()));
	m_pVJController->GetSound()->Load(sFile);
	m_pVJController->GetDisplayWindow()->ProcessMessages();
}


//---------------------------------------------------------------------------//
//	LoadMedia
//---------------------------------------------------------------------------//
auto CLuaPlayer::LoadMedia(std::string const& sFile) -> uint {
	GLOG(("NeonPlayer LoadMedia(%s)\n", sFile.c_str()));
	CNodeFile NodeFile;
	if (!NodeFile.LoadFromFile(sFile)) {
		GLOG(("ERROR: Can't load file %s\n", sFile.c_str()));
		return false;
	}

	// Has nodes?
	auto pNode = NodeFile.FirstNode("layer");
	if (!pNode) {
		return false;
	}

	// Source file?
	auto pNodeFX = pNode->FirstNode("source");
	if (!pNodeFX) {
		return false;
	}

	// Load FX data
	std::string const sFXClass = pNodeFX->AttrAsString("class");
	std::string const sFXDir = pNodeFX->AsString("dir");
	std::string const sFXFile = pNodeFX->AsString("file");

	// todo: fix this, not all will come from "Media\" directory.
	PushDir("Media\\" + sFXDir);
	uint uSourceID = m_pVJController->SourceLoad(sFXClass, sFXFile);
	PopDir();

	// Load the initial variables (effects, vars, etc)...
	if (uSourceID != INVALID_ID) {
		auto pNodeVars = pNodeFX->FirstNode("vars");
		if (pNodeVars) {
			// Common vars
			auto pNodeCommon = pNodeVars->FirstNode("source");
			if (pNodeCommon) {
				m_pVJController->SourceSetSpeed(uSourceID, GetValue(pNodeCommon, "speed", 0.2f));
				m_pVJController->SourceSetAlpha(uSourceID, GetValue(pNodeCommon, "alpha", 1.f));
				m_pVJController->SourceSetBlend(uSourceID, GetValue(pNodeCommon, "blend", 0));
				m_pVJController->SourceSetQuality(uSourceID, GetValue(pNodeCommon, "quality", 0));
				m_pVJController->SourceSetFade(uSourceID, GetValue(pNodeCommon, "fade", 250.f) * 0.01f);
			}

			// ebp-> is this LoadVars (common vars) in conflict with above 'effect' vars?
			// were they the same thing at one point?

			// Common Vars
			LoadVars(pNodeVars, uSourceID, -1, 0);

			// Object Vars
			auto pNodeObjVars = pNodeVars->FirstNode("objvars");
			if (pNodeObjVars) {
				// Recorrer todos los objvar
				auto pNodeObjVar = pNodeObjVars->FirstNode("objvar");
				while (pNodeObjVar) {
					int iScope = pNodeObjVar->AttrAsInt("scope", -1);
					int iObj = pNodeObjVar->AttrAsInt("obj", -1);
					LoadVars(pNodeObjVar, uSourceID, iScope, iObj);
					pNodeObjVar = pNodeObjVars->NextNode("objvar");
				}
			}
		}

		// Effect Vars
		auto pNodeEffects = pNodeFX->FirstNode("effects");
		if (pNodeEffects) {
			auto pNodeEffect = pNodeEffects->FirstNode("effect");
			while (pNodeEffect) {
				int cell = pNodeEffect->AttrAsInt("cell");
				std::string sName = pNodeEffect->AttrAsString("name");
				// Try to load
				uint uEffectID = m_pVJController->SourceAddEffect(uSourceID, cell, sName);
				if (uEffectID != INVALID_ID) {
					LoadVars(pNodeEffect, uSourceID, uEffectID);
				}
				pNodeEffect = pNodeEffects->NextNode("effect");
			}
		}
	}

	m_pVJController->GetDisplayWindow()->ProcessMessages();
	return (uSourceID);
}


//---------------------------------------------------------------------------//
//	AddEvent
//---------------------------------------------------------------------------//
auto CLuaPlayer::AddEvent(TEvent* pEvent) -> void {
	GLOG(("NeonPlayer AddEvent(%s, src=%d, time=%f, dur=%f)\n", pEvent->GetName(), pEvent->uSourceID, pEvent->fTime, pEvent->fDuration));
	m_vEvents.push_back(pEvent);
	m_pVJController->GetDisplayWindow()->ProcessMessages();
}


//---------------------------------------------------------------------------//
//	LoadVars
//---------------------------------------------------------------------------//
auto CLuaPlayer::LoadVars(CNodeFile::CNode* pNode, uint uSourceID, int iScope, int iObj) -> void {
	// FX Vars
	int iVar = 0;
	auto pNodeVars = pNode->FirstNode("var");
	while (pNodeVars) {
		auto pCtrlVar = m_pVJController->SourceGetVarCtrls(uSourceID, iScope);
		switch (pCtrlVar[iVar].iType) {
			case TCtrlVar::CHECK_BOX: {
				bool vb = neon::SafeGet<bool>(pNodeVars->ToElement(), "value", false);
				m_pVJController->SourceSetVar(uSourceID, iScope, iObj, iVar, &vb);
				break;
			}
			case TCtrlVar::COMBO_BOX: {
				int vi = neon::SafeGet<int>(pNodeVars->ToElement(), "value", 0);
				m_pVJController->SourceSetVar(uSourceID, iScope, iObj, iVar, &vi);
				break;
			}
			case TCtrlVar::SLIDER: {
				float vf = neon::SafeGet<float>(pNodeVars->ToElement(), "value", 0.f);
				m_pVJController->SourceSetVar(uSourceID, iScope, iObj, iVar, &vf);
				break;
			}
			default: {
				break;
			}
		}
		pNodeVars = pNodeVars->NextNode("var");
		++iVar;
	}
}


//---------------------------------------------------------------------------//
//	LoadVars
//---------------------------------------------------------------------------//
auto CLuaPlayer::LoadVars(CNodeFile::CNode* pNode, uint uSourceID, int iEffect) -> void {
	// FX Vars
	int iVar = 0;
	auto pNodeVars = pNode->FirstNode("var");
	while (pNodeVars) {
		auto pCtrlVar = m_pVJController->SourceGetEffectVarCtrls(uSourceID, iEffect);
		switch (pCtrlVar[iVar].iType) {
			case TCtrlVar::CHECK_BOX: {
				bool vb = neon::SafeGet<bool>(pNodeVars->ToElement(), "value", false);
				m_pVJController->SourceSetEffectVar(uSourceID, iEffect, iVar, &vb);
				break;
			}
			case TCtrlVar::COMBO_BOX: {
				int vi = neon::SafeGet<int>(pNodeVars->ToElement(), "value", 0);
				m_pVJController->SourceSetEffectVar(uSourceID, iEffect, iVar, &vi);
				break;
			}
			case TCtrlVar::SLIDER: {
				float vf = neon::SafeGet<float>(pNodeVars->ToElement(), "value", 0.f);
				m_pVJController->SourceSetEffectVar(uSourceID, iEffect, iVar, &vf);
				break;
			}
			default: {
				break;
			}
		}
		pNodeVars = pNodeVars->NextNode("var");
		iVar++;
	}
}


//---------------------------------------------------------------------------//
//	GetFFVarName
//---------------------------------------------------------------------------//
auto GetFFVarName(TCtrlVar* pVars, char const* pVar) -> int {
	int i = 0;
	while (pVars[i].iType != TCtrlVar::INVALID) {
		if (Stricmp(pVars[i].szName, pVar) == 0) {
			return i;
		}
		++i;
	}
	return -1;
}


//---------------------------------------------------------------------------//
//	ExecEventImm
//---------------------------------------------------------------------------//
auto CLuaPlayer::ExecEventImm(TEvent* pEvent, float fTime) -> void {
	UNREFERENCED_PARAMETER(fTime);
	// Get var ID from name
	if (pEvent->iEvent == EV_FF_SETVAR) {
		int iIDEffect = m_pVJController->SourceGetEffectID(pEvent->uSourceID, pEvent->iEffectSlot);
		pEvent->iSetVar = GetFFVarName(m_pVJController->SourceGetEffectVarCtrls(pEvent->uSourceID, iIDEffect), pEvent->pSetVar);
	}

	// Exec Event
	if (pEvent->fDuration > 0.f) {
		m_vEventsRunning.push_back(pEvent);
	} else {
		// log the event name and time it happened...
		GLOG(("%s: %f\n", pEvent->GetName(), fTime));
		switch (pEvent->iEvent) {
			case EV_SOURCE_ATTACH: {
				m_pVJController->SourceAttach(pEvent->uSourceID, pEvent->iSetVarInt);
				break;
			}
			case EV_SOURCE_UNATTACH: {
				m_pVJController->SourceUnattach(pEvent->uSourceID);
				break;
			}
			case EV_SOURCE_RESET: {
				break;
			}
			case EV_SOURCE_SETSPEED: {
				m_pVJController->SourceSetSpeed(pEvent->uSourceID, pEvent->fSetVarFloat);
				break;
			}
			case EV_SOURCE_SETALPHA: {
				m_pVJController->SourceSetAlpha(pEvent->uSourceID, pEvent->fSetVarFloat);
				break;
			}
			case EV_SOURCE_SETTIME: {
				m_pVJController->SourceSetTime(pEvent->uSourceID, pEvent->fSetVarFloat);
				break;
			}
			case EV_SOURCE_SETBLEND: {
				m_pVJController->SourceSetBlend(pEvent->uSourceID, pEvent->iSetVarInt);
				break;
			}
			case EV_SOURCE_SETQUALITY: {
				m_pVJController->SourceSetQuality(pEvent->uSourceID, pEvent->iSetVarInt);
				break;
			}
			case EV_SOURCE_SETFADE: {
				m_pVJController->SourceSetFade(pEvent->uSourceID, pEvent->fSetVarFloat);
				break;
			}
			case EV_SOURCE_ADDEFFECT: {
				std::string sEffect(pEvent->pSetEffect);	// ebp-> from effect name hack, removed constness from
				// last parameter so that adding the filter could write a modified name into it.
				m_pVJController->SourceAddEffect(pEvent->uSourceID, pEvent->iEffectSlot, sEffect);
				break;
			}
			case EV_SOURCE_DELEFFECT: {
				m_pVJController->SourceRemoveEffect(pEvent->uSourceID, pEvent->iEffectSlot);
				break;
			}
			case EV_SOURCE_SWAPEFFECT: {
				m_pVJController->SourceSwapEffect(pEvent->uSourceID, pEvent->iEffectSlot, pEvent->iSetVarInt);
				break;
			}
			case EV_SOURCE_SETVAR: {
				switch (pEvent->iSetVarType) {
					case SET_VAR_INT: {
						m_pVJController->SourceSetVar(pEvent->uSourceID, pEvent->iSetVarScope, pEvent->iSetVarObject, pEvent->iSetVar, &pEvent->iSetVarInt);
						break;
					}
					case SET_VAR_FLOAT: {
						m_pVJController->SourceSetVar(pEvent->uSourceID, pEvent->iSetVarScope, pEvent->iSetVarObject, pEvent->iSetVar, &pEvent->fSetVarFloat);
						break;
					}
					case SET_VAR_BOOL: {
						m_pVJController->SourceSetVar(pEvent->uSourceID, pEvent->iSetVarScope, pEvent->iSetVarObject, pEvent->iSetVar, &pEvent->bSetVarBool);
						break;
					}
					default: {
						break;
					}
				}
				break;
			}
			case EV_FF_SETVAR: {
				int iIDEffect = m_pVJController->SourceGetEffectID(pEvent->uSourceID, pEvent->iEffectSlot);
				switch (pEvent->iSetVarType) {
					case SET_VAR_INT: {
						m_pVJController->SourceSetEffectVar(pEvent->uSourceID, iIDEffect, pEvent->iSetVar, &pEvent->iSetVarInt);
						break;
					}
					case SET_VAR_FLOAT: {
						m_pVJController->SourceSetEffectVar(pEvent->uSourceID, iIDEffect, pEvent->iSetVar, &pEvent->fSetVarFloat);
						break;
					}
					case SET_VAR_BOOL: {
						m_pVJController->SourceSetEffectVar(pEvent->uSourceID, iIDEffect, pEvent->iSetVar, &pEvent->bSetVarBool);
						break;
					}
					default: {
						break;
					}
				}
				break;
			}
			default: {
				break;
			}
		}
	}
}


//---------------------------------------------------------------------------//
//	ExecEventRun
//	Use this to update values over time for certain events.
//---------------------------------------------------------------------------//
auto CLuaPlayer::ExecEventRun(TEvent* pEvent, float fTime) -> void {
	switch (pEvent->iEvent) {
		case EV_SOURCE_SETSPEED: {
			m_pVJController->SourceSetSpeed(pEvent->uSourceID, pEvent->GetValueF(fTime));
			break;
		}
		case EV_SOURCE_SETALPHA: {
			m_pVJController->SourceSetAlpha(pEvent->uSourceID, pEvent->GetValueF(fTime));
			break;
		}
		case EV_SOURCE_SETTIME: {
			m_pVJController->SourceSetTime(pEvent->uSourceID, pEvent->GetValueF(fTime));
			break;
		}
		case EV_SOURCE_SETVAR: {
			m_pVJController->SourceSetVar(pEvent->uSourceID, pEvent->iSetVarScope, pEvent->iSetVarObject, pEvent->iSetVar, pEvent->GetValuePF(fTime));
			break;
		}
		case EV_FF_SETVAR: {
			int iIDEffect = m_pVJController->SourceGetEffectID(pEvent->uSourceID, pEvent->iEffectSlot);
			m_pVJController->SourceSetEffectVar(pEvent->uSourceID, iIDEffect, pEvent->iSetVar, pEvent->GetValuePF(fTime));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVarScope
//---------------------------------------------------------------------------//
auto CLuaPlayer::GetVarScope(uint uSourceID, char const* pScope) -> int {
	std::vector<std::string> vScopes;
	m_pVJController->SourceGetVarScopes(uSourceID, vScopes);
	for (uint i = 0; i < vScopes.size(); ++i) {
		if (Stricmp(vScopes[i], pScope) == 0) {
			return i;
		}
	}
	return -1;
}


//---------------------------------------------------------------------------//
//	GetVarObject
//---------------------------------------------------------------------------//
auto CLuaPlayer::GetVarObject(uint uSourceID, int iScope, char const* pObject) -> int {
	std::vector<std::string> vObjects;
	m_pVJController->SourceGetVarObjects(uSourceID, vObjects, iScope);
	for (uint i = 0; i < vObjects.size(); ++i) {
		if (Stricmp(vObjects[i], pObject) == 0) {
			return i;
		}
	}
	return -1;
}


//---------------------------------------------------------------------------//
//	GetVarName
//---------------------------------------------------------------------------//
auto CLuaPlayer::GetVarName(uint uSourceID, int iScope, char const* pVar) -> int {
	UNREFERENCED_PARAMETER(iScope);
	auto pVars = m_pVJController->SourceGetVarCtrls(uSourceID);
	int i = 0;
	while (pVars[i].iType != TCtrlVar::INVALID) {
		if (Stricmp(pVars[i].szName, pVar) == 0) {
			return i;
		}
		++i;
	}
	return -1;
}


//---------------------------------------------------------------------------//
//
//	L U A   B I N D I N G S
//
//---------------------------------------------------------------------------//
//	Be sure to return the number of lua return values in the C bindings:

//---------------------------------------------------------------------------//
//	NeonPlayer_Setup
//---------------------------------------------------------------------------//
auto NeonPlayer_Setup(lua_State* pLuaState) -> int {
	s_pLuaPlayer->Setup(lua_tofloat(pLuaState, 1), lua_tofloat(pLuaState, 2), lua_tointeger(pLuaState, 3), lua_tobool(pLuaState, 4));
	return 0;
}


//---------------------------------------------------------------------------//
//	NeonPlayer_Start
//---------------------------------------------------------------------------//
auto NeonPlayer_Start(lua_State* pLuaState) -> int {
#if (PLATFORM == PLATFORM_WINDOWS)
	UNREFERENCED_PARAMETER(pLuaState);
#endif
	s_pLuaPlayer->Start();
	return 0;
}

//---------------------------------------------------------------------------//
//	NeonPlayer_LoadMusic
//---------------------------------------------------------------------------//
auto NeonPlayer_LoadMusic(lua_State* pLuaState) -> int {
	s_pLuaPlayer->LoadMusic(lua_tostring(pLuaState, 1));
	return 0;
}

//---------------------------------------------------------------------------//
//	NeonPlayer_LoadMedia
//---------------------------------------------------------------------------//
auto NeonPlayer_LoadMedia(lua_State* pLuaState) -> int {
	uint uSourceID = s_pLuaPlayer->LoadMedia(lua_tostring(pLuaState, 1));
	lua_pushunsigned(pLuaState, uSourceID);
	return 1;
}

//---------------------------------------------------------------------------//
//	NeonPlayer_AddEvent
//---------------------------------------------------------------------------//
auto NeonPlayer_AddEvent(lua_State* lua) -> int {
	auto pEvent = NEW TEvent;
	pEvent->iEvent = static_cast<EEventID>(lua_tointeger(lua, 1));
	pEvent->fTime = lua_tofloat(lua, 2);
	pEvent->uSourceID = lua_tounsigned(lua, 3);
	pEvent->fDuration = 0.f;

	switch (pEvent->iEvent) {
		// For FX
		case EV_SOURCE_ATTACH: {
			// (EV_SOURCE_ATTACH, TIME, SRCID, LAYER)
			pEvent->iSetVarInt = lua_tointeger(lua, 4);
			break;
		}

		case EV_SOURCE_UNATTACH: {
			// (EV_SOURCE_UNATTACH, TIME, SRCID)
			break;
		}

		case EV_SOURCE_RESET: {
			// (EV_SOURCE_RESET, TIME, SRCID)
			break;
		}

		case EV_SOURCE_SETSPEED:
		case EV_SOURCE_SETALPHA:
		case EV_SOURCE_SETTIME: {
			// (EV_FX_SETXXX, TIME, SRCID, VALUE, VALUETO, DURATION)
			pEvent->fSetVarFloat = lua_tofloat(lua, 4);
			pEvent->fDuration = lua_tofloat(lua, 6);
			if (pEvent->fDuration > 0.f) {
				pEvent->fSetVarFloatTo = lua_tofloat(lua, 5);
			}
			// else ?
			break;
		}

		case EV_SOURCE_SETBLEND: {
			// (EV_SOURCE_SETBLEND, TIME, SRCID, BLENDMODE)
			pEvent->iSetVarInt = lua_tointeger(lua, 4);
			break;
		}

		case EV_SOURCE_SETQUALITY: {
			// (EV_SOURCE_SETQUALITY, TIME, SRCID, QUALITY)
			pEvent->iSetVarInt = lua_tointeger(lua, 4);
			break;
		}

		case EV_SOURCE_SETFADE: {
			// (EV_SOURCE_SETFADE, TIME, SRCID, FADE)
			pEvent->fSetVarFloat = lua_tofloat(lua, 4);
			break;
		}

		case EV_SOURCE_ADDEFFECT: {
			// (EV_SOURCE_ADDEFFECT, TIME, SRCID, FILTERNAME, SLOT)
			Strncpy(pEvent->pSetEffect, lua_tostring(lua, 4), 32);
			pEvent->iEffectSlot = lua_tointeger(lua, 5);
			break;
		}

		case EV_SOURCE_DELEFFECT: {
			// (EV_SOURCE_DELEFFECT, TIME, SRCID, SLOT)
			pEvent->iEffectSlot = lua_tointeger(lua, 4);
			break;
		}

		case EV_SOURCE_SWAPEFFECT: {
			// (EV_SOURCE_SWAPEFFECT, TIME, SRCID, SLOT1, SLOT2)
			pEvent->iEffectSlot = lua_tointeger(lua, 4);
			pEvent->iSetVarInt = lua_tointeger(lua, 5);
			break;
		}

		case EV_SOURCE_SETVAR: {
			// (EV_SOURCE_SETVAR, TIME, SRCID, VARTYPE, VARSCOPE, VAROBJECT, VAR, VALUE, VALUETO, DURATION);
			pEvent->iSetVarType = lua_tointeger(lua, 4);
			pEvent->iSetVarScope = s_pLuaPlayer->GetVarScope(pEvent->uSourceID, lua_tostring(lua, 5));
			pEvent->iSetVarObject = s_pLuaPlayer->GetVarObject(pEvent->uSourceID, pEvent->iSetVarScope, lua_tostring(lua, 6));
			pEvent->iSetVar = s_pLuaPlayer->GetVarName(pEvent->uSourceID, pEvent->iSetVarScope, lua_tostring(lua, 7));
			pEvent->fDuration = lua_tofloat(lua, 10);
			switch (pEvent->iSetVarType) {
				case SET_VAR_INT: {
					pEvent->iSetVarInt = lua_tointeger(lua, 8);
					break;
				}
				case SET_VAR_FLOAT: {
					pEvent->fSetVarFloat = lua_tofloat(lua, 8);
					if (pEvent->fDuration > 0.f) {
						pEvent->fSetVarFloatTo = lua_tofloat(lua, 9);
					}
					break;
				}
				case SET_VAR_BOOL: {
					pEvent->bSetVarBool = lua_tobool(lua, 8);
					break;
				}
				default: {
					break;
				}
			}
			break;
		}

		case EV_SOURCE_SETLAYOUTX:
		case EV_SOURCE_SETLAYOUTY: {
			// Unused
			return 0;
		}

		// For FreeFrame
		// todo-> remove Freeframe code...
		case EV_FF_SETVAR: {
			// (EV_FF_SETVAR, TIME, SRCID, VARTYPE, VAR, DURATION, VALUE, VALUETO);
			pEvent->iSetVarType = lua_tointeger(lua, 4);
			Strncpy(pEvent->pSetVar, lua_tostring(lua, 5), 32);
			pEvent->fDuration = lua_tofloat(lua, 8);

			switch (pEvent->iSetVarType) {
				case SET_VAR_INT: {
					pEvent->iSetVarInt = lua_tointeger(lua, 6);
					break;
				}
				case SET_VAR_FLOAT: {
					pEvent->fSetVarFloat = lua_tofloat(lua, 6);
					if (pEvent->fDuration > 0.f) {
						pEvent->fSetVarFloatTo = lua_tofloat(lua, 7);
					}
					break;
				}
				case SET_VAR_BOOL: {
					pEvent->bSetVarBool = lua_tobool(lua, 6);
					break;
				}
				default: {
					break;
				}
			}
			break;
		}

		default: {
			break;
		}
	}

	s_pLuaPlayer->AddEvent(pEvent);
	return 0;
}
