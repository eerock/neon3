//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LUAFILE_H
#define NEON_LUAFILE_H

#include "LuaUtils.h"

class CLuaFile {
public:
	CLuaFile();
	~CLuaFile();

	auto Init(std::string const& sFile) -> bool;
	auto IsOk() -> bool { return m_bOk; }

	auto ExecFunc(const char *pszFuncion, ...) -> void;
	auto LuaState() -> lua_State* { return m_pLuaState; }

private:
	bool m_bOk;
	lua_State* m_pLuaState;
};

#endif//NEON_LUAFILE_H
