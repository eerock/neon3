//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#include "Base.h"
#include "LuaFile.h"
#include "LuaPlayer.h"
#include "Event.h"
#include "Sources/Source.h"
#include "Effects/Effect.h"
#include "CtrlVar.h"
#include "VJDefs.h"

#ifdef __cplusplus
extern "C" {
#endif//__cplusplus
#include "lauxlib.h"
#include "lualib.h"
#ifdef __cplusplus
}
#endif//__cplusplus


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CLuaFile::CLuaFile()
	: m_bOk(false)
	, m_pLuaState(nullptr)
{}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CLuaFile::Init(std::string const& sFile) -> bool {
	// ebp-> Supports LUA v5.2.0
	m_pLuaState = luaL_newstate();
	if (m_pLuaState) {
		luaL_openlibs(m_pLuaState);

		// Register the LUA hook functions...
		lua_register(m_pLuaState, "Setup", NeonPlayer_Setup);
		lua_register(m_pLuaState, "Start", NeonPlayer_Start);
		lua_register(m_pLuaState, "LoadMusic", NeonPlayer_LoadMusic);
		lua_register(m_pLuaState, "LoadMedia", NeonPlayer_LoadMedia);
		lua_register(m_pLuaState, "AddEvent", NeonPlayer_AddEvent);

		char nameBuffer[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, nameBuffer);
		std::string sPath = nameBuffer + sFile;

		if (luaL_loadfile(m_pLuaState, sPath.c_str()) == LUA_OK) {
			if (lua_pcall(m_pLuaState, 0, 0, 0) == LUA_OK) {
				m_bOk = true;
			} else {
				GLOG(("ERROR: Error parsing LUA script '%s'", sFile.c_str()));
			}
		} else {
			GLOG(("ERROR: Can't load LUA file %s.\n", sPath.c_str()));
		}
	}

	return IsOk();
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CLuaFile::~CLuaFile() {
#ifdef DESTRUCTOR_VERBOSE
	GLOG(("~CLuaFile\n"));
#endif
	if (IsOk()) {
		if (m_pLuaState) {
			lua_close(m_pLuaState);
			m_pLuaState = nullptr;
		}
		m_bOk = false;
	}
}


//---------------------------------------------------------------------------//
//	ExecFunc
//---------------------------------------------------------------------------//
auto CLuaFile::ExecFunc(const char *pszFunc, ...) -> void {
	if (!IsOk()) {
		return;
	}

	// Formatear la funcion
	// es-en: Format the function
#define EXEC_BUFFER_SIZE	(1024)
	char pFn[EXEC_BUFFER_SIZE];
	va_list valist;
	va_start(valist, pszFunc);
	Vsnprintf(pFn, EXEC_BUFFER_SIZE, pszFunc, valist);
	va_end(valist);

	int error = luaL_dostring(m_pLuaState, pFn);
	if (error != LUA_OK) {
		GLOG(("WARNING: Can't execute Lua function '%s'\n", pFn));
	}
}


