//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_LUAPLAYER_H
#define NEON_LUAPLAYER_H

#include "Sound.h"
#include "LuaUtils.h"

class CVJController;
class CLuaFile;
struct TEvent;


//---------------------------------------------------------------------------//
//	Class CLuaPlayer
//---------------------------------------------------------------------------//
class CLuaPlayer {
public:
	CLuaPlayer();
	~CLuaPlayer();

	auto Init(THInst hInst) -> bool;
	auto Update() -> bool;
	auto Draw() -> void;
	
	// L U A   B I N D I N G S
	auto Setup(float fDuration, float fStartTime, int nSources, bool bEnableFPS) -> void;
	auto Start() -> void;
	auto LoadMusic(std::string const& sFile) -> void;
	auto LoadMedia(std::string const& sFile) -> uint;
	auto AddEvent(TEvent* pEvent) -> void;
	// L U A   B I N D I N G S

	auto GetVarScope(uint uSourceID, char const* pScope) -> int;
	auto GetVarObject(uint uSourceID, int iScope, char const* pObject) -> int;
	auto GetVarName(uint uSourceID, int iScope, char const* pVar) -> int;

private:
	auto LoadVars(CNodeFile::CNode* pNode, uint uSourceID, int iScope, int iObj) -> void;
	auto LoadVars(CNodeFile::CNode* pNode, uint uSourceID, int iEffect) -> void;
	auto ExecEventImm(TEvent* pEvent, float fTime) -> void;
	auto ExecEventRun(TEvent* pEvent, float fTime) -> void;

	bool m_bOk;
	float m_fStartTime;
	float m_fDuration;
	CVJController* m_pVJController;

	// Event Management
	std::vector<TEvent*> m_vEvents;
	std::vector<TEvent*> m_vEventsSorted;
	std::vector<TEvent*> m_vEventsRunning;
	std::vector<TEvent*>::iterator m_eventIter;
};


// LUA function declarations...
auto NeonPlayer_Setup(lua_State* pLuaState) -> int;
auto NeonPlayer_Start(lua_State* pLuaState) -> int;
auto NeonPlayer_LoadMusic(lua_State* pLuaState) -> int;
auto NeonPlayer_LoadMedia(lua_State* pLuaState) -> int;
auto NeonPlayer_AddEvent(lua_State* pLuaState) -> int;


#endif//NEON_LUAPLAYER_H
