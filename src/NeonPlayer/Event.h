//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef NEON_EVENT_H
#define NEON_EVENT_H

enum {
	SET_VAR_INT,
	SET_VAR_FLOAT,
	SET_VAR_BOOL,
};

// Event enum
enum EEventID { //EEvents
	// For Sources
	EV_SOURCE_LOAD,			// unused
	EV_SOURCE_UNLOAD,		// unused
	EV_SOURCE_ATTACH,
	EV_SOURCE_UNATTACH,
	EV_SOURCE_RESET,
	EV_SOURCE_SETSPEED,
	EV_SOURCE_SETALPHA,
	EV_SOURCE_SETTIME,
	EV_SOURCE_SETBLEND,
	EV_SOURCE_SETQUALITY,
	EV_SOURCE_SETFADE,
	EV_SOURCE_ADDEFFECT,
	EV_SOURCE_DELEFFECT,
	EV_SOURCE_SWAPEFFECT,
	EV_SOURCE_SETVAR,
	EV_SOURCE_SETLAYOUTX,	// unused
	EV_SOURCE_SETLAYOUTY,	// unused

	// For FF
	EV_FF_SETVAR,
};


// TEvent - an event happening at a time for a duration, etc...
struct TEvent {
	EEventID iEvent;
	float fTime;
	float fDuration;

	// Source ID and Effect Slot
	uint uSourceID;
	int iEffectSlot;

	// SetVar
	union {
		int  iSetVar;
		char pSetVar[32];	// this is bigger than int!!
	};

	int iSetVarScope;
	int iSetVarObject;

	// Value
	union {
		void* pValue;
		float fSetVarFloat;
		int iSetVarInt;
		bool bSetVarBool;
		char pSetEffect[32];	// this is bigger than int, ptr, float, bool!!
	};

	// SetVarType
	int iSetVarType;

	// Value to
	union {
		float fSetVarFloatTo;
		int iSetVarIntTo;
		bool bSetVarBoolTo;
	};

	auto GetValuePF(float _fTime) -> void* {
		static float f = 0.f;
		f = GetValueF(_fTime);
		return &f;
	}

	auto GetValueF(float _fTime) -> float {
		if (fDuration > 0.f) {
			return (_fTime > (fTime + fDuration)) ? fSetVarFloatTo : (((_fTime - fTime) * (fSetVarFloatTo - fSetVarFloat) / fDuration) + fSetVarFloat);
		} else {
			return fSetVarFloat;
		}
	}

	auto ExpireTime() const -> float {
		return fTime + fDuration;
	}

	auto GetName() const -> char const* {
		switch (iEvent) {
			case EV_SOURCE_LOAD: return "Source Load";
			case EV_SOURCE_UNLOAD: return "Source Unload";
			case EV_SOURCE_ATTACH: return "Source Attach";
			case EV_SOURCE_UNATTACH: return "Source Unattach";
			case EV_SOURCE_RESET: return "Source Reset";
			case EV_SOURCE_SETSPEED: return "Source Set Speed";
			case EV_SOURCE_SETALPHA: return "Source Set Alpha";
			case EV_SOURCE_SETBLEND: return "Source Set Blend";
			case EV_SOURCE_SETTIME: return "Source Set Time";
			case EV_SOURCE_SETQUALITY: return "Source Set Quality";
			case EV_SOURCE_SETFADE: return "Source Set Fade";
			case EV_SOURCE_ADDEFFECT: return "Source Add Effect";
			case EV_SOURCE_DELEFFECT: return "Source Delete Effect";
			case EV_SOURCE_SWAPEFFECT: return "Source Swap Effects";
			case EV_SOURCE_SETVAR: return "Source Set Var";
			case EV_SOURCE_SETLAYOUTX: return "Source Set Layout X";
			case EV_SOURCE_SETLAYOUTY: return "Source Set Layout Y";
			case EV_FF_SETVAR: return "Freeframe Set Var";
			default: return "Unknown Event ID";
		}
	}
};

// TEvent Comparator object...
typedef struct {
	auto operator() (TEvent* e1, TEvent* e2) -> bool {
		return e1->fTime < e2->fTime;
	}
} TEventComparator;


// ebp-> this must be the old version...
//struct TEvent {
//	int iEvent;
//	// Target id: FX/Filter
//	union {
//		int iSource;
//		int iFF;
//	};
//	// Event parameter
//	union {
//		float  fSetSpeed;
//		float  fSetAlpha;
//		float  fSetBlend;
//		float  fSetTime;
//		char   pSetEffect[32];
//		char   pSetVar[32];
//	};
//	// SetVar
//	union {
//		float fSetVarFloat;
//		int   iSetVarInt;
//		bool  bSetVarBool;
//	};
//	// SetVarFade
//	float fSetVarFadeDuration;
//	union {
//		float fSetVarFloatTo;
//		int   iSetVarIntTo;
//		bool  bSetVarBoolTo;
//	};
//};


#endif//NEON_EVENT_H
