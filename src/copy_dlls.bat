
@echo off

set FFLAGS=/V /F /D /Y /C

rem X86 DLLs

set ARCH=x86
set ARCHDIR=Win32

set DEST=..\%ARCH%\

xcopy .\FXDLLs\build\%ARCHDIR%\FXBasic\Release\*.dll %DEST% %FFLAGS%
xcopy .\FXDLLs\build\%ARCHDIR%\FXMosaic\Release\*.dll %DEST% %FFLAGS%

xcopy .\ext\portaudio\build\msvc\%ARCHDIR%\Release\*.dll %DEST% %FFLAGS%

rem X64 DLLs

set ARCH=x64
set ARCHDIR=%ARCH%

set DEST=..\%ARCH%\

xcopy .\FXDLLs\build\%ARCHDIR%\FXBasic\Release\*.dll %DEST% %FFLAGS%
xcopy .\FXDLLs\build\%ARCHDIR%\FXMosaic\Release\*.dll %DEST% %FFLAGS%

xcopy .\ext\portaudio\build\msvc\%ARCHDIR%\Release\*.dll %DEST% %FFLAGS%

echo Done!
