//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

#ifndef __N3_EFFECTMOSAIC_H__
#define __N3_EFFECTMOSAIC_H__

#include "N3_Utils.h"


//---------------------------------------------------------------------------//
//	Defines
//---------------------------------------------------------------------------//
#define MIN_WIDTH	(8)
#define MIN_HEIGHT	(6)
#define MAX_WIDTH	(40)
#define MAX_HEIGHT	(30)
#define MAX_BLOCKS	(MAX_WIDTH * MAX_HEIGHT)


//---------------------------------------------------------------------------//
enum EEffectMode {
	STRETCH_WH,
	STRETCH_W,
	STRETCH_H,
};


//---------------------------------------------------------------------------//
//	Effect Mosaic
//---------------------------------------------------------------------------//
class CEffectMosaic : public IEffect {
public:
	// Ctor/Dtor
	CEffectMosaic();
	~CEffectMosaic();

	auto Init(
		void* pEffectInstance,
		IDirect3DDevice9* pD3DDevice,
		TOptions const& vOptions) -> bool override;

	// Vars
	auto GetVarCtrls() const -> TCtrlVar* override;
	auto SetVar(int iVar, void const* pData) -> void override;
	auto GetVar(int iVar) -> void const* override;

	// SetTime
	auto SetTime(float fTime) -> void override;

	// Draw
	auto Apply(TEffectTgt* pEffectTgt) -> bool override;

private:
	// Mosaic
	auto UpdateBlocks() -> void;
	auto FillQuad(TVertex_XYZW* pVertices,
				  int iX, int iY, int sW, int sH) -> void;

	void*	m_pEffectInstance;
	bool	m_bAlpha;
	int		m_iType;
	int		m_iTotal;
	int		m_iWidth;
	int		m_iHeight;
	float	m_afBlocks[MAX_BLOCKS];
	float	m_fIntens;
	float	m_fSize;
	float	m_fInvW;
	float	m_fInvH;
	float	m_fBlockW;
	float	m_fBlockH;
	float	m_fHalfBlockW;
	float	m_fHalfBlockH;
};

#endif//__N3_EFFECTMOSAIC_H__
