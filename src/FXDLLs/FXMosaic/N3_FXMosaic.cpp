//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
//	EffectMosaic
//	DLL Effect example for Neon v3.0.0a
//---------------------------------------------------------------------------//

#include "N3_FXMosaic.h"


enum TMosaicVar {
	VAR_TYPE,
	VAR_INTENSITY,
	VAR_SIZE,
	VAR_ALPHA,
};

//---------------------------------------------------------------------------//
//	Vars for the GUI
//---------------------------------------------------------------------------//
static TCtrlVar m_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_TYPE, "Type", false, 3, { "Stretch WH", "Stretch W", "Stretch H" } },
	{ TCtrlVar::SLIDER, VAR_INTENSITY, "Intens", true, 0, { 0 } },
	{ TCtrlVar::SLIDER, VAR_SIZE, "Size", true, 0, { 0 } },
	{ TCtrlVar::CHECK_BOX, VAR_ALPHA, "Alpha", true, 0, { 0 } },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CEffectMosaic::CEffectMosaic()
	: m_pEffectInstance(nullptr)
	, m_bAlpha(false)
	, m_iType(STRETCH_WH)
	, m_iTotal(0)
	, m_iWidth(0)
	, m_iHeight(0)
	, m_fIntens(0.f)
	, m_fSize(0.f)
	, m_fInvW(0.f)
	, m_fInvH(0.f)
	, m_fBlockW(0.f)
	, m_fBlockH(0.f)
	, m_fHalfBlockW(0.f)
	, m_fHalfBlockH(0.f)
{
	memset(m_afBlocks, 0, sizeof(m_afBlocks[0]) * MAX_BLOCKS);
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CEffectMosaic::Init(
			void* pEffectInstance,
			IDirect3DDevice9* pD3DDevice,
			TOptions const& vOptions) -> bool {

	UNREFERENCED_PARAMETER(vOptions);
	m_fIntens = 0.f;
	m_fSize = 0.2f;
	m_iType = STRETCH_WH;
	m_bAlpha = false;
	m_pUtils->Init(pD3DDevice, MAX_BLOCKS * 6);
	m_pEffectInstance = pEffectInstance;
	UpdateBlocks();
	return true;
}


//---------------------------------------------------------------------------//
//	Destructor
//---------------------------------------------------------------------------//
CEffectMosaic::~CEffectMosaic() {}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CEffectMosaic::GetVarCtrls() const -> TCtrlVar* {
	return m_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CEffectMosaic::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_TYPE: {
			m_iType = *(static_cast<int const*>(pData));
			break;
		}
		case VAR_INTENSITY: {
			m_fIntens = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_SIZE: {
			m_fSize = *(static_cast<float const*>(pData));
			UpdateBlocks();
			break;
		}
		case VAR_ALPHA: {
			m_bAlpha = *(static_cast<bool const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CEffectMosaic::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_TYPE: {
			return &m_iType;
		}
		case VAR_INTENSITY: {
			return &m_fIntens;
		}
		case VAR_SIZE: {
			return &m_fSize;
		}
		case VAR_ALPHA: {
			return &m_bAlpha;
		}
		default: {
			return nullptr;
		}
	}
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CEffectMosaic::SetTime(float fTime) -> void {
	UNREFERENCED_PARAMETER(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CEffectMosaic::Apply(TEffectTgt* pEffectTgt) -> bool {
	auto m_pD3DDevice = m_pUtils->GetD3DDevice();
	// Fill Mode
	D3DVIEWPORT9 vp;
	m_pD3DDevice->GetViewport(&vp);
	m_fBlockW = (vp.Width * m_fInvW);
	m_fBlockH = (vp.Height * m_fInvH);
	m_fHalfBlockW = (m_fBlockW * 0.5f);
	m_fHalfBlockH = (m_fBlockH * 0.5f);

	// Set the source texture, the blend mode, and then draw a screen quad
	IDirect3DSurface9* pSurface = nullptr;
	pEffectTgt->pTexTgt->GetSurfaceLevel(0, &pSurface);
	m_pD3DDevice->SetRenderTarget(0, pSurface);
	m_pD3DDevice->SetTexture(0, pEffectTgt->pTexSrc);
	m_pD3DDevice->Clear(0, NULL, D3DCLEAR_TARGET, 0, 0, 0);
	ApplyBasicShader();
	auto pVertices = m_pUtils->LockVertexBuffer(m_iTotal * 6);
	for (int j = 0; j < m_iHeight; ++j) {
		for (int i = 0; i < m_iWidth; ++i) {
			FillQuad(pVertices, i, j, vp.Width, vp.Height);
			pVertices += 6;
		}
	}
	m_pUtils->UnlockVertexBuffer();
	m_pUtils->DrawPrimitive(D3DPT_TRIANGLELIST, m_iTotal * 2);
	// Return true if we used TexTgt as target for the effect, false if we used pTexSrc
	return true;
}


//---------------------------------------------------------------------------//
//	UpdateBlocks
//---------------------------------------------------------------------------//
auto CEffectMosaic::UpdateBlocks() -> void {
	m_iWidth = static_cast<int>((MAX_WIDTH - MIN_WIDTH) * m_fSize) + MIN_WIDTH;
	m_iHeight = static_cast<int>((MAX_HEIGHT - MIN_HEIGHT) * m_fSize) + MIN_HEIGHT;
	m_fInvW = 1.f / m_iWidth;
	m_fInvH = 1.f / m_iHeight;
	m_iTotal = m_iWidth * m_iHeight;
	for (int i = 0; i < m_iTotal; ++i) {
		m_afBlocks[i] = static_cast<float>(rand()) * (1.f / RAND_MAX);
	}
}


//---------------------------------------------------------------------------//
//	FillQuad
//---------------------------------------------------------------------------//
auto CEffectMosaic::FillQuad(TVertex_XYZW* pVertices,
							 int iX, int iY, int sW, int sH) -> void {
	TVector2 v0, v1, uv0, uv1;
	float fPosIni = m_afBlocks[iX + iY * m_iWidth] * 0.7f;
	float fPosEnd = fPosIni + 0.3f;
	float fSize;
	if (m_fIntens > fPosIni) {
		fSize = (m_fIntens <= fPosEnd) ? (fPosEnd - m_fIntens) / 0.3f : 0.f;
	} else {
		fSize = 1.f;
	}

	unsigned uColor = 0xFFFFFFFF;
	if (m_bAlpha) {
		uColor = (int(fSize * 255.f) << 24) | 0x00FFFFFF;
	}

	float fCX = iX * m_fBlockW + m_fHalfBlockW;
	float fCY = iY * m_fBlockH + m_fHalfBlockH;

	switch (m_iType) {
		case STRETCH_WH: {
			v0.x = fCX - fSize * m_fHalfBlockW;
			v0.y = fCY - fSize * m_fHalfBlockH;
			v1.x = fCX + fSize * m_fHalfBlockW;
			v1.y = fCY + fSize * m_fHalfBlockH;
			break;
		}
		case STRETCH_W: {
			v0.x = fCX - fSize * m_fHalfBlockW;
			v0.y = fCY - 1.f   * m_fHalfBlockH;
			v1.x = fCX + fSize * m_fHalfBlockW;
			v1.y = fCY + 1.f   * m_fHalfBlockH;
			break;
		}
		case STRETCH_H: {
			v0.x = fCX - 1.f   * m_fHalfBlockW;
			v0.y = fCY - fSize * m_fHalfBlockH;
			v1.x = fCX + 1.f   * m_fHalfBlockW;
			v1.y = fCY + fSize * m_fHalfBlockH;
			break;
		}
		default: {
			return;
		}
	}

	uv0.x = (fCX - m_fHalfBlockW) / sW;
	uv0.y = (fCY - m_fHalfBlockH) / sH;
	uv1.x = (fCX + m_fHalfBlockW) / sW;
	uv1.y = (fCY + m_fHalfBlockH) / sH;

	HCV_SET_COLOR(pVertices, uColor);
	HCV_SET_XYZW(pVertices, v0.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, uv0.x, uv0.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, uColor);
	HCV_SET_XYZW(pVertices, v1.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, uv1.x, uv0.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, uColor);
	HCV_SET_XYZW(pVertices, v1.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, uv1.x, uv1.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, uColor);
	HCV_SET_XYZW(pVertices, v0.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, uv0.x, uv0.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, uColor);
	HCV_SET_XYZW(pVertices, v1.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, uv1.x, uv1.y);
	++pVertices;

	HCV_SET_COLOR(pVertices, uColor);
	HCV_SET_XYZW(pVertices, v0.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
	HCV_SET_UV0(pVertices, uv0.x, uv1.y);
	++pVertices;
}



//---------------------------------------------------------------------------//
//
//	DLL Interface
//
//---------------------------------------------------------------------------//


//---------------------------------------------------------------------------//
//	DllMain
//	Main entry point for this DLL
//---------------------------------------------------------------------------//
auto APIENTRY DllMain(
			HANDLE hModule,
			DWORD ul_reason_for_call,
			LPVOID lpReserved) -> BOOL {

	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpReserved);
	switch (ul_reason_for_call) {
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
		default: {
			break;
		}
	}
	return TRUE;
}


//---------------------------------------------------------------------------//
//	Effect_Init
//	Called when the program wants an instance of this effect
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_Init(
			unsigned uVersion,
			void* pEffectInstance,
			IDirect3DDevice9* pD3DDevice,
			TOptions const& vOptions,
			unsigned* pID) -> int {

	int eRet = 0;
	if (uVersion == EFFECT_DLL_VERSION) {
		auto pEffect = new CEffectMosaic;
		if (pEffect->Init(pEffectInstance, pD3DDevice, vOptions)) {
			*pID = reinterpret_cast<unsigned>(pEffect);
		} else {
			eRet = -2;
		}
	} else {
		eRet = -1;
	}

	return eRet;
}


//---------------------------------------------------------------------------//
//	Effect_End
//	Called when the effect is closed
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_End(unsigned uInstance) -> void {
	auto pEffect = reinterpret_cast<CEffectMosaic*>(uInstance);
	delete pEffect;
}


//---------------------------------------------------------------------------//
//	Effect_GetVarCtrls
//	Called when the GUI wants the specs of all vars available
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_GetVarCtrls(unsigned uID) -> TCtrlVar* {
	return reinterpret_cast<CEffectMosaic*>(uID)->GetVarCtrls();
}


//---------------------------------------------------------------------------//
//	Effect_SetVar
//	Called when the GUI sets a var value
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_SetVar(unsigned uID, int iVar, void const* pData) -> void {
	reinterpret_cast<CEffectMosaic*>(uID)->SetVar(iVar, pData);
}


//---------------------------------------------------------------------------//
//	Effect_GetVar
//	Called when the GUI wants a var value
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_GetVar(unsigned uID, int iVar) -> void const* {
	return reinterpret_cast<CEffectMosaic*>(uID)->GetVar(iVar);
}


//---------------------------------------------------------------------------//
//	Effect_SetTime
//	Called to set the time on each frame
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_SetTime(unsigned uID, float fTime) -> void {
	reinterpret_cast<CEffectMosaic*>(uID)->SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Effect_Apply
//	Apply the effect to the effect source texture or the target texture
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_Apply(unsigned uID, TEffectTgt* pEffectTgt) -> bool {
	return reinterpret_cast<CEffectMosaic*>(uID)->Apply(pEffectTgt);
}
