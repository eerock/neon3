//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
//	EffectBasic
//	DLL Effect example for Neon v3.0.0a
//---------------------------------------------------------------------------//

#include "N3_FXBasic.h"


enum EBasicVar {
	VAR_COMBO_BOX,
	VAR_SLIDER,
	VAR_CHECK_BOX,
};

//---------------------------------------------------------------------------//
//static FuncSetBlendMode s_pSetBlendMode;

//----------------------------------------------------------------------------
//	Example Vars for the GUI
//---------------------------------------------------------------------------//
static TCtrlVar m_Vars[] = {
	{ TCtrlVar::COMBO_BOX, VAR_COMBO_BOX, "ComboBox", true, 3, {"Option 1", "Option 2", "Option 3"} },
	{ TCtrlVar::SLIDER, VAR_SLIDER, "Slider", true, 0, {0} },
	{ TCtrlVar::CHECK_BOX, VAR_CHECK_BOX, "CheckBox", true, 0, {0} },
	{ TCtrlVar::INVALID },
};


//---------------------------------------------------------------------------//
//	Constructor
//---------------------------------------------------------------------------//
CEffectBasic::CEffectBasic()
	: m_pEffectInstance(nullptr)
	, m_iComboBox(0)
	, m_fSlider(0.f)
	, m_bCheckBox(false)
{}


//---------------------------------------------------------------------------//
//	End
//---------------------------------------------------------------------------//
CEffectBasic::~CEffectBasic() {
}


//---------------------------------------------------------------------------//
//	Init
//---------------------------------------------------------------------------//
auto CEffectBasic::Init(void* pEffectInstance, IDirect3DDevice9* pD3DDevice, TOptions const& vOptions) -> bool {
	UNREFERENCED_PARAMETER(vOptions);
	m_iComboBox = 0;
	m_fSlider = 0.f;
	m_bCheckBox = false;
	m_pUtils->Init(pD3DDevice, 4);
	m_pEffectInstance = pEffectInstance;
	return true;
}


//---------------------------------------------------------------------------//
//	GetVarCtrls
//---------------------------------------------------------------------------//
auto CEffectBasic::GetVarCtrls() const -> TCtrlVar* {
	return m_Vars;
}


//---------------------------------------------------------------------------//
//	SetVar
//---------------------------------------------------------------------------//
auto CEffectBasic::SetVar(int iVar, void const* pData) -> void {
	switch (iVar) {
		case VAR_COMBO_BOX: {	// magic fuckin numbers
			m_iComboBox = *(static_cast<int const*>(pData));
			break;
		}
		case VAR_SLIDER: {
			m_fSlider = *(static_cast<float const*>(pData));
			break;
		}
		case VAR_CHECK_BOX: {
			m_bCheckBox = *(static_cast<bool const*>(pData));
			break;
		}
		default: {
			break;
		}
	}
}


//---------------------------------------------------------------------------//
//	GetVar
//---------------------------------------------------------------------------//
auto CEffectBasic::GetVar(int iVar) -> void const* {
	switch (iVar) {
		case VAR_COMBO_BOX: {
			return &m_iComboBox;
		}
		case VAR_SLIDER: {
			return &m_fSlider;
		}
		case VAR_CHECK_BOX: {
			return &m_bCheckBox;
		}
		default: {
			break;
		}
	}
	return nullptr;
}


//---------------------------------------------------------------------------//
//	SetTime
//---------------------------------------------------------------------------//
auto CEffectBasic::SetTime(float fTime) -> void {
	UNREFERENCED_PARAMETER(fTime);
}


//---------------------------------------------------------------------------//
//	Apply
//---------------------------------------------------------------------------//
auto CEffectBasic::Apply(TEffectTgt* pEffectTgt) -> bool {
	// This will set the blend mode
	//m_pUtils->GetD3DDevice()->SetTexture(0, pEffectTgt->pTexSrc);
	//s_pSetBlendMode(m_pEffectInstance);
	//m_pUtils->DrawQuad(pOptions->vScr0, pOptions->vScr1, pOptions->vUV0, pOptions->vUV1);
	D3DVIEWPORT9 vp;
	m_pUtils->GetD3DDevice()->GetViewport(&vp);
	IDirect3DSurface9* pSurface = nullptr;
	pEffectTgt->pTexTgt->GetSurfaceLevel(0, &pSurface);
	m_pUtils->GetD3DDevice()->SetRenderTarget(0, pSurface);
	m_pUtils->GetD3DDevice()->SetTexture(0, pEffectTgt->pTexSrc);
	m_pUtils->GetD3DDevice()->Clear(0, NULL, D3DCLEAR_TARGET, 0, 0, 0);
	ApplyBasicShader();
	TVectorI2 v0(vp.X, vp.Y);
	TVectorI2 v1(vp.Width + vp.X, vp.Height + vp.Y);
	m_pUtils->DrawQuad(v0, v1, TVector2(0.f, 0.f), TVector2(1.f, 1.f));
	return true;
}


//---------------------------------------------------------------------------//
//
//	DLL Interface
//
//---------------------------------------------------------------------------//


//---------------------------------------------------------------------------//
//	DllMain
//	Main entry point for this DLL
//---------------------------------------------------------------------------//
auto APIENTRY DllMain(
			HANDLE hModule,
			DWORD ul_reason_for_call,
			LPVOID lpReserved) -> BOOL {

	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpReserved);
	switch (ul_reason_for_call) {
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
		default: {
			break;
		}
	}
	return TRUE;
}


//---------------------------------------------------------------------------//
//	Effect_Init
//	Called when the effect is loaded
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_Init(
			unsigned uVersion,
			void* pEffectInstance,
			IDirect3DDevice9 *pD3DDevice,
			TOptions const& vOptions,
			unsigned* pID) -> int {

	int eRet = 0;
	if (uVersion == EFFECT_DLL_VERSION) {
		auto pEffect = new CEffectBasic();
		if (pEffect->Init(pEffectInstance, pD3DDevice, vOptions)) {
			*pID = reinterpret_cast<unsigned>(pEffect);
		} else {
			eRet = -2;
		}
	} else {
		eRet = -1;
	}
	return eRet;
}


//---------------------------------------------------------------------------//
//	Effect_End
//	Called when the effect is closed
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_End(unsigned uInstance) -> void {
	auto pEffect = reinterpret_cast<CEffectBasic*>(uInstance);
	delete pEffect;
}


//---------------------------------------------------------------------------//
//	Effect_GetVarCtrls
//	Called when the GUI wants the specs of all vars available
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_GetVarCtrls(unsigned uID) -> TCtrlVar* {
	return reinterpret_cast<CEffectBasic*>(uID)->GetVarCtrls();
}


//---------------------------------------------------------------------------//
//	Effect_SetVar
//	Called when the GUI sets a var value
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_SetVar(unsigned uID, int iVar, void const* pData) -> void {
	reinterpret_cast<CEffectBasic*>(uID)->SetVar(iVar, pData);
}


//---------------------------------------------------------------------------//
//	Effect_GetVar
//	Called when the GUI wants a var value
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_GetVar(unsigned uID, int iVar) -> void const* {
	return reinterpret_cast<CEffectBasic*>(uID)->GetVar(iVar);
}


//---------------------------------------------------------------------------//
//	Effect_SetTime
//	Called once each frame
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_SetTime(unsigned uID, float fTime) -> void {
	reinterpret_cast<CEffectBasic*>(uID)->SetTime(fTime);
}


//---------------------------------------------------------------------------//
//	Effect_Apply
//	Apply the effect to the effect source texture or the target texture
//---------------------------------------------------------------------------//
EXPORT_DLL
auto Effect_Apply(unsigned uID, TEffectTgt* pEffectTgt) -> void {
	reinterpret_cast<CEffectBasic*>(uID)->Apply(pEffectTgt);
}
