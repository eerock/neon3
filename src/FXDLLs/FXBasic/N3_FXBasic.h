//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
//	Header file for Neon v3.0.0a DLL Effect development
//---------------------------------------------------------------------------//

#ifndef __N3_EFFECTBASIC_H__
#define __N3_EFFECTBASIC_H__

#include "N3_Utils.h"

//---------------------------------------------------------------------------//
// Input function for setting the blend mode
typedef void(__cdecl* FuncSetBlendMode) (void* pEffectInstance);


//---------------------------------------------------------------------------//
//	Effect Basic - a basic example of a DLL effect.
//---------------------------------------------------------------------------//
class CEffectBasic : public IEffect {
public:
	// Ctor/Dtor
	CEffectBasic();
	~CEffectBasic();

	auto Init(
		void* pEffectInstance,
		IDirect3DDevice9* pD3DDevice,
		TOptions const& vOptions) -> bool override;

	// Vars
	auto GetVarCtrls() const -> TCtrlVar* override;
	auto SetVar(int iVar, void const* pData) -> void override;
	auto GetVar(int iVar) -> void const* override;
	
	// SetTime
	auto SetTime(float fTime) -> void override;

	// Draw
	auto Apply(TEffectTgt* pEffectTgt) -> bool override;

private:
	void*	m_pEffectInstance;
	int		m_iComboBox;
	float	m_fSlider;
	bool	m_bCheckBox;
};

#endif//__N3_EFFECTBASIC_H__
