//---------------------------------------------------------------------------//
//
// Neon v3.0.0a
//
// Copyright (c) 2012-2014 Eric Phister <neon@eerock.com>
// http://neon.eerock.com
//
// Copyright (c) 2006-2008 Jordi Ros <shine.3p@gmail.com>
// http://www.neonv2.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program called license.txt
// If not, see <http://www.gnu.org/licenses/>
//
//---------------------------------------------------------------------------//

//----------------------------------------------------------------------------
//	Neon v3.0.0a Effect ver 3 Common Utils
//	N3_Utils.h - Utils file for creating vertex buffer and locking/unlocking it
//----------------------------------------------------------------------------

#ifndef __N3_UTILS_H__
#define __N3_UTILS_H__

#include <d3d9.h>
#include <vector>
#include <string>

#define EFFECT_DLL_VERSION	3
#define EXPORT_DLL			extern "C" __declspec(dllexport)

#define OffsetOf(s, m)		(size_t)&(((s*)0)->m)

//---------------------------------------------------------------------------//
//	Tools for D3D
//---------------------------------------------------------------------------//
#define	HARD_COLOR_ARGB(a, r, g, b)			D3DCOLOR_ARGB((a), (r), (g), (b))
#define HCV_SET_XYZW(_pV,_fX,_fY,_fZ,_fW)	{ (_pV)->vPos.x = (_fX); (_pV)->vPos.y = (_fY); (_pV)->vPos.z = (_fZ); (_pV)->vPos.w = (_fW); }
#define HCV_SET_COLOR(_pV,_Diffuse)			{ (_pV)->uDiffuse = _Diffuse; }
#define HCV_SET_ARGB(_pV,_uA,_uR,_uG,_uB)	{ (_pV)->uDiffuse = HARD_COLOR_ARGB(_uA,_uR,_uG,_uB); }
#define HCV_SET_UV0(_pV,_fU,_fV)			{ (_pV)->vUV0.x = _fU; (_pV)->vUV0.y = _fV; }
#define HCV_SET_UV1(_pV,_fU,_fV)			{ (_pV)->vUV1.x = _fU; (_pV)->vUV1.y = _fV; }

#define HCV_DECL(Name) struct TVertex_##Name## {
#define HCV_DECL_END() }


//---------------------------------------------------------------------------//
template <class T>
inline auto SafeRelease(T** ppT) -> void {
	if (*ppT) {
		(*ppT)->Release();
		(*ppT) = nullptr;
	}
}


//---------------------------------------------------------------------------//
//	Vector Types
//---------------------------------------------------------------------------//
struct TVector2 {
	TVector2() : x(0.f), y(0.f) {}
	TVector2(float _x, float _y) : x(_x), y(_y) {}
	float x;
	float y;
};

struct TVectorI2 {
	TVectorI2() : x(0), y(0) {}
	TVectorI2(int _x, int _y) : x(_x), y(_y) {}
	int x;
	int y;
};

struct TVector4 {
	TVector4() : x(0.f), y(0.f), z(0.f), w(0.f) {}
	TVector4(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) {}
	float x;
	float y;
	float z;
	float w;
};


//---------------------------------------------------------------------------//
//	Fixed Vertex
//---------------------------------------------------------------------------//
HCV_DECL(XYZW)
	TVector4 vPos;		// Vertice (transformado)
	unsigned uDiffuse;	// Diffuse
	D3DVECTOR vUV0;		// Coordenadas de textura
	D3DVECTOR vUV1;		// Coordenadas de textura
HCV_DECL_END();


//---------------------------------------------------------------------------//
//	Struct for the effect options passed
//---------------------------------------------------------------------------//
struct TEffectTgt {
	IDirect3DTexture9*	pTexSrc;
	IDirect3DTexture9*	pTexTgt;
};

//---------------------------------------------------------------------------//
//	Struct for setting
//---------------------------------------------------------------------------//
struct TCtrlVar {
	enum EType {
		INVALID = -1,
		CHECK_BOX = 0,
		EDIT_BOX,
		COMBO_BOX,
		SLIDER,
		BUTTON,
		MAX_VAR_TYPES,
	};

	static const int SIZE_NAME_STR = 15;
	static const int SIZE_OPTION_STR = 31;
	static const int NUM_OPTIONS_MAX = 16;

	int iType;
	int iNum;
	char szName[SIZE_NAME_STR + 1];
	bool bLinkable;

	// For ComboBox
	int iNumOptions;
	char aszOptions[NUM_OPTIONS_MAX][SIZE_OPTION_STR + 1];
};


typedef std::vector<std::pair<std::string, std::string>> TOptions;


//---------------------------------------------------------------------------//
// CUtils class
//---------------------------------------------------------------------------//
class CUtils {
public:
	CUtils () { m_bOk = false; }
	~CUtils () { End(); }

	auto IsOk () const -> bool { return m_bOk; }
	auto GetD3DDevice () const -> IDirect3DDevice9* { return m_pD3DDevice; }

	//-----------------------------------------------------------------------//
	// Init
	//-----------------------------------------------------------------------//
	auto Init(IDirect3DDevice9* pD3DDevice, int iVertices) -> bool {
		m_pVB = nullptr;
		m_pDecl = nullptr;
		m_pD3DDevice = pD3DDevice;
		m_iVertices = iVertices;

		auto hRet = m_pD3DDevice->CreateVertexBuffer(
			m_iVertices * sizeof(TVertex_XYZW),
			D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
			0,
			D3DPOOL_DEFAULT,
			&m_pVB,
			nullptr
		);

		if (SUCCEEDED(hRet)) {
			D3DVERTEXELEMENT9 Elements[] = {
				{ 0, OffsetOf(TVertex_XYZW, vPos), D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITIONT, 0 },
				{ 0, OffsetOf(TVertex_XYZW, uDiffuse), D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
				{ 0, OffsetOf(TVertex_XYZW, vUV0), D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
				{ 0, OffsetOf(TVertex_XYZW, vUV1), D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
				D3DDECL_END(),
			};

			hRet = m_pD3DDevice->CreateVertexDeclaration(Elements, &m_pDecl);
			if (SUCCEEDED(hRet)) {
				m_bOk = true;
			}
		}
		return IsOk();
	}

	//-----------------------------------------------------------------------//
	// End
	//-----------------------------------------------------------------------//
	void End() {
		if (IsOk()) {
			SafeRelease(&m_pVB);
			SafeRelease(&m_pDecl);
			m_bOk = false;
		}
	}

	//-----------------------------------------------------------------------//
	// LockVertexBuffer
	//-----------------------------------------------------------------------//
	auto LockVertexBuffer(int iVertices) -> TVertex_XYZW* {
		TVertex_XYZW* pVertices = nullptr;
		m_pVB->Lock(0, sizeof(TVertex_XYZW) * iVertices, (void**)&pVertices, D3DLOCK_DISCARD);
		return pVertices;
	}

	//-----------------------------------------------------------------------//
	// UnlockVertexBuffer
	//-----------------------------------------------------------------------//
	auto UnlockVertexBuffer() ->void {
		m_pVB->Unlock();
	}

	//-----------------------------------------------------------------------//
	// DrawPrimitive
	//-----------------------------------------------------------------------//
	auto DrawPrimitive(int iPrimitive, int iNumPrimitives) -> void {
		m_pD3DDevice->SetVertexDeclaration(m_pDecl);
		m_pD3DDevice->SetStreamSource(0, m_pVB, 0, sizeof(TVertex_XYZW));
		m_pD3DDevice->DrawPrimitive((D3DPRIMITIVETYPE)iPrimitive, 0, iNumPrimitives);
	}

	//-----------------------------------------------------------------------//
	// DrawQuad
	//-----------------------------------------------------------------------//
	auto DrawQuad(TVectorI2 const& v0, TVectorI2 const& v1, TVector2 const& uv0, TVector2 const& uv1) -> void {
		auto pVertices = LockVertexBuffer(4);

		HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
		HCV_SET_XYZW(pVertices, v0.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
		HCV_SET_UV0(pVertices, uv0.x, uv0.y);
		++pVertices;

		HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
		HCV_SET_XYZW(pVertices, v1.x - 0.5f, v0.y - 0.5f, 0.0f, 1.0f);
		HCV_SET_UV0(pVertices, uv1.x, uv0.y);
		++pVertices;

		HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
		HCV_SET_XYZW(pVertices, v1.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
		HCV_SET_UV0(pVertices, uv1.x, uv1.y);
		++pVertices;

		HCV_SET_COLOR(pVertices, 0xFFFFFFFF);
		HCV_SET_XYZW(pVertices, v0.x - 0.5f, v1.y - 0.5f, 0.0f, 1.0f);
		HCV_SET_UV0(pVertices, uv0.x, uv1.y);
		++pVertices;

		UnlockVertexBuffer();
		DrawPrimitive(D3DPT_TRIANGLEFAN, 2);
	}

private:
	bool							m_bOk;
	int								m_iVertices;
	IDirect3DDevice9*				m_pD3DDevice;
	LPDIRECT3DVERTEXBUFFER9			m_pVB;
	LPDIRECT3DVERTEXDECLARATION9	m_pDecl;
};


//---------------------------------------------------------------------------//
//	Class IEffect
//---------------------------------------------------------------------------//
class IEffect {
public:
	IEffect() { m_pUtils = new CUtils(); }
	virtual ~IEffect() { delete m_pUtils; m_pUtils = nullptr; }
	virtual auto Init(void*, IDirect3DDevice9*, TOptions const&) -> bool = 0;
	virtual auto GetVarCtrls() const -> TCtrlVar* = 0;
	virtual auto SetVar(int iVar, void const* pData) -> void = 0;
	virtual auto GetVar(int iVar) -> void const* = 0;
	virtual auto SetTime(float fTime) -> void = 0;
	virtual auto Apply(TEffectTgt* pEffectTgt) -> bool = 0;
	virtual auto ApplyBasicShader() -> void {
		IDirect3DDevice9* m_pD3DDevice = m_pUtils->GetD3DDevice();
		m_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		m_pD3DDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		m_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
		m_pD3DDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		m_pD3DDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	}

protected:
	CUtils* m_pUtils = nullptr;

private:
};

#endif//__N3_UTILS_H__
