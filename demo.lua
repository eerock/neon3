﻿-------------------------------------------------------------------------------
-- NeonPlayer LUA Function Reference:
--
-- Setup(float duration, float startTime, int numSources, bool enableFPS);
--		This function initializes the player.
--		duration: length of time in seconds to run the player
--		startTime: time at which the player starts (needs more info)
--		numSources: number of source layers to initialize (needs code fix, currently hardcoded at 10)
--		enableFPS: bool to enable/disable the fps and time debug display
--
-- Start();
--		This function kicks off event processing, and starts audio file playback.
--		Call this last in your script, after Setup and after AddEvent calls.
--
-- srcID = LoadMedia(string path);
--		This function loads a source, same as dragging one into the main grid.
--		path: string path to the NLY file to load, relative to Neon exe folder.
--
-- LoadMusic(string path);
--		** DO NOT USE, NOT WORKING YET **
--		This function loads a sound file for playing.
--		path: string path to the sound file, relative to Neon exe folder.
--
-- AddEvent(...);
--		todo: Give a full account of eventids and params to this function
--		eventID 2 => Attach to master output
--			AddEvent(eventAttach, flt startTime, int srcID, int layer);
--		eventID 3 => Unattach from master output
--			AddEvent(eventUnattach, flt startTime, int srcID)
--		eventID 4 => Reset
--		eventID 5 => Set Speed
--		eventID 6 => Set Alpha
--		eventID 7 => Set Time
--		eventID 8 => Set Blend
--		eventID 9 => Set Quality
--		eventID 10 => Set Fade
--		eventID 11 => Add Effect
--			AddEvent(eventAddEffect, flt startTime, int srcID, str fxName, int slot)
--		eventID 12 => Delete Effect
--			AddEvent(eventDelEffect, flt startTime, int srcID, int slot)
--		eventID 13 => Swap Effects
--			AddEvent(eventSwpEffects, flt startTime, int srcID, int slot1, int slot2)
--		eventID 14 => Set Var
-------------------------------------------------------------------------------

eventAttach = 2
eventUnattach = 3
eventReset = 4
eventSetSpeed = 5
eventSetAlpha = 6
eventSetTime = 7
eventSetBlend = 8
eventSetQuality = 9
eventSetFade = 10
eventAddEffect = 11
eventDelEffect = 12
eventSwpEffects = 13
eventSetVar = 14

Setup(5.0, 0.0, 5, true)

src1 = LoadMedia("Media/Images/Gif_Triangle.nsd")
AddEvent(eventSetFade, 0.0, src1, 0.0)
AddEvent(eventAttach, 0.0, src1, 0)

src2 = LoadMedia("Media/3DScenes/3D_Cube/3D_Cube2.nsd")
AddEvent(eventSetFade, 1.0, src2, 0.0)
AddEvent(eventAttach, 1.0, src2, 1)
AddEvent(eventAddEffect, 1.0, src2, "Pixelate - Brick", 0)
AddEvent(eventAddEffect, 1.0, src2, "Neon - Spriter - Sphere", 1)
AddEvent(eventSwpEffects, 2.0, src2, 0, 1)
AddEvent(eventDelEffect, 3.0, src2, 0)
AddEvent(eventDelEffect, 3.0, src2, 1)
AddEvent(eventUnattach, 4.0, src2)

Start()
