# Neon #

Neon is a video & media mixing application for Windows.  Neon can play many media types and uses ffmpeg libraries for decoding files.  The main application window is displayed using OpenGL and the display engine uses DirectX9.  The next version of this application will likely be pure OpenGL and work will be made to port it to Linux and OS X.

### History ###

* Not much is known about [version 1](http://xplsv.com/neon/), but that project was inherited by Jordi Ros who coded [version 2](http://vjforums.info/threads/new-vjtool-neonv2-released.14818/#post-120879) around 2006 and was developed by him until in 2008.  [Version 2.5](http://neonv2.com) was to be a major update but was never fully finished.  Jordi released the source code and stopped working on it to pursue other interests.
* In 2012 I became interested in doing live visuals & video mixing and found Neon through [VJForums.info](http://vjforums.info).  After a night or two I got it to compile, and kept working on it as a pet project.
* Around that time I contacted Charles (charles@photropik.com) who had been a user of Neon and was interested in helping get the effects library put back together.
* Today, I still maintain the source code for what's now deemed 'neon3'.  This bitbucket repo is the main page for the project's development, and the website for Neon can be found [here](http://neon.eerock.com).

### How do I get set up? ###

* Everything has been converted to build in Visual Studio 2013.  Clone or Fork the repo and open up '<neon_root>\src\neon3.sln'.
* Set 'Neon' as the startup project (if it is not already).
* Set **Configuration Properties -> Debugging -> Environment**: 
```
#!txt
PATH=%PATH%;$(SolutionDir)..\x64
```
  or  
```
#!txt
PATH=%PATH%;$(SolutionDir)..\x86
```

* Set **Configuration Properties -> Debugging -> Working Directory**: 
```
#!txt

$(SolutionDir)..
```

* Neon depends on the following external libraries: Boost v1.55.0, Lua v5.2, tinyxml v2.6.2, FFMpeg, DirectX SDK (June 2010), Portaudio, and Oscpack.  All but DirectX are included in the project.
* To package up Neon into a distributable form, run 
```
#!txt
<neon_root>\copy_builds.bat
```
It will copy builds to 
```
#!txt
<neon_root>\__cur_build\x86
```
 and 
```
#!txt
<neon_root>\__cur_build\x64
```

* There are also VC++2013 redistributable package installers in 
```
#!txt

<neon_root>\x64
```
 and 
```
#!txt

<neon_root>\x86
```


### Contribution guidelines ###

* Writing tests - yes please :)
* Code review - just try to follow the existing coding style.
* Pull requests - if you want to get involved, let's talk!
* Other guidelines - work on converting old stdc code to at least C++ stl equivalents.  Wherever possible introduce modern C++11 and later concepts.
* OpenGL fanatics - please apply.  Wishlist includes rewriting the Graphics project in modern OpenGL and GLSL.
* Systems gurus - please also apply.  Wishlist includes making neon3 properly multi-threaded and utilizing GPU and GPU more.
* UI cowboys - MGui project isn't flexible enough to scale the main application UI.

### Contact ###

* Eric aka eerock: [neon@eerock.com](mailto:neon@eerock.com).
* [neon.eerock.com](http://neon.eerock.com)