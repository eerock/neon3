// Distort - Twirl

float V_Time : TIME;
float V_STime : SINTIME;
float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;
float V_Var4 : VAR4;
float4 color;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = V_Var1 * 10.0;
	float beat = V_Time * (V_Var2 * 10.0);
	float beatspeed = V_Var3;
	float gamma = V_Var4;
	float3 zyg;
	zyg.xy = In.Tex0.xy;
	zyg.z = 0.5;
	zyg -= 0.5;
	float a = atan2(zyg.y, zyg.x);
	float r = length(zyg);
	a += r * sin(beat * 0.25) * power;
	float2 p;
	p.x = 0.5 + cos(a) * r;
	p.y = 0.5 + sin(a) * r;
	return tex2D(sampler0, p);
}

technique Twirl {
	pass PassTwirl {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
