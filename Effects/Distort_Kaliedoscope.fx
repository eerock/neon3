// Distort - Kaliedoscope

float screenW : SCREENW;
float screenH : SCREENH;
float Divisions : VAR1;
float Iterations : VAR2;
float Rotate : VAR3;
float Zoom : VAR4;
float CenterX : VAR5;
float CenterY : VAR6;
float IterationZoom : VAR7;
float CellOffsetX : VAR8;
float CellOffsetY : VAR9;
float CellRotate : VAR10;
float2 CellScale : VAR11;
bool Aspect : VAR12;
bool Filter : VAR13;
bool processAlpha : VAR16;
float4 BorderCol : COLOR = 0.0;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 r2d(float2 x, float a) {
	a *= acos(-1.0) * 2.0;
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

float2 kal(float2 x, float sz) {
	float2 dx = (x - 0.5);
	float an = atan2(dx.x, -dx.y) / acos(-1.0) / 2.0 + 0.5;
	float2 xx = r2d(dx, floor(an * sz + 0.5) / sz);
	xx.x = abs(xx.x);
	xx += 0.5;
	return xx;
}

// zoom vs
void vs2d(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {
	float2 R = { screenW, screenH };
	vp.xy *= 2.0;
	uv += 0.5 / R;
}

// default vs
void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In, float2 vp : VPOS) : COLOR {
	float2 R = { screenW, screenH };
	float2 asp = lerp(1.0, R / R.x, Aspect);
	float2 CellOffset = float2(CellOffsetX, CellOffsetY);
	float2 x = (vp) / R;
	float sz = round(Divisions);
	float zz = pow(2.0, Zoom * 5.0 - 1.0);
	float2 Off = float2(CenterX, CenterY);
	float2 dx = r2d((x - 0.5 + Off) * asp, Rotate) * zz + 0.5;
	float2 xx = kal(dx, sz);
	for (float i = 0; i < min(Iterations - 1.0, 30.0); i++) {
		xx *= pow(2.0, IterationZoom * 0.1);
		if (xx.y > 1) {
			xx = kal(float2(xx.x, 2.0 - xx.y), sz);
		}
	}
	xx = (xx - 0.5) / asp + 0.5;
	xx = r2d(xx - 0.5, CellRotate - Rotate) / asp + 0.5;
	xx += CellOffset;
	xx = (xx - 0.5) * CellScale + 0.5;
	float4 c = tex2D(sampler0, xx);
	if (Filter) {
		c = tex2D(sampler0, xx);	// wut?
	}
	return c;
}

technique Kaliedoscope {
	pass PassKaliedoscope {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();	// vs2d();
		pixelshader = compile ps_3_0 PS();
	}
}
