// Photo - Technicolor 3

float V_Var1 : VAR1;
float4 redfilter : COLOR = { 1.0, 0.0, 0.0, 0.0 };
float4 greenfilter : COLOR = { 0.0, 1.0, 0.0, 0.0 };
float4 bluefilter : COLOR = { 0.0, 0.0, 1.0, 0.0 };
float4 redorangefilter : COLOR = { 0.99, 0.263, 0.0, 0.0 };
float4 cyanfilter : COLOR = { 0.0, 1.0, 1.0, 0.0 };
float4 magentafilter : COLOR = { 1.0, 0.0, 1.0, 0.0 };
float4 yellowfilter : COLOR = { 1.0, 1.0, 0.0, 0.0 };
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float4 filtgreen = col * greenfilter;
	float4 filtblue = col * magentafilter;
	float4 filtred = col * redorangefilter;
	float4 rednegative = float((filtred.r + filtred.g + filtred.b) / 3.0);
	float4 greennegative = float((filtgreen.r + filtgreen.g + filtgreen.b) / 3.0);
	float4 bluenegative = float((filtblue.r + filtblue.g + filtblue.b) / 3.0);
	float4 redoutput = rednegative + cyanfilter;
	float4 greenoutput = greennegative + magentafilter;
	float4 blueoutput = bluenegative + yellowfilter;
	float4 result = redoutput * greenoutput * blueoutput;
	result.a = col.a;
	float4 finalmix = lerp(col, result, V_Var1);
	if (!processAlpha) {
		finalmix.a = 1.0;
	}
	return finalmix;
}

technique Technicolor3 {
	pass PassTechnicolor3 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
