// Color - Pass

float vr: VAR1;
float vg: VAR2;
float vb: VAR3;
float strength: VAR4;
bool bypass: VAR15;
bool processAlpha: VAR16;

struct VS_OUTPUT {
	float4 Position: POSITION;
	float4 Color: COLOR0;
	float3 tex0: TEXCOORD0;
};

sampler s0 = sampler_state {
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

float4 PS(VS_OUTPUT In, float2 Tex: TEXCOORD0): COLOR0 {
	float4 passColor = float4(vr, vg, vb, 1.0);
	float4 col = tex2D(s0, In.tex0);
	float g = (col.x + col.y + col.z) * 0.333;
	float4 filter = float4(g, g, g, col.w);
	float d = 0.0;
	d += (col.x - passColor.x) * (col.x - passColor.x);
	d += (col.y - passColor.y) * (col.y - passColor.y);
	d += (col.z - passColor.z) * (col.z - passColor.z);
	float f = 0.0;
	if (d > strength) {
		f = 1.0;
	}
	if (d <= strength) {
		f = d /= strength;
	}
	float4 output;
	output = passColor + f * (filter - passColor);
	if (!processAlpha) {
		output.w = 1.0;
	}
	return output;
}

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

technique ColorPass {
	pass P0 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
