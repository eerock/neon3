// Photo - Sepia

float mix : VAR1;
float intensity : VAR2;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSA(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	float c = (color.r + color.g + color.b) / 3.0;
	color.r = ((c * 235.0 / 255.0) - color.r) * mix + color.r;
	color.g = ((c * 210.0 / 255.0) - color.g) * mix + color.g;
	color.b = ((c * 130.0 / 255.0) - color.b) * mix + color.b;
	color.a *= mix;
	return color;
}

float4 PSB(VS_OUTPUT In) : COLOR {
	float Brightness = 1.23;
	float4x4 YIQ = {
		0.299, 0.587, 0.114, 0.0,
		0.596, -0.275, -0.321, 0.0,
		0.212, -0.523, 0.311, 0.0,
		0.0, 0.0, 0.0, 0.0
	};
	float4x4 IYIQ = {
		1.0, 0.95568806036115671171, 0.61985809445637075388, 0.0,
		1.0, -0.27158179694405859326, -0.64687381613840131330, 0.0,
		1.0, -1.1081773266826619523, 1.7050645599191817149, 0.0,
		0.0, 0.0, 0.0, 0.0
	};
	float4 s = tex2D(sampler0, In.Tex0);
	float4 r = mul(YIQ, s);
	r.y = intensity;
	r.z = 0.0;
	float4 col = Brightness * mul(IYIQ, r);
	col.a = mix * s.a;
	return col;
}

technique SepiaA {
	pass PassSepiaA {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSA();
	}
}

technique SepiaB {
	pass PassSepiaB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSB();
	}
}
