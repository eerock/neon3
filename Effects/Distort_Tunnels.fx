// Distort - Tunnels
// Neon port by charles@photropik.com
// October 2012

float screenW : SCREENW;
float screenH : SCREENH;
float time : TIME;
float Dist : VAR1;
float Rotate : VAR2;
bool animateRotation : VAR3;
float vRotationalSpeed : VAR4;
float vZoom : VAR5;
bool animateZoom : VAR6;
float vZoomSpeed : VAR7;
float FogDistance : VAR8;
float Offset : VAR9;
float4 ColorA = { 0.0, 0.0, 0.0, 1.0 };

bool processAlpha : VAR16;

float2 r2d(float2 x, float a) {
	a *= acos(-1.0) * 2.0;
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

// zoom vs
void vs2d(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {
	float2 R = { screenW, screenH };
	vp.xy *= 2.0;
	uv += 0.5 / R;
}

// blank vs
void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSFly(VS_OUTPUT In) : COLOR {
	float2 R = { screenW, screenH };
	float2 p = -1.0 + 2.0 * In.Tex0;
	float2 uv;
	float zoom = vZoom;
	if (animateZoom) {
		zoom = zoom + time * vZoomSpeed;
	}

	float rotate = Rotate;
	if (animateRotation) {
		rotate = time * vRotationalSpeed;
	}

	float x = p.x * cos(rotate) - p.y * sin(rotate);
	float y = p.x * sin(rotate) + p.y * cos(rotate);
	uv.x = 0.25 * x / abs(y);
	uv.y =  zoom + 0.25 / abs(y);

	float4 mixout = float4(tex2D(sampler0, uv).xyzw * pow(abs(y), Dist * 2.0));
	if (!processAlpha) {
		mixout.a = 1.0;
	}
	return mixout;
}

float4 PSTunnelFly(float2 vp : VPOS) : COLOR {
	float2 R = { screenW, screenH };
	float2 p = -1.0 + 2.0 * vp / R;
	float2 uv;
	float an = Rotate;
	float x = p.x * cos(an) - p.y * sin(an);
	float y = p.x * sin(an) + p.y * cos(an);
	uv.x = 0.25 * x / abs(y) * Dist + (Offset + 0.5);
	uv.y = 0.25 * time + 0.25 / abs(y) * Dist;
	float4 col = tex2D(sampler0, uv);
	return lerp(col, ColorA, 1.0 / (1.0 + (FogDistance * 10.0) * pow(y / Dist, 2.0)));
}

float4 PSTunnelSquare(float2 vp : VPOS) : COLOR {
	float2 R = { screenW, screenH };
	float2 p = -1.0 + 2.0 * vp / R;
	float2 pRot = r2d(p, Rotate);
	float2 uv;
	float r = pow(pow(pRot.x * pRot.x, 3.0) + pow(pRot.y * pRot.y, 3.0), 1.0 / (Dist * 8.0));
	uv.x = 0.5 * time + 0.5 / r;
	uv.y = (atan2(pRot.y, pRot.x) / 3.1416 + Offset);
	float4 col = tex2D(sampler0, uv);
	return lerp(col, ColorA, 1.0 / (1.0 + (FogDistance * 10.0) * pow(r / Dist, 2.0)));
}

float4 PSTunnelCylinder(float2 vp : VPOS) : COLOR {
	float2 R = { screenW, screenH };
	float2 p = -1.0 + 2.0 * vp / R;
	float2 uv;
	float a = atan2(p.y, p.x);
	float r = sqrt(dot(p, p)) * (1.0 - Dist);
	uv.x = 0.5 * time + 0.5 / r;
	uv.y = (a / 3.1416) + Rotate;
	float4 col = tex2D(sampler0, uv);
	return lerp(col, ColorA, 1.0 / (1.0 + (FogDistance * 10.0) * pow(r / Dist, 2.0)));
}

technique Fly {
	pass PassFly {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSFly();
	}
}

technique TunnelFly {
	pass PassTunnelFly {
		vertexshader = compile vs_3_0 vs2d();
		pixelshader = compile ps_3_0 PSTunnelFly();
	}
}

technique TunnelSquare {
	pass PassTunnelSquare {
		vertexshader = compile vs_3_0 vs2d();
		pixelshader = compile ps_3_0 PSTunnelSquare();
	}
}

technique TunnelCylinder {
	pass PassTunnelCylinder {
		vertexshader = compile vs_3_0 vs2d();
		pixelshader = compile ps_3_0 PSTunnelCylinder();
	}
}
