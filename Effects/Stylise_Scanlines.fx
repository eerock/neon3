// Stylize - Scanlines

float size : VAR1;
float speed : VAR2;
float time : TIME;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float normaliseSize = size * 10.0;
	float animationSpeed = 1.0 + speed * time;
	float lineSize = normaliseSize * animationSpeed;
	float4 c = tex2D(sampler0, In.Tex0);
	c.r *= sin(In.Tex0.y * 100.0 * lineSize) * 2.0;
	c.g *= cos(In.Tex0.y * 200.0 * lineSize) * 2.0;
	c.b *= sin(In.Tex0.y * 300.0 * lineSize) * 2.0;
	return c;
}

technique Scanlines {
	pass PassScanlines {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
