//--------------------------------------------------------------------------------------
// 
// WPF ShaderEffect HLSL -- HardMixEffect
//
//--------------------------------------------------------------------------------------

#define BlendColorDodgef(base, blend)	((blend == 1.0) ? blend : min(base / (1.0 - blend), 1.0))
#define BlendColorBurnf(base, blend)	((blend == 0.0) ? blend : max((1.0 - ((1.0 - base) / blend)), 0.0))
#define BlendVividLightf(base, blend)	((blend < 0.5) ? BlendColorBurnf(base, (2.0 * blend)) : BlendColorDodgef(base, (2.0 * (blend - 0.5))))
#define BlendHardMixf(base, blend)		((BlendVividLightf(base, blend) < 0.5) ? 0.0 : 1.0)

float Src02Alpha : VAR1;

sampler input = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

sampler blend = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float4 Src01Cd: TEXCOORD0;
	float4 Src02Cd: TEXCOORD1;
};

void VS(inout float4 vp : POSITION, inout float2 uv0 : TEXCOORD0, inout float2 uv1 : TEXCOORD1) {}

// todo: use VS_OUTPUT and use the two Tex0 and Tex1 coords.
float4 PS(float2 uv : TEXCOORD0) : COLOR {
	float4 inputColor;
	inputColor = tex2D(input, uv);

	float4 blendColor;
	blendColor = tex2D(blend, uv);

	inputColor.r = BlendHardMixf(inputColor.r, blendColor.r);
	inputColor.g = BlendHardMixf(inputColor.g, blendColor.g);
	inputColor.b = BlendHardMixf(inputColor.b, blendColor.b);

	return inputColor;
}

technique HalftoneBitmap {
	pass PassHalftoneBitmap {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
