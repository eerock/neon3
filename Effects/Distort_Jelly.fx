// Distort - Jelly

float V_Time : TIME;
float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;
float4 color;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = V_Var1;
	float beat = V_Time * (V_Var2 * 10.0);
	float speed = V_Var3;
	float2 zyg0, p;
	float z = power * 0.5;
	float b = beat * speed;
	zyg0.x = cos(b * 0.51212) + 0.5;
	zyg0.y = sin(b * 0.49512) + 0.5;
	zyg0 -= In.Tex0;
	p.x = sin(zyg0.x * 2.0) * z;
	p.y = sin(zyg0.y * 2.0) * z;
	p += In.Tex0;
	p.x = (p.x + 100.0) % 1.0;
	p.y = (p.y + 100.0) % 1.0;
	float4 ctex = tex2D(sampler0, p);
	return ctex;
}

technique Jelly {
	pass PassJelly {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
