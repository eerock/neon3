// Stylize - ASCII

float screenW : SCREENW;
float screenH : SCREENH;
bool Grayscale : VAR1;
bool Filter : VAR14;
bool Aspect : VAR15;
bool processAlpha : VAR16;
float2 R;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
	float2 Tex1 : TEXCOORD1;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

sampler sampler1 = sampler_state {
	Filter = MIN_MAG_MIP_POINT;
};

float mx(float3 p) {
	return max(p.x, max(p.y, p.z));
}

// this does a zoom in, mostly undesirable as there's no way to control it...
void vs2d(inout float4 vp : POSITION0, inout float2 uv : TEXCOORD0) {
	float2 R = float2(screenW, screenH);
	vp.xy *= 2.0;
	uv += 0.5 / R;
}

void VS(inout float4 vp : POSITION, inout float2 uv0 : TEXCOORD0, inout float2 uv1 : TEXCOORD1) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 R = { screenW, screenH };
	float2 vp = In.Tex0 * R - 0.25;
	
	float2 sz = float2(8.0, 12.0);
	float4 c = tex2D(sampler0, floor(vp / sz) * sz / R + 0.5 / R);
	float grey = mx(c.rgb);
	grey = pow(grey, 5.0);
	float letter = tex2D(sampler1, (frac(vp / sz) + float2(grey * 176.0, 0.0)) / float2(176.0, 1.0));
	c.rgb = normalize(c.rgb) * sqrt(3.0) * letter;
	if (Grayscale) {
		c.rgb = letter;
	}
	return c;
}

technique Ascii {
	pass PassAscii {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		AddressU[1] = CLAMP;
		AddressV[1] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
