// Neon - Blend
// This shader might be unused.

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	if (color.rgba < 128) {	// wrong, doesn't evaluate to a scalar.
		color.rgba *= 2;
	}
	return color;
}

technique Blend {
	pass PassBlend {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
