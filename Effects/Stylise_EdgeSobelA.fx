// Stylize - Sobel Edge Detection (One)

float screenW : SCREENW;
float screenH : SCREENH;

float vSampleWidth : VAR1;
float vSampleHeight : VAR2;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 sobelEdgeDetection(float2 cTex, sampler sTex, float width, float height) {
	width = vSampleWidth / 100.0;
	height = vSampleHeight / 100.0;
	float2 sampleOffsets[8] = { -width, -height, 0.0, -height, width, -height, -width, 0.0, width, 0.0, -width, height, 0.0, height, width, height, };

	int i = 0;
	float4 c = 0.5;
	float2 texCoords;
	float4 texSamples[8];
	float4 vertGradient;
	float4 horzGradient;
	for (i = 0; i < 8; i++) {
		texCoords = cTex + sampleOffsets[i]; 
		texSamples[i] = tex2D(sTex, texCoords);
		texSamples[i] = dot(texSamples[i], 0.333333);
	}
	vertGradient = -(texSamples[0] + texSamples[5] + 2.0 * texSamples[3]);
	vertGradient += (texSamples[2] + texSamples[7] + 2.0 * texSamples[4]);
	horzGradient = -(texSamples[0] + texSamples[2] + 2.0 * texSamples[1]);
	horzGradient += (texSamples[5] + texSamples[7] + 2.0 * texSamples[6]);
	c = sqrt(horzGradient * horzGradient + vertGradient * vertGradient);
	if (!processAlpha) {
		c.a = 1.0;
	}
	return c;
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

// ebp-> todo: remove the second parameter?
// then why do we have VS_OUTPUT In.Tex0?  Is it just an alias of float2 TEXCOORD0 from
// VS_OUTPUT float4 tex0 : TEXCOORD0.  Are they the same thing?
float4 PS(VS_OUTPUT In) : COLOR {
	return sobelEdgeDetection(In.Tex0, sampler0, float(screenW), float(screenH));
}

technique SobelOne {
	pass PassSobelOne {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
