// Stylise - Sobel Edge Detection (Two)

float V_Alpha : ALPHA;
float V_InvScreenW : INVSCREENW;
float V_InvScreenH : INVSCREENH;
float V_Type : VAR1;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 tex0 : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 Color;
	float4 _11 = tex2D(sampler0, In.Tex0 + float2(-V_InvScreenW, -V_InvScreenH));
	float4 _12 = tex2D(sampler0, In.Tex0 + float2(0.0, -V_InvScreenH));
	float4 _13 = tex2D(sampler0, In.Tex0 + float2(V_InvScreenW, -V_InvScreenH));
	float4 _21 = tex2D(sampler0, In.Tex0 + float2(-V_InvScreenW, 0.0));
	float4 _23 = tex2D(sampler0, In.Tex0 + float2(V_InvScreenW, 0.0));
	float4 _31 = tex2D(sampler0, In.Tex0 + float2(-V_InvScreenW, V_InvScreenH));
	float4 _32 = tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH));
	float4 _33 = tex2D(sampler0, In.Tex0 + float2(V_InvScreenW, V_InvScreenH));
	float4 x = -_11 - (2.0 * _21) - _31 + _13 + (2.0 * _23) + _33;
	float4 y = +_11 + (2.0 * _12) + _13 - _31 - (2.0 * _32) - _33;
	float c = sqrt((x * x) + (y * y));
	if (V_Type < 1.0) {
		Color.r = Color.g = Color.b = c;
	} else if (V_Type < 2.0) {
		Color.r = Color.g = Color.b = 1.0 - c;
	} else if (V_Type < 3.0) {
		Color = tex2D(sampler0, In.Tex0) + float4(c, c, c, c);
	} else if (V_Type <= 4.0) {
		Color = tex2D(sampler0, In.Tex0) - float4(c, c, c, c);
	}
	if (!processAlpha) {
		Color.a = 1.0;
	}
	return Color;
}

technique SobelTwo {
	pass PassSobelTwo {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
