// Neon - Blur

float V_InvScreenW : INVSCREENW;
float V_InvScreenH : INVSCREENH;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSX(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 1.0, 0.0)); //0.42;
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 1.0, 0.0)); //0.42;
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 2.0, 0.0)); //0.35;
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 2.0, 0.0)); //0.35;
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 3.0, 0.0)); //0.23;
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 3.0, 0.0)); //0.23;
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 4.0, 0.0)); //0.19;
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 4.0, 0.0)); //0.19;
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 5.0, 0.0)); //0.14;
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 5.0, 0.0)); //0.14;
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 6.0, 0.0)); //0.10;
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 6.0, 0.0)); //0.10;
	color *= 1.0 / 13.0;
	//color.a = 1.0;
	return color;
}

// the BlurXGlow.fx one did this at the end...
 	//color.a /= 13.0;
	//color *= 0.2;
	//color = color.r * color.g * color.b / 2;	// 3?
	//color *= color.a;

float4 PSY(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	color += tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH * 1.0)); //0.42;
	color += tex2D(sampler0, In.Tex0 - float2(0.0, V_InvScreenH * 1.0)); //0.42;
	color += tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH * 2.0)); //0.35;
	color += tex2D(sampler0, In.Tex0 - float2(0.0, V_InvScreenH * 2.0)); //0.35;
	color += tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH * 3.0)); //0.23;
	color += tex2D(sampler0, In.Tex0 - float2(0.0, V_InvScreenH * 3.0)); //0.23;
	color += tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH * 4.0)); //0.19;
	color += tex2D(sampler0, In.Tex0 - float2(0.0, V_InvScreenH * 4.0)); //0.19;
	color += tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH * 5.0)); //0.14;
	color += tex2D(sampler0, In.Tex0 - float2(0.0, V_InvScreenH * 5.0)); //0.14;
	color += tex2D(sampler0, In.Tex0 + float2(0.0, V_InvScreenH * 6.0)); //0.10;
	color += tex2D(sampler0, In.Tex0 - float2(0.0, V_InvScreenH * 6.0)); //0.10;
	color = color * 1.0 / 13.0;
	//color.a = 1.0;
	return color;
}

technique BlurX {
	pass PassBlurX {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSX();
	}
}

technique BlurY {
	pass PassBlurY {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSY();
	}
}
