// Distort - Ripple

float XPos : VAR1;
float YPos : VAR2;
float distortion : VAR3;
float speed : VAR4;
float time : TIME;
float Timer = 3.14159 / 0.75;
bool bypass : VAR15;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	//float4 Color : COLOR0;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float sinTime = sin(time) * (speed * 20.0);
	float animDistortion = distortion + sinTime;
	float2 center = { XPos, YPos };
	float2 distance = abs(In.Tex0 - center);
	float scalar = length(distance);
	scalar = abs(1 - scalar);
	float sinoffset = sin(Timer / scalar);
	sinoffset = clamp(sinoffset, 0.0, 1.0);
	float sinsign = cos(Timer / scalar);
	sinoffset = sinoffset * animDistortion / 32.0;
	float4 col = tex2D(sampler0, In.Tex0 + (sinoffset * sinsign));
	return col;
}

technique Ripple {
	pass PassRipple {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
