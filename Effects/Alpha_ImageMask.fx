// Alpha - Image Mask
// Adapted for Neon by charles@photropik.com from code found at vvvv.org
//
float AlphaAmount : VAR1;
float MaskAlphaAmount : VAR2;
bool invertMask : VAR3;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 TexCd : TEXCOORD0;
	float2 MaskCd : TEXCOORD1;
};

sampler SourceSampler = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

sampler MaskSampler = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp : POSITION, inout float2 uv0 : TEXCOORD0, inout float2 uv1 : TEXCOORD1) {}

float4 PS(VS_OUTPUT In): COLOR {
	// Source image
	float4 col = tex2D(SourceSampler, In.TexCd);
	// Mask image. This is specified in the NF2
	float4 maskcol = tex2D(MaskSampler, In.MaskCd);
	// Create an empty texture
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	// Calculate a black and white image 
	float luminance = (0.3 * maskcol.r + 0.59 * maskcol.g + 0.11 * maskcol.b);
	// Calculate the alpha channel level with a lerp and input from a slider value
	float maskalpha = lerp(luminance, 1.0, 1.0 - MaskAlphaAmount);
	
	outColor = col;
	
	float alpha = mul(col.a, maskalpha);
	// Invert the mask if required
	if (invertMask) {
		alpha = 1.0 - alpha;
	}
	// Fade the effect in or out.
	outColor.a = mul(alpha, AlphaAmount);
	return outColor;
}

technique ImageMask {
	pass PassImageMask {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
