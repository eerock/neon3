// Color - Colorize RGB

float level : VAR1;
float red : VAR2;
float green : VAR3;
float blue : VAR4;
float processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	float c = (color.r + color.g + color.b) * 0.333333333;
	color.r = (c * red - color.r) * level + color.r;
	color.g = (c * green - color.g) * level + color.g;
	color.b = (c * blue - color.b) * level + color.b;
	if (!processAlpha) {
		color.a = 1.0;
	}
	return color;
}

technique ColorizeRGB {
	pass PassColorizeRGB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
