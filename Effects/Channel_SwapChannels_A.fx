// Channel - Swap Channels A

bool cutRed : VAR1;
bool cutGreen : VAR2;
bool cutBlue : VAR3;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0: TEXCOORD0;
};

void VS(inout VS_OUTPUT vs_out) {}

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 PS_RGB(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	if (cutRed) {
		col.r = 0.0;
	}
	if (cutGreen) {
		col.g = 0.0;
	}
	if (cutBlue) {
		col.b = 0.0;
	}
	return col;
}

float4 PS_RBG(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	if (cutRed) {
		col.r = 0.0;
	}
	if (cutGreen) {
		col.g = 0.0;
	}
	if (cutBlue) {
		col.b = 0.0;
	}
	col.rbg = col.rgb;
	return col;
}

float4 PS_GBR(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	if (cutRed) {
		col.r = 0.0;
	}
	if (cutGreen) {
		col.g = 0.0;
	}
	if (cutBlue) {
		col.b = 0.0;
	}
	col.gbr = col.rgb;
	return col;
}

float4 PS_GRB(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	if (cutRed) {
		col.r = 0.0;
	}
	if (cutGreen) {
		col.g = 0.0;
	}
	if (cutBlue) {
		col.b = 0.0;
	}
	col.grb = col.rgb;
	return col;
}

float4 PS_BRG(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	if (cutRed) {
		col.r = 0.0;
	}
	if (cutGreen) {
		col.g = 0.0;
	}
	if (cutBlue) {
		col.b = 0.0;
	}
	col.brg = col.rgb;
	return col;
}

float4 PS_BGR(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	if (cutRed) {
		col.r = 0.0;
	}
	if (cutGreen) {
		col.g = 0.0;
	}
	if (cutBlue) {
		col.b = 0.0;
	}
	col.bgr = col.rgb;
	return col;
}

technique RGB {
	pass PassRGB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS_RGB();
	}
}

technique RBG {
	pass PassRBG {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS_RBG();
	}
}

technique GBR {
	pass PassGBR {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS_GBR();
	}
}

technique GRB {
	pass PassGRB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS_GRB();
	}
}

technique BRG {
	pass PassBRG {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS_BRG();
	}
}

technique BGR {
	pass PassBGR {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS_BGR();
	}
}
