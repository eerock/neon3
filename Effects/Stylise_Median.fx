// Stylize - Median

float screenW : SCREENW;
float screenH : SCREENH;
float vBrightness : VAR1;
float level : VAR2;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float3 rgb2lum = float3(0.30, 0.59, 0.11);
	float2 s1 = { 0.0, 0.0078125 };
	float2 s2 = { 0.0078125, 0.0 };
	float2 tc = In.Tex0;
	float3 col0 = tex2D(sampler0, tc);
	float3 col1 = tex2D(sampler0, tc + s1);
	float3 col2 = tex2D(sampler0, tc + s2);
	float3 col3 = tex2D(sampler0, tc - s1);
	float3 col4 = tex2D(sampler0, tc - s2);
	float b0 = dot(col0.xyz, rgb2lum);
	float b1 = dot(col1.xyz, rgb2lum);
	float b2 = dot(col2.xyz, rgb2lum);
	float b3 = dot(col3.xyz, rgb2lum);
	float b4 = dot(col4.xyz, rgb2lum);
	float flag0 = step(b0,b1) + step(b0, b2) + step(b0, b3) + step(b0, b4);
	float flag1 = step(b1,b0) + step(b1, b2) + step(b1, b3) + step(b1, b4);
	float flag2 = step(b2,b0) + step(b2, b1) + step(b2, b3) + step(b2, b4);
	float flag3 = step(b3,b0) + step(b3, b1) + step(b3, b2) + step(b3, b4);
	float4 col = tex2D(sampler0, In.Tex0);
	col.xyz = vBrightness * ((flag0 == 2.0) ? col0 : ((flag1 == 2.0) ? col1 : ((flag2 == 2.0) ? col2 : ((flag3 == 2.0) ? col3 : col4))));

	if (!processAlpha) {
		col.a = 1.0;
	}
	return col;
}

technique Median {
	pass PassMedian {
		vertexshader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}
