// Mipmapper
// This is unused, todo: try it out.

float vRotate : VAR1;
float vZoomWidth : VAR2;
float vZoom : VAR3;
float linkScale : VAR4;
float quality : VAR5;
float mixTaps : VAR6;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = BORDER;
	AddressV = BORDER;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 coords = In.Tex0;
	float4 sum = tex2Dlod(sampler0, float4(coords, 6.0, 6.0));
	return sum;
}

technique RotateZoomBlur {
	pass PassRotateZoomBlur {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
