// Distort - Zygo

float V_Time : TIME;
float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;
float V_Var4 : VAR4;
float4 color;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
};

float zygzyg(float i, float val) {
	float beat = V_Time * (V_Var2 * 10.0);
	float n = i * beat * 1.3212120;
	return sin((n + 515.51515 + beat * 0.5963217123) * val) * sin((n + 142.151212 + beat * 0.051561558) * val);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = V_Var1 * 10.0;
	float beat = V_Time * (V_Var2 * 10.0);
	float speed = V_Var3;
	float size = V_Var4;
	float2 zyg;
	float b = beat * (speed + 0.01) * 2.0;
	float cc = (5.51515 + b * 0.05963217123);
	zyg.x = 0.5 + cos(cc) * sin((1.151212 + b * 0.051561558));
	zyg.y = 0.5 + sin(cc) * sin((2.151212 + b * 0.0514115120));
	float r = dot(In.Tex0, zyg) * 30.0 * size;
	float l = (sin(r) * 0.5 + 0.5) * 0.1 * power;
	float2 p = In.Tex0 * (1.0 - l) + zyg * l;
	p.x = (p.x + 100.0) % 1.0;
	p.y = (p.y + 100.0) % 1.0;
	return tex2D(sampler0, p);
}

technique Zygo {
	pass PassZygo {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
