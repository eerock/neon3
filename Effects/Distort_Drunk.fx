// Distort - Double Vision

float Timer : TIME;
float drunkness : VAR1;
float screenW : SCREENW;
float screenH : SCREENH;
float resolution = 128.0;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float d = drunkness / resolution;
	float4 col = tex2D(sampler0, In.Tex0);
	col += tex2D(sampler0, In.Tex0 + float2(d * 8.0, 0.0)) * 0.8;
	col += tex2D(sampler0, In.Tex0 + float2(d * 16.0, 0.0)) * 0.6;
	col += tex2D(sampler0, In.Tex0 + float2(d * 24.0, 0.0)) * 0.4;
	col += tex2D(sampler0, In.Tex0 + float2(d * 32.0, 0.0)) * 0.2;
	col += tex2D(sampler0, In.Tex0 + float2(d * 64.0, 0.0)) * 0.2;
	col += tex2D(sampler0, In.Tex0 - float2(d * 8.0, 0.0)) * 0.8;
	col += tex2D(sampler0, In.Tex0 - float2(d * 16.0, 0.0)) * 0.6;
	col += tex2D(sampler0, In.Tex0 - float2(d * 24.0, 0.0)) * 0.4;
	col += tex2D(sampler0, In.Tex0 - float2(d * 32.0, 0.0)) * 0.2;
	col += tex2D(sampler0, In.Tex0 - float2(d * 64.0, 0.0)) * 0.2;
	col /= 5.0;
	return col;
}

technique DoubleVision {
	pass PassDoubleVision {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
