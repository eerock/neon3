// Stylize - Muffy

float fA : VAR1;
float fB : VAR2;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float A = 400.0 * fA;
	float B = 100.0 * fB;
	float4 col = tex2D(sampler0, In.Tex0 + ((sin(In.Tex0.y * A) / B) * cos(In.Tex0.x)) + (sin(In.Tex0.x * A) / B)); 
	return col;
}

technique Muffy {
	pass PassMuffy {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
