// Distort - Rotate Zoom

float4 BorderCol : COLOR = 0.0;
float Rotate : VAR1;
float vScale : VAR2;
float OffsetX : VAR3;
float OffsetY : VAR4;
bool Aspect : VAR5;
bool Filter : VAR6;
float screenW : SCREENW;
float screenH : SCREENH;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 r2d(float2 x, float a) {
	a *= acos(-1.0) * 2.0;
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

// default vs
void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 R = { screenW, screenH };
	//float Rotate = 2.0 * vRotate - 1.0;
	float2 Scale = (vScale, vScale);
	float2 asp = lerp(1.0, R.x / R, Aspect);
	float2 vp = In.Tex0 * R - 0.25;
	float2 dx = r2d((In.Tex0 - 0.5 - float2(OffsetX, OffsetY)) / asp, Rotate) / Scale * asp + 0.5;
	float4 c = tex2D(sampler0, dx);
	return c;
}

technique Border {
	pass PassBorder {
		AddressU[0] = BORDER;
		AddressV[0] = BORDER;
		AddressU[1] = BORDER;
		AddressV[1] = BORDER;
		BorderColor[0] = BorderCol;
		BorderColor[1] = BorderCol;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Clamp {
	pass PassClamp {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		AddressU[1] = CLAMP;
		AddressV[1] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Wrap {
	pass PassWrap {
		AddressU[0] = WRAP;
		AddressV[0] = WRAP;
		AddressU[1] = WRAP;
		AddressV[1] = WRAP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Mirror {
	pass PassMirror {
		AddressU[0] = MIRROR;
		AddressV[0] = MIRROR;
		AddressU[1] = MIRROR;
		AddressV[1] = MIRROR;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
