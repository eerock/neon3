// Distort - Circular

float V_Time : TIME;
float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = V_Var1;
	float beat = V_Time * (V_Var2 * 10.0);
	float speed = V_Var3;
	float2 zyg, p;
	float b = beat * speed;
	zyg.x = cos(In.Tex0.x * 0.0145111 + b) + 0.5;
	zyg.y = sin(In.Tex0.y * 0.01216584 + b) + 0.5;
	zyg -= In.Tex0;
	float d = zyg.x * zyg.x + zyg.y * zyg.y;
	p.x = cos(d) * power;
	p.y = sin(d) * power;
	p += In.Tex0;
	p.x = (p.x + 10.0) % 1.0;
	p.y = (p.y + 10.0) % 1.0;
	return tex2D(sampler0, p);
}

technique Circular {
	pass PassCircular {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
