// Stylize - Color Edge

float vPower : VAR1;
float vSize : VAR2;
float vLuminosity : VAR3;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = ANISOTROPIC;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = vPower;
	float size = vSize;
	float luminosity = vLuminosity;
	const int NUM = 9;
	const float threshold = 0.05;
	const float2 c[NUM] = {
		float2(-0.0078125, 0.0078125),
		float2(0.0, 0.0078125),
		float2(0.0078125, 0.0078125),
		float2(-0.0078125, 0.0),
		float2(0.0, 0.0),
		float2(0.0078125, 0.007),
		float2(-0.0078125, -0.0078125),
		float2(0.0, -0.0078125),
		float2(0.0078125, -0.0078125)
	};
	float3 col[NUM];
	int i;
	for (i = 0; i < NUM; i++) {
		col[i] = tex2D(sampler0, In.Tex0 + c[i] * size);
	}
	float3 rgb2lum = float3(0.30, 0.59, 0.11);
	float lum[NUM];
	for (i = 0; i < NUM; i++) {
		lum[i] = dot(col[i].rgb, rgb2lum);
	}
	float x = lum[2] + lum[8] + 2.0 * lum[5] - lum[0] - 2.0 * lum[3] - lum[6];
	float y = lum[6] + 2.0 * lum[7] + lum[8] - lum[0] - 2.0 * lum[1] - lum[2];
	float edge = (x * x + y * y < threshold) ? 1.0 : (1.0 - power);
	float4 ctex;
	ctex.rgb = luminosity * 2.0 * col[5].xyz * edge.xxx;
	float4 tAlpha = tex2D(sampler0, In.Tex0);
	ctex.a = tAlpha.a;
	if (!processAlpha) {
		ctex.a = 1.0;
	}
	return ctex;
}

technique ColorEdge {
	pass PassColorEdge {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
