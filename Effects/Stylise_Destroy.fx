// Stylize - Destroy

float V_Var1 : VAR1;
float4 color;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = ANISOTROPIC;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = V_Var1 * 10.0;
	float2 p;
	float4 col = tex2D(sampler0, In.Tex0);
	p.x = col.x;
	p.y = col.y;
	float4 oot = (tex2D(sampler0, p) * power) + (col * (1.0 - power)) * color;
	return oot;
}

technique Destroy {
	pass PassDestroy {
		AddressU[0] = WRAP;
		AddressV[0] = WRAP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
