// Source - Gradients

float4 c1 = { 1.0, 1.0, 1.0, 1.0 };
float4 c2 = { 0.0, 0.0, 0.0, 1.0 };
float Lamn : VAR1;
float Lpow : VAR2;
float Ramn : VAR3;
float Rpow : VAR4;
float R2amn : VAR5;
float R2pow : VAR6;
float Pamn : VAR7;
float Ppow : VAR8;
float Multiplier : VAR9;
float Phase : VAR10;
float LPe : VAR11;
float PPe : VAR12;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

VS_OUTPUT VS(float4 Pos : POSITION, float2 Tex0 : TEXCOORD0) {
	VS_OUTPUT Out;
	Out.Pos = Pos;
	Out.Tex0 = Tex0;
	return Out;
}

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 PS(VS_OUTPUT In) : COLOR {
	// rubbish
	float4 col = 0.0;
	float2 p1 = 0.0;
	float2 p2 = 0.0;
	float2 p3 = { 0.0, 1.0 };
	float4 v = 0.0;
	float val, l, r, r2, p = 0.0;
	
	// xy
	p2.x = In.Tex0.x;
	p2.y = In.Tex0.y;
	
	v.xy = p1;
	v.zw = p2;
	v -= 0.5;
	l = p2.y;
	
	if (LPe > 0.5) {
		l = pow(l, Lpow);
	}
	
	r = distance(p1 + 0.5, p2);
	r2 = dot(v, v);
	p = 2.3873 * atan2(0.5 - p2.x, 0.5 - p2.y);
	if (PPe > 0.5) {
		p = pow(p, Ppow);
	}
	
	val = ((l * Lamn) + (pow(r, Rpow) * Ramn) + (pow(r2, R2pow) * R2amn) + (p * Pamn)) * Multiplier;
	val = frac(val + Phase);
	val *= 2.0;
	
	col= lerp(c1, c2, val);
	return col;
}

technique Gradients {
	pass PassGradients {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
