// Distort - Position Scale

float screenW : SCREENW;
float screenH : SCREENH;
float4 BorderCol : COLOR = { 0.5, 0.5, 0.1, 1.0 };
float V_Rand1 : RAND1;
float V_Rand2 : RAND2;
float V_Rand3 : RAND3;
float time : TIME;
float sTime : SINTIME;
float vX : VAR1;
float vY : VAR2;
float vSX : VAR3;
float vSY : VAR4;
float vSinXFader : VAR5;
float vSinYFader : VAR6;
float vTimeXFader : VAR7;
float vTimeYFader : VAR8;
float randomXIntensity : VAR9;
float randomYIntensity : VAR10;
float vTimeXYFader : VAR11;
float randomXYIntensity : VAR13;
float vSinXYFader : VAR12;

struct VS_OUTPUT {
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 R = { screenW, screenH };
	float tx = (2.0 * vX - 1.0);
	float ty = (2.0 * vY - 1.0);
	float2 newPos = float2(tx, ty);
	float P = 2.0;
	float fXTime = frac(time * vTimeXFader);
	float fYTime = frac(time * vTimeYFader);
	float fScaleTime = frac(time * vTimeXYFader);
	float randomScale = V_Rand1 * randomXYIntensity;
	float t = (2.0 * vSX - 1.0)*4.0;
	t += randomScale;
	t += fScaleTime;
	t += sTime * vSinXYFader;
	float2 uv = In.Tex0;
	uv += newPos;
	uv -= float2(0.5, 0.5);
	uv /= pow(P, t);
	uv += float2(0.5, 0.5);
	float randomX = V_Rand1 * randomXIntensity;
	float randomY = V_Rand2 * randomYIntensity;
	uv += float2(fXTime, fYTime);
	uv += float2(sTime * vSinXFader, sTime * vSinYFader);
	uv += float2(randomX, randomY);
	uv += float2(0.5, 0.5) / R;
	float4 output = tex2D(sampler0, uv);
	return output;
}

technique BorderPoint {
	pass PassBorderPoint {
		MipFilter[0] = POINT;
		MinFilter[0] = POINT;
		MagFilter[0] = POINT;
		AddressU[0] = BORDER;
		AddressV[0] = BORDER;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique BorderAnisotropic {
	pass PassBorderAnisotropic {
		MipFilter[0] = ANISOTROPIC;
		MinFilter[0] = ANISOTROPIC;
		MagFilter[0] = ANISOTROPIC;
		AddressU[0] = BORDER;
		AddressV[0] = BORDER;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique BorderLinear {
	pass PassBorderLinear {
		MipFilter[0] = LINEAR;
		MinFilter[0] = LINEAR;
		MagFilter[0] = LINEAR;
		AddressU[0] = BORDER;
		AddressV[0] = BORDER;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique BorderMirror {
	pass PassBorderMirror {
		MipFilter[0] = ANISOTROPIC;
		MinFilter[0] = ANISOTROPIC;
		MagFilter[0] = ANISOTROPIC;
		AddressU[0] = MIRROR;
		AddressV[0] = MIRROR;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique BorderClamp {
	pass PassBorderClamp {
		MipFilter[0] = LINEAR;
		MinFilter[0] = LINEAR;
		MagFilter[0] = LINEAR;
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique BorderWrap {
	pass PassBorderWrap {
		MipFilter[0] = ANISOTROPIC;
		MinFilter[0] = ANISOTROPIC;
		MagFilter[0] = ANISOTROPIC;
		AddressU[0] = WRAP;
		AddressV[0] = WRAP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
