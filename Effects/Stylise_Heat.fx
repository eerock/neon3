// Stylize - Heat

bool processAlpha : VAR16;
float vAlpha : VAR1;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float dist = 5.0;
	float4 color = tex2D(sampler0, In.Tex0);
	float alphaChannel = vAlpha;
	if (processAlpha) {
		alphaChannel = color.a;
	}
	float4 colorA;
	float4 colorB;
	if (color.r < 0.25) {
		dist = color.r / 0.25;
		colorA = float4(0.5, 0.5, 1.0, alphaChannel);
		colorB = float4(0.0, 0.0, 1.0, alphaChannel);
	} else if(color.r < 0.35) {
		dist = (color.r - 0.25) / 0.1;
		colorA = float4(0.0, 0.0, 1.0, alphaChannel);
		colorB = float4(1.0, 1.0, 0.0, alphaChannel);
	} else {
		dist = (color.r - 0.35) / 0.65;
		colorA = float4(1.0, 1.0, 0.0, alphaChannel);
		colorB = float4(1.0, 0.0, 0.0, alphaChannel);
	}
	return lerp(colorA, colorB, dist);
}

technique Heat {
	pass PassHeat {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
