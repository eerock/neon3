// Photo - Filmic Contrast

float vGain : VAR1;
static const float3 lumcoeff = float3(0.2125, 0.7154, 0.0721);

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

VS_OUTPUT VS(float4 Pos : POSITION, float2 Tex0 : TEXCOORD0) {
	VS_OUTPUT Out;
	Out.Pos = Pos;
	Out.Tex0 = Tex0;
	return Out;
};

float4 PS(VS_OUTPUT In) : COLOR {
	float gain = (vGain * 200.0) - 100.0;
	float4 color = tex2D(sampler0, In.Tex0);
	float4 outColor = color;
	float newGain = (gain + 100.0) * 0.005;
	color.rgb = clamp(color.rgb, float3(0.0, 0.0, 0.0), float3(1.0, 1.0, 1.0));
	float luma = (color.r * lumcoeff.r) + (color.g * lumcoeff.g) + (color.b * lumcoeff.b);
	float val;
	if (luma < 0.5) {
		if (1.0 - newGain > 0.0) {
			val = pow(2.0 * luma, log(1.0 - newGain) / log(0.5));
		} else {
			val = 0.0;
		}
	} else {
		if (1.0 - newGain > 0.0) {
			val = 2.0 - pow(2.0 - (2.0 * luma), log(1.0 - newGain) / log(0.5));
		} else {
			val = 2.0;
		}
	}
	val *= 0.5;
	val = val / (luma + 0.00001);
	if (color.r > 1.0) {
		outColor.r = color.r;
	} else {
		outColor.r = color.r * val;
	}
	if (color.g > 1.0) {
		outColor.g = color.g;
	} else {
		outColor.g = color.g * val;
	}
	if (color.b > 1.0) {
		outColor.b = color.b;
	} else {
		outColor.b = color.b * val;
	}
	outColor.a = color.a;
	return outColor;
}

technique FilmicContrast {
	pass PassFilmicContrast {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
