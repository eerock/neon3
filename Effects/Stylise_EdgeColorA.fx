// Stylize - Edge

float screenW : SCREENW;
float screenH : SCREENH;
float Radius : VAR1;		// ebp-> radius goes to zero it snaps off, looks bad.
float Bright : VAR2;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 q(float2 x, float2 off, float v) {
	float2 R = float2(screenH, screenW);	// is this on purpose?? (H, W) and not (W, H)
	// maybe...
	//return float4(tex2D(sampler0, x + off / R), 0.0, 1.0 + v);	// do this??
	return tex2D(sampler0, float4(x + off / R, 0.0, 1.0 + v));
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 R = float2(screenH, screenW);	// on purpose?
	float2 x = In.Tex0;
	float2 vp = x * R;
	float rad = max(Radius, 0.0);
	float3 e = float3(1.0, -1.0, 0.0) * rad;
	float v = log2(rad);
	float4 cx = q(x, e.xy, v) + q(x, e.xz, v) + q(x, e.xx, v) - q(x, e.yy, v) - q(x, e.yz, v) - q(x, e.yx, v);
	float4 cy = q(x, e.yy, v) + q(x, e.zy, v) + q(x, e.xy, v) - q(x, e.yx, v) - q(x, e.zx, v) - q(x, e.xx, v);
	float4 c = sqrt(cx * cx + cy * cy) * Bright * pow(2.0, rad / max(R.x, R.y)) / sqrt(saturate(rad) + 0.001);
	c.a = tex2D(sampler0, x).a;
	return c;
}

technique Edge {
	pass PassEdge {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		AddressU[1] = CLAMP;
		AddressV[1] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
