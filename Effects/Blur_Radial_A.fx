// Blur - Radial A

float screenW : SCREENW;
float screenH : SCREENH;
float BlurX : VAR1;
float BlurY : VAR2;
float BlurZ : VAR3;
float BlurR : VAR4;
float Width : VAR5;
bool Aspect : VAR6;
float4 BorderCol : COLOR = { 0.0, 0.0, 0.0, 0.0 };

bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos0 : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 r2d(float2 x, float a) {
	a *= acos(-1.0) * 2.0;
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In, float2 vp : vpos) : COLOR {
	float2 R = float2(screenW, screenH);
	float2 x = (vp + 0.5) / R;
	float2 asp = lerp(1.0, R.x / R, Aspect);
	float4 c = 0.0;
	float4 myAlpha = 0.0;
	float kk = 0.0;
	float2 piv = 0.5 + float2(BlurX, BlurY);
	float wd = Width * tex2D(sampler0, x).x;
	for (float i = 0; i < 1; i += 1.0 / 16.0) {
		c += tex2D(sampler0, r2d((x - piv) / asp, i * BlurR * wd) / pow(2, (BlurZ * wd) * 6 * i) * asp + piv);
		//myAlpha = c;
		kk++;
	}
	c /= kk;
	return c;
}

technique Clamp {
	pass PassClamp {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Wrap {
	pass PassWrap {
		AddressU[0] = WRAP;
		AddressV[0] = WRAP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Mirror {
	pass PassMirror {
		AddressU[0] = MIRROR;
		AddressV[0] = MIRROR;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Border {
	pass PassBorder {
		AddressU[0] = BORDER;
		AddressV[0] = BORDER;
		BorderColor[0] = BorderCol;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
