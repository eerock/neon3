// Photo - Technicolor 2

float V_Var1 : VAR1;
float4 redfilter : COLOR = { 1.0, 0.0, 0.0, 0.0 };
float4 bluegreenfilter : COLOR = { 0.0, 1.0, 1.0, 0.0 };
float4 cyanfilter : COLOR = { 0.0, 1.0, 0.5, 0.0 };
float4 magentafilter : COLOR = { 1.0, 0.0, 0.25, 0.0 };
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float4 filtred = col * redfilter;
	float4 filtbluegreen = col * bluegreenfilter;
	float4 rednegative = float(filtred.r);
	float4 bluegreennegative = float((filtbluegreen.g + filtbluegreen.b) / 2.0);
	float4 redoutput = rednegative + cyanfilter;
	float4 bluegreenoutput = bluegreennegative + magentafilter;
	float4 result = redoutput * bluegreenoutput;
	result.a = col.a;
	float4 finalmix = lerp(col, result, V_Var1);
	if (!processAlpha) {
		finalmix.a = 1.0;
	}
	return finalmix;
}

technique Technicolor2 {
	pass PassTechnicolor2 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
