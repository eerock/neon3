// Distort - Star

float timer : TIME;
float alpha = 1.0;
float power : VAR1;
float a1 : VAR2;
float a2 : VAR3;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float normalisedPower = power * 10.0;
	float2 uv;
	float2 p = -1.0 + 2.0 * In.Tex0;
	float a = atan2(p.y, p.x);
	float r = sqrt(dot(p, p));
	float s = r * (1.0 + 0.8 * cos(timer * 1.0));
	uv.x = a1 * p.y + .03 * cos(-timer + a * 3.0) / s;
	uv.y = 0.1 * timer + a2 * p.x + 0.03 * sin(-timer + a * 3.0) / s;
	float w = 0.9 + pow(max(1.5 - r, 0.0), normalisedPower);
	w *= 0.6 + 0.4 * cos(timer + 3.0 * a);
	float4 col = tex2D(sampler0, uv).xyzw;
	col = col * w;
	if (!processAlpha) {
		col.a = 1.0;
	}
	return col;
}

technique Star {
	pass PassStar {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
