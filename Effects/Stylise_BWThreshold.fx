// Stylize = BW Threshold

float brightness : VAR1;
float3 Luminance = { 0.2125, 0.7154, 0.0721 };

struct VS_OUTPUT {
	float4 Color : COLOR;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 TexelKernel[4] = {
	{ 0.0, 1.0 },
	{ 1.0, 0.0 },
	{ 0.0, -1.0 },
	{ -1.0, 0.0 }
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float nb = brightness * 10.0;
	float4 Total = 0;
	for (int i = 0; i < 4; i++) {
		Total += (abs(col - tex2D(sampler0, In.Color + TexelKernel[i])) - 0.5) * 1.2 + 0.5;	// this is a little odd.
	}
	col.r = nb * saturate(dot(Luminance, Total));
	col.g = nb * saturate(dot(Luminance, Total));
	col.b = nb * saturate(dot(Luminance, Total));
	return col;
}

technique BWThreshold {
	pass PassBWThreshold {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
