// Color - Invert RGB

float v_red : VAR1;
float v_green : VAR2;
float v_blue : VAR3;
float v_alpha : VAR4;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col_a = tex2D(sampler0, In.Tex0);
	float4 col_b = 0.0;
	col_b.r = v_red * (1.0 - col_a.r) + (1.0 - v_red) * col_a.r;
	col_b.g = v_green * (1.0 - col_a.g) + (1.0 - v_green) * col_a.g;
	col_b.b = v_blue * (1.0 - col_a.b) + (1.0 - v_blue) * col_a.b;

	if (processAlpha) {
		col_b.a = col_a.a;
	} else {
		col_b.a = 1.0;
	}

	return lerp(col_a, col_b, v_alpha);
}

technique InvertRGB {
	pass PassInvertRGB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
