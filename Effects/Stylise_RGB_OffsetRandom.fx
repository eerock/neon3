// Stylize - RGB Offset Random

float V_Alpha : ALPHA;
float V_InvScreenW : INVSCREENW;
float V_InvScreenH : INVSCREENH;
float V_Rand1 : RAND1;
float V_Rand2 : RAND2;
float V_Rand3 : RAND3;
float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 DispRGB(float2 xy, float Disp, float Rand) {
	xy.x = xy.x + (Disp * V_InvScreenW * V_Var1) + (15.0 * V_InvScreenW * V_Var3 * Rand);
	xy.y = xy.y + (Disp * V_InvScreenH * V_Var2) + (15.0 * V_InvScreenH * V_Var3 * Rand);
	return tex2D(sampler0, xy);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 r = DispRGB(In.Tex0, 13.0, V_Rand1 - 0.5);
	float4 g = DispRGB(In.Tex0, 7.0, V_Rand2 - 0.5);
	float4 b = DispRGB(In.Tex0, -10.0, V_Rand3 - 0.5);
	float a = V_Alpha * (r.a + g.a + b.a) / 3.0;
	return float4(r.r, g.g, b.b, a);
}

technique RGBOffsetRandom {
	pass PassRGBOffsetRandom {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
