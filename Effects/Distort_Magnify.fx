// Distort - Magnify

float screenW : SCREENW;
float screenH : SCREENH;
float inner_radius : VAR1;
float outer_radius : VAR2;
float magnification : VAR3;
float centerX : VAR4;
float centerY : VAR5;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

// zoom vs - i guess it's appropriate here.
void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {
	float2 R = { screenW, screenH };
	vp.xy *= 2.0;
	uv += 0.5 / R;
}

float4 PS(float2 vp : vpos) : COLOR {
	float2 center = { centerX, centerY };
	float2 R = { screenW, screenH };
	float2 x = (vp + 0.5) / R;
	float2 center_to_pixel = x - center;
	float distance = length(center_to_pixel);
	float4 color;
	float2 sample_point;

	if (distance < outer_radius) {
		if (distance < inner_radius) {
			sample_point = center + (center_to_pixel / magnification);
		} else {
			float radius_diff = outer_radius - inner_radius;
			float ratio = (distance - inner_radius) / radius_diff;
			ratio = ratio * 3.14159;
			float adjusted_ratio = cos(ratio);
			adjusted_ratio = adjusted_ratio + 1;
			adjusted_ratio = adjusted_ratio / 2;
			sample_point = ((center + (center_to_pixel / magnification)) * (adjusted_ratio)) + (x * (1 - adjusted_ratio));
		}
	} else {
		sample_point = x;
	}
	return tex2D(sampler0, sample_point);
}

technique Magnify {
	pass PassMagnify {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
