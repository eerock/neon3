// Color - Black & White B

float Mix : VAR1;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

static const float3 LuminanceConv = { 0.2125, 0.7154, 0.0721 };

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 output = dot((float3)tex2D(sampler0, In.Tex0), LuminanceConv);
	float4 col = tex2D(sampler0, In.Tex0);
	output.a = col.a;
	return lerp(col, output, Mix);
}

technique BlackAndWhiteB {
	pass PassBlackAndWhiteB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
