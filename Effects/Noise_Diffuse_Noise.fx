// Noise - Diffuse Noise

float Timer : TIME;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	//float4 Color: COLOR0;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 t = In.Tex0;
	float x = t.x * t.y * 123456.0 * Timer;
	x = fmod(x, 13.0) * fmod(x, 123.0);
	float dx = fmod(x, 0.01);
	float dy = fmod(x, 0.012);
	float4 c = tex2D(sampler0, t + float2(dx, dy));
	return c;
}

technique DiffuseNoise {
	pass PassDiffuseNoise {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
