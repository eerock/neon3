// Color - Balance

float red : VAR1;
float green : VAR2;
float blue : VAR3;
float ared : VAR4;
float agreen : VAR5;
float ablue : VAR6;
float power : VAR7;
float lum : VAR8;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float4 Col0: COLOR0;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float4 c = col;
	float2 p;
	float4 cx;
	float i;
	p.y = 0.0;
	p.x = col.r;
	cx = tex2D(sampler0, p);
	i = (cx.x + cx.y);
	c.r = c.r * lum + (c.r * i * red * 2.0) + ared - 0.5;
	p.y = 0.0;
	p.x = col.g * 0.5;
	cx = tex2D(sampler0, p);
	i = (cx.x + cx.y);
	c.g = c.g * lum + (c.g * i * green * 2.0) + agreen - 0.5;
	p.y = 1.0;
	p.x = col.b * 0.1;
	cx = tex2D(sampler0, p);
	i = (cx.x + cx.y);
	c.b = c.b * lum + (c.b * i * blue * 2.0) + ablue - 0.5;
	col = ((c * power) + col * (1.0 - power));
	return col;
}

technique ColorBalance {
	pass PassColorBalance {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
