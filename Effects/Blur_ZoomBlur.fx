// Blur - Zoom Blur
//

float vRotate : VAR1;
float vZoomWidth : VAR2;
float vZoom : VAR3;
bool linkScale : VAR4;
float quality : VAR5;
float centerX : VAR6;
float centerY : VAR7;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = BORDER;
	AddressV = BORDER;
};

void VS(inout float4 vp: POSITION0, inout float2 uv: TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR0 {
	float zoom;
	float blurWidth = 2.0 * vZoomWidth - 1.0;

	// 0.0, 0.0 is the center of the screen
	// so substracting uv from it will result in
	// a vector pointing to the middle of the screen
	//vec2 dir = xy - v_texCoord;

	float2 direction = float2(centerX, centerY);

	if (linkScale && (vZoomWidth > 0.5)) {
		zoom = (2.0 * vZoomWidth - 1.0) * -1.0;
	} else {
		zoom = (2.0 * vZoom - 1.0) * -1.0;
	}

	float2 uv = In.Tex0;	//-direction;
	float4 sum = (float4)0;
	int SAMPLES = floor(quality);

	for (float i = 0; i < SAMPLES; i++) {
		float scale = (blurWidth * (i / (SAMPLES - 1)));
		scale += zoom;
		float2 tapCoordsZoom = ((In.Tex0 - direction) * scale) + uv;
		sum += tex2Dlod(sampler0, float4(tapCoordsZoom, 0, 0));
	}
	sum /= SAMPLES;

	if (!processAlpha) {
		sum.a = 1.0;
	}
	return sum;
}

technique ZoomBlur {
	pass PassZoomBlur {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
