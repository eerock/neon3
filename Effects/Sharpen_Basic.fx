// Sharpen - Basic
// TouchViZ Sharpen

float sampleStep : VAR1;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

VS_OUTPUT VS(float4 Pos : POSITION, float2 Tex0 : TEXCOORD0) {
	VS_OUTPUT Out;
	Out.Pos = Pos;
	Out.Tex0 = Tex0;
	return Out;
}

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 PS(VS_OUTPUT In) : COLOR {
	float2 offset[9];
	float4 samples[9];
	//float tx = floor(pow(screenW, Horizontal));
	//float ty = floor(pow(screenH, Vertical));

	offset[0] = float2(sampleStep * -0.001, sampleStep * 0.001);
	offset[1] = float2(0.00, sampleStep * 0.001);
	offset[2] = float2(sampleStep * 0.001, sampleStep * 0.001);
	offset[3] = float2(sampleStep * -0.001, 0.0);
	offset[4] = float2(0.0, 0.0);
	offset[5] = float2(sampleStep * 0.001, sampleStep * 0.001);
	offset[6] = float2(sampleStep * -0.001, sampleStep * -0.001);
	offset[7] = float2(0.0, sampleStep * -0.001);
	offset[8] = float2(sampleStep * 0.001, sampleStep * -0.001);
	samples[0] = tex2D(sampler0, In.Tex0 + offset[0]);
	samples[1] = tex2D(sampler0, In.Tex0 + offset[1]);
	samples[2] = tex2D(sampler0, In.Tex0 + offset[2]);
	samples[3] = tex2D(sampler0, In.Tex0 + offset[3]);
	samples[4] = tex2D(sampler0, In.Tex0 + offset[4]);
	samples[5] = tex2D(sampler0, In.Tex0 + offset[5]);
	samples[6] = tex2D(sampler0, In.Tex0 + offset[6]);
	samples[7] = tex2D(sampler0, In.Tex0 + offset[7]);
	samples[8] = tex2D(sampler0, In.Tex0 + offset[8]);
	
	float4 col_b = (samples[4] * 9.0) - (samples[0] + samples[1] + samples[2] + samples[3] + samples[5] + samples[6] +  samples[7] + samples[8]);
	return col_b;
}

technique SharpenBasic {
	pass PassSharpenBasic {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
