// Stylize - Emboss Color

float V_InvScreenW : INVSCREENW;
float V_InvScreenH : INVSCREENH;
float level : VAR1;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 color;
	float4 base;
	float2 xya;
	float2 xyb;
	xya.x = In.Tex0.x - V_InvScreenW;
	xya.y = In.Tex0.y - V_InvScreenH;
	xyb.x = In.Tex0.x + V_InvScreenW;
	xyb.y = In.Tex0.y + V_InvScreenH;
	base = tex2D(sampler0, In.Tex0);
	color.rgba = 0.3;
	color -= tex2D(sampler0, xya) * 2.0;
	color += tex2D(sampler0, xyb) * 2.0;
	color = (color.r + color.g + color.b) / 4.0 + base;
	color = base + (color - base) * level;
	if (!processAlpha) {
		color.a = 1.0;
	}
	return color;
}

technique EmbossColor {
	pass PassEmbossColor {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
