// Distort - Caustic

float speed : VAR1;
float scaleX : VAR2;
float scaleY : VAR3;
float time : TIME;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Mirror;
	AddressV = Mirror;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 wave;
	float newSpeed = (speed * 10.0) * time;
	float newScaleX = scaleX * 300.0;
	float newScaleY = scaleY * 100.0;
	wave.y = In.Tex0.y + (sin((In.Tex0.x * newScaleX) + newSpeed) * 0.01);
	wave.x = In.Tex0.x + (sin((In.Tex0.y * newScaleY) + newSpeed) * 0.01);
	float4 c = tex2D(sampler0, wave);
	if (!processAlpha) {
		c.a = 1.0;
	}
	return c;
}

technique Caustic {
	pass PassCaustic {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
