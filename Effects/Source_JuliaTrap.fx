// Source - Julia Trap Circle

float time : TIME;
float trapx : VAR1;
float trapy : VAR2;
float v_joffsetx : VAR3;
float v_joffsety : VAR4;
float v_ccx : VAR5;
float v_ccy : VAR6;
float v_jscalex : VAR7;
float v_jscaley : VAR8;
float v_radius : VAR9;
float v_power : VAR10;
float v_scale : VAR11;
int v_iter : VAR12;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

VS_OUTPUT VS(float4 Pos : POSITION, float2 Tex0 : TEXCOORD0) {
	VS_OUTPUT Out;
	Out.Pos = Pos;
	Out.Tex0 = Tex0;
	return Out;
}

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 PS(VS_OUTPUT In) : COLOR {
	float2 trap = { 2.0 * trapx - 1.0, 2.0 * trapy - 1.0 };
	float2 jscale = { 2.0 * v_jscalex - 1.0, 2.0 * v_jscaley - 1.0};
	float2 joffset  = { 2.0 * v_joffsetx - 1.0, 2.0 * v_joffsety - 1.0 };	
	float2 cc = { 2.0 * v_ccx - 1.0, 2.0 * v_ccy - 1.0};
	float Scale = v_scale * 5.0;	
	float Power = v_power * 5.0;
	float radius = v_radius * 5.0;
	int iter = 64;
	//int iter = floor(128 * v_iter);
	float2 p = -1.0 + 2.0 * In.Tex0;
	p = p * jscale + joffset;
	float dmin = 1000000.0;
	float2 z  = p;
	bool done = false;
	float sum = 0.0;
	for (int i = 0; i < iter && done == false; i++) {
		z = cc + float2(z.x * z.x - z.y * z.y, 2.0 * z.x * z.y);
		float m2 = dot(z, z);
		if (m2 > 100.0) {
			done = true;
		} else {
			float dist = distance(z, trap);
			dist -= radius;
			dmin = min(dmin, dist);
		}
	}
	float4 col;
	col = tex1D(sampler0, pow(abs(dmin), Power) * Scale);
	col.a = 1.0;
	return col;
}

technique JuliaSingle {
	pass PassJuliaSingle {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

