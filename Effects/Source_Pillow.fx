// Source - Pillow

float V_Var1 : VAR1;
float V_Var2 : VAR2;
float4 ColorA : COLOR = { 0.0, 0.0, 0.0, 1.0 };
float4 ColorB : COLOR = { 1.0, 1.0, 1.0, 1.0 };
float2 XY = { 0.0, 0.0 };
float2 Scale = { 1.0, 1.0 };
float Rotate = 0.0;
bool cutout : VAR16;
bool composite : VAR15;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 r2d(float2 x, float a) {
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float2 Gamma = { (V_Var1 * 10.0) - 5, (V_Var1 * 10.0) - 5.0 };
	float2 ClampBody = { V_Var2, V_Var2 };
	float2 x = r2d((In.Tex0 - 0.5 - XY) / Scale, Rotate * acos(-1.0) * 2.0) + 0.5;
	float4 c = pow(smoothstep(0.5, 0.4999 * saturate(ClampBody.x), abs(x.x - 0.5)), pow(2.0, Gamma.x)) * pow(smoothstep(0.5, 0.4999 * saturate(ClampBody.y), abs(x.y - 0.5)), pow(2.0, Gamma.y));
	c = lerp(ColorA, ColorB, c);
	if (composite) {
		if (!cutout) {
			c = lerp(ColorA, col, c);
		}
	}
	if (cutout) {
		c.a = 1.0 - (c.r + c.g + c.b ) / 3.0;
	}
	return c;
}

technique Pillow {
	pass PassPillow {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
