// Stylize - Cartoon

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 ColorBlend = float4(4.0, 4.0, 4.0, 0.11);
	half4 color = tex2D(sampler0, In.Tex0);
	color.r = round(color.r * ColorBlend.r) / ColorBlend.r;
	color.g = round(color.g * ColorBlend.g) / ColorBlend.g;
	color.b = round(color.b * ColorBlend.b) / ColorBlend.b;
	const float threshold = 0.11;
	const int NUM = 9;
	const float2 c[NUM] = {
		float2(-0.0078125, 0.0078125),
		float2(0.0, 0.0078125),
		float2(0.0078125, 0.0078125),
		float2(-0.0078125, 0.0),
		float2(0.0, 0.0),
		float2(0.0078125, 0.007),
		float2(-0.0078125, -0.0078125),
		float2(0.0, -0.0078125),
		float2(0.0078125, -0.0078125),
	};
	int i;
	float3 col[NUM];
	for(i = 0; i < NUM; i++) {
		col[i] = tex2D(sampler0, In.Tex0 + 0.2 * c[i]);
	}
	float3 rgb2lum = float3(0.30, 0.59, 0.11);
	float lum[NUM];
	for(i = 0; i < NUM; i++) {
		lum[i] = dot(col[i].xyz, rgb2lum);
	}
	float x = lum[2] + lum[8] + 2.0 * lum[5] - lum[0] - 2.0 * lum[3] - lum[6];
	float y = lum[6] + 2.0 * lum[7] + lum[8] - lum[0] - 2.0 * lum[1] - lum[2];
	float edge = (x * x + y * y < threshold) ? 1.0 : 0.0;
	color.rgb *= edge;
	return color;
}

technique Cartoon {
	pass PassCartoon {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
