// Alpha - Luma Key

float screenW : SCREENW;
float screenH : SCREENH;
float invert : VAR2;
float luma : VAR1;

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp: POSITION0, inout float2 uv: TEXCOORD0) {}

float4 PS(float2 uv: TEXCOORD0): COLOR {
	float4 col = tex2D(sampler0, uv);
	col.a = 1.0;
	float temp = (col.r * 0.33) + (col.g * 0.59) + (col.b * 0.11);
	if (temp < luma) {
		col.a = invert;
	} else {
		col.a = 1.0 - invert;
	}
	
	return col;
}

technique LumaKey {
	pass PassLumaKey {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
