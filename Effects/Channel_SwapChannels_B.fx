// Channel - Swap Channels B

bool rg : VAR1;		// red-green
bool rb : VAR2;		// red-blue
//bool ra : VAR3;	// red-alpha
bool rx : VAR4;		// cut-red

bool gr : VAR5;		// green-red
bool gb : VAR6;		// green-blue
//bool ga : VAR7;	// green-alpha
bool gx : VAR8;		// cut-green

bool br : VAR9;		// blue-red
bool bg : VAR10;	// blue-green
//bool ba : VAR11;	// blue-alpha
bool bx : VAR12;	// cut-blue

//bool ar : VAR13;	// alpha-red
//bool ag : VAR14;	// alpha-green
//bool ab : VAR15;	// alpha-blue
//bool ax : VAR16;	// cut-alpha

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout VS_OUTPUT vs_inout) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	// red channel swaps
	if (rg) {
		col.rg = col.gr;
	}
	if (rb) {
		col.rb = col.br;
	}
	if (rx) {
		col.r = 0;
	}
	// green channel swaps
	if (gr) {
		col.gr = col.rg;
	}
	if (gb) {
		col.gb = col.bg;
	}
	if (gx) {
		col.g = 0;
	}
	// blue channel swaps
	if (br) {
		col.br = col.rb;
	}
	if (bg) {
		col.bg = col.gb;
	}
	if (bx) {
		col.b = 0;
	}
	// alpha channel swaps
	//if (ar) {
	//	col.ar = col.ra;
	//}
	//if (ab) {
	//	col.ab = col.ba;
	//}
	//if (ag) {
	//	col.ag = col.ga;
	//}
	//if (ax) {
	//	col.a = 0;
	//}
	return col;
}

/*	// old version
float4 PS(VS_OUTPUT In): COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	// Red Channel
	if (rg) {
		col.r = col.g;
	}
	if (rb) {
		col.r = col.b;
	}
	//if (ra) {
	//	col.r = col.a;
	//}
	if (rx) {
		col.r = 0.0;
	}
	// Green Channel
	if (gr) {
		col.g = col.r;
	}
	if (gb) {
		col.g = col.b;
	}
	//if (ga) {
	//	col.g = col.a;
	//}
	if (gx) {
		col.g = 0.0;
	}
	// Blue Channel
	if (br) {
		col.b = col.r;
	}
	if (bg) {
		col.b = col.g;
	}
	//if (ba) {
	//	col.b = col.a;
	//}
	if (bx) {
		col.b = 0.0;
	}
	
	// Alpha Channel
	//if (ar) {
	//	col.a = col.r;
	//}
	//if (ag) {
	//	col.a = col.g;
	//}
	//if (ab) {
	//	col.a = col.b;
	//}
	//if (ax) {
	//	col.a = 1.0;
	//}
	
	return col;
}
*/

technique RGB {
	pass PassRGB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
