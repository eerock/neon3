// Photo - Technicolor

float LowR : VAR1;
float HighR : VAR2;
float LowG : VAR3;
float HighG : VAR4;
float LowB : VAR5;
float HighB : VAR6;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSTwoStrip(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	float4 halfColor = color.rgba * 0.5;
	float4 outColor = color;
	outColor.r = color.r - halfColor.g + halfColor.b;
	outColor.g = color.g - halfColor.r + halfColor.b;
	outColor.b = color.b - halfColor.r + halfColor.g;
	return outColor;
}

float4 PSThreeStrip(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	float redMatte = color.r - max(color.g, color.b);
	redMatte = 1.0 - redMatte;
	if (redMatte > HighR) {
		redMatte = color.a;
	}
	if (redMatte < LowR) {
		redMatte = 0.0;
	} else {
		redMatte = (redMatte - LowR) / ((HighR - LowR) + 0.000001);
	}
	float greenMatte = color.g - max(color.r, color.b);
	greenMatte = 1.0 - greenMatte;
	if (greenMatte > HighG) {
		greenMatte = color.a;
	}
	if (greenMatte < LowG) {
		greenMatte = 0.0;
	} else {
		greenMatte = (greenMatte - LowG) / ((HighG - LowG) + 0.000001);
	}
	float blueMatte = color.b - max(color.r, color.g);
	blueMatte = 1.0 - blueMatte;
	if (blueMatte > HighB) {
		blueMatte = color.a;
	}
	if (blueMatte < LowB) {
		blueMatte = 0.0;
	} else {
		blueMatte = (blueMatte - LowB) / ((HighB - LowB) + 0.000001);
	}
	float4 outColor;
	outColor.r = color.r * greenMatte * blueMatte;
	outColor.g = color.g * redMatte * blueMatte;
	outColor.b = color.b * redMatte * greenMatte;
	outColor.a = color.a;
	return outColor;
}

technique TwoStrip {
	pass PassTwoStrip {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSTwoStrip();
	}
}

technique ThreeStrip {
	pass PassThreeStrip {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSThreeStrip();
	}
}
