// Distort - Tile

float tileX : VAR1;
float tileY : VAR2;
float screenW : SCREENW;
float screenH : SCREENH;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR0 {
	float tx = floor(pow(screenW, tileX));
	float ty = floor(pow(screenH, tileY));
	float4 col = tex2D(sampler0, In.Tex0 * float2(tx, ty));
	return col;
}

technique Tile {
	pass PassTile {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
