// Noise - Old Film
// ported to Neon from ??? by neon@eerock.com

// Parameters
float Timer : TIME;
float ScratchAmount : VAR1;
float NoiseAmount : VAR2;
float VignetteAmount : VAR3;
float VignettePower : VAR4;
float Frame : VAR5;
float RandomCoord1x : RAND1;
float RandomCoord1y : RAND2;
float RandomCoord2x : RAND3;
float RandomCoord2y : RAND4;

// Source Texture Sampler
sampler2D sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

// Noise Texture Sampler
sampler2D sampler1 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
	float2 Tex1 : TEXCOORD1;
};

void VS(inout float4 vp : POSITION, inout float2 uv0 : TEXCOORD0, inout float2 uv1 : TEXCOORD1) {
	// nothing
}

float4 PS(VS_OUTPUT In) : COLOR {
	// Sample texture
	float4 color = tex2D(sampler0, In.Tex0);

	// Add Scratch
	float2 sc = Frame * float2(0.02, 0.9);
	sc.x = frac(In.Tex0.x + sc.x);
	float scratch = tex2D(sampler0, sc).r;	// doing this gives some vertical lines, and they move along automatically.
	//float scratch = tex2D(sampler1, sc).r;	// original: doing this gives some vertical lines too, but it moves via the Frame parameter.
	
	scratch /= (6 * (1 - ScratchAmount));	// new version!
	scratch = 1 - abs(1 - scratch);
	scratch = max(0, scratch);
	color.rgb += scratch.rrr;

	// Calculate random coord + sample
	float2 rand1 = float2(RandomCoord1x, RandomCoord1y);
	float2 rand2 = float2(RandomCoord2x, RandomCoord2y);
	float2 rCoord = (In.Tex0 + rand1 + rand2) * 0.333333333;	// original
	//float2 rCoord = (In.Tex1 + rand1 + rand2) * 0.333333333;	// test, what's the difference (if any)
	float4 rand = tex2D(sampler1, rCoord);

	// Add noise
	if ((1 - NoiseAmount) > rand.r) {
		color.rgb += 0.1 + rand.rgb * 0.4;
	}

	// Convert to gray + desaturated Sepia
	float gray = dot(color, float4(0.3, 0.59, 0.11, 0));
	color = float4(gray * float3(0.9, 0.8, 0.6), 1);

	// Calc distance to center
	float2 dist = 0.5 - In.Tex0;

	// Random light fluctuation
	float fluc = RandomCoord2x * 0.04 - 0.02;

	// Vignette effect
	color.rgb *= (VignetteAmount + fluc - dot(dist, dist)) * VignettePower;

	return color;
}


technique OldFilm {
	pass PassOldFilm {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
