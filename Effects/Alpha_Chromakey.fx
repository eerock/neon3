// Alpha - Chroma Key
// Chromakey Simple from TextureFX @ vvvv.org
// Ported by to Neon by charles@photropik.com
// October 2012

float hue : VAR1;
float range : VAR2;
float brightclip : VAR3;

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION0, inout float2 uv : TEXCOORD0) {}

float4 PS(float2 uv : TEXCOORD0): COLOR {
	float4 col = tex2D(sampler0, uv);
	float r, g, b, delta;
	float colorMax, colorMin;
	float h = 0.0, s = 0.0, v = 0.0;
	float4 hsv = 0.0;
	
	r = col[0];
	g = col[1];
	b = col[2];
	
	colorMax = max(r, g);
	colorMax = max(colorMax, b);
	
	colorMin = min(r, g);
	colorMin = min(colorMin, b);
	
	v = colorMax;	// this is value of HSV
	
	if (colorMax != 0) {
		s = (colorMax - colorMin) / colorMax;
	}
	
	if (s != 0) {	// if not achromatic
		delta = colorMax - colorMin;
		if (r == colorMax) {
			h = (g - b) / delta;
		} else if (g == colorMax) {
			h = 2.0 + (b - r) / delta;
		} else {	// b is max
			h = 4.0 + (r - g) / delta;
		}
		h *= 60.0;
		
		if (h < 0.0) {
			h += 360.0;
		}
		
		hsv[0] = h / 360.0;	// scale h between 0 and 1
		hsv[1] = s;
		hsv[3] = v;
	}
	
	if (hsv[0] < (hue + range) && hsv[0] > (hue - range) && hsv[3] > brightclip) {	// && hsv[1] > sat
		col.a = 0.0;
	}
	
	return col;
}

technique ChromaKey { 
	pass PassChromaKey {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
