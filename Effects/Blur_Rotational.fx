// Blur - Rotational

float rotate : VAR1;
float centerX : VAR2;
float centerY : VAR3;
float screenW : SCREENW;
float screenH : SCREENH;
float quality : VAR4;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR {
	float2 direction = float2(centerX, centerY);
	int SAMPLES = floor(quality);
	float2 offsetCoord = In.Tex0 - float2(0.5, 0.5);
	float r = length(offsetCoord);
	float theta = atan2(offsetCoord.y, offsetCoord.x);
	float4 sum = (float4)0;
	float2 tapCoords = (float2)0;

	for (float i = 0; i < SAMPLES; i++) {
		float tapTheta = theta + i * (rotate / SAMPLES);
		float tapR = r;
		tapCoords.x = tapR * cos(tapTheta) + 0.5;
		tapCoords.y = tapR * sin(tapTheta) + 0.5;
		// using tex2Dlod as otherwise the compiler freaks out
		sum += tex2Dlod(sampler0, float4(tapCoords, 0, 0));
	}
	sum /= SAMPLES;
	return sum;
}

technique RotationalBlur {
	pass PassRotationalBlur {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
