// Color - Levels A

float blackPoint : VAR1;
float whitePoint : VAR2;
float grayPoint : VAR3;
float blackOffset : VAR4;
float whiteOffset : VAR5;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float4 Col0 : COLOR0;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float levels(float value) {
	float clampedBlackPoint = clamp(blackPoint, blackPoint, whitePoint - 0.008);
	float clampedWhitePoint = clamp(whitePoint, blackPoint + 0.008, whitePoint);
	float clampedGrayPoint = clamp(grayPoint, blackPoint + 0.004, whitePoint - 0.004);
	value = saturate((value - clampedBlackPoint) / (clampedWhitePoint - clampedBlackPoint));
	value = pow(value, clampedGrayPoint);
	value = value * (whiteOffset - blackOffset) + blackOffset;
	return value;
}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	col.r = levels(col.r);
	col.g = levels(col.g);
	col.b = levels(col.b);
	return col;
}

technique LevelsA {
	pass PassLevelsA {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
