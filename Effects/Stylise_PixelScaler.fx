//
// Pixelate/Scaler
// written for Neon by Eric Phister (eerock@gmail.com)
//
float fResolution : VAR1;
float invScreenW : INVSCREENW;
float invScreenH : INVSCREENH;
float screenW : SCREENW;
float screenH : SCREENH;

struct PS_INPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sSampler = sampler_state {
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

float4 PS(PS_INPUT In) : COLOR0 {
	// ok, this works, but it's essentially the same code as Pixelate Brick.
	// what was I originally trying to do with this shader??
	float tx = floor(pow(abs(screenW), fResolution));
	float ty = floor(pow(abs(screenH), fResolution));
	float2 rez = float2(tx, ty);
	float2 size = 1.0 / rez;
	float2 count = floor(In.Tex0 / size);
	float2 center = count * size + size * 0.5;
	float4 color = tex2D(sSampler, center);

	return color;
}


void VS(inout float4 vp: POSITION, inout float2 uv: TEXCOORD0) {}

technique PixScaler {
	pass PS_0 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
