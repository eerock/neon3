// Color - Colorize HSV

float v_Hue : VAR1;
float v_Sat : VAR2;
float v_Value : VAR3;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

float4 RGB_to_HSV(float4 incolor) {
	float delta;
	float colorMax, colorMin;
	float h, s, v;
	colorMax = max(incolor.r, incolor.g);
	colorMax = max(colorMax, incolor.b);
	colorMin = min(incolor.r, incolor.g);
	colorMin = min(colorMin, incolor.b);
	v = colorMax;
	if (colorMax != 0.0) {
		s = (colorMax - colorMin) / colorMax;
	}

	if (s != 0.0) {
		delta = colorMax - colorMin;
		if (incolor.r == colorMax) {
			h = (incolor.g - incolor.b) / delta;
		} else if (incolor.g == colorMax) {
			h = 2.0 + (incolor.b - incolor.r) / delta;
		} else {
			h = 4.0 + (incolor.r - incolor.g) / delta;
		}
		h *= 60.0;
		if (h < 0.0) {
			h += 360.0;
		}
		h = h / 360.0;
	}
	float4 hsv = { h, s, v, 1.0 };
	return hsv;
}

float4 HSV_to_RGB(float4 hsv) {
	float4 outcolor = { 0.0, 0.0, 0.0, 1.0 };
	float p, q, t;
	float h, s, v;
	float i;
	if (hsv[1] == 0) {
		outcolor = hsv[2];
	} else {
		h = hsv.x * 360.0;
		s = hsv.y;
		v = hsv.z;
		if (h == 360.0) {
			h = 0.0;
		}
		h /= 60.0;
		i = floor(h);
		p = v * (1.0 - s);
		q = v * (1.0 - (s * (h - i)));
		t = v * (1.0 - (s * (1.0 - (h - i))));
		if (i == 0) {
			outcolor.r = v;
			outcolor.g = t;
			outcolor.b = p;
		} else if(i == 1) {
			outcolor.r = q;
			outcolor.g = v;
			outcolor.b = p;
		} else if(i == 2) {
			outcolor.r = p;
			outcolor.g = v;
			outcolor.b = t;
		} else if (i == 3) {
			outcolor.r = p;
			outcolor.g = q;
			outcolor.b = v;
		} else if (i == 4) {
			outcolor.r = t;
			outcolor.g = p;
			outcolor.b = v;
		} else if (i == 5) {
			outcolor.r = v;
			outcolor.g = p;
			outcolor.b = q;
		} else {
			outcolor.r = v;
			outcolor.g = t;
			outcolor.b = p;
		}
	}
	return outcolor;
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float hue = v_Hue * 2.0 - 1.0;
	float sat = v_Sat * 2.0 - 1.0;
	float value = v_Value * 2.0 - 1.0;
	float4 col = tex2D(sampler0, In.Tex0);
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	outColor = RGB_to_HSV(col);
	outColor[0] = (abs(hue));
	outColor[1] = saturate(outColor[1] + sat);
	outColor[2] = saturate(outColor[2] + value);
	outColor = HSV_to_RGB(outColor);
	outColor.a = col.a;
	return outColor;
}

technique ColorizeHSV {
	pass PassColorizeHSV {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
