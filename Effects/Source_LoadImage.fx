// Source - Load Image

float Src02Alpha : VAR1;
float playback : VAR2;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
	float2 Tex1 : TEXCOORD1;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

sampler sampler1 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp : POSITION, inout float2 uv0 : TEXCOORD0, inout float2 uv1 : TEXCOORD1) {}

float4 PSBlend(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	
	float3 texCoords = float3(In.Tex1, playback);
	float4 src02 = tex3D(sampler1, texCoords);
	outColor = lerp(src02, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSMultiply(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	src02.r = src02.r * src01.r;
	src02.g = src02.g * src01.g;
	src02.b = src02.b * src01.b;
	outColor.r = lerp(src02.r, src01.r, 1.0 - Src02Alpha);
	outColor.g = lerp(src02.g, src01.g, 1.0 - Src02Alpha);
	outColor.b = lerp(src02.b, src01.b, 1.0 - Src02Alpha);
	outColor.a = lerp(src02.a, src01.a, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSScreen(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	src02 = 1.0 - (1.0 - (src01)) * (1.0 - (src02));
	outColor = lerp(src02, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSDarken(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src01.r < src02.r) ? src01.r : src02.r;
	temp.g = (src01.g < src02.g) ? src01.g : src02.g;
	temp.b = (src01.b < src02.b) ? src01.b : src02.b;
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSLighten(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src01.r > src02.r) ? src01.r : src02.r;
	temp.g = (src01.g > src02.g) ? src01.g : src02.g;
	temp.b = (src01.b > src02.b) ? src01.b : src02.b;
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSDifference(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = abs(src01.r - src02.r);
	temp.g = abs(src01.g - src02.g);
	temp.b = abs(src01.b - src02.b);
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSExclusion(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = src01.r + src02.r - (2.0 * (src01.r * src02.r));
	temp.g = src01.g + src02.g - (2.0 * (src01.g * src02.g));
	temp.b = src01.b + src02.b - (2.0 * (src01.b * src02.b));
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSOverlay(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src01.r < 0.5) ?
		(2.0 * (src01.r * src02.r)) : 1.0 - (2.0 * ((1.0 - src01.r) * (1.0 - src02.r)));
	temp.g = (src01.g < 0.5) ?
		(2.0 * (src01.g * src02.g)) : 1.0 - (2.0 * ((1.0 - src01.g) * (1.0 - src02.g)));
	temp.b = (src01.b < 0.5) ?
		(2.0 * (src01.b * src02.b)) : 1.0 - (2.0 * ((1.0 - src01.b) * (1.0 - src02.b)));
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSHardlight(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src02 = tex2D(sampler0, In.Tex0);
	float4 src01 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src02.r < 0.5) ?
		(2.0 * (src01.r * src02.r)) : 1.0 - (2.0 * ((1.0 - src01.r) * (1.0 - src02.r)));
	temp.g = (src02.g < 0.5) ?
		(2.0 * (src01.g * src02.g)) : 1.0 - (2.0 * ((1.0 - src01.g) * (1.0 - src02.g)));
	temp.b = (src02.b < 0.5) ?
		(2.0 * (src01.b * src02.b)) : 1.0 - (2.0 * ((1.0 - src01.b) * (1.0 - src02.b)));
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSSoftlight(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = 2.0 * (src01.r * src02.r) + src01.r * src01.r - 2.0 * (pow(src01.r, 2.0) * src02.r);
	temp.g = 2.0 * (src01.g * src02.g) + src01.g * src01.g - 2.0 * (pow(src01.g, 2.0) * src02.g);
	temp.b = 2.0 * (src01.b * src02.b) + src01.b * src01.b - 2.0 * (pow(src01.b, 2.0) * src02.b);
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSDodge(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src02.r == 1.0) ? 1.0 : src01.r / (1.0 - src02.r);
	temp.g = (src02.g == 1.0) ? 1.0 : src01.g / (1.0 - src02.g);
	temp.b = (src02.b == 1.0) ? 1.0 : src01.b / (1.0 - src02.b);
	
	if (temp.r > 1.0) {
		temp.r = 1.0;
	}
	if (temp.g > 1.0) {
		temp.g = 1.0;
	}
	if (temp.b > 1.0) {
		temp.b = 1.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSBurn(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src02.r == 0.0) ? 0.0 :  1.0 - (1.0 - src01.r) / src02.r;
	temp.g = (src02.g == 0.0) ? 0.0 :  1.0 - (1.0 - src01.g) / src02.g;
	temp.b = (src02.b == 0.0) ? 0.0 :  1.0 - (1.0 - src01.b) / src02.b;
	if (temp.r < 0.0) {
		temp.r = 0.0;
	}
	if (temp.g < 0.0) {
		temp.g = 0.0;
	}
	if (temp.b < 0.0) {
		temp.b = 0.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSReflect(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src02.r == 1.0) ? 1.0 : (src01.r * src01.r) / (1.0 - src02.r);
	temp.g = (src02.g == 1.0) ? 1.0 : (src01.g * src01.g) / (1.0 - src02.g);
	temp.b = (src02.b == 1.0) ? 1.0 : (src01.b * src01.b) / (1.0 - src02.b);
	if (temp.r > 1.0) {
		temp.r = 1.0;
	}
	if (temp.g > 1.0) {
		temp.g = 1.0;
	}
	if (temp.b > 1.0) {
		temp.b = 1.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSGlow(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src02.r == 1.0) ? 1.0 : (src02.r * src02.r) / (1.0 - src01.r);
	temp.g = (src02.g == 1.0) ? 1.0 : (src02.g * src02.g) / (1.0 - src01.g);
	temp.b = (src02.b == 1.0) ? 1.0 : (src02.b * src02.b) / (1.0 - src01.b);
	if (temp.r > 1.0) {
		temp.r = 1.0;
	}
	if (temp.g > 1.0) {
		temp.g = 1.0;
	}
	if (temp.b > 1.0) {
		temp.b = 1.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSFreeze(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src02.r == 0.0) ? 0.0 : 1.0 - (1.0 - (src01.r * src01.r)) / src02.r;
	temp.g = (src02.g == 0.0) ? 0.0 : 1.0 - (1.0 - (src01.g * src01.g)) / src02.g;
	temp.b = (src02.b == 0.0) ? 0.0 : 1.0 - (1.0 - (src01.b * src01.b)) / src02.b;
	if (temp.r < 0.0) {
		temp.r = 0.0;
	}
	if (temp.g < 0.0) {
		temp.g = 0.0;
	}
	if (temp.b < 0.0) {
		temp.b = 0.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSHeat(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src01.r == 0.0) ? 0.0 : 1.0 - (1.0 - (src02.r * src02.r)) / src01.r;
	temp.g = (src01.g == 0.0) ? 0.0 : 1.0 - (1.0 - (src02.g * src02.g)) / src01.g;
	temp.b = (src01.b == 0.0) ? 0.0 : 1.0 - (1.0 - (src02.b * src02.b)) / src01.b;
	if (temp.r < 0.0) {
		temp.r = 0.0;
	}
	if (temp.g < 0.0) {
		temp.g = 0.0;
	}
	if (temp.b < 0.0) {
		temp.b = 0.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSAdd(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp = src01 + src02;
	if (temp.r > 1.0) {
		temp.r = 1.0;
	}
	if (temp.g > 1.0) {
		temp.g = 1.0;
	}
	if (temp.b > 1.0) {
		temp.b = 1.0;
	}
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

float4 PSSubtract(VS_OUTPUT In) : COLOR {
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = tex2D(sampler1, In.Tex1);
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp = src01 - src02;
	if (temp.r < 0.0) {
		temp.r = 0.0;
	}
	if (temp.g < 0.0) {
		temp.g = 0.0;
	}
	if (temp.b < 0.0) {
		temp.b = 0.0;
	}	
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	return outColor;
}

technique Blend {
	pass PassBlend {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSBlend();
	}
}

technique Multiply {
	pass PassMultiply {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSMultiply();
	}
}

technique Screen {
	pass PassScreen {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSScreen();
	}
}

technique Darken {
	pass PassDarken {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSDarken();
	}
}

technique Lighten {
	pass PassLighten {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSLighten();
	}
}

technique Difference {
	pass PassDifference {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSDifference();
	}
}

technique Exclusion {
	pass PassExclusion {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSExclusion();
	}
}

technique Overlay {
	pass PassOverlay {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSOverlay();
	}
}

technique Hardlight {
	pass PassHardLight {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSHardlight();
	}
}

technique Softlight {
	pass PassSoftLight {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSSoftlight();
	}
}

technique Dodge {
	pass PassDodge {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSDodge();
	}
}

technique Burn {
	pass PassBurn {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSBurn();
	}
}

technique Reflect {
	pass PassReflect {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSReflect();
	}
}

technique Glow {
	pass PassGlow {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSGlow();
	}
}

technique Freeze {
	pass PassFreeze {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSFreeze();
	}
}

technique Heat {
	pass PassHeat {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSHeat();
	}
}

technique Add {
	pass PassAdd {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSAdd();
	}
}

technique Subtract {
	pass PassSubtract {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSSubtract();
	}
}
