// Stylize - RGB Shift

float vr : VAR1;
float vg : VAR2;
float vb : VAR3;
float va : VAR4;
float randomIntensity : VAR5;
float sintime : SINTIME;
float time : TIME;
float V_Rand1 : RAND1;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float randomizer = V_Rand1 * randomIntensity;
	float r = vr / 10.0;
	float g = vg / 10.0;
	float b = vb / 10.0;
	float a = va + randomizer;
	a = lerp(0.0, 6.28318531, a);
	float cosa = cos(a);
	float sina = sin(a);
	float red = tex2D(sampler0, In.Tex0 + float2(cosa, sina) * r).r;
	float green = tex2D(sampler0, In.Tex0 + float2(cosa, sina) * g).g;
	float blue = tex2D(sampler0, In.Tex0 + float2(cosa, sina) * b).b;
	float alpha = tex2D(sampler0, In.Tex0).a;
	float4 col = float4(red, green, blue, alpha);
	return col;
}

technique RGBShift {
	pass PassRGBShift {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
