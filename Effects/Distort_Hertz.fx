// Distort - Hertz

float vDistortion : VAR1;
float direction : VAR4;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float distortion = pow(vDistortion, 5.0);
	float2 p = In.Tex0;
	p.x = p.y;
	float4 key = tex2D(sampler0, p);
	float d = (key.r + key.g + key.b) / 3.0;
	p = In.Tex0;
	p.x -= d * vDistortion * (direction - 0.5) * 2.0;
	p.x = (p.x + 10.0) % 1.0;
	return tex2D(sampler0, p);
}

technique Hertz {
	pass PassHertz {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
