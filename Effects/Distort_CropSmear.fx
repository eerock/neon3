// Distort - Crop / Smear Effect
// by Charles Crammond 
// charles@photropik.com
// October 2012

float screenW : SCREENW;
float screenH : SCREENH;
float v1 : VAR1;
float v2 : VAR2;
float v3 : VAR3;
float v4 : VAR4;
bool smear : VAR5;
bool processAlpha : VAR6;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
	//float4 Color: COLOR0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 QuadPosition : POSITION, inout float2 myUV : TEXCOORD0) {}

float4 PSLeftRight(VS_OUTPUT In, float2 QuadPosition : vpos) : COLOR {
	float left = v1 * screenW;
	float right = v2 * screenW;
	float bottom = v3 * screenH;
	float top = v4 * screenH;
	float2 cnt = In.Tex0;
	float4 col = tex2D(sampler0, cnt);

	if (QuadPosition.x < left) {
		if (smear) {
			col = tex2D(sampler0, float2(v1, cnt.y));
		} else {
			col.a = 0.0;
		}
	}
	if (QuadPosition.x > right) {
		if (smear) {
			col = tex2D(sampler0, float2(v2, cnt.y));
		} else {
			col.a = 0.0;
		}
	}
	if (QuadPosition.y > top) {
		if (smear) {
			col = tex2D(sampler0, float2(cnt.x, v4));
		} else {
			col.a = 0.0;
		}
	}
	if (QuadPosition.y < bottom) {
		if (smear) {
			col = tex2D(sampler0, float2(cnt.x, v3));
		} else {
			col.a = 0.0;
		}
	}
	return col;
}

float4 PSTopBottom(VS_OUTPUT In, float2 QuadPosition : vpos) : COLOR {
	float left = v1 * screenW;
	float right = v2 * screenW;
	float bottom = v3 * screenH;
	float top = v4 * screenH;
	float2 cnt = In.Tex0;
	float4 col = tex2D(sampler0, cnt);

	if (QuadPosition.y > top) {
		if (smear) {
			col = tex2D(sampler0, float2(cnt.x, v4));
		} else {
			col.a = 0.0;
		}
	}
	if (QuadPosition.y < bottom) {
		if (smear) {
			col = tex2D(sampler0, float2(cnt.x, v3));
		} else {
			col.a = 0.0;
		}
	}
	if (QuadPosition.x < left) {
		if (smear) {
			col = tex2D(sampler0, float2(v1, cnt.y));
		} else {
			col.a = 0.0;
		}
	}
	if (QuadPosition.x > right) {
		if (smear) {
			col = tex2D(sampler0, float2(v2, cnt.y));
		} else {
			col.a = 0.0;
		}
	}
	if (!processAlpha) {
		col.a = 1.0;
	}
	return col;
}

technique LeftRight {
	pass PassLeftRight {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSLeftRight();
	}
}

technique TopBottom {
	pass PassTopBottom {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSTopBottom();
	}
}
