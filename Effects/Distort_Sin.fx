// Distort - Sin

float V_Time : TIME;
float V_Alpha : ALPHA;
float V_Var1 : VAR1;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR0 {
	float a = (In.Tex0.x + (cos(V_Time * 1.67 + In.Tex0.y * 1.45 + 2.10) * 1.56 + cos(V_Time * 1.74 + In.Tex0.y * 1.39 + 1.87)) * V_Var1);
	float b = (In.Tex0.y + (cos(V_Time * 1.25 + In.Tex0.x * 1.31 + 0.65) * 2.01 + cos(V_Time * 1.45 + In.Tex0.x * 1.62 + 1.25)) * V_Var1);
	float4 color = tex2D(sampler0, frac(float2(a, b)));
	color.a = V_Alpha * color.a;
	return color;
}

technique Sin {
	pass PassSin {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
