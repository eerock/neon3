// Photo - Stranger

float r1 : VAR1;
float g1 : VAR2;
float b1 : VAR3;
float mix : VAR4;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float4 orig = tex2D(sampler0, In.Tex0);
	col.r = sin(col.r * pow(r1, 3.0));
	col.g = sin(col.g * pow(b1, 3.0));
	col.b = sin(col.b * pow(g1, 3.0));
	float4 finalmix = lerp(orig, col, mix);
	if (!processAlpha) {
		finalmix.a = 1.0;
	}
	return finalmix;
}

technique Stranger {
	pass PassStranger {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
