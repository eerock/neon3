// Color - Hue Brightness Saturation Contrast A
// Amount to shift the Hue, range 0 to 6

float Hue : VAR1;
float Brightness : VAR2;
float Contrast : VAR3;
float Saturation : VAR4;

sampler sampler0 : register(S0);

float3x3 QuaternionToMatrix(float4 quat) {
	float3 cross = quat.yzx * quat.zxy;
	float3 square= quat.xyz * quat.xyz;
	float3 wimag = quat.w * quat.xyz;

	square = square.xyz + square.yzx;

	float3 diag = 0.5 - square;
	float3 a = (cross + wimag);
	float3 b = (cross - wimag);

	return float3x3(
		2.0 * float3(diag.x, b.z, a.y),
		2.0 * float3(a.z, diag.y, b.x),
		2.0 * float3(b.y, a.x, diag.z)
	);
}

static const float3 lumCoeff = { 0.2125, 0.7154, 0.0721 };

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(float2 uv : TEXCOORD0): COLOR	{
	float4 outputColor = tex2D(sampler0, uv);
	float3 hsv;
	float3 intensity;
	float3 root3 = float3(0.57735, 0.57735, 0.57735);
	float half_angle = 0.5 * radians(Hue); // Hue is radians of 0 tp 360 degree
	float4 rot_quat = float4((root3 * sin(half_angle)), cos(half_angle));
	float3x3 rot_Matrix = QuaternionToMatrix(rot_quat);
	outputColor.rgb = mul(rot_Matrix, outputColor.rgb);
	outputColor.rgb = (outputColor.rgb - 0.5) * (Contrast + 1.0) + 0.5;

	intensity = float(dot(outputColor.rgb, lumCoeff));
	outputColor.rgb = lerp(intensity, outputColor.rgb, Saturation);
	return outputColor;
}

technique HBSC {
	pass PassHBSC {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
