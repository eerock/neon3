// Blur - Radial Blur 
// update by charles@photropik.com0
// October 2012

//float screenW: SCREENW;
//float screenH: SCREENH;
//bool Aspect: VAR6;

float vRotate : VAR1;
float vZoomWidth : VAR2;
float vZoom : VAR3;
float linkScale : VAR4;
float quality : VAR5;
float mixTaps : VAR6;
float BlurX : VAR7;
float BlurY : VAR8;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = ANISOTROPIC;
};

void VS(inout float4 vp : POSITION0, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR0 {
	// Zoom
	float blurWidth = 2.0 * vZoomWidth - 1.0;
	float zoom;
	
	// Link scale to zoom value option
	if (linkScale && (vZoomWidth > 0.5)) {
		zoom = (2.0 * vZoomWidth - 1.0) * -1.0;
	} else {
		zoom = (2.0 * vZoom - 1.0) * -1.0;
	}
	
	// Store unmolested uvs
	float2 uv = In.Tex0;
	
	// Rotate
	float2 offsetCoord = In.Tex0 - float2(0.5, 0.5);
	float r = length(offsetCoord);
	float theta = atan2(offsetCoord.y, offsetCoord.x);
	float4 sum = (float4)0.0;
	float2 tapCoordsRotate = (float2)0.0;
	int SAMPLES = floor(quality);
	
	for (float i = 0; i < SAMPLES; i++) {
		float2 direction = float2(BlurX, BlurX);

		// Offset Center
		float offsetX = (BlurX * (i / (SAMPLES - 1)));
		float offsetY = (BlurY * (i / (SAMPLES - 1)));
		float2 offset = float2(offsetX, offsetY);
		
		// Zoom 
		float scale = (blurWidth * (i / (SAMPLES - 1)));
		scale += zoom;
		float2 tapCoordsZoom = ((In.Tex0 - 0.5 - direction) * scale) + uv;	// + offset;
		
		// Rotate
		float tapTheta = theta + i * (vRotate / SAMPLES);
		float tapR = r;
		tapCoordsRotate.x = tapR * cos(tapTheta) + 0.5 + BlurX;
		tapCoordsRotate.y = tapR * sin(tapTheta) + 0.5 + BlurY;
		
		// Blend
		float2 tapCoordsMixed = lerp(tapCoordsZoom, tapCoordsRotate, mixTaps);
		
		sum += tex2Dlod(sampler0, float4(tapCoordsMixed, 0, 0));
	}
	sum /= SAMPLES;
	
	if (!processAlpha) {
		sum.a = 1.0;
	}
	return sum;
}

technique Clamp {
	pass PassClamp {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Mirror {
	pass PassMirror {
		AddressU[0] = MIRROR;
		AddressV[0] = MIRROR;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Wrap {
	pass PassWrap {
		AddressU[0] = WRAP;
		AddressV[0] = WRAP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique Border {
	pass PassBorder {
		AddressU[0] = BORDER;
		AddressV[0] = BORDER;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
