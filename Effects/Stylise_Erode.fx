// Stylize - Erode

float V_Var1 : VAR1;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;	// ??
	AddressU = WRAP;	// ??
	AddressV = WRAP;
};

float2 samples[8] = {
	-1.0, -1.0,
	0.0, -1.0,
	1.0, -1.0,
	-1.0, 0.0,
	1.0, 0.0,
	-1.0, 1.0,
	0.0, 1.0,
	1.0, 1.0,
};

float2 NPR_DilateErode_Erode_Pixel_Shader_samples[8] = {
	-1.0, -1.0,
	0.0, -1.0,
	1.0, -1.0,
	-1.0, 0.0,
	1.0, 0.0,
	-1.0, 1.0,
	0.0, 1.0,
	1.0, 1.0,
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float Dilate_pixelSize = V_Var1 / 10.0;
	float4 minSamp = tex2D(sampler0, In.Tex0);
	for (int i = 0; i < 8; i++) {
		float4 sample = tex2D(sampler0, In.Tex0 + Dilate_pixelSize * NPR_DilateErode_Erode_Pixel_Shader_samples[i]);
		minSamp = min(minSamp, sample);
	}	
	if (!processAlpha) {
		minSamp.a = 1.0;
	}
	return minSamp;
}

technique Erode {
	pass PassErode {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
