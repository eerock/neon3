// Stylize - Bloom

float BloomScale : VAR1;
float blur : VAR2;
bool processAlpha : VAR16;

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSBloomX(float2 Tex0 : TEXCOORD0) : COLOR {
	static const int g_cKernelSize = 13;
	float define = blur * 100.0 + 100.0;
	float2 TexelKernel[13] = {{ -6.0, 0.0 }, { -5.0, 0.0 }, { -4.0, 0.0 }, { -3.0, 0.0 }, { -2.0, 0.0 }, { -1.0, 0.0 }, { 0.0, 0.0 }, { 1.0, 0.0 }, { 2.0, 0.0 }, { 3.0, 0.0 }, { 4.0, 0.0 }, { 5.0, 0.0 }, { 6.0, 0.0 }, };
	const float BlurWeights[13] = { 0.002216, 0.008764, 0.026995, 0.064759, 0.120985, 0.176033, 0.199471, 0.176033, 0.120985, 0.064759, 0.026995, 0.008764, 0.002216, };
	float4 Color = 0.0;
	for (int i = 0; i < g_cKernelSize; i++) {
		Color += tex2D(sampler0, Tex0 + TexelKernel[i].xy / define) * BlurWeights[i];
	}
	Color = tex2D(sampler0, Tex0) + Color * BloomScale;
	if (!processAlpha) {
		Color.a = 1.0;
	}
	return Color;
}

float4 PSBloomY(float2 Tex0 : TEXCOORD0) : COLOR {
	static const int g_cKernelSize = 13;
	float define = blur * 100.0 + 100.0;
	float2 TexelKernel[13] = {{ -6.0, 0.0 }, { -5.0, 0.0 }, { -4.0, 0.0 }, { -3.0, 0.0 }, { -2.0, 0.0 }, { -1.0, 0.0 }, { 0.0, 0.0 }, { 1.0, 0.0 }, { 2.0, 0.0 }, { 3.0, 0.0 }, { 4.0, 0.0 }, { 5.0, 0.0 }, { 6.0, 0.0 }, };
	const float BlurWeights[13] = { 0.002216, 0.008764, 0.026995, 0.064759, 0.120985, 0.176033, 0.199471, 0.176033, 0.120985, 0.064759, 0.026995, 0.008764, 0.002216, };
	float4 Color = 0.0;
	for (int i = 0; i < g_cKernelSize; i++) {
		Color += tex2D(sampler0, Tex0 + TexelKernel[i].yx / define) * BlurWeights[i];
	}
	Color = Color * BloomScale * 1.5;
	Color = tex2D(sampler0, Tex0) + Color * BloomScale;
	if (!processAlpha) {
		Color.a = 1.0;
	}
	return Color;
}

technique BloomX {
	pass PassBloomX {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSBloomX();
	}
}

technique BloomY {
	pass PassBloomY {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSBloomY();
	}
}
