// Color - Exposure Offset Gamma
// Rudimentary Exposure Offset and Gamma

float exposure : VAR1;
float offset : VAR2;
float vgamma : VAR3;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float gamma = 1.0 / vgamma;
	float4 input = tex2D(sampler0, In.Tex0);
	float4 output = 1.0;

	input *= exposure;
	output = pow(input + offset, gamma);
	return output;
}

technique ExposureOffsetGamma {
	pass PassExposureOffsetGamma {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
