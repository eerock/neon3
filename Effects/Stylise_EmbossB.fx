// Stylize - Emboss B

float V_InvScreenW : INVSCREENW;
float V_InvScreenH : INVSCREENH;
float luminosity : VAR1;
float contrast : VAR2;
float power : VAR3;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR0 {
	float4 cori = tex2D(sampler0, In.Tex0);
	float4 ctex;
	ctex.a = cori.a;
	if (!processAlpha) {
		ctex.a = 1.0;
	}
	ctex.rgb = luminosity;
	ctex -= tex2D(sampler0, In.Tex0 - 0.001) * 10.0 * contrast;
	ctex += tex2D(sampler0, In.Tex0 + 0.001) * 10.0 * contrast;
	ctex.rgb = (ctex.r + ctex.g + ctex.b) / 3.0;
	return ((ctex * power) + cori * (1.0 - power));
}

technique EmbossB {
	pass PassEmbossB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
