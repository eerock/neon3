//	Screen-space scanline killer / pixel killer
//	Kills every other scanline or every other pixel
//	Original code by <unknown>.  Need to look that up.
// 	Neon port by eerock: neon@eerock.com
// 	August 2012

float screenW : SCREENW;
float screenH : SCREENH;
bool processAlpha : VAR1;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	//float4 Color: COLOR0;
	float2 Tex0 : TEXCOORD;
};

sampler2D sampler0: register(s0);

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSOddRow(VS_OUTPUT In): COLOR {
	// Default color is fully transparent
	// could add a colour in here.
	float4 color = 0.0;
	
	// Scale to int texture size
	float row = In.Tex0.y * screenH * 0.5;
	
	// Calc diff between rounded half and half to get 0 or 0.5
	float diff = round(row) - row;
	float diffSq = diff * diff;
	
	// Even or odd? Only even lines are sampled
	if (diffSq < 0.1) {
		color = tex2D(sampler0, In.Tex0);
	}
	// Set the alpha to 1.0 if we don't want a transparent grid.
	if (!processAlpha) {
		color.a = 1.0;
	}
	return color;
}

float4 PSOddColumn(VS_OUTPUT In) : COLOR {
	// Default color is fully transparent
	float4 color = 0.0;
	
	// Scale to int texture size
	float col = In.Tex0.x * screenW * 0.5;
	
	// Calc diff between rounded half and half to get 0 or 0.5
	float diff = round(col) - col;
	float diffSq = diff * diff;
	
	// Even or odd? Only even lines are sampled
	if (diffSq < 0.1) {
		color = tex2D(sampler0, In.Tex0);
	}	

	if (!processAlpha) {
		color.a = 1.0;
	}
	return color;
}

float4 PSOddPixel(VS_OUTPUT In) : COLOR {
	// Default color is fully transparent
	float4 color = 0.0;
	
	// Scale to int texture size, add x and y
	float2 vpos = { In.Tex0.x * screenW * 0.5, In.Tex0.y * screenH * 0.5 };
	float vposSum = vpos.x + vpos.y;
	
	// Calc diff between rounded half and half to get 0 or 0.5
	float diff = round(vposSum) - vposSum;
	float diffSq = diff * diff;
	
	// Even or odd? Only even pixels are sampled
	if (diffSq < 0.1) {
		color = tex2D(sampler0, In.Tex0);
	}
	if (!processAlpha) {
		color.a = 1.0;
	}
	return color;
}

technique KillOddRow {
	pass PassKillOddRow {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSOddRow();
	}
}

technique KillOddColumn {
	pass PassKillOddColumn {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSOddColumn();
	}
}

technique KillOddPixel {
	pass PassKillOddPixel {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSOddPixel();
	}
}
