// Neon - Blur X Glow

float V_InvScreenW : INVSCREENW;

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 color = tex2D(sampler0, In.Tex0);
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 1.0, 0.0));
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 1.0, 0.0));
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 2.0, 0.0));
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 2.0, 0.0));
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 3.0, 0.0));
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 3.0, 0.0));
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 4.0, 0.0));
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 4.0, 0.0));
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 5.0, 0.0));
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 5.0, 0.0));
	color += tex2D(sampler0, In.Tex0 + float2(V_InvScreenW * 6.0, 0.0));
	color += tex2D(sampler0, In.Tex0 - float2(V_InvScreenW * 6.0, 0.0));
	color.a /= 13.0;
	color *= 0.2;
	color = color.r * color.g * color.b / 2;	// 3?
	color *= color.a;
	return color;
}

technique Blur {
	pass PassBlur {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
