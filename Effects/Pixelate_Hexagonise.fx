// Stylize - Hexagonize

float vCellSize : VAR1;
float morph : VAR2;
float screenW : SCREENW;	
float screenH : SCREENH;	

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	MipFilter = NONE;		// ebp-> why?
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

float4 lm(float4 c) {
	c.rgb = dot(c.rgb, normalize(float3(0.33, 0.59, 0.11)) / 1.5);
	return c;
}

void VS(inout float4 vp : POSITION0, inout float2 uv : TEXCOORD0) {
	//float2 R = float2(screenW, screenH);
	//vp.xy *= 2;
	//uv += 0.5 / R;
}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 R = float2(screenW, screenH);
	float2 Morph = float2(morph, morph);
	float2 x = In.Tex0;
	float screenAdjustedCellSize = pow(screenW, vCellSize);	
	int2 CellSize = float2(round(screenAdjustedCellSize), round(screenAdjustedCellSize));
	float2 sz = R / min(max(0.5 / R, CellSize), R) * float2(1, 0.577);
	float4 m1 = tex2D(sampler0, lerp(x, (round((x - 0.5) * sz - 0.5) + 0.5) / sz + 0.5, 1.0));
	float4 m2 = tex2D(sampler0, lerp(x, (round((x - 0.5) * sz)) / sz + 0.5, 1.0));
	float4 m = lerp(m1, m2, step(abs(frac((x.y - 0.5) * sz.y + 0.5) - 0.5), 0.25 + (abs(frac((x.x - 0.5) * sz.x + 0.5 + 0.5) - 0.5) - 0.25) / 3.0));
	float4 c1 = tex2D(sampler0, lerp(x, (round((x - 0.5) * sz - 0.5) + 0.5) / sz + 0.5, Morph * lm(m).x));
	float4 c2 = tex2D(sampler0, lerp(x, (round((x - 0.5) * sz)) / sz + 0.5, Morph * lm(m).x));
	float4 c = lerp(c1, c2, step(abs(frac((x.y - 0.5) * sz.y+.5)-.5), 0.25 + (abs(frac((x.x - 0.5) * sz.x + 0.5 + 0.5) - 0.5) - 0.25) / 3.0));
	return c;
}

technique HexMirror {
	pass PassHexMirror {
		AddressU[0] = MIRROR;
		AddressV[0] = MIRROR;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique HexClamp {
	pass PassHexClamp {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}

technique HexBorder {
	pass PassHexBorder {
		AddressU[0] = Border;
		AddressV[0] = Border;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
