// Stylize - Mosaic Pattern

float screenW : SCREENW;
float screenH : SCREENH;
float vCellSize : VAR3;
float Rotate : VAR1;
float Scale : VAR2;
bool Mirror : VAR4;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;		// should this be POSITION0?
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 r2d(float2 x, float a) {
	a *= acos(-1.0) * 2.0;
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

void vs2d(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {
	float2 R = float2(screenW, screenH);
	vp.xy *= 2.0;
	uv += 0.5 / R;
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 Cells;
	Cells.x = round(vCellSize * 64.0);
	Cells.y = round(vCellSize * 64.0);
	float2 dx = r2d(frac((x - 0.5) * Cells - 0.5) - 0.5, Rotate * acos(-1.0) * 2.0) / Cells / Scale + 0.5;
	if (Mirror) {
		dx = r2d(abs(frac((x - 0.5) * Cells / 2 - 0.5) - 0.5), Rotate * acos(-1.0) * 2.0) / Cells * 2.0 / Scale + 0.5;
	}
	float4 c = tex2D(sampler0, dx);
	if (!processAlpha) {
		c.a = 1.0;
	}
	return c;
}

technique MosaicPattern {
	pass PassMosaicPattern {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
