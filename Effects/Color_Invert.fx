// Color - Invert

// ebp-> sort out these variables, they are 'Power' and 'Mix' in the nfx.
// but they might be mixed up.
float V_Var1 : VAR1;
float V_Var2 : VAR2;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSOne(VS_OUTPUT In) : COLOR {
	float power = V_Var1;
	float level = V_Var2;
	float4 col = tex2D(sampler0, In.Tex0);
	float4 ctex = col;
	if (level >= (col.r + col.g + col.b) / 3.0) {
		ctex = 1.0 - col;
	}
	ctex.a = col.a;
	return ((ctex * power) + col * (1.0 - power));
}

float4 PSTwo(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	col.rgb = lerp(col.rgb, 1.0 - col.rgb, V_Var1);
	return col;
}

technique InvertOne {
	pass PassInvertOne {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSOne();
	}
}

technique InvertTwo {
	pass PassInvertTwo {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSTwo();
	}
}
