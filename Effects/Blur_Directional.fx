// Blur - Directional

float vDirection : VAR1;
float Width : VAR2;
float quality : VAR3;
float screenW : SCREENW;
float screenH : SCREENH;
float processAlpha : VAR16;

float4 BorderCol: COLOR = { 0.0, 0.0, 0.0, 0.0 };
bool Aspect = true;
bool Filter = true;

struct VS_OUTPUT {
	float4 Pos0 : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION0, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In, float2 uv: TEXCOORD0): COLOR {
	float2 R = float2(screenW, screenW);
	int passes = floor(quality);
	float2 asp = lerp(1.0, R.x / R, Aspect);
	float4 c = 0.0;
	float kk = 0.0;
	float vBlurX = sin(radians(vDirection * 360.0 / 1.0));
	float vBlurY = cos(radians(vDirection * 360.0 / 1.0));
	float wd = (Width / 10) * tex2D(sampler0, uv);	// can use In.Tex0 ??  Truncation!!
	for (float i = 0; i < 1; i += 1.0 / passes) {
		float k = 1.0;
		float2 tapCoords = ((((uv - 0.5) / asp + 2.0 * float2(vBlurX, vBlurY) * wd * (i)) * asp + 0.5) * k);
		c += tex2Dlod(sampler0, float4(float2(tapCoords), 0.0, 0.0));
		kk += k;
	}
	c = c / kk;
	if (!processAlpha) {
		c.a = 1.0;
	}
	return c;
}

technique DirectionalBlur {
	pass PassDirectionalBlur {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
