// Distort - HV Distort

float vIntensity : VAR1;
float vTime : TIME;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = BORDER;
	AddressV = BORDER;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 HPS(VS_OUTPUT In) : COLOR {
	float intensity = pow(vIntensity, 1.0);
	float x = In.Tex0.x + (cos((In.Tex0.y + 0.23) * 1720.0) * 0.11 + cos((In.Tex0.y - 0.45) * 2700.0) * 0.03 + cos((In.Tex0.y - 0.45) * 2250.0) * 0.08) * (intensity);
	float y = In.Tex0.y;
	float4 color = tex2D(sampler0, float2(x, y));
	return color;
}

float4 VPS(VS_OUTPUT In) : COLOR {
	float intensity = pow(vIntensity, 1.0);
	float x = In.Tex0.x;
	float y = In.Tex0.y + (cos((In.Tex0.x + 0.23) * 1720.0) * 0.11 + cos((In.Tex0.x - 0.45) * 2700.0) * 0.03 + cos((In.Tex0.x - 0.45) * 2250.0) * 0.08) * (intensity);
	float4 color = tex2D(sampler0, float2(x, y));
	return color;
}

technique HDistort {
	pass Pass1 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 HPS();
	}
}

technique VDistort {
	pass Pass1 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 VPS();
	}
}
