// Stylise - Hatchlines
// updated by neon@eerock.com

float screenW : SCREENW;
float screenH : SCREENH;
float4 ColorA : COLOR = { 0.0, 0.0, 0.0, 1.0 };
float4 ColorB : COLOR = { 1.0, 1.0, 1.0, 1.0 };
float threshold : VAR1 = 5.0;
float thickness : VAR2 = 5.0;
float rot : VAR3 = 0.0;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 r2d(float2 x, float a) {
	a *= acos(-1.0) * 2.0;
	return float2(cos(a) * x.x + sin(a) * x.y, cos(a) * x.y - sin(a) * x.x);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(float2 vp : VPOS, float2 tex0 : TEXCOORD0): COLOR {
	float2 R = float2(screenW, screenH);

	// old way, before  we passed in only the VPOS...
	// looks like it tries to calculate uv from pos and sample it.
	//float2 uv = (vp + 0.5) / R;
	//float4 col = tex2D(Samp, uv);

	float4 col = tex2D(sampler0, tex0);
	col.rgb = sqrt(col.rgb);
	return lerp(ColorA, ColorB, any(((r2d(vp - R * 0.5, rot) + R).y) % thickness < col.rgb * threshold)) * float4(1.0, 1.0, 1.0, col.a);
}

technique HatchLine {
	pass PassHatchLine {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
