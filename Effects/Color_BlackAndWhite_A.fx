// Color - Black & White A

float vbrightness : VAR1;
float vMix : VAR2;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float brightness = vbrightness * 10.0;
	float4 col = tex2D(sampler0, In.Tex0); 
	float4 bw = { 0.30, 0.59, 0.11, 0.0 };
	float4 output = brightness * dot(col, bw);
	return lerp(col, output, vMix);
}

technique BlackAndWhiteA {
	pass PassBlackAndWhiteA {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
