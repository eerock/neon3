// Distort - Polar

float V_Alpha : ALPHA;
float slider : VAR1;
float repeat : VAR2;
float scale : VAR3;
float offsetx : VAR4;
float offsety : VAR5;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float2 cartesian(float2 coords) {
	float2 nuevas;
	nuevas[0] = coords[0] - (0.5 + offsetx);
	nuevas[1] = 1.0 - coords[1];	// ?
	nuevas[1] = coords[1] - (0.5 + offsety);
	return nuevas;
}

float2 cartToPolar(float2 coords) {
	float mag = (length(coords) * 2.0) * scale;
	if (!repeat) {
		mag = saturate(mag);
	}
	float angle = (atan2(coords[1], coords[0])) * slider;
	angle /= 6.28319;
	coords[0] = frac(angle);
	coords[1] = frac(mag);
	return coords;
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 coords;
	float4 col;
	coords = cartToPolar(cartesian(In.Tex0));
	col = tex2D(sampler0, coords);
	if (!processAlpha) {
		col.a = V_Alpha;
	}
	return col;
}

technique Polar {
	pass PassPolar {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
