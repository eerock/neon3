// Stylize - Emboss A

float strength : VAR1;
float offset : VAR2;
bool processAlpha : VAR16;
float off = 0.004;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR0 {
	float finalOffset = off * offset;
	float4 col = tex2D(sampler0, In.Tex0);
	col.rgb = 0.5;
	col -= tex2D(sampler0, In.Tex0 - finalOffset) * strength;
	col += tex2D(sampler0, In.Tex0 + finalOffset) * strength;
	col.rgb = (col.r + col.g + col.b) * 0.33333333;
	if (!processAlpha) {
		col.a = 1.0;
	}
	return col;
}

technique EmbossA {
	pass PassEmbossA {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
