// Noise - Grain
// this shader is slowwwwwwwwwwwwww

float Amount : VAR1;
float Lights : VAR2;
float Midtones : VAR3;
float Shadows : VAR4;
bool Grey : VAR5;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 dx = In.Tex0 + tex2D(sampler0, In.Tex0 * 1.5 + 0.2) * 0.6;
	float4 c = float4(
				sin(dx.yx * 28.0) + dx * dx.yx * 2.0,
				length(In.Tex0 * 2.0),
				sin(dx.x * 12.0 + dx.y * 28.0)) +
			tex2D(sampler0, dx + 0.2) * 3.0 +
			tex2D(sampler0, (dx.yx - 0.45) * 0.8 + 0.45) * 2.0 +
			tex2D(sampler0, (dx - 0.5) * 998.8 + 0.5) +
			tex2D(sampler0, (dx.yx - 0.5) * 798.8 + 0.5 + 0.31);

	c = frac(c * sqrt(float4(4.5, 5.54, 7.5, 9) * 2243.0));
	if (Grey) {
		c = c.r;
	}
	float4 map = tex2D(sampler0, In.Tex0);
	c = lerp(map, step(c, pow(map, 1.0)), Amount * (saturate(map * 2.0 - 1.0) * Lights + saturate(1.0 - map * 2.0) * Shadows + Midtones * (1.0 - 2.0 * abs(frac(map) - 0.5))));
	if (!processAlpha) {
		c.a = 1.0;
	}
	return c;
}

technique Grain {
	pass PassGrain {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
