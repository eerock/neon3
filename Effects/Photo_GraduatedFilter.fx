// Photo - Graduated Filter

float screenW : SCREENW;
float screenH : SCREENH;
float h1 : VAR1;
float s1 : VAR2;
float v1 : VAR3;
float h2 : VAR4;
float s2 : VAR5;
float v2 : VAR6;
float fade : VAR7;
float Src02Alpha : VAR9;
bool orientation : VAR8;
bool composite : VAR10;
bool affectAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 HSV_to_RGB(float4 hsv) {
	float4 outcolor = { 0.0, 0.0, 0.0, 1.0 };
	float p, q, t;
	float h, s, v;
	float i;
	if (hsv[1] == 0) {
		outcolor = hsv[2];
	} else {
		h = hsv.x * 360.0;
		s = hsv.y;
		v = hsv.z;
		if (h == 360.0) {
			h = 0.0;
		}
		h /= 60.0;
		i = floor(h);
		p = v * (1.0 - s);
		q = v * (1.0 - (s * (h - i)));
		t = v * (1.0 - (s * (1.0 - (h - i))));
		if (i == 0) {
			outcolor.r = v;
			outcolor.g = t;
			outcolor.b = p;
		} else if (i == 1) {
			outcolor.r = q;
			outcolor.g = v;
			outcolor.b = p;
		} else if (i == 2) {
			outcolor.r = p;
			outcolor.g = v;
			outcolor.b = t;
		} else if (i == 3) {
			outcolor.r = p;
			outcolor.g = q;
			outcolor.b = v;
		} else if (i == 4) {
			outcolor.r = t;
			outcolor.g = p;
			outcolor.b = v;
		} else if (i == 5) {
			outcolor.r = v;
			outcolor.g = p;
			outcolor.b = q;
		} else {
			outcolor.r = v;
			outcolor.g = t;
			outcolor.b = p;
		}
	}
	return outcolor;
}

VS_OUTPUT VS(float4 Pos : POSITION, float2 Tex0 : TEXCOORD0) {
	VS_OUTPUT Out;
	Out.Pos = Pos;
	Out.Tex0 = Tex0;
	return Out;
}

void vs2d(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 startColor = float4(h1, s1, v1, 1.0);
	startColor = HSV_to_RGB(startColor);

	float4 endColor = float4(h2, s2, v2, 1.0);
	endColor = HSV_to_RGB(endColor);

	float4 grad = In.Tex0.x;
	if (orientation) {
		grad = In.Tex0.y;
	}
	grad = pow(abs(grad), fade);

	float4 gradOut = lerp(startColor, endColor, grad);
	return gradOut;
}

float4 PSOverlay(VS_OUTPUT In) : COLOR {
	float4 startColor = float4(h1, s1, v1, 1.0);
	startColor = HSV_to_RGB(startColor);

	float4 endColor = float4(h2, s2, v2, 1.0);
	endColor = HSV_to_RGB(endColor);

	float4 grad = In.Tex0.x;
	if (orientation) {
		grad = In.Tex0.y;
	}
	grad = pow(abs(grad), fade);
	float4 gradOut = lerp(startColor, endColor, grad);
	float4 outColor = { 0.0, 0.0, 0.0, 1.0 };
	float4 src01 = tex2D(sampler0, In.Tex0);
	float4 src02 = gradOut;
	float4 temp = { 0.0, 0.0, 0.0, 1.0 };
	temp.r = (src01.r < 0.5) ?
		(2.0 * (src01.r * src02.r)) :
		1.0 - (2.0 * ((1.0 - src01.r) * (1.0 - src02.r)));
	temp.g = (src01.g < 0.5) ?
		(2.0 * (src01.g * src02.g)) :
		1.0 - (2.0 * ((1.0 - src01.g) * (1.0 - src02.g)));
	temp.b = (src01.b < 0.5) ?
		(2.0 * (src01.b * src02.b)) :
		1.0 - (2.0 * ((1.0 - src01.b) * (1.0 - src02.b)));
	outColor = lerp(temp, src01, 1.0 - Src02Alpha);
	if (!composite) {
		outColor = gradOut;
	}
	return outColor;
}

float4 PSScreen(VS_OUTPUT In) : COLOR {
	// This is our rudimentary colour picker
	float4 startColor = float4(h1, s1, v1, 0.0);
	startColor = HSV_to_RGB(startColor);

	float4 endColor = float4(h2, s2, v2, 0.0);
	endColor = HSV_to_RGB(endColor);


	// Change from horizontal to vertical
	float4 grad = In.Tex0.x;
	if (orientation) {
		grad = In.Tex0.y;
	}

	// Create a gradient
	grad = pow(abs(grad), fade);

	// Lerp the colours using the gradient
	float4 gradOut = lerp(startColor, endColor, grad);
	
	float4 inputColor = gradOut;
	float4 blendColor = tex2D(sampler0, In.Tex0);

	// R = 1 - (1-Base) × (1-Blend)
	inputColor.r = 1.0 - (1.0 - inputColor.r) * (1.0 - blendColor.r);
	inputColor.g = 1.0 - (1.0 - inputColor.g) * (1.0 - blendColor.g);
	inputColor.b = 1.0 - (1.0 - inputColor.b) * (1.0 - blendColor.b);

	// alpha
	inputColor.a = 1.0;
	if (!composite) {
		inputColor = gradOut;
	}
	return inputColor;
}

technique Overlay {
	pass PassOverlay {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSOverlay();
	}
}

technique Screen {
	pass PassScreen {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSScreen();
	}
}
