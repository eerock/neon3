// Blur - Simple Blur

float V_Var1: VAR1;
float quality: VAR2;
float screenW: SCREENW;
float screenH: SCREENH;

struct VS_OUTPUT {
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = ANISOTROPIC;
};

void VS(inout float4 vp : POSITION0, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR0 {
	float xr = V_Var1 / screenW;
	float yr = V_Var1 / screenH;
	int SAMPLES = 6;
	float4 rxy = float4(0.0, 0.0, 0.0, 0.0);
	for (int i = 0; i < SAMPLES; i++) {
		for (int j = 0; j < SAMPLES; j++) {
			float2 tapCoords = In.Tex0 + float2((i - 2) * xr, (j - 2) * yr);
			rxy += tex2D(sampler0, In.Tex0 + float2((i - 2) * xr, (j - 2) * yr));
		}
	}
	float4 op = rxy / 36.0;
	return op;
}

technique SimpleBlur {
	pass PassSimpleBlur {
		AddressU[0] = CLAMP;
		AddressV[0] = CLAMP;
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
