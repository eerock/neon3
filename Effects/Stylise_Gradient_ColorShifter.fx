// Stylize - Gradient Color Shifter

float red : VAR1;
float green : VAR2;
float blue : VAR3;
float power : VAR4;
float beat : VAR5;
float speed : VAR6;
float time : TIME;

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

float zygzyg(float i,float val) {
	float n = i * beat * 1.3212120;
	return sin((n + 515.51515 + beat * 0.5963217123) * val) *
		sin((n + 142.151212 + beat * 0.051561558) * val);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float fTime = frac(time);
	float2 speed2 = speed * time;
	float2 zyg;
	float b = beat * (speed2 - 0.5) * 10.0;
	float4 col = tex2D(sampler0, In.Tex0);
	float y = In.Tex0.y * 10.0;
	col.r += sin(y * (red - 0.5) + b * 1.055616584) * power;
	col.g += sin(y * (green - 0.5) + b * 1.052051352) * power;
	col.b += sin(y * (blue - 0.5) + b * 1.059520321) * power;
	return col;
}

technique GradientColorShifter {
	pass PassGradientColorShifter {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
