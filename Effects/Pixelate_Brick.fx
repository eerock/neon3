// Stylize - Brick
// Ported to Neon from Brick.fx  by Digital Slaves
// charles@photropik.com 
// October 2012

float XSize : VAR1;
float YSize : VAR2;
float screenW : SCREENW;
float screenH : SCREENH;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float tx = floor(pow(screenW, XSize));
	float ty = floor(pow(screenH, YSize));
	float2 rez = { tx, ty };
	float2 size = 1.0 / rez;
	bool row = floor(In.Tex0.y / size.y) % 1.0;
	if (row) {
		In.Tex0.x += size.x / 2.0;
	}
	float2 count = floor(In.Tex0 / size);
	float2 center = count * size + size / 2.0;
	float4 col = tex2D(sampler0, center); 
	return col;
}

technique Brick {
	pass PassBrick {
		vertexshader = compile vs_3_0 VS();
		pixelshader =compile ps_3_0 PS();
	}
}
