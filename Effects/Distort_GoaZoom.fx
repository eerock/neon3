// Distort - Goa Zoom

float power : VAR3;
float beat : VAR4;
float speed : VAR5;
float zoom : VAR6;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float2 p;
	float z = power * 10.0;
	float b = beat * speed;
	float zoo = (1.0 + sin(b)) * zoom;
	p.x = sin((In.Tex0.x - 0.5) * z * zoo);
	p.y = sin((In.Tex0.y - 0.5) * z * zoo);
	p += In.Tex0;
	p.x = (p.x + 100.0) % 1.0;
	p.y = (p.y + 100.0) % 1.0;
	float4 ctex = tex2D(sampler0, p);
	return ctex;
}

technique GoaZoom {
	pass PassGoaZoom {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
