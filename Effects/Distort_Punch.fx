// Distort - Punch

float vLevel : VAR1;
float vPower : VAR2;
float vMix : VAR3;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = ANISOTROPIC;
	AddressU = BORDER;
	AddressV = BORDER;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float level = (2.0 * vLevel - 1.0) * 10.0;
	float power = (2.0 * vPower - 1.0) * 10.0;
	float3 zyg;
	zyg.xy = In.Tex0;
	zyg.z = 0.5;
	zyg -= 0.5f;
	float a = atan2(zyg.y, zyg.x);
	float r = length(zyg);
	r += sin(r + level * 0.25) * power * 0.25;
	float2 p;
	p.x = cos(a);
	p.y = sin(a);
	p *= r;
	p += 0.5;
	float4 col = tex2D(sampler0, In.Tex0);
	float4 punch = tex2D(sampler0, p);
	return lerp(col, punch, vMix);
}

technique PunchBorder {
	pass PassPunchBorder {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
