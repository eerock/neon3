// Distort - Crop

float v_top : VAR1;
float v_bottom : VAR2;
float v_left : VAR3;
float v_right : VAR4;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = BORDER;
	AddressV = BORDER;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float t = 2.0 * v_top - 1.0;
	float b = 2.0 * v_bottom - 1.0;
	float l = 2.0 * v_left - 1.0;
	float r = 2.0 * v_right - 1.0;
	float2 pt1 = float2(l, t);
	float2 pt2 = float2(r, b);
	pt1 = (pt1 + 1.0) / 2.0;
	pt2 = (pt2 + 1.0) / 2.0;
	float2 midpoint = lerp(pt1, pt2, 0.5);
	float scalex = distance(pt1.x, pt2.x);
	float scaley = distance(pt1.y, pt2.y);
	float borderx = midpoint.x - (scalex / 2.0);
	float2 newPos = In.Tex0;
	float bordery = 1.0 - (midpoint.y + (scaley / 2.0));
	float2 newTexCd = In.Tex0;
	newTexCd.x = In.Tex0.x * scalex + borderx;
	newTexCd.y = In.Tex0.y * scaley + bordery;
	float4 col = tex2D(sampler0, newTexCd);
	return col;
}

technique Crop {
	pass PassCrop {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
