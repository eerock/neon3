// Distort - Wave Warp

float time : TIME;
float angle : VAR3;
float width : VAR4;
float height : VAR5;
float cosa;
float sina;
bool pinningSwitch : VAR1;
static const float TWOPI = 6.28318531;
static const float PI = 3.14159265;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = BORDER;
	AddressV = BORDER;
};

float rand(float2 co) {
	return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PSSine(VS_OUTPUT In) : COLOR {
	float sina = sin(angle * 2.0 * PI);
	float cosa = cos(angle * 2.0 * PI);
	float2 dir = float2(cosa, sina);
	float2x2 R = float2x2(cosa, sina, -1.0 * sina, cosa);
	float2 xy = In.Tex0;
	float4 col = tex2D(sampler0, xy);
	float sheight = lerp(0.0, 0.5, height);
	xy -= sheight / 2.0 * dir;
	float2 rxy = mul(R, xy);
	xy += dir * sheight * sin(fmod(lerp(0.5, 45.0, width) * rxy.y + time, TWOPI));
	if (pinningSwitch) {
		xy = clamp(abs(xy), 0.01, 0.99);
	}
	col = tex2D(sampler0, xy);
	return col;
}

float4 PSSquare(VS_OUTPUT In) : COLOR {
	float sina = sin(angle * 2.0 * PI);
	float cosa = cos(angle * 2.0 * PI);
	float2 dir = float2(cosa, sina);
	float2x2 R = float2x2(cosa, sina, -1.0 * sina, cosa);
	float2 xy = In.Tex0;
	float sheight = lerp(0.0, 0.5, height);
	xy -= sheight / 2.0 * dir;
	float2 rxy = mul(R, xy);
	float one = 1.0;
	xy += dir * sheight * step(0.5, fmod(lerp(1.0, 45.0, width) * rxy.y + time, one));
	if (pinningSwitch > 0.5) {
		xy = clamp(abs(xy), 0.01, 0.99);
	}
	float4 col = tex2D(sampler0, xy);
	return col;
};

float4 PSSawtooth(VS_OUTPUT In) : COLOR {
	float sina = sin(angle * 2.0 * PI);
	float cosa = cos(angle * 2.0 * PI);
	float2 dir = float2(cosa, sina);
	float2x2 R = float2x2(cosa, sina, -1.0 * sina, cosa);
	float2 xy = In.Tex0;
	float sheight = lerp(0.0, 0.5, height);
	xy -= sheight / 2.0 * dir;
	float2 rxy = mul(R, xy);
	float one = 1.0;
	xy += dir * sheight * fmod(lerp(1.0, 45.0, width) * rxy.y + time, one);
	if (pinningSwitch) {
		xy = clamp(abs(xy), 0.01, 0.99);
	}
	float4 col = tex2D(sampler0, xy);
	return col;
};

float4 PSTriangle(VS_OUTPUT In) : COLOR {
	float sina = sin(angle * 2.0 * PI);
	float cosa = cos(angle * 2.0 * PI);
	float2 dir = float2(cosa, sina);
	float2x2 R = float2x2(cosa, sina, -1.0 * sina, cosa);	
	float2 xy = In.Tex0;
	float sheight = lerp(0.0, 0.5, height);
	xy -= sheight / 2.0 * dir;
	float2 rxy = mul(R, xy);
	float one = 1.0;
	xy += dir * sheight * abs(2.0 * fmod(lerp(1.0, 45.0, width) * rxy.y + time, one) - 1.0);
	if (pinningSwitch) {
		xy = clamp(abs(xy), 0.01, 0.99);
	}
	float4 col = tex2D(sampler0, xy);
	return col;
};

float4 PSNoise(VS_OUTPUT In) : COLOR {
	float sina = sin(angle * 2.0 * PI);
	float cosa = cos(angle * 2.0 * PI);
	float2 dir = float2(cosa, sina);
	float2x2 R = float2x2(cosa, sina, -1.0 * sina, cosa);	
	float2 xy = In.Tex0;
	float sheight = lerp(0.0, 0.1, height);
	xy -= sheight / 2.0 * dir;
	float2 rxy = mul(R, xy);
	float swidth = width + 0.1;
	float scale = 200.0;
	xy += dir * sheight * rand(float2(rxy.y * swidth * scale - frac(rxy.y * swidth * scale) + time, rxy.y * swidth * scale - frac(rxy.y * swidth * scale) + time));
	if (pinningSwitch) {
		xy = clamp(abs(xy), 0.01, 0.99);
	}
	float4 col = tex2D(sampler0, xy);
	return col;
};

technique Sine {
	pass PassSine {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSSine();
	}
}

technique Square {
	pass PassSquare {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSSquare();
	}
}

technique Sawtooth {
	pass PassSawtooth {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSSawtooth();
	}
}

technique Triangle {
	pass PassTriangle {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSTriangle();
	}
}

technique Noise {
	pass PassNoise {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSNoise();
	}
}
