// Color - Levels B

float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;
float V_Var4 : VAR4;
float4 color;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR {
	float power = V_Var1;
	float start = V_Var2;
	float end = V_Var3;
	float gamma = V_Var4;
	float4 col = tex2D(sampler0, In.Tex0);
	float4 ctex = col;
	float g = (gamma < 0.5f) ? (gamma * 2.0) : (1.0 + (gamma - 0.5) * 6.0);
	float a = g / (end - start);
	ctex = (ctex - start) * a;
	ctex.a = col.a;
	return ((ctex * power) + col * (1.0 - power));
}

technique LevelsB {
	pass PassLevelsB {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
