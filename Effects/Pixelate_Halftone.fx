// Stylize - Halftone
// todo: correct for aspect ratio so we circles instead of elipses
// todo: Need to fix this, there's a lot of things different here.

float screenW : SCREENW;
float screenH : SCREENH;

float NumTiles : VAR1;
float VSat : VAR2;
float DotSize : VAR3;
float Soft : VAR4;
bool processAlpha : VAR16;

struct vertexOutput {
	float4 HPosition: POSITION;
	float4 UV: TEXCOORD0;
	float stride: TEXCOORD1;
	float dotsize: TEXCOORD2;
	float halfstride: TEXCOORD3;
};

vertexOutput VS_Quad(float4 Position: POSITION, float4 TexCoord: TEXCOORD0) {
	//float DotSize = DSize;
	vertexOutput OUT = (vertexOutput)0;
	OUT.HPosition = float4(Position);
	OUT.UV = TexCoord;
	// Use the screen size and pow to make a nice curve to select number of tiles
	OUT.stride = 1.0 / (floor(pow(screenW, NumTiles)));
	OUT.halfstride = OUT.stride * 0.5;
	OUT.dotsize = DotSize * OUT.stride;
	return OUT;
}

sampler Sampler = sampler_state {
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

float4 pixelsPS(vertexOutput IN): COLOR {
	float2 PCenter = IN.UV - fmod(IN.UV, IN.stride) + IN.halfstride;
	float4 tileColor = tex2D(Sampler, frac(PCenter));
	float4 isinDot = (length(IN.UV - PCenter) < IN.dotsize / 2.0) * tileColor;
	if (!processAlpha) {
		isinDot.a = 1.0;
	}
	return isinDot;
}

float4 halftonePS(vertexOutput IN): COLOR {
	float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
	float2 PCenter = Pbase + IN.halfstride;
	float4 tileColor = tex2D(Sampler, PCenter);
	float bright = length(tileColor.rgb) / 1.732; 
	float isinDot = length(IN.UV - PCenter) < bright * IN.dotsize;
	float4 result = isinDot;
	if (!processAlpha) {
		result.a = 1.0;
	}
	return result;
}

float4 softhalftonePS(vertexOutput IN): COLOR {
	float Softness = Soft * 10.0;
	float Sat = VSat * 10.0;
	float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
	float2 PCenter = Pbase + IN.halfstride;
	float4 tileColor = tex2D(Sampler, PCenter);
	float bright = length(tileColor.rgb) / 1.732; 
	float rad = length(IN.UV - PCenter);
	float s = bright * IN.dotsize;
	float isinDot = rad < s;
	float4 result = 0.5 - (rad / s - 1.0) / Softness;
	result *= (tileColor * Sat);
	if (!processAlpha) {
		result.a = 1.0;
	}
	return result;
}

float4 overlap5halftoneBWPS(vertexOutput IN): COLOR {
	float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
	float2 PCenter = Pbase + IN.halfstride;
	float Sat=VSat * 10.0;
	float2 CurrentCircle = PCenter + float2(0.0, -1.0) * IN.stride;
	float4 CurrentColor = tex2D(Sampler, CurrentCircle);
	float bright = CurrentColor.r; 
	float isinDot = length(IN.UV - CurrentCircle) < bright * IN.dotsize;
	float tileColor = isinDot;
	CurrentCircle = PCenter + float2(0.0, 1.0) * IN.stride;
	CurrentColor = tex2D(Sampler, CurrentCircle);
	bright = CurrentColor.r;
	isinDot = length(IN.UV - CurrentCircle) < bright * IN.dotsize;
	tileColor = tileColor + isinDot;
	for (int x = -1; x <= 1; x++) {
		CurrentCircle = PCenter + float2(x, 0.0) * IN.stride;
		CurrentColor = tex2D(Sampler, CurrentCircle);
		bright = CurrentColor.r;
		isinDot = length(IN.UV - CurrentCircle) < bright * IN.dotsize;
		tileColor = tileColor + isinDot;
	}
	float4 result = tileColor * (tileColor * Sat);
	if (!processAlpha) {
		result.a = 1.0;
	}
	return result;
}

float4 linehalftonePS(vertexOutput IN): COLOR {
	float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
	float4 tileColor = tex2D(Sampler, Pbase);
	float bright = tileColor.rgb / 1.732; 
	float isinDot = IN.UV - Pbase < bright * IN.dotsize;
	float4 result = isinDot;
	if (!processAlpha) {
		result.a = 1.0;
	}
	return result;
}

float4 halftonepictPS(vertexOutput IN): COLOR {
	float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
	float4 tile = tex2D(Sampler, frac(IN.UV * (1.0 / IN.stride)));
	float4 tileColor = tex2D(Sampler, frac(Pbase));
	float4 result = (tile * tileColor) * 2.0;
	if (!processAlpha) {
		result.a = 1.0;
	}
	return result;
}

technique Halftone {
	pass p0 {
		vertexshader = compile vs_3_0 VS_Quad();
		pixelshader = compile ps_3_0 halftonePS();
	}
}

technique Pixels {
	pass p0 {
		vertexshader = compile vs_3_0 VS_Quad();
		pixelshader = compile ps_3_0 pixelsPS();
	}
}

technique SoftHalftone {
	pass p0 {
		vertexshader = compile vs_3_0 VS_Quad();
		pixelshader = compile ps_3_0 softhalftonePS();
	}
}

technique Overlap5HalftoneBW {
	pass p0 {
		vertexshader = compile vs_3_0 VS_Quad();
		pixelshader = compile ps_3_0 overlap5halftoneBWPS();
	}
}

technique LineHalftone {
	pass p0 {
		vertexshader = compile vs_3_0 VS_Quad();
		pixelshader = compile ps_3_0 linehalftonePS();
	}
}

technique HalftonePic {
	pass p0 {
		vertexshader = compile vs_3_0 VS_Quad();
		pixelshader = compile ps_3_0 halftonepictPS();
	}
}
