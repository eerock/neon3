// Stylize - Posterize

float Factor : VAR1;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float normalisedFactor = Factor * 2.0;
	float myFactor = 1.0 / (normalisedFactor + 0.0000001);
	float4 col = tex2D(sampler0, In.Tex0);
	col = round(col * myFactor) / myFactor;
	if (!processAlpha) {
		col.a = 1.0;
	}
	return col;
}

technique Posterize {
	pass PassPosterize {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
