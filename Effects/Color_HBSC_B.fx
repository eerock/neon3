// Color - Hue Brightness Saturation Contrast
// TextureFX HBSC
// Neon port by charles@photropik.com September 2012

float Hue : VAR1;
float Saturation : VAR2;
float Contrast : VAR3;
float Brightness : VAR4;
float HueCycles : VAR5;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = ANISOTROPIC;
};

float3 hsv2rgb(float3 h) {
	h.z += max(0.0, h.y - 1.0);
	float3 c = saturate((abs(frac(-h.x + float3(3.0, 1.0, 2.0) / 3.0) * 6.0 - 3.0) - 1.0)) * h.z;
	c = lerp(c, max(c.r, max(c.g, c.b)), 1.0 - h.y);
	return c;
}

float3 rgb2hsv(float3 c) {
	float cmax = max(c.r, max(c.g, c.b));
	float cmin = min(c.r, min(c.g, c.b));
	float l = cmax;
	float d = (cmax - cmin);
	float s = max(abs(c.r - c.g), max(abs(c.g - c.b), abs(c.r - c.b))) / cmax;
	float h;
	float3 ch = (cmax == c);
	if (ch.r == ch.g && ch.r == 1.0) {
		ch.rg = float2(1.0, 0.0);
	}
	if (ch.g == ch.b && ch.g == 1.0) {
		ch.gb = float2(1.0, 0.0);
	}
	if (ch.b == ch.r && ch.b == 1.0) {
		ch.br = float2(1.0, 0.0);
	}
	h = frac((dot(min(2.0, ch), (c.gbr - c.brg)/d + float3(0.0, 2.0, 4.0))) / 6.0);
	if (cmax == cmin) {
		h = s = 0;
	}
	return float3(h, s, l);
}

float3 hsl2rgb(float3 h) {
	return lerp(h.z, saturate((abs(frac(-h.x + float3(3.0, 1.0, 2.0) / 3.0) * 6.0 - 3.0) - 1.0)) * h.z * 2.0, saturate(h.y) * saturate(2.0 - 2.0 * h.z));
}

float3 rgb2hsl(float3 c) {
	float cmax = max(c.r, max(c.g, c.b));
	float cmin = min(c.r, min(c.g, c.b));
	float l = (cmax + cmin) / 2.0;
	float d = (cmax - cmin);
	float s = l > 0.5 ? d / (2.0 - cmax - cmin) / l / 2.0 : d / (cmax + cmin);
	float h;
	float3 ch = (cmax == c);
	if (ch.r == ch.g && ch.r == 1.0) {
		ch.rg = float2(1.0, 0.0);
	}
	if (ch.g == ch.b && ch.g == 1.0) {
		ch.gb = float2(1.0, 0.0);
	}
	if (ch.b == ch.r && ch.b == 1.0) {
		ch.br = float2(1.0, 0.0);
	}
	h = frac((dot(min(2.0, ch), (c.gbr - c.brg)/d + float3(0.0, 2.0, 4.0))) / 6.0);
	if (cmax == cmin) {
		h = s = 0;
	}
	return float3(h, s, l);
}

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 c = tex2D(sampler0, In.Tex0);
	float3 h = rgb2hsl(c.rgb);
	h.x = frac(h.x + Hue) * HueCycles;
	h.y *= Saturation;
	c.rgb = hsl2rgb(h);
	c.rgb = normalize(c.rgb) * sqrt(3.0) * pow(length(c.rgb) / sqrt(3.0), pow(2.0, Contrast)) * pow(2.0, Brightness);
	//c.rgb = h.x;
	return c;
}

technique HBSC {
	pass PassHBSC {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
