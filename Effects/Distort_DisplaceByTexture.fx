// Distort - Displace By Texture

float V_Alpha : ALPHA;
float Displacement : VAR1;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
	float2 Tex1 : TEXCOORD1;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

sampler sampler1 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv0 : TEXCOORD0, inout float2 uv1 : TEXCOORD1) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float2 disp = Displacement * 2 * ((float2)tex2D(sampler1, In.Tex1) - 0.5);
	float4 texCol = tex2D(sampler0, In.Tex0 + disp);
	texCol.a *= V_Alpha;
	return texCol;
}

technique DisplaceByTexture {
	pass PassDisplaceByTexture {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
