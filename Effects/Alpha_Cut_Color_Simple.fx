// Alpha - Cut Color Simple

float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;
float V_Var4 : VAR4;
float V_Var5 : VAR5;
float V_Var6 : VAR6;
float V_Var7 : VAR7;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PScutcolor(VS_OUTPUT In): COLOR0 {
	float4 color = tex2D(Sampler, In.Tex0);
	float c;
	float hgt = 0;
	// 
	c = (color.r + color.g + color.b) * 0.333333;
	
	hgt = c;
	
	float vVar2 = 0.0625 * V_Var2;
	
	float4 col2 = tex2D(sampler0, In.Tex0 + float2(vVar2, 0));
	hgt += (col2.r + col2.g + col2.b) * 0.333333;
	col2 = tex2D(sampler0, In.Tex0 + float2(-vVar2, 0));
	hgt += (col2.r + col2.g + col2.b) * 0.333333;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, vVar2));
	hgt += (col2.r + col2.g + col2.b) * 0.333333;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, -vVar2));
	hgt += (col2.r + col2.g + col2.b) * 0.333333;
	vVar2 = 0.75 * vVar2;

	// no idea what this is
	/*col2 = tex2D(sampler0, In.Tex0 + float2(V_Var2, V_Var2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(sampler0, In.Tex0 + float2(-V_Var2, V_Var2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(sampler0, In.Tex0 + float2(-V_Var2, -V_Var2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(sampler0, In.Tex0 + float2(V_Var2, -V_Var2));
	hgt += (col2.r + col2.g + col2.b) / 3;

	hgt *= 2;
	V_Var2 *= 2;
	col2 = tex2D(sampler0, In.Tex0 + float2(V_Var2, 0));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(sampler0, In.Tex0 + float2(-V_Var2, 0));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, V_Var2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, -V_Var2));
	*/
	hgt *= 0.5;
	
	float vVar4 = (c - V_Var4) / V_Var5;
	
	vVar4 > 1 ? 1 : vVar4;
	
	vVar4 < 0 ? 0 : vVar4;
	
	hgt = vVar4 + 0.1;
	//hgt>1?1:hgt;
	
	color.r *= (hgt > V_Var6 * 4) * V_Var7;
	color.g *= (hgt > V_Var6 * 4) * V_Var7;
	color.b *= (hgt > V_Var6 * 4) * V_Var7;
	hgt = hgt < 0 ? 0 : hgt;
	hgt = hgt > 1 ? 1 : hgt;
	color.a = (4 * hgt * V_Var1 + V_Var3);
	return color;
}

float4 PScutblack(VS_OUTPUT In): COLOR0 {
	float4 color = tex2D(Sampler, In.Tex0.xy);
	float c;
	float hgt = 0;
	c = (color.r + color.g + color.b) / 3;
	hgt = c;
	float VVar2 = 0.0625 * V_Var2;
	float4 col2 = tex2D(Sampler, In.Tex0 + float2(VVar2, 0));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(-VVar2, 0));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(0, VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0+float2(0, -VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	VVar2 = 0.75 * VVar2;
	col2 = tex2D(Sampler, In.Tex0 + float2(VVar2, VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(-VVar2, VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(-VVar2, -VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(VVar2, -VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	hgt *= 2;
	VVar2 *= 2;
	col2 = tex2D(Sampler, In.Tex0 + float2(VVar2, 0));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(-VVar2, 0));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(0, VVar2));
	hgt += (col2.r + col2.g + col2.b) / 3;
	col2 = tex2D(Sampler, In.Tex0 + float2(0, -VVar2));
	hgt *= 0.5;
	color *= V_Var7;
	hgt = abs(hgt);
	color.a = (4 * hgt * V_Var1 + V_Var3);
	return color;
}

float4 PSsimplcol(VS_OUTPUT In): COLOR0 {
	float4 color = tex2D(Sampler, In.Tex0.xy);
	float c;
	c = (color.r + color.g + color.b) * 0.333;
	c = pow(c + V_Var3, 16 * V_Var5 * V_Var5);
	color.r *= (c > V_Var6 * 4) * V_Var7;
	color.g *= (c > V_Var6 * 4) * V_Var7;
	color.b *= (c > V_Var6 * 4) * V_Var7;
	color.a = (c * V_Var1 + V_Var3);
	return color;
}

technique CutColor {
	pass PassCut {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PScutcolor();
	}
}

technique CutBlack {
	pass PassCut {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PScutblack();
	}
}

technique SimpleCol {
	pass PassCut {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PSsimplcol();
	}
}
