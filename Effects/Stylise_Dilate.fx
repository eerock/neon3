// Stylize - Dilate

float pixelSize : VAR1;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

float2 samples[8] = {
	-1.0, -1.0,
	0.0, -1.0,
	1.0, -1.0,
	-1.0, 0.0,
	1.0, 0.0,
	-1.0, 1.0,
	0.0, 1.0,
	1.0, 1.0,
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float Dilate_pixelSize = pixelSize / 10.0;
	float4 maxSamp = tex2D(sampler0, In.Tex0);
	for (int i = 0; i < 8; i++) {
		float4 sample = tex2D(sampler0, In.Tex0 + Dilate_pixelSize * samples[i]);
		maxSamp = max(maxSamp, sample);
	}
	if (!processAlpha) {
		maxSamp.a = 1.0;
	}
	return maxSamp;
}

technique Dilate {
	pass PassDilate {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
