// Noise - Static

float fNintensity : VAR1;
float fSintensity : VAR2;
float fScount : VAR3;
float Timer : TIME;
float chromaLevel : VAR4;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	//float4 Color : COLOR0;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float normalisedfScount = 4096.0 * fScount;
	float4 cTextureScreen = tex2D(sampler0, In.Tex0);
	float x = cTextureScreen.x * cTextureScreen.y * Timer * 1000.0;
	x = fmod(x, 13.0) * fmod(x, 123.0);
	float dx = fmod(x, 0.01);
	float3 cResult = cTextureScreen.rgb + cTextureScreen.rgb * saturate(0.1 + dx.xxx * 100.0);
	float2 sc;
	sincos(cTextureScreen.y * normalisedfScount, sc.x, sc.y);
	cResult += cTextureScreen.rgb * float3(sc.x, sc.y, sc.x) * fSintensity;
	cResult = lerp(cTextureScreen, cResult, saturate(fNintensity));
	float3 blackWhite = dot(cResult.rgb, float3(0.3, 0.59, 0.11));
	cResult = lerp(blackWhite, cResult, chromaLevel);
	return float4(cResult, cTextureScreen.a);
}

technique Static {
	pass PassStatic {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
