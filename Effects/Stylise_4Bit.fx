// Stylize - 4-Bit

float strength = 1.0;
float vVar2 : VAR2;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float v_Var = vVar2 * 8.0;
	float4 rgba = tex2D(sampler0, In.Tex0);
	float c = strength;
	rgba.x = 0.5 + c * (rgba.x - 0.5);
	rgba.y = 0.5 + c * (rgba.y - 0.5);
	rgba.z = 0.5 + c * (rgba.z - 0.5);
	float K = 256.0;
	if (v_Var <= 8.0) {
		K = 255.0;
	}
	if (v_Var <= 7.0) {
		K = 128.0;
	}
	if (v_Var <= 6.0) {
		K = 64.0;
	}
	if (v_Var <= 5.0) {
		K = 32.0;
	}
	if (v_Var <= 4.0) {
		K = 16.0;
	}
	if (v_Var <= 3.0) {
		K = 8.0;
	}
	if (v_Var <= 2.0) {
		K = 4.0;
	}
	if (v_Var <= 1.0) {
		K = 2.0;
	}
	int r = (rgba.x + 1.0 / K) * K;
	int g = (rgba.y + 1.0 / K) * K;
	int b = (rgba.z + 1.0 / K) * K;
	float4 col = float4(r / K, g / K, b / K, rgba.w);
	return col;
}

technique Bit4 {
	pass PassBit4 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
