// Source - Chessboard

float screenW : SCREENW;
float screenH : SCREENH;
float Horizontal : VAR1;
float Vertical : VAR2;
float4 Black : COLOR = { 0.0, 0.0, 0.0, 1.0 };
float4 White : COLOR = { 1.0, 1.0, 1.0, 1.0 };
float2 R;

bool invertAlpha : VAR14;
bool affectAlpha : VAR16;
bool compositeSourceOnBlack : VAR15;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

VS_OUTPUT VS(float4 Pos : POSITION, float2 Tex0 : TEXCOORD0) {
	VS_OUTPUT Out;
	Out.Pos = Pos;
	Out.Tex0 = Tex0;
	return Out;
}

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

float4 PS(VS_OUTPUT In) : COLOR {
	float tx = floor(pow(screenW, Horizontal));
	float ty = floor(pow(screenH, Vertical));
	
	float2 stepHV = float2(Horizontal, Vertical);
	float2 cHV = abs(In.Tex0) / stepHV;
	cHV = cHV % 2 >= 1;
	int2 b = In.Tex0 < 0 ? -1.0 : 0.0;
	cHV -= b;
	bool chess = (cHV.x + cHV.y) % 2;
	float4 output = lerp(Black, White, chess);
	float4 col = tex2D(sampler0, In.Tex0);
	
	if (!compositeSourceOnBlack) {
		col = output;
	}
	if (affectAlpha) {
		col.a = (output.r + output.g + output.b) / 3.0;
		if (invertAlpha) {
			col.a = 1.0 - col.a;
		}
	}
	return col;
}

technique Chessboard {
	pass PassChessboard {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
