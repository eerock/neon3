// Alpha - Cut Black

float Level : VAR1;
float Edges : VAR2;
float Fade : VAR3;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp: POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In): COLOR0 {
	float4 color = tex2D(sampler0, In.Tex0);
	float avgScale = 0.333333333;
	float c = (color.r + color.g + color.b) * avgScale;
	float hgt = c;
	float var2 = 0.0625 * Edges;
	float4 col2 = tex2D(sampler0, In.Tex0 + float2(var2, 0));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(-var2, 0));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, -var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	var2 *= 0.75;
	col2 = tex2D(sampler0, In.Tex0 + float2(var2, var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(-var2, var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(-var2, -var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(var2, -var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	hgt *= 2.0;
	var2 *= 2.0;
	col2 = tex2D(sampler0, In.Tex0 + float2(var2, 0));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(-var2, 0));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, var2));
	hgt += (col2.r + col2.g + col2.b) * avgScale;
	col2 = tex2D(sampler0, In.Tex0 + float2(0, -var2));
	hgt *= 0.5;
	color *= Fade;
	hgt = abs(hgt);
	color.a = (4.0 * hgt * Level);
	return color;
}

technique CutBlack {
	pass PassCutBlack {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
