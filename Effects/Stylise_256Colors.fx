// Stylize - 256 Colors

bool bypass : VAR15;
bool processAlpha : VAR16;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	//float4 Color: COLOR0;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	col = round(col * 10.0) / 10.0;
	if (!processAlpha) {
		col.a = 1.0;
	}
	return col;
}

technique Colors256 {
	pass PassColors256 {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
