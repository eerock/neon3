// Distort - Mirror

float x : VAR1;
float y : VAR2;
float power : VAR3;

struct VS_OUTPUT {
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float4 col = tex2D(sampler0, In.Tex0);
	float2 c = In.Tex0;
	float2 m = c;
	if (c.x < x) {
		c.x = 2.0 * x - c.x;
		if (c.x > 1.0) {
			c.x -= 1.0;
		}
	}
	if (c.y < y) {
		c.y = 2.0 * y - c.y;
		if (c.y > 1.0) {
			c.y -= 1.0;
		}
	}
	float4 ctex = tex2D(sampler0, c);
	col = ((ctex * power) + col * (1.0 - power));
	return col;
}

technique Mirror {
	pass PassMirror {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
