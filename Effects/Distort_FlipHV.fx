// Distort - Flip H/V

bool flipH : VAR1;
bool flipV : VAR2;

struct VS_OUTPUT {
	float4 Pos : POSITION;
	float2 Tex0 : TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	int fH = (flipH ? -1 : 1);
	int fV = (flipV ? -1 : 1);
	float4 col = tex2D(sampler0, In.Tex0 * int2(fH, fV));
	return col;
}

technique FlipHV {
	pass PassFlipHV {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
