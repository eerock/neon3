// Distort - Ripple Wave

float V_Alpha : ALPHA;
float V_InvScreenW : INVSCREENW;
float V_InvScreenH : INVSCREENH;
float V_Rand1 : RAND1;
float V_Rand2 : RAND2;
float V_Rand3 : RAND3;
float V_Time : TIME;
float V_STime : SINTIME;
float V_Var1 : VAR1;
float V_Var2 : VAR2;
float V_Var3 : VAR3;
float V_Var4 : VAR4;
float4 color;

struct VS_OUTPUT {
	float4 Pos: POSITION;
	float2 Tex0: TEXCOORD0;
};

sampler sampler0 = sampler_state {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

void VS(inout float4 vp : POSITION, inout float2 uv : TEXCOORD0) {}

float4 PS(VS_OUTPUT In) : COLOR {
	float power = V_Var1 * 10.0;
	float beat = V_Time * (V_Var2 * 10.0);
	float speed = V_Var3;
	float frequency = V_Var4;
	float2 p;
	float f = frequency * 10.0;
	float b = beat * speed;
	p.x = sin(In.Tex0.y * f + b) * 0.1 * power;
	p.y = sin(In.Tex0.x * f + b) * 0.1 * power;
	p += In.Tex0;
	p.x = (p.x + 100.0) % 1.0;
	p.y = (p.y + 100.0) % 1.0;
	float4 ctex = tex2D(sampler0, p);
	return ctex;
}

technique RippleWave {
	pass PassRippleWave {
		vertexshader = compile vs_3_0 VS();
		pixelshader = compile ps_3_0 PS();
	}
}
