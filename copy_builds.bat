
@echo off

set NAME=neon3

echo.
set /P ANSWER=Do you want to copy to Dropbox? (Y/N) 
echo.

REM Externals
set DROPBOX=C:\Dropbox\Neon\
set EXE_7Z="C:\Program Files\7-zip\7z.exe"

set ARCH=x86
set ARCHDIR=Win32
set BASE=.\__cur_build\%ARCH%\
set DEST=%BASE%%NAME%\

set FFLAGS=/V /F /D /Y /C
set DFLAGS=/V /F /D /E /Y /C

echo.
echo Copying %ARCH% files into %DEST%
echo.

REM xcopy license.txt %DEST% %FFLAGS%
REM xcopy info.txt %DEST% %FFLAGS%
xcopy demo.lua %DEST% %FFLAGS%
xcopy launcher.bat %DEST% %FFLAGS%

xcopy .\%ARCH%\*.dll %DEST% %FFLAGS%
xcopy .\%ARCH%\vcredist_%ARCH%.exe %DEST% %FFLAGS%
xcopy .\src\build\%ARCHDIR%\Debug\Neon\NeonD.exe %DEST% %FFLAGS%
xcopy .\src\build\%ARCHDIR%\Release\Neon\Neon.exe %DEST% %FFLAGS%

xcopy Media\3DScenes %DEST%Media\3DScenes\ %DFLAGS%
xcopy Media\Images %DEST%Media\Images\ %DFLAGS%
xcopy Media\Layouts %DEST%Media\Layouts\ %DFLAGS%
xcopy Media\SolidColors %DEST%Media\SolidColors\ %DFLAGS%
xcopy Media\Videos %DEST%Media\Videos\ %DFLAGS%

xcopy Effects\images %DEST%Effects\images\ %DFLAGS%
xcopy Effects\*.nfx %DEST%Effects\ %FFLAGS%
xcopy Effects\*.fx %DEST%Effects\ %FFLAGS%
xcopy Effects\*.dll %DEST%Effects\ %FFLAGS%
xcopy Effects\*.txt %DEST%Effects\ %FFLAGS%

REM xcopy Presets %DEST%Presets\ %DFLAGS%
xcopy Projects\*.npj %DEST%Projects\ %FFLAGS%
xcopy Skins %DEST%Skins\ %DFLAGS%

REM copy done
REM compress it to dated build

set DATED=%BASE%%NAME%_%ARCH%_%date:~10,4%.%date:~4,2%.%date:~7,2%

echo.
if exist %DATED%.7z (
	%EXE_7Z% u -r %DATED%.7z %DEST%
) else (
	%EXE_7Z% a -r %DATED%.7z %DEST%
)
echo Compressed build as %DATED%.7z

echo.
if exist %DATED%.zip (
	%EXE_7Z% u -r %DATED%.zip %DEST%
) else (
	%EXE_7Z% a -r %DATED%.zip %DEST%
)
echo Compressed build as %DATED%.zip

REM compress done
REM optional copy to dropbox

if /i {%ANSWER%} == {y} (goto :dropbox)
if /i {%ANSWER%} == {yes} (goto :dropbox)

goto :next

:dropbox

xcopy %DATED%.7z %DROPBOX% %FFLAGS%
xcopy %DATED%.zip %DROPBOX% %FFLAGS%

:next

set ARCH=x64
set ARCHDIR=x64
set BASE=.\__cur_build\%ARCH%\
set DEST=%BASE%%NAME%\

echo.
echo Copying %ARCH% files into %DEST%
echo.

REM xcopy license.txt %DEST% %FFLAGS%
REM xcopy info.txt %DEST% %FFLAGS%
xcopy demo.lua %DEST% %FFLAGS%
xcopy launcher.bat %DEST% %FFLAGS%

xcopy .\%ARCH%\*.dll %DEST% %FFLAGS%
xcopy .\%ARCH%\vcredist_%ARCH%.exe %DEST% %FFLAGS%
xcopy .\src\build\%ARCHDIR%\Debug\Neon\NeonD.exe %DEST% %FFLAGS%
xcopy .\src\build\%ARCHDIR%\Release\Neon\Neon.exe %DEST% %FFLAGS%

xcopy Media\3DScenes %DEST%Media\3DScenes\ %DFLAGS%
xcopy Media\Images %DEST%Media\Images\ %DFLAGS%
xcopy Media\Layouts %DEST%Media\Layouts\ %DFLAGS%
xcopy Media\SolidColors %DEST%Media\SolidColors\ %DFLAGS%
xcopy Media\Videos %DEST%Media\Videos\ %DFLAGS%

xcopy Effects\images %DEST%Effects\images\ %DFLAGS%
xcopy Effects\*.nfx %DEST%Effects\ %FFLAGS%
xcopy Effects\*.fx %DEST%Effects\ %FFLAGS%
xcopy Effects\*.dll %DEST%Effects\ %FFLAGS%
xcopy Effects\*.txt %DEST%Effects\ %FFLAGS%

REM xcopy Presets %DEST%Presets\ %DFLAGS%
xcopy Projects\*.npj %DEST%Projects\ %FFLAGS%
xcopy Skins %DEST%Skins\ %DFLAGS%

REM copy done
REM compress it to dated build

set DATED=%BASE%%NAME%_%ARCH%_%date:~10,4%.%date:~4,2%.%date:~7,2%

echo.
if exist %DATED%.7z (
	%EXE_7Z% u -r %DATED%.7z %DEST%
) else (
	%EXE_7Z% a -r %DATED%.7z %DEST%
)
echo Compressed build as %DATED%.7z

echo.
if exist %DATED%.zip (
	%EXE_7Z% u -r %DATED%.zip %DEST%
) else (
	%EXE_7Z% a -r %DATED%.zip %DEST%
)
echo Compressed build as %DATED%.zip

REM compress done
REM optional copy to dropbox

if /i {%ANSWER%} == {y} (goto :dropbox)
if /i {%ANSWER%} == {yes} (goto :dropbox)

goto :leave

:dropbox

xcopy %DATED%.7z %DROPBOX% %FFLAGS%
xcopy %DATED%.zip %DROPBOX% %FFLAGS%

:leave

echo.
echo Done!
