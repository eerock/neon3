﻿
@echo off

set SHADER_MODEL=_3_0
set ARCH=x64
set FXC="%DXSDK_DIR%Utilities\bin\%ARCH%\fxc.exe"
echo %FXC%
for %%f in ("Effects\*.fx") do (
	echo Compiling %%f...
	%FXC% /EPS /Tps%SHADER_MODEL% %%f
	pause
	%FXC% /EVS /Tvs%SHADER_MODEL% %%f
	pause
)

echo Done!